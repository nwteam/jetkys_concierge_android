package jp.concierge.jetkys.android.net;

/**
 * Created by zhuwenjin on 15/9/18.
 */
public class ProtocolConstant {

    public static final int TIMEOUT_HTTP_CONNECTION = 40 * 1000;
    public static final int TIMEOUT_HTTP_SO = 40 * 1000;

    public static final String SERVER_URL = "http://salient.jetkys.com/api/";
//    public static final String SERVER_URL = "http://52.193.145.1/api/";
    public static final String HTTP_URL = SERVER_URL + "index.php/";
    public static final String IMAGE_URL = "https://salient.jetkys.com/";
//    public static final String IMAGE_URL = "http://52.193.145.1/";
    public static final String WEB_VIEW_URL = "https://salient.jetkys.com/";
//    public static final String WEB_VIEW_URL = "http://jetkys:ubqzjre10QK2@52.193.145.1/";
    

    public static final String USER_AGREEMENT = HTTP_URL + "intf/agreement";

    public static final String ERR_CODE_NOT_LOGIN = "NO_LOGIN";

    public static final String ERR_MSG_TIMEOUT = "SERVER CONNECTION TIMEOUT";
    public static final String ERR_MSG_UNKNOWN = "SERVER UNKNOWN ERROR";

    public static final String ERR_RETURN_DATA_BAD = "SERVER PARAMS ERROR";
}
