package jp.concierge.jetkys.android.net.http;

public abstract class ActionListener<T> {
    public abstract void onSuccess(T result);

    public abstract void onFailure(String errCode, String errMsg);
}
