package jp.concierge.jetkys.android.net.http.client;

import android.content.Context;
import android.text.TextUtils;
import jp.concierge.jetkys.android.net.ProtocolConstant;
import jp.concierge.jetkys.android.net.http.HttpAction;
import jp.concierge.jetkys.android.net.http.client.listener.MHttpResponse;
import jp.concierge.jetkys.android.utils.PhotoUtils;
import jp.concierge.jetkys.android.utils.ZLog;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;

import java.io.*;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;

public class HttpClientRequest {
    public static final String TAG = HttpClientRequest.class.getSimpleName();

    private static HttpClientRequest instance = null;

    private Context mContext;

    private HttpClientRequest(Context context) {
        mContext = context;
    }

    public static HttpClientRequest getInstance(Context context) {
        if (instance == null) {
            instance = new HttpClientRequest(context);
        }
        return instance;
    }

    public void submit(HttpAction action,
                       MHttpResponse listener) {
        String result = null;
        switch (action.getMethod()) {
            case HttpAction.GET:
//                result = uploadRefGetReq(action.getParams(), action.getUrl());
                break;
            case HttpAction.POST:
                result = uploadRefPostReq(action.getParams(), action.getUrl());
                break;
        }

        if (TextUtils.isEmpty(result)) {
            listener.failed("0", ProtocolConstant.ERR_MSG_TIMEOUT);
            return;
        }

        listener.success(result);
    }

    public String uploadRefPostReq(Map<String, String> params, String url) {
        ZLog.d(TAG, "--- DO POST: [" + url + "] ---");

        String result = null;
        BufferedReader reader = null;
        try {
            HttpClient client = new DefaultHttpClient();
            client.getParams().setParameter(
                    CoreConnectionPNames.CONNECTION_TIMEOUT,
                    ProtocolConstant.TIMEOUT_HTTP_CONNECTION);
            client.getParams().setParameter(
                    CoreConnectionPNames.SO_TIMEOUT,
                    ProtocolConstant.TIMEOUT_HTTP_SO);

            HttpPost request = new HttpPost();
//            request.setHeader("Authorization", "Basic amV0a3lzOnVicXpqcmUxMFFLMg==");
            request.setURI(new URI(url));

//            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//
//            // Generate K-V Entities
//            if (params != null && !params.isEmpty()) {
//                for (String key : params.keySet()) {
//                    String v = params.get(key);
//                    String value = v == null ? "" : v;
//
//
//                    if (PhotoUtils.isPhotoParam(value)) {
//                        String absPath = PhotoUtils.getUploadPhotoPath(value);
//                        ZLog.d(TAG, "Param: K=[" + key + "], FV=[" + absPath + "]");
//                        File file = new File(absPath);
//                        entity.addPart(key, new FileBody(file));
//                    } else {
//                        ZLog.d(TAG, "Param: K=[" + key + "], SV=[" + value + "]");
//                        entity.addPart(key, new StringBody(value, Charset.forName("UTF-8")));
//                    }
//                }
//            }
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);    //如果有SocketTimeoutException等情况，可修改这个枚举

            // Generate K-V Entities
            if (params != null && !params.isEmpty()) {
                for (String key : params.keySet()) {
                    String v = params.get(key);
                    String value = v == null ? "" : v;


                    if (PhotoUtils.isPhotoParam(value)) {
                        String absPath = PhotoUtils.getUploadPhotoPath(value);
                        ZLog.d(TAG, "Param: K=[" + key + "], FV=[" + absPath + "]");
                        File file = new File(absPath);
                        builder.addPart(key, new FileBody(file));
                    } else {
                        ZLog.d(TAG, "Param: K=[" + key + "], SV=[" + value + "]");
                        builder.addTextBody(key, value,  ContentType.TEXT_PLAIN.withCharset("UTF-8"));
                    }
                }
            }

            HttpEntity entity = builder.build();
            request.setEntity(entity);

            HttpResponse response = client.execute(request);
            reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer strBuffer = new StringBuffer("");
            String line = null;
            while ((line = reader.readLine()) != null) {
                strBuffer.append(line);
            }
            result = strBuffer.toString();

        } catch (ConnectTimeoutException e) {
            errJson(ProtocolConstant.ERR_MSG_TIMEOUT);
            ZLog.v(ProtocolConstant.ERR_MSG_TIMEOUT);
            e.printStackTrace();
        } catch (Exception e) {
            errJson(ProtocolConstant.ERR_MSG_UNKNOWN);
            ZLog.v("Exception e" + ProtocolConstant.ERR_MSG_UNKNOWN);
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                    reader = null;
                } catch (IOException e) {
                    ZLog.v("IOException e" + ProtocolConstant.ERR_MSG_UNKNOWN);
                    errJson(ProtocolConstant.ERR_MSG_UNKNOWN);
                    e.printStackTrace();
                }
            }
        }

        ZLog.d(TAG, "RETURN:" + result);

        return result;
    }

    private String testJson = "{\"error\":null,\"method\":\"get_version\",\"iv\":\"3df8c6e2543e205607853d6efafe34c1\",\"token\":\"2a9999c293cd213e4821d53c67860d0f9cd1ea587b3b1dd7eb71e62bba42a10f\",\"datetime\":\"20160203134311\",\"request_no\":\"0\",\"data\":\"56e8b034e7db7c61355f20db9e88edadfdc76cb01e07a97b239f8f05d0870a5ee1febc7aa2eb783ba464ec0bf2f783c7dfe37bb6c302309585daa747988aef161830994142cc9db6bd30dd7e82333194cf2b8863b874deeade3c23c7c1420af7\"}";
    private String errJson = "{\"error\":{\"code\":201,\"message\":\"不正なアクセスです\"},\"method\":\"get_version\",\"iv\":\"6ffec7cc5ee0ed8591f6ca0c3f7405c1\",\"token\":\"1f4ff1e95685a231fc912f1c69941344cc7373551b9053875b1021ea30988e9b\",\"datetime\":\"20160324231049\",\"request_no\":0,\"data\":\"e7aafea441b64876f52fb393b66a10399e44fdf7138cfd1b2c6fd67cc99c44a42e98a30b11d4f39450f68a5da167ac8e\"}";

    private String errJson(String errMsg) {
        return "{\"error\": { \"code\":0,\"message\":\"" + errMsg + "\"},\"data\": \"\"}";
    }

//    private class MyHostnameVerifier implements HostnameVerifier {
//
//        @Override
//        public boolean verify(String hostname, SSLSession session) {
//            return true;
//        }
//    }
//
//    private class MyTrustManager implements X509TrustManager {
//
//        @Override
//        public void checkClientTrusted(X509Certificate[] chain, String authType)
//                throws CertificateException {
//        }
//
//        @Override
//        public void checkServerTrusted(X509Certificate[] chain, String authType)
//                throws CertificateException {
//        }
//
//        @Override
//        public X509Certificate[] getAcceptedIssuers() {
//            return null;
//        }
//    }
//
//    private void saveCookies(String orginCookie) {
//        if (TextUtils.isEmpty(orginCookie)) {
//            return;
//        }
//
//        SharedPreferences sp = mContext.getSharedPreferences(COOKIE, Context.MODE_PRIVATE);
//        sp.edit().putString("cookie", orginCookie).commit();
//    }
//
//    public String loadCookies() {
//        SharedPreferences sp = mContext.getSharedPreferences(COOKIE, Context.MODE_PRIVATE);
//        String cookie = sp.getString("cookie", null);
//        return cookie;
//    }
//
//    public String uploadRefGetReq(Map<String, String> params, String url) {
//        String reStr = null;
//        try {
//            HttpGet method = new HttpGet(url);
//            BasicHttpParams httpParams = new BasicHttpParams();
//            HttpConnectionParams.setConnectionTimeout(httpParams, 15000);
//            HttpConnectionParams.setSoTimeout(httpParams, 15000);
//            if (params != null && params.size() > 0) {
//                for (String key : params.keySet()) {
//                    httpParams.setParameter(key, params.get(key));
//                }
//            }
//            HttpClient client = new DefaultHttpClient(httpParams);
//
//            org.apache.http.HttpResponse httpResponse = client.execute(method);
//            int status = httpResponse.getStatusLine().getStatusCode();
//            if (status == 200) {
//                String acceptEncoding = "";
//                if (method.getFirstHeader("Content-Encoding") != null)
//                    acceptEncoding = method.getFirstHeader("Content-Encoding").getValue();
//                if (acceptEncoding.toLowerCase().indexOf("gzip") > -1) {
//                    GZIPInputStream gzin = new GZIPInputStream(httpResponse.getEntity().getContent());
//                    reStr = IOUtils.toString(gzin, "utf-8");
//                    if (gzin != null) {
//                        gzin.close();
//                    }
//                } else {
//                    reStr = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//        }
//        return reStr;
//    }
}
