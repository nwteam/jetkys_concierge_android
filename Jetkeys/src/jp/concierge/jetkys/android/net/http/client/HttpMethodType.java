package jp.concierge.jetkys.android.net.http.client;

import jp.concierge.jetkys.android.net.ProtocolConstant;
import jp.concierge.jetkys.android.net.http.HttpAction;

public enum HttpMethodType {
    REGISTER("gossip/create_user", "create_user", HttpAction.POST),
    LOGIN("gossip/auth", "auth", HttpAction.POST),
    SAVE_PUSH("gossip/save_push", "save_push", HttpAction.POST),
    UPDATE_USER("gossip/upload_user", "upload_user", HttpAction.POST),
    UPDATE_USER_AVATAR("gossip/upload_user_image", "", HttpAction.POST),
    GET_CONCIERGE_LIST("concierge/get_concierge_list", "get_concierge_list", HttpAction.POST),
    GET_CONCIERGE("concierge/get_concierge", "get_concierge", HttpAction.POST),
    ADD_MY_CONCIERGE("concierge/add_my_concierge", "add_my_concierge", HttpAction.POST),
    REMOVE_MY_CONCIERGE("concierge/remove_my_concierge", "remove_my_concierge", HttpAction.POST),
    GET_DIAGNOSIS_LIST("concierge/get_diagnosis_list", "get_diagnosis_list", HttpAction.POST),
    GET_DIAGNOSIS("concierge/get_diagnosis", "get_diagnosis", HttpAction.POST),
    GET_NOTICE_LIST("concierge/get_notice_list", "get_notice_list", HttpAction.POST),
    GET_FRIEND_LIST("concierge/contact_list", "contact_list", HttpAction.POST),
    GET_NOTICE("concierge/get_notice", "get_notice", HttpAction.POST),
    GET_MAIL_INBOX_LIST("concierge/get_mail_inbox_list_new", "get_mail_inbox_list_new", HttpAction.POST),
    GET_MAIL_SEND_LIST("concierge/get_mail_send_list_new", "get_mail_send_list_new", HttpAction.POST),
    GET_MAIL_INFO_LIST("concierge/mail_info_list", "mail_info_list", HttpAction.POST),
    GET_MAIL("concierge/get_mail", "get_mail", HttpAction.POST),
    SEND_MAIL("concierge/send_mail", "send_mail", HttpAction.POST),
    UPLOAD_IMAGE("upload_image", "index", HttpAction.POST),
    SET_USER_IMAGE("gossip/upload_user_image", "upload_user_image", HttpAction.POST),
    GET_USER_INFO("gossip/user_info", "user_info", HttpAction.POST),
    EDIT_USER("gossip/edit_user", "edit_user", HttpAction.POST),
    UPLOAD_USER("gossip/upload_user", "upload_user", HttpAction.POST),
    GET_CONVERSATION_LIST("concierge/get_message_list", "get_message_list", HttpAction.POST),
    GET_CHATMSG_LIST("concierge/get_message", "get_message", HttpAction.POST),
    SEND_MESSAGE("concierge/send_message", "send_message", HttpAction.POST),
    ENTER_MESSAGE("concierge/enter_message", "enter_message", HttpAction.POST),
    SUBMIT_SETTING("concierge/user_setting", "user_setting", HttpAction.POST),
    GET_SNS_USER("gossip/get_sns_user", "get_sns_user", HttpAction.POST),
    UPDATE_SNS_TOKEN("gossip/update_sns_token", "update_sns_token", HttpAction.POST),;


    public String URL;
    public String methodStr;
    public int method;

    HttpMethodType(String url, String methodStr, int method) {
        this.URL = ProtocolConstant.HTTP_URL + url;
        this.methodStr = methodStr;
        this.method = method;
    }
}
