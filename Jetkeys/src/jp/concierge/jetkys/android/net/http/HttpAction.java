package jp.concierge.jetkys.android.net.http;

import android.text.TextUtils;
import jp.concierge.jetkys.android.net.http.client.HttpMethodType;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HttpAction {
    public static final int POST = 1;
    public static final int GET = 2;
    public static final int PUT = 3;

    private HashMap<String, String> params;
    private JSONObject obj;
    private ActionListener<?> mActionListener;
    private String url;
    private boolean needParase;
    private int method;
    private String methodStr;

    public HttpAction(HttpMethodType type) {
        params = new HashMap<>();
        this.url = type.URL;
        this.method = type.method;
        this.methodStr = type.methodStr;
        needParase = true;
        obj = new JSONObject();
    }

    public int getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public boolean getNeedParase() {
        return needParase;
    }

    public void setNeedParase(boolean needParase) {
        this.needParase = needParase;
    }

    public void setActionListener(ActionListener<?> listener) {
        mActionListener = listener;
    }

    public ActionListener<?> getActionListener() {
        return mActionListener;
    }

    public JSONObject getJson() {
        return obj;
    }

    public void putAll(JSONObject json) {
        obj = json;
    }

    public Object getJsonParam(String key) {
        try {
            if (key == null || obj.isNull(key)) {
                return null;
            }

            return obj.get(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void putJson(String key, Object value) {
        try {
            if (value == null) {
                obj.remove(key);
                return;
            }
            obj.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void putParam(String key, String value) {
        params.put(key, value == null ? "" : value);
    }

    public void putParam(String key, int value) {
        params.put(key, String.valueOf(value));
    }

    public void putParam(String key, boolean value) {
        params.put(key, value ? "true" : "false");
    }

    public HashMap<String, String> getParams() {
        return params;
    }

    public String getAssembledJsonStr() {
        JSONObject object = new JSONObject();

        try {
            if (!TextUtils.isEmpty(methodStr)) {
                object.put(methodStr, obj);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return object.toString();
    }
}

