package jp.concierge.jetkys.android.net.http.client.listener;

public abstract class MHttpResponse {
    public abstract void success(String responseJson);

    public abstract void failed(String errCode, String errMsg);
}
