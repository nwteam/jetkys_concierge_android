package jp.concierge.jetkys.android.net.thread;

import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import java.util.ArrayDeque;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
*
* @since 1.5
* @author Doug Lea
*/
public abstract class AsyncTask<Params, Progress, Result> {
    private static final String LOG_TAG = "QAsyncTask";

    private static final int CORE_POOL_SIZE = 3;
    private static final int MAXIMUM_POOL_SIZE = 128;
    private static final int KEEP_ALIVE = 1;
    private static final BlockingQueue<Runnable> sPoolWorkQueue = new LinkedBlockingQueue<Runnable>(200);
    
    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable r) {
            return new Thread(r, LOG_TAG + "#" + mCount.getAndIncrement());
        }
    };

    public static final ThreadPoolExecutor THREAD_POOL_EXECUTOR
            = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE,
                    TimeUnit.SECONDS, sPoolWorkQueue, sThreadFactory, new ThreadPoolExecutor.CallerRunsPolicy());
    
    public static final SerialExecutor SERIAL_EXECUTOR = new SerialExecutor();

    private static final int MESSAGE_POST_RESULT = 0x1;
    private static final int MESSAGE_POST_PROGRESS = 0x2;

    private static final InternalHandler sHandler = new InternalHandler();

    private static volatile SerialExecutor sDefaultExecutor = SERIAL_EXECUTOR;
    private final WorkerRunnable<Params, Result> mWorker;
    private final TagFutureTask<Result> mFuture;

    private volatile Status mStatus = Status.PENDING;
    
    private final AtomicBoolean mCancelled = new AtomicBoolean();
    private final AtomicBoolean mTaskInvoked = new AtomicBoolean();

    private static class SerialExecutor{
        final ArrayDeque<TagFutureTask<?>> mTasks = new ArrayDeque<TagFutureTask<?>>();
        private ConcurrentHashMap<String, TagFutureTask<?>> mWorkMaps = new ConcurrentHashMap<String, TagFutureTask<?>>();

        @SuppressWarnings({ "rawtypes" })
        public synchronized void execute(final TagFutureTask<?> r) {
            if(r.tag != null && mWorkMaps.containsKey(r.tag)) {
                Log.d(LOG_TAG, "--->scheduleQ is exist!");
                return ;
            }
            if(r.tag != null) {
                mWorkMaps.put(r.tag, r);
            }
            mTasks.offer(new TagFutureTask(r, r.tag) {
                @Override
                public void run() {
                    try {
                        r.run();
                    } finally {
                        scheduleNext();
                    }
                }
            });
            scheduleNext();
        }

        protected synchronized void scheduleNext() {
            int count = THREAD_POOL_EXECUTOR.getCorePoolSize() - THREAD_POOL_EXECUTOR.getActiveCount();
            Log.d(LOG_TAG, "--->scheduleQNext:" + mTasks.size() + "/" + count);
            for (int i = 0; i < count; i++) {
                TagFutureTask<?> task = mTasks.poll();
                if (task != null) {
                    THREAD_POOL_EXECUTOR.execute(task);
                    if (task.tag != null) {
                        mWorkMaps.remove(task.tag);
                    }
                }
            }
        }
        
        public synchronized void cancelSchedule(String tag) {
            if(tag != null && mWorkMaps.containsKey(tag)) {
                TagFutureTask<?> task = mWorkMaps.get(tag);
                task.cancel(true);
                mTasks.remove(task);
            }
        }
    }
    
    /** 取消Tag关联的任务*/
    public static void cancalTask(String tag) {
        SERIAL_EXECUTOR.cancelSchedule(tag);
    }

    public enum Status {
        /**
         * Indicates that the task has not been executed yet.
         */
        PENDING,
        /**
         * Indicates that the task is running.
         */
        RUNNING,
        /**
         * Indicates that {@link AsyncTask#onPostExecute} has finished.
         */
        FINISHED,
    }

    public static void init() {
        sHandler.getLooper();
    }

    public static void setDefaultExecutor(SerialExecutor exec) {
        sDefaultExecutor = exec;
    }

    public AsyncTask() {
        this(null);
    }

    public AsyncTask(String tag) {
        mWorker = new WorkerRunnable<Params, Result>() {
            public Result call() throws Exception {
                mTaskInvoked.set(true);

                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                return postResult(doInBackground(mParams));
            }
        };

        mFuture = new TagFutureTask<Result>(mWorker, tag) {
            @Override
            protected void done() {
                try {
                    postResultIfNotInvoked(get());
                } catch (InterruptedException e) {
                    Log.w(LOG_TAG, e);
                } catch (ExecutionException e) {
                    throw new RuntimeException("An error occured while executing doInBackground()",
                            e.getCause());
                } catch (CancellationException e) {
                    postResultIfNotInvoked(null);
                }
            }
        };
    }

    private void postResultIfNotInvoked(Result result) {
        final boolean wasTaskInvoked = mTaskInvoked.get();
        if (!wasTaskInvoked) {
            postResult(result);
        }
    }

    private Result postResult(Result result) {
        @SuppressWarnings("unchecked")
        Message message = sHandler.obtainMessage(MESSAGE_POST_RESULT,
                new AsyncTaskResult<Result>(this, result));
        message.sendToTarget();
        return result;
    }
    
    public final Status getStatus() {
        return mStatus;
    }

    protected abstract Result doInBackground(Params... params);

    protected void onPreExecute() {
    }

    protected void onPostExecute(Result result) {
    }
    
    protected void onProgressUpdate(Progress... values) {
    }

    protected void onCancelled(Result result) {
        onCancelled();
    }    

    protected void onCancelled() {
    }

    public final boolean isCancelled() {
        return mCancelled.get();
    }

    public final boolean cancel(boolean mayInterruptIfRunning) {
        mCancelled.set(true);
        return mFuture.cancel(mayInterruptIfRunning);
    }

    public final Result get() throws InterruptedException, ExecutionException {
        return mFuture.get();
    }

    public final Result get(long timeout, TimeUnit unit) throws InterruptedException,
            ExecutionException, TimeoutException {
        return mFuture.get(timeout, unit);
    }

    public final AsyncTask<Params, Progress, Result> execute(Params... params) {
        return executeOnExecutor(sDefaultExecutor, params);
    }

    @SuppressWarnings("incomplete-switch")
	public final AsyncTask<Params, Progress, Result> executeOnExecutor(SerialExecutor exec,
            Params... params) {
        if (mStatus != Status.PENDING) {
            switch (mStatus) {
                case RUNNING:
                    throw new IllegalStateException("Cannot execute task:"
                            + " the task is already running.");
                case FINISHED:
                    throw new IllegalStateException("Cannot execute task:"
                            + " the task has already been executed "
                            + "(a task can be executed only once)");
            }
        }

        mStatus = Status.RUNNING;

        onPreExecute();

        mWorker.mParams = params;
        exec.execute(mFuture);

        return this;
    }

    @SuppressWarnings("rawtypes")
	public static void execute(Runnable runnable) {
        sDefaultExecutor.execute(new TagFutureTask(runnable));
    }

    protected final void publishProgress(Progress... values) {
        if (!isCancelled()) {
            sHandler.obtainMessage(MESSAGE_POST_PROGRESS,
                    new AsyncTaskResult<Progress>(this, values)).sendToTarget();
        }
    }

    private void finish(Result result) {
        if (isCancelled()) {
            onCancelled(result);
        } else {
            onPostExecute(result);
        }
        mStatus = Status.FINISHED;
    }
    
    @SuppressWarnings({"unchecked"})
    private static class InternalHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            @SuppressWarnings("rawtypes")
			AsyncTaskResult result = (AsyncTaskResult) msg.obj;
            switch (msg.what) {
                case MESSAGE_POST_RESULT:
                    // There is only one result
                    result.mTask.finish(result.mData[0]);
                    break;
                case MESSAGE_POST_PROGRESS:
                    result.mTask.onProgressUpdate(result.mData);
                    break;
            }
        }
    }

    private static abstract class WorkerRunnable<Params, Result> implements Callable<Result> {
        Params[] mParams;
    }
    
    @SuppressWarnings("rawtypes")
    private static class AsyncTaskResult<Data> {
		final AsyncTask mTask;
        final Data[] mData;

        AsyncTaskResult(AsyncTask task, Data... data) {
            mTask = task;
            mData = data;
        }
    }
    
    private static class TagFutureTask<Result> extends FutureTask<Result> {
        public String tag;
        
        public TagFutureTask(Callable<Result> callable, String tag) {
            super(callable);
            this.tag = tag;
        }
        
        public TagFutureTask(Runnable runable) {
            super(runable, null);
        }
        
        public TagFutureTask(Runnable runable, String tag) {
            this(runable);
            this.tag = tag;
        }
    }
}
