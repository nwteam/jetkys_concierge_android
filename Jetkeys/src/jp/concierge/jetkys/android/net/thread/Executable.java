package jp.concierge.jetkys.android.net.thread;

/**
 * Created by hy on 2014/12/12.
 */
public abstract class Executable<T> {
    public abstract T execute () throws Exception;
}
