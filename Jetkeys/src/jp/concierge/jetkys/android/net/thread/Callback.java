package jp.concierge.jetkys.android.net.thread;

public abstract class Callback<T> {

    private String tag;

    public Callback() {
    }

    public Callback(String tag) {
        this.tag = tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    protected abstract void onSucceed(T result);

    protected abstract void onError(Object object);

    public void notify(Object object, boolean isSuceecd) {
        if (isSuceecd) {
            onSucceed((T) object);
        } else {
            onError(object);
        }
    }
}