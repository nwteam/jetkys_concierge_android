package jp.concierge.jetkys.android.net.thread;

/**
 * 异步线程池，用于执行异步大计算量业务
 * @author yue.huang
 */
public class ThreadHelper {
	
    
    /** 在后台队列中执行*/
    public static AsyncTask<Void, Object, Object> executeInQThreadPool(final Executable<?> executable, final Callback<?> listener) {

        AsyncTask<Void, Object, Object> task = new AsyncTask<Void, Object, Object>(listener != null ? listener.getTag() : null) {
            @Override
            protected Object doInBackground(Void... params) {
                Object object = null;
                if(executable == null){
                    return null;
                }
                try {
                    object = executable.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                    object = e;
                }
                if (listener != null) {
                    if (object != null && object instanceof Exception) {
                        listener.notify(object, false);
                    } else {
                        try {
                            listener.notify(object, true);
                        } catch (Exception e) {
                            e.printStackTrace();
                            listener.notify("操作失败", false);
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
            }
        };

        try {
            task.execute();
        } catch (Exception e) {
            e.printStackTrace();
            if (listener != null) {
                listener.notify("操作失败", false);
            }
        }
        return task;
    }
    

}