package jp.concierge.jetkys.android;

import android.app.*;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender.SendIntentException;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import cn.jpush.android.api.JPushInterface;
import jp.concierge.jetkys.android.beans.LoginBean;
import jp.concierge.jetkys.android.beans.Message;
import jp.concierge.jetkys.android.gameutils.encrypt.EncryptData;
import jp.concierge.jetkys.android.manager.AccountManager;
import jp.concierge.jetkys.android.manager.AccountManager.LOGIN_TYPE;
import jp.concierge.jetkys.android.manager.ManagerFactory;
import jp.concierge.jetkys.android.net.http.ActionListener;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.push.ChatManager;
import jp.concierge.jetkys.android.push.ZJPushReceiver;
import jp.concierge.jetkys.android.ui.*;
import jp.concierge.jetkys.android.utils.Utils;
import jp.concierge.jetkys.android.utils.ZLog;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class MainActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener, ConnectionCallbacks{
    public static final int CONTAINER = R.id.realtabcontent;

    public static final String FILTER_LOGIN_COMPLETE = "MainActivity.FILTER_LOGIN_COMPLETE";

    public static final String TAB0 = "TopPageFragment";
    public static final String TAB1 = "FriendFragment";
    public static final String TAB2 = "MsgListFragment";
    public static final String TAB3 = "MailListFragment";
    public static final String TAB4 = "SearchFragment";
    public static final String TAB5 = "SettingFragment";
    
    private static final int RC_SIGN_IN = 9002;
    private GoogleApiClient mGoogleApiClient;
    
    private CallbackManager mCallbackManager;
    
   	private static final String TWITTER_CONSUMER_KEY = "xrpfKiOxvXSPBhmCBpUifzztx";
   	private static final String TWITTER_CONSUMER_SECRET = "ubLLj4vtwKMqkZ6Yvs3RFw00y7YPuZr8CV4qdNRbcyyX1oQrty";

   	private static final String TWITTER_CALLBACK_URL = "twitterapp://connect";
    // Twitter oauth urls
   	private static final String URL_TWITTER_AUTH = "auth_url";
   	private static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
   	private static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";
    
 // Twitter
    private static Twitter twitter;
    private static RequestToken requestToken;
    
    public TabHost mTabHost;

    private TopPageFragment mTopPageFragment;
    private FriendFragment mFriendFragment;
    private MsgListFragment mMsgListFragment;
    private MailListFragment mMailListFragment;
    private SearchFragment mSearchFragment;
    private SettingFragment mSettingFragment;

    private int mCurrTab;

    public FragmentManager mFragmentManager;

    private Context mContext;

    private ConcurrentHashMap<Integer, BaseFragment.FResult> mResultMap;

    private LoginView mLoginView;
    private RegisterView mRegisterView;

    private NewMessageReceivedBroadCast mNewMessageReceivedBroadCast;
    private JpushReceivedBroadCast mJpushReceivedBroadCast;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    private void testTransfer(Fragment f) {
        getFragmentManager().beginTransaction().add(CONTAINER, f).commit();
    }

    public boolean checkEmpty(String param, String hint) {
        boolean con = TextUtils.isEmpty(param);
        if (con) {
            toast(String.format("%sを入力してください", hint));
        }
        return con;
    }

    private AccountManager mAccountManager;

    public ProgressDialog mLoadingDialog;

    private ChatManager mChatManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        mFragmentManager = getFragmentManager();

        mResultMap = new ConcurrentHashMap<>();

        initTab();

        mAccountManager = (AccountManager) ManagerFactory.getInstance().getManager(this, AccountManager.class);

        mLoginView = (LoginView) findViewById(R.id.loginview);
        mLoginView.setInit(this, mAccountManager);
        mRegisterView = (RegisterView) findViewById(R.id.registerview);
        mRegisterView.setInit(this, mAccountManager);

        mLoadingDialog = new ProgressDialog(this);
        mLoadingDialog.setCanceledOnTouchOutside(false);
        mLoadingDialog.setMessage("しばらくお待ちください");

        mChatManager = ChatManager.getInstance();
        mChatManager.init(this);

        mNewMessageReceivedBroadCast = new NewMessageReceivedBroadCast();
        mNewMessageReceivedBroadCast.register();

        mJpushReceivedBroadCast = new JpushReceivedBroadCast();
        mJpushReceivedBroadCast.register();

        try {
            Message msg = (Message) getIntent().getSerializableExtra("msg");
            tryStartChat(msg);
        } catch (Exception e) {
        }

        uploadPushToken();
        
        mGoogleApiClient = new GoogleApiClient.Builder(this)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(Plus.API)
        .addScope(new Scope(Scopes.PLUS_LOGIN))
        .addScope(new Scope(Scopes.PLUS_ME))
        .build();

        //[START] Facebook init
        getFBHashKey();
        FacebookSdk.sdkInitialize(getApplicationContext());
        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);

        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

        	@Override
        	public void onSuccess(LoginResult result) {
        		final AccessToken accessToken = result.getAccessToken();
        		GraphRequest request = GraphRequest.newMeRequest(
        				accessToken,
        				new GraphRequest.GraphJSONObjectCallback() {
        					@Override
        					public void onCompleted(JSONObject object,
        							GraphResponse response) {
        						ZLog.d("TAG", "Fetch user: " + object.toString());
        						ZLog.d("TAG", "Token: " + accessToken.getToken());
        						try {
//        							String profile_image_url = object.getJSONObject("picture").getJSONObject("data").getString("url");
        							loginViaThirdParty(LOGIN_TYPE.FACEBOOK, object.getString("name"), object.getString("email"), "", object.getString("id"), accessToken.getToken(),"");
//        							String unick = object.getString("name");
//        							registerViaThirdParty(unick, object.getString("email"), LOGIN_TYPE.FACEBOOK, object.getString("id"), accessToken.getToken(),"");
        						} catch (JSONException e) {
        							e.printStackTrace();
        						}

        					}
        				});
        		Bundle parameters = new Bundle();
        		parameters.putString("fields", "id, name, picture.type(large), email");
        		parameters.putString("fields", "id,name, email");
        		request.setParameters(parameters);
        		request.executeAsync();
        	}

        	@Override
        	public void onError(FacebookException error) {
        		// TODO Auto-generated method stub

        	}

        	@Override
        	public void onCancel() {
        		// TODO Auto-generated method stub

        	}
        });
        //[END] Facebook init
       
        //[START] Twitter init
        Uri uri = getIntent().getData();
        if (uri != null) {
        	ZLog.d("TAG", "[Twitter] uri: " + uri.toString());
        	if (uri.toString().startsWith(TWITTER_CALLBACK_URL)) {
        		// oAuth verifier
        		final String verifier = uri
        				.getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);
        		ZLog.d("TAG", "[Twitter] verifier: " + verifier);
        		new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
		        			// Get the access token
		        			final twitter4j.auth.AccessToken accessToken = twitter.getOAuthAccessToken(
		        					requestToken, verifier);	
		        			
		        			// Getting user details from twitter
		        			final long userID = accessToken.getUserId();
		        			User user = twitter.showUser(userID);
		        			final String unick = user.getName();
		        			final String profile_image_url = user.getProfileImageURL();
		        			final String email = "";
		        			
		        			MainActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									loginViaThirdParty(LOGIN_TYPE.TWITTER, unick, email, profile_image_url, String.valueOf(userID), accessToken.getToken(),"");
//									registerViaThirdParty(unick, email, LOGIN_TYPE.TWITTER, String.valueOf(userID), accessToken.getToken(),accessToken.getTokenSecret());
								}
							});
		        		} catch (Exception e) {
		        			// Check log for login errors
		        			// TODO Auto-generated method stub
		        			e.printStackTrace();
		        		}
					}
				}).start();
        	}
        }
        //[END] Twitter init
        ZLog.d("TAG","onCreate");
    }
    
    private void TwitterHandler(){
   	 //[START] Twitter init
       Uri uri = getIntent().getData();
       if (uri != null) {
       	ZLog.d("TAG", "[Twitter] uri: " + uri.toString());
       	if (uri.toString().startsWith(TWITTER_CALLBACK_URL)) {
       		// oAuth verifier
       		final String verifier = uri
       				.getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);
       		ZLog.d("TAG", "[Twitter] verifier: " + verifier);
       		new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
		        			// Get the access token
		        			final twitter4j.auth.AccessToken accessToken = twitter.getOAuthAccessToken(
		        					requestToken, verifier);	
		        			
		        			// Getting user details from twitter
		        			final long userID = accessToken.getUserId();
		        			User user = twitter.showUser(userID);
		        			final String unick = user.getName();
		        			final String profile_image_url = user.getProfileImageURL();
		        			final String email = "";
		        			
		        			MainActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									loginViaThirdParty(LOGIN_TYPE.TWITTER, unick, email, profile_image_url, String.valueOf(userID), accessToken.getToken(),"");
//									registerViaThirdParty(unick, email, LOGIN_TYPE.TWITTER, String.valueOf(userID), accessToken.getToken(),accessToken.getTokenSecret());
								}
							});
		        		} catch (Exception e) {
		        			// Check log for login errors
		        			// TODO Auto-generated method stub
		        			e.printStackTrace();
		        		}
					}
				}).start();
       	}
       }
       //[END] Twitter init
}

    private void tryStartChat(Message msg) {
        if (msg == null) return;

        if (TextUtils.isEmpty(mAccountManager.user_id)) return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTabHost.setCurrentTab(2);
            }
        }, 200);

//        Bundle bundle = new Bundle();
//        String whomKey = "1".equals(msg.vector) ? "cid" : "uid";
//        bundle.putString(whomKey, msg.from_id);
//        bundle.putString("title", msg.nickname);
//        bundle.putString("convid", msg.conversation_id);
//
//        final ChatFragment fragment = new ChatFragment();
//        fragment.setArguments(bundle);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                startFragment(fragment);
//            }
//        }, 200);
    }

    public void showLoading() {
        if (mLoadingDialog.isShowing()) return;

        mLoadingDialog.show();
    }

    public void dismissLoading() {
        mLoadingDialog.dismiss();
    }

    private void test() {
        String datas = "B53AA8015825F6CC75FEE710DCEBDA921088872081673A7868788C3C375070B90D0BAB357BFD1A663CF36A646D67A17778E946A1E3CD0E896962EAFC892A16C8829F362510B429E2E0E7F5B430FDB79195F00F59D175B2A38E6D638B9286A555368BB8DEF0139BDFAE8DFE2E08A05360E5F9A9D528C09CCE45F80FD642BC22D2A4763A51292D315C5F3634454A6E8A551582E8EAE531743338521DEB9AD6A9E0";
        String iv = "CE369B3ECA2285536BF179C6DDED154C";
        String realData = EncryptData.decrypt(datas, iv);

        ZLog.v("DECRYPT", realData);
    }

    @Override
    protected void onResume() {
        super.onResume();

        JPushInterface.onResume(mContext);

        if (!mAccountManager.isLogined()) {
            showLogin();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        JPushInterface.onPause(mContext);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mLoginView.onDestory();
        mRegisterView.onDestory();

        mNewMessageReceivedBroadCast.unregister();
        mNewMessageReceivedBroadCast = null;

        mJpushReceivedBroadCast.unregister();
        mJpushReceivedBroadCast = null;

        mChatManager.mMainActivity = null;
    }

    private void initTab() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();
        mTabHost.setOnTabChangedListener(mTabChangeListener);
        mTabHost.setCurrentTab(0);

        TabHost.TabSpec spec0 = mTabHost.newTabSpec(TAB0);
        spec0.setIndicator(getTabIndicator(null, R.drawable.btn_tab_toppage));
        spec0.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec0);

        TabHost.TabSpec spec1 = mTabHost.newTabSpec(TAB1);
        spec1.setIndicator(getTabIndicator(null, R.drawable.btn_tab_friend));
        spec1.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec1);

        TabHost.TabSpec spec2 = mTabHost.newTabSpec(TAB2);
        spec2.setIndicator(getTabIndicator(null, R.drawable.btn_tab_msglist));
        spec2.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec2);

        TabHost.TabSpec spec3 = mTabHost.newTabSpec(TAB3);
        spec3.setIndicator(getTabIndicator(null, R.drawable.btn_tab_mail));
        spec3.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec3);

        TabHost.TabSpec spec4 = mTabHost.newTabSpec(TAB4);
        spec4.setIndicator(getTabIndicator(null, R.drawable.btn_tab_search));
        spec4.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec4);

        TabHost.TabSpec spec5 = mTabHost.newTabSpec(TAB5);
        spec5.setIndicator(getTabIndicator(null, R.drawable.btn_tab_setting));
        spec5.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec5);
    }

    private View getTabIndicator(String name, int resId) {
        View v = getLayoutInflater().inflate(R.layout.item_tabhost, null);
        TextView tv = (TextView) v.findViewById(R.id.tv_item_tabhost_name);
        tv.setText("" + name);
        ImageView iv = (ImageView) v.findViewById(R.id.iv_item_tabhost_icon);
        iv.setImageResource(resId);
        return v;
    }

    public void toMailList() {
        mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        mTabHost.setCurrentTab(3);
    }

    /**
     * 从Map里面取出Result
     *
     * @param fragmentHashCode
     * @return
     */
    public BaseFragment.FResult getResult(int fragmentHashCode) {
        BaseFragment.FResult result = mResultMap.get(fragmentHashCode);
        mResultMap.remove(fragmentHashCode);
        return result;
    }

    /**
     * 将新Fragment的result放进上个Fragment创建的Result中，等上个Fragment取
     *
     * @param hashKey
     * @param resultCode
     * @param datas
     */
    public void makeResult(int hashKey, int resultCode, Bundle datas) {
        BaseFragment.FResult result = mResultMap.get(hashKey);
        if (result == null) return;

        result.resultCode = resultCode;
        result.datas = datas;
    }

    public void startFragmentForResult(BaseFragment sourceF,
                                       BaseFragment targetF,
                                       int requestCode) {

        Bundle bundle = targetF.getArguments();
        if (bundle == null) bundle = new Bundle();

        bundle.putBoolean(BaseFragment.NEED_FRAGMENT_RESULT, true);
        int hasKey = sourceF.hashCode();
        bundle.putInt(BaseFragment.FRAGMENT_RESULT_KEY, hasKey);
        startFragment(targetF);

        BaseFragment.FResult fResult = new BaseFragment.FResult();
        fResult.requestCode = requestCode;
        mResultMap.put(hasKey, fResult);
    }

    public void startFragment(BaseFragment targetF) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(
                android.R.animator.fade_in,
                android.R.animator.fade_out
        );
        ft.replace(MainActivity.CONTAINER, targetF);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void finishFragment() {
        mFragmentManager.popBackStack();
    }

    private void transTab0(FragmentTransaction transaction) {
        if (mTopPageFragment == null) {
            transaction.add(CONTAINER, new TopPageFragment(), TAB0);
        } else {
            transaction.attach(mTopPageFragment);
        }
    }

    private void transTab1(FragmentTransaction transaction) {
        if (mFriendFragment == null) {
            transaction.add(CONTAINER, new FriendFragment(), TAB1);
        } else {
            transaction.attach(mFriendFragment);
        }
    }

    private void transTab2(FragmentTransaction transaction) {
        if (mMsgListFragment == null) {
            transaction.add(CONTAINER, new MsgListFragment(), TAB2);
        } else {
            transaction.attach(mMsgListFragment);
        }
    }

    private void transTab3(FragmentTransaction transaction) {
        if (mMailListFragment == null) {
            transaction.add(CONTAINER, new MailListFragment(), TAB3);
        } else {
            transaction.attach(mMailListFragment);
        }
    }

    private void transTab4(FragmentTransaction transaction) {
        if (mSearchFragment == null) {
            transaction.add(CONTAINER, new SearchFragment(), TAB4);
        } else {
            transaction.attach(mSearchFragment);
        }
    }

    private void transTab5(FragmentTransaction transaction) {
        if (mSettingFragment == null) {
            transaction.add(CONTAINER, new SettingFragment(), TAB5);
        } else {
            transaction.attach(mSettingFragment);
        }
    }

    private TabHost.OnTabChangeListener mTabChangeListener = new TabHost.OnTabChangeListener() {

        @Override
        public void onTabChanged(String tabId) {
            mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            mTopPageFragment = (TopPageFragment) mFragmentManager.findFragmentByTag(TAB0);
            mFriendFragment = (FriendFragment) mFragmentManager.findFragmentByTag(TAB1);
            mMsgListFragment = (MsgListFragment) mFragmentManager.findFragmentByTag(TAB2);
            mMailListFragment = (MailListFragment) mFragmentManager.findFragmentByTag(TAB3);
            mSearchFragment = (SearchFragment) mFragmentManager.findFragmentByTag(TAB4);
            mSettingFragment = (SettingFragment) mFragmentManager.findFragmentByTag(TAB5);

            FragmentTransaction transaction = mFragmentManager.beginTransaction();

            if (mTopPageFragment != null) {
                transaction.detach(mTopPageFragment);
            }

            if (mFriendFragment != null) {
                transaction.detach(mFriendFragment);
            }

            if (mMsgListFragment != null) {
                transaction.detach(mMsgListFragment);
            }

            if (mMailListFragment != null) {
                transaction.detach(mMailListFragment);
            }

            if (mSearchFragment != null) {
                transaction.detach(mSearchFragment);
            }

            if (mSettingFragment != null) {
                transaction.detach(mSettingFragment);
            }

            if (tabId.equals(TAB0)) {
                transTab0(transaction);
            } else if (tabId.equals(TAB1)) {
                transTab1(transaction);
            } else if (tabId.equals(TAB2)) {
                transTab2(transaction);
            } else if (tabId.equals(TAB3)) {
                transTab3(transaction);
            } else if (tabId.equals(TAB4)) {
                transTab4(transaction);
            } else if (tabId.equals(TAB5)) {
                transTab5(transaction);
            }
            transaction.commitAllowingStateLoss();
            mCurrTab = mTabHost.getCurrentTab();
        }
    };

    public static class DummyTabContent implements TabHost.TabContentFactory {
        private Context mContext;

        public DummyTabContent(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String s) {
            View v = new View(mContext);
            return v;
        }
    }

    public void toast(String msg) {
        Utils.toast(mContext, msg);
    }

    private class NewMessageReceivedBroadCast extends BroadcastReceiver {

        public void unregister() {
            unregisterReceiver(this);
        }

        public void register() {
            registerReceiver(this, new IntentFilter(ChatManager.FILTER_NEW_MSG_RECEIVED));
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                return;
            }

            final Message msg = (Message) intent.getSerializableExtra("msg");

            if (!mChatManager.conversationId.equals(msg.conversation_id)
                    && mChatManager.mChatFragment != null) {
                // 如果当前聊天的人不是这个人，则消失
                mChatManager.mChatFragment.finish();
            }

            tryStartChat(msg);
        }
    }

    private class JpushReceivedBroadCast extends BroadcastReceiver {

        public void unregister() {
            unregisterReceiver(this);
        }

        public void register() {
            registerReceiver(this, new IntentFilter(ZJPushReceiver.FILTER_JPUSH_RECEIVED));
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            uploadPushToken();
        }
    }

//    private long exitTime = 0l;
//
//    @Override
//    public void onBackPressed() {
//        if (mRegisterView.getVisibility() == View.VISIBLE) {
//            showLogin();
//        } else {
//            if (mFragmentManager.getBackStackEntryCount() > 0 ||
//                    Math.abs(exitTime - System.currentTimeMillis()) < 2000) {
//                super.onBackPressed();
//            } else {
//                toast("もう一回バックキー押すとアプリ終了します");
//                exitTime = System.currentTimeMillis();
//            }
//        }
//    }
    private long exitTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN){   
            if((System.currentTimeMillis()-exitTime) > 2000){  
                Toast.makeText(getApplicationContext(), "もう一回バックキー押すとアプリ終了します", Toast.LENGTH_SHORT).show();                                
                exitTime = System.currentTimeMillis();   
            } else {
                finish();
                System.exit(0);
            }
            return true;   
        }
        return super.onKeyDown(keyCode, event);
    }    

    public void moveToRegister() {
        AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
        alpha.setDuration(400);
        mRegisterView.startAnimation(alpha);
        mRegisterView.setVisibility(View.VISIBLE);
        mRegisterView.setInit(this, mAccountManager);
    }

    public void showLogin() {
        int count = mTabHost.getTabWidget().getChildCount();
        for (int i = 0; i < count; i++) {
            mTabHost.getTabWidget().getChildTabViewAt(i).setEnabled(false);
        }

        mLoginView.setVisibility(View.VISIBLE);
        mRegisterView.setVisibility(View.GONE);
    }

    public void dismissLogin() {
        mLoginView.setVisibility(View.GONE);
        mRegisterView.setVisibility(View.GONE);

        Utils.disableKeyBoard(this);
    }

    public void onLoginComplete() {
        int count = mTabHost.getTabWidget().getChildCount();
        for (int i = 0; i < count; i++) {
            mTabHost.getTabWidget().getChildTabViewAt(i).setEnabled(true);
        }

        mTabHost.setEnabled(true);
        sendBroadcast(new Intent(FILTER_LOGIN_COMPLETE));
        uploadPushToken();
    }

    private void uploadPushToken() {
        String uid = mAccountManager.user_id;
        String token = SharedPreferenceHelper.getString("jpush_regid");

        if (TextUtils.isEmpty(uid) || TextUtils.isEmpty(token)) return;

        mAccountManager.uploadPushToken(token, new Callback<Void>() {
            @Override
            protected void onSucceed(Void result) {
                dismissLoading();
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
    }
	public void relogin() {
        mAccountManager.clearLoginstatus();

        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    public void signInGoogleP() {
    	mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
    	ZLog.d("TAG", "[onConnectionFailed]: " + arg0.hasResolution());
    	if (!arg0.hasResolution()) {
    		//GooglePlayServicesUtil.getErrorDialog(arg0.getErrorCode(), this, 0).show();
    		ZLog.d("TAG", "[onConnectionFailed]: failed errorCode = " + arg0.getErrorCode());
    		return;
    	}

    	try {
    		arg0.startResolutionForResult(this, RC_SIGN_IN);
    	} catch (SendIntentException e) {
    		mGoogleApiClient.connect();
    	}
    }

	@Override
	public void onConnected(Bundle arg0) {
		ZLog.d("TAG", "[onConnected]: " + (arg0 != null ? arg0.toString() : "null"));
		final Context context = this.getApplicationContext();
		AsyncTask.execute(new Runnable() {
			@Override
			public void run() {
				String scope = "oauth2:" + Scopes.PLUS_LOGIN;
				try {
					// We can retrieve the token to check via
					// tokeninfo or to pass to a service-side
					// application.
					final String accountName = Plus.AccountApi.getAccountName(mGoogleApiClient);
					String displayName = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient).getDisplayName();
//					if (displayName == null || displayName.equals("")) {
//						displayName = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient).getNickname();
//					}
					final String unick = displayName;
					final String snsID = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient).getId();
					final String profile_image_url = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient).getImage().getUrl();
					ZLog.d("TAG", "[onConnected] accountName: " + accountName);
					ZLog.d("TAG", "[onConnected] unick: " + unick);
					
					final  String token = GoogleAuthUtil.getToken(context,
							accountName, scope);
					ZLog.d("TAG", "[onConnected] token: " + token);
					
					MainActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loginViaThirdParty(LOGIN_TYPE.GOOGLEP, unick, accountName, profile_image_url, snsID, token,"");
//							registerViaThirdParty(unick, accountName, LOGIN_TYPE.GOOGLEP, snsID, token,"");
						}
					});
					
				} catch (UserRecoverableAuthException e) {
					// This error is recoverable, so we could fix this
					// by displaying the intent to the user.
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (GoogleAuthException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		ZLog.d("TAG", "[onConnectionSuspended]: " + arg0);
	}
	
	@Override
	protected void onStart() {
		ZLog.d("TAG","onStart");
		super.onStart();
		TwitterHandler();
	}
	
	@Override
	protected void onStop() {
		ZLog.d("TAG","onStop");
		super.onStop();
		
		 if (mGoogleApiClient.isConnected()) {
             mGoogleApiClient.disconnect();
      }
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ZLog.d("TAG","onActivityResult");
		mCallbackManager.onActivityResult(requestCode, resultCode, data);

		// Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
		if (requestCode == RC_SIGN_IN) {
			ZLog.d("TAG", "[onActivityResult] [G+] sign-in, responseCode: " + resultCode);
			if (resultCode == RESULT_OK) {
				ZLog.d("TAG", "[onActivityResult] [G+] connecting");
				if (!mGoogleApiClient.isConnecting()) {
					mGoogleApiClient.connect();
				}
				return;
			} else {
				ZLog.d("TAG", "[onActivityResult] [G+] cancel");
			}
		}
	}
	
	public void registerViaThirdParty(String unick, String email, LOGIN_TYPE type, String snsID, String accessToken, String tokenSecret) {
		ZLog.d("TAG", "[registerViaThirdParty] unick=" + unick + ", email=" + email + ", type=" + type.id + ", snsID=" + snsID + ", accessToken=" + accessToken +" , tokenSecret="+tokenSecret);
//		showLoading();
		mAccountManager.registerViaThirdParty(unick, email, type, snsID, accessToken,tokenSecret, new Callback<LoginBean>() {
            @Override
            protected void onSucceed(LoginBean result) {
            	ZLog.d("TAG", "[registerViaThirdParty] succeed");
                dismissLoading();

                toast("ログイン成功しました");

                onLoginComplete();
                dismissLogin();
            }

            @Override
            protected void onError(Object object) {
            	ZLog.d("TAG", "[registerViaThirdParty] error");
                dismissLoading();
            }
        });
	}
	//[START]　SNSから情報を取得した後の処理
	public void loginViaThirdParty(final LOGIN_TYPE type, final String unick, final String email, final String profile_image_url, final String snsID, final String accessToken, final String tokenSecret) {
//		ZLog.d("TAG", "[loginViaThirdParty]" + ", type=" + type.id + ", unick=" + unick + ", email=" + email + ", profile_image_url=" + profile_image_url + ", snsID=" + snsID + ", accessToken=" + accessToken);
	
		showLoading();
		mAccountManager.loginViaThirdParty(type,email, snsID, accessToken,tokenSecret, new Callback<LoginBean>() {
            @Override
            protected void onSucceed(LoginBean result) {
            	ZLog.d("TAG", "[registerViaThirdParty] succeed");
                dismissLoading();

                toast("ログイン成功しました");

                onLoginComplete();
                dismissLogin();
            }

            @Override
            protected void onError(Object object) {
            	ZLog.d("TAG", "[registerViaThirdParty] error");
//                dismissLoading();
                registerViaThirdParty(unick, email, type, snsID, accessToken,tokenSecret);
            }
        });
	
	}
	public void authViaThirdParty(final boolean autoLogin,
			final LOGIN_TYPE type,
			final String nickname,
    		final String profile_image_url,
			final String email,
			final String password,
			final String twitter_id,
			final String twitter_token,
			final String twitter_token_secret,
			final String facebook_id,
			final String facebook_token,
			final String googleplus_id,
			final String googleplus_token) {
		ZLog.d("TAG", "[auth] " + 
				", autoLogin=" + autoLogin + 
				", type=" + type.id + 
				", email=" + email + 
				", password=" + password + 
				", twitter_id=" + twitter_id + 
				", twitter_token=" + twitter_token + 
				", twitter_token_secret=" + twitter_token_secret + 
				", facebook_id=" + facebook_id + 
				", facebook_token=" + facebook_token + 
				", googleplus_id=" + googleplus_id + 
				", googleplus_token=" + googleplus_token 
				);
		showLoading();
		mAccountManager.auth(autoLogin, type, email, password, twitter_id, twitter_token, twitter_token_secret, facebook_id, facebook_token, googleplus_id, googleplus_token, new Callback<LoginBean>() {
            @Override
            protected void onSucceed(LoginBean result) {
            	ZLog.d("TAG", "[auth] succeed");
                dismissLoading();

                toast("ログイン成功しました");

                onLoginComplete();
                dismissLogin();
            }

            @Override
            protected void onError(Object object) {
            	ZLog.d("TAG", "[auth] error");
                dismissLoading();
                //getsnsToken
                int errorCode = Integer.parseInt((String)object);
                if (errorCode <= 0) {
                	return;
                }
                String snsId = "";
                if (type.equals(LOGIN_TYPE.EMAIL)) {
					
				} else if (type.equals(LOGIN_TYPE.TWITTER)) {
					snsId = twitter_id;
				} else if (type.equals(LOGIN_TYPE.FACEBOOK)) {
					snsId = facebook_id;
				} else if (type.equals(LOGIN_TYPE.GOOGLEP)) {
					snsId = googleplus_id;
				}
                showLoading();
                mAccountManager.getSnsUser(type, snsId, new Callback<Void>(){
					@Override
					protected void onSucceed(Void result) {
						ZLog.d("TAG", "[getSnsToken] succeed");
						dismissLoading();
						//update_sns_token
						String user_id = "";
						showLoading();
						mAccountManager.updateSnsToken(type, user_id, nickname, profile_image_url, email, twitter_id, twitter_token, twitter_token_secret, facebook_id, facebook_token, googleplus_id, googleplus_token, new Callback<Void>() {

							@Override
							protected void onSucceed(Void result) {
				            	ZLog.d("TAG", "[updateSnsToken] succeed");
				                dismissLoading();

				                toast("ログイン成功しました");

				                onLoginComplete();
				                dismissLogin();
							}

							@Override
							protected void onError(Object object) {
								ZLog.d("TAG", "[updateSnsToken] error");
								dismissLoading();
							}
						});
					}

					@Override
					protected void onError(Object object) {
						ZLog.d("TAG", "[getSnsToken] error");
						dismissLoading();
						//createUser
						int errorCode = Integer.parseInt((String)object);
		                if (errorCode <= 0) {
		                	return;
		                }
						createUser(type, nickname, profile_image_url, email, password, twitter_id, twitter_token, twitter_token_secret, facebook_id, facebook_token, googleplus_id, googleplus_token);
					}
                	
                });
            }
        });
	}
	
	public void createUser(LOGIN_TYPE type,
    		String nickname,
    		String profile_image_url,
    		String email,
    		String password,
    		String twitter_id,
    		String twitter_token,
    		String twitter_token_secret,
    		String facebook_id,
    		String facebook_token,
    		String googleplus_id,
    		String googleplus_token) {
		ZLog.d("TAG", "[createUser] " + 
				", type=" + type.id + 
				"nickname=" + nickname + 
				", profile_image_url=" + profile_image_url + 
				", password=" + password + 
				", twitter_id=" + twitter_id + 
				", twitter_token=" + twitter_token + 
				", twitter_token_secret=" + twitter_token_secret + 
				", facebook_id=" + facebook_id + 
				", facebook_token=" + facebook_token + 
				", googleplus_id=" + googleplus_id + 
				", googleplus_token=" + googleplus_token 
				);
		showLoading();
		mAccountManager.createUser(type, nickname, profile_image_url, email, password, twitter_id, twitter_token, twitter_token_secret, facebook_id, facebook_token, googleplus_id, googleplus_token, new Callback<LoginBean>() {
            @Override
            protected void onSucceed(LoginBean result) {
            	ZLog.d("TAG", "[createUser] succeed");
                dismissLoading();

                toast("ログイン成功しました");

                onLoginComplete();
                dismissLogin();
            }

            @Override
            protected void onError(Object object) {
            	ZLog.d("TAG", "[createUser] error");
                dismissLoading();
            }
        });
	}
	
	//[START] Facebook
	private void getFBHashKey() {
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"jp.concierge.jetkys.android", 
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				ZLog.d("TAG", "HashKey: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public void signinFacebook() {
		LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
	}
    //[END] Facebook

    //[START] Twitter
    public void signinTwitter() {
    	ConfigurationBuilder builder = new ConfigurationBuilder();
    	builder.setDebugEnabled(true);
    	builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
    	builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
    	Configuration configuration = builder.build();

    	TwitterFactory factory = new TwitterFactory(configuration);
    	twitter = factory.getInstance();
    	new Thread(new Runnable() {
			@Override
			public void run() {
		    	try {
		    		requestToken = twitter
		    				.getOAuthRequestToken(TWITTER_CALLBACK_URL);

		    		MainActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri
		    				.parse(requestToken.getAuthenticationURL())));
		    	} catch (TwitterException e) {
		    		e.printStackTrace();
		    	}
			}
		}).start();
    }
    //[END] Twitter


}
