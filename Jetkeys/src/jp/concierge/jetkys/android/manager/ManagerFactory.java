package jp.concierge.jetkys.android.manager;

import android.content.Context;
import jp.concierge.jetkys.android.utils.ZLog;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * 所有模块Manger的工厂类
 *
 * @author huangyue 2013-10-18
 */
public class ManagerFactory {
    private static ManagerFactory mManagerFactory;
    private static Hashtable<String, BaseManager> mMagHash;

    private ManagerFactory() {
        mMagHash = new Hashtable<String, BaseManager>();
    }

    public static ManagerFactory getInstance() {
        if (mManagerFactory == null) {
            mManagerFactory = new ManagerFactory();
        }
        return mManagerFactory;
    }

    public void destroy() {
        Iterator<BaseManager> itr = mMagHash.values().iterator();
        while (itr.hasNext()) {
            itr.next().onDestroy();
        }
    }

    public BaseManager getManager(Context context, Class<? extends BaseManager> ownerClass) {
        if (ownerClass == null) {
            return null;
        }

        if (!mMagHash.containsKey(ownerClass.getName())) {
            try {
                Constructor<?> managerConst = ownerClass.getDeclaredConstructor();
                managerConst.setAccessible(true);

                BaseManager bm = (BaseManager) managerConst.newInstance();
                Field field = BaseManager.class.getDeclaredField("mContext");
                field.setAccessible(true);
                field.set(bm, context);
                mMagHash.put(ownerClass.getName(), bm);
                bm.onCreate(context);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mMagHash.get(ownerClass.getName());
    }
}
