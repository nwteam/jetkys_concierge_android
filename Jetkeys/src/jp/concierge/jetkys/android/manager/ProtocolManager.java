package jp.concierge.jetkys.android.manager;

import android.content.Context;
import android.os.Handler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jp.concierge.jetkys.android.beans.BaseJsonEntity;
import jp.concierge.jetkys.android.beans.UploadPhotoBean;
import jp.concierge.jetkys.android.gameutils.encrypt.EncryptData;
import jp.concierge.jetkys.android.net.ProtocolConstant;
import jp.concierge.jetkys.android.net.encrypt.NetConst;
import jp.concierge.jetkys.android.net.encrypt.NetParams;
import jp.concierge.jetkys.android.net.http.ActionListener;
import jp.concierge.jetkys.android.net.http.HttpAction;
import jp.concierge.jetkys.android.net.http.client.HttpClientRequest;
import jp.concierge.jetkys.android.net.http.client.listener.MHttpResponse;
import jp.concierge.jetkys.android.net.thread.Executable;
import jp.concierge.jetkys.android.net.thread.ThreadHelper;
import jp.concierge.jetkys.android.utils.CryptUtil;
import jp.concierge.jetkys.android.utils.ZLog;
import org.json.JSONObject;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class ProtocolManager extends BaseManager implements NetParams {
    public static final String TAG = ProtocolManager.class.getSimpleName();

    private Handler mHandler;
    private Gson gson;
    private HttpClientRequest mClient;

    private Context mContext;

    private AccountManager mAccountManager;

    @Override
    public void onCreate(Context context) {
        mContext = context.getApplicationContext();
        mHandler = new Handler();
        mClient = HttpClientRequest.getInstance(mContext);

        mAccountManager = getManager(context, AccountManager.class);

        GsonBuilder gb = new GsonBuilder();
        gb.generateNonExecutableJson();
        gson = gb.create();
    }

    /**
     * map 转 Object
     *
     * @param cls
     * @return
     */
    public static Object json2Object(Map<String, Object> item, Class<?> cls) {
        GsonBuilder gb = new GsonBuilder();
        gb.generateNonExecutableJson();
        Gson gson = gb.create();
        JSONObject json = new JSONObject(item);
        return gson.fromJson(json.toString(), cls);
    }

    public static Object json2Object(String jsonStr, Class<?> cls) {
        GsonBuilder gb = new GsonBuilder();
        gb.generateNonExecutableJson();
        Gson gson = gb.create();
        return gson.fromJson(jsonStr, cls);
    }

    private void onSuccess(HttpAction action, String jsonString) {
        ActionListener<Object> liseter = (ActionListener<Object>) action.getActionListener();
        if (liseter == null) {
            return;
        }
        Type[] types = ((ParameterizedType) liseter
                .getClass()
                .getGenericSuperclass())
                .getActualTypeArguments();
        if (types == null) {
            ZLog.e(TAG, ProtocolConstant.ERR_RETURN_DATA_BAD);
            liseter.onFailure("-2", ProtocolConstant.ERR_RETURN_DATA_BAD);
        } else {
            try {
                BaseJsonEntity entity = new BaseJsonEntity();
                entity.parseJson(gson, jsonString, types[0]);
                if (entity.isSucess()) {
                    liseter.onSuccess(entity.data);
                } else {
                    liseter.onFailure(entity.errCode, entity.message);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (liseter != null) {
                    liseter.onFailure("-3", ProtocolConstant.ERR_RETURN_DATA_BAD);
                }
            }
        }
    }

    public void submitWithDid(final HttpAction action) {
        String IVStr = EncryptData.getIV();
        String dataTime = CryptUtil.getDateTime();
        String DIDStr = CryptUtil.getDID(dataTime);
        String tokenDID = CryptUtil.getDIDToken(DIDStr, dataTime);

        HashMap<String, String> map = action.getParams();
        map.put(DID, EncryptData.encrypt(DIDStr, IVStr));
        map.put(IV, IVStr);
        map.put(TOKEN, EncryptData.encrypt(tokenDID, IVStr));
        map.put(DATETIME, dataTime);
        map.put(REQUEST_NO, NetConst.REQUEST_NO_CODE);
        map.put(DEVICE_TYPE, NetConst.DEVICE_TYPE_CODE);

        String data = action.getAssembledJsonStr();
        if (data != null) {
            map.put(DATA, data);
//            map.put(DATA, EncryptData.encrypt(data, IVStr));
        }

        ThreadHelper.executeInQThreadPool(new Executable<Object>() {

            @Override
            public Object execute() throws Exception {
                synSubmit(action);
                return null;
            }

        }, null);
    }

    public void submitWithUid(final HttpAction action) {
        String IVStr = EncryptData.getIV();
        String dataTime = CryptUtil.getDateTime();
//        String idStr = CryptUtil.getID(mAccountManager.user_id, dataTime);
//        String tokenDID = CryptUtil.getUseridToken(
//                mAccountManager.install_key, idStr, dataTime);
        String tokenDID = CryptUtil.getUseridtoken2(
                mAccountManager.install_key,
                mAccountManager.user_id, dataTime);

        HashMap<String, String> map = action.getParams();
        map.put(IV, IVStr);
        map.put(TOKEN, EncryptData.encrypt(tokenDID, IVStr));
        map.put(DATETIME, dataTime);
        map.put(REQUEST_NO, NetConst.REQUEST_NO_CODE);
        map.put(DEVICE_TYPE, NetConst.DEVICE_TYPE_CODE);
        map.put(ID, EncryptData.encrypt(mAccountManager.user_id, IVStr));
//        map.put(ID, mAccountManager.user_id);

        String data = action.getAssembledJsonStr();
        if (data != null) {
//            map.put(DATA, EncryptData.encrypt(data, IVStr));
            map.put(DATA, data);
        }

        ThreadHelper.executeInQThreadPool(new Executable<Object>() {

            @Override
            public Object execute() throws Exception {
                synSubmit(action);
                return null;
            }

        }, null);
    }

    private void synSubmit(final HttpAction action) {
        try {
            mClient.submit(action,
                    new MHttpResponse() {

                        @Override
                        public void success(final String jsonString) {
                            mHandler.post(new Runnable() {

                                @Override
                                public void run() {
                                    onSuccess(action, jsonString);
                                }
                            });
                        }

                        @Override
                        public void failed(final String errCode,
                                           final String errMsg) {
                            mHandler.post(new Runnable() {

                                @Override
                                public void run() {
                                    onFailed(action, errCode, errMsg);
                                }
                            });
                        }
                    });
        } catch (Exception e) {
        }
    }

    private void onFailed(HttpAction action, String errCode, String errMsg) {
        ActionListener<?> liseter = action.getActionListener();
        if (liseter != null) {
            liseter.onFailure(errCode, errMsg);
        }
    }

    @Override
    public void onDestroy() {
        mClient = null;
    }
}
