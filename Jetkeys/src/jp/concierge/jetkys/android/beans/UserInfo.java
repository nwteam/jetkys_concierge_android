package jp.concierge.jetkys.android.beans;

import java.io.Serializable;

/**
 * Created by WilliZ on 16/03/31.
 */
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 8915963496738270715L;

    public String nickname;
    public String pt;
    public String profile_image_url;
    public String if_new;
    public String diagnosis_ticket;
    public String chat_ticket;
    public String mail_ticket;
    public String image_ticket;
    public String gender;
}
