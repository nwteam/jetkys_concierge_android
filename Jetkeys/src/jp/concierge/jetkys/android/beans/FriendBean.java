package jp.concierge.jetkys.android.beans;

/**
 * Created by WilliZ on 16/03/25.
 */
public class FriendBean {
    public String id;
    public String lname;
    public String fname;
    public String lname_kana;
    public String fname_kana;
    public String gender;
    public String image;
    public String birthday;
    public String if_mail;
    public String if_message;
    public String nickname;

    public String getName() {
//        return lname + fname;
    	return nickname;
    }
    public String getGender() {
    	return gender;
    }
}
