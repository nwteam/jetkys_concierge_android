package jp.concierge.jetkys.android.beans;

/**
 * Created by WilliZ on 16/03/30.
 */
public class Member {
    public String id;
    public float balance;
    public String pushToken;
    public String nick;
    public String email;
}
