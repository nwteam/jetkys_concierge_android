package jp.concierge.jetkys.android.beans.base;

import jp.concierge.jetkys.android.beans.FriendBean;

import java.util.List;

/**
 * Created by WilliZ on 16/03/30.
 */
public class BaseFriendListEntity {
    public List<FriendBean> concierges;
    public List<FriendBean> friend;
}
