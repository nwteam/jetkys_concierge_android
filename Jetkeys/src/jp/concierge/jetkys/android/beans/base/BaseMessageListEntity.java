package jp.concierge.jetkys.android.beans.base;

import jp.concierge.jetkys.android.beans.Concierge;
import jp.concierge.jetkys.android.beans.Message;

import java.util.List;

/**
 * Created by WilliZ on 16/03/30.
 */
public class BaseMessageListEntity {
    public String conversation_id;
    public List<Message> list;
}
