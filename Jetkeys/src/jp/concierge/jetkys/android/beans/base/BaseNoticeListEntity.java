package jp.concierge.jetkys.android.beans.base;

import jp.concierge.jetkys.android.beans.SysMessage;

import java.util.List;

/**
 * Created by WilliZ on 16/03/30.
 */
public class BaseNoticeListEntity {
    public List<SysMessage> notice_list;
}
