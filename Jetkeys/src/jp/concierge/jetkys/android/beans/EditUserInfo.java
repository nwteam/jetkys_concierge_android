package jp.concierge.jetkys.android.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by WilliZ on 16/03/31.
 */
public class EditUserInfo implements Serializable {

    private static final long serialVersionUID = 8915963496738270715L;

    public String nickname;
    public String birthday;
    public String gender;
    public String blood;
    public String category_name;
}
