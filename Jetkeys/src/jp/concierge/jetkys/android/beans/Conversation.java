package jp.concierge.jetkys.android.beans;

/**
 * Created by WilliZ on 16/03/25.
 */
public class Conversation {
    public String id;
    public String nickname;
    public String image;
    public String message;
    public String msg_type;
    public String diagnosis_id;
    public String concierge_id;
    public String user_id;
    public String if_msg;
    public String concierge_gender;
}
