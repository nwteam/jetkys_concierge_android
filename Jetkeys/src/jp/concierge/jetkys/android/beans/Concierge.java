package jp.concierge.jetkys.android.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by WilliZ on 16/03/19.
 */
public class Concierge implements Serializable {
    private static final long serialVersionUID = 1704578364183537726L;

    public String id;
    public String lname;
    public String fname;
    public String lname_kana;
    public String fname_kana;
    public String gender;
    public String image;
    public String capacity;
    public String self_introduction;
    public String chat_point;
    public String mail_point;
    public String point;
    public String email;
    public String tel;
    public String age;
    public List<String> concierges_categories;
    public String nickname;

    public String getName() {
//        return lname + "" + fname;
    	return nickname;
    }
    public String getGender() {
    	return gender;
    }
}
