package jp.concierge.jetkys.android.beans;

import java.io.Serializable;

/**
 * Created by WilliZ on 16/03/31.
 */
public class Diagnosis implements Serializable {
    private static final long serialVersionUID = 521889425921919629L;

    public String id;
    public String concierge_id;
    public String title;
    public String message;
    public String recommend_flag;
    public String point;
}
