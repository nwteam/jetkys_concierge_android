package jp.concierge.jetkys.android.beans;

/**
 * Created by WilliZ on 16/03/29.
 */
public class LoginBean {
    public String id;
    public String install_key;
    public String concierge_id;
    public String nickname;
    public String profile_image_url;
    public String setting_mail;
    public String setting_message;
    public String setting_image;
    public String diagnosis_ticket;
    public String chat_ticket;
    public String mail_ticket;
    public String image_ticket;
}
