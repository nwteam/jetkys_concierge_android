package jp.concierge.jetkys.android.beans;

import com.google.gson.Gson;
import jp.concierge.jetkys.android.gameutils.encrypt.EncryptData;
import jp.concierge.jetkys.android.net.encrypt.NetConst;
import jp.concierge.jetkys.android.net.encrypt.NetParams;
import jp.concierge.jetkys.android.utils.ZLog;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;

/**
 * Created by zhuwenjin on 15/9/18.
 */
public class BaseJsonEntity implements Serializable, NetParams {

    public static class OutterParseException extends IllegalArgumentException {
        public OutterParseException(String json) {
            super("OUTTER DECRYPT ERROR. Json=\n" + json);
        }
    }

    private static final long serialVersionUID = 1L;

    private JSONObject errJson;

    /**
     * 返回结果
     */
    public boolean result;

    /**
     * 返回信息
     */
    public String message;
    /**
     * 返回信息
     */
    public String errCode;

    /**
     * 数据
     */
    public Object data;

    public BaseJsonEntity() {
        errJson = null;
    }

    public void parseJson(Gson gs, String jsonStr, Type type) throws Exception {
        String dataStr;
        try {
            dataStr = fuckingOutterParse(jsonStr);
        } catch (Exception e) {
            throw new OutterParseException(jsonStr);
        }

        if ("0".equals(dataStr)) {
            result = false;
            return;
        }

         //dataStr = dataStr.replaceAll("\\\\", "");
        ZLog.d("JSONENTITY", "RealJson:\n" + dataStr);
        JSONObject retJson = new JSONObject(dataStr);
        result = retJson.getBoolean("result");
        if (result) { // logical success
            String realDataStr = retJson.getString("data");
            data = gs.fromJson(realDataStr, type);
        }
    }

    private String fuckingOutterParse(String jsonStr) throws Exception {
        JSONObject json = new JSONObject(jsonStr);

        try {
            errJson = json.getJSONObject("error");
        } catch (JSONException e) {
            // do nothing
        }

        if (errJson == null) {
            //decrypt "data" by "iv"
            String ivStr = json.getString("iv");
            String dataStr = json.getString("data");
            String realDataStr = getRealData(dataStr, ivStr);

            // get real result, separate from key: method
            String methodStr = json.getString("method");
            JSONObject resultJson = new JSONObject(realDataStr);
            return resultJson.getString(methodStr);
        } else {
            errCode = errJson.optString("code");
            message = errJson.optString("message");
            return "0";
        }
    }

    public String getRealData(String encryptedData, String iv) {
//        return EncryptData.decrypt(encryptedData, iv);
        return encryptedData;
    }

    public boolean isSucess() {
        return result;
    }
}
