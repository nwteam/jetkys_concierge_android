package jp.concierge.jetkys.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreferenceHelper {
	public static final String DATAS = "datas";

	private static SharedPreferences sp;

	public static void init(Context context) {
        sp = context.getSharedPreferences(DATAS, Context.MODE_PRIVATE);
    }

	public static boolean getBoolean(String key, boolean defValue) {
		return sp.getBoolean(key, defValue);
	}

	public static String getString(String key) {
		return getString(key, "");
	}

	public static String getString(String key, String defValue) {
		return sp.getString(key, defValue);
	}

	public static void putString(String key, String value) {
		sp.edit().putString(key, value).apply();
	}

	public static Editor getEditor() {
		return sp.edit();
	}

    public static void clear() {
        sp.edit().clear().apply();
    }
}