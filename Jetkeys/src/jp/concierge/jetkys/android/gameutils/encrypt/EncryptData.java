package jp.concierge.jetkys.android.gameutils.encrypt;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.content.Context;
import android.provider.Settings.Secure;
import jp.concierge.jetkys.android.utils.ZLog;

public class EncryptData {
	// private String message;
	public static Context context;
	private String keyString; // = "0123456789ABCDEF";
	final protected static char[] hexArray = { '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	// private byte[] encryptedMessage;
	// private byte[] iv;
	private static final String TAG = "EncryptData";

	// public EncryptData(String key) {
	// keyString = md5(key);
	// ZLog.d(TAG, "KeyString: " + keyString);
	// }

	public ArrayList<byte[]> encrypt(String message) {

		ArrayList<byte[]> ret = new ArrayList<byte[]>();

		// String keyString = "0123456789ABCDEF"; // 16byte = 128bit
		SecretKeySpec key = new SecretKeySpec(keyString.getBytes(), "AES");

		Cipher c;
		try {
			c = Cipher.getInstance("AES/CBC/PKCS5Padding");
			c.init(Cipher.ENCRYPT_MODE, key);
			byte[] encryptedMessage = c.doFinal(message.getBytes());
			byte[] iv = c.getIV();
			ret.add(encryptedMessage);
			ret.add(iv);
			//ZLog.d(TAG, "message: " + message);
			//ZLog.d(TAG, "encrypted: " + byteArrayToHexString(encryptedMessage));
			//ZLog.d(TAG, "IV: " + byteArrayToHexString(iv));

			byte[] decryptedMess = decrypt(encryptedMessage, iv);
			//ZLog.d(TAG, "decrypt: " + byteArrayToHexString(decryptedMess));

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}

		return ret;
	}

	void setDecryptData(byte[] encryptedMessage, byte[] iv) {
		// this.encryptedMessage = encryptedMessage;
		// this.iv = iv;
	}

	byte[] decrypt(byte[] encryptedMessage, byte[] iv) {

		byte[] ret = null;

		// String keyString = "0123456789ABCDEF"; // 16byte = 128bit
		SecretKeySpec key = new SecretKeySpec(keyString.getBytes(), "AES");

		try {
			Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec ips = new IvParameterSpec(iv);
			c.init(Cipher.DECRYPT_MODE, key, ips);
			byte[] decryptedMessage = c.doFinal(encryptedMessage);
			ret = decryptedMessage;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}

		return ret;
	}

	public static String md5(String s) {
		  byte[] hash;
		    try {
		        hash = MessageDigest.getInstance("MD5").digest(s.getBytes("UTF-8"));
		    } catch (NoSuchAlgorithmException e) {
		        throw new RuntimeException("Huh, MD5 should be supported?", e);
		    } catch (UnsupportedEncodingException e) {
		        throw new RuntimeException("Huh, UTF-8 should be supported?", e);
		    }

		    StringBuilder hex = new StringBuilder(hash.length * 2);
		    for (byte b : hash) {
		        if ((b & 0xFF) < 0x10) hex.append("0");
		        hex.append(Integer.toHexString(b & 0xFF));
		    }
		    return hex.toString();
	}

	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];

		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
					.digit(s.charAt(i + 1), 16));
		}

		return data;
	}

	public static String byteArrayToHexString(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		int v;

		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}

		return new String(hexChars);
	}

	public static String getIV() {
		SecureRandom random = new SecureRandom();
		byte[] iv = random.generateSeed(16);
		return byteArrayToHexString(iv);
		/*String keyStr =  md5("irRnvYe8yi1q");
		ZLog.e(TAG, "keyStr: " + keyStr+" keyStr="+keyStr.length());
		SecretKeySpec key = new SecretKeySpec(keyStr.getBytes(), "AES");
		Cipher c;
		try {
			c = Cipher.getInstance("AES/CBC/PKCS5Padding");
			c.init(Cipher.ENCRYPT_MODE, key);
			byte[] iv = c.getIV();
			String ivStr = byteArrayToHexString(iv);
			ZLog.e(TAG, "iv: " + ivStr);
			return ivStr;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";*/
	}

	public static String getUUID() {
		return Installation.id(context);
	}

	public static String encrypt(String message, String iv) {
		//ZLog.d(TAG, "encrypt");
		//ZLog.d(TAG, "message: " + message);
		//ZLog.d(TAG, "IV: " + iv);
		String keyString = md5("irRnvYe8yi1q"); // 16byte = 128bit //At5CcmaxnRmE
		SecretKeySpec key = new SecretKeySpec(keyString.getBytes(), "AES");

		Cipher c;
		try {
			c = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec ips = new IvParameterSpec(hexStringToByteArray(iv));
			c.init(Cipher.ENCRYPT_MODE, key, ips);
			byte[] encryptedMessage = c.doFinal(message.getBytes());

			String str = byteArrayToHexString(encryptedMessage);
			
			//ZLog.d(TAG, "encrypted: " + str);
			//decrypt(str, iv);
			return str;

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public static String decrypt(String encryptedMessage, String iv) {
		//ZLog.d("Encrypt", "decrypt");
		//ZLog.d(TAG, "message: " + encryptedMessage);
		//ZLog.d(TAG, "IV: " + iv);
		String keyString = md5("irRnvYe8yi1q"); // 16byte = 128bit //At5CcmaxnRmE
		SecretKeySpec key = new SecretKeySpec(keyString.getBytes(), "AES");

		Cipher c;
		try {
			c = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec ips = new IvParameterSpec(hexStringToByteArray(iv));
			c.init(Cipher.DECRYPT_MODE, key, ips);
			byte[] decryptedMessage = c
					.doFinal(hexStringToByteArray(encryptedMessage));

			String str = new String(decryptedMessage, "UTF-8");

			ZLog.e(TAG, "decrypted: " + str);

			return str;

			// byte[] decryptedMess = decrypt(encryptedMessage, iv);
			// ZLog.d(TAG, "decrypt: " + byteArrayToHexString(decryptedMess));

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public static String sha256(String message) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(message.getBytes());
			return bytesToHex(md.digest());
		} catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
			ZLog.e("Failed to encrypt password.", e.toString());
		}
		return "";
	}
	
	public static String bytesToHex(byte[] bytes) {
	    StringBuffer result = new StringBuffer();
	    for (byte byt : bytes) result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
	    return result.toString();
	}
	
	
}
