package jp.concierge.jetkys.android.gameutils.log;

import android.util.Log;

public class JLog {
	private static boolean enable = true; //todo false

	public static void enableLog(boolean aEnable){
		enable = aEnable;
	}
	public static void d(String tag, String msg) {
		if (enable)
			Log.d(tag, msg);
	}

	public static void e(String tag, String msg) {
		if (enable)
			Log.e(tag, msg);
	}

	public static void e(String tag, String msg, Throwable e) {
		if (enable)
			Log.e(tag, msg, e);
	}

	public static void i(String tag, String msg) {
		if (enable)
			Log.i(tag, msg);
	}

	public static void w(String tag, String msg, Throwable tr) {
		if (enable)
			Log.w(tag, msg, tr);
	}
	
	public static void w(String tag, String msg) {
		if (enable)
			Log.w(tag, msg);
	}
	
	public static void v(String tag, String msg) {
		if (enable)
			Log.v(tag, msg);
	}

}
