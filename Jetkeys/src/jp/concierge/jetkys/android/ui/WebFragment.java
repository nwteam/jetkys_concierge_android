package jp.concierge.jetkys.android.ui;

import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import butterknife.Bind;
import jp.concierge.jetkys.android.R;

/**
 * Created by WilliZ on 16/03/21.
 */
public class WebFragment extends BaseFragment {
    public static final String URL = "EXTRAS.url";
    public static final String TITLE = "EXTRAS.title";

    @Override
    protected int layout() {
        return R.layout.fragment_webview;
    }

    @Bind(R.id.webview)
    WebView webView;

    private String mTitle, mUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }

        mTitle = bundle.getString(TITLE);
        mUrl = bundle.getString(URL);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        needBackFinish();
        setTitle(mTitle);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webView.loadUrl(mUrl);
//        webView.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
////                super.onReceivedSslError(view, handler, error);
//                handler.proceed();
//            }
//        });
    }

    public static void start(BaseFragment f, String title, String url) {
        Bundle b = new Bundle();
        b.putString(TITLE, title);
        b.putString(URL, url);

        WebFragment fragment = new WebFragment();
        fragment.setArguments(b);
        f.startFragment(fragment);
    }
}