package jp.concierge.jetkys.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.OnClick;
import jp.concierge.jetkys.android.PicBrowseActivity;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.MailBean;
import jp.concierge.jetkys.android.beans.base.BaseMailListEntity;
import jp.concierge.jetkys.android.manager.NetManager;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.utils.PhotoPicker;
import jp.concierge.jetkys.android.utils.PhotoUtils;

import java.util.Calendar;

/**
 * Created by WilliZ on 16/03/20.
 */
public class MailDetailFragment extends BaseFragment {

    @Override
    protected int layout() {
        return R.layout.fragment_maildetail;
    }

    @Bind(R.id.tv_title)
    TextView mTitleTv;
    @Bind(R.id.tv_time)
    TextView mTimeTv;
    @Bind(R.id.tv_content)
    TextView mContentTv;

    @Bind(R.id.iv_photo)
    ImageView mPhotoIv;

    @OnClick(R.id.iv_photo)
    void onPhotoClick() {
        if (mMailBean == null) return;

        String imgUrl = mMailBean.image_url;

        Intent intent = new Intent();
        intent.setClass(mActivity, PicBrowseActivity.class);
        intent.putExtra("url", PhotoUtils.getPhotoUrl(imgUrl));
        startActivity(intent);
    }

    @Bind(R.id.btn_submit)
    View mSubmitView;

    @OnClick(R.id.btn_submit)
    void submitClick() {
        if (mMailBean == null) return;

        Bundle bundle = new Bundle();
        bundle.putBoolean("reply", true);
        bundle.putSerializable("mail", mMailBean);

        MailEditFragment fragment = new MailEditFragment();
        fragment.setArguments(bundle);
        startFragment(fragment);
    }

    private String mailId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mailId = getArguments().getString("id");
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        needBackFinish();

        setDatas();
    }

    private MailBean mMailBean;

    private void setDatas() {
        if (mMailBean == null) return;

        mTitleTv.setText(mMailBean.title);
        mTimeTv.setText(mMailBean.send_ts);
        mContentTv.setText(mMailBean.message);

        if ("1".equals(mMailBean.attachment)) {
            mPhotoIv.setVisibility(View.VISIBLE);
            asyncLoadImage(mMailBean.image_url, mPhotoIv);
        }

        if (mAccountManager.user_id.equals(mMailBean.from_id)) {
            mSubmitView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();
        mNetManager.getMailDetail(mailId, new Callback<MailBean>() {
            @Override
            protected void onSucceed(MailBean result) {
                dismissLoading();
                mMailBean = result;
                setDatas();
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }
}
