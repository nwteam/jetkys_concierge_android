package jp.concierge.jetkys.android.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.OnClick;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.MailBean;
import jp.concierge.jetkys.android.ui.utils.DisplayUtil;
import jp.concierge.jetkys.android.ui.utils.PhotoPicker;
import jp.concierge.jetkys.android.utils.Utils;

import java.util.Calendar;

/**
 * Created by WilliZ on 16/03/20.
 */
public class MailEditFragment extends BaseFragment {

    @Override
    protected int layout() {
        return R.layout.fragment_mail_edit;
    }

    @OnClick(R.id.layout_pick_year)
    void pickYear() {
        ViewPicker.showPickYear(mActivity, new ViewPicker.OnItemClickListener() {
            @Override
            public void onItemClick(int index, String value) {
                if (!value.equals(mYearTv.getText().toString())) {
                    mDateTv.setText("1");
                }

                mYearTv.setText(value);
            }
        });
    }

    @OnClick(R.id.layout_pick_month)
    void pickMonth() {
        ViewPicker.showPickMonth(mActivity, new ViewPicker.OnItemClickListener() {
            @Override
            public void onItemClick(int index, String value) {
                if (!value.equals(mMonthTv.getText().toString())) {
                    mDateTv.setText("1");
                }

                mMonthTv.setText(value);
            }
        });
    }

    @OnClick(R.id.layout_pick_date)
    void pickDate() {
        int year = Integer.valueOf(mYearTv.getText().toString());
        int month = Integer.valueOf(mMonthTv.getText().toString());

        ViewPicker.showPickDate(mActivity, year, month, new ViewPicker.OnItemClickListener() {
            @Override
            public void onItemClick(int index, String value) {
                mDateTv.setText(value);
            }
        });
    }

    @OnClick(R.id.layout_pick_blood)
    void pickBlood() {
        ViewPicker.showPickBlood(mActivity, new ViewPicker.OnItemClickListener() {
            @Override
            public void onItemClick(int index, String value) {
                mBloodTv.setText(value);
            }
        });
    }

    @Bind(R.id.tv_year)
    TextView mYearTv;
    @Bind(R.id.tv_month)
    TextView mMonthTv;
    @Bind(R.id.tv_date)
    TextView mDateTv;
    @Bind(R.id.tv_blood)
    TextView mBloodTv;

    @Bind(R.id.layout_mail_birth)
    View picLayout;

    private void initAllPick() {
        Calendar calendar = Calendar.getInstance();
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH) + 1;
        int d = calendar.get(Calendar.DATE);

        mYearTv.setText("" + y);
        mMonthTv.setText("" + m);
        mDateTv.setText("" + d);

        mBloodTv.setText("A");
    }

    private boolean isReply;

    @Bind(R.id.et_mail_content)
    EditText mContentEt;

    @Bind(R.id.et_mail_title)
    EditText mTitleEt;

    @OnClick(R.id.btn_submit)
    void submitClick() {
        String title = mTitleEt.getText().toString();
        String content = mContentEt.getText().toString();

        mMailBean.title = title;
        mMailBean.message = content;
        mMailBean.image_url = mPhotoPath;

        Bundle bundle = new Bundle();
        bundle.putSerializable("mail", mMailBean);

        MailPrevFragment fragment = new MailPrevFragment();
        fragment.setArguments(bundle);
        startFragment(fragment);
    }

    private MailBean mMailBean;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPhotoPicker = new PhotoPicker(this, mActivity);

        Bundle bundle = getArguments();
        isReply = bundle.getBoolean("reply", false);

        mMailBean = (MailBean) bundle.getSerializable("mail");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Utils.disableKeyBoard(mActivity);
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        needBackFinish();

        if (isReply) {
            picLayout.setVisibility(View.GONE);

            ViewGroup.LayoutParams params = mContentEt.getLayoutParams();
            params.height = DisplayUtil.dp2px(mActivity, 300);
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            mContentEt.setLayoutParams(params);
        }

        initAllPick();
    }

    private String mPhotoPath;

    private PhotoPicker mPhotoPicker;

    @OnClick(R.id.camera)
    void sendPhoto() {
        mPhotoPicker.startPick();
    }

    private void photoPicked(String path) {
        mPhotoPath = path;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String photoPath = mPhotoPicker.onActivityResult(requestCode, resultCode, data);
        if (!TextUtils.isEmpty(photoPath)) { // handled
            photoPicked(photoPath);
            return;
        }
    }
}
