package jp.concierge.jetkys.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Bind;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.SysMessage;
import jp.concierge.jetkys.android.beans.base.BaseNoticeListEntity;
import jp.concierge.jetkys.android.manager.NetManager;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.views.PullListView;
import jp.concierge.jetkys.android.utils.TimeUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WilliZ on 16/03/21.
 */
public class SysmsglistFragment extends BaseFragment {

    @Override
    protected int layout() {
        return R.layout.fragment_sysmsglist;
    }

    @Bind(R.id.pulllistview)
    PullListView mListView;
    private ListAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new ListAdapter();
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        needBackFinish();
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - mListView.getRefreshableView().getHeaderViewsCount();

                SysMessage msg = mAdapter.getItem(pos);
                msg.status = "1";
                mAdapter.notifyDataSetChanged();

                Bundle bundle = new Bundle();
                bundle.putSerializable("msg", msg);

                SysmsgDetailFragment fragment = new SysmsgDetailFragment();
                fragment.setArguments(bundle);
                startFragment(fragment);
            }
        });
        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                refresh();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                loadmore();
            }
        });
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        refresh();
    }

    private void refresh() {
        mNetManager.getNoticeList(
                NetManager.PAGE_FIRST,
                NetManager.PAGE_FIRST + NetManager.PAGE_NUM,
                new Callback<BaseNoticeListEntity>() {
                    @Override
                    protected void onSucceed(BaseNoticeListEntity result) {
                        mListView.onRefreshComplete();
                        mAdapter.clear();

                        List<SysMessage> list = result.notice_list;
                        mAdapter.addAll(list);
                    }

                    @Override
                    protected void onError(Object object) {
                        mListView.onRefreshComplete();
                    }
                });
    }

    private void loadmore() {
        int end = mAdapter.getCount() + 1;
        mNetManager.getNoticeList(
                end,
                end + NetManager.PAGE_NUM,
                new Callback<BaseNoticeListEntity>() {
                    @Override
                    protected void onSucceed(BaseNoticeListEntity result) {
                        mListView.onRefreshComplete();

                        List<SysMessage> list = result.notice_list;
                        mAdapter.addAll(list);
                    }

                    @Override
                    protected void onError(Object object) {
                        mListView.onRefreshComplete();
                    }
                });
    }

    private void test() {
        List<SysMessage> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            SysMessage msg = new SysMessage();
            msg.title = "TITLE" + i;
            msg.text = "CONTENTCONTENTCONTENT" + i;
            msg.date = TimeUtil.getCurrentTime(TimeUtil.PATTERN_DATE);
            list.add(msg);
        }
        mAdapter.addAll(list);
    }

    private class ListAdapter extends ArrayAdapter<SysMessage> {
        private LayoutInflater mInflater;

        public ListAdapter() {
            super(mActivity, -1);

            mInflater = mActivity.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_sysmsglist, null);

                h.txt1 = (TextView) convertView.findViewById(R.id.tv_item_list_sysmsglist_txt1);
                h.txt2 = (TextView) convertView.findViewById(R.id.tv_item_list_sysmsglist_txt2);
                h.newSign = convertView.findViewById(R.id.iv_msg_new);

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            SysMessage msg = getItem(position);
            h.txt1.setText(msg.title);
            h.txt2.setText(msg.text);

            h.newSign.setVisibility("1".equals(msg.status) ? View.INVISIBLE : View.VISIBLE);

            return convertView;
        }

        private class Holder {
            View newSign;
            TextView txt1;
            TextView txt2;
        }
    }
}
