package jp.concierge.jetkys.android.ui.utils;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

/**
 * Created by WilliZ on 16/03/26.
 */
public class ViewStateHolder {
    public static final String TAG = "ViewStateHolder";

    public static void saveScroll(Bundle savedBundle, View view) {
        int scrollX = view.getScrollX();
        int scrollY = view.getScrollY();
        savedBundle.putInt("v_scrollx", scrollX);
        savedBundle.putInt("v_scrolly", scrollY);
    }

    public static int[] loadScroll(Bundle savedBundle) {
        int x = savedBundle.getInt("v_scrollx", 0);
        int y = savedBundle.getInt("v_scrolly", 0);
        return new int[]{x, y};
    }

    public static void saveListPosition(Bundle savedBundle, ListView lv) {
        int firstPosition = lv.getFirstVisiblePosition();
        savedBundle.putInt("v_first", firstPosition);
    }

    public static int loadListPosition(Bundle savedBundle) {
        int firstPosition = savedBundle.getInt("v_first", 0);
        return firstPosition;
    }
}
