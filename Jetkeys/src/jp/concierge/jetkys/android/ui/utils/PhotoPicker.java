package jp.concierge.jetkys.android.ui.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.utils.FileUtils;
import jp.concierge.jetkys.android.utils.Utils;

import java.io.File;

/**
 * Created by WilliZ on 16/03/20.
 */
public class PhotoPicker {
    public static final int REQUEST_TAKE_PHOTO = 1100;
    public static final int REQUEST_PICK_PHOTO = 1200;
    public static final int REQUEST_CUT_PHONTO = 1300;

    private Activity mActivity;
    private Fragment mFragment;
    private String mHintTake, mHintPick;
    private String mTmpPhotoPath;

    public PhotoPicker(Activity activity) {
        mActivity = activity;
        Resources res = activity.getResources();
        mHintTake = res.getString(R.string.hint_upload_photo_take);
        mHintPick = res.getString(R.string.hint_upload_photo_pick);
    }

    public PhotoPicker(Fragment fragment, Activity activity) {
    	mFragment = fragment;

        mActivity = activity;
        Resources res = activity.getResources();
        mHintTake = res.getString(R.string.hint_upload_photo_take);
        mHintPick = res.getString(R.string.hint_upload_photo_pick);
    }

    public void startPick() {
        String[] hints = new String[]{mHintTake, mHintPick};
        new AlertDialog.Builder(mActivity).setItems(hints, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // take picture
                        try {
                            mTmpPhotoPath = takePhoto();
                        } catch (Exception e) {
                            String ss = mActivity.getResources().getString(R.string.hint_sdcard_not_mounted);
                            Utils.toast(mActivity, ss);
                        }
                        break;
                    case 1: // pick picture
                        pickPhoto();
                        break;
                    default:
                        break;
                }
                dialog.dismiss();
            }
        }).show();
    }

    Uri takePhotoUri;
    
    private String takePhoto() {
        if (!FileUtils.isSDMounted()) { // sdcard mounted
            throw new NullPointerException("sdcard not mounted, cannot take photo.");
        }
        File dir = new File(FileUtils.DCIM);
        if (!dir.exists()) {
            dir.mkdir();
        }
        String filePath;
        String file = "jetkeys" + System.currentTimeMillis() + ".jpg";
        File picFile = new File(dir, file);
        filePath = picFile.getAbsolutePath();
        takePhotoUri = Uri.fromFile(picFile);

        Intent take = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        take.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        take.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        take.putExtra(MediaStore.EXTRA_OUTPUT, takePhotoUri);
        if (mFragment != null) {
            mFragment.startActivityForResult(take, REQUEST_TAKE_PHOTO);
        } else {
            mActivity.startActivityForResult(take, REQUEST_TAKE_PHOTO);
        }
        return filePath;
    }

    private void pickPhoto() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);// ACTION_OPEN_DOCUMENT
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/jpeg");
        if (mFragment != null) {
            mFragment.startActivityForResult(intent, REQUEST_PICK_PHOTO);
        } else {
            mActivity.startActivityForResult(intent, REQUEST_PICK_PHOTO);
        }
    }

    private String cutPhoto(Uri uri){
    	 if (!FileUtils.isSDMounted()) { // sdcard mounted
             throw new NullPointerException("sdcard not mounted, cannot take photo.");
         }
         File dir = new File(FileUtils.DCIM);
         if (!dir.exists()) {
             dir.mkdir();
         }
    	String filePath;
        String file = "jetkeys" + System.currentTimeMillis() + ".jpg";
        File picFile = new File(dir, file);
        filePath = picFile.getAbsolutePath();
        Uri uriOut = Uri.fromFile(picFile);
        
    	Intent intent = new Intent("com.android.camera.action.CROP");

        intent.setDataAndType(uri, "image/*");

        intent.putExtra("crop", "true");

        intent.putExtra("aspectX", 1);

        intent.putExtra("aspectY", 1);

        intent.putExtra("scale", true);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriOut);

        intent.putExtra("return-data", false);

        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());

        intent.putExtra("noFaceDetection", true); // no face detection
        if (mFragment != null) {
        	mFragment.startActivityForResult(intent, REQUEST_CUT_PHONTO);
        }else{
        	mFragment.startActivityForResult(intent, REQUEST_CUT_PHONTO);
        }
        return filePath;
    }
    
    public String onActivityResult(int requestCode, int resultCode, Intent data) {
//    	Log.d("doubleL", "[PhotoPicker.java]画像データの取得開始");
//        if(data == null){
//            return null;
//        }else{
        	if (resultCode != Activity.RESULT_OK && (requestCode == REQUEST_PICK_PHOTO || requestCode == REQUEST_TAKE_PHOTO || requestCode == REQUEST_CUT_PHONTO)) {
                mTmpPhotoPath = null;
            }
        	switch (requestCode) {
            case REQUEST_PICK_PHOTO:
                Uri photoUri = data.getData();
                mTmpPhotoPath = cutPhoto(photoUri);
//                mTmpPhotoPath = FileUtils.getPath(mActivity, photoUri);
                return "";
            case REQUEST_TAKE_PHOTO:
            	mTmpPhotoPath = cutPhoto(takePhotoUri);
            	return "";
            case REQUEST_CUT_PHONTO:
            	return mTmpPhotoPath;
            default:
                break;
        	}
        	return mTmpPhotoPath;
//        }
    }
}