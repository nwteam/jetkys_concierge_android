package jp.concierge.jetkys.android.ui;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.*;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaDrm;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.Bind;
import butterknife.OnClick;

import com.handmark.pulltorefresh.library.PullToRefreshBase;

import jp.concierge.jetkys.android.PicBrowseActivity;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.Settings;
import jp.concierge.jetkys.android.beans.Concierge;
import jp.concierge.jetkys.android.beans.Conversation;
import jp.concierge.jetkys.android.beans.Message;
import jp.concierge.jetkys.android.beans.UploadPhotoBean;
import jp.concierge.jetkys.android.beans.UserInfo;
import jp.concierge.jetkys.android.beans.base.BaseMessageListEntity;
import jp.concierge.jetkys.android.manager.NetManager;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.push.ChatManager;
import jp.concierge.jetkys.android.ui.utils.PhotoPicker;
import jp.concierge.jetkys.android.ui.views.PullListView;
import jp.concierge.jetkys.android.utils.DialogUtil;
import jp.concierge.jetkys.android.utils.PhotoUtils;
import jp.concierge.jetkys.android.utils.TimeUtil;
import jp.concierge.jetkys.android.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ChatFragment extends BaseFragment {
    public static final String FILTER_CHAT_MSG_RECEIVER = "ChatFragment.FILTER_CHAT_MSG_RECEIVER";
    public static final String FILTER_CHAT_FINISH_RECEIVER = "ChatFragment.FILTER_CHAT_FINISH_RECEIVER";

    private ChatMsgViewAdapter mAdapter;
    private List<Message> mMessageList = new ArrayList<Message>();

    @Override
    protected int layout() {
        return R.layout.fragment_chatdetail;
    }

    public ChatManager mChatManager;

    private MessageReceiver mMessageReceiver;

    @Bind(R.id.pulllistview)
    PullListView mListView;

    @Bind(R.id.et_chat_comment)
    EditText mEditTextContent;

    @OnClick(R.id.btn_chat_photo)
    public void sendPicture() {
        if (TextUtils.isEmpty(convid)) return;


//        // cid为空的话，代表在和用户聊天，自己是专家，发送不需要确认
//        if (!TextUtils.isEmpty(cid) && Settings.get(Settings.PAY_CONFIRM_SEND_MESSAGE)) {
//            // 需要确认
//            new AlertDialog.Builder(mActivity)
//                    .setMessage(R.string.hint_send_image_need_ticket)
//                    .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            mPhotoPicker.startPick();
//                        }
//                    }).setNegativeButton(R.string.cancel, null).show();
//        } else {
//            mPhotoPicker.startPick();
//        }
//        Message msg = new Message();
//        if (mAccountManager.user_id.equals(msg.from_id)&&Settings.get(Settings.PAY_CONFIRM_SEND_MESSAGE)) {
        if (!TextUtils.isEmpty(cid)&&Settings.get(Settings.PAY_CONFIRM_SEND_MESSAGE)) {
//	    	AlertDialog.Builder builder = new Builder(mActivity);
	    	String title_str="";
	    	String image_ticket=mUserInfo.image_ticket;
	    	if(image_ticket.equals("0")){
	    		title_str = "画像の送信に\n"+mConciergeInfo.chat_point+"kysを消費します。";
//        		builder.setTitle(title_str);
	    	}else{
	    		title_str = getString(R.string.hint_send_message_need_ticket);
//	    		builder.setTitle(R.string.hint_send_image_need_ticket);
	    	}
	    	DialogUtil.ShowSelectContent(mActivity, title_str, new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					int id = arg0.getId();
					if(id == R.id.dialog_btn1){
						submitSetting("setting_message",false);
    	        		mPhotoPicker.startPick();
					}else if(id == R.id.dialog_btn2){
						mPhotoPicker.startPick();
					}
				}
			});
	    	//定义列表中的选项
//	    	final String[] items = new String[]{
//	    	        "はい、以降表示しない",
//	    	        "はい",
//	    	};
	    	//设置列表选项
//	    	builder.setItems(items, new DialogInterface.OnClickListener() {
//	    	        //点击任何一个列表选项都会触发这个方法
//	    	        //arg1：点击的是哪一个选项
//	    	        @Override
//	    	        public void onClick(DialogInterface dialog, int which) {
//	    	        	if(items[which].equals("はい、以降表示しない")){
//	    	        		submitSetting("setting_message",false);
//	    	        		mPhotoPicker.startPick();
//	    	        	}
//	    	        	if(items[which].equals("はい")){
//	    	        		mPhotoPicker.startPick();
//	    	        	}
//	    	        }
//	    	});
//	    	// 取消选择
//	    	builder.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
//	    	        @Override
//	    	        public void onClick(DialogInterface dialog, int which) {
//	    	        }
//	    	});
//	    	builder.show();
    	}else{
    		mPhotoPicker.startPick();
    	}
    }

    private PhotoPicker mPhotoPicker;

    private void photoPicked(String path) {
        uploadPic(path);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String photoPath = mPhotoPicker.onActivityResult(requestCode, resultCode, data);
        if (!TextUtils.isEmpty(photoPath)) { // handled
            photoPicked(photoPath);
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.btn_chat_submit)
    public void sendChat() {

        if (TextUtils.isEmpty(convid)) return;

        final String content = mEditTextContent.getText().toString();
        if (TextUtils.isEmpty(content)) {
            return;
        }

//        if (Settings.get(Settings.PAY_CONFIRM_SEND_MESSAGE)) {
//            // 需要确认
//            new AlertDialog.Builder(mActivity)
//                    .setMessage(R.string.hint_send_message_need_ticket)
//                    .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            submit(null, content);
//                        }
//                    }).setNegativeButton(R.string.cancel, null).show();
//        } else {
//            submit(null, content);
//        }
//        Message msg = new Message();
//        if (mAccountManager.user_id.equals(msg.from_id)&&Settings.get(Settings.PAY_CONFIRM_SEND_MESSAGE)) {
        if (!TextUtils.isEmpty(cid)&&Settings.get(Settings.PAY_CONFIRM_SEND_MESSAGE)) {	
//	    	AlertDialog.Builder builder = new Builder(mActivity);
	    	String title_str="";
	    	String chat_ticket=mUserInfo.chat_ticket;
	    	if(chat_ticket.equals("0")){
	    		title_str = "メッセージの送信に\n"+mConciergeInfo.chat_point+"kysを消費します。";
//        		builder.setTitle(title_str);
	    	}else{
	    		title_str = getString(R.string.hint_see_message_need_ticket);
//	    		builder.setTitle(R.string.hint_send_message_need_ticket);
	    	}
	    	
	    	DialogUtil.ShowSelectContent(mActivity, title_str, new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					int id = arg0.getId();
					if(id == R.id.dialog_btn1){
						submitSetting("setting_message",false);
    	        		submit(null, content);
					}else if(id == R.id.dialog_btn2){
						submit(null, content);
					}
				}
			});
	    	//定义列表中的选项
//	    	final String[] items = new String[]{
//	    	        "はい、以降表示しない",
//	    	        "はい",
//	    	};
//	    	//设置列表选项
//	    	builder.setItems(items, new DialogInterface.OnClickListener() {
//	    	        //点击任何一个列表选项都会触发这个方法
//	    	        //arg1：点击的是哪一个选项
//	    	        @Override
//	    	        public void onClick(DialogInterface dialog, int which) {
//	    	        	if(items[which].equals("はい、以降表示しない")){
//	    	        		submitSetting("setting_message",false);
//	    	        		submit(null, content);
//	    	        	}
//	    	        	if(items[which].equals("はい")){
//	    	        		submit(null, content);
//	    	        	}
//	    	        }
//	    	});
//	    	// 取消选择
//	    	builder.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
//	    	        @Override
//	    	        public void onClick(DialogInterface dialog, int which) {
//	    	        }
//	    	});
//	    	builder.show();
    	}else{
    		submit(null, content);
    	}
    }

    private void uploadPic(String path) {
        mNetManager.uploadPhoto(path, new Callback<UploadPhotoBean>() {
            @Override
            protected void onSucceed(UploadPhotoBean result) {
                submit(result.imageid, null);
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }

    private void submit(final String photo, final String message) {
        mNetManager.sendMessage(convid, photo, cid, message, uid,
                new Callback<Void>() {
                    @Override
                    protected void onSucceed(Void result) {
                        dismissLoading();

                        if (!TextUtils.isEmpty(photo)) {
//                            Message msg = new Message();
//                            msg.new_from_id = mAccountManager.user_id;
//                            msg.from_id = mAccountManager.user_id;
//                            msg.type = Message.PICTURE;
//                            msg.message = photo;
//                            msg.status = "1";
//                            addMessage(msg);
                        }

                        if (!TextUtils.isEmpty(message)) {
//                            Message msg = new Message();
//                            msg.new_from_id = mAccountManager.user_id;
//                            msg.from_id = mAccountManager.user_id;
//                            msg.type = Message.TEXT;
//                            msg.message = message;
//                            msg.status = "1";
//                            addMessage(msg, true);
                        	mEditTextContent.setText("");
                        }
//                        Log.e("1111", "mMessageList.size()="+mMessageList.size());
                        if(mMessageList.size() > 20){
                        	for(int i = 0;i < 20;i++){
                        		mMessageList.remove(mMessageList.size() - 1);
                        	}
                        }else{
                        	mMessageList.clear();
                        }
//                        onFirstTimeInit();
//                        Log.e("11112", "mMessageList.size()="+mMessageList.size());
                        
                        mNetManager.getChatMsgList(did, cid, uid, NetManager.PAGE_FIRST,
                                new Callback<BaseMessageListEntity>() {
                                    @Override
                                    protected void onSucceed(BaseMessageListEntity result) {
                                        dismissLoading();

                                        convid = result.conversation_id;

                                        mMessageList.addAll(result.list);
                                        mAdapter.notifyDataSetChanged();

                                        mListView.getRefreshableView().setSelection(mListView.getRefreshableView().getCount() - 1);
                                    }

                                    @Override
                                    protected void onError(Object object) {
                                        dismissLoading();
                                    }
                                });
                        
                    }

                    @Override
                    protected void onError(Object object) {
                        dismissLoading();
                    }
                });
        showLoading();
    }

    public String convid;
    public String uid, cid, did;
    private String title;
    private UserInfo mUserInfo;
    private Concierge mConciergeInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMessageReceiver = new MessageReceiver();
        mMessageReceiver.register();

        mPhotoPicker = new PhotoPicker(this, mActivity);

        mChatManager = ChatManager.getInstance();
        mChatManager.setChatFragment(this);

        Bundle bundle = getArguments();
//        convid = "77";
        convid = bundle.getString("convid");
        uid = bundle.getString("uid");
        cid = bundle.getString("cid");
        did = bundle.getString("did");
        title = bundle.getString("title");

        mChatManager.conversationId = convid;
        
        mNetManager.getUserInfo("1", new Callback<UserInfo>() {
            @Override
            protected void onSucceed(UserInfo result) {
                dismissLoading();

                mUserInfo = result;
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        if(!TextUtils.isEmpty(cid)){
	        mNetManager.getConciergeDetail(cid, new Callback<Concierge>() {
	            @Override
	            protected void onSucceed(Concierge result) {
	                dismissLoading();
	
	                mConciergeInfo = result;
	            }
	
	            @Override
	            protected void onError(Object object) {
	                dismissLoading();
	            }
	        });
        }
        showLoading();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Utils.disableKeyBoard(mActivity);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mChatManager.conversationId = "";
        mChatManager.setChatFragment(null);

        mMessageReceiver.unregister();
        mMessageReceiver = null;
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        setTitle(title);
        needBackFinish();

        mAdapter = new ChatMsgViewAdapter();
        mListView.setAdapter(mAdapter);
        mListView.getRefreshableView().setSelector(new ColorDrawable(Color.TRANSPARENT));
        mListView.getRefreshableView().setDividerHeight(0);
        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                loadmore();
            }
        });
    }

//    public void test() {
//        for (int i = 0; i < 8; i++) {
//            Message msg = new Message();
//            msg.text = MSG_TEST[i];
//            msg.time = DATE_TEST[i];
//            if (i % 2 == 0) {
//                msg.uid = "1";
//                msg.paid = true;
//            }
//            mMessageList.add(msg);
//        }
//
//        for (int i = 0; i < 3; i++) {
//            Message msg = new Message();
//            msg.type = Message.PICTURE;
//            msg.text = MSG_TEST[i];
//            msg.time = DATE_TEST[i];
//            msg.picUrl = "http://imgsrc.baidu.com/forum/pic/item/7ce4d10735fae6cd402c27930fb30f2440a70ff3.jpg";
//            if (i % 2 == 0 && i > 0) {
//                msg.uid = "1";
//            }
//            mMessageList.add(i * 3, msg);
//        }
//
//        mAdapter.notifyDataSetChanged();
//    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        mNetManager.getChatMsgList(did, cid, uid, NetManager.PAGE_FIRST,
                new Callback<BaseMessageListEntity>() {
                    @Override
                    protected void onSucceed(BaseMessageListEntity result) {
                        dismissLoading();

                        convid = result.conversation_id;

                        mMessageList.addAll(result.list);
                        mAdapter.notifyDataSetChanged();

                        mListView.getRefreshableView().setSelection(mListView.getRefreshableView().getCount() - 1);
                    }

                    @Override
                    protected void onError(Object object) {
                        dismissLoading();
                    }
                });
        showLoading();
    }

    private void loadmore() {
        int start = mMessageList.size() + 1;
        mNetManager.getChatMsgList(did, cid, uid, start,
                new Callback<BaseMessageListEntity>() {
                    @Override
                    protected void onSucceed(BaseMessageListEntity result) {
                        mListView.onRefreshComplete();

                        convid = result.conversation_id;

                        mMessageList.addAll(0, result.list);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    protected void onError(Object object) {
                        mListView.onRefreshComplete();
                    }
                });
    }

    private void seeFullContent(int index) {
        final Message msg = mMessageList.get(index);
        mNetManager.seeMessage(msg.id, new Callback<Message>() {
            @Override
            protected void onSucceed(Message result) {
                dismissLoading();

                msg.id = result.id;
                msg.message = result.message;
                msg.type = result.type;
                msg.from_id = result.from_id;
                msg.time = result.time;
                msg.status = "1";

                mAdapter.notifyDataSetChanged();
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }

    private void addMessage(Message msg, boolean clearEdit) {
        if (clearEdit) {
            mEditTextContent.setText("");
        }

        msg.time = TimeUtil.getCurrentTime(TimeUtil.PATTERN_SEC24_1);
        addMessage(msg);
    }

    public void addMessage(Message msg) {
        mMessageList.add(msg);
        mAdapter.notifyDataSetChanged();

        mListView.getRefreshableView().setSelection(mListView.getRefreshableView().getCount() - 1);
    }

    private class MessageReceiver extends BroadcastReceiver {

        public void register() {
            mActivity.registerReceiver(this, new IntentFilter(FILTER_CHAT_MSG_RECEIVER));
        }

        public void unregister() {
            mActivity.unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Message msg = (Message) intent.getSerializableExtra("msg");
            if (msg != null) {
                addMessage(msg);
            }
        }
    }

    public class ChatMsgViewAdapter extends BaseAdapter {

        private final int LEFT_TEXT = 0;
        private final int RIGHT_TEXT = 1;
        private final int LEFT_PIC = 2;
        private final int RIGHT_PIC = 3;

        private LayoutInflater mInflater;

        public ChatMsgViewAdapter() {
            mInflater = LayoutInflater.from(mActivity);
        }

        @Override
        public int getCount() {
            return mMessageList.size();
        }

        @Override
        public Message getItem(int position) {
            return mMessageList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(final int position) {
            Message msg = mMessageList.get(position);

            String uid = msg.new_from_id;
            String myself = mAccountManager.user_id;

            if (myself.equals(uid)) {
                msg.status = "1";
                if (Message.PICTURE.equals(msg.type)) {
                    return RIGHT_PIC;
                } else {
                    return RIGHT_TEXT;
                }
            } else {
                if (Message.PICTURE.equals(msg.type)) {
                    return LEFT_PIC;
                } else {
                    return LEFT_TEXT;
                }
            }
        }

        @Override
        public int getViewTypeCount() {
            return 4;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final int type = getItemViewType(position);

            ViewHolder h = null;
            if (convertView == null) {
                h = new ViewHolder();
                switch (type) {
                    case LEFT_TEXT:
                        convertView = mInflater.inflate(R.layout.chatting_item_msg_text_left, null);
                        h.content = (TextView) convertView.findViewById(R.id.tv_chatcontent);
                        break;
                    case LEFT_PIC:
                        convertView = mInflater.inflate(R.layout.chatting_item_msg_pic_left, null);
                        h.pic = (ImageView) convertView.findViewById(R.id.iv_item_list_chat_pic);
                        break;
                    case RIGHT_TEXT:
                        convertView = mInflater.inflate(R.layout.chatting_item_msg_text_right, null);
                        h.content = (TextView) convertView.findViewById(R.id.tv_chatcontent);
                        break;
                    case RIGHT_PIC:
                        convertView = mInflater.inflate(R.layout.chatting_item_msg_pic_right, null);
                        h.pic = (ImageView) convertView.findViewById(R.id.iv_item_list_chat_pic);
                        break;
                }

                if (convertView == null) {
                    return null;
                }

                h.timeHour = (TextView) convertView.findViewById(R.id.tv_sendtime_hour);
                h.time = (TextView) convertView.findViewById(R.id.tv_sendtime);
                h.uname = (TextView) convertView.findViewById(R.id.tv_username);
                h.avatar = (ImageView) convertView.findViewById(R.id.iv_avatar);
                h.blur = convertView.findViewById(R.id.layout_blur);
                h.seeFull = convertView.findViewById(R.id.btn_see_full);

                convertView.setTag(h);
            } else {
                h = (ViewHolder) convertView.getTag();
            }

            final Message item = getItem(position);

            if (position == 0) {
            	if(!item.time.equals("")){
            		h.time.setText(item.time.substring(0, 10));
            	}
                h.time.setVisibility(View.VISIBLE);
            } else { // pos > 0 compare prev record
                Message msg0 = getItem(position - 1);
                if (!timeEquals(msg0.time, item.time)) {
                	if(!item.time.equals("")){
                		h.time.setText(item.time.substring(0, 10));
                	}
                	h.time.setVisibility(View.VISIBLE);
                } else {
//                	h.time.setText(item.time);
//                    h.time.setVisibility(View.VISIBLE);
                    h.time.setVisibility(View.GONE);
                }
            }
            if(!item.time.equals("")){
            	h.timeHour.setText(item.time.substring(10,16));
            }else{
            	h.timeHour.setText("");
            }

//            asyncLoadImageAvatar(item.uavatar, h.avatar);
            asyncLoadImageAvatar(item.uavatar, h.avatar,item.gender);
            h.uname.setText(item.nickname);

            View contentV = null;
            switch (type) {
                case LEFT_TEXT:
                case RIGHT_TEXT:
                    h.content.setText(item.message);
                    contentV = h.content;
                    break;

                case LEFT_PIC:
                case RIGHT_PIC:
                    final String imgUrl = item.message;
                    asyncLoadImage(imgUrl, h.pic);
                    h.pic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View arg0) {
                            Intent intent = new Intent();
                            intent.setClass(mActivity, PicBrowseActivity.class);
                            intent.putExtra("url", PhotoUtils.getPhotoUrl(imgUrl));
                            startActivity(intent);
                        }
                    });
                    contentV = h.pic;
                    break;
            }

            boolean paid = "1".equals(item.status);
            if (contentV != null) {
                contentV.setVisibility(paid ? View.VISIBLE : View.GONE);
            }
            h.blur.setVisibility(paid ? View.GONE : View.VISIBLE);
            h.seeFull.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // cid为空的话，代表在和用户聊天，自己是专家，发送不需要确认
//                    if (!TextUtils.isEmpty(cid) && Settings.get(Settings.PAY_CONFIRM_SEE_PICTURE)) {
//                        new AlertDialog.Builder(mActivity)
//                                .setMessage(R.string.hint_see_message_need_money)
//                                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        seeFullContent(position);
//                                    }
//                                }).setNegativeButton(R.string.cancel, null).show();
//                    } else {
//                        seeFullContent(position);
//                    }
//                	Message msg = new Message();
                	switch(type){
                		case LEFT_TEXT:
//                			if (mAccountManager.user_id.equals(item.from_id)&&Settings.get(Settings.PAY_CONFIRM_SEND_MESSAGE)) {
                			if (!TextUtils.isEmpty(cid)&&Settings.get(Settings.PAY_CONFIRM_SEND_MESSAGE)) {
//                    	    	AlertDialog.Builder builder = new Builder(mActivity);
                    	    	String title_str="";
                    	    	String chat_ticket=mUserInfo.chat_ticket;
                    	    	if(chat_ticket.equals("0")){
                    	    		title_str = "メッセージの受信に\n"+mConciergeInfo.chat_point+"kysを消費します。";
//                            		builder.setTitle(title_str);
                    	    	}else{
                    	    		title_str = getString(R.string.hint_see_message_need_ticket);
//                    	    		builder.setTitle(R.string.hint_see_message_need_ticket);
                    	    	}
                    	    	
                    	    	DialogUtil.ShowSelectContent(mActivity, title_str, new OnClickListener() {
									
									@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										int id = arg0.getId();
										if(id == R.id.dialog_btn1){
											submitSetting("setting_message",false);
                	    	        		seeFullContent(position);
										}else if(id == R.id.dialog_btn2){
											seeFullContent(position);
										}
									}
								});
                    	    	//定义列表中的选项
//                    	    	final String[] items = new String[]{
//                    	    	        "はい、以降表示しない",
//                    	    	        "はい",
//                    	    	};
                    	    	
//                    	    	//设置列表选项
//                    	    	builder.setItems(items, new DialogInterface.OnClickListener() {
//                    	    	        //点击任何一个列表选项都会触发这个方法
//                    	    	        //arg1：点击的是哪一个选项
//                    	    	        @Override
//                    	    	        public void onClick(DialogInterface dialog, int which) {
//                    	    	        	if(items[which].equals("はい、以降表示しない")){
//                    	    	        		submitSetting("setting_message",false);
//                    	    	        		seeFullContent(position);
//                    	    	        	}
//                    	    	        	if(items[which].equals("はい")){
//                    	    	        		seeFullContent(position);
//                    	    	        	}
//                    	    	        }
//                    	    	});
//                    	    	// 取消选择
//                    	    	builder.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
//                    	    	        @Override
//                    	    	        public void onClick(DialogInterface dialog, int which) {
//                    	    	        }
//                    	    	});
//                    	    	builder.show();
                        	}else{
                        		seeFullContent(position);
                        	}
                			break;
                		case LEFT_PIC:
//                			if (mAccountManager.user_id.equals(item.from_id)&&Settings.get(Settings.PAY_CONFIRM_SEE_PICTURE)) {
                			if (!TextUtils.isEmpty(cid)&&Settings.get(Settings.PAY_CONFIRM_SEE_PICTURE)) {
                    	    	AlertDialog.Builder builder = new Builder(mActivity);
                    	    	String title_str="";
                    	    	String image_ticket=mUserInfo.image_ticket;
                    	    	if(image_ticket.equals("0")){
                    	    		title_str = "画像の受信に"+mConciergeInfo.chat_point+"kysを消費します。";
                            		builder.setTitle(title_str);
                    	    	}else{
                    	    		builder.setTitle(R.string.hint_see_image_need_ticket);
                    	    	}
                    	    	//定义列表中的选项
                    	    	final String[] items = new String[]{
                    	    	        "はい、以降表示しない",
                    	    	        "はい",
                    	    	};
                    	    	//设置列表选项
                    	    	builder.setItems(items, new DialogInterface.OnClickListener() {
                    	    	        //点击任何一个列表选项都会触发这个方法
                    	    	        //arg1：点击的是哪一个选项
                    	    	        @Override
                    	    	        public void onClick(DialogInterface dialog, int which) {
                    	    	        	if(items[which].equals("はい、以降表示しない")){
                    	    	        		submitSetting("setting_message",false);
                    	    	        		seeFullContent(position);
                    	    	        	}
                    	    	        	if(items[which].equals("はい")){
                    	    	        		seeFullContent(position);
                    	    	        	}
                    	    	        }
                    	    	});
                    	    	// 取消选择
                    	    	builder.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
                    	    	        @Override
                    	    	        public void onClick(DialogInterface dialog, int which) {
                    	    	        }
                    	    	});
                    	    	builder.show();
                        	}else{
                        		seeFullContent(position);
                        	}
                			break;                			
                	}
                    
                }
            });

            return convertView;
        }

        private boolean timeEquals(String time0, String time1) {
            try {
                String t0 = time0.substring(0, 10); // yyyy-MM-dd
                String t1 = time1.substring(0, 10);

                return t0.equals(t1);
            } catch (Exception e) {
                return false;
            }
        }

        private class ViewHolder {
            public TextView time;
            public TextView timeHour;
            public ImageView avatar;
            public TextView uname;
            public TextView content;
            public ImageView pic;
            public View blur;
            public View seeFull;
        }
    }
    private void submitSetting(final String key, final boolean value) {
        mNetManager.submitSetting(key, value, new Callback<Void>() {
            @Override
            protected void onSucceed(Void result) {
                dismissLoading();
                if ("setting_mail".equals(key)) {
                    Settings.save(Settings.PAY_CONFIRM_SEND_MAIL, value);
                } else if ("setting_message".equals(key)) {
                    Settings.save(Settings.PAY_CONFIRM_SEND_MESSAGE, value);
                } else if ("setting_image".equals(key)) {
                    Settings.save(Settings.PAY_CONFIRM_SEE_PICTURE, value);
                }
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }
}
