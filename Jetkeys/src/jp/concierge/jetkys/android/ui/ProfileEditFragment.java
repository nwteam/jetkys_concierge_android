package jp.concierge.jetkys.android.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import butterknife.Bind;
import butterknife.OnClick;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.EditUserInfo;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.utils.ConstData;
import jp.concierge.jetkys.android.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by WilliZ on 16/03/21.
 */
public class ProfileEditFragment extends BaseFragment {
    @Override
    protected int layout() {
        return R.layout.fragment_profile_edit;
    }

    @Bind({R.id.cb_search_0, R.id.cb_search_1, R.id.cb_search_2,
            R.id.cb_search_3, R.id.cb_search_4, R.id.cb_search_5,
            R.id.cb_search_6, R.id.cb_search_7, R.id.cb_search_8,
            R.id.cb_search_9, R.id.cb_search_10, R.id.cb_search_11,
            R.id.cb_search_12, R.id.cb_search_13,})
    List<CheckBox> mTypeCbList;
    private ArrayList<Integer> mCheckedTypeList;

    @Bind(R.id.rgroup_gender)
    RadioGroup mGenderRgroup;
    @Bind(R.id.rb_male)
    RadioButton mMaleRb;
    @Bind(R.id.rb_female)
    RadioButton mFemaleRb;

    @OnClick(R.id.layout_pick_year)
    void pickYear() {
        ViewPicker.showPickYear(mActivity, new ViewPicker.OnItemClickListener() {
            @Override
            public void onItemClick(int index, String value) {
                if (!value.equals(mYearTv.getText().toString())) {
                    mDateTv.setText("1");
                }

                mYearTv.setText(value);
            }
        });
    }

    @OnClick(R.id.layout_pick_month)
    void pickMonth() {
        ViewPicker.showPickMonth(mActivity, new ViewPicker.OnItemClickListener() {
            @Override
            public void onItemClick(int index, String value) {
                if (!value.equals(mMonthTv.getText().toString())) {
                    mDateTv.setText("1");
                }

                mMonthTv.setText(value);
            }
        });
    }

    @OnClick(R.id.layout_pick_date)
    void pickDate() {
        int year = Integer.valueOf(mYearTv.getText().toString());
        int month = Integer.valueOf(mMonthTv.getText().toString());

        ViewPicker.showPickDate(mActivity, year, month, new ViewPicker.OnItemClickListener() {
            @Override
            public void onItemClick(int index, String value) {
                mDateTv.setText(value);
            }
        });
    }

    @OnClick(R.id.layout_pick_blood)
    void pickBlood() {
        ViewPicker.showPickBlood(mActivity, new ViewPicker.OnItemClickListener() {
            @Override
            public void onItemClick(int index, String value) {
                mBloodTv.setText(value);
            }
        });
    }

    @Bind(R.id.tv_year)
    TextView mYearTv;
    @Bind(R.id.tv_month)
    TextView mMonthTv;
    @Bind(R.id.tv_date)
    TextView mDateTv;
    @Bind(R.id.tv_blood)
    TextView mBloodTv;

    @Bind(R.id.et_profileedit_name)
    EditText mNameEt;

    @Bind(R.id.layout_edit_category)
    View mCategoryView;

    private void initAllPick() {
        Calendar calendar = Calendar.getInstance();
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH) + 1;
        int d = calendar.get(Calendar.DATE);

        mYearTv.setText("" + y);
        mMonthTv.setText("" + m);
        mDateTv.setText("" + d);

        mBloodTv.setText("A");
    }

    private String type, title;
    private EditUserInfo mEditUserInfo;
    private boolean mCheckItemInitingLock;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCheckedTypeList = new ArrayList<>();

        Bundle bundle = getArguments();
        type = bundle.getString("type");
        title = bundle.getString("title");
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        needBackFinish();
        setTitle(title);

        if ("2".equals(type)) {
            mCategoryView.setVisibility(View.VISIBLE);
        }

        int size = ConstData.sTypeLists.size();
        for (int i = 0; i < size; i++) {
            String type = ConstData.sTypeLists.get(i);
            CheckBox cb = mTypeCbList.get(i);
            cb.setText(type);

            final int pos = i;
            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (mCheckItemInitingLock) return;

                    if (isChecked && !mCheckedTypeList.contains(pos)) {
                        mCheckedTypeList.add(Integer.valueOf(pos));
                    } else {
                        mCheckedTypeList.remove(Integer.valueOf(pos));
                    }

                    Collections.sort(mCheckedTypeList);
                }
            });
        }
        initAllPick();
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        mNetManager.getEditUser(type, new Callback<EditUserInfo>() {
            @Override
            protected void onSucceed(EditUserInfo result) {
                dismissLoading();
                mEditUserInfo = result;
                setDatas();
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }

    private void setDatas() {
        if (mEditUserInfo == null) return;

        mNameEt.setText(mEditUserInfo.nickname);
        if ("1".equals(mEditUserInfo.gender)) {
            mMaleRb.setChecked(true);
        } else {
            mFemaleRb.setChecked(true);
        }

        try {
            String[] birth = mEditUserInfo.birthday.split("-");

            mYearTv.setText("" + birth[0]);
            mMonthTv.setText("" + birth[1]);
            mDateTv.setText("" + birth[2]);
        } catch (Exception e) {
        }

        if (!TextUtils.isEmpty(mEditUserInfo.category_name)) {
            mCheckItemInitingLock = true;
            String[] list = mEditUserInfo.category_name.split(",");
            if (list.length > 0) {
                for (String cat : list) {
                    int index = ConstData.findIndex(cat);
                    mCheckedTypeList.add(index);
                    mTypeCbList.get(index).setChecked(true);
                }
            }
            mCheckItemInitingLock = false;
        }
    }

    @OnClick(R.id.btn_submit)
    void submitOnClick() {
        String name = mNameEt.getText().toString();
        String birth = ViewPicker.getBirth(mYearTv, mMonthTv, mDateTv);
        String gender = ViewPicker.getGender(mGenderRgroup);
        String category = ConstData.getTypeCheckedIds(mCheckedTypeList);

        mNetManager.uploadUser(type, name, birth, gender, category,
                new Callback<Void>() {
                    @Override
                    protected void onSucceed(Void result) {
                        dismissLoading();

                        toast("データ更新成功");

                        setResult(RESULT_OK);
                        finish();
                    }

                    @Override
                    protected void onError(Object object) {
                        dismissLoading();
                    }
                });
        showLoading();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utils.disableKeyBoard(mActivity);
    }
}
