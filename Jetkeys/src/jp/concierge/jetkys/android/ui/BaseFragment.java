package jp.concierge.jetkys.android.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import cn.jpush.android.api.JPushInterface;
import jp.concierge.jetkys.android.MainActivity;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.manager.AccountManager;
import jp.concierge.jetkys.android.manager.BaseManager;
import jp.concierge.jetkys.android.manager.ManagerFactory;
import jp.concierge.jetkys.android.manager.NetManager;
import jp.concierge.jetkys.android.ui.utils.ZImageLoader;
import jp.concierge.jetkys.android.utils.PhotoUtils;
import jp.concierge.jetkys.android.utils.Utils;
import jp.concierge.jetkys.android.utils.ZLog;

/**
 * Created by WilliZ on 16/03/19.
 */
public abstract class BaseFragment extends Fragment {
    public static int RESULT_OK = -1;

    public static final String NEED_FRAGMENT_RESULT = "Z.BaseFragment.NeedResult";
    public static final String FRAGMENT_RESULT_KEY = "Z.BaseFragment.Result.Key";

    public static class FResult {
        public int resultCode;
        public int requestCode;
        public Bundle datas;
    }

    public static final String TAG = "BaseFragment";

    protected MainActivity mActivity;

    private Bundle mSavedBundle;

    private boolean mNeedResult;
    private int mResultHashKey;

    protected AccountManager mAccountManager;
    protected NetManager mNetManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = (MainActivity) getActivity();

        firstTimeInit = true;

        Bundle bundle = getArguments();
        if (bundle != null) {
            mNeedResult = bundle.getBoolean(NEED_FRAGMENT_RESULT, false);
            mResultHashKey = bundle.getInt(FRAGMENT_RESULT_KEY, 0);
        }

        mAccountManager = getManager(AccountManager.class);
        mNetManager = getManager(NetManager.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        JPushInterface.onFragmentResume(mActivity, getClass().getSimpleName());
    }

    @Override
    public void onPause() {
        super.onPause();
        JPushInterface.onFragmentPause(mActivity, getClass().getSimpleName());
    }

    private boolean firstTimeInit;
    private View mRoot;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRoot = inflater.inflate(layout(), container, false);
        ButterKnife.bind(this, mRoot);

        mAccountManager.newOne();
        mNetManager.newOne();

        initViews(mSavedBundle);

        if (firstTimeInit) {
            onFirstTimeInit();
        }

        return mRoot;
    }

    /**
     * 子类应该实现的是这个方法，#onCreateView()可能还有其他事情要做
     *
     * @param bundle
     */
    protected void initViews(Bundle bundle) {
    }

    /**
     * 该方法只调用一次，只用一次且必须在view初始化之后的操作可以在这里进行
     */
    protected void onFirstTimeInit() {
        firstTimeInit = false;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        readResult();
    }

    /**
     * 与#startFragmentForResult()做对应，由于fragment的特殊性，
     * 采取的办法是在#onCreateView()之后，
     * 也就是view被重建的时候从宿主Activity中读取这段上个fragment给返回来的信息
     */
    private void readResult() {
        FResult result = mActivity.getResult(hashCode());
        if (result != null) {
            onFragmentResult(result.requestCode, result.resultCode, result.datas);
        }
    }

    /**
     * 类似onActivityResult，子类实现得到这些参数
     *
     * @param requestCode
     * @param resultCode
     * @param datas
     */
    protected void onFragmentResult(int requestCode, int resultCode, Bundle datas) {
    }

    public void setResult(int resultCode) {
        mActivity.makeResult(mResultHashKey, resultCode, null);
    }

    /**
     * 同Activity的setresult
     *
     * @param resultCode
     * @param datas
     */
    public void setResult(int resultCode, Bundle datas) {
        mActivity.makeResult(mResultHashKey, resultCode, datas);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ZLog.v(TAG, "onDestroyView()");

        dismissLoading();

        mSavedBundle = new Bundle();
        saveBundle(mSavedBundle);
    }

    /**
     * 这个方法是在Fragment被销毁的时候保存一些状态用的，
     * 在#onDestroyView()中被调用
     *
     * @param savedBundle 需要被保存的
     */
    protected void saveBundle(Bundle savedBundle) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        ButterKnife.unbind(this);

        mNetManager.onDestroy();
        mAccountManager.onDestroy();
    }

    protected abstract int layout();

    protected void needBackFinish() {
        ((View) findViewById(R.id.iv_topbar_back)).setVisibility(View.VISIBLE);
        ((View) findViewById(R.id.layout_topbar_left)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void setTitle(CharSequence title) {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(title);
    }

    public View getRootView() {
        return mRoot;
    }

    public <T> T findViewById(int res) {
        return (T) mRoot.findViewById(res);
    }

    public void toast(CharSequence s) {
        if (!TextUtils.isEmpty(s)) {
            Utils.toast(mActivity, s);
        }
    }

    public void startFragment(BaseFragment f) {
        mActivity.startFragment(f);
    }

    public void startFragmentForResult(BaseFragment targetf,
                                       int requestCode) {
        mActivity.startFragmentForResult(this, targetf, requestCode);
    }

    public void finish() {
        mActivity.finishFragment();
    }

    public <T extends BaseManager> T getManager(Class<? extends BaseManager> ownerClass) {
        return (T) ManagerFactory.getInstance().getManager(mActivity, ownerClass);
    }

    public void showLoading() {
        mActivity.showLoading();
    }

    public void dismissLoading() {
        mActivity.dismissLoading();
    }

    public void asyncLoadImage(String url, ImageView iv) {
        String realurl = PhotoUtils.getPhotoUrl(url);
        if (!TextUtils.isEmpty(realurl)) {
        		ZImageLoader.asyncLoadImage(realurl, iv);
        }
    }
    public void asyncLoadImageAvatar(String url, ImageView iv,String gender) {
    	String url_man="a0e2c173e2f8446dee02b60f2af6125a2c29b3885099661f8d906835bfcc4d04.png";
    	String url_woman="f05f4a61083384031db4751ae8f655b1527eae0704260debb9672ddd6d6948f8.png";
        String realurl = PhotoUtils.getPhotoUrl(url);
        String realurl_man = PhotoUtils.getPhotoUrl(url_man);
        String realurl_woman = PhotoUtils.getPhotoUrl(url_woman);
    	if(gender.equals("1")){
    		if (!TextUtils.isEmpty(realurl)) {
    			ZImageLoader.asyncLoadImage(realurl, iv);
    		}else{
    			ZImageLoader.asyncLoadImage(realurl_man, iv);
    		}
    	}else if(gender.equals("2")){
    		if (!TextUtils.isEmpty(realurl)) {
    			ZImageLoader.asyncLoadImage(realurl, iv);
    		}else{
    			ZImageLoader.asyncLoadImage(realurl_woman, iv);
    		}
    	}else{
    		if (!TextUtils.isEmpty(realurl)) {
            	ZImageLoader.asyncLoadImage(realurl, iv);
            }
    	}
    }
        
    
}