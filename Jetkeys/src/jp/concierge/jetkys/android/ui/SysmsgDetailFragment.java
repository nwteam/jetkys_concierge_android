package jp.concierge.jetkys.android.ui;

import android.os.Bundle;
import android.widget.TextView;
import butterknife.Bind;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.SysMessage;
import jp.concierge.jetkys.android.beans.base.BaseNoticeListEntity;
import jp.concierge.jetkys.android.net.thread.Callback;

/**
 * Created by WilliZ on 16/03/21.
 */
public class SysmsgDetailFragment extends BaseFragment {

    @Override
    protected int layout() {
        return R.layout.fragment_sysmsgdetail;
    }

    @Bind(R.id.tv_sysmsgdetail_title)
    TextView title;
    @Bind(R.id.tv_sysmsgdetail_content)
    TextView content;
    @Bind(R.id.tv_sysmsgdetail_time)
    TextView time;

    private SysMessage mSysMsg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSysMsg = (SysMessage) getArguments().getSerializable("msg");
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        needBackFinish();

        setDatas();
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        mNetManager.getNoticeDetail(mSysMsg.id, new Callback<SysMessage>() {
            @Override
            protected void onSucceed(SysMessage result) {
                dismissLoading();
                mSysMsg = result;

                setDatas();
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });

        showLoading();
    }

    private void setDatas() {
        if (mSysMsg == null) return;

        title.setText(mSysMsg.title);
        content.setText(mSysMsg.text);
        time.setText(mSysMsg.date);
    }
}
