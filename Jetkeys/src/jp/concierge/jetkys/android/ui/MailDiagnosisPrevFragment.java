package jp.concierge.jetkys.android.ui;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.OnClick;
import jp.concierge.jetkys.android.PicBrowseActivity;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.Settings;
import jp.concierge.jetkys.android.beans.Concierge;
import jp.concierge.jetkys.android.beans.Diagnosis;
import jp.concierge.jetkys.android.beans.MailBean;
import jp.concierge.jetkys.android.beans.UploadPhotoBean;
import jp.concierge.jetkys.android.beans.UserInfo;
import jp.concierge.jetkys.android.manager.AccountManager;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.utils.DisplayUtil;
import jp.concierge.jetkys.android.ui.utils.PhotoPicker;
import jp.concierge.jetkys.android.ui.utils.ZImageLoader;
import jp.concierge.jetkys.android.utils.DialogUtil;
import jp.concierge.jetkys.android.utils.ZLog;

import java.util.Calendar;

/**
 * Created by WilliZ on 16/03/20.
 */
public class MailDiagnosisPrevFragment extends BaseFragment {

    @Override
    protected int layout() {
        return R.layout.fragment_mail_preview;
    }
    
    @OnClick(R.id.btn_submit)
    void submitClick() {
//    	if (Settings.get(Settings.PAY_CONFIRM_SEND_MAIL)) {
//        new AlertDialog.Builder(mActivity)
//                .setMessage(R.string.hint_confirm_content_ok)
//                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        submit0();
//                    }
//                }).setNegativeButton(R.string.cancel, null).show();
//    	}else{
//    		submit0();
//    	}
//    	Dialog dialog = new AlertDialog.Builder(mActivity)
//    			.setMessage(R.string.hint_send_mail_need_ticket)
//    			.setPositiveButton("はい、以降表示しない",new DialogInterface.OnClickListener() {
//    				 @Override
//    				 public void onClick(DialogInterface dialog, int which) {
//                       submit0();
//    				 }
//    				}).setNegativeButton("いいえ", null)
//    				.setNeutralButton("はい",new DialogInterface.OnClickListener() {
//       				 @Override
//       				 public void onClick(DialogInterface dialog, int which) {
//                          submit0();
//       				 }
//       				}).create();
//    				dialog.show();
    	if (!mMailBean.concierge_id.equals(mAccountManager.concierge_id)&&Settings.get(Settings.PAY_CONFIRM_SEND_MAIL)) {
//	    	AlertDialog.Builder builder = new Builder(mActivity);
	    	String title_str="";
	    	String mail_ticket=mUserInfo.diagnosis_ticket;
	    	ZLog.d("TAG", "[mDiagnosisInfo] point: " + mDiagnosisInfo.point);
	    	if("0".equals(mail_ticket)){
//	    		title_str = "メールの送信に"+mConciergeInfo.chat_point+"kysを消費します。";
	    		title_str = "メールの送信に\n"+mDiagnosisInfo.point+"kysを消費します。";
//        		builder.setTitle(title_str);
	    	}
//	    	else if("".equals(mMailBean.conversation_id)){
//	    		title_str = "メールの送信に"+mDiagnosisInfo.point+"kysを消費します。";
//        		builder.setTitle(title_str);
//	    	}
	    	else{
	    		
	    		title_str = getString(R.string.hint_send_mail_need_ticket);
//	    		builder.setTitle(R.string.hint_send_mail_need_ticket);
	    	}
	    	
	    	DialogUtil.ShowSelectContent(mActivity, title_str, new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					int id = arg0.getId();
					if(id == R.id.dialog_btn1){
						submitSetting("setting_mail",false);
    	        		submit0();
					}else if(id == R.id.dialog_btn2){
						submit0();
					}
				}
			});
	    	
	    	//定义列表中的选项
//	    	final String[] items = new String[]{
//	    	        "はい、以降表示しない",
//	    	        "はい",
//	    	};
//	    	//设置列表选项
//	    	builder.setItems(items, new DialogInterface.OnClickListener() {
//	    	        //点击任何一个列表选项都会触发这个方法
//	    	        //arg1：点击的是哪一个选项
//	    	        @Override
//	    	        public void onClick(DialogInterface dialog, int which) {
//	    	        	if(items[which].equals("はい、以降表示しない")){
//	    	        		submitSetting("setting_mail",false);
//	    	        		submit0();
//	    	        	}
//	    	        	if(items[which].equals("はい")){
//	    	        		submit0();
//	    	        	}
//	    	        }
//	    	});
//	    	// 取消选择
//	    	builder.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
//	    	        @Override
//	    	        public void onClick(DialogInterface dialog, int which) {
//	    	        }
//	    	});
//	    	builder.show();
    	}else{
    		submit0();
    	}
    	
    }
    
    private void submit0() {
        if (!TextUtils.isEmpty(mMailBean.image_url)) {
            uploadPic();
        } else {
            submit("");
        }
    }

    private void uploadPic() {
        mNetManager.uploadPhoto(mMailBean.image_url, new Callback<UploadPhotoBean>() {
            @Override
            protected void onSucceed(UploadPhotoBean result) {
                submit(result.imageid);
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }

    private void submit(String imgurl) {
        mNetManager.sendMail(
                mMailBean.concierge_id,
                mMailBean.title,
                mMailBean.message,
                imgurl,
                mMailBean.conversation_id,
                mMailBean.diagnosis_id, new Callback<Void>() {
                    @Override
                    protected void onSucceed(Void result) {
                        dismissLoading();
                        toast(getString(R.string.hint_submit_success));

                        finishToMailList();
                    }

                    @Override
                    protected void onError(Object object) {
                        dismissLoading();
                    }
                });
        showLoading();
    }

    public void finishToMailList() {
        mActivity.sendBroadcast(new Intent(MailListFragment.FILTER_MAIL_NEED_REFRESH));

        mActivity.toMailList();
    }

    @OnClick(R.id.btn_cancel)
    void cancelClick() {
        finish();
    }

    @Bind(R.id.tv_mail_title)
    TextView mTitleTv;
    @Bind(R.id.tv_mail_content)
    TextView mContentTv;
    @Bind(R.id.iv_mail_camera)
    ImageView mCameraIv;
    @Bind(R.id.iv_photo)
    ImageView mPhotoIv;

    @OnClick(R.id.iv_photo)
    void onPicClick() {
        if (TextUtils.isEmpty(mMailBean.image_url)) return;

        Intent intent = new Intent();
        intent.setClass(mActivity, PicBrowseActivity.class);
        intent.putExtra("url", "file://" + mMailBean.image_url);
        startActivity(intent);
    }

    private MailBean mMailBean;
    private UserInfo mUserInfo;
    private Concierge mConciergeInfo;
    private Diagnosis mDiagnosisInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        mMailBean = (MailBean) bundle.getSerializable("mail");
        
        mNetManager.getUserInfo("1", new Callback<UserInfo>() {
            @Override
            protected void onSucceed(UserInfo result) {
                dismissLoading();

                mUserInfo = result;
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        mNetManager.getConciergeDetail(mMailBean.concierge_id, new Callback<Concierge>() {
            @Override
            protected void onSucceed(Concierge result) {
                dismissLoading();

                mConciergeInfo = result;
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        mNetManager.getDiagnosis(mMailBean.diagnosis_id, new Callback<Diagnosis>() {
            @Override
            protected void onSucceed(Diagnosis result) {
                dismissLoading();

                mDiagnosisInfo = result;
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        needBackFinish();

        setDatas();
    }

    private void setDatas() {
        mTitleTv.setText(mMailBean.title);
        mContentTv.setText(mMailBean.message);

        boolean hasAttachment = !TextUtils.isEmpty(mMailBean.image_url);
        mCameraIv.setImageResource(hasAttachment ?
                R.drawable.ico_camera_with_attachment : R.drawable.ic_title_camera);
        mPhotoIv.setVisibility(hasAttachment ? View.VISIBLE : View.GONE);
        String pic = "file://" + mMailBean.image_url;
        ZImageLoader.asyncLoadImage(pic, mPhotoIv);
    }
    private void submitSetting(final String key, final boolean value) {
        mNetManager.submitSetting(key, value, new Callback<Void>() {
            @Override
            protected void onSucceed(Void result) {
                dismissLoading();
                if ("setting_mail".equals(key)) {
                    Settings.save(Settings.PAY_CONFIRM_SEND_MAIL, value);
                } else if ("setting_message".equals(key)) {
                    Settings.save(Settings.PAY_CONFIRM_SEND_MESSAGE, value);
                } else if ("setting_image".equals(key)) {
                    Settings.save(Settings.PAY_CONFIRM_SEE_PICTURE, value);
                }
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }
}
