package jp.concierge.jetkys.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.RadioGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.OnClick;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.billing.util.IabHelper;
import jp.concierge.jetkys.android.billing.util.IabResult;
import jp.concierge.jetkys.android.billing.util.Purchase;

/**
 * Created by WilliZ on 16/03/31.
 */
public class BuyPointFragment extends BaseFragment {
	 // Debug tag, for logging
    static final String TAG = "TrivialDrive";
    
    IabHelper mHelper;
	
    @Override
    protected int layout() {
        return R.layout.fragment_buypoint;
    }

    @OnClick(R.id.btn_cancel)
    void cancelClick() {
        finish();
    }

    @Bind(R.id.rgroup_buypoint_items)
    RadioGroup mItemsRadioGroup;

    @OnClick(R.id.btn_submit)
    void buyClick() {
        String point = "";
        int checkedId = mItemsRadioGroup.getCheckedRadioButtonId();
        switch (checkedId) {
            case R.id.rb_buypoint_60:
                point = "skys240";
                break;
            case R.id.rb_buypoint_150:
                point = "skys600";
                break;
            case R.id.rb_buypoint_300:
                point = "skys1200";
                break;
            case R.id.rb_buypoint_450:
                point = "skys1800";
                break;
            case R.id.rb_buypoint_870:
                point = "skys3400";
                break;
            case R.id.rb_buypoint_1250:
                point = "skys4800";
                break;
            case R.id.rb_buypoint_2580:
                point = "skys9800";
                break;
        }

        if ("".equals(point)) return;
        
        mHelper.launchPurchaseFlow(getActivity(), point, 10001,   
        		   mPurchaseFinishedListener, "");
    }

    private void gotoBuy(String point) {
        toast("[" + point + "]");
    }

    @Bind(R.id.tv_point)
    TextView mPointTv;

    private String point;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        point = getArguments().getString("point");
        
     // ...
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoy+rVPPh1PcVOZJKIblFq5f0/nfEVN7tS0ON0oQplVT5X+G1Ft7MS+ifmLHLDI84d4vbm+QG/Y98Bj7ru1D4wwYLuqSaA8M1E7XTJjw8NGt+3M/yVwkpvjSB9LSnfgu5Ly3Srcggw7wahWnFYmzXo+F441at6DcFPno6FoOsiu+Sv0uuxl3AikYO9CX6sAHgIKMHOu6k1QCUTBH4VMDKb7GZtr/Zqgag5CRQDL88FWsCORU362EXIaODSadVA1VHmKWnWloIErsQcVcIeSpFfT0VzzIY8Ii1oDhqc115t5RkapuUUS4pIopkzFaI0eWDeJeZfJakTYS7KlgeU6JNVwIDAQAB";

        // compute your public key and store it in base64EncodedPublicKey
        mHelper = new IabHelper(getActivity(), base64EncodedPublicKey);
        
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
        	   public void onIabSetupFinished(IabResult result) {
        	      if (!result.isSuccess()) {
        	         // Oh noes, there was a problem.
        	         Log.d(TAG, "Problem setting up In-app Billing: " + result);
        	      }
        	         // Hooray, IAB is fully set up!
        	   }
        	});
    }
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    // We're being destroyed. It's important to dispose of the helper here!
    @Override
    public void onDestroy() {
        super.onDestroy();

        // very important:
        Log.d(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

    @Override
    protected void initViews(Bundle bundle) {
        super.initViews(bundle);

        mPointTv.setText(point);
    }
    
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
    public void onIabPurchaseFinished(IabResult result, Purchase purchase) 
    {
       int current_point = Integer.valueOf(mPointTv.getText().toString());
       
       if (result.isFailure()) {
          Log.d(TAG, "Error purchasing: " + result);
          gotoBuy("購入に失敗しました。");
          return;
       }      
       else if (purchase.getSku().equals("skys240")) {
          // consume the gas and update the UI
    	   gotoBuy("60kysを購入しました。");
    	   String new_point=String.valueOf(current_point+60);
    	   mPointTv.setText(new_point);
       }
       else if (purchase.getSku().equals("skys600")) {
           // consume the gas and update the UI
     	   gotoBuy("150kysを購入しました。");
		  String new_point=String.valueOf(current_point+150);
		  mPointTv.setText(new_point);
        }
       else if (purchase.getSku().equals("skys1200")) {
           // consume the gas and update the UI
     	   gotoBuy("300kysを購入しました。");
 		  String new_point=String.valueOf(current_point+300);
 		  mPointTv.setText(new_point);
        }
       else if (purchase.getSku().equals("skys1800")) {
           // consume the gas and update the UI
     	   gotoBuy("450kysを購入しました。");
 		  String new_point=String.valueOf(current_point+450);
 		  mPointTv.setText(new_point);
        }
       else if (purchase.getSku().equals("skys3400")) {
           // consume the gas and update the UI
     	   gotoBuy("870kysを購入しました。");
 		  String new_point=String.valueOf(current_point+870);
 		  mPointTv.setText(new_point);
        }
       else if (purchase.getSku().equals("skys4800")) {
           // consume the gas and update the UI
     	   gotoBuy("1250kysを購入しました。");
 		  String new_point=String.valueOf(current_point+1250);
 		  mPointTv.setText(new_point);
        }
       else if (purchase.getSku().equals("skys9800")) {
           // consume the gas and update the UI
     	   gotoBuy("2580kysを購入しました。");
 		  String new_point=String.valueOf(current_point+2580);
 		  mPointTv.setText(new_point);
        }
       else{
    	   gotoBuy("購入に失敗しました。");
    	   return;
       }
    }
 };
}
