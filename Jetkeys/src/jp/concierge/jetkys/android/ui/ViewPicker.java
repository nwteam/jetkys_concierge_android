package jp.concierge.jetkys.android.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.RadioGroup;
import android.widget.TextView;
import jp.concierge.jetkys.android.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by WilliZ on 16/03/20.
 */
public class ViewPicker {

    private static String[] YEARS = null;
    private static String[] MONTHS = {
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"
    };

    private static String[] BLOODS = {
            "A", "B", "O", "AB"
    };

    static {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);

        List<String> ylist = new ArrayList<>();
        while (year >= 1900) {
            ylist.add(String.valueOf(year));
            year--;
        }

        int ysize = ylist.size();
        YEARS = new String[ysize];
        for (int i = 0; i < ysize; i++) {
            YEARS[i] = ylist.get(i);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int index, String value);
    }

    public static AlertDialog showPickYear(Context context, final OnItemClickListener l) {
        if (l == null) throw new IllegalArgumentException("showPickYear() - OnItemClickListener CANNOT NULL");

        return new AlertDialog.Builder(context)
                .setItems(YEARS, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        l.onItemClick(which, YEARS[which]);
                    }
                })
                .show();
    }

    public static AlertDialog showPickMonth(Context context, final OnItemClickListener l) {
        if (l == null) throw new IllegalArgumentException("showPickMonth() - OnItemClickListener CANNOT NULL");

        return new AlertDialog.Builder(context)
                .setItems(MONTHS, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        l.onItemClick(which, MONTHS[which]);
                    }
                })
                .show();
    }

    public static AlertDialog showPickDate(Context context, int year, int month, final OnItemClickListener l) {
        if (l == null) throw new IllegalArgumentException("showPickDate() - OnItemClickListener CANNOT NULL");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);

        int lastDay = calendar.getActualMaximum(Calendar.DATE);
        final String[] DAYS = new String[lastDay];
        for (int i = 0; i < lastDay; i++) {
            DAYS[i] = String.valueOf(i + 1);
        }

        return new AlertDialog.Builder(context)
                .setItems(DAYS, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        l.onItemClick(which, DAYS[which]);
                    }
                })
                .show();
    }

    public static AlertDialog showPickBlood(Context context, final OnItemClickListener l) {
        if (l == null) throw new IllegalArgumentException("showPickBlood() - OnItemClickListener CANNOT NULL");

        return new AlertDialog.Builder(context)
                .setItems(BLOODS, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        l.onItemClick(which, BLOODS[which]);
                    }
                })
                .show();
    }

    public static String getGender(RadioGroup genderRadioGroup) {
        String gender = genderRadioGroup.getCheckedRadioButtonId() == R.id.rb_male ? "1" : "2";
        return gender;
    }

    public static String getBirth(TextView ytv, TextView mtv, TextView dtv) {
        String y = ytv.getText().toString();
        String m = mtv.getText().toString();
        String d = dtv.getText().toString();
        return y + "-" + m + "-" + d;
    }
}