package jp.concierge.jetkys.android.ui;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.concierge.jetkys.android.MainActivity;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.LoginBean;
import jp.concierge.jetkys.android.manager.AccountManager;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.push.ChatManager;
import jp.concierge.jetkys.android.push.NotificationManager;

import java.util.Calendar;

/**
 * Created by WilliZ on 16/03/28.
 */
public class RegisterView extends LinearLayout {

    public RegisterView(Context context) {
        super(context);
    }

    public RegisterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.fragment_register, this);

        ButterKnife.bind(this);
    }

    private MainActivity mActivity;
    private AccountManager mAccountManager;

    public void setInit(MainActivity activity, AccountManager manager) {
        mActivity = activity;
        mAccountManager = manager;

        initAllPick();
    }

    public void onDestory() {
    }

    @OnClick(R.id.btn_login)
    public void loginClick() {
        mActivity.showLogin();
    }

    @OnClick(R.id.btn_register)
    public void registerClick() {
        String gender = ViewPicker.getGender(mGenderRgroup);
        String name = mNameEt.getText().toString();
        String email = mEmailEt.getText().toString();
        String email2 = mEmailEt2.getText().toString();
        String pwd = mPwdEt.getText().toString();
        String birth = ViewPicker.getBirth(mYearTv, mMonthTv, mDateTv);

        if (mActivity.checkEmpty(name, "お名前")) {
            return;
        }
        if (mActivity.checkEmpty(email, "メールアドレス")) {
            return;
        }
        if (mActivity.checkEmpty(email2, "メールアドレス")) {
            return;
        }
        if (mActivity.checkEmpty(pwd, "パスワード")) {
            return;
        }

        mAccountManager.registerViaEmail(
                name, email, pwd, gender, birth,
                new Callback<LoginBean>() {
                    @Override
                    protected void onSucceed(LoginBean result) {
                        mActivity.dismissLoading();

                        mActivity.toast("登録成功しました");

                        mActivity.onLoginComplete();
                        mActivity.dismissLogin();
                    }

                    @Override
                    protected void onError(Object object) {
                        mActivity.dismissLoading();
                    }
                }
        );
        mActivity.showLoading();
    }

    @Bind(R.id.et_name)
    EditText mNameEt;
    @Bind(R.id.et_email)
    EditText mEmailEt;
    @Bind(R.id.et_email2)
    EditText mEmailEt2;
    @Bind(R.id.et_password)
    EditText mPwdEt;

    @Bind(R.id.rgroup_gender)
    RadioGroup mGenderRgroup;

    @OnClick(R.id.layout_pick_year)
    void pickYear() {
        ViewPicker.showPickYear(mActivity, new ViewPicker.OnItemClickListener() {
            @Override
            public void onItemClick(int index, String value) {
                if (!value.equals(mYearTv.getText().toString())) {
                    mDateTv.setText("1");
                }

                mYearTv.setText(value);
            }
        });
    }

    @OnClick(R.id.layout_pick_month)
    void pickMonth() {
        ViewPicker.showPickMonth(mActivity, new ViewPicker.OnItemClickListener() {
            @Override
            public void onItemClick(int index, String value) {
                if (!value.equals(mMonthTv.getText().toString())) {
                    mDateTv.setText("1");
                }

                mMonthTv.setText(value);
            }
        });
    }

    @OnClick(R.id.layout_pick_date)
    void pickDate() {
        int year = Integer.valueOf(mYearTv.getText().toString());
        int month = Integer.valueOf(mMonthTv.getText().toString());

        ViewPicker.showPickDate(mActivity, year, month, new ViewPicker.OnItemClickListener() {
            @Override
            public void onItemClick(int index, String value) {
                mDateTv.setText(value);
            }
        });
    }

    @Bind(R.id.tv_year)
    TextView mYearTv;
    @Bind(R.id.tv_month)
    TextView mMonthTv;
    @Bind(R.id.tv_date)
    TextView mDateTv;

    private void initAllPick() {
        Calendar calendar = Calendar.getInstance();
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH) + 1;
        int d = calendar.get(Calendar.DATE);

        mYearTv.setText("" + y);
        mMonthTv.setText("" + m);
        mDateTv.setText("" + d);
    }
}
