package jp.concierge.jetkys.android.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.Concierge;
import jp.concierge.jetkys.android.ui.BaseFragment;

/**
 * Created by WilliZ on 16/03/24.
 */
public class TopPagePeopleView extends RelativeLayout {
    public TopPagePeopleView(Context context) {
        super(context);
    }

    public TopPagePeopleView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    private void init() {
        inflate(getContext(), R.layout.item_top_people, this);
        ButterKnife.bind(this);
    }

    @Bind(R.id.iv_avatar)
    ImageView avatarIv;
    @Bind(R.id.tv_name)
    TextView nameTv;
    @Bind(R.id.tv_desc)
    TextView descTv;
    @Bind(R.id.tv_content)
    TextView contentTv;

    public void setConc(Concierge conc, BaseFragment f) {
//    	f.asyncLoadImage(conc.image, avatarIv);
        f.asyncLoadImageAvatar(conc.image, avatarIv,conc.getGender());

        nameTv.setText(conc.getName());
        descTv.setText(conc.capacity);
        contentTv.setText(conc.self_introduction);
    }
}
