package jp.concierge.jetkys.android.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

import java.util.List;

/**
 * Created by WilliZ on 16/03/28.
 */
public class InnerScrollListView extends ListView {

    public InnerScrollListView(Context context) {
        super(context);
    }

    public InnerScrollListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InnerScrollListView(Context context, AttributeSet attrs,
                               int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    /**
     * 重写该方法，达到使ListView适应ScrollView的效果
     */
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
