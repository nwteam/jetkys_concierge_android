package jp.concierge.jetkys.android.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

/**
 * Created by WilliZ on 16/03/19.
 */
public class PullListView extends PullToRefreshListView {
    public PullListView(Context context) {
        super(context);
    }

    public PullListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
