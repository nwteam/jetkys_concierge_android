package jp.concierge.jetkys.android.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import com.handmark.pulltorefresh.library.PullToRefreshExpandableListView;

/**
 * Created by WilliZ on 16/03/25.
 */
public class PullExpandListView extends PullToRefreshExpandableListView {
    public PullExpandListView(Context context) {
        super(context);
    }

    public PullExpandListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
