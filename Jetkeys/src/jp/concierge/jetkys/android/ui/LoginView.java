package jp.concierge.jetkys.android.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.concierge.jetkys.android.MainActivity;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.LoginBean;
import jp.concierge.jetkys.android.manager.AccountManager;
import jp.concierge.jetkys.android.net.ProtocolConstant;
import jp.concierge.jetkys.android.net.thread.Callback;

/**
 * Created by WilliZ on 16/03/28.
 */
public class LoginView extends LinearLayout {

    public LoginView(Context context) {
        super(context);
    }

    public LoginView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.fragment_login, this);

        ButterKnife.bind(this);
    }

    private MainActivity mActivity;
    private AccountManager mAccountManager;

    public void setInit(MainActivity activity, AccountManager manager) {
        mActivity = activity;
        mAccountManager = manager;
    }

    public void onDestory() {
    }
    
    @OnClick(R.id.btn_login)
    public void loginClick() {
        String name = mNameEt.getText().toString();
        String pwd = mPwdEt.getText().toString();
        boolean autoLogin = mAutoLoginCb.isChecked();

        if (mActivity.checkEmpty(name, "ユーザーID")) {
            return;
        }
        if (mActivity.checkEmpty(pwd, "パスワード")) {
            return;
        }

        mAccountManager.loginViaEmail(autoLogin, name, pwd, new Callback<LoginBean>() {
            @Override
            protected void onSucceed(LoginBean result) {
                mActivity.dismissLoading();

                mActivity.toast("ログイン成功しました");

                mActivity.onLoginComplete();
                mActivity.dismissLogin();
            }

            @Override
            protected void onError(Object object) {
                mActivity.dismissLoading();
            }
        });
        mActivity.showLoading();
    }

    @OnClick(R.id.btn_register)
    public void registerClick() {
        mActivity.moveToRegister();
    }

    @Bind(R.id.et_uid)
    EditText mNameEt;
    @Bind(R.id.et_password)
    EditText mPwdEt;

    @Bind(R.id.cb_login_auto)
    CheckBox mAutoLoginCb;

    @OnClick(R.id.tv_pwd_forget)
    void pwdForgetClick() {
        Uri uri = Uri.parse(ProtocolConstant.WEB_VIEW_URL+"pw_help");
        Intent it = new Intent(Intent.ACTION_VIEW, uri);
        getContext().startActivity(it);
    }

    @OnClick(R.id.tv_cant_login)
    void cantLoginClick() {
        Uri uri = Uri.parse(ProtocolConstant.WEB_VIEW_URL+"pw_help");
        Intent it = new Intent(Intent.ACTION_VIEW, uri);
        getContext().startActivity(it);
    }

    @OnClick(R.id.tv_help)
    void helpClick() {
        Uri uri = Uri.parse(ProtocolConstant.WEB_VIEW_URL+"help_a");
        Intent it = new Intent(Intent.ACTION_VIEW, uri);
        getContext().startActivity(it);
    }
    

    @OnClick(R.id.tv_google_plus_login)
    public void loginViaGooglePlusClick() {
    	mActivity.signInGoogleP();
    }

    @OnClick(R.id.tv_Twitter_login)
    public void loginViaTwitterClick() {
       mActivity.signinTwitter();
    }
    
    @OnClick(R.id.tv_facebook_login)
    public void loginViaFacebookClick() {
    	mActivity.signinFacebook();
    }
}
