package jp.concierge.jetkys.android.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.OnClick;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.Settings;
import jp.concierge.jetkys.android.beans.Diagnosis;
import jp.concierge.jetkys.android.beans.MailBean;
import jp.concierge.jetkys.android.beans.base.BaseDiagnosisListEntity;
import jp.concierge.jetkys.android.net.thread.Callback;

/**
 * Created by WilliZ on 16/03/21.
 */
public class DiagnosisFragment extends BaseFragment {
    @Override
    protected int layout() {
        return R.layout.fragment_diagnosis;
    }

    @OnClick(R.id.btn_submit)
    void submitClick() {
//        if (Settings.get(Settings.PAY_CONFIRM_SEND_MAIL)) {
//            new AlertDialog.Builder(mActivity)
//                    .setMessage(R.string.hint_send_mail_need_money)
//                    .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            toMail();
//                        }
//                    }).setNegativeButton(R.string.cancel, null).show();
//        } else {
//            toMail();
//        }
        toMail();
    }

    public void toMail() {
        if (mDiagnosis == null) return;

        MailBean mailBean = new MailBean();
        mailBean.diagnosis_id = mDiagnosis.id;
        mailBean.concierge_id = mDiagnosis.concierge_id;

        Bundle bundle = new Bundle();
        bundle.putBoolean("reply", true);
        bundle.putSerializable("mail", mailBean);

        MailDiagnosisEditFragment fragment = new MailDiagnosisEditFragment();
        fragment.setArguments(bundle);
        startFragment(fragment);
    }

    @Bind(R.id.tv_ident_name)
    TextView mIdentNameTv;
    @Bind(R.id.tv_ident_price)
    TextView mIdentPriceTv;
    @Bind(R.id.tv_member_ident_content)
    TextView mContentTv;
    @Bind(R.id.tv_price)
    TextView mPriceTv;
    @Bind(R.id.tv_ident_limit)
    TextView mLimitTv;

    private String id, limit;
    private Diagnosis mDiagnosis;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        id = getArguments().getString("id");
        limit = getArguments().getString("limit");
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        needBackFinish();

        setDatas();
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        mNetManager.getDiagnosis(id, new Callback<Diagnosis>() {
            @Override
            protected void onSucceed(Diagnosis result) {
                dismissLoading();

                mDiagnosis = result;
                setDatas();
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }

    private void setDatas() {
        if (mDiagnosis == null) return;

        mIdentNameTv.setText(mDiagnosis.title);
        mIdentPriceTv.setText(mDiagnosis.point + "kys");
        mContentTv.setText(mDiagnosis.message);
        mPriceTv.setText(mDiagnosis.point + "kys");
        mLimitTv.setText(limit + "時間以内");
    }
}
