package jp.concierge.jetkys.android.push;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import jp.concierge.jetkys.android.R;

/**
 * Created by WilliZ on 16/03/29.
 */
public class NotificationManager {

    public static void showNotification(Context context,
                                  String title,
                                  String content,
                                  PendingIntent pendingIntent) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setWhen(System.currentTimeMillis());
        mBuilder.setAutoCancel(true);
        mBuilder.setDefaults(NotificationCompat.DEFAULT_ALL);
        mBuilder.setSmallIcon(R.drawable.ic_launcher); // necessary
        // mBuilder.setTicker(content);
        mBuilder.setTicker("新着メッセージがありました");
        // mBuilder.setContentTitle(title);
        mBuilder.setContentTitle(context.getString(R.string.app_name));
        mBuilder.setContentText("新着メッセージがありました");
        mBuilder.setContentIntent(pendingIntent);

        Notification notification = mBuilder.build();
        notification.flags |= PendingIntent.FLAG_ONE_SHOT;

        android.app.NotificationManager notificationManager = (android.app.NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }
}
