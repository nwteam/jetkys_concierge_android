package jp.concierge.jetkys.android.push;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import jp.concierge.jetkys.android.MainActivity;
import jp.concierge.jetkys.android.beans.Message;
import jp.concierge.jetkys.android.ui.ChatFragment;

/**
 * Created by WilliZ on 16/03/29.
 */
public class ChatManager {
    public static final String FILTER_NEW_MSG_RECEIVED = "ChatManager.FILTER_NEW_MSG_RECEIVED";

    private static ChatManager INSTANCE = null;

    private ChatManager() {
    }

    public static ChatManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ChatManager();
        }
        return INSTANCE;
    }

    public MainActivity mMainActivity;
    public ChatFragment mChatFragment;

    public String conversationId = "";

    public void init(MainActivity activity) {
        mMainActivity = activity;
    }

    public void setChatFragment(ChatFragment fragment) {
        mChatFragment = fragment;
    }

    public void handleMessage(Context context, String notifTile, Message message) {
        if (mMainActivity == null) {
            // 主程序都没启动，点击则启动主程序
            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra("msg", message);
            PendingIntent pendingIntent = PendingIntent.getActivity(
                    context, 1, intent, PendingIntent.FLAG_ONE_SHOT
            );
            NotificationManager.showNotification(
                    context, "APP_NAME", "MSG_CONTENT", pendingIntent
            );
            return;
        }

        if (mChatFragment != null && conversationId.equals(message.conversation_id)) {
            // 如果正在和这个人聊天
//            sendMessage(context, message);
            mChatFragment.addMessage(message);
        } else {
            // 没有在和这个人聊天，包含如下情况
            // 1. ChatFragment 未启动
            // 2. ChatFragment 已经启动但不是这个人
            // 交给MainActivity处理，如果没启动则启动，启动了则先finish再重启
            Intent intent = new Intent(FILTER_NEW_MSG_RECEIVED);
            intent.putExtra("msg", message);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    context, 1, intent, PendingIntent.FLAG_ONE_SHOT
            );
            NotificationManager.showNotification(
                    context, "APP_NAME", "MSG_CONTENT", pendingIntent
            );
        }
    }

    private void sendMessage(Context context, Message message) {
        Intent intent = new Intent(ChatFragment.FILTER_CHAT_MSG_RECEIVER);
        intent.putExtra("msg", message);
        context.sendBroadcast(intent);
    }
}
