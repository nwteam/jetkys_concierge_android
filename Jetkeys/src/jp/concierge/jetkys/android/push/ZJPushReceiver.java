package jp.concierge.jetkys.android.push;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.Settings;
import jp.concierge.jetkys.android.SharedPreferenceHelper;
import jp.concierge.jetkys.android.beans.Message;
import jp.concierge.jetkys.android.manager.AccountManager;
import jp.concierge.jetkys.android.manager.ManagerFactory;
import jp.concierge.jetkys.android.utils.ZLog;
import org.json.JSONException;

import cn.jpush.android.api.JPushInterface;
import org.json.JSONObject;

public class ZJPushReceiver extends BroadcastReceiver {
    public static final String FILTER_JPUSH_RECEIVED = "ZJPushReceiver.FILTER_JPUSH_RECEIVED";
    public static final String TAG = ZJPushReceiver.class.getSimpleName();

    private AccountManager mAccountManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null) {
            return;
        }

        if (!Settings.get(Settings.MESSAGE_NOTIF)) return;

        mAccountManager = (AccountManager) ManagerFactory.getInstance().getManager(context, AccountManager.class);

        Bundle bundle = intent.getExtras();
        String action = intent.getAction();

        if (JPushInterface.ACTION_REGISTRATION_ID.equals(action)) {
            String registationId = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);
            ZLog.d("JPUSH regid=" + registationId);

            if (registationId == null) {
                return;
            }

            SharedPreferenceHelper.putString("jpush_regid", registationId);

            context.sendBroadcast(new Intent(ZJPushReceiver.FILTER_JPUSH_RECEIVED));
        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(action)) {
            // String extraExtra = bundle.getString(JPushInterface.EXTRA_EXTRA);
            String extraMessage = bundle.getString(JPushInterface.EXTRA_MESSAGE);
            String extraTitle = bundle.getString(JPushInterface.EXTRA_TITLE);

            // 规定：
            // article_repied = 自己的投稿被回复时
            // was_followed = 被别人设置为 “知り合い’”
            // new_msg_incoming = Message聊天里来新消息时
            // system_msg_incoming = 后台运营方发来的系统通知 “お知らせ” 时
            // String test =
            // "{\"type\":\"article_repied\",\"title\":\"新着メッセージがありました\",\"content\":\"A给b回复了\",\"time\":\"2014-11-04 18:08:33\"}";

                 String newExtraMsg = extraMessage.replaceAll("\\\\", "");
//            String newExtraMsg = extraMessage;

            ZLog.d("ZPUSHRECEIVER", "PUSH RECEIVE: " + newExtraMsg);

            if (TextUtils.isEmpty(mAccountManager.user_id)) {
                // user not login
                return;
            }

            try {
                parseMessage(context, extraTitle, newExtraMsg);
            } catch (JSONException e) {
                e.printStackTrace();
                // TODO somthing
            }
        }
    }

    private void parseMessage(Context context, String title, String extraMessage)
            throws JSONException {
        JSONObject object = new JSONObject(extraMessage);
//        String msg = object.optString("message");
//        String type = object.optString("type");
        String content = object.optString("content");
//
//        String notifTitle = context.getString(R.string.app_name);
//
//        if (type.equals("article_repied")) {
//            Intent pIntent = new Intent();
//            if (!ZActivityManager.getInstance().isAppRunning) {
//                // if app-ui is not running, run it
//                pIntent.setClass(context, MenuActivity.class);
//                // pIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                pIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            }
//
//            showNotification(context, notifTitle, title,
//                    PendingIntent.getActivity(context, 1, pIntent, PendingIntent.FLAG_ONE_SHOT));
//        } else if (type.equals("was_followed")) {
//            Intent pIntent = new Intent();
//            if (!ZActivityManager.getInstance().isAppRunning) {
//                // if app-ui is not running, run it
//                pIntent.setClass(context, MenuActivity.class);
//                // pIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                pIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            }
//
//            showNotification(context, notifTitle, title,
//                    PendingIntent.getActivity(context, 1, pIntent, PendingIntent.FLAG_ONE_SHOT));
//            // showNotification(context, notifTitle, title, null);
//        } else if (type.equals("new_msg_incoming")) {
//        parseChatJson(notifTitle, extraMessage, context);
//        } else if (type.equals("system_msg_incoming")) {
        parseChatJson(context, title, content);
//        }
    }

    private void parseChatJson(Context context, String title, String extraContent)
            throws JSONException {
        try {
            GsonBuilder gb = new GsonBuilder();
            gb.generateNonExecutableJson();
            Gson gson = gb.create();
            Message msg = gson.fromJson(extraContent, Message.class);

            ChatManager chatActivityManager = ChatManager.getInstance();
            chatActivityManager.handleMessage(context, title, msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}