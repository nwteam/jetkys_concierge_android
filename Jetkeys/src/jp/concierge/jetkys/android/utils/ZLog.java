package jp.concierge.jetkys.android.utils;

import android.util.Log;

public class ZLog {
	private static final String TAG_DEFAULT = "DEFAULT";

	public static boolean DEBUG = false;

	public static void d(String msg) {
		d(TAG_DEFAULT, msg);
	}

	public static void d(String tag, String msg) {
		Log.d(tag, msg);
		// System.err.println("LOG-D: " + msg);
	}

	public static void v(String msg) {
		v(TAG_DEFAULT, msg);
	}

	public static void v(String tag, String msg) {
		Log.v(tag, msg);
		// System.err.println("LOG-V: " + msg);
	}

	public static void e(String msg) {
		e(TAG_DEFAULT, msg);
	}

	public static void e(String tag, String msg) {
		Log.e(tag, msg);
		// System.err.println("LOG-E: " + msg);
	}
}
