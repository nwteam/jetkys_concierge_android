package jp.concierge.jetkys.android.utils;

import jp.concierge.jetkys.android.R;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View.OnClickListener;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class DialogUtil {

	public static void ShowSelectContent(Context mContext,String titleStr,final OnClickListener listener){
		final AlertDialog builder = new AlertDialog.Builder(mContext).create();
		builder.show();
		
		Window window = builder.getWindow();
		window.setContentView(R.layout.dialog_select_layout);
		
		TextView TitleTv = (TextView) window.findViewById(R.id.dialog_title);
		TitleTv.setText(titleStr);
		
		TextView textBtn1 = (TextView) window.findViewById(R.id.dialog_btn1);
		TextView textBtn2 = (TextView) window.findViewById(R.id.dialog_btn2);
		TextView textBtn3 = (TextView) window.findViewById(R.id.dialog_btn3);
		
		textBtn1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(listener != null){
					listener.onClick(arg0);
				}
				builder.dismiss();
			}
		});
		
	textBtn2.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					if(listener != null){
						listener.onClick(arg0);
					}
					builder.dismiss();
				}
			});

	textBtn3.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			if(listener != null){
				listener.onClick(arg0);
			}
			builder.dismiss();
		}
	});
		
	}
}
