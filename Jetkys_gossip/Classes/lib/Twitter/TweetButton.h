#ifndef __syanago__TweetButton__
#define __syanago__TweetButton__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "TweetObserver.h"

USING_NS_CC;

namespace Twitter {
class TweetButton : public ui::Button
{
public:
    TweetButton();
    static TweetButton* create(const std::string& message, const std::string& spriteFileName);
    void initialize(const std::string& message);
    void onTapTweetButton(Ref *sender, Widget::TouchEventType type);
private:
    std::string _message;
};
    
}











#endif /* defined(__syanago__TweetButton__) */
