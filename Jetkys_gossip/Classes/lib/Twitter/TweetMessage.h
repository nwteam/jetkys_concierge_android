#ifndef __syanago__TweetMessage__
#define __syanago__TweetMessage__

#include "cocos2d.h"

USING_NS_CC;

class TweetMessage
{
public:
    static std::string getScoreTweetMessage(const int masterCharacterIdOfHelp,const int courseId, const int clearTurn);
    static std::string getFuelTweetMessage(const int courseId, const int masterCharacterId);
    static std::string getFriendTweetMessage();
    static std::string getRankingTweetMessage(const int playerRank, const std::string rankingName);
};
#endif
