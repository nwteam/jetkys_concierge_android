#include "TweetMessage.h"
#include "PlayerController.h"
#include "CourseModel.h"
#include "CharacterModel.h"

std::string TweetMessage::getScoreTweetMessage(const int masterCharacterIdOfHelp, const int courseId, const int clearTurn)
{
    std::shared_ptr<CourseModel>courseModel(CourseModel::find(courseId));
    std::shared_ptr<CharacterModel>playerCharacterModel(CharacterModel::find(PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]->getCharactersId()));
    std::shared_ptr<CharacterModel>helpCharacterModel(CharacterModel::find(masterCharacterIdOfHelp));
    return StringUtils::format("【ベストスコア】%s さんが、「%s」とフレンドの「%s」の力を借りて、コースNo%d %s を %dターン でクリア！ #車なごスコア",
                               PLAYERCONTROLLER->_player->getPlayerName().c_str(),
                               playerCharacterModel->getCarName().c_str(),
                               helpCharacterModel->getCarName().c_str(),
                               courseModel->getNo(),
                               (courseModel->getName() + " " + courseModel->getDescription()).c_str(),
                               clearTurn);
}

std::string TweetMessage::getFuelTweetMessage(const int courseId, const int masterCharacterId)
{
    std::shared_ptr<CourseModel>courseModel(CourseModel::find(courseId));
    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(masterCharacterId));
    return StringUtils::format("【現在給油中】%s さんが、車なごコレクションでコースNo%d %s を「%s」ちゃんと走行しようとしてるよ！　#車なごヘルプ",
                               PLAYERCONTROLLER->_player->getPlayerName().c_str(),
                               courseModel->getNo(),
                               (courseModel->getName() + "" + courseModel->getDescription()).c_str(),
                               characterModel->getCarName().c_str());
}

std::string TweetMessage::getFriendTweetMessage()
{
    return StringUtils::format("【フレンド募集】%s さんが、車なごコレクションをプレイしてるよ！フレンドIDは「%s」です！車なごコレクション公式サイト→syanago.com #車なごフレンド",
                               PLAYERCONTROLLER->_player->getPlayerName().c_str(),
                               PLAYERCONTROLLER->_player->getContactCode().c_str());
}

std::string TweetMessage::getRankingTweetMessage(const int playerRank, const std::string rankingName)
{
    return StringUtils::format("【ランキング挑戦中！】%s さんが、 %s に参加！現在ランキング%d位！ #車なごイベント",
                               PLAYERCONTROLLER->_player->getPlayerName().c_str(),
                               rankingName.c_str(),
                               playerRank);
}