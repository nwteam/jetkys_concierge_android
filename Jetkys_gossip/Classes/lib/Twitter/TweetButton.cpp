#include "TweetButton.h"
#include "Tweet.h"
#include "SoundHelper.h"
using namespace Twitter;

TweetButton::TweetButton():
    _message("") {}

TweetButton* TweetButton::create(const std::string& message, const std::string& file)
{
    TweetButton* btn = new (std::nothrow) TweetButton();
    if (btn && btn->init(file)) {
        btn->initialize(message);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void TweetButton::initialize(const std::string& message)
{
    _message = message;
    addTouchEventListener(CC_CALLBACK_2(TweetButton::onTapTweetButton, this));
}

void TweetButton::onTapTweetButton(Ref* sender, Widget::TouchEventType type)
{
    if (type == TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        Tweet::openTweetDialog(_message.c_str());
    }
}







