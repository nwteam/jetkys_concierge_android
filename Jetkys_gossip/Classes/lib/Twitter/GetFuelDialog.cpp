#include "GetFuelDialog.h"
#include "FontDefines.h"
#include "SoundHelper.h"


bool GetFuelDialog::init(const TweetObserver::TweetAndGetFuelEndCallback& callbackForTweet, const onTapByTokenButtonCallback& callBackForToken, std::string tweet, std::string title, std::string contentText)
{
    if (!Layer::init()) {
        return false;
    }
    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    Size winSize = Director::getInstance()->getWinSize();

    auto black = LayerColor::create(Color4B(Color3B::BLACK));
    black->setOpacity(180);
    addChild(black);

    auto backGround = Sprite::create("get_fuel_dialog_base.png");
    backGround->setPosition(Point(winSize.width / 2, winSize.height / 2));
    addChild(backGround);

    showByTweetButton(backGround, GetFuelTweetButton::create(tweet, callbackForTweet));
    showByTokenButton(backGround, callBackForToken);
    showCloseButton(backGround);

    return true;
}

void GetFuelDialog::showByTweetButton(Sprite* backGround, GetFuelTweetButton* button)
{
    button->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    button->setPosition(Vec2(backGround->getContentSize() / 2) + Vec2(20, -60));
    backGround->addChild(button);
}



void GetFuelDialog::showCloseButton(Sprite* backGround)
{
    auto closeButton = ui::Button::create("gacha_dialog_button.png");
    closeButton->addTouchEventListener(CC_CALLBACK_2(GetFuelDialog::onTouchCloseButton, this));
    closeButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    closeButton->setPosition(Vec2(backGround->getContentSize().width / 2, 28));
    auto closeLabel = Label::createWithTTF("閉じる", FONT_NAME_2, 28);
    closeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    closeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    closeLabel->setPosition(Vec2(closeButton->getContentSize() / 2) - Vec2(0, 14));
    closeButton->addChild(closeLabel);
    backGround->addChild(closeButton);
}

void GetFuelDialog::showByTokenButton(Sprite* backGround, const onTapByTokenButtonCallback& callBackForToken)
{
    auto byTokenButton = ui::Button::create("get_fuel_token_button.png");
    byTokenButton->addTouchEventListener(callBackForToken);
    byTokenButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    byTokenButton->setPosition(Vec2(backGround->getContentSize() / 2) + Vec2(-20, -60));
    backGround->addChild(byTokenButton);
}

void GetFuelDialog::onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        getEventDispatcher()->removeEventListener(_listener);
        removeFromParent();
    }
}














