#include "Tweet.h"
#include "platform/android/jni/JniHelper.h"

using namespace Twitter;

void Tweet::openTweetDialog(const char* tweet)
{
	JniMethodInfo t;
	if (JniHelper::getStaticMethodInfo(
			t,
			"com/syanago/twitter/Tweet",
			"openTweetDialog",
			"(Ljava/lang/String;)V")) {
		t.env->CallStaticVoidMethod(t.classID, t.methodID, t.env->NewStringUTF(tweet));
		t.env->DeleteLocalRef(t.classID);
	}
}

void Tweet::openTweetDialog(const char* tweet, TweetObserver* observer)
{
	JniMethodInfo t;
	if (JniHelper::getStaticMethodInfo(
				t,
				"com/syanago/twitter/Tweet",
				"openTweetDialog2",
				"(Ljava/lang/String;Lcom/syanago/twitter/TweetListener;)V")) {
		JniMethodInfo f;
		if(JniHelper::getMethodInfo(f, "com/syanago/twitter/TweetListener", "<init>", "(J)V")){
			jobject listener = f.env->NewObject(f.classID, f.methodID, (unsigned long long) observer);
			t.env->CallStaticVoidMethod(t.classID, t.methodID, t.env->NewStringUTF(tweet), listener);
		}
	}

}
















