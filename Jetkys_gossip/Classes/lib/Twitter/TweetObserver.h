#ifndef __syanago__TweetObserver__
#define __syanago__TweetObserver__

#include <functional>
#include "RequestAPI.h"
#include "DefaultProgress.h"

using namespace SyanagoAPI;

namespace Twitter {
    
class TweetObserver
{
public:
    typedef std::function<void(void)> TweetAndGetFuelEndCallback;
    TweetObserver(const TweetAndGetFuelEndCallback& callback);
    ~TweetObserver();
    
    void onCancelled();
    void onSendPushed();
    void onDismissed();
    void onSuccess();
    
private:
    void onResponseRequestGetFuel(Json* json);
    
    TweetAndGetFuelEndCallback _callback;
    bool send;
    bool cancel;
    RequestAPI* request;
    DefaultProgress* progress;
};

}


#endif /* defined(__syanago__TweetObserver__) */
