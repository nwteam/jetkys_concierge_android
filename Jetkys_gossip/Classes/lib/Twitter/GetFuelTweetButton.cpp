#include "GetFuelTweetButton.h"
#include "Tweet.h"
#include "PlayerController.h"
#include "SoundHelper.h"

using namespace Twitter;

const std::string GetFuelTweetButton::FILE_NAME = "get_fuel_tweet_button.png";
const std::string GetFuelTweetButton::FILE_NAME_OFF = "get_fuel_tweet_button_off.png";

GetFuelTweetButton::GetFuelTweetButton(const std::string& message, const TweetObserver::TweetAndGetFuelEndCallback& callback):
    _message(message)
    , _observer(nullptr)
{
    _observer = new TweetObserver(callback);
}

GetFuelTweetButton::~GetFuelTweetButton()
{
    delete _observer;
}

GetFuelTweetButton* GetFuelTweetButton::create(const std::string& message, const TweetObserver::TweetAndGetFuelEndCallback& callback)
{
    GetFuelTweetButton* btn = new (std::nothrow) GetFuelTweetButton(message, callback);
    if (btn && btn->init(FILE_NAME, FILE_NAME_OFF, FILE_NAME_OFF, TextureResType::LOCAL)) {
        btn->initialize();
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void GetFuelTweetButton::initialize()
{
    // for debug
    // if(true){
    if (PLAYERCONTROLLER->_player->getFuelTweetCount() > 0) {
        addTouchEventListener(CC_CALLBACK_2(GetFuelTweetButton::onTapTweetButton, this));
    } else {
        setBright(false);
        setTouchEnabled(false);
    }
}

void GetFuelTweetButton::onTapTweetButton(Ref* sender, Widget::TouchEventType type)
{
    if (type == TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        Tweet::openTweetDialog(_message.c_str(), _observer);
    }
}