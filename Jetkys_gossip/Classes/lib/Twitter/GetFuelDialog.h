#ifndef __syanago__GetFuelDialog__
#define __syanago__GetFuelDialog__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "TweetObserver.h"
#include "create_func.h"
#include "GetFuelTweetButton.h"
#include <functional>

USING_NS_CC;
using namespace Twitter;

class GetFuelDialog : public Layer, public create_func<GetFuelDialog>
{
public:
    using create_func::create;
    typedef std::function<void(Ref*, ui::Widget::TouchEventType)>onTapByTokenButtonCallback;
    bool init(const TweetObserver::TweetAndGetFuelEndCallback& callbackForTweet, const onTapByTokenButtonCallback& callBackForToken, std::string tweet, std::string title, std::string contentText);
    
   
private:
    void showByTweetButton(Sprite* backGround, GetFuelTweetButton* button);
    void showByTokenButton(Sprite* backGround, const onTapByTokenButtonCallback& callBackForToken);
    void showCloseButton(Sprite* backGround);
    
    void onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type);
    
    EventListenerTouchOneByOne* _listener;
};



#endif /* defined(__syanago__GetFuelDialog__) */
