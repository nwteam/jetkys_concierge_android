#include "TweetObserver.h"
#include "PlayerController.h"

using namespace Twitter;
using namespace SyanagoAPI;

TweetObserver::TweetObserver(const TweetAndGetFuelEndCallback& callback):
    cancel(false)
    , send(false)
    , _callback(callback)
{
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
}

TweetObserver::~TweetObserver()
{
    delete progress;
    delete request;
}

void TweetObserver::onCancelled()
{
    cancel = true;
    send = false;
    CCLOG("[TweetObserver::onCancelled()]");
}

void TweetObserver::onSendPushed()
{
    cancel = false;
    send = true;
    CCLOG("[TweetObserver::onSendPushed()]");
    onSuccess();
};

void TweetObserver::onDismissed()
{
    CCLOG("[TweetObserver::onDismissed()");
    if (send && !cancel) {
        // onSuccess();
    }
}

void TweetObserver::onSuccess()
{
    request->getFuel(PLAYERCONTROLLER->_player->getRank(), PLAYERCONTROLLER->_player->getFuel(), CC_CALLBACK_1(TweetObserver::onResponseRequestGetFuel, this));
}

void TweetObserver::onResponseRequestGetFuel(Json* json)
{
    Json* result = Json_getItem(json, "log_action");
    if (result) {
        int fuel = atoi(Json_getString(Json_getItem(result, "data"), "fuel", "0"));
        if (fuel != 0) {
            PLAYERCONTROLLER->_player->setFuel(fuel);
            PLAYERCONTROLLER->_player->setFuelTweetCount(0);
        }
    }
    _callback();
}



