#ifndef __syanago__GetFuelTweetButton__
#define __syanago__GetFuelTweetButton__
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "TweetObserver.h"

USING_NS_CC;

namespace Twitter {
    class GetFuelTweetButton : public ui::Button
    {
    public:
        GetFuelTweetButton(const std::string& message, const TweetObserver::TweetAndGetFuelEndCallback& callback);
        ~GetFuelTweetButton();
        static const std::string FILE_NAME;
        static const std::string FILE_NAME_OFF;
        static GetFuelTweetButton* create(const std::string& message, const TweetObserver::TweetAndGetFuelEndCallback& callback);
        void initialize();
        void onTapTweetButton(Ref *sender, Widget::TouchEventType type);
    private:
        TweetObserver* _observer;
        std::string _message;
    };
    
}




#endif /* defined(__syanago__GetFuelTweetButton__) */
