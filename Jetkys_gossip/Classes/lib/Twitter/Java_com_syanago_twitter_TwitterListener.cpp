#import "cocos2d.h"
#import "TweetObserver.h"

USING_NS_CC;
using namespace Twitter;

extern "C"
{
	JNIEXPORT void JNICALL Java_com_syanago_twitter_TweetListener_onNativeSuccess(JNIEnv* env, jobject thiz, jlong delegate);
};

JNIEXPORT void JNICALL Java_com_syanago_twitter_TweetListener_onNativeSuccess(JNIEnv* env, jobject thiz, jlong delegate)
{
	reinterpret_cast<TweetObserver*>(delegate)->onSuccess();
}
