#ifndef syanago_Tweet_h
#define syanago_Tweet_h

#include "TweetObserver.h"

namespace Twitter {
    
class Tweet {
public:
    static void openTweetDialog(const char* tweet);
    static void openTweetDialog(const char* tweet, TweetObserver* observer);
};
}


#endif
