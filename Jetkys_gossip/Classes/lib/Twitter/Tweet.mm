#include "Tweet.h"
#import <Social/Social.h>

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    #import "UIAlertView+BlocksExtension.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#endif

using namespace Twitter;
void Tweet::openTweetDialog(const char* tweet)
{
    Tweet::openTweetDialog(tweet, nullptr);
};


void Tweet::openTweetDialog(const char* tweet, TweetObserver* observer)
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        UIViewController* myViewController = [UIApplication sharedApplication].keyWindow.rootViewController;

        SLComposeViewController* tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];

        [tweetSheet setInitialText: [NSString stringWithUTF8String:tweet]];

        if (observer != nullptr) {
            tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
                switch (result) {
                case SLComposeViewControllerResultCancelled:
                    observer->onCancelled();
                    break;
                case SLComposeViewControllerResultDone:
                    observer->onSendPushed();
                    break;
                }
                [myViewController dismissViewControllerAnimated:YES completion:^{
                     observer->onDismissed();
                 }];
            };
        }
        [myViewController presentViewController:tweetSheet animated:YES completion:^{
             NSLog(@"Tweet sheet has been presented.");
         }];
    } else {
        [[[[UIAlertView alloc]
           initWithTitle:@"アカウントが設定されていません"
           message:@"iOSの「設定」でTwitterアカウントを設定してください"
           callback:^(NSInteger buttonIndex) {
               if ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter:"]] == false) {
                   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://appsto.re/jp/NwV5t.i"]];
               }
           }
           cancelButtonTitle:@"OK"
           otherButtonTitles:nil, nil]
          autorelease]
         show];
    }
}

