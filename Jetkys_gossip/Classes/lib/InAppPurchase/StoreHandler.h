#ifndef syanago_StoreHandler_h
#define syanago_StoreHandler_h

enum StoreHandlerState
{
    STORE_STATE_NONE = 0,
    STORE_STATE_FINISH,
    STORE_STATE_RESTORED,
    STORE_STATE_CANCEL,
    STORE_STATE_ERROR,
    STORE_STATE_DIFERRED,
    STORE_STATE_MAX,
};

class StoreHandler
{
public:
    bool canMakePayments();
    static void setCallBack(StoreHandler* callback);
    
    StoreHandler();
    virtual ~StoreHandler();
    virtual void callBackFunc(int state, const char* productId, const char* Id, const char* receipt);
    virtual void purchaseEndCallBackFunc();
    
    void productRequestStart(const char* productId);
    
    void finishLastTransaction();
    
    void applicationDidEnterBackground();
    void applicationWillEnterForeground();
    
    
    static const char* urlEncode(const char* encodeStr);
    static const char* urlDecode(const char* decodeStr);
    static void setStartingUp(bool sw);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    static StoreHandler* getCallbackObj();
#endif
};

#endif
