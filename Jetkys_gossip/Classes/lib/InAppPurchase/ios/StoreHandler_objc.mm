#import "StoreHandler_objc.h"
#import "AppController.h"

iOstype* myCallback = nil;
static bool startingUp = false;
SKPaymentTransaction* lastTransaction = nil;
NSString* productIdentifire = @"";

@implementation StoreHandler_objc

+ (StoreHandler_objc*)getInstance
{
    static StoreHandler_objc* _instance = nil;
    @synchronized(self) {
        if (_instance == nil) {
            _instance = [[StoreHandler_objc alloc] init];
        }
    }
    return _instance;
}

-(bool)canMakePayments
{
    return [SKPaymentQueue canMakePayments];
}

-(void)setCallBack:(iOstype*)callBack
{
    myCallback = callBack;
}

-(void)productRequestStart:(NSString*)productId
{
    productIdentifire = productId;
    [StoreHandler_objc setSrartingUp:false];
    NSSet* productIds = [NSSet setWithObject:productId];
    SKProductsRequest* skProductsRequest;
    skProductsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIds];
    skProductsRequest.delegate = self;
    [skProductsRequest start];
}

+(void)setSrartingUp:(bool)sw
{
    startingUp = sw;
}

-(NSString*)getProductIdentifire
{
    return productIdentifire;
}

-(void)applicationDidEnterBackground
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

-(void)applicationWillEnterForeground
{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
}

-(void)productsRequest:(SKProductsRequest*)request didReceiveResponse:(SKProductsResponse*)response
{
    if ([response.invalidProductIdentifiers count] > 0) {
        for (NSString* identifier in response.invalidProductIdentifiers) {
            #ifdef DEBUG
                printf("inbalid %s\n", [identifier UTF8String]);
            #endif
        }
        if (myCallback != NULL) {
            myCallback->callBackFunc(STORE_STATE_ERROR, "", "", "");
        }
        return;
    }

    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    for (SKProduct* product in response.products) {
        SKPayment* payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
}

-(void)finishLastTransaction
{
    if (lastTransaction != nil) {
        [[SKPaymentQueue defaultQueue] finishTransaction:lastTransaction];
        lastTransaction = nil;
        productIdentifire = @"";
    }
    if (myCallback != NULL) {
        myCallback->purchaseEndCallBackFunc();
    }
}

-(void)paymentQueue:(SKPaymentQueue*)queue updatedTransactions:(NSArray*)transactions
{
    for (SKPaymentTransaction* transaction in transactions) {
        if (transaction.transactionState == SKPaymentTransactionStatePurchasing) {} else if (transaction.transactionState == SKPaymentTransactionStatePurchased) {
            @try
            {
                lastTransaction = transaction;
                myCallback->callBackFunc(transaction.transactionState,
                                         [transaction.payment.productIdentifier UTF8String],
                                         [transaction.transactionIdentifier UTF8String],
                                         [transaction.transactionReceipt.base64Encoding UTF8String]);
            }
            @catch (NSException* exception) {}
        } else if (transaction.transactionState == SKPaymentTransactionStateFailed) {
            @try
            {
                int errcode = transaction.error.code;
                switch (errcode) {
                case SKErrorUnknown: {
                    break;
                }
                case SKErrorPaymentCancelled: {
                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                    break;
                }
                default: {
                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                    break;
                }
                }
            }
            @catch (NSException* exception)
            {}
        } else if (transaction.transactionState == SKPaymentTransactionStateDeferred) {} else if (transaction.transactionState == SKPaymentTransactionStateRestored) {
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        } else {}
    }
}

-(void) paymentQueue:(SKPaymentQueue*)queue removedTransactions:(NSArray*)transactions
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [[SKPaymentQueue defaultQueue]restoreCompletedTransactions];
}

@end