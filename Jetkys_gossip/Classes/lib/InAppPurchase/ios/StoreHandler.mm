#include "StoreHandler.h"
#include "StoreHandler_objc.h"

static StoreHandler_objc* myStoreHandler_objc = nil;

StoreHandler::StoreHandler()
{
    myStoreHandler_objc = [StoreHandler_objc getInstance];
}

bool StoreHandler::canMakePayments()
{
    return myStoreHandler_objc.canMakePayments;
}

void StoreHandler::setCallBack(StoreHandler* callback)
{
    [myStoreHandler_objc setCallBack:callback];
}

void StoreHandler::productRequestStart(const char* productId)
{
    [myStoreHandler_objc productRequestStart:[NSString stringWithCString: productId encoding:NSUTF8StringEncoding]];
}

void StoreHandler::finishLastTransaction()
{
    [myStoreHandler_objc finishLastTransaction];
}

void StoreHandler::applicationDidEnterBackground()
{
    [myStoreHandler_objc applicationDidEnterBackground];
}

void StoreHandler::applicationWillEnterForeground()
{
    [myStoreHandler_objc applicationWillEnterForeground];
}

void StoreHandler::setStartingUp(bool sw)
{
    [StoreHandler_objc setStartingUp:sw];
}

const char* StoreHandler::urlEncode(const char* encodeStr)
{
    NSString* str = [NSString stringWithCString:encodeStr encoding:NSUTF8StringEncoding];
    NSString* encoded = [(NSString*)CFURLCreateStringByAddingPercentEscapes (NULL,
                                                                             (CFStringRef)str, NULL,
                                                                             (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8)
                         autorelease];
    return [encoded UTF8String];
}

const char* StoreHandler::urlDecode(const char* decodeStr)
{
    NSString* str = [NSString stringWithCString:decodeStr encoding:NSUTF8StringEncoding];
    NSString* decoded = [(NSString*)CFURLCreateStringByReplacingPercentEscapesUsingEncoding (NULL,
                                                                                             (CFStringRef)str, CFSTR(""), kCFStringEncodingUTF8)
                         autorelease];
    return [decoded UTF8String];
}

StoreHandler::~StoreHandler() {}
void StoreHandler::callBackFunc(int state, const char* productId, const char* Id, const char* receipt) {}
void StoreHandler::purchaseEndCallBackFunc() {}
