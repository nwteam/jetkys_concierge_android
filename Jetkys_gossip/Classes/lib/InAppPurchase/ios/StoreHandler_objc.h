#ifndef syanago_StoreHandler_objc_h
#define syanago_StoreHandler_objc_h

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

#import "StoreHandler.h"

typedef struct StoreHandler iOstype;

@interface StoreHandler_objc : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver>
{}

+(StoreHandler_objc*)getInstance;
+(void)setSrartingUp:(bool)sw;

-(void)finishLastTransaction;

-(void)applicationDidEnterBackground;
-(void)applicationWillEnterForeground;

-(bool)canMakePayments;
-(void)setCallBack:(iOstype*)callBack;
-(void)productRequestStart:(NSString*)productId;
-(NSString*)getProductIdentifire;

@end
#endif
