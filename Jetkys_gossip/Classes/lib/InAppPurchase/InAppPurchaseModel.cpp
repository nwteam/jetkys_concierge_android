#include "InAppPurchaseModel.h"
#include "PlayerController.h"
#include "base64.h"
#include <string.h>
#include "PlayerController.h"
#include "Native.h"
#include "ArtLtvMeasurement.h"
#include "MeasurementInformation.h"
#include <string.h>

void InAppPurchaseModel::buyProduct(const std::string productId, const int placeId, std::vector<std::shared_ptr<const Token> >token, const OnResponceCallback& successCallback, const OnResponceCallback& errorCallback)
{
    _token = token;
    _placeId = placeId;
    successReserveCallback = successCallback;
    errorReserveCallback = errorCallback;

    if (storeHandler == nullptr) {
        storeHandler = new StoreHandler();
    }
    if (progress == nullptr) {
        progress = new DefaultProgress();
    }
    if (progress == nullptr) {
        request = new RequestAPI(progress);
    }
    storeHandler->setCallBack(this);
    if (storeHandler->canMakePayments() != true) {
        CCLOG("[InAppPurchaseModel::buyProduct]課金不可");
        return;
    }
    progress->onStart();
    storeHandler->productRequestStart(productId.c_str());
}

void InAppPurchaseModel::callBackFunc(int state, const char* productId, const char* inAppPurchaseId, const char* receipt)
{
    if (state != STORE_STATE_FINISH) {
        CCLOG("[InAppPurchaseModel::callBackFunc]課金が完了してません state:%d", state);
        purchaseErrorCallBack();
        return;
    }

    if (_token.size() < 1) {
        CCLOG("[InAppPurchaseModel::callBackFunc]アプリ内に商品情報がありません");
        if (_productInfomation != nullptr) {
            _productInfomation = nullptr;
        }
        std::shared_ptr<PurchaseMolde>_productInfomation(new PurchaseMolde(state, productId, inAppPurchaseId, receipt));
        if (progress != nullptr) {
            progress = new DefaultProgress();
            request = new RequestAPI(progress);
        }
        request->getPayTokenItem(CC_CALLBACK_1(InAppPurchaseModel::responceItemList, this));
        return;
    }

    int ownerRank = 0;
    if (PLAYERCONTROLLER->_player != NULL) {
        ownerRank = PLAYERCONTROLLER->_player->getRank();
    }

    std::string clientIp = "";

    const int tokenDataNumber = searchTokenWithProductId(productId);
    if (tokenDataNumber < 0) {
        CCLOG("[InAppPurchaseModel::callBackFunc]一致するproductIdが有りません productId:%s", productId);
        purchaseErrorCallBack();
        return;
    }

    #if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
        request->checkAppStoreReceipt(CC_CALLBACK_1(InAppPurchaseModel::boughtProduct, this), ownerRank, 10000, _token.at(tokenDataNumber)->getQuantity(), _token.at(tokenDataNumber)->getQuantityFree(), 100, _token.at(tokenDataNumber)->getPriceYen(), inAppPurchaseId, clientIp, receipt);
    #else
        request->checkGooglePlayReceipt(CC_CALLBACK_1(InAppPurchaseModel::boughtProduct, this), ownerRank, 10000, _token.at(tokenDataNumber)->getQuantity(), _token.at(tokenDataNumber)->getQuantityFree(), _token.at(tokenDataNumber)->getPriceYen(), _placeId, receipt, inAppPurchaseId);
    #endif
};

void InAppPurchaseModel::responceItemList(Json* responce)
{
    _token.clear();
    std::string result = Json_getString(Json_getItem(responce, "get_pay_item"), "result", "false");
    if (result == "true") {
        Json* mDatas = Json_getItem(Json_getItem(Json_getItem(responce, "get_pay_item"), "data"), "pay_items");
        if (mDatas && mDatas->type == Json_Array) {
            Json* child;
            for (child = mDatas->child; child; child = child->next) {
                std::shared_ptr<Token>model(new Token(child));
                _token.push_back(model);
            }
        }
    }
    callBackFunc(_productInfomation->getStatus(), _productInfomation->getProductId().c_str(), _productInfomation->getInAppPurchaseId().c_str(), _productInfomation->getReceipt().c_str());
}

int InAppPurchaseModel::searchTokenWithProductId(const std::string productId)
{
    for (int i = 0; i < _token.size(); i++) {
        if (_token.at(i)->getProductId() == productId) {
            return i;
        }
    }
    return -1;
}

void InAppPurchaseModel::boughtProduct(Json* responce)
{
    Json* data = convertJsonOfPurchaseToken(responce);
    int pay_token = atoi(Json_getString(data, "pay_token", "-1"));
    int free_token = atoi(Json_getString(data, "free_token", "-1"));
    int buyToken = pay_token + free_token - PLAYERCONTROLLER->_player->getPayToken() - PLAYERCONTROLLER->_player->getFreeToken();
    PLAYERCONTROLLER->_player->setPayToken(pay_token);
    PLAYERCONTROLLER->_player->setFreeToken(free_token);
    sendBoughtMeasurementInformation(buyToken);
    storeHandler->finishLastTransaction();
}

Json* InAppPurchaseModel::convertJsonOfPurchaseToken(Json* responce)
{
    #if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
        if (strcmp(Json_getString(Json_getItem(responce, "check_appstore_receipt"), "result", "false"), "true") == 0) {
            Json* result = Json_getItem(Json_getItem(responce, "check_appstore_receipt"), "data");
            if (strcmp(Json_getString(result, "access", ""), "OK") != 0 || atoi(Json_getString(result, "status", "-1")) != 0) {
                CCLOG("[InAppPurchaseModel::convertJsonOfPurchaseToken]JsonDataエラー(access:%s, status:%d", Json_getString(result, "access", ""), atoi(Json_getString(result, "status", "-1")));
                return Json_create("");
            }
            return result;
        }
    #else
        if (strcmp(Json_getString(Json_getItem(responce, "log_googleplay_purchase"), "result", "false"), "true") == 0) {
            return Json_getItem(Json_getItem(responce, "log_googleplay_purchase"), "data");
        }
    #endif
    return Json_create("");
}

void InAppPurchaseModel::sendBoughtMeasurementInformation(const int boughtToken)
{
    if (boughtToken > 0) {
        ArtLtvMeasurement::send(MEASURMENT_INFO::ART::DPU, (int)PLAYERCONTROLLER->_player->getID());           // Art DPU
        ArtLtvMeasurement::send(MEASURMENT_INFO::ART::MPU, (int)PLAYERCONTROLLER->_player->getID());           // Art MPU
    }

    switch (boughtToken) {
    case 180:
        ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_TOKEN_180, (int)PLAYERCONTROLLER->_player->getID());
        break;
    case 95:
        ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_TOKEN_95, (int)PLAYERCONTROLLER->_player->getID());
        break;
    case 56:
        ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_TOKEN_56, (int)PLAYERCONTROLLER->_player->getID());
        break;
    case 27:
        ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_TOKEN_27, (int)PLAYERCONTROLLER->_player->getID());
        break;
    case 13:
        ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_TOKEN_13, (int)PLAYERCONTROLLER->_player->getID());
        break;
    case 6:
        ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_TOKEN_6, (int)PLAYERCONTROLLER->_player->getID());
        break;
    case 1:
        ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_TOKEN_1, (int)PLAYERCONTROLLER->_player->getID());
        break;
    default:
        break;
    }
}

void InAppPurchaseModel::purchaseEndCallBackFunc()
{
    if (progress != nullptr) {
        progress->onEnd();
    }
    if (successReserveCallback != nullptr) {
        successReserveCallback();
    }
}

void InAppPurchaseModel::purchaseErrorCallBack()
{
    if (progress != nullptr) {
        progress->onEnd();
    }
    if (errorReserveCallback != nullptr) {
        errorReserveCallback();
    }
}
