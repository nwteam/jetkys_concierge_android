#ifndef __syanago__PurchaseMolde__
#define __syanago__PurchaseMolde__

#include "cocos2d.h"

using namespace cocos2d;

class PurchaseMolde
{
public:
    PurchaseMolde(int state, const char* productId, const char* inAppPurchaseId, const char* receipt){
        status = state;
        productId = productId;
        inAppPurchaseId = inAppPurchaseId;
        receipt = receipt;
    }
    
    CC_SYNTHESIZE_READONLY(int, status, Status);
    CC_SYNTHESIZE_READONLY(std::string, productId,  ProductId);
    CC_SYNTHESIZE_READONLY(std::string, inAppPurchaseId,  InAppPurchaseId);
    CC_SYNTHESIZE_READONLY(std::string, receipt,  Receipt);
};

#endif /* defined(__syanago__PurchaseMolde__) */
