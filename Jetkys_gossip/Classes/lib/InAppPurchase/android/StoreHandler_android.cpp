#include "cocos2d.h"
#include "StoreHandler.h"
#include "StoreHandler_android.h"

#include "platform/android/jni/JniHelper.h"

StoreHandler* StoreHandler_android::handler = NULL;
StoreHandler_android* StoreHandler_android::myInstance = NULL;

USING_NS_CC;

StoreHandler_android::StoreHandler_android()
{
	if(myInstance == NULL)
	{
		myInstance = this;
	}
}

StoreHandler_android::~StoreHandler_android()
{
	if(this == myInstance)
	{
		myInstance = NULL;
	}
}

StoreHandler_android* StoreHandler_android::getInstance()
{
	if(myInstance == NULL) {
		myInstance = new StoreHandler_android();
	}
	return myInstance;
}

void StoreHandler_android::setCallback(StoreHandler* callback)
{
	StoreHandler_android::handler = callback;
}

void StoreHandler_android::callbackFunc(int state, const char* productId, const char* Id, const char* receipt)
{
	handler->callBackFunc(state,productId,Id,receipt);
}

void StoreHandler_android::purchaseEndCallBackFunc()
{
	handler->purchaseEndCallBackFunc();
}

void StoreHandler_android::finishTransaction()
{
}

