#include "StoreHandler.h"
#include "StoreHandler_android.h"

#include <platform/android/jni/JniHelper.h>

USING_NS_CC;
using namespace std;

static StoreHandler_android* storeHandler_android = StoreHandler_android::getInstance();

StoreHandler::StoreHandler()
{}

StoreHandler::~StoreHandler()
{}

bool StoreHandler::canMakePayments()
{
	return true;
}

void StoreHandler::setCallBack(StoreHandler* p)
{
	storeHandler_android->setCallback(p);
}

void StoreHandler::callBackFunc(int state, const char* productId, const char* Id, const char* receipt)
{}

void StoreHandler::purchaseEndCallBackFunc()
{}

void StoreHandler::applicationDidEnterBackground()
{}

void StoreHandler::applicationWillEnterForeground()
{}

void StoreHandler::finishLastTransaction()
{
	JniMethodInfo t;
	if(!JniHelper::getStaticMethodInfo(t,
			"com/syanago/purchase/InAppPurchase",
			"finishLastTransaction",
			"()V"))
	{
		return;
	}
	t.env->CallStaticVoidMethod(t.classID, t.methodID);
	t.env->DeleteLocalRef(t.classID);
}

void StoreHandler::productRequestStart(const char* productId)
{
	JniMethodInfo t;
	if(!JniHelper::getStaticMethodInfo(t,
			"com/syanago/purchase/InAppPurchase",
			"requestPurchasing",
			"(Ljava/lang/String;)V")){
		return;
	}
	jstring strArgl = t.env->NewStringUTF(productId);
	t.env->CallStaticVoidMethod(t.classID, t.methodID, strArgl);
	t.env->DeleteLocalRef(t.classID);
}
