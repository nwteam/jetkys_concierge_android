#ifndef STOREHANDLER_ANDOROID_H_
#define STOREHANDLER_ANDOROID_H_

#include "StoreHandler.h"

#include "platform/android/jni/JniHelper.h"
#include <Jni.h>
#include <android/log.h>
#include <stdlib.h>

class StoreHandler_android
{
public:
	StoreHandler_android();
	virtual ~StoreHandler_android();

	static StoreHandler_android* getInstance();

	void setCallback(StoreHandler* callback);
	void purchaseEndCallBackFunc();

	virtual void callbackFunc(int state, const char* productId, const char* Id, const char* receipt);

	static StoreHandler* handler;
	void finishTransaction();
private:
	static StoreHandler_android* myInstance;
};

#endif /*STOREHANDLER_ANDROID_H_ */
