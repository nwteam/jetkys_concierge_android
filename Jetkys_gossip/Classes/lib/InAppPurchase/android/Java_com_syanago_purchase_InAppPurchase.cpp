#include "cocos2d.h"
#include "Base64.h"
#include <string.h>

#include "StoreHandler.h"
#include "StoreHandler_android.h"

USING_NS_CC;

extern "C" {
	JNIEXPORT void JNICALL Java_com_syanago_purchase_InAppPurchase_inAppPurchaseCallback(JNIEnv* env, jobject thiz, jint, jstring, jstring, jstring);
	JNIEXPORT void JNICALL Java_com_syanago_purchase_InAppPurchase_inAppPurchaseEndCallback(JNIEnv* env, jobject thiz);
};

JNIEXPORT void JNICALL Java_com_syanago_purchase_InAppPurchase_inAppPurchaseCallback(JNIEnv* env, jobject thiz, jint purchaseStatus, jstring productId, jstring Id, jstring receipt)
{
	StoreHandler_android* h = StoreHandler_android::getInstance();
	int state = (int)purchaseStatus;
	const char* pId = env->GetStringUTFChars(productId, 0);
	const char* identifer = env->GetStringUTFChars(Id, 0);
	char* transactionReceipt = NULL;
	const char* originalReceipt = env->GetStringUTFChars(receipt, 0);
	cocos2d::base64Encode((const unsigned char*)originalReceipt, strlen(originalReceipt), &transactionReceipt);
	CCLOG("[JNICALL Java_com_syanago_purchase_InAppPurchase_inAppPurchaseCallback]state:%d, productId:%s, Id:%s, receipt:%s", state, pId, identifer, transactionReceipt);

	if(h != NULL)
	{
		switch(state)
		{
		case 0:
		case 3:
			state = STORE_STATE_ERROR;
			break;
		case 1:
			state = STORE_STATE_FINISH;
			break;
		case 2:
			state = STORE_STATE_CANCEL;
			break;
		default:
			state = STORE_STATE_ERROR;
			break;
		}
		h->callbackFunc(state, pId, identifer, transactionReceipt);
	}
}

JNIEXPORT void JNICALL Java_com_syanago_purchase_InAppPurchase_inAppPurchaseEndCallback(JNIEnv* env, jobject thiz)
{
	StoreHandler_android* h = StoreHandler_android::getInstance();
	if(h != NULL)
	{
		h->purchaseEndCallBackFunc();
	}
}
