#ifndef __syanago__InAppPurchaseModel__
#define __syanago__InAppPurchaseModel__

#include "cocos2d.h"
#include <functional>
#include <string>
#include <vector>
#include "editor-support/spine/Json.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"
#include "Token.h"
#include "PurchaseMolde.h"

#include "StoreHandler.h"

using namespace cocos2d;
using namespace SyanagoAPI;

class InAppPurchaseModel : public StoreHandler
{
public:
    InAppPurchaseModel():
    storeHandler(nullptr),
    successReserveCallback(nullptr),
    errorReserveCallback(nullptr),
    _placeId(0)
    {
        progress = new DefaultProgress();
        request = new RequestAPI(progress);
        storeHandler = new StoreHandler();
    }
    
    ~InAppPurchaseModel()
    {
        delete storeHandler;
        delete progress;
        delete request;
        progress = nullptr;
        request = nullptr;
        storeHandler = nullptr;
        successReserveCallback = nullptr;
        errorReserveCallback = nullptr;
    }
    typedef std::function<void()> OnResponceCallback;

    void buyProduct(const std::string productId, const int placeId, std::vector<std::shared_ptr<const Token>> token, const OnResponceCallback& successCallback,const OnResponceCallback& errorCallback);
    void callBackFunc(int state, const char* productId, const char* inAppPurchaseId, const char* receipt);
    
    
private:
    StoreHandler* storeHandler;
    DefaultProgress* progress;
    RequestAPI* request;
    OnResponceCallback successReserveCallback;
    OnResponceCallback errorReserveCallback;
    
    std::vector<std::shared_ptr<const Token>> _token;
    
    int _placeId;
    
    void responceItemList(Json* responce);
    void boughtProduct(Json* responce);
    void purchaseEndCallBackFunc();
    void purchaseErrorCallBack();
    
    int searchTokenWithProductId(const std::string productId);
    Json* convertJsonOfPurchaseToken(Json* responce);
    void sendBoughtMeasurementInformation(const int boughtToken);
    
    std::shared_ptr<const PurchaseMolde> _productInfomation;
    
};

#endif /* defined(__syanago__InAppPurchaseModel__) */
