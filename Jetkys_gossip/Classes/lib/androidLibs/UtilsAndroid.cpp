/*
 * UtilsAndroid.cpp
 *
 *  Created on: Apr 16, 2014
 *      Author: hnc
 */

#include "UtilsAndroid.h"
#include "platform/android/jni/JniHelper.h"
USING_NS_CC;

#define  CLASS_NAME "beetsoftvn/game/helper/UtilsHelper"

extern "C" {

/**
 * ================== Alert =============================
 */
void alertShow(const char *title, const char *message) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "alertShow",
			"(Ljava/lang/String;Ljava/lang/String;)V")) {
		return;
	}
	jstring stringArg1 = methodInfo.env->NewStringUTF(title);
	jstring stringArg2 = methodInfo.env->NewStringUTF(message);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg1, stringArg2);
	methodInfo.env->DeleteLocalRef(stringArg1);
	methodInfo.env->DeleteLocalRef(stringArg2);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void alertShowWithCallback(const char *title, const char *message) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"alertShowWithCallback",
			"(Ljava/lang/String;Ljava/lang/String;)V")) {
		return;
	}
	jstring stringArg1 = methodInfo.env->NewStringUTF(title);
	jstring stringArg2 = methodInfo.env->NewStringUTF(message);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg1, stringArg2);
	methodInfo.env->DeleteLocalRef(stringArg1);
	methodInfo.env->DeleteLocalRef(stringArg2);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

/**
 * ================== End Alert =============================
 */

/**
 * ================== Ads =============================
 */

void adsShowBanner() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "adsShowBanner",
			"()V")) {
		return;
	}
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void adsHideBanner() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "adsHideBanner",
			"()V")) {
		return;
	}
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void adsShowInterstitial() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"adsShowInterstitial", "()V")) {
		return;
	}
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

/**
 * ================== End Ads =============================
 */

/**
 * ================== FB =============================
 */
void fbPublishStory(const char * postStr) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"fbPublishStory", "(Ljava/lang/String;)V")) {
		return;
	}
	jstring stringArg = methodInfo.env->NewStringUTF(postStr);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg);
	methodInfo.env->DeleteLocalRef(stringArg);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void fbPostStatus(const char * postStr) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"fbPostStatusUpdate", "(Ljava/lang/String;)V")) {
		return;
	}
	jstring stringArg = methodInfo.env->NewStringUTF(postStr);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg);
	methodInfo.env->DeleteLocalRef(stringArg);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void fbPostPhoto(const char * imagePath) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "fbPostPhoto",
			"(Ljava/lang/String;)V")) {
		return;
	}
	jstring stringArg = methodInfo.env->NewStringUTF(imagePath);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg);
	methodInfo.env->DeleteLocalRef(stringArg);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void fbPublishImageWithCaption(const char *caption, const char *imgPath) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"fbPublishImageWithCaption",
			"(Ljava/lang/String;Ljava/lang/String;)V")) {
		return;
	}
	jstring stringArg1 = methodInfo.env->NewStringUTF(caption);
	jstring stringArg2 = methodInfo.env->NewStringUTF(imgPath);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg1, stringArg2);
	methodInfo.env->DeleteLocalRef(stringArg1);
	methodInfo.env->DeleteLocalRef(stringArg2);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void fbShareStatus() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "fbShareStatus",
			"()V")) {
		return;
	}
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void fbSharePhoto(const char * postStr) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "fbSharePhoto",
			"(Ljava/lang/String;)V")) {
		return;
	}
	jstring stringArg = methodInfo.env->NewStringUTF(postStr);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg);
	methodInfo.env->DeleteLocalRef(stringArg);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

/**
 * ================== End FB =============================
 */

/**
 * ================== End TW =============================
 */
void twCheckConnected() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"twCheckConnected", "()V")) {
		return;
	}
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void twUpdateStatus(const char *caption, const char *imgPath) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"twUpdateStatus", "(Ljava/lang/String;Ljava/lang/String;)V")) {
		return;
	}
	jstring stringArg1 = methodInfo.env->NewStringUTF(caption);
	jstring stringArg2 = methodInfo.env->NewStringUTF(imgPath);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg1, stringArg2);
	methodInfo.env->DeleteLocalRef(stringArg1);
	methodInfo.env->DeleteLocalRef(stringArg2);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void twShareStatus(const char *caption, const char *imgPath) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "twShareStatus",
			"(Ljava/lang/String;Ljava/lang/String;)V")) {
		return;
	}
	jstring stringArg1 = methodInfo.env->NewStringUTF(caption);
	jstring stringArg2 = methodInfo.env->NewStringUTF(imgPath);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg1, stringArg2);
	methodInfo.env->DeleteLocalRef(stringArg1);
	methodInfo.env->DeleteLocalRef(stringArg2);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}
/**
 * ================== End TW =============================
 */

/**
 * ================= Game center =========================
 */

void gcGameServicesSignIn() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"gameServicesSignIn", "()V")) {
		return;
	}
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void gcUpdateTopScoreLeaderboard(const char * id, long score) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"updateTopScoreLeaderboard", "(Ljava/lang/String;J)V")) {
		return;
	}
	jstring stringArg = methodInfo.env->NewStringUTF(id);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg, (jlong) score);
	methodInfo.env->DeleteLocalRef(stringArg);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void gcUpdateAchievement(const char * id, int percentage) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"updateAchievement", "(Ljava/lang/String;I)V")) {
		return;
	}
	jstring stringArg = methodInfo.env->NewStringUTF(id);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg, percentage);
	methodInfo.env->DeleteLocalRef(stringArg);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void gcShowLeaderboards() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"showLeaderboards", "()V")) {
		return;
	}
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

void gcShowAchievements() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME,
			"showAchievements", "()V")) {
		return;
	}
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

/**
 * ================= End Game center =========================
 */

/**
 * ================= InAppBilling =========================
 */

void iabOnBuy(const char * skuID) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "onBuy", "(Ljava/lang/String;)V")) {
		return;
	}
	jstring stringArg = methodInfo.env->NewStringUTF(skuID);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg);
	methodInfo.env->DeleteLocalRef(stringArg);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}


/**
 * ================= End InAppBilling =========================
 */

/*
 * Play Character Voice
 * */
void iaonPlayCharacterVoice(const char * voice) {
JniMethodInfo methodInfo;
if (!JniHelper::getStaticMethodInfo(methodInfo,
"org/cocos2dx/cpp/AppActivity", "onPlayCharacterVoice", "(Ljava/lang/String;)V")) {
return;
}
jstring stringArg = methodInfo.env->NewStringUTF(voice);
methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
methodInfo.methodID, stringArg);
methodInfo.env->DeleteLocalRef(stringArg);
methodInfo.env->DeleteLocalRef(methodInfo.classID);
}
/*
 * Play Character Voice
 * */

/**
 * ================= GAI =========================
 */
void sendEvent(const char *category, const char *action, const char *label,
		long value) {
	JniMethodInfo mi;

	if (JniHelper::getStaticMethodInfo(mi, CLASS_NAME, "sendEvent",
			"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V")) {
		jstring jCategory = mi.env->NewStringUTF(category);
		jstring jAction = mi.env->NewStringUTF(action);
		jstring jLabel = mi.env->NewStringUTF(label);
		jlong jValue = value;

		mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, jCategory,
				jAction, jLabel, jValue);

		mi.env->DeleteLocalRef(jCategory);
		mi.env->DeleteLocalRef(jAction);
		mi.env->DeleteLocalRef(jLabel);
		mi.env->DeleteLocalRef(mi.classID);
	}
}

void sendView(const char *appScreen) {
	JniMethodInfo mi;

	if (JniHelper::getStaticMethodInfo(mi, CLASS_NAME, "sendView",
			"(Ljava/lang/String;)V")) {
		jstring jAppScreen = mi.env->NewStringUTF(appScreen);

		mi.env->CallStaticVoidMethod(mi.classID, mi.methodID, jAppScreen);

		mi.env->DeleteLocalRef(jAppScreen);
		mi.env->DeleteLocalRef(mi.classID);
	}
}
/**
 * ================= END GAI =========================
 */


/**
 * ================ ART ANDROID =======================
 */

void iabArtLtvSupport(const char * skuID, const char * playerId) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo,
				"org/cocos2dx/cpp/AppActivity", "onArt", "(Ljava/lang/String;Ljava/lang/String;)V")) {
			return;
		}
	jstring jToken = methodInfo.env->NewStringUTF(skuID);
	jstring jPlayerId = methodInfo.env->NewStringUTF(playerId);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, jToken, jPlayerId);
	methodInfo.env->DeleteLocalRef(jToken);
	methodInfo.env->DeleteLocalRef(jPlayerId);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

/**
 * ================ END ART ANDROID ===================
 */

void openUrl(const char *url) {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "openUrl",
			"(Ljava/lang/String;)V")) {
		return;
	}
	jstring stringArg = methodInfo.env->NewStringUTF(url);
	methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
			methodInfo.methodID, stringArg);
	methodInfo.env->DeleteLocalRef(stringArg);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
}

/***
 *
 */
std::string android_getOSVersion() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, "gameutils.helper.Utils",
			"getOSVersion", "()Ljava/lang/String;")) {
		return "";
	}

	jobject ret = methodInfo.env->CallStaticObjectMethod(methodInfo.classID,
			methodInfo.methodID);

	std::string str = methodInfo.env->GetStringUTFChars((jstring) ret, 0);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
	methodInfo.env->DeleteLocalRef(ret);

	return str;
}

std::string android_getAppVersion() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo, "gameutils.helper.Utils",
			"getAppVersion", "()Ljava/lang/String;")) {
		return "";
	}

	jobject ret = methodInfo.env->CallStaticObjectMethod(methodInfo.classID,
			methodInfo.methodID);

	std::string str = methodInfo.env->GetStringUTFChars((jstring) ret, 0);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
	methodInfo.env->DeleteLocalRef(ret);

	return str;
}

std::string android_getUUID() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo,
			"gameutils/encrypt/EncryptData", "getUUID",
			"()Ljava/lang/String;")) {
		return "";
	}

	jobject ret = methodInfo.env->CallStaticObjectMethod(methodInfo.classID,
			methodInfo.methodID);

	std::string str = methodInfo.env->GetStringUTFChars((jstring) ret, 0);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
	methodInfo.env->DeleteLocalRef(ret);

	return str;
}

std::string android_getIV() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo,
			"gameutils/encrypt/EncryptData", "getIV", "()Ljava/lang/String;")) {
		return "";
	}

	jobject ret = methodInfo.env->CallStaticObjectMethod(methodInfo.classID,
			methodInfo.methodID);

	std::string str = methodInfo.env->GetStringUTFChars((jstring) ret, 0);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
	methodInfo.env->DeleteLocalRef(ret);
	return str;
}

std::string android_encrypt(const char * targetString, const char * hexIV) {
	CCLOG("android_encrypt mess: %s, iv: %s", targetString, hexIV);
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo,
			"gameutils/encrypt/EncryptData", "encrypt",
			"(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;")) {
		return "";
	}

	jstring stringArg1 = methodInfo.env->NewStringUTF(targetString);
	jstring stringArg2 = methodInfo.env->NewStringUTF(hexIV);

	jobject ret = methodInfo.env->CallStaticObjectMethod(methodInfo.classID,
			methodInfo.methodID, stringArg1, stringArg2);

	std::string str = methodInfo.env->GetStringUTFChars((jstring) ret, 0);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
	methodInfo.env->DeleteLocalRef(ret);

	return str;
}

std::string android_decrypt(const char * targetString, const char * hexIV) {
	CCLOG("android_decrypt mess: %s, iv: %s", targetString, hexIV);
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo,
			"gameutils/encrypt/EncryptData", "decrypt",
			"(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;")) {
		return "";
	}

	jstring stringArg1 = methodInfo.env->NewStringUTF(targetString);
	jstring stringArg2 = methodInfo.env->NewStringUTF(hexIV);

	jobject ret = methodInfo.env->CallStaticObjectMethod(methodInfo.classID,
			methodInfo.methodID, stringArg1, stringArg2);

	std::string str = methodInfo.env->GetStringUTFChars((jstring) ret, 0);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
	methodInfo.env->DeleteLocalRef(ret);

	return str;
}

std::string android_sha256(const char * targetString) {
	CCLOG("android_sha256 mess: %s", targetString);
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo,
			"gameutils/encrypt/EncryptData", "sha256",
			"(Ljava/lang/String;)Ljava/lang/String;")) {
		return "";
	}

	jstring stringArg1 = methodInfo.env->NewStringUTF(targetString);

	jobject ret = methodInfo.env->CallStaticObjectMethod(methodInfo.classID,
			methodInfo.methodID, stringArg1);

	std::string str = methodInfo.env->GetStringUTFChars((jstring) ret, 0);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
	methodInfo.env->DeleteLocalRef(ret);

	return str;
}

std::string android_getPushToken() {
	JniMethodInfo methodInfo;
	if (!JniHelper::getStaticMethodInfo(methodInfo,
			"org.cocos2dx.cpp.AppActivity", "getPushToken",
			"()Ljava/lang/String;")) {
		return "";
	}

	jobject ret = methodInfo.env->CallStaticObjectMethod(methodInfo.classID,
			methodInfo.methodID);

	std::string str = methodInfo.env->GetStringUTFChars((jstring) ret, 0);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
	methodInfo.env->DeleteLocalRef(ret);

	return str;
}

/**
 * ========================= Jni Callback ===========================
 */

JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_AppActivity_alertCallback(
		JNIEnv *env, jobject obj, jint index) {
//	Alert::BAlertView::callback_click((int) index);
}

//JNIEXPORT void JNICALL Java_ClassName_MethodName
//  (JNIEnv *env, jobject obj, jstring javaString)
//{
//    //Get the native string from javaString
//    const char *nativeString = env->GetStringUTFChars(javaString, 0);
//
//    //Do something with the nativeString
//
//    //DON'T FORGET THIS LINE!!!
//    env->ReleaseStringUTFChars(javaString, nativeString);
//}
/**
 * ========================= End Jni Callback ===========================
 */
}
