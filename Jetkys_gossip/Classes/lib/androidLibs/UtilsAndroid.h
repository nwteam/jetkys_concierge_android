/*
 * UtilsAndroid.h
 *
 *  Created on: Apr 16, 2014
 *      Author: hnc
 */

#ifndef UTILSANDROID_H_
#define UTILSANDROID_H_

#include "cocos2d.h"

extern "C" {
	extern void alertShowWithCallback(const char *title, const char *message);
	extern void alertShow(const char *title, const char *message);

	extern void adsShowBanner();
	extern void adsHideBanner();
	extern void adsShowInterstitial();

	extern void fbPostPhoto(const char * imagePath);
	extern void fbPostStatus(const char * postStr);
	extern void fbPublishStory(const char * postStr);
	extern void fbPublishImageWithCaption(const char *caption, const char *imgPath);
	extern void fbShareStatus();
	extern void fbSharePhoto(const char *imgPath);

	extern void twCheckConnected();
	extern void twUpdateStatus(const char *caption, const char *imgPath);
	extern void twShareStatus(const char *caption, const char *imgPath);

	//Gamecenter
	extern void gcGameServicesSignIn();
	extern void gcUpdateTopScoreLeaderboard(const char * id, long score);
	extern void gcUpdateAchievement(const char * id, int percentage);
	extern void gcShowLeaderboards();
	extern void gcShowAchievements();

	//inAppBilling
	extern void iabOnBuy(const char *skuID);
	extern void openUrl(const char *url);
	extern void iaonPlayCharacterVoice(const char *voice);		//Character Voice

	extern void sendEvent(const char *category, const char *action, const char *label, long value);
	extern void sendView(const char *appScreen);

	//ART
	extern void iabArtLtvSupport(const char * skuID, const char * playerId);

	//
	std::string android_getOSVersion();
	std::string android_getAppVersion();
	std::string android_getUUID();
	std::string android_getIV();
	std::string android_encrypt(const char * targetString, const char * hexIV);
	std::string android_decrypt(const char * targetString, const char * hexIV);
	std::string android_sha256(const char * targetString);
	std::string android_getPushToken();
}

#endif /* UTILSANDROID_H_ */
