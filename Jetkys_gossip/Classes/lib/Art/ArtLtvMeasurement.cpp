#include "ArtLtvMeasurement.h"
#include "UserDefaultManager.h"
#include "API.h"

// 計測
void ArtLtvMeasurement::send(const char* token, const int playerId)
{
    if (UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DOMAIN_URL) == SyanagoAPI::API::DOMAINS::PRODUCTION) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        ArtLtvSupport::sendArtData(token,StringUtils::format("%d", playerId).c_str());
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        iabArtLtvSupport(token, StringUtils::format("%d", playerId).c_str());
#endif
    }
}