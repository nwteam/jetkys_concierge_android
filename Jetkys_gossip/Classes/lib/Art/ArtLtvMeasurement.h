#ifndef __syanago__ArtLtvMeasurement__
#define __syanago__ArtLtvMeasurement__

#include "cocos2d.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "ArtLvtSupport.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "UtilsAndroid.h"
#endif

USING_NS_CC;

class ArtLtvMeasurement
{
public:
    static void send(const char* token, const int playerId);
};

#endif
