#ifndef syanago_MeasurementInformation_h
#define syanago_MeasurementInformation_h
#include "cocos2d.h"

namespace MEASURMENT_INFO {
    namespace ART{
        static const char* BOUGHT_TOKEN_1 = "1567";
        static const char* BOUGHT_TOKEN_6 = "1568";
        static const char* BOUGHT_TOKEN_12 = "1338";
        static const char* BOUGHT_TOKEN_13 = "1569 ";
        static const char* BOUGHT_TOKEN_26 = "1343";
        static const char* BOUGHT_TOKEN_27 = "1571";
        static const char* BOUGHT_TOKEN_55 = "1340";
        static const char* BOUGHT_TOKEN_56 = "1570";
        static const char* BOUGHT_TOKEN_95 = "1341";
        static const char* BOUGHT_TOKEN_180 = "1342";
        static const char* DAU = "1335";
        static const char* MAU = "1510";
        static const char* DPU = "1363";
        static const char* MPU = "1364";
        static const char* TRIED_FREE_GACHA = "1353";
        static const char* TRIED_PAY_GACHA = "1352";
        static const char* TRIED_10_COUNT_PAY_GACHA = "1535";
        static const char* TRIED_FIRST_TIME_PAY_GACHA = "1362";
        static const char* BUYED_FUEL = "1356";
        static const char* EXPANDED_GARAGE = "1357";
        static const char* SETED_DICE_EYE = "1354";
        static const char* RELEASED_DICE_EYE = "1355";
        static const char* BOUGHT_COIN_30000 = "1358";
        static const char* BOUGHT_COIN_165000 = "1359";
        static const char* BOUGHT_COIN_360000 = "1360";
        static const char* BOUGHT_COIN_840000 = "1361";
        static const char* BOUGHT_RACE_TURN = "1599";
        static const char* AWAKE_CHARACTER = "1601";
    };
};

#endif
