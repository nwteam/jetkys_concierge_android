#import <Foundation/Foundation.h>
#include "ArtLvtSupport.h"
#import "ArtLtv.h"
#include "MeasurementInformation.h"

void ArtLtvSupport::sendArtData(const char* data, const char* playerId)
{
    ArtLtv* ltv = [[ArtLtv alloc] init];
    if (isSendTokenData(data) == true) {
        [ltv addParameter:@PARAM_PRICE:[ [ NSString alloc ] initWithUTF8String:getPriceData(data) ]];
    }
    const int sendId = atoi(data);
    [ltv sendLtv:sendId:[[NSString alloc]initWithUTF8String:getPriceData(playerId)]];
}

bool ArtLtvSupport::isSendTokenData(const char* data)
{
    using namespace MEASURMENT_INFO::ART;
    if (data == BOUGHT_TOKEN_1) {
        return true;
    } else if (data == BOUGHT_TOKEN_6)    {
        return true;
    } else if (data == BOUGHT_TOKEN_13)    {
        return true;
    } else if (data == BOUGHT_TOKEN_27)    {
        return true;
    } else if (data == BOUGHT_TOKEN_56)    {
        return true;
    } else if (data == BOUGHT_TOKEN_95)    {
        return true;
    } else if (data == BOUGHT_TOKEN_180)    {
        return true;
    }
    return false;
}

const char* ArtLtvSupport::getPriceData(const char* data)
{
    using namespace MEASURMENT_INFO::ART;
    if (data == BOUGHT_TOKEN_1) {
        return "120";
    } else if (data == BOUGHT_TOKEN_6)    {
        return "480";
    } else if (data == BOUGHT_TOKEN_13)    {
        return "960";
    } else if (data == BOUGHT_TOKEN_27)    {
        return "1900";
    } else if (data == BOUGHT_TOKEN_56)    {
        return "3800";
    } else if (data == BOUGHT_TOKEN_95)    {
        return "5400";
    } else if (data == BOUGHT_TOKEN_180)    {
        return "9800";
    }
    return "";
}