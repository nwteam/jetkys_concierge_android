#ifndef syanago_ArtLvtSupport_h
#define syanago_ArtLvtSupport_h

class ArtLtvSupport{
public:
    static void sendArtData(const char* data, const char* playerId);
    
private:
    static bool isSendTokenData(const char* data);
    static const char* getPriceData(const char* data);
};

#endif
