#include "BrowserLauncher.h"
#include "BrowserLauncherJni.h"

namespace Cocos2dExt
{
    void BrowserLauncher::launchUrl(char const* pszUrl)
    {
        launchUrlJNI(pszUrl);
    }
}