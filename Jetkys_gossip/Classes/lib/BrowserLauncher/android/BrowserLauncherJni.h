#ifndef __BROWSER_LAUNCHER_JNI__
#define __BROWSER_LAUNCHER_JNI__

#include <jni.h>

extern "C"
{
    extern void launchUrlJNI(char const *pszUrl);
}

#endif // __BROWSER_LAUNCHER_JNI__