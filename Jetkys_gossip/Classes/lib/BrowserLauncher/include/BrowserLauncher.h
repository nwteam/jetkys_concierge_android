#ifndef _BROWSER_LAUNCHER_H_
#define _BROWSER_LAUNCHER_H_

#include <stddef.h>

namespace Cocos2dExt {
    
    class BrowserLauncher
    {
    public:
        static void launchUrl(char const *pszUrl);
    };
    
} // end of namespace Cocos2dExt

#endif // _BROWSER_LAUNCHER_H_