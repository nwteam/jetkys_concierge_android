#import "BrowserLauncher_objc.h"

@implementation BrowserLauncher

+ (void)launchUrl:(NSString*)pszUrl
{
    NSURL* url = [NSURL URLWithString:pszUrl];
    [[UIApplication sharedApplication] openURL:url];
}

@end