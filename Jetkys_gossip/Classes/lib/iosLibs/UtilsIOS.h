#ifndef __Dapgian__Utils_IOS__
#define __Dapgian__Utils_IOS__

#include <iostream>
extern void openURL(const char* urlStr);
extern bool isWideScreen();

//iOS 6 or later
extern std::string getDIDInKeychain();
extern void saveDIDInKeychain(std::string did);
extern void delDIDInKeychain();
extern std::string getUUID();
extern std::string getIOsVersion();
extern std::string getAppVersion();
extern std::string ios_getPushToken();

//extern void requestPurchasing(const char *productID);


#endif /* defined(__Dapgian__Utils_IOS__) */
