#ifndef syanago_Constant_h
#define syanago_Constant_h

#include "cocos2d.h"

USING_NS_CC;

//課金アイテム取得場所
#define GET_PLACE_APP_STORE = 10;
#define GET_PLACE_GOOGLE_PLAY = 20;
#define GET_PLACE_EVENT = 30;
#define GET_PLACE_FRIEND_GACHA = 40;

/* ゲーム内通貨 */
#define ITEM_TOKEN = 10000;
#define ITEM_COIN = 10010;

/* 開発・本番モード */
#define SERVER_MODE = self::MODE_DEV;
#define MODE_PROD = 1; //本番
#define MODE_DEV = 2; //開発

/* アプリ固有キー（トークン生成用） */
#define APP_TOKEN_SEED = "Af2d3qXgaWrWyF4C"; //アプリ固有キー
#define INSTALL_SEED_LENGTH = 32; //インストール固有キー長
#define SEED_CHECK_COUNT = 10; //最大シードの重複チェック回数

/* ログイン */
#define DAILY_LOGIN_DAY = 10; //連続ログインボーナスの日数（現状では連続ログインボーナスGETでの初期化は無いので、100年としておく）

/* 時間制御 */
#define TIMER_TOLERANCE = 300; //サーバー時刻差の上限許容範囲（秒）
#define TIMER_THRESHOLD = 300; //サーバー時刻差の下限許容範囲（秒）

/* 暗号化関連 */
#define CRYPT_LV = MCRYPT_RIJNDAEL_128; // 暗号化フォーマット（mcrypt拡張モジュールで使用可能）
#define SECRET_KEY = "At5CcmaxnRmE"; //秘密キー

/* ダイアログテンプレートのフォルダ */
#define DIALOG_EVENT = "dialog_template/event.html"; //イベント
#define DIALOG_LOGIN_BONUS = "dialog_template/login_bonus.html"; //連続ログインボーナス
#define DIALOG_LOGIN_BONUS_ITEM = "dialog_template/login_bonus_item.html"; //N連続ログイン達成ボーナス
#define DIALOG_HELP = "dialog_template/help.html"; //助っ人
#define DIALOG_INVITATION = "dialog_template/invitation_bonus.html"; //招待ボーナス
#define DIALOG_INVITATION_COMPLETE = "dialog_template/invitation_complete_bonus.html"; //招待キャンペーン達成ボーナス
#define DIALOG_NOTICE = "dialog_template/notice.html"; //お知らせ

/* アプリケーションエラー */
#define CODE_ERR_NOT_POST_ACCESS = 201; //POST以外の不正アクセス
#define CODE_ERR_MISSING_POSTDATA = 202; //POSTデータ不足or不正
#define CODE_ERR_APP_VERSION_NOT_MATCHED = 203; //アプリバージョン不一致
#define CODE_ERR_VALIDATION_FAIL = 204; //バリデーション失敗
#define CODE_ERR_USERID_NOT_FOUND = 205; //ユーザーIDなし
#define CODE_ERR_ACCOUNT_BANNED = 206; //ユーザー不正
#define CODE_ERR_ACCOUNT_DISABLED = 207; //ユーザー論理削除済
#define CODE_ERR_LOG_MISSING_ARGUMENT = 208; //ロガーの引数不足
#define CODE_ERR_NOT_CACHE_APC = 209; //メモリ（APC）内にデータ無し
#define CODE_ERR_NOT_CACHE_MEMCACHED = 210; //メモリ（Memcached）内にデータ無し
#define CODE_ERR_CONTROL_NAME = 211; //コントローラー名が正しくない
#define CODE_ERR_USER_CANNOT_REGISTER = 212; //ユーザー登録失敗
#define CODE_ERR_USER_CANNOT_REGISTER_SHARD = 213; //ユーザー登録失敗(シャードされたDB）
#define CODE_ERR_USER_CANNOT_UPDATE = 214; //ユーザー情報変更失敗
#define CODE_ERR_USER_CANNOT_UPDATE_SHARD = 215; //ユーザー情報変更失敗(シャードされたDB）
#define CODE_ERR_DUPLICATE_LOGIN = 216; //重複ログイン、機種変更前端末でのログイン
#define CODE_ERR_CLIENT_TIME_FRAUD = 217; //クライアント時刻不正
#define CODE_ERR_REQUEST_JSON_DECODE = 218; //リクエストJSON(data)パース失敗
#define CODE_ERR_PASSWORD_EXPIRATION = 219; //引き継ぎパスワード有効期限切れ
#define CODE_ERR_PASSWORD_NOT_FOUND = 220; //引き継ぎパスワードが見つからない
#define CODE_ERR_PAGE_NOT_FOUND = 221; //ページのデータ無し
#define CODE_ERR_RESET_MARATHON = 222; //リセットマラソンによる新規登録エラー
#define CODE_ERR_NG_WORD = 223; //入力エラー（NGワード）
#define CODE_ERR_NG_FACE_MARK = 224; //入力エラー（絵文字）
#define CODE_ERR_FRIEND_MAX = 225; //フレンド最大数エラー

/* システムエラー */
#define CODE_ERR_APC_REFRESH_REQUEST = 600; //GameServerに対するAPC更新リクエストエラー
#define CODE_ERR_APC_DISABLED = 700; //APC無効
#define CODE_ERR_MEMCACHED_DISABLED = 701; //Memcached無効
#define CODE_ERR_SAVE_FAILED = 800; //保存エラー
#define CODE_ERR_PDO_EXCEPTION_OCCURRED = 801; //DBエラー
#define CODE_ERR_DB_GENERAL = 900; //その他エラー（DB）
#define CODE_ERR_GENERAL = 901; //その他エラー


#endif
