#import "UIAlertViewCallback.h"

@implementation UIAlertViewCallback

@synthesize callback;

- (id)initWithCallback:(UIAlertViewCallback_t)aCallback
{
    if (self = [super init]) {
        self.callback = aCallback;
        [self retain];
    }
    return self;
}

- (void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (callback)
        callback(buttonIndex);
    [self release];
}

- (void)dealloc
{
    self.callback = nil;
    [super dealloc];
}

@end