#import "EncryptData.h"
#import "NSData+CommonDigest.h"
#import <Security/SecRandom.h>

@implementation EncryptData

+ (NSString*)sha256: (NSString*) data
{
    const char* str = [data UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, (CC_LONG) strlen(str), result);

    NSMutableString* ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x", result[i]];
    }
    return ret;
}

+ (NSData*)crypto:(NSData*)data operation:(CCOperation)operation key:(NSString*)key iv:(const void*)iv
{
    char keyPtr[kCCKeySizeAES256 + 1];
    bzero(keyPtr, sizeof(keyPtr));
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];

    size_t bufferSize = [data length] + kCCBlockSizeAES128;
    void* buffer = malloc(bufferSize);
    size_t numBytesEncrypted = 0;

    CCCryptorStatus cryptorStatus = CCCrypt(operation, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                            keyPtr, kCCKeySizeAES256,
                                            iv,
                                            [data bytes], [data length],
                                            buffer, bufferSize,
                                            &numBytesEncrypted);

    if (cryptorStatus == kCCSuccess) {
        NSData* data = [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
        return data;
    }
    free(buffer);
    return nil;
}

+ (NSString*) convertEncryptDataToHex: (NSData*) data
{
    const unsigned char* dataBuffer = (const unsigned char*)[data bytes];

    if (!dataBuffer)
        return [NSString string];

    NSUInteger dataLength  = [data length];
    NSMutableString* hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];

    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];

    return [NSString stringWithString:hexString];
}

+ (NSData*)convertHexToBin: (NSString*) input
{
    const char* chars = [input UTF8String];
    int i = 0, len = (int)input.length;

    NSMutableData* data = [NSMutableData dataWithCapacity:len / 2];
    char byteChars[3] = {'\0', '\0', '\0'};
    unsigned long wholeByte;

    while (i < len) {
        byteChars[0] = chars[i++];
        byteChars[1] = chars[i++];
        wholeByte = strtoul(byteChars, NULL, 16);
        [data appendBytes:&wholeByte length:1];
    }

    return data;
}

+ (NSData*)createDataWithHexString:(NSString*)inputString
{
    NSUInteger inLength = [inputString length];

    unichar* inCharacters = malloc(sizeof(unichar) * inLength);
    [inputString getCharacters:inCharacters range:NSMakeRange(0, inLength)];

    UInt8* outBytes = malloc(sizeof(UInt8) * ((inLength / 2) + 1));

    NSInteger i, o = 0;
    UInt8 outByte = 0;
    for (i = 0; i < inLength; i++) {
        UInt8 c = inCharacters[i];
        SInt8 value = -1;

        if      (c >= '0' && c <= '9') value =      (c - '0');
        else if (c >= 'A' && c <= 'F') value = 10 + (c - 'A');
        else if (c >= 'a' && c <= 'f') value = 10 + (c - 'a');

        if (value >= 0) {
            if (i % 2 == 1) {
                outBytes[o++] = (outByte << 4) | value;
                outByte = 0;
            } else {
                outByte = value;
            }
        } else {
            if (o != 0) break;
        }
    }
    free(inCharacters);
    return [[NSData alloc] initWithBytesNoCopy:outBytes length:o freeWhenDone:YES];
}

+ (NSString*)MD5Hash: (NSString*) key
{
    const char* data = [key UTF8String];
    if (key.length == 0) {
        return nil;
    }
    CC_LONG len = (int)key.length;
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(data, len, result);
    NSMutableString* ms = @"".mutableCopy;
    for (int i = 0; i < 16; i++) {
        [ms appendFormat:@"%02X", result[i]];
    }
    return [ms lowercaseString];
}

+ (NSData*)generateRandomIV:(size_t)length
{
    NSMutableData* data = [NSMutableData dataWithLength:length];

    int output = SecRandomCopyBytes(kSecRandomDefault,
                                    length,
                                    data.mutableBytes);
    NSAssert(output == 0, @"error generating random bytes: %d",
             errno);
    return data;
}

@end