#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>

@interface EncryptData:
    NSObject

+ (NSData*)crypto:(NSData*)data operation:(CCOperation)operation key:(NSString*)key iv:(const void*)iv;
+ (NSString*) convertEncryptDataToHex: (NSData*) data;
+ (NSString*)sha256: (NSString*) data;
+ (NSData*)convertHexToBin: (NSString*) input;
+ (NSData*)createDataWithHexString: (NSString*)inputString;
+ (NSString*)MD5Hash: (NSString*) key;
+ (NSData*)generateRandomIV:(size_t)length;
@end
