#import "UtilsIOS.h"
#import "AppController.h"
#import "SecureUDID.h"
// #import "InAppPurchase/InAppPurchase.h"
#import "KeyChain/Keychain.h"

#define IS_WIDESCREEN (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double)568) < DBL_EPSILON)
#define SERVICE_NAME @"SYANAGO_SERVICE" // ANY_NAME_FOR_YOU
#define GROUP_NAME @"PM7352S8QE.syanagoAc1Hottea"
#define SAVE_KEY @"0123456789"
#define SAVE_DOMAIN @"jp.hottea.syanago"

void openURL(const char* urlStr)
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithUTF8String:urlStr]]];
}

bool isWideScreen()
{
    if (IS_WIDESCREEN)
        return true;
    else
        return false;
}

std::string getDIDInKeychain()
{
    Keychain* keychain = [[Keychain alloc] initWithService:SERVICE_NAME withGroup:nil];
    NSString* key = SAVE_KEY;
    NSData* data = [keychain find:key];

    if (data != nil) {
        NSString* value = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        return [value UTF8String];
    }
    return "";
}

void saveDIDInKeychain(std::string did)
{
    Keychain* keychain = [[Keychain alloc] initWithService:SERVICE_NAME withGroup:nil];
    NSString* key = SAVE_KEY;
    NSData* value = [[NSString stringWithUTF8String:did.c_str()] dataUsingEncoding:NSUTF8StringEncoding];

    NSData* data = [keychain find:key];
    if (data == nil) {
        // Save to keyChain
        if ([keychain insert:key :value])
            NSLog(@"Successfully added keychain");
        else
            NSLog(@"Failed to add keychain");
    } else {
        if ([keychain update:key :value])
            NSLog(@"Successfully update keychain");
        else
            NSLog(@"Failed to update keychain");
    }
}

void delDIDInKeychain()
{
    Keychain* keychain = [[Keychain alloc] initWithService:SERVICE_NAME withGroup:nil];
    NSString* key = SAVE_KEY;
    NSData* data = [keychain find:key];

    if (data != nil) {
        // del
        if ([keychain remove:key])
            NSLog(@"Successfully deleted keychain");
        else
            NSLog(@"Failed to del keychain");
    }
}

std::string getUUID()
{
    // NSString *UUID = [[NSUUID UUID] UUIDString];
    NSString* UUID = nil;

    // 端末Aに初めてインストール
    // DIDを新規発行し、KeyChain とSQLiteに保存
    // KeyChain内のDIDを参照する
    Keychain* keychain = [[Keychain alloc] initWithService:SERVICE_NAME withGroup:nil];
    NSString* key = SAVE_KEY;

    NSData* data = [keychain find:key];

    if (data == nil) {
        NSLog(@"Keychain data not found");

        // create new and Save to file
        UUID = [SecureUDID UDIDForDomain:SAVE_DOMAIN usingKey:SAVE_KEY];
        NSLog(@"get uuid: %@", UUID);

        // Save to keyChain
        NSData* value = [UUID dataUsingEncoding:NSUTF8StringEncoding];

        if ([keychain insert:key :value]) {
            NSLog(@"Successfully added data");
        } else
            NSLog(@"Failed to  add data");
    } else   {
        UUID = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"Data is =%@", UUID);

        // Update to file
        [SecureUDID updateUUIDForDomain:SAVE_DOMAIN usingKey:SAVE_KEY usingValue:UUID];
    }

    return [UUID UTF8String];
}

std::string getIOsVersion()
{
    float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSString* verStr = [NSString stringWithFormat:@"%f", ver];
    return [verStr UTF8String];
}

std::string getAppVersion()
{
    NSString* appVer = [NSString stringWithFormat:@"%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    return [appVer UTF8String];
}

std::string ios_getPushToken()
{
    return [[AppController getPushToken] UTF8String];
}

// void requestPurchasing(const char *productID) {
//    [[[InAppPurchase alloc] init] requestPurchasing:[NSString stringWithUTF8String:productID]];
// }