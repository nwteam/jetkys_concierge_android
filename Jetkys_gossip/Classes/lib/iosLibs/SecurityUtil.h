#ifndef __syanago__SecurityUtil__
#define __syanago__SecurityUtil__

#include <iostream>

class SecurityUtil{
private:
    std::string log;
    std::string data;
    
    
public:
    void construct();
    void createPassword(int length = 8, int type = 3);
    void getTokenInitial(std::string did, std::string datetime);
    void getToken(std::string userID, std::string installKey, std::string dateTime);
    static std::string encrypt(std::string targetString, std::string hexIV);
    static std::string decrypt(std::string targetString, std::string hexIV);
    static std::string sha256(const char *targetString);
    static std::string getIV();
};

#endif /* defined(__syanago__SecurityUtil__) */
