#import <Foundation/Foundation.h>
#import "UIAlertView+BlocksExtension.h"

@interface UIAlertViewCallback : NSObject <UIAlertViewDelegate> {
    UIAlertViewCallback_t callback;
}

@property (nonatomic, copy) UIAlertViewCallback_t callback;

- (id)initWithCallback:(UIAlertViewCallback_t) callback;

@end
