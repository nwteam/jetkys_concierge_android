#import "UIAlertView+BlocksExtension.h"
#import "UIAlertViewCallback.h"

@implementation UIAlertView (BlocksExtension)

- (id)initWithTitle:(NSString*)title message:(NSString*)message callback:(UIAlertViewCallback_t)callback cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles, ...{
    self = [self initWithTitle:title message:message delegate:nil cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
    if (self) {
        va_list args;
        va_start(args, otherButtonTitles);
        for (NSString* arg = otherButtonTitles; arg != nil; arg = va_arg(args, NSString*)) {
            [self addButtonWithTitle:arg];
        }
        va_end(args);
        self.delegate = [[[UIAlertViewCallback alloc] initWithCallback:callback] autorelease];
    }
    return self;
}

@end