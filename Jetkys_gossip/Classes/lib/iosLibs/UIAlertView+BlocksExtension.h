#import <UIKit/UIKit.h>

@interface UIAlertView(BlocksExtension)

typedef void (^UIAlertViewCallback_t)(NSInteger buttonIndex);

- (id)initWithTitle:(NSString *)title
            message:(NSString *)message
           callback:(UIAlertViewCallback_t)callback
  cancelButtonTitle:(NSString *)cancelButtonTitle
  otherButtonTitles:(NSString *)otherButtonTitles, ...;

@end
