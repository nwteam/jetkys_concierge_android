#import "SecurityUtil.h"
#import "Constant.h"
#import "EncryptData.h"

std::string SecurityUtil::encrypt(std::string targetString, std::string hexIv)
{
    NSString* str = [NSString stringWithUTF8String:targetString.c_str()];
    NSData* targetData = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSString* key = [EncryptData MD5Hash:@"At5CcmaxnRmE"];

    NSString* hexIvStr = [NSString stringWithUTF8String:hexIv.c_str()];
    NSData* ivStr =  [EncryptData createDataWithHexString: hexIvStr];

    NSData* data = [EncryptData crypto:targetData operation:kCCEncrypt key:key iv:[ivStr bytes]];
    NSString* encryptStr = [EncryptData convertEncryptDataToHex:data];
    return [encryptStr UTF8String];
}

std::string SecurityUtil::decrypt(std::string targetString, std::string hexIv)
{
    NSString* key = [EncryptData MD5Hash:@"At5CcmaxnRmE"];

    NSString* dataStr = [NSString stringWithUTF8String:targetString.c_str()];
    NSData* data =  [EncryptData createDataWithHexString: dataStr];

    NSString* hexIvStr = [NSString stringWithUTF8String:hexIv.c_str()];
    NSData* ivStr =  [EncryptData createDataWithHexString: hexIvStr];

    NSData* dataDecrypted = [EncryptData crypto:data operation:kCCDecrypt key:key iv:[ivStr bytes]];
    NSString* stringDecrypted = [[NSString alloc] initWithData:dataDecrypted encoding:NSUTF8StringEncoding];

    return [stringDecrypted UTF8String];
}

std::string SecurityUtil::sha256(const char* targetString)
{
    NSString* output = [EncryptData sha256: [NSString stringWithUTF8String:targetString]];
    return [output UTF8String];
}

std::string SecurityUtil::getIV()
{
    NSData* data =  [EncryptData generateRandomIV: 16];
    NSString* encryptStr = [EncryptData convertEncryptDataToHex:data];
    return [encryptStr UTF8String];
}

