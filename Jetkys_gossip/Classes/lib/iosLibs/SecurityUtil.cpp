//
//  SecurityUtil.cpp
//  syanago
//
//  Created by Hao Nguyen on 7/22/14.
//
//

#include "SecurityUtil.h"
#include "Native.h"
//#import "Constant.h"
//#import "EncryptData.h"

std::string SecurityUtil::encrypt(std::string targetString, std::string hexIv)
{
//    NSString *str = [NSString stringWithUTF8String:targetString];
//    NSData *targetData = [str dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *key = [EncryptData MD5Hash:@"At5CcmaxnRmE"];
//    
////    NSString *hexIvStr = @"00000000000000000000000000000000";
//    NSString *hexIvStr = [NSString stringWithUTF8String:hexIv.c_str()];
//    NSData *ivStr =  [EncryptData createDataWithHexString: hexIvStr];
//    
////  NSString *ivRev = [EncryptData convertEncryptDataToHex:ivStr];
////  NSLog(@"hexIvStr %@", hexIvStr);
////  NSLog(@"ivStr %@", ivStr);
////  NSLog(@"ivStrReverse %@", ivRev);
//    
////    NSLog(@"iv: %@", ivStr);
////    NSLog(@"key: %@", key);
//    
//    // 暗号化
//    NSData *data = [EncryptData crypto:targetData operation:kCCEncrypt key:key iv:[ivStr bytes]];
//    NSString *encryptStr = [EncryptData convertEncryptDataToHex:data];
//    NSLog(@"stringEncrypted Hex: %@", encryptStr);
//    
//    //test decrypt
//    // 復号化
////    NSData *dataDecrypted = [EncryptData crypto:data operation:kCCDecrypt key:key iv:[ivStr bytes]];
////    NSString *stringDecrypted = [[NSString alloc] initWithData:dataDecrypted encoding:NSUTF8StringEncoding];
////    NSLog(@"stringDecrypted: %@", stringDecrypted);
////    if([stringDecrypted isEqualToString:str]){
////        NSLog(@"多分成功");
////    }else{
////        NSLog(@"多分失敗");
////    }
//    //end test decrypt
//    
//    return [encryptStr UTF8String];
	//CCLOG("SecurityUtil::encrypt %s, %s", targetString.c_str(), hexIv.c_str());
	return Native::encrypt(targetString.c_str(), hexIv.c_str());
}

std::string SecurityUtil::decrypt(std::string targetString, std::string hexIv) {
//    NSString *key = [EncryptData MD5Hash:@"At5CcmaxnRmE"];
//    
//    NSString *dataStr = [NSString stringWithUTF8String:targetString.c_str()];
//    NSData *data =  [EncryptData createDataWithHexString: dataStr];
//    
//    //    NSString *hexIvStr = @"00000000000000000000000000000000";
//    NSString *hexIvStr = [NSString stringWithUTF8String:hexIv.c_str()];
//    NSData *ivStr =  [EncryptData createDataWithHexString: hexIvStr];
//    
//    NSData *dataDecrypted = [EncryptData crypto:data operation:kCCDecrypt key:key iv:[ivStr bytes]];
//    NSString *stringDecrypted = [[NSString alloc] initWithData:dataDecrypted encoding:NSUTF8StringEncoding];
//    NSLog(@"stringDecrypted: %@", stringDecrypted);
//    
//    return [stringDecrypted UTF8String];

	return Native::decrypt(targetString, hexIv);
}

std::string SecurityUtil::sha256(const char *targetString) {
//    NSString *output = [EncryptData sha256: [NSString stringWithUTF8String:targetString]];
//    //NSLog(@"SecurityUtil::sha256 String: %s", targetString);
//    NSLog(@"SecurityUtil::sha256 sha: %@", output);
//    return [output UTF8String];
	return Native::sha256(targetString);
}

std::string SecurityUtil::getIV() {
//    NSData *data =  [EncryptData generateRandomIV: 16];
//    NSString *encryptStr = [EncryptData convertEncryptDataToHex:data];
//    return [encryptStr UTF8String]

	return Native::getIV();
}

