#include "MainScene.h"
#include "jCommon.h"
#include "MacroDefines.h"
#include "GameDefines.h"
#include "DetailLayer.h"
#include "RaceScene.h"
#include "SoundHelper.h"
#include "UserDefaultManager.h"
#include "ArtLtvMeasurement.h"
#include "MeasurementInformation.h"
#include "SelectFirstCarScene.h"
#include "MissionDialog.h"
#include "WebViewAlertDialog.h"
#include "HeaderStatus.h"
#include "ItemBoxLayer.h"
#include "ItemBoxDialog.h"
#include "OverHoldContentDialog.h"
#include "MessageDialog.h"
#include "RankModel.h"
#include "Native.h"
#include "MeasurementInformation.h"
#include "FadeInLayer.h"
#include "PartsDetailLayer.h"

MainScene::MainScene():
    _mainIndex(MainIndex::MI_HOME)
    , _statusWebView(false)
    , _statusHtmlDialog(false)
    , _isShowing(false)
    , isVisibleSideMenu(false)
{}

MainScene::~MainScene()
{
    Director::getInstance()->getTextureCache()->removeAllTextures();
}

Scene* MainScene::createScene()
{
    Director::getInstance()->getTextureCache()->removeAllTextures();
    SpriteFrameCache::getInstance()->removeSpriteFrames();
    Director::getInstance()->purgeCachedData();

    auto scene = Scene::create();
    scene->addChild(MainScene::create());
    return scene;
}

bool MainScene::init()
{
    if (!Layer::init()) {
        return false;
    }
    UserDefaultManager::initMainScene();
    return true;
}

void MainScene::onEnterTransitionDidFinish()
{
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
    progress->onStart();
    request->getPlayer(CC_CALLBACK_1(MainScene::onResponseGetPlayer, this));
}

void MainScene::onResponseGetPlayer(Json* response)
{
    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::END_FIRST_LOGIN, true);
    Json* data = Json_getItem(Json_getItem(response, "get_player"), "data");
    PLAYERCONTROLLER->updatePlayerController(data);

    // check empty
    if (nullptr == PLAYERCONTROLLER->_playerTeams) {
        transitScene(SelectFirstCarScene::createScene());
        return;
    }

    int leaderCharacterID = PLAYERCONTROLLER->_playerTeams->getLearderCharacterId();
    int mstCharaId = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[leaderCharacterID]->getCharactersId();
    UserDefault::getInstance()->setIntegerForKey("LeaderCharaMst", mstCharaId);

    SOUND_HELPER->playingMainSceneBackgroundMusic();
    ArtLtvMeasurement::send(MEASURMENT_INFO::ART::DAU, (int)PLAYERCONTROLLER->_player->getID());
    ArtLtvMeasurement::send(MEASURMENT_INFO::ART::MAU, (int)PLAYERCONTROLLER->_player->getID());


    initPlist();

    prepareLayers();
    showParts();

    addChild(FadeinLayer::create(), Z_ORDER::Z_BLACK_OUT);
    homeLayer->show();

    // gachaTutorial
    if (PLAYERCONTROLLER->_player->getGameStep() == 1 &&
        PLAYERCONTROLLER->_player->getFreeToken() + PLAYERCONTROLLER->_player->getPayToken() >= SystemSettingModel::getModel()->getTokenForPayGacha()) {
        gachaTutorial();
        return;
    }
    // NEW_PLAYER == false -> start tutorial)
    if (!UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::NEW_PLAYER, false)) {
        startTutorial();
        return;
    }
    UserDefault::getInstance()->setBoolForKey("Tutorial", false);
    _isTutorial = false;

    // show information dialogs
    if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::GO_TO_TITLE_SCENE) == true) {
        auto dialog = DialogView::createTitleBackDialog();
        dialog->_delegate = this;
        addChild(dialog, Z_ORDER::Z_DIALOG);
    }

    if (std::strcmp("get_dialog", Json_getString(data, "request_api", "null")) > 0) {
        UserDefault::getInstance()->setBoolForKey("DialogViewFlag", true);
    }
    if (UserDefault::getInstance()->getBoolForKey("DialogViewFlag") == true) {
        showHtmldialog();
    } else if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::IS_RANK_UP) == true) {
        showRankDialog();
    }
}

void MainScene::initPlist()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon_pag.plist");
}

void MainScene::showParts()
{
    // Footer
    footerLayer = FooterLayer::create();
    footerLayer->_mainScene = this;
    footerLayer->setBage();
    addChild(footerLayer, Z_ORDER::Z_FOOTER);

    // Header
    headerLayer = HeaderLayer::create();
    headerLayer->changeMessage(PLAYERCONTROLLER->_messages.at(1));
    addChild(headerLayer, Z_ORDER::Z_HEADER);

    // side
    _teamEditMainLayer = TeamEditMainLayer::create();
    _teamEditMainLayer->_mainScene = this;
    _teamEditMainLayer->setVisible(true);
    addChild(_teamEditMainLayer, Z_ORDER::Z_SIDE_MENU);

    _garageMainLayer = GarageMainLayer::create();
    _garageMainLayer->_mainScene = this;
    _garageMainLayer->setVisible(true);
    addChild(_garageMainLayer, Z_ORDER::Z_SIDE_MENU);
}

void MainScene::prepareLayers()
{
    // home
    homeLayer = HomeLayer::create();
    homeLayer->_mainScene = this;
    homeLayer->setVisible(false);
    addChild(homeLayer, Z_ORDER::Z_MAIN_CONTENT);

    startLayer = StartLayer::create();
    startLayer->_mainScene = this;
    startLayer->setTag(MI_START);
    startLayer->setVisible(false);
    startLayer->initWithPlayerController();
    addChild(startLayer, Z_ORDER::Z_MAIN_CONTENT);

    shopLayer = ShopLayer::create();
    shopLayer->_mainScene = this;
    shopLayer->setVisible(false);
    shopLayer->setTag(MI_SHOP);
    addChild(shopLayer, Z_ORDER::Z_MAIN_CONTENT);

    gachaLayer = GachaLayer::create();
    gachaLayer->_mainScene = this;
    gachaLayer->setVisible(false);
    gachaLayer->setTag(MI_GACHA);
    addChild(gachaLayer, Z_ORDER::Z_MAIN_CONTENT);

    friendLayer = FriendLayer::create();
    friendLayer->_mainScene = this;
    friendLayer->setVisible(false);
    friendLayer->setTag(MI_FRIEND);
    addChild(friendLayer, Z_ORDER::Z_MAIN_CONTENT);

    othersLayer = OthersLayer::create();
    othersLayer->setVisible(false);
    othersLayer->_mainScene = this;
    othersLayer->setTag(MI_OTHERS);
    addChild(othersLayer, Z_ORDER::Z_MAIN_CONTENT);
}

void MainScene::startTutorial()
{
    _isTutorial = true;
    TutorialLayer* tutorialLayer = TutorialLayer::create();
    tutorialLayer->_mainScene = this;
    addChild(tutorialLayer, Z_ORDER::Z_TUTORIAL);
}

void MainScene::gachaTutorial()
{
    _isTutorial = false;
    UserDefault::getInstance()->setBoolForKey("Tutorial", false);
    if (PLAYERCONTROLLER->_player->getGameStep() < 2) {
        UserDefault::getInstance()->setBoolForKey("GachaFinish", false);
        _isTutorial = true;
        showGachaTutorial();
        TutorialLayer* tutorialLayer = TutorialLayer::create();
        tutorialLayer->_mainScene = this;
        addChild(tutorialLayer, Z_ORDER::Z_TUTORIAL);
        tutorialLayer->gachaTutorial();
    }
}

void MainScene::showRankDialog()
{
    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::IS_RANK_UP, false);
    auto dialog = DialogView::createRankUpDialog();
    dialog->_delegate = this;
    addChild(dialog, Z_ORDER::Z_DIALOG);
}

void MainScene::showStartTutorial()
{
    startLayer->startTutorial();
    getChildByTag(MainIndex::MI_HOME)->setVisible(false);
    _mainIndex = MainIndex::MI_START;
}

void MainScene::showGachaTutorial()
{
    getChildByTag(_mainIndex)->setVisible(false);
    _mainIndex = MainIndex::MI_GACHA;
    getChildByTag(MainIndex::MI_GACHA)->setVisible(true);

    _teamEditMainLayer->setVisible(false);
    _garageMainLayer->setVisible(false);
    othersLayer->setVisible(false);
    gachaLayer->resetPageTag();
    if (UserDefault::getInstance()->getBoolForKey("GachaFinish", false)) {
        gachaLayer->playTutorialGacha();
    }
}


void MainScene::showCostOverDialog()
{
    if (getChildByTag(TAG_SPRITE::COST_OVER_DIALOG)) {
        return;
    }
    auto dialog = MessageDialog::create("編成コストオーバー", "編成コストがオーバーしています。\nチームを再編成をしてください。", [this]() {
        footerLayer->_startTab = 0;
    });
    dialog->setTag(TAG_SPRITE::COST_OVER_DIALOG);
    addChild(dialog, Z_ORDER::Z_DIALOG);
}

const bool MainScene::isTeamCostEceeded()
{
    auto teamModel = PLAYERCONTROLLER->_playerTeams;
    int totalCost = getCharacterCost(teamModel->getLearderCharacterId());
    totalCost += getCharacterCost(teamModel->getSupport1CharacterId());
    totalCost += getCharacterCost(teamModel->getSupport2CharacterId());
    totalCost += getCharacterCost(teamModel->getSupport3CharacterId());

    return totalCost > RankModel::find(PLAYERCONTROLLER->_player->getRank())->getMaxCost();
}

const int MainScene::getCharacterCost(const int playerCharacterId)
{
    if (playerCharacterId < 1) {
        return 0;
    }
    // stupid code
    int masterCharacterId = PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getCharactersId();
    return CharacterModel::find(masterCharacterId)->getCost();
}

void MainScene::showHomeLayer()
{
    if (_isTutorial) {
        return;
    }

    if (getChildByTag(TAG_SPRITE::ITEM_BOX_LAYER)) {
        removeChildByTag(TAG_SPRITE::ITEM_BOX_LAYER);
    }
    if (_statusWebView == true) {
        deleteWebView();
    }
    if (_statusHtmlDialog == true) {
        closeHtmlDialog();
    }

    getChildByTag(_mainIndex)->setVisible(false);

    _mainIndex = MI_HOME;
    homeLayer->setVisible(true);
    othersLayer->setVisible(false);
    _teamEditMainLayer->setVisible(true);
    _garageMainLayer->setVisible(true);
}


void MainScene::showStartLayer()
{
    if (gachaLayer) {
        gachaLayer->resetPageTag();
    }
    Director::getInstance()->getTextureCache()->removeAllTextures();
    std::string content = "";
    if (PLAYERCONTROLLER->_playerCharacterModels.size() > PLAYERCONTROLLER->_player->getGarageSize()) {
        content += "[車なご]";
    }
    if ((int)PLAYERCONTROLLER->_playerParts.size() > PLAYERCONTROLLER->_player->getGarageSize() * 2) {
        content += "[装備]";
    }
    if (content != "") {
        content += "がいっぱいです。";
        showOverHoldDialog(content);
        return;
    }
    if (isTeamCostEceeded()) {
        showCostOverDialog();
        return;
    }

    if (_statusWebView == true) {
        deleteWebView();
    }
    if (_statusHtmlDialog == true) {
        closeHtmlDialog();
    }

    getChildByTag(_mainIndex)->setVisible(false);
    _mainIndex = MainIndex::MI_START;
    startLayer->setVisible(true);
    startLayer->showLayer(StartIndex::ST_MAIN);

    if (getChildByTag(TAG_SPRITE::ITEM_BOX_LAYER)) {
        removeChildByTag(TAG_SPRITE::ITEM_BOX_LAYER);
    }

    _teamEditMainLayer->setVisible(false);
    _garageMainLayer->setVisible(false);
    othersLayer->setVisible(false);
}

void MainScene::showLayer(MainIndex layerIndex)
{
    if (_isTutorial) {
        return;
    }

    if (getChildByTag(TAG_SPRITE::ITEM_BOX_LAYER)) {
        removeChildByTag(TAG_SPRITE::ITEM_BOX_LAYER);
    }

    switch (layerIndex) {
    case MI_FRIEND:
        friendLayer->resetPageTag();
        break;
    case MI_GACHA:
        gachaLayer->resetPageTag();
        break;
    case MI_SHOP:
        shopLayer->resetPageTag();
        break;
    case MI_OTHERS:
        othersLayer->resetPageTag();
        break;
    default:
        break;
    }

    switch (_mainIndex) {
    case MI_SHOP:
        shopLayer->resetPageTag();
        break;
    case MI_FRIEND:
        friendLayer->resetPageTag();
        break;
    case MI_GACHA:
        gachaLayer->resetPageTag();
        break;
    case MI_OTHERS:
        othersLayer->resetPageTag();
        break;
    default:
        break;
    }

    auto currentLayer = getChildByTag(_mainIndex);
    currentLayer->setVisible(false);

    // next layer show effect
    _mainIndex = layerIndex;
    auto nextLayer = getChildByTag(_mainIndex);
    nextLayer->setVisible(true);

    if (_statusWebView == true) {
        deleteWebView();
    }

    if (_statusHtmlDialog == true) {
        closeHtmlDialog();
    }
    switch (layerIndex) {
    case MI_SHOP:
        _teamEditMainLayer->setVisible(false);
        _garageMainLayer->setVisible(false);
        othersLayer->setVisible(false);
        break;
    case MI_GACHA:
        _teamEditMainLayer->setVisible(false);
        _garageMainLayer->setVisible(false);
        othersLayer->setVisible(false);
        break;

    case MI_FRIEND:
        _teamEditMainLayer->setVisible(false);
        _garageMainLayer->setVisible(false);
        othersLayer->setVisible(false);
        break;
    case MI_OTHERS:
        othersLayer->resetPageTag();
        _teamEditMainLayer->setVisible(false);
        _garageMainLayer->setVisible(false);
        break;
    case MI_HOME:
        _teamEditMainLayer->setVisible(true);
        _garageMainLayer->setVisible(true);
        break;
    default:
        break;
    }
}

bool MainScene::isMainTutorial()
{
    return _isTutorial;
}

void MainScene::setMainTutorial(bool flag)
{
    _isTutorial = flag;
}

void MainScene::changeSideZOrder(SideLayerIndex slIndex)
{
    if (slIndex == SLI_TEAM_EDIT) {
        _teamEditMainLayer->setLocalZOrder(Z_ORDER::Z_SIDE_MENU);
        _garageMainLayer->setLocalZOrder(Z_ORDER::Z_SIDE_MENU - 1);
    } else {
        _teamEditMainLayer->setLocalZOrder(Z_ORDER::Z_SIDE_MENU - 1);
        _garageMainLayer->setLocalZOrder(Z_ORDER::Z_SIDE_MENU);
    }
}

void MainScene::showDetailPlayerCharacter(int pcID)
{
    _isShowing = true;
    addChild(DetailLayer::create(DetailCharacterData(pcID)), Z_ORDER::Z_DETAIL_LAYER);
}

void MainScene::showDetailParts(int pID, int playerPId)
{
    _isShowing = true;
    addChild(PartsDetailLayer::create(DetailPartsData(pID, playerPId + 1), false), Z_ORDER::Z_DETAIL_LAYER);
}

void MainScene::showGachaDetailPlayerCharacter(int pcID)
{
    DetailLayer* layer = DetailLayer::create(DetailCharacterData(pcID));
    layer->_gachaFlag = true;
    addChild(layer, Z_ORDER::Z_DETAIL_LAYER);
    _isShowing = true;
}

void MainScene::showGachaDetailParts(int pID, int playerPId)
{
    addChild(PartsDetailLayer::create(DetailPartsData(pID, playerPId), true), Z_ORDER::Z_DETAIL_LAYER);
    _isShowing = true;
}


void MainScene::showFriendCharacter(int playerMstCharacterId, int level, int exercise, int reaction, int decision)
{
    addChild(DetailLayer::create(DetailCharacterData(playerMstCharacterId, level, exercise, reaction, decision)), Z_ORDER::Z_DETAIL_LAYER);
    _isShowing = true;
}

void MainScene::showHtmlTagWebView(std::string htmlTag)
{
    _statusWebView = true;

    _webView = cocos2d::experimental::ui::WebView::create();
    // for iphone : ipad
    int y = (Director::getInstance()->getWinSize().height == 1136) ? WEBVIEW_POSITUON_Y_WIDEVIEW : WEBVIEW_POSITUON_Y;
    setRectNode(_webView,
                WEBVIEW_POSITUON_X_WIDEVIEW,
                y - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF,
                WEBVIEW_WIDTH,
                WEBVIEW_HEIGHT);
    _webView->setOnShouldStartLoading(CC_CALLBACK_2(MainScene::getWebViewClick, this));
    _webView->loadHTMLString(htmlTag, "");
    addChild(_webView, Z_ORDER::Z_WEBVIEW);
}

bool MainScene::getWebViewClick(cocos2d::experimental::ui::WebView* webView, const std::string &url)
{
    if (url == WEBVIEW_ALEART) {
        _webView->setVisible(false);
        auto dialog = WebViewAlertDialog::create(CC_CALLBACK_0(MainScene::onWebViewAlertDialogDicisionCallBack, this),
                                                 CC_CALLBACK_0(MainScene::onWebViewAlertDialogCancelCallBack, this));
        addChild(dialog, Z_ORDER::Z_DIALOG);
    }
    return true;
}

void MainScene::onWebViewAlertDialogDicisionCallBack()
{
    _webView->setVisible(true);
    _webView->evaluateJS(JAVASCRIPT_FUNCTION_NAME);
}

void MainScene::onWebViewAlertDialogCancelCallBack()
{
    _webView->setVisible(true);
}

void MainScene::showUrlWebView(UrlWebViewTag Tag)
{
    std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
    std::string url;
    switch (Tag) {
    case WEB_EVENT: {
        url = systemSettingModel->getEventServerUrl();
        break;
    }
    case WEB_HELP: {
        url = systemSettingModel->getHelpServerUrl();
        break;
    }
    default:
        break;
    }
    Size winSize = Director::getInstance()->getWinSize();
    _webView = cocos2d::experimental::ui::WebView::create();
    addChild(_webView, Z_ORDER::Z_WEBVIEW);
    // update_webview
    if (winSize.height == 1136) {           // for iphone
        setRectNode(_webView, WEBVIEW_POSITUON_X_WIDEVIEW,
                    WEBVIEW_POSITUON_Y_WIDEVIEW - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF,
                    WEBVIEW_WIDTH,
                    WEBVIEW_HEIGHT);
    } else {         // for ipad
        setRectNode(_webView, WEBVIEW_POSITUON_X,
                    WEBVIEW_POSITUON_Y - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF,
                    WEBVIEW_WIDTH,
                    WEBVIEW_HEIGHT);
    }

    _webView->loadURL(url);
    _statusWebView = true;
}

void MainScene::deleteWebView()
{
    if (_webView) {
        _webView->removeFromParent();
    }
    _statusWebView = false;
}


void MainScene::showHtmldialog()
{
    UserDefault::getInstance()->setBoolForKey("DialogViewFlag", false);
    _statusHtmlDialog = true;
    _htmlDialog = HTMLDialog::create();
    _htmlDialog->_delegate = this;
    addChild(_htmlDialog, Z_ORDER::Z_DIALOG);
}

void MainScene::endHtmlDialog()
{
    _statusHtmlDialog = false;
    if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::IS_RANK_UP) == true) {
        showRankDialog();
    }
}

void MainScene::closeHtmlDialog()
{
    _htmlDialog->closeDialog();
    _statusHtmlDialog = false;
}

void MainScene::goTofuelShop()
{
    _mainIndex = MI_SHOP;
    startLayer->setVisible(false);
    auto nextLayer = getChildByTag(_mainIndex);
    nextLayer->setVisible(true);
    _teamEditMainLayer->setVisible(false);
    _garageMainLayer->setVisible(false);
    othersLayer->setVisible(false);
    shopLayer->showLayer(SI_GAS);
}

void MainScene::goToTokenShop()
{
    shopLayer->resetPageTag();
    _mainIndex = MI_SHOP;
    gachaLayer->setVisible(false);
    auto nextLayer = getChildByTag(_mainIndex);
    nextLayer->setVisible(true);
    _teamEditMainLayer->setVisible(false);
    _garageMainLayer->setVisible(false);
    othersLayer->setVisible(false);

    shopLayer->showLayer(SI_TOKEN);
}

int MainScene::checkMoving()
{
    if (_teamEditMainLayer->isMoving()) {
        return 1;
    } else if (_garageMainLayer->isMoving()) {
        return 2;
    }
    return 0;
}


void MainScene::endShowing()
{
    _isShowing = false;
}

void MainScene::goToRaceEfectMain()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto noTouchPriorityLayer = LayerPriority::create();
    addChild(noTouchPriorityLayer);

    _loadingLabel = Label::createWithTTF("Waiting", FONT_NAME_2, 40);
    _loadingLabel->setColor(Color3B::WHITE);
    _loadingLabel->setAnchorPoint(Vec2(0.0, 0.0));
    _loadingLabel->setPosition(Point(winSize.width / 2 - 100, winSize.height / 2));
    auto blackOutLayer = LayerColor::create(Color4B::BLACK, winSize.width, winSize.height);
    blackOutLayer->addChild(_loadingLabel);
    blackOutLayer->setOpacity(80);
    addChild(blackOutLayer, Z_ORDER::Z_BLACK_OUT);
    _count = 0;
    schedule(schedule_selector(MainScene::updateLoadingLabel), 0.2f);
    blackOutLayer->setPosition(Point::ZERO);
    noTouchPriorityLayer->setPosition(Point::ZERO);
}

void MainScene::releaseMesod()
{
    homeLayer = NULL;
    startLayer = NULL;
    shopLayer = NULL;
    gachaLayer = NULL;
    friendLayer = NULL;
    othersLayer = NULL;
    _teamEditMainLayer = NULL;
    _garageMainLayer = NULL;
    footerLayer = NULL;
    headerLayer = NULL;
    _webView = NULL;
    _htmlDialog = NULL;
}



void MainScene::setBlackImage()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto splite = makeSprite("webview_bg_normal.png");
    addChild(splite, Z_ORDER::Z_DIALOG);
    splite->setTag(90);
    splite->setPosition(Point(winSize.width / 2, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - splite->getContentSize().height / 2));
}

void MainScene::removeBlackImage()
{
    if (getChildByTag(90) != nullptr) {
        removeChildByTag(90);
    }
}


void MainScene::updateLoadingLabel(float delta)
{
    switch (_count) {
    case 0: {
        _loadingLabel->setString("Waiting");
        _count++;
        break;
    }
    case 1: {
        _loadingLabel->setString("Waiting.");
        _count++;
        break;
    }
    case 2: {
        _loadingLabel->setString("Waiting..");
        _count++;
        break;
    }
    case 3: {
        _loadingLabel->setString("Waiting...");
        _count = 0;
        break;
    }
    default:
        _count = 0;
        break;
    }
}

void MainScene::resetStartTab()
{
    footerLayer->_startTab = 0;
}


MainIndex MainScene::getMainIndex()
{
    return _mainIndex;
}

void MainScene::showMissionDialog()
{
    if (getChildByTag(TAG_SPRITE::MISSION_DIALOG)) {
        return;
    }
    auto dialog = MissionDialog::create();
    dialog->setTag(TAG_SPRITE::MISSION_DIALOG);
    addChild(dialog, Z_ORDER::Z_DIALOG);
}

void MainScene::showItemBox()
{
    if (getChildByTag(TAG_SPRITE::ITEM_BOX_LAYER)) {
        return;
    }
    isVisibleSideMenu = _teamEditMainLayer->isVisible();
    _teamEditMainLayer->setVisible(false);
    _garageMainLayer->setVisible(false);
    std::shared_ptr<ItemBoxGahcaCallbackModel>callbackModel(new ItemBoxGahcaCallbackModel([this]() {
        headerLayer->setVisible(true);
        footerLayer->setVisible(true);
    },
                                                                                          [this]() {
        headerLayer->setVisible(false);
        footerLayer->setVisible(false);
    }
                                                                                          ,
                                                                                          CC_CALLBACK_1(MainScene::showGachaDetailPlayerCharacter, this),
                                                                                          CC_CALLBACK_1(MainScene::showDetailPlayerCharacter, this),
                                                                                          CC_CALLBACK_2(MainScene::showGachaDetailParts, this),
                                                                                          CC_CALLBACK_2(MainScene::showDetailParts, this)));
    auto itemBoxLayer = ItemBoxLayer::create(CC_CALLBACK_1(MainScene::closeItemBox, this),
                                             CC_CALLBACK_0(HeaderLayer::updateInfoLayer, headerLayer),
                                             CC_CALLBACK_1(MainScene::showItemBoxDialog, this),
                                             callbackModel);
    itemBoxLayer->setTag(TAG_SPRITE::ITEM_BOX_LAYER);
    addChild(itemBoxLayer, Z_ORDER::Z_ITEM_BOX);
}

void MainScene::showItemBoxDialog(const std::shared_ptr<ItemListModel::Item>& itemModel)
{
    if (getChildByTag(TAG_SPRITE::ITEM_BOX_DIALOG)) {
        return;
    }
    auto dialog = ItemBoxDialog::create(itemModel, CC_CALLBACK_1(MainScene::onTapUseItem, this));
    dialog->setTag(TAG_SPRITE::ITEM_BOX_DIALOG);
    addChild(dialog, Z_DIALOG);
}

void MainScene::onTapUseItem(const std::shared_ptr<ItemListModel::Item>& itemModel)
{
    auto itemBoxLayerNode = getChildByTag(TAG_SPRITE::ITEM_BOX_LAYER);
    if (itemBoxLayerNode) {
        static_cast<ItemBoxLayer*>(itemBoxLayerNode)->useItemCallBack(itemModel);
    }
}

void MainScene::closeItemBox(Ref* pSender)
{
    if (!getChildByTag(TAG_SPRITE::ITEM_BOX_LAYER)) {
        return;
    }
    _teamEditMainLayer->setVisible(isVisibleSideMenu);
    _garageMainLayer->setVisible(isVisibleSideMenu);
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    removeChildByTag(TAG_SPRITE::ITEM_BOX_LAYER);
}

void MainScene::showOverHoldDialog(const std::string message)
{
    addChild(OverHoldContentDialog::create(message,
                                           CC_CALLBACK_0(MainScene::goToComposition, this),
                                           CC_CALLBACK_0(MainScene::goToSell, this),
                                           CC_CALLBACK_0(MainScene::goToExtension, this)), Z_ORDER::Z_DIALOG);
}

void MainScene::goToComposition()
{
    footerLayer->_startTab = 0;
    showLayer(MI_HOME);
    changeSideZOrder(SLI_GARAGE);
    _garageMainLayer->showListLayer();
}

void MainScene::goToSell()
{
    footerLayer->_startTab = 0;
    showLayer(MI_HOME);
    changeSideZOrder(SLI_GARAGE);
    _garageMainLayer->showSellLayer();
}

void MainScene::goToExtension()
{
    footerLayer->_startTab = 0;
    showLayer(MI_HOME);
    // ガレージ拡張画面
    showLayer(MI_SHOP);
    dynamic_cast<ShopLayer*>(getChildByTag(MI_SHOP))->showLayer(SI_GARAGE);
}

void MainScene::setRectNode(Node* node, float x, float y, float width, float height)
{
    node->setPosition(Vec2(x + width * 0.5, y + height * 0.5f));
    node->setContentSize(Size(width, height));
}
