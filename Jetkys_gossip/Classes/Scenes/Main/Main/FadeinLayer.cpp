#include "FadeinLayer.h"
#include "DisableEventListener.h"

bool FadeinLayer::init()
{
    if (!LayerColor::initWithColor(Color4B::BLACK)) {
        return false;
    }
    listener = DisableEventListener::create();
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    runAction(Sequence::create(FadeTo::create(0.8, 0),
                               CallFunc::create(CC_CALLBACK_0(FadeinLayer::remove, this)),
                               NULL));
    return true;
}

void FadeinLayer::remove()
{
    getEventDispatcher()->removeEventListener(listener);
    removeFromParent();
}