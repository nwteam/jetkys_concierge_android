#ifndef __Syanago__MainScene__
#define __Syanago__MainScene__

#include "cocos2d.h"
#include "FooterLayer.h"
#include "HeaderLayer.h"
#include "Loading.h"
#include "DialogView.h"
#include "PlayerController.h"
#include "HomeLayer.h"
#include "HTMLDialog.h"
#include "OthersUseURL.h"
#include "StartLayer.h"
#include "ShopLayer.h"
#include "GachaLayer.h"
#include "FriendLayer.h"
#include "OthersLayer.h"
#include "TeamEditMainLayer.h"
#include "GarageMainLayer.h"
#include "TutorialLayer.h"
#include "SoundHelper.h"
#include "ui/CocosGUI.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"
#include "ItemListModel.h"

USING_NS_CC;
using namespace SyanagoAPI;

enum MainIndex {
    MI_HOME = 0,
    MI_START,
    MI_SHOP,
    MI_GACHA,
    MI_FRIEND,
    MI_OTHERS,
};

enum SideLayerIndex {
    SLI_TEAM_EDIT,
    SLI_GARAGE,
};

enum UrlWebViewTag {
    WEB_EVENT,
    WEB_HELP,
};

class MainScene : public Layer , public HtMLDialogDelegate, public DialogDelegate {
public:
    enum TAG_SPRITE {
        MISSION_DIALOG = 12,
        ITEM_BOX_LAYER,
        ITEM_BOX_DIALOG,
        COST_OVER_DIALOG,
    };
    
    MainScene();
    ~MainScene();
    
    static Scene *createScene();
    CREATE_FUNC(MainScene);
    bool init();
    
    virtual void onEnterTransitionDidFinish();
    
    void startTutorial();
    
    void showStartLayer();
    void showHomeLayer();
    void showLayer(MainIndex layerIndex);
    
    CC_SYNTHESIZE(MainIndex, _mainIndex, MIndex);
    
    void changeSideZOrder(SideLayerIndex slIndex);
    
    void showDetailPlayerCharacter(int pcID);
    void showGachaDetailPlayerCharacter(int pcID);
    void showDetailParts(int pID, int playerPId);
    void showGachaDetailParts(int pID, int playerPId);
    void showFriendCharacter(int playerMstCharacterId, int level, int exercise, int reaction, int decision);
    
    void showHtmlTagWebView(std::string htmlTag);
    void showUrlWebView(UrlWebViewTag Tag);
    void deleteWebView();
    
    void goTofuelShop();
    void goToTokenShop();
    
    void showHtmldialog();
    //HtMLDialogDelegate
    void closeHtmlDialog();
    void endHtmlDialog();
    
    //tutorial
    bool isMainTutorial();
    void setMainTutorial(bool flag);
    void showStartTutorial();
    void showGachaTutorial();
    
    void setBlackImage();
    void removeBlackImage();
    
    int checkMoving();
    
    void endShowing();
    
    void showMissionDialog();
    void showItemBox();
    void showItemBoxDialog(const std::shared_ptr<ItemListModel::Item>& itemModel);
    void onTapUseItem(const std::shared_ptr<ItemListModel::Item>& itemModel);
    
    void goToRaceEfectMain();
    
    void resetStartTab();
    void releaseMesod();
    
    void updateLoadingLabel(float delta);
    MainIndex getMainIndex();
    
    HomeLayer* homeLayer;
    ShopLayer* shopLayer;
    GachaLayer* gachaLayer;
    FriendLayer *friendLayer;
    OthersLayer * othersLayer;
    StartLayer* startLayer;
    
    FooterLayer* footerLayer;
    HeaderLayer* headerLayer;
    
    //detail is Showing
    bool _isShowing;
    
    bool getWebViewClick(cocos2d::experimental::ui::WebView* webView, const std::string &url);
    void onWebViewAlertDialogDicisionCallBack();
    void onWebViewAlertDialogCancelCallBack();
    
    void showOverHoldDialog(const std::string message);
private:
    
    
    enum Z_ORDER{
        Z_MAIN_CONTENT = 0,
        Z_ITEM_BOX,
        Z_FOOTER,
        Z_SIDE_MENU,
        Z_HEADER,
        Z_DETAIL_LAYER,
        Z_DIALOG,
        Z_WEBVIEW = 50,
        Z_BLACK_OUT = 100,
        Z_TUTORIAL = 1000,
    };
    
    const int WEBVIEW_HEIGHT = 547;
    const int WEBVIEW_WIDTH = 576;
    const int WEBVIEW_POSITUON_X = 30;
    const int WEBVIEW_POSITUON_X_WIDEVIEW = 32;
    const int WEBVIEW_POSITUON_Y = 163 + 40 + 150 - 150 + 15 - 31 - 20 -10;
    const int WEBVIEW_POSITUON_Y_WIDEVIEW = 163 + 40 + 150 + 15 - 31;
    const std::string WEBVIEW_ALEART = "syanagowebview://showAlert";//メモ [syanagowebview]は自動的に小文字になります
    const std::string JAVASCRIPT_FUNCTION_NAME = "syanagoShowAlertDicisionCallBack()";//TODO webのfunctionメソッド名
    
    void initPlist();
    void showParts();
    void prepareLayers();
    
    void onResponseGetPlayer(Json* response);
    
    void gachaTutorial();
    
    void showRankDialog();
    
    void goToComposition();
    void goToSell();
    void goToExtension();
    
    bool isVisibleSideMenu;
    void closeItemBox(Ref* pSender);
    
    const bool isTeamCostEceeded();
    const int getCharacterCost(const int playerCharacterId);
    void showCostOverDialog();

    void setRectNode(Node *node, float x, float y, float width, float height);
    DefaultProgress* progress;
    RequestAPI* request;
    
    TeamEditMainLayer *_teamEditMainLayer;
    GarageMainLayer *_garageMainLayer;
    
    cocos2d::experimental::ui::WebView* _webView;
    
    bool _statusWebView;
    HTMLDialog* _htmlDialog;
    bool _statusHtmlDialog;
    bool _isTutorial;
    
    Label* _loadingLabel;
    int _count;
    
};


#endif /* defined(__Syanago__MainScene__) */
