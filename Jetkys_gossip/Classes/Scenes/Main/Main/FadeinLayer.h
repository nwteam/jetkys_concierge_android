#ifndef __syanago__FadeinLayer__
#define __syanago__FadeinLayer__

#include "cocos2d.h"
#include "DisableEventListener.h"

USING_NS_CC;

class FadeinLayer : public LayerColor
{
public:
    CREATE_FUNC(FadeinLayer);
    bool init();
private:
    void remove();
    DisableEventListener* listener;
};



#endif /* defined(__syanago__FadeinLayer__) */
