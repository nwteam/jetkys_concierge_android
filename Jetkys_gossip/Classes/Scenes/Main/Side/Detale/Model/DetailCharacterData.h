#ifndef __syanago__DetailCharacterData__
#define __syanago__DetailCharacterData__

#include "cocos2d.h"
#include <map>
#include "PlayerController.h"
#include "CharacterModel.h"
#include "LeaderSkillNameModel.h"
#include "SkillNameModel.h"
#include "SkillModel.h"
#include "BodyTypeModel.h"

namespace EXP_DATA{
    static const char* EXP_SCALE = "expScale";
    static const char* NEXT_LEVEL_UP_EXP = "nextLevelUpExp";
}

class DetailCharacterData
{
public:
    CC_SYNTHESIZE_READONLY(int, playerCharacterId, PlayerCharacterId);
    CC_SYNTHESIZE_READONLY(int, dearValue, DearValue);
    CC_SYNTHESIZE_READONLY(int, masterCharacterId, MasterCharacterId);
    CC_SYNTHESIZE_READONLY(int, level, Level);
    CC_SYNTHESIZE_READONLY(int, power, Power);
    CC_SYNTHESIZE_READONLY(int, technique, Technique);
    CC_SYNTHESIZE_READONLY(int, brake, Brake);
    CC_SYNTHESIZE_READONLY(std::string, name, Name);
    CC_SYNTHESIZE_READONLY(std::string, vehicleModeName, VehicleModeName);//typeName
    CC_SYNTHESIZE_READONLY(int, cost, Cost);
    CC_SYNTHESIZE_READONLY(int, rarity, Rarity);
    CC_SYNTHESIZE_READONLY(int, bodyTypeId, BodyTypeId);
    CC_SYNTHESIZE_READONLY(std::string, bodyTypeName, BodyTypeName);
    CC_SYNTHESIZE_READONLY(int, leaderSkillId, LeaderSkillIs);
    CC_SYNTHESIZE_READONLY(std::string, leaderSkillName, LeaderSkillName);
    CC_SYNTHESIZE_READONLY(std::string, leaderSkillDescription, LeaderSkillDescription);
    CC_SYNTHESIZE_READONLY(int, skillId, SkillId);
    CC_SYNTHESIZE_READONLY(std::string, skillName, SkillName);
    CC_SYNTHESIZE_READONLY(std::string, skillDescription, SkillDescription);
    CC_SYNTHESIZE_READONLY(int, skillMotionTurn, SkillMotionTurn);
    CC_SYNTHESIZE_READONLY(bool, isFriend, IsFriend);
    
    DetailCharacterData():
    playerCharacterId(0),
    masterCharacterId(1),
    level(1),
    power(0),
    technique(0),
    brake(0),
    name(""),
    vehicleModeName(""),
    cost(0),
    rarity(1),
    bodyTypeId(1),
    bodyTypeName(""),
    leaderSkillId(1),
    leaderSkillName(""),
    leaderSkillDescription(""),
    skillId(1),
    skillName(""),
    skillDescription(""),
    skillMotionTurn(0),
    isFriend(false){}
    
    DetailCharacterData(int tempPlayerCharacterId):
    playerCharacterId(0),
    masterCharacterId(1),
    level(1),
    power(0),
    technique(0),
    brake(0),
    name(""),
    vehicleModeName(""),
    cost(0),
    rarity(1),
    bodyTypeId(1),
    bodyTypeName(""),
    leaderSkillId(1),
    leaderSkillName(""),
    leaderSkillDescription(""),
    skillId(1),
    skillName(""),
    skillDescription(""),
    skillMotionTurn(0),
    isFriend(false)
    {
        if(tempPlayerCharacterId < 1){
            return;
        }
        
        isFriend = false;
        playerCharacterId = tempPlayerCharacterId;
        masterCharacterId = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getCharactersId();
        dearValue = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getDearValue();
        level = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getLevel();
        power = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getExercise();
        technique = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getReaction();
        brake = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getDecision();
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(masterCharacterId));
        name = characterModel->getCarName();
        vehicleModeName = characterModel->getName();
        rarity = characterModel->getRarity();
        cost = characterModel->getCost();
        bodyTypeId = characterModel->getBodyType();
        leaderSkillId = characterModel->getLeaderSkill();
        auto leaderSkillNameId = characterModel->getLeaderSkillName();
        skillId = characterModel->getSkill();
        std::shared_ptr<LeaderSkillNameModel>leaderSkillNameModel(LeaderSkillNameModel::find(leaderSkillNameId));
        leaderSkillName = leaderSkillNameModel->getName();
        leaderSkillDescription = leaderSkillNameModel->getDescription();
        std::shared_ptr<SkillNameModel>skillNameModel(SkillNameModel::find(skillId));
        skillName =skillNameModel->getName();
        skillDescription = skillNameModel->getDescription();
        std::shared_ptr<SkillModel>skillModel(SkillModel::find(skillId));
        skillMotionTurn = skillModel->getTriggerConditions();
        std::shared_ptr<BodyTypeModel>bodyTypeModel(BodyTypeModel::find(bodyTypeId));
        bodyTypeName = bodyTypeModel->getName();
    }
    
    DetailCharacterData(int tempMasterCharacterId, int tempLevel, int tempPower, int tempTechnique, int tempBrake):
    playerCharacterId(0),
    masterCharacterId(1),
    level(1),
    power(0),
    technique(0),
    brake(0),
    name(""),
    vehicleModeName(""),
    cost(0),
    rarity(1),
    bodyTypeId(1),
    bodyTypeName(""),
    leaderSkillId(1),
    leaderSkillName(""),
    leaderSkillDescription(""),
    skillId(1),
    skillName(""),
    skillDescription(""),
    isFriend(false)
    {
        isFriend = true;
        masterCharacterId = tempMasterCharacterId;
        level = tempLevel;
        power = tempPower;
        technique = tempTechnique;
        brake = tempBrake;
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(masterCharacterId));
        name = characterModel->getCarName();
        vehicleModeName = characterModel->getName();
        rarity = characterModel->getRarity();
        cost = characterModel->getCost();
        bodyTypeId = characterModel->getBodyType();
        leaderSkillId = characterModel->getLeaderSkillName();
        auto leaderSkillNameId = characterModel->getLeaderSkillName();
        skillId = characterModel->getSkill();
        std::shared_ptr<LeaderSkillNameModel>leaderSkillNameModel(LeaderSkillNameModel::find(leaderSkillNameId));
        leaderSkillName = leaderSkillNameModel->getName();
        leaderSkillDescription = leaderSkillNameModel->getDescription();
        std::shared_ptr<SkillNameModel>skillNameModel(SkillNameModel::find(skillId));
        skillName = skillNameModel->getName();
        skillDescription = skillNameModel->getDescription();
        std::shared_ptr<SkillModel>skillModel(SkillModel::find(skillId));
        skillMotionTurn = skillModel->getTriggerConditions();
        std::shared_ptr<BodyTypeModel>bodyTypeModel(BodyTypeModel::find(bodyTypeId));
        bodyTypeName = bodyTypeModel->getName();
    }
    
    std::map<std::string, float> getExpViewData();
    std::vector<int> getDice(){
        return PLAYERCONTROLLER->getPlayerCharacterDiceValues(playerCharacterId);
    }
    std::vector<int> getEquipmentParts(){
        return PLAYERCONTROLLER->getPlayerCharacterPartsIds(playerCharacterId);
    }
    
};

#endif
