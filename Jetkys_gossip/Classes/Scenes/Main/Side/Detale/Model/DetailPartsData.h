#ifndef __syanago__DetailPartsData__
#define __syanago__DetailPartsData__

#include "cocos2d.h"
#include "PlayerController.h"
#include "PartModel.h"

class DetailPartsData
{
public:
    CC_SYNTHESIZE_READONLY(int, playerPartsId, PlyaerPartsId);
    CC_SYNTHESIZE_READONLY(int, masterPartsId, MasterPartsId);
    CC_SYNTHESIZE_READONLY(std::string, name, Name);
    CC_SYNTHESIZE_READONLY(int, rarity, Rarity);
    CC_SYNTHESIZE_READONLY(int, power, Power);
    CC_SYNTHESIZE_READONLY(int, technique, Technique);
    CC_SYNTHESIZE_READONLY(int, brake, Brake);
    CC_SYNTHESIZE_READONLY(std::string, description, Description);
    
    DetailPartsData():
    playerPartsId(0),
    masterPartsId(1),
    name(""),
    rarity(1),
    power(0),
    technique(0),
    brake(0),
    description("")
    {}
    
    DetailPartsData(int tempMasterPartsId, int tempPlayerPartsId):
    playerPartsId(0),
    masterPartsId(1),
    name(""),
    rarity(1),
    power(0),
    technique(0),
    brake(0),
    description("")
    {
        playerPartsId = tempPlayerPartsId;
        masterPartsId = tempMasterPartsId;
        std::shared_ptr<PartModel>partsModel(PartModel::find(masterPartsId));
        name  = partsModel->getName();
        rarity = partsModel->getRarity();
        power =partsModel->getExercise();
        technique = partsModel->getReaction();
        brake = partsModel->getDecision();
        description = partsModel->getRemarks();
    }
    
};

#endif