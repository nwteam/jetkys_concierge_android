#include "DetailCharacterData.h"
#include "LevelModel.h"

std::map<std::string, float>DetailCharacterData::getExpViewData()
{
    std::map<std::string, float>result;
    float nowExp = (float)PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getExp();
    int nextLevel = level + 1;
    if (nextLevel <= 99) {
        std::shared_ptr<LevelModel>levelModel(LevelModel::find(level));
        float currentLevelexp = levelModel->getRequiredExp();
        std::shared_ptr<LevelModel>nextLevelModel(LevelModel::find(level));
        float nextExp = nextLevelModel->getRequiredExp();
        result[EXP_DATA::NEXT_LEVEL_UP_EXP] = nextExp - nowExp;
        result[EXP_DATA::EXP_SCALE] = result[EXP_DATA::NEXT_LEVEL_UP_EXP] / (nextExp - currentLevelexp);
    } else {
        result[EXP_DATA::NEXT_LEVEL_UP_EXP]  = 0;
        result[EXP_DATA::EXP_SCALE] = 1;
    }
    return result;
}