#ifndef __syanago__DetailLayer__
#define __syanago__DetailLayer__

#include "cocos2d.h"
#include "PlayerController.h"
#include "FontDefines.h"
#include "LayerPriority.h"
#include "DetailCharacterData.h"
#include "DetailPartsData.h"
#include "create_func.h"

#define DETAILLAYER_ZORDER 99

USING_NS_CC;

class DetailLayer : public LayerPriority, public create_func<DetailLayer> {
public:
    DetailLayer();
    virtual ~DetailLayer();
    using create_func::create;
    bool init(DetailCharacterData characterData);
    typedef std::function<void()> endCallBack;
    bool init(DetailCharacterData characterData, const endCallBack& callback);
    
    bool _gachaFlag;
    
private:
    enum TAG_SPRITE{
        STATUS_LAYER = 0,
        CHARACTER_LAYER,
        PARTS_LAYER,
    };
    const int PARTS_TAG_NUMBER = 1000;
    const int kPriorityDetailLayer =  -550;
    const int kPriorityDetailMenu = (kPriorityDetailLayer - 2);
    
    bool onTouchBegan(cocos2d::Touch* touch,cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    
    int _charaMstId;
    int _partsMastId;
    int _partsCount;
    float _maxScale;
    bool _selectParts1;
    bool _selectParts2;
    bool _statusViewFlag;
    
    bool isTouch;
    
    Sprite* _partsText1;
    Sprite* _partsText2;
    
    endCallBack _callback;
    
    void showBackGround();
    Sprite* createStatusBase();
    void showRarity(const int rarity, Sprite* icon, Sprite* statusBase);
    Sprite* createCharacterImage(const int masterCharacterId);
    Sprite* createIconImage(const std::string filename, Sprite* statusBase);
    void showCharacterIconStatus(const int playerCharacterId, Sprite* icon);
    Label* createNameLabel(const std::string name, Sprite* icon);
    Label* createLevelLabel(const int playerCharacterLebel, Sprite* icon, Sprite* statusBase);
    Label* createVehicleModelLabel(const std::string vehcleModelName, Sprite* statusBase, Sprite* icon);
    Label* createBodyTypeLabel(const std::string bodyTypeName, Sprite* bodyTypeBase);
    Label* createCharacterCost(const int characterCost, Sprite* bodyTypeBase, Label* bodyTypeLabel);
    Sprite* createCharacterDiceSet(std::vector<int> diceSet, Sprite* statusBase, Label* lebelLabel);
    Sprite* createBodyTypeBase(Sprite* icon);
    Sprite* createExpBar(const int nextLevelUpexp, const float expBarScale, Sprite* bodyTypeBase, Label* levelLabel);
    Sprite* createCharacterStatusView(const int power, const int technique, const int brake, Sprite* statusBase, const float positionY);
    Sprite* createLeaderSkillView(const std::string leaderSkillName, const std::string leaderSkillDescription, Sprite* statusBase,Sprite* statusValueBase);
    Sprite* createSkillView(const std::string skillName, const std::string skillDescription, const int skillMotionTurn, Sprite* leaderSkilBase);
    Layer* createPlayerCharacterPartsLayer(const std::vector<int> parts, Sprite* statusBase);
    Sprite* createDearValueView(const int dearValue);
    
    
    void setCallBack(const endCallBack& callback);

    void showSceneBackButton();
    void onTapBackButton(Ref* pSender);
};


#endif /* defined(__syanago__DetailLayer__) */
