#ifndef __syanago__PartsDetailLayer__
#define __syanago__PartsDetailLayer__


#include "cocos2d.h"
#include "PlayerController.h"
#include "FontDefines.h"
#include "LayerPriority.h"
#include "DetailCharacterData.h"
#include "DetailPartsData.h"
#include "create_func.h"

#define DETAILLAYER_ZORDER 99

USING_NS_CC;

class PartsDetailLayer : public LayerPriority, public create_func<PartsDetailLayer> {
public:
    PartsDetailLayer();
    ~PartsDetailLayer();
    using create_func::create;
    bool init(DetailPartsData data, const bool gachaFlag);
    
private:
    enum TAG_SPRITE{
        STATUS_LAYER = 0,
        CHARACTER_LAYER,
        PARTS_LAYER,
    };
    const int PARTS_TAG_NUMBER = 1000;
    const int kPriorityDetailLayer =  -550;
    const int kPriorityDetailMenu = (kPriorityDetailLayer - 2);
    
    void showBackGround();
    Sprite* createPartsImage(const int masterPartsId);
    Sprite* createStatusBase();
    Sprite* createIconImage(const std::string filename, Sprite* statusBase);
    void showPartsIconStatus(const int playerPartsId, Sprite* icon);
    Label* createNameLabel(const std::string name, Sprite* icon);
    void showRarity(const int rarity, Sprite* icon, Sprite* statusBase);
    Sprite* createPartsStatusView(const int power, const int technique, const int brake, const std::string description, Sprite* statusBase);
    void showSceneBackButton();
    void btnBackCallBack(Ref* pSender);
    
    bool onTouchBegan(cocos2d::Touch* touch,cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    
    int _partsMastId;
    int _partsCount;
    float _maxScale;
    bool _selectParts1;
    bool _selectParts2;
    bool _statusViewFlag;
    bool _gachaFlag;
    bool isTouch;
    
    Sprite* _partsText1;
    Sprite* _partsText2;
    
    
    
    
};



#endif /* defined(__syanago__PartsDetailLayer__) */
