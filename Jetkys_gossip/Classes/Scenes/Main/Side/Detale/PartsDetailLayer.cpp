#include "PartsDetailLayer.h"
#include "SoundHelper.h"
#include "MainScene.h"
#include "TelopScrollView.h"
#include "LevelModel.h"

PartsDetailLayer::PartsDetailLayer():
    _partsCount(0)
    , _partsMastId(1)
    , _maxScale(1.0)
    , isTouch(true)
    , _statusViewFlag(true)
    , _gachaFlag(true)
{}

PartsDetailLayer::~PartsDetailLayer()
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("icon_pab.plist");
}


bool PartsDetailLayer::init(DetailPartsData partsData, const bool gachaFlag)
{
    if (!LayerPriority::initWithPriority(kPriorityDetailLayer)) {
        return false;
    }
    _gachaFlag = gachaFlag;
    showBackGround();

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(PartsDetailLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(PartsDetailLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(PartsDetailLayer::onTouchEnded, this);

    auto charaTouchLayer = Layer::create();
    charaTouchLayer->setTag(TAG_SPRITE::CHARACTER_LAYER);
    charaTouchLayer->addChild(createPartsImage(partsData.getMasterPartsId()));
    addChild(charaTouchLayer);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, charaTouchLayer);

    auto statusLayer = Layer::create();
    statusLayer->setTag(TAG_SPRITE::STATUS_LAYER);
    auto statusbase = createStatusBase();
    auto icon = createIconImage(StringUtils::format("%d_pag.png", partsData.getMasterPartsId()), statusbase);
    showPartsIconStatus(partsData.getPlyaerPartsId(), icon);
    statusbase->addChild(icon);

    auto nameLabel = createNameLabel(partsData.getName(), icon);
    statusbase->addChild(nameLabel);

    showRarity(partsData.getRarity(), icon, statusbase);

    statusbase->addChild(createPartsStatusView(partsData.getPower(), partsData.getTechnique(), partsData.getBrake(), partsData.getDescription(), statusbase));
    statusLayer->addChild(statusbase);
    addChild(statusLayer);

    if (_gachaFlag == false) {
        showSceneBackButton();
    }
    return true;
}

void PartsDetailLayer::showBackGround()
{
    auto BgSp = Sprite::create("home_bg.png");
    BgSp->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    addChild(BgSp);
}

Sprite* PartsDetailLayer::createPartsImage(const int masterPartsId)
{
    _partsMastId = masterPartsId;
    _maxScale = 1;

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon_pab.plist");
    Sprite* result = makeSprite(StringUtils::format("%d_pab.png", masterPartsId).c_str());
    result->setAnchorPoint(Vec2(0.5, 0.5));
    result->setPosition(Director::getInstance()->getWinSize() / 2);

    auto shadowSprite = Sprite::create("detail_parts_shadow.png");
    shadowSprite->setAnchorPoint(Vec2(0.5, 0.5));
    shadowSprite->setPosition(Point(result->getContentSize().width / 2, result->getContentSize().height * 0.1));
    result->addChild(shadowSprite, -1);
    return result;
}

Sprite* PartsDetailLayer::createStatusBase()
{
    auto result = makeSprite("detale_base.png");
    Size winSize = Director::getInstance()->getWinSize();
    result->setPosition(Point(winSize.width / 2, result->getContentSize().height / 2 + 12));
    return result;
}

Sprite* PartsDetailLayer::createIconImage(const std::string filename, Sprite* satatusBase)
{
    auto result = makeSprite(filename.c_str());
    result->setPosition(Point(result->getContentSize().width / 2 + 14,
                              satatusBase->getContentSize().height - 14 - result->getContentSize().height / 2));
    return result;
}

void PartsDetailLayer::showPartsIconStatus(const int playerPartsId, Sprite* icon)
{
    bool patsflag = false;
    for (std::pair<int, PlayerCharactersModel*>pair : PLAYERCONTROLLER->_playerCharacterModels) {
        if (playerPartsId == pair.second->isAnyPartsEquiped()) {
            patsflag = true;
        }
    }

    if (patsflag == true) {
        auto usingImage = makeSprite("garage_list_using_label.png");
        usingImage->setPosition(Vec2(icon->getContentSize().width - 23, icon->getContentSize().height - 15));
        icon->addChild(usingImage);
    }
}

Label* PartsDetailLayer::createNameLabel(const std::string name, Sprite* icon)
{
    auto result = Label::createWithTTF(name, FONT_NAME_2, 28);
    result->setPosition(Point(icon->getPosition().x + (icon->getContentSize().width * 0.5) + (result->getContentSize().width * 0.5) + 10,
                              icon->getPosition().y + (icon->getContentSize().height * 0.5) - result->getContentSize().height * 0.5));
    result->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    return result;
}

void PartsDetailLayer::showRarity(const int rarity, Sprite* icon, Sprite* statusBase)
{
    auto rankPoint = Point(icon->getPosition().x +  (icon->getContentSize().width * 0.5) + 10 + 26 / 2,
                           icon->getPosition().y);
    for (int i = 1; i <= rarity; i++) {
        auto rank = makeSprite("other_rarity.png");
        statusBase->addChild(rank);
        rank->setPosition(rankPoint);
        rankPoint += Vec2(26 + 2, 0);
    }
}

Sprite* PartsDetailLayer::createPartsStatusView(const int power, const int technique, const int brake, const std::string description, Sprite* statusBase)
{
    auto result = makeSprite("dettale_parts_base.png");
    result->setPosition(Point(statusBase->getContentSize().width / 2, result->getContentSize().height / 2 + 14));

    auto label = Label::createWithTTF("効果", FONT_NAME_2, 22);
    result->addChild(label);
    label->setColor(Color3B(255, 173, 194));
    label->setPosition(Point(10 + label->getContentSize().width / 2,
                             result->getContentSize().height - label->getContentSize().height / 2 - 10));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto statusPosi = Point(10, label->getPosition().y - 20);
    if (power > 0) {
        auto powerLabel = Label::createWithTTF("馬力: +" + FormatWithCommas(power), FONT_NAME_2, 22);
        result->addChild(powerLabel, 1);
        powerLabel->setPosition(statusPosi + Vec2(powerLabel->getContentSize().width / 2,
                                                  -powerLabel->getContentSize().height / 2));
        statusPosi += Vec2(powerLabel->getContentSize().width + 10, 0);
    }

    if (technique > 0) {
        auto techniqueLabel = Label::createWithTTF("技量: +" + FormatWithCommas(technique), FONT_NAME_2, 22);
        result->addChild(techniqueLabel, 1);
        techniqueLabel->setPosition(statusPosi + Vec2(techniqueLabel->getContentSize().width / 2,
                                                      -techniqueLabel->getContentSize().height / 2));
        statusPosi += Vec2(techniqueLabel->getContentSize().width + 10, 0);
    }

    if (brake > 0) {
        auto deciseLabel = Label::createWithTTF("忍耐: +" + FormatWithCommas(brake), FONT_NAME_2, 22);
        result->addChild(deciseLabel, 1);
        deciseLabel->setPosition(statusPosi + Vec2(deciseLabel->getContentSize().width / 2,
                                                   -deciseLabel->getContentSize().height / 2));
        statusPosi += Vec2(deciseLabel->getContentSize().width + 10, 0);
    }
    if (power > 0 || technique > 0 || brake > 0) {
        statusPosi -= Vec2(0, 27);
        statusPosi = Vec2(10, statusPosi.y);
    }
    auto disLabel = Label::createWithTTF(description, FONT_NAME_2, 22);
    result->addChild(disLabel, 1);
    disLabel->setDimensions(result->getContentSize().width - 20, 0);
    disLabel->setPosition(statusPosi + Vec2(disLabel->getContentSize().width / 2,
                                            -disLabel->getContentSize().height / 2));

    return result;
}

void PartsDetailLayer::showSceneBackButton()
{
    auto btBack = makeMenuItem("back_button.png", CC_CALLBACK_1(PartsDetailLayer::btnBackCallBack, this));
    auto menu = MenuPriority::createWithPriority(kPriorityDetailMenu);
    menu->addChild(btBack);
    menu->setPosition(Vec2(14 + btBack->getContentSize().width / 2, Director::getInstance()->getWinSize().height - btBack->getContentSize().width / 2));
    addChild(menu);
}

void PartsDetailLayer::btnBackCallBack(Ref* pSender)
{
    isTouch = false;
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    auto mainScene = dynamic_cast<MainScene*>(getParent());
    if (mainScene) {
        mainScene->endShowing();
    }
    removeFromParent();
}

bool PartsDetailLayer::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (!isTouch) {
        return false;
    }
    if (_gachaFlag == false) {
        for (int i = 0; i < _partsCount; i++) {
            Node* pCard = getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->getChildByTag(TAG_SPRITE::PARTS_LAYER)->getChildByTag(PARTS_TAG_NUMBER + i);
            if (pCard) {
                Rect cardRect = pCard->getBoundingBox();
                if (cardRect.containsPoint(CCDirector::getInstance()->convertToGL(touch->getLocationInView()))) {
                    if (i == 0) {
                        _partsText1->setVisible(true);
                    } else if (i == 1) {
                        _partsText2->setVisible(true);
                    }
                    return true;
                }
            }
        }
        if (_statusViewFlag == true) {
            getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->runAction(Sequence::create(ScaleTo::create(0.2, _maxScale), NULL));
            getChildByTag(TAG_SPRITE::STATUS_LAYER)->setVisible(false);
            _statusViewFlag = false;
            if (_partsCount > 0) {
                getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->getChildByTag(TAG_SPRITE::PARTS_LAYER)->setVisible(false);
            }
        } else {
            getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->runAction(Sequence::create(ScaleTo::create(0.2, 1.0), NULL));
            getChildByTag(TAG_SPRITE::STATUS_LAYER)->setVisible(true);
            _statusViewFlag = true;
            if (_partsCount > 0) {
                getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->getChildByTag(TAG_SPRITE::PARTS_LAYER)->setVisible(true);
            }
        }
    } else {
        btnBackCallBack(this);
    }
    return true;
}

void PartsDetailLayer::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (_gachaFlag == false && _statusViewFlag == true) {
        for (int i = 0; i < _partsCount; i++) {
            Node* pCard = getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->getChildByTag(TAG_SPRITE::PARTS_LAYER)->getChildByTag(PARTS_TAG_NUMBER + i);
            if (pCard) {
                Rect cardRect = pCard->getBoundingBox();
                if (cardRect.containsPoint(CCDirector::getInstance()->convertToGL(touch->getLocationInView()))) {
                    if (i == 0) {
                        _partsText1->setVisible(true);
                    } else if (i == 1) {
                        _partsText2->setVisible(true);
                    }
                    return;
                } else {
                    if (_partsCount == 1) {
                        _partsText1->setVisible(false);
                    } else if (_partsCount == 2) {
                        _partsText2->setVisible(false);
                        _partsText1->setVisible(false);
                    }
                }
            }
        }
    }
}

void PartsDetailLayer::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (_gachaFlag == false) {
        if (_statusViewFlag == true) {
            if (_partsCount == 1) {
                _partsText1->setVisible(false);
            } else if (_partsCount == 2) {
                _partsText2->setVisible(false);
                _partsText1->setVisible(false);
            }
        }
    }
}





















