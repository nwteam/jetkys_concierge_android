#include "DetailLayer.h"
#include "SoundHelper.h"
#include "MainScene.h"
#include "TelopScrollView.h"
#include "LevelModel.h"

DetailLayer::DetailLayer():
    _partsCount(0)
    , _charaMstId(1)
    , _partsMastId(1)
    , _maxScale(1.0)
    , isTouch(true)
    , _statusViewFlag(true)
    , _gachaFlag(false)
    , _callback(nullptr)
{}

DetailLayer::~DetailLayer()
{
    if (_charaMstId > 0) {
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_ca.plist", _charaMstId).c_str());
    }
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("icon_pab.plist");
}


bool DetailLayer::init(DetailCharacterData characterData, const endCallBack& callback)
{
    _callback = callback;
    return init(characterData);
}

bool DetailLayer::init(DetailCharacterData characterData)
{
    if (!LayerPriority::initWithPriority(kPriorityDetailLayer)) {
        return false;
    }
    showBackGround();

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(DetailLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(DetailLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(DetailLayer::onTouchEnded, this);

    auto charaTouchLayer = Layer::create();
    charaTouchLayer->setTag(TAG_SPRITE::CHARACTER_LAYER);
    charaTouchLayer->addChild(createCharacterImage(characterData.getMasterCharacterId()));
    addChild(charaTouchLayer);

    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, charaTouchLayer);

    auto statusLayer = Layer::create();
    statusLayer->setTag(TAG_SPRITE::STATUS_LAYER);
    auto statusbase = createStatusBase();
    auto icon = createIconImage(StringUtils::format("%d_i.png", characterData.getMasterCharacterId()), statusbase);
    if (characterData.getIsFriend() == false) {
        showCharacterIconStatus(characterData.getPlayerCharacterId(), icon);
        // お気に入り
        if (PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[characterData.getPlayerCharacterId()]->getLock() == 1) {
            Sprite* lockIcon = Sprite::create("lock_icon.png");
            lockIcon->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
            lockIcon->setPosition(Point(10, icon->getContentSize().height - 10));
            icon->addChild(lockIcon);
        }
    }
    statusbase->addChild(icon);
    statusbase->addChild(createNameLabel(characterData.getName(), icon));

    showRarity(characterData.getRarity(), icon, statusbase);

    statusbase->addChild(createVehicleModelLabel(characterData.getVehicleModeName(), statusbase, icon));

    auto levelLabel = createLevelLabel(characterData.getLevel(), icon, statusbase);
    statusbase->addChild(levelLabel);

    auto bodyTypeBase = createBodyTypeBase(icon);
    auto bodyTypeLabel = createBodyTypeLabel(characterData.getBodyTypeName(), bodyTypeBase);
    bodyTypeBase->addChild(bodyTypeLabel);
    bodyTypeBase->addChild(createCharacterCost(characterData.getCost(), bodyTypeBase, bodyTypeLabel));
    statusbase->addChild(bodyTypeBase);

    float statusViewPositionY = levelLabel->getPosition().y - 10;
    if (characterData.getIsFriend() == false) {
        std::map<std::string, float>expData = characterData.getExpViewData();
        statusbase->addChild(createExpBar(expData[EXP_DATA::NEXT_LEVEL_UP_EXP], expData[EXP_DATA::EXP_SCALE], bodyTypeBase, levelLabel));

        auto diceSet = createCharacterDiceSet(characterData.getDice(), statusbase, levelLabel);
        statusbase->addChild(diceSet);
        statusViewPositionY = diceSet->getPosition().y - 10 - diceSet->getContentSize().height / 2;
    }

    auto statusView = createCharacterStatusView(characterData.getPower(), characterData.getTechnique(), characterData.getBrake(), statusbase, statusViewPositionY);
    statusbase->addChild(statusView);

    auto leaderSkillBase = createLeaderSkillView(characterData.getLeaderSkillName(), characterData.getLeaderSkillDescription(), statusbase, statusView);
    statusbase->addChild(leaderSkillBase);
    statusbase->addChild(createSkillView(characterData.getSkillName(), characterData.getSkillDescription(), characterData.getSkillMotionTurn(), leaderSkillBase));

    if (characterData.getIsFriend() == false) {
        auto dearValueView = createDearValueView(characterData.getDearValue());
        dearValueView->setPosition(Point(dearValueView->getContentSize().width / 2,
                                         statusbase->getContentSize().height + 10 + dearValueView->getContentSize().height / 2));
        statusbase->addChild(dearValueView);
    }
    statusLayer->addChild(statusbase);

    if (characterData.getIsFriend() == false) {
        charaTouchLayer->addChild(createPlayerCharacterPartsLayer(characterData.getEquipmentParts(), statusbase));
    }
    addChild(statusLayer);

    if (_gachaFlag == false) {
        showSceneBackButton();
    }
    return true;
}


void DetailLayer::onTapBackButton(Ref* pSender)
{
    isTouch = false;
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    auto mainScene = dynamic_cast<MainScene*>(getParent());
    if (mainScene) {
        mainScene->endShowing();
    }
    if (_callback != nullptr) {
        _callback();
    } else {
        removeFromParent();
    }
}

bool DetailLayer::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (!isTouch) {
        return false;
    }
    if (_gachaFlag == false) {
        for (int i = 0; i < _partsCount; i++) {
            Node* pCard = getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->getChildByTag(TAG_SPRITE::PARTS_LAYER)->getChildByTag(PARTS_TAG_NUMBER + i);
            if (pCard) {
                Rect cardRect = pCard->getBoundingBox();
                if (cardRect.containsPoint(CCDirector::getInstance()->convertToGL(touch->getLocationInView()))) {
                    if (i == 0) {
                        _partsText1->setVisible(true);
                    } else if (i == 1) {
                        _partsText2->setVisible(true);
                    }
                    return true;
                }
            }
        }
        if (_statusViewFlag == true) {
            getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->runAction(Sequence::create(ScaleTo::create(0.2, _maxScale), NULL));
            getChildByTag(TAG_SPRITE::STATUS_LAYER)->setVisible(false);
            _statusViewFlag = false;
            if (_partsCount > 0) {
                getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->getChildByTag(TAG_SPRITE::PARTS_LAYER)->setVisible(false);
            }
        } else {
            getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->runAction(Sequence::create(ScaleTo::create(0.2, 1.0), NULL));
            getChildByTag(TAG_SPRITE::STATUS_LAYER)->setVisible(true);
            _statusViewFlag = true;
            if (_partsCount > 0) {
                getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->getChildByTag(TAG_SPRITE::PARTS_LAYER)->setVisible(true);
            }
        }
    } else {
        onTapBackButton(this);
    }
    return true;
}

void DetailLayer::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (_gachaFlag == false && _statusViewFlag == true) {
        for (int i = 0; i < _partsCount; i++) {
            Node* pCard = getChildByTag(TAG_SPRITE::CHARACTER_LAYER)->getChildByTag(TAG_SPRITE::PARTS_LAYER)->getChildByTag(PARTS_TAG_NUMBER + i);
            if (pCard) {
                Rect cardRect = pCard->getBoundingBox();
                if (cardRect.containsPoint(CCDirector::getInstance()->convertToGL(touch->getLocationInView()))) {
                    if (i == 0) {
                        _partsText1->setVisible(true);
                    } else if (i == 1) {
                        _partsText2->setVisible(true);
                    }
                    return;
                } else {
                    if (_partsCount == 1) {
                        _partsText1->setVisible(false);
                    } else if (_partsCount == 2) {
                        _partsText2->setVisible(false);
                        _partsText1->setVisible(false);
                    }
                }
            }
        }
    }
}

void DetailLayer::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (_gachaFlag == false) {
        if (_statusViewFlag == true) {
            if (_partsCount == 1) {
                _partsText1->setVisible(false);
            } else if (_partsCount == 2) {
                _partsText2->setVisible(false);
                _partsText1->setVisible(false);
            }
        }
    }
}

void DetailLayer::showBackGround()
{
    auto BgSp = Sprite::create("home_bg.png");
    BgSp->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    addChild(BgSp);
}

void DetailLayer::showSceneBackButton()
{
    auto btBack = makeMenuItem("back_button.png", CC_CALLBACK_1(DetailLayer::onTapBackButton, this));
    auto menu = MenuPriority::createWithPriority(kPriorityDetailMenu);
    menu->addChild(btBack);
    menu->setPosition(Vec2(14 + btBack->getContentSize().width / 2, Director::getInstance()->getWinSize().height - btBack->getContentSize().width / 2));
    addChild(menu);
}

Sprite* DetailLayer::createCharacterImage(const int masterCharacterId)
{
    if (masterCharacterId > 0) {
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_ca.plist", masterCharacterId).c_str());
    }
    auto result = makeSprite(StringUtils::format("%d_t.png", masterCharacterId).c_str());
    result->setAnchorPoint(Point(0.5, 1));
    auto winSize = Director::getInstance()->getWinSize();
    result->setPosition(Point(winSize.width / 2, winSize.height));

    _charaMstId = masterCharacterId;
    _maxScale = winSize.width / result->getContentSize().width;

    float heightScale = _maxScale * result->getContentSize().height;
    float maxYScale = winSize.height / heightScale;
    if (maxYScale < 1) {
        _maxScale =  winSize.height / result->getContentSize().height;
    }
    return result;
}

Sprite* DetailLayer::createIconImage(const std::string filename, Sprite* satatusBase)
{
    auto result = makeSprite(filename.c_str());
    result->setPosition(Point(result->getContentSize().width / 2 + 14,
                              satatusBase->getContentSize().height - 14 - result->getContentSize().height / 2));
    return result;
}

Sprite* DetailLayer::createStatusBase()
{
    auto result = makeSprite("detale_base.png");
    Size winSize = Director::getInstance()->getWinSize();
    result->setPosition(Point(winSize.width / 2, result->getContentSize().height / 2 + 12));
    return result;
}

Label* DetailLayer::createNameLabel(const std::string name, Sprite* icon)
{
    auto result = Label::createWithTTF(name, FONT_NAME_2, 28);
    result->setPosition(Point(icon->getPosition().x + (icon->getContentSize().width * 0.5) + (result->getContentSize().width * 0.5) + 10,
                              icon->getPosition().y + (icon->getContentSize().height * 0.5) - result->getContentSize().height * 0.5));
    result->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    return result;
}

void DetailLayer::showRarity(const int rarity, Sprite* icon, Sprite* statusBase)
{
    auto rankPoint = Point(icon->getPosition().x +  (icon->getContentSize().width * 0.5) + 10 + 26 / 2,
                           icon->getPosition().y);
    for (int i = 1; i <= rarity; i++) {
        auto rank = makeSprite("other_rarity.png");
        statusBase->addChild(rank);
        rank->setPosition(rankPoint);
        rankPoint += Vec2(26 + 2, 0);
    }
}

Label* DetailLayer::createVehicleModelLabel(const std::string vehcleModelName, Sprite* statusBase, Sprite* icon)
{
    auto result = Label::createWithTTF("型式　  " + vehcleModelName, FONT_NAME_2, 18);
    result->setPosition(Point(statusBase->getContentSize().width - 22,
                              icon->getPosition().y - 9));
    result->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->setAnchorPoint(Vec2(1.0, 0.5));

    return result;
}

Label* DetailLayer::createLevelLabel(const int playerCharacterLebel, Sprite* icon, Sprite* statusBase)
{
    const int maxLevel = LevelModel::maxLevel() - 1;
    if (playerCharacterLebel < maxLevel) {
        auto result = Label::createWithTTF(StringUtils::format("Lv %d", playerCharacterLebel), FONT_NAME_2, 28);
        result->setAnchorPoint(Vec2(0.0, 0.5));
        result->setPosition(Point(icon->getPosition().x - icon->getContentSize().width / 2,
                                  icon->getPosition().y - (icon->getContentSize().height * 0.5) - (result->getContentSize().height * 0.5) - 10));
        result->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

        auto levelSpaceLabel = Label::createWithTTF("/", FONT_NAME_2, 24);
        levelSpaceLabel->setPosition(result->getPosition() + Point(result->getContentSize().width + levelSpaceLabel->getContentSize().width / 2 + 5, 1));
        levelSpaceLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        levelSpaceLabel->setColor(Color3B(COLOR_YELLOW));
        statusBase->addChild(levelSpaceLabel);

        auto maxLevelLabel = Label::createWithTTF(StringUtils::format("Lv %d", maxLevel), FONT_NAME_2, 20);
        maxLevelLabel->setPosition(levelSpaceLabel->getPosition() + Point(levelSpaceLabel->getContentSize().width / 2 + maxLevelLabel->getContentSize().width / 2 + 3, 1));
        maxLevelLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        maxLevelLabel->setColor(Color3B(COLOR_YELLOW));
        statusBase->addChild(maxLevelLabel);
        return result;
    } else {
        auto result = Label::createWithTTF("Lv ", FONT_NAME_2, 28);
        result->setAnchorPoint(Vec2(0.0, 0.5));
        result->setPosition(Point(icon->getPosition().x - icon->getContentSize().width / 2,
                                  icon->getPosition().y - (icon->getContentSize().height * 0.5) - (result->getContentSize().height * 0.5) - 10));
        result->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        auto levelSpaceLabel = Label::createWithTTF("MAX", FONT_NAME_2, 28);
        levelSpaceLabel->setPosition(result->getPosition() + Point(result->getContentSize().width + levelSpaceLabel->getContentSize().width / 2 + 5, 0));
        levelSpaceLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        levelSpaceLabel->setColor(Color3B(COLOR_YELLOW));
        statusBase->addChild(levelSpaceLabel);

        return result;
    }
}


Sprite* DetailLayer::createBodyTypeBase(Sprite* icon)
{
    auto result = makeSprite("detale_bodyType_base.png");
    result->setPosition(Point(icon->getPosition().x + (icon->getContentSize().width * 0.5) + (result->getContentSize().width * 0.5) + 10,
                              icon->getPosition().y - (icon->getContentSize().height * 0.5) + (result->getContentSize().height * 0.5)));
    return result;
}

Sprite* DetailLayer::createExpBar(const int nextLevelUpexp, const float expBarScale, Sprite* bodyTypeBase, Label* levelLabel)
{
    auto result = makeSprite("detale_exp_bar_base.png");
    result->setPosition(Point(bodyTypeBase->getPosition().x + 51, levelLabel->getPosition().y + result->getContentSize().height));

    auto nextLabel = Label::createWithTTF("次のLvまで", FONT_NAME_2, 18);
    nextLabel->setAnchorPoint(Vec2(0.0f, 0.0f));
    nextLabel->setPosition(Point(0, result->getContentSize().height - 18 + 3));
    nextLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->addChild(nextLabel);

    auto nextLvUpLabel = Label::createWithTTF(FormatWithCommas(nextLevelUpexp), FONT_NAME_2, 18);
    nextLvUpLabel->setAnchorPoint(Vec2(1.0f, 0.0f));
    nextLvUpLabel->setPosition(Point(result->getContentSize().width, result->getContentSize().height - 18 + 3));
    nextLvUpLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->addChild(nextLvUpLabel);

    auto expBar = makeSprite("detale_exp_bar.png");
    expBar->setAnchorPoint(Vec2(0.0, 0.0));
    expBar->setScaleX(expBarScale);
    result->addChild(expBar);
    return result;
}

Label* DetailLayer::createBodyTypeLabel(const std::string bodyTypeName, Sprite* bodyTypeBase)
{
    auto result = Label::createWithTTF(bodyTypeName, FONT_NAME_2, 22);
    result->setAnchorPoint(Vec2(0.0f, 0.5f));
    result->setPosition(Point(150, bodyTypeBase->getContentSize().height / 2 - 11));
    result->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    return result;
}

Label* DetailLayer::createCharacterCost(const int characterCost, Sprite* bodyTypeBase, Label* bodyTypeLabel)
{
    auto result = Label::createWithTTF(StringUtils::format("%d", characterCost), FONT_NAME_2, 22);
    result->setAnchorPoint(Vec2(1.0f, 0.5f));
    result->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->setPosition(Point(bodyTypeBase->getContentSize().width - 5, bodyTypeLabel->getPosition().y));
    return result;
}

Sprite* DetailLayer::createCharacterDiceSet(std::vector<int>diceSet, Sprite* statusBase, Label* lebelLabel)
{
    auto result = makeSprite("detaile_dice_base_1.png");
    result->setPosition(Vec2(14 + result->getContentSize().width * 0.5f,
                             lebelLabel->getPosition().y - 10 - result->getContentSize().height / 2));

    auto saikoro_bg2 = makeSprite("detaile_dice_base_2.png");
    statusBase->addChild(saikoro_bg2, 1);
    saikoro_bg2->setPosition(Vec2(statusBase->getContentSize().width - 14 - saikoro_bg2->getContentSize().width * 0.5f,
                                  result->getPositionY()));

    for (int i = 0; i < 9; i++) {
        if (diceSet.at(i) < 1) {
            diceSet.at(i) = 0;
        }
        auto saikoroImg = makeSprite(StringUtils::format("de_dice_%d.png", diceSet.at(i)).c_str());
        statusBase->addChild(saikoroImg, 2);
        if (i < 6) {
            saikoroImg->setPosition(Vec2(15.0f + (saikoroImg->getContentSize().width + 4) * (0.5f + i) + 7 * i,
                                         result->getPositionY() + 2));
        } else {
            saikoroImg->setPosition(Vec2(statusBase->getContentSize().width - 18.0f - (saikoroImg->getContentSize().width + 4) * (0.5f + 8 - i) - 7 * (8 - i),
                                         saikoro_bg2->getPositionY() + 2));
        }
    }
    return result;
}

Sprite* DetailLayer::createCharacterStatusView(const int power, const int technique, const int brake, Sprite* statusBase, const float positionY)
{
    auto result = makeSprite("detale_status_base.png");
    result->setPosition(Point(statusBase->getContentSize().width / 2,
                              positionY - result->getContentSize().height / 2));

    auto status2 = makeSprite("detale_status_base.png");
    statusBase->addChild(status2);
    status2->setPosition(Point(14 + result->getContentSize().width / 2,
                               result->getPosition().y));

    auto status3 = makeSprite("detale_status_base.png");
    statusBase->addChild(status3);
    status3->setPosition(Point(statusBase->getContentSize().width - 14 - status3->getContentSize().width / 2,
                               result->getPosition().y));

    auto bariki = Label::createWithTTF("馬力", FONT_NAME_2, 22);
    status2->addChild(bariki);
    bariki->setPosition(Point(5 + bariki->getContentSize().width / 2, status2->getContentSize().height / 2 - 11));
    bariki->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto barikiStatus = Label::createWithTTF(FormatWithCommas(power), FONT_NAME_2, 22);
    status2->addChild(barikiStatus);
    barikiStatus->setPosition(Point(status2->getContentSize().width - barikiStatus->getContentSize().width / 2 - 5,
                                    status2->getContentSize().height / 2 - 11));
    barikiStatus->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto giryou = Label::createWithTTF("技量", FONT_NAME_2, 22);
    result->addChild(giryou);
    giryou->setPosition(Point(5 + giryou->getContentSize().width / 2, result->getContentSize().height / 2 - 11));
    giryou->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto giryouStatus = Label::createWithTTF(FormatWithCommas(technique), FONT_NAME_2, 22);
    result->addChild(giryouStatus);
    giryouStatus->setPosition(Point(result->getContentSize().width - giryouStatus->getContentSize().width / 2 - 5,
                                    result->getContentSize().height / 2 - 11));
    giryouStatus->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));


    auto nintai = Label::createWithTTF("忍耐", FONT_NAME_2, 22);
    status3->addChild(nintai);
    nintai->setPosition(Point(5 + nintai->getContentSize().width / 2, status3->getContentSize().height / 2 - 11));
    nintai->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto nintaiStatus = Label::createWithTTF(FormatWithCommas(brake), FONT_NAME_2, 22);
    status3->addChild(nintaiStatus);
    nintaiStatus->setPosition(Point(status3->getContentSize().width - nintaiStatus->getContentSize().width / 2 - 5,
                                    status3->getContentSize().height / 2 - 11));
    nintaiStatus->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    return result;
}

Sprite* DetailLayer::createLeaderSkillView(const std::string leaderSkillName, const std::string leaderSkillDescription, Sprite* statusBase, Sprite* statusValueBase)
{
    auto result = makeSprite("detale_skill_base.png");
    result->setPosition(Point(statusBase->getContentSize().width / 2,
                              statusValueBase->getPosition().y - statusValueBase->getContentSize().height / 2 - 10 - result->getContentSize().height / 2));

    auto leaderSkillNameTitleLabel = Label::createWithTTF("リーダースキル ", FONT_NAME_2, 22);
    result->addChild(leaderSkillNameTitleLabel);
    leaderSkillNameTitleLabel->setAnchorPoint(Vec2(0.0f, 1.0f));
    leaderSkillNameTitleLabel->setPosition(Point(10, result->getContentSize().height - 14 + 11));
    leaderSkillNameTitleLabel->setColor(Color3B(141, 250, 255));
    leaderSkillNameTitleLabel->enableShadow();

    auto leaderSkillNameLabel = Label::createWithTTF(leaderSkillName, FONT_NAME_2, 22);
    result->addChild(leaderSkillNameLabel);
    leaderSkillNameLabel->setAnchorPoint(Vec2(0.0f, 1.0f));
    leaderSkillNameLabel->setPosition(Point(leaderSkillNameTitleLabel->getPosition().x + leaderSkillNameTitleLabel->getContentSize().width,
                                            result->getContentSize().height - 14 + 11));
    leaderSkillNameLabel->enableShadow();

    auto leaderSkillDescriptionLabel = Label::createWithTTF(leaderSkillDescription, FONT_NAME_2, 22);
    result->addChild(leaderSkillDescriptionLabel);
    leaderSkillDescriptionLabel->setAnchorPoint(Vec2(0.0f, 0.0f));
    leaderSkillDescriptionLabel->setDimensions(result->getContentSize().width - 20, 0);
    leaderSkillDescriptionLabel->setColor(Color3B::BLACK);
    leaderSkillDescriptionLabel->setPosition(Point(10, -15));
    return result;
}

Sprite* DetailLayer::createSkillView(const std::string skillName, const std::string skillDescription, const int skillMotionTurn, Sprite* leaderSkilBase)
{
    auto result = makeSprite("detale_skill_base.png");
    result->setPosition(Point(leaderSkilBase->getPosition().x,
                              leaderSkilBase->getPosition().y - 10 - leaderSkilBase->getContentSize().height / 2 - result->getContentSize().height / 2));

    auto skillNameTitleLabel = Label::createWithTTF("スキル ", FONT_NAME_2, 22);
    result->addChild(skillNameTitleLabel);
    skillNameTitleLabel->setAnchorPoint(Vec2(0.0f, 1.0f));
    skillNameTitleLabel->setPosition(Point(10, result->getContentSize().height - 14 + 11));
    skillNameTitleLabel->setColor(COLOR_YELLOW);
    skillNameTitleLabel->enableShadow();

    auto skillNameLabel = Label::createWithTTF(skillName, FONT_NAME_2, 22);
    result->addChild(skillNameLabel);
    skillNameLabel->setAnchorPoint(Vec2(0.0f, 1.0f));
    skillNameLabel->setPosition(Point(skillNameTitleLabel->getPosition().x + skillNameTitleLabel->getContentSize().width,
                                      result->getContentSize().height - 14 + 11));
    skillNameLabel->enableShadow();

    auto motionTurnLabel = Label::createWithTTF(StringUtils::format("ターン %d", skillMotionTurn), FONT_NAME_2, 22);
    result->addChild(motionTurnLabel);
    motionTurnLabel->setAnchorPoint(Vec2(1.0f, 1.0f));
    motionTurnLabel->setPosition(Point(result->getContentSize().width - 10, result->getContentSize().height - 14 + 11));
    motionTurnLabel->enableShadow();

    auto skillDescriptionLabel = Label::createWithTTF(skillDescription, FONT_NAME_2, 22);
    result->addChild(skillDescriptionLabel);
    skillDescriptionLabel->setAnchorPoint(Vec2(0.0f, 0.0f));
    skillDescriptionLabel->setDimensions(result->getContentSize().width - 20, 0);
    skillDescriptionLabel->setColor(Color3B::BLACK);
    skillDescriptionLabel->setPosition(Point(10, -15));
    return result;
}

Sprite* DetailLayer::createDearValueView(const int dearValue)
{
    Sprite* dearValueBase = Sprite::create("detail_dear_value.png");
    std::string deaValueString;
    const int maxDearValue = 250;
    if (dearValue >= maxDearValue) {
        Label* dearValueLabel = Label::createWithTTF("MAX", FONT_NAME_2, 22);
        dearValueLabel->setPosition(Point(dearValueBase->getContentSize().width / 2 + 10,
                                          dearValueBase->getContentSize().height / 2 - 11));
        dearValueLabel->setColor(COLOR_YELLOW);
        dearValueLabel->enableShadow();
        dearValueBase->addChild(dearValueLabel);
    } else {
        Label* dearValueLabel = Label::createWithTTF(StringUtils::format("%d/%d", dearValue, maxDearValue), FONT_NAME_2, 22);
        dearValueLabel->setPosition(Point(dearValueBase->getContentSize().width - dearValueLabel->getContentSize().width / 2 - 5,
                                          dearValueBase->getContentSize().height / 2 - 11));
        dearValueLabel->enableShadow();
        dearValueBase->addChild(dearValueLabel);
    }
    return dearValueBase;
}

// TO DO リファクタリング可
Layer* DetailLayer::createPlayerCharacterPartsLayer(const std::vector<int>parts, Sprite* statusBase)
{
    Point paIconPosi = Point(0, 0);
    auto result = Layer::create();
    result->setTag(TAG_SPRITE::PARTS_LAYER);
    for (int i = 3; i > -1; i--) {
        if (parts.at(i) < 1) {
            continue;
        }
        _partsCount++;
        int partsId = PLAYERCONTROLLER->findIndexOfPlayerPartId(parts.at(i));
        int partsMstId = PLAYERCONTROLLER->_playerParts.at(partsId)->getPartsId();
        std::shared_ptr<PartModel>partsModel(PartModel::find(partsMstId));
        auto partsImg = makeSprite(StringUtils::format("%d_pag.png", partsMstId).c_str());
        result->addChild(partsImg, (3 - _partsCount));
        partsImg->setTag(PARTS_TAG_NUMBER + (_partsCount - 1));
        partsImg->setPosition(Vec2(statusBase->getPosition().y + (statusBase->getContentSize().width / 2) + partsImg->getContentSize().width * 0.5,
                                   statusBase->getPosition().y + (statusBase->getContentSize().height / 2) + 5 + partsImg->getContentSize().height * 0.5 + (partsImg->getContentSize().height * ((_partsCount - 1))) + ((_partsCount - 1)) * 5));

        std::string partsNa = partsModel->getName();
        int power = partsModel->getExercise();
        int technique = partsModel->getReaction();
        int dicision = partsModel->getDecision();

        if ((_partsCount - 1) == 0) {
            _partsText1 = makeSprite("detale_parts_popup_base.png");
            partsImg->addChild(_partsText1);
            _partsText1->setVisible(false);
            _partsText1->setAnchorPoint(Vec2(1.0f, 0.0f));
            _partsText1->setPosition(Point(25, partsImg->getContentSize().height - 25));

            auto partsName = Label::createWithTTF("装備2", FONT_NAME_2, 28);
            _partsText1->addChild(partsName);
            partsName->setColor(COLOR_PINK);
            partsName->setPosition(Point(partsName->getContentSize().width / 2 + 10 + 12.5 - 5,
                                         _partsText1->getContentSize().height - partsName->getContentSize().height / 2 - 15));
            partsName->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));


            auto telopPartsNameVal = TelopScrollView::initScrollTelop(partsNa,
                                                                      28,
                                                                      _partsText1->getContentSize().width - partsName->getContentSize().width - 50,
                                                                      30,
                                                                      Color3B::WHITE);
            _partsText1->addChild(telopPartsNameVal);
            telopPartsNameVal->setFontShadow(Color3B::BLACK, 2);
            telopPartsNameVal->setPosition(Point(partsName->getPosition().x + partsName->getContentSize().width / 2 + 10 + telopPartsNameVal->getContentSize().width / 2,
                                                 partsName->getPosition().y));

            auto statusPosi = Point(10, 19);
            if (power > 0) {
                auto powerLabel = Label::createWithTTF("「馬」+" + FormatWithCommas(power), FONT_NAME_2, 19);
                _partsText1->addChild(powerLabel);
                powerLabel->setAnchorPoint(Vec2(0, 0.5));
                powerLabel->setPosition(statusPosi);
                powerLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
                statusPosi = statusPosi + Vec2(powerLabel->getContentSize().width, 0);
            }
            if (dicision > 0) {
                auto dicisionLabel = Label::createWithTTF("「忍」+" + FormatWithCommas(dicision), FONT_NAME_2, 19);
                _partsText1->addChild(dicisionLabel);
                dicisionLabel->setAnchorPoint(Vec2(0, 0.5));
                dicisionLabel->setPosition(statusPosi);
                dicisionLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
                statusPosi = statusPosi + Vec2(dicisionLabel->getContentSize().width, 0);
            }
            if (technique > 0) {
                auto techniqueLabel = Label::createWithTTF("「技」+" + FormatWithCommas(technique), FONT_NAME_2, 19);
                _partsText1->addChild(techniqueLabel);
                techniqueLabel->setAnchorPoint(Vec2(0, 0.5));
                techniqueLabel->setPosition(statusPosi);
                techniqueLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
                statusPosi = statusPosi + Vec2(techniqueLabel->getContentSize().width, 0);
            }
        } else {
            _partsText2 = makeSprite("detale_parts_popup_base.png");
            _partsText2->setVisible(false);
            partsImg->addChild(_partsText2);
            _partsText2->setVisible(false);
            _partsText2->setAnchorPoint(Vec2(1.0f, 0.0f));
            _partsText2->setPosition(Point(25, partsImg->getContentSize().height - 25));
            auto partsName = Label::createWithTTF("装備1", FONT_NAME_2, 28);
            _partsText2->addChild(partsName);
            partsName->setColor(COLOR_PINK);
            partsName->setPosition(Point(partsName->getContentSize().width / 2 + 10 + 12.5 - 5,
                                         _partsText2->getContentSize().height - partsName->getContentSize().height / 2 - 15));
            partsName->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));


            auto telopPartsNameVal = TelopScrollView::initScrollTelop(partsNa,
                                                                      28,
                                                                      _partsText2->getContentSize().width - partsName->getContentSize().width - 50,
                                                                      30,
                                                                      Color3B::WHITE);
            _partsText2->addChild(telopPartsNameVal);
            telopPartsNameVal->setFontShadow(Color3B::BLACK, 2);
            telopPartsNameVal->setPosition(Point(partsName->getPosition().x + partsName->getContentSize().width / 2 + 10 + telopPartsNameVal->getContentSize().width / 2,
                                                 partsName->getPosition().y));

            auto statusPosi = Point(10, 19);
            if (power > 0) {
                auto powerLabel = Label::createWithTTF("「馬」+" + FormatWithCommas(power), FONT_NAME_2, 19);
                _partsText2->addChild(powerLabel);
                powerLabel->setAnchorPoint(Vec2(0, 0.5));
                powerLabel->setPosition(statusPosi);
                powerLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
                statusPosi = statusPosi + Vec2(powerLabel->getContentSize().width, 0);
            }

            if (dicision > 0) {
                auto dicisionLabel = Label::createWithTTF("「忍」+" + FormatWithCommas(dicision), FONT_NAME_2, 19);
                _partsText2->addChild(dicisionLabel);
                dicisionLabel->setAnchorPoint(Vec2(0, 0.5));
                dicisionLabel->setPosition(statusPosi);
                dicisionLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
                statusPosi = statusPosi + Vec2(dicisionLabel->getContentSize().width, 0);
            }

            if (technique > 0) {
                auto techniqueLabel = Label::createWithTTF("「技」+" + FormatWithCommas(technique), FONT_NAME_2, 19);
                _partsText2->addChild(techniqueLabel);
                techniqueLabel->setAnchorPoint(Vec2(0, 0.5));
                techniqueLabel->setPosition(statusPosi);
                techniqueLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
                statusPosi = statusPosi + Vec2(techniqueLabel->getContentSize().width, 0);
            }
        }
    }
    _selectParts1 = false;
    _selectParts2 = false;

    return result;
}

void DetailLayer::showCharacterIconStatus(const int playerCharacterId, Sprite* icon)
{
    bool suportflag = false;
    int suportId = PLAYERCONTROLLER->_playerTeams->getSupport1CharacterId();
    if (suportId != playerCharacterId) {
        suportId = PLAYERCONTROLLER->_playerTeams->getSupport2CharacterId();
        if (suportId != playerCharacterId) {
            suportId = PLAYERCONTROLLER->_playerTeams->getSupport3CharacterId();
            if (suportId != playerCharacterId) {
                suportflag = false;
            } else {
                suportflag = true;
            }
        } else {
            suportflag = true;
        }
    } else {
        suportflag = true;
    }

    if (PLAYERCONTROLLER->_playerTeams->getLearderCharacterId() == playerCharacterId) {
        auto leaderIcon = makeSprite("detale_leder.png");
        leaderIcon->setPosition(Vec2(icon->getContentSize().width - 23, icon->getContentSize().height - 15));
        icon->addChild(leaderIcon);
    }

    if (suportflag == true) {
        auto suportIcon = makeSprite("garage_list_using_label.png");
        suportIcon->setPosition(Vec2(icon->getContentSize().width - 23, icon->getContentSize().height - 15));
        icon->addChild(suportIcon);
    }
}



void DetailLayer::setCallBack(const endCallBack& callback)
{
    _callback = callback;
}