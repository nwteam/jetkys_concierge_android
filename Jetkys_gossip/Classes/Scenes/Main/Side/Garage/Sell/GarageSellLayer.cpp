#include "GarageSellLayer.h"
#include "jCommon.h"
#include "PlayerController.h"
#include "FontDefines.h"
#include "CharacterModel.h"
#include "BodyTypeModel.h"
#include "PartModel.h"

GarageSellLayer::~GarageSellLayer()
{
    _tmpSelectedIndexs.clear();
}

bool GarageSellLayer::init(GarageSell type, const ButtonCallback &okButtonCallback, const ButtonCallback &clearButtonCallback)
{
    if (!LayerPriority::initWithPriority(kPriorityGarageSellLayer)) {
        return false;
    }

    _type = type;
    _tmpSelectedIndexs = std::vector<int>();
    Size winSize = Director::getInstance()->getWinSize();
    setContentSize(Size(winSize.width, 170));


    _touchListener->onTouchBegan = [this](Touch* touch, Event* event) {
                                       if (Rect(0, 0, getContentSize().width, getContentSize().height).containsPoint(convertToNodeSpace(touch->getLocation()))) {
                                           ////CCLOG("Predict Touch %f, %f", touch->getLocation().x, touch->getLocation().y);
                                           return true;
                                       }
                                       return false;
                                   };

    auto board = makeSprite("garage_sell_board.png");
    addChild(board);
    board->setPosition(Vec2(winSize.width / 2, getContentSize().height - board->getContentSize().height / 2));

    int coinPredict = 999;
    int expPredict = 999999;

    int carryPredict = (int)PLAYERCONTROLLER->_playerCharacterModels.size();
    int carryMax = PLAYERCONTROLLER->_player->getGarageSize();


    // Info
    auto predictLabel = Label::createWithTTF("合計売価", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(predictLabel, 1);
    predictLabel->setAnchorPoint(Vec2(0, 0.5));
    predictLabel->enableShadow();
    predictLabel->setColor(COLOR_YELLOW);
    predictLabel->setPosition(board->getPosition() + Vec2(-board->getContentSize().width / 2 + 20, 40 - 12));

    _coinValue = Label::createWithTTF(FormatWithCommas(coinPredict), FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    addChild(_coinValue, 1);
    _coinValue->setAnchorPoint(Vec2(1, 0.5));
    _coinValue->enableShadow();
    _coinValue->setColor(COLOR_YELLOW);
    _coinValue->setPosition(predictLabel->getPosition() + Vec2(355, 0));

    predictLabel = Label::createWithTTF("選択数", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(predictLabel, 1);
    predictLabel->setAnchorPoint(Vec2(0, 0.5));
    predictLabel->enableShadow();
    predictLabel->setPosition(board->getPosition() + Vec2(-board->getContentSize().width / 2 + 20, -12));

    _selectValue = Label::createWithTTF(StringUtils::format("%3d/%3d", expPredict, 10), FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    addChild(_selectValue, 1);
    _selectValue->setAnchorPoint(Vec2(1, 0.5));
    _selectValue->enableShadow();
    _selectValue->setPosition(predictLabel->getPosition() + Vec2(355, 0));

    predictLabel = Label::createWithTTF("所持数", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(predictLabel, 1);
    predictLabel->setAnchorPoint(Vec2(0, 0.5));
    predictLabel->enableShadow();
    predictLabel->setPosition(board->getPosition() + Vec2(-board->getContentSize().width / 2 + 20, -40 - 12));

    _carryValue = Label::createWithTTF(StringUtils::format("%3d/%3d", carryPredict, carryMax), FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    addChild(_carryValue, 1);
    _carryValue->setAnchorPoint(Vec2(1, 0.5));
    _carryValue->enableShadow();
    _carryValue->setPosition(predictLabel->getPosition() + Vec2(355, 0));

    // Menu
    auto menu = MenuPriority::createWithPriority(kPriorityGarageSellLayer - 1);
    addChild(menu);
    menu->setPosition(board->getPosition() + Vec2(197, 0));

    auto button = makeMenuItem("garage_sell_button.png", "OK", okButtonCallback);
    menu->addChild(button);
    button->setTag(GS_BT_OK);


    button = makeMenuItem("garage_sell_button.png", "クリア", clearButtonCallback);
    menu->addChild(button);
    button->setTag(GS_BT_RESET);

    menu->alignItemsVerticallyWithPadding(20);

    return true;
}

void GarageSellLayer::updateInfomations()
{
    int coin = 0;
    int select = (int)_tmpSelectedIndexs.size();
    int holdItem = 0;
    int holdMaxItem = 0;

    for (int i = 0; i < select; i++) {
        coin += saleMethod(_tmpSelectedIndexs.at(i));
    }

    if (_type == GS_CHARACTER) {
        holdItem = (int)PLAYERCONTROLLER->_playerCharactersSorted.size();
        holdMaxItem = (int)PLAYERCONTROLLER->_player->getGarageSize();
    } else {
        holdItem = (int)PLAYERCONTROLLER->_playerPartsSorted.size();
        holdMaxItem = ((int)PLAYERCONTROLLER->_player->getGarageSize()) * 2;
    }

    _coinValue->setString(FormatWithCommas(coin));
    _selectValue->setString(StringUtils::format("%3d/%3d", select, kNumberSelectMax));
    _carryValue->setString(StringUtils::format("%3d/%3d", holdItem, holdMaxItem));
}

int GarageSellLayer::saleMethod(int itemId)
{
    int salePrice;
    switch (_type) {
    case GS_CHARACTER: {
        int id = PLAYERCONTROLLER->_playerCharactersSorted.at(itemId)->getCharactersId();
        int level = PLAYERCONTROLLER->_playerCharactersSorted.at(itemId)->getLevel();
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(id));
        int bodyType = characterModel->getBodyType();
        int rarity = characterModel->getRarity();
        std::shared_ptr<BodyTypeModel>bodyTypeModel(BodyTypeModel::find(bodyType));
        int bacePrice = bodyTypeModel->getPriceCoefficient();
        salePrice = bacePrice * rarity * (level + 200) / 100;
        break;
    }
    case GS_PART: {
        int id = PLAYERCONTROLLER->_playerPartsSorted.at(itemId)->getPartsId();
        std::shared_ptr<PartModel>partModel(PartModel::find(id));
        salePrice = partModel->getSalePrice();
        break;
    }
    default:
        break;
    }
    return salePrice;
}