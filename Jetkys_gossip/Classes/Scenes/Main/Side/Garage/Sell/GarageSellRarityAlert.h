#ifndef __syanago__GarageSellRarityAlert__
#define __syanago__GarageSellRarityAlert__

#include "LayerPriority.h"
#include "PopUpDelegate.h"

#define kPriorityGarageSellRarityAlert -700

enum GarageSellRarity {
    GSR_BT_YES = 20,GSR_BT_YES_2, GSR_BT_NO
};

enum GarageSellRarityType {
    GST_CHARACTER, GST_PART
};

class GarageSellRarityAlert : public LayerPriority {
public:
    
    PopUpDelegate *_delegate;
    
    static GarageSellRarityAlert *createWithType(GarageSellRarityType type, std::vector<int> pcIndex);
    bool initWithType(GarageSellRarityType type, std::vector<int> pcIndex);
    
    static GarageSellRarityAlert *createWithPartsType(std::vector<int> pcIndex);
    bool initWithPartsType(std::vector<int> pcIndex);
    
    void callBackbtn(Ref *psender);
};

#endif /* defined(__syanago__GarageSellRarityAlert__) */
