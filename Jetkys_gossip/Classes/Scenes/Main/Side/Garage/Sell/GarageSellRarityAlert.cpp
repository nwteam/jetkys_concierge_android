#include "GarageSellRarityAlert.h"
#include "jCommon.h"
#include "PlayerController.h"
#include "FontDefines.h"
#include "CharacterModel.h"
#include "PartModel.h"

GarageSellRarityAlert* GarageSellRarityAlert::createWithType(GarageSellRarityType type, std::vector<int>pcIndex)
{
    auto layer = new GarageSellRarityAlert();
    layer->initWithType(type, pcIndex);
    layer->autorelease();

    return layer;
}

bool GarageSellRarityAlert::initWithType(GarageSellRarityType type, std::vector<int>pcIndex)
{
    if (!LayerPriority::initWithPriority(kPriorityGarageSellRarityAlert)) {
        return false;
    }

    _delegate = nullptr;

    Size winSize = Director::getInstance()->getWinSize();

    auto bg = LayerColor::create(Color4B(0, 0, 0, 150), winSize.width, winSize.height);
    addChild(bg);

    // add bgr dialog
    auto board = makeSprite("garage_sell_popup_bg.png");
    addChild(board);
    board->setPosition(Vec2(winSize.width / 2, winSize.height / 2));



    auto titleLabel = Label::createWithTTF("売却します", FONT_NAME_2, 30, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(titleLabel);
    titleLabel->setPosition(Vec2(winSize.width / 2, board->getPositionY() + board->getContentSize().height / 2 - 30 - titleLabel->getContentSize().height / 2 - 15));

    if (type == GST_CHARACTER) {
        // img player_character
//        int pcID = PLAYERCONTROLLER->_playerCharactersSorted.at(pcIndex)->getID();
//        int level = PLAYERCONTROLLER->_playerCharactersSorted.at(pcIndex)->getLevel();

        // todo dummy image character
        auto imageLayer = Layer::create();
        addChild(imageLayer);
        int count = (int)pcIndex.size();
        int rarityCount  = 0;
        Sprite* image;
        for (int i = 0; i < count; i++) {
            if (pcIndex.at(i) > 0) {
                int mstId = PLAYERCONTROLLER->_playerCharacterModels[pcIndex.at(i)]->getCharactersId();
                std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(mstId));
                int rarity = characterModel->getRarity();
                if (rarity >= 3) {
                    if (rarityCount < 4) {
                        rarityCount++;
                        image = makeSprite(StringUtils::format("%d_i.png", mstId).c_str());
                        imageLayer->addChild(image);
                        image->setPosition(titleLabel->getPosition() + Vec2((rarityCount - 1) * (image->getContentSize().width + 20),
                                                                            -titleLabel->getContentSize().height / 2 - 30 - image->getContentSize().height / 2 + 40));
                        auto node = Node::create();
                        imageLayer->addChild(node);
                        node->setPosition(image->getPosition() + Vec2(0, -image->getContentSize().height / 2));

                        for (int i = 0; i < rarity; i++) {
                            auto start = makeSprite("rarity.png");
                            node->addChild(start);
                            if (rarity % 2 == 1) {
                                start->setPosition(Vec2(start->getContentSize().width * (i - rarity / 2), 0));
                            } else {
                                int j = i - rarity / 2;
                                start->setPosition(Vec2(start->getContentSize().width * (0.5f + j), 0));
                            }
                        }
                    }
                }
            }
        }
        imageLayer->setPosition(imageLayer->getPosition() + Vec2(-((rarityCount - 1) * (image->getContentSize().width + 20) / 2), 0));
        // level
//        auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %d", level), FONT_NAME_2, 16);
//        image->addChild(levelLabel);
//        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
//        levelLabel->setPosition(Vec2(image->getContentSize().width/2, 6));
//        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);

        //
        auto messLabel = Label::createWithTTF("売却する車なごの中に", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
        addChild(messLabel);
        messLabel->setColor(COLOR_YELLOW);
        messLabel->setAnchorPoint(Vec2(0.5, 1));
        messLabel->setPosition(Vec2(winSize.width / 2,
                                    image->getPositionY() - image->getContentSize().height / 2 - 30));

        auto messLabel2 = Label::createWithTTF("★３以上の車なごが含まれています。", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
        addChild(messLabel2);
        messLabel2->setColor(COLOR_YELLOW);
        messLabel2->setAnchorPoint(Vec2(0.5, 1));
        messLabel2->setPosition(Vec2(winSize.width / 2,
                                     messLabel->getPositionY() - messLabel->getContentSize().height / 2 - 5));

        auto messLabel3 = Label::createWithTTF("売却してよろしいですか？", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
        addChild(messLabel3);
        messLabel3->setColor(COLOR_YELLOW);
        messLabel3->setAnchorPoint(Vec2(0.5, 1));
        messLabel3->setPosition(Vec2(winSize.width / 2,
                                     messLabel2->getPositionY() - messLabel2->getContentSize().height / 2 - 5));
    } else {
        // img player_character
//        int pcID = PLAYERCONTROLLER->_playerPartsSorted.at(pcIndex)->getPartsId();
//        MASTER_CONTROLLER->setPart(pcID);
//        int rarity = MASTER_CONTROLLER->_part->getRarity();

        // todo dummy image
        auto imageLayer = Layer::create();
        addChild(imageLayer);
        int count = (int)pcIndex.size();
        int rarityCount  = 0;
        Sprite* image;
        for (int i = 0; i < count; i++) {
            if (pcIndex.at(i) > 0) {
                int partsId = PLAYERCONTROLLER->findIndexOfPlayerPartId(pcIndex.at(i));
                int mstId = PLAYERCONTROLLER->_playerParts.at(partsId)->getPartsId();
                std::shared_ptr<PartModel>partsModel(PartModel::find(mstId));
                int rarity = partsModel->getRarity();
                if (rarity >= 3) {
                    if (rarityCount < 4) {
                        rarityCount++;
                        image = makeSprite(StringUtils::format("%d_pag.png", mstId).c_str());
                        imageLayer->addChild(image);
                        image->setPosition(titleLabel->getPosition() + Vec2((rarityCount - 1) * (image->getContentSize().width + 20),
                                                                            -titleLabel->getContentSize().height / 2 - 30 - image->getContentSize().height / 2 + 40));
                        auto node = Node::create();
                        imageLayer->addChild(node);
                        node->setPosition(image->getPosition() + Vec2(0, -image->getContentSize().height / 2));

                        for (int i = 0; i < rarity; i++) {
                            auto start = makeSprite("rarity.png");
                            node->addChild(start);
                            if (rarity % 2 == 1) {
                                start->setPosition(Vec2(start->getContentSize().width * (i - rarity / 2), 0));
                            } else {
                                int j = i - rarity / 2;
                                start->setPosition(Vec2(start->getContentSize().width * (0.5f + j), 0));
                            }
                        }
                    }
                }
            }
        }
        imageLayer->setPosition(imageLayer->getPosition() + Vec2(-((rarityCount - 1) * (image->getContentSize().width + 20) / 2), 0));
        // rarity
//        auto node = Node::create();
//        addChild(node);
//        node->setPosition(image->getPosition() + Vec2(0, -image->getContentSize().height/2));
//
//        for (int i = 0; i < rarity; i++) {
//            auto start = makeSprite("rarity.png");
//            node->addChild(start);
//            if (rarity % 2 == 1) {
//                start->setPosition(Vec2(start->getContentSize().width * (i - rarity/2) ,0));
//            } else {
//                int j = i - rarity/2;
//                start->setPosition(Vec2(start->getContentSize().width * (0.5f + j), 0));
//            }
//        }

        //
        auto messLabel = Label::createWithTTF("売却する装備の中に", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
        addChild(messLabel);
        messLabel->setColor(COLOR_YELLOW);
        messLabel->setAnchorPoint(Vec2(0.5, 1));
        messLabel->setPosition(Vec2(winSize.width / 2,
                                    image->getPositionY() - image->getContentSize().height / 2 - 30));

        auto messLabel2 = Label::createWithTTF("★３以上の装備が含まれています。", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
        addChild(messLabel2);
        messLabel2->setColor(COLOR_YELLOW);
        messLabel2->setAnchorPoint(Vec2(0.5, 1));
        messLabel2->setPosition(Vec2(winSize.width / 2,
                                     messLabel->getPositionY() - messLabel->getContentSize().height / 2 - 5));

        auto messLabel3 = Label::createWithTTF("売却してよろしいですか？", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
        addChild(messLabel3);
        messLabel3->setColor(COLOR_YELLOW);
        messLabel3->setAnchorPoint(Vec2(0.5, 1));
        messLabel3->setPosition(Vec2(winSize.width / 2,
                                     messLabel2->getPositionY() - messLabel2->getContentSize().height / 2 - 5));
    }


    // add button close
    auto menu = MenuPriority::createWithPriority(kPriorityGarageSellRarityAlert - 1);
    addChild(menu);

    auto button = makeMenuItem("garage_sell_button.png", "はい", CC_CALLBACK_1(GarageSellRarityAlert::callBackbtn, this));
    menu->addChild(button);
    button->setTag(GSR_BT_YES);

    button = makeMenuItem("garage_sell_button.png", "いいえ", CC_CALLBACK_1(GarageSellRarityAlert::callBackbtn, this));
    menu->addChild(button);
    button->setTag(GSR_BT_NO);

    menu->alignItemsHorizontallyWithPadding(50);
    menu->setPosition(Vec2(winSize.width / 2, board->getPositionY() - board->getContentSize().height / 2 + 30 + button->getNormalImage()->getContentSize().height / 2));

    return true;
}

GarageSellRarityAlert* GarageSellRarityAlert::createWithPartsType(std::vector<int>pcIndex)
{
    auto layer = new GarageSellRarityAlert();
    layer->initWithPartsType(pcIndex);
    layer->autorelease();

    return layer;
}

bool GarageSellRarityAlert::initWithPartsType(std::vector<int>pcIndex)
{
    if (!LayerPriority::initWithPriority(kPriorityGarageSellRarityAlert)) {
        return false;
    }

    _delegate = nullptr;

    Size winSize = Director::getInstance()->getWinSize();

    auto bg = LayerColor::create(Color4B(0, 0, 0, 150), winSize.width, winSize.height);
    addChild(bg);

    // add bgr dialog
    auto board = makeSprite("garage_sell_popup_bg.png");
    addChild(board);
    board->setPosition(Vec2(winSize.width / 2, winSize.height / 2));



    auto titleLabel = Label::createWithTTF("売却します", FONT_NAME_2, 30, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(titleLabel);
    titleLabel->setPosition(Vec2(winSize.width / 2, board->getPositionY() + board->getContentSize().height / 2 - 30 - titleLabel->getContentSize().height / 2 - 15));

    auto imageLayer = Layer::create();
    addChild(imageLayer);
    int count = (int)pcIndex.size();
    int rarityCount  = 0;
    Sprite* image;
    for (int i = 0; i < count; i++) {
        if (pcIndex.at(i) > 0) {
            int mstId = PLAYERCONTROLLER->_playerCharacterModels[pcIndex.at(i)]->getCharactersId();
            int level = PLAYERCONTROLLER->_playerCharacterModels[pcIndex.at(i)]->getLevel();
            if (PLAYERCONTROLLER->_playerCharacterModels[pcIndex.at(i)]->isAnyPartsEquiped() == true) {
                if (rarityCount < 4) {
                    rarityCount++;
                    // todo dummy image character
                    image = makeSprite(StringUtils::format("%d_i.png", mstId).c_str());
                    imageLayer->addChild(image);
                    image->setPosition(titleLabel->getPosition() + Vec2(((rarityCount - 1) * (image->getContentSize().width + 20)),
                                                                        -titleLabel->getContentSize().height / 2 - image->getContentSize().height / 2 + 5));

                    // level
                    auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %d", level), FONT_NAME_2, 16);
                    image->addChild(levelLabel);
                    levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
                    levelLabel->setPosition(Vec2(image->getContentSize().width / 2, 6 - 8));
                    levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
                }
            }
        }
    }
    imageLayer->setPosition(imageLayer->getPosition() + Vec2(-((rarityCount - 1) * (image->getContentSize().width + 20) / 2), 0));
    // level
    //        auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %d", level), FONT_NAME_2, 16);
    //        image->addChild(levelLabel);
    //        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    //        levelLabel->setPosition(Vec2(image->getContentSize().width/2, 6));
    //        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);

    //
    auto messLabel = Label::createWithTTF("売却する車なごの中に", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(messLabel);
    messLabel->setColor(COLOR_YELLOW);
    messLabel->setAnchorPoint(Vec2(0.5, 1));
    messLabel->setPosition(Vec2(winSize.width / 2,
                                image->getPositionY() - image->getContentSize().height / 2 - 30));

    auto messLabel2 = Label::createWithTTF("装備を付けた車なごが含まれています。", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(messLabel2);
    messLabel2->setColor(COLOR_YELLOW);
    messLabel2->setAnchorPoint(Vec2(0.5, 1));
    messLabel2->setPosition(Vec2(winSize.width / 2,
                                 messLabel->getPositionY() - messLabel->getContentSize().height / 2 - 5));

    auto messLabel3 = Label::createWithTTF("一緒に売却してよろしいですか？", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(messLabel3);
    messLabel3->setColor(COLOR_YELLOW);
    messLabel3->setAnchorPoint(Vec2(0.5, 1));
    messLabel3->setPosition(Vec2(winSize.width / 2,
                                 messLabel2->getPositionY() - messLabel2->getContentSize().height / 2 - 5));

    // add button close
    auto menu = MenuPriority::createWithPriority(kPriorityGarageSellRarityAlert - 1);
    addChild(menu);

    auto button = makeMenuItem("garage_sell_button.png", "はい", CC_CALLBACK_1(GarageSellRarityAlert::callBackbtn, this));
    menu->addChild(button);
    button->setTag(GSR_BT_YES_2);

    button = makeMenuItem("garage_sell_button.png", "いいえ", CC_CALLBACK_1(GarageSellRarityAlert::callBackbtn, this));
    menu->addChild(button);
    button->setTag(GSR_BT_NO);

    menu->alignItemsHorizontallyWithPadding(50);
    menu->setPosition(Vec2(winSize.width / 2, board->getPositionY() - board->getContentSize().height / 2 + 30 + button->getNormalImage()->getContentSize().height / 2));

    return true;
}


void GarageSellRarityAlert::callBackbtn(Ref* psender)
{
    // Destroy dialog
    int aIndex = ((Node*)psender)->getTag();
    PopUpDelegate* delegate = _delegate;
    removeFromParent();
    // call back
    if (aIndex != GSR_BT_NO && delegate != nullptr) {
        delegate->btCallback(aIndex, nullptr);
    }
}
