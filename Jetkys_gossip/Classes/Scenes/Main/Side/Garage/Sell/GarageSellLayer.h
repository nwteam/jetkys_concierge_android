#ifndef __syanago__GarageSellLayer__
#define __syanago__GarageSellLayer__

#include "create_func.h"
#include "cocos2d.h"
#include "LayerPriority.h"

USING_NS_CC;

enum GarageSell {
    GS_CHARACTER, GS_PART
};

enum GarageSellButton {
    GS_BT_OK, GS_BT_RESET
};

#define kPriorityGarageSellLayer -650
#define kNumberSelectMax 10

class GarageSellLayer : public create_func<GarageSellLayer>, public LayerPriority{
public:
    ~GarageSellLayer();
    
    typedef std::function<void(Ref *pSender)> ButtonCallback;
    bool init(GarageSell type, const ButtonCallback& okButtonCallback, const ButtonCallback& clearButtonCallback);
    using create_func::create;
    
    std::vector<int> _tmpSelectedIndexs;
    void updateInfomations();
    void menuItemCallback(Ref *pSender);
    int saleMethod(int itemId);
    
private:
    GarageSell _type;
    Label *_coinValue;
    Label *_selectValue;
    Label *_carryValue;
};



#endif /* defined(__syanago__GarageSellLayer__) */
