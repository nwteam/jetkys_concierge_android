#include "MainScene.h"
#include "GarageListLayer.h"
#include "GarageMainLayer.h"
#include "GaragePartSortLayer.h"
#include "GarageSellRarityAlert.h"
#include "StringDefines.h"
#include "SoundHelper.h"
#include "DetailLayer.h"
#include "FontDefines.h"
#include "SoundHelper.h"
#include "CharacterModel.h"
#include "PartModel.h"
#include "HeaderStatus.h"
#include "CharacterIconSprite.h"
#include "PartsIconSprite.h"

// development from SyanagoBaseLayer
#define kPrioritySyanagoBaseLayer -500
#define bt_back_bt 12345
#define kPrioritySyanagoBaseScrollView kPrioritySyanagoBaseLayer - 1
#define kPrioritySyanagoBaseMenu kPrioritySyanagoBaseLayer - 2
#define kColumn 5
#define kSizeCharacterIcon Size(118.0f, 118.0f)
#define kPlayerCharacterTagStart 1000

GarageListLayer::GarageListLayer():
    progress(nullptr)
    , api(nullptr)
    , _pcSelectedIndex(-1)
    , _currentCharacterAndPartsMenuStatus(GSPI_SYANAGO)
    , _sellLayer(nullptr)
    , _pcIDSelectedTemp(std::vector<int>())
    , _scrollView(nullptr)
    , _scrollBar(nullptr)
{
    progress = new DefaultProgress();
    api = new RequestAPI(progress);
}

GarageListLayer::~GarageListLayer()
{
    delete progress;
    delete api;
    _pcIDSelectedTemp.clear();
}


bool GarageListLayer::init(GarageMainLayer* layer)
{
    if (!LayerPriority::initWithPriority(kPrioritySyanagoBaseLayer)) {
        return false;
    }

    // development
    garageMainLayer = layer;
    _mainLayer = layer;

    showBackGround();
    showHeader();
    showPartsAndSyanagoSwitch();
    showSortMenu();

    PLAYERCONTROLLER->resetPlayerCharacterSorted();
    PLAYERCONTROLLER->resetPlayerPartsSorted();

    sortCharacter();

    return true;
}

void GarageListLayer::showBackGround()
{
    Sprite* background = Sprite::create("home_bg.png");
    background->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2, Director::getInstance()->getWinSize().height / 2));
    addChild(background, -10);
}

void GarageListLayer::showHeader()
{
    Size winSize = Director::getInstance()->getWinSize();

    Sprite* bg = Sprite::create("head_line_no2.png");
    bg->setPosition(Vec2(bg->getContentSize().width / 2, winSize.height - HEADER_STATUS::SIZE::HEIGHT - 12 - bg->getContentSize().height / 2));
    bg->setTag(TAG_SPRITE::HEADER_LINE);
    addChild(bg);

    auto btBack = makeMenuItem("back_button.png", CC_CALLBACK_1(GarageListLayer::onTapBackButton, this));
    btBack->setTag(bt_back_bt);
    auto menuBack = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    menuBack->setPosition(Vec2(bg->getPosition().x - bg->getContentSize().width / 2 + 14 + btBack->getNormalImage()->getContentSize().width / 2, bg->getPosition().y));
    menuBack->addChild(btBack);
    addChild(menuBack);

    Label* label = Label::createWithTTF("", FONT_NAME_2, 34, Size(Vec2::ZERO));
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->setAnchorPoint(Vec2(0, 0.5f));
    label->setPosition(menuBack->getPosition() + Vec2(btBack->getNormalImage()->getContentSize().width / 2 + 10,  -17));
    label->enableShadow();
    label->setTag(TAG_SPRITE::HEADER_LABEL);
    addChild(label);
}

void GarageListLayer::onTapBackButton(Ref* pSender)
{
    if (checkMenuReturnAndShowFairy()) {
        return;
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    if (_sellLayer) {
        removeSellLayer();
        showDatas();
        garageMainLayer->_reportMess->setString(PLAYERCONTROLLER->_messages.at(15));
    } else {
        garageMainLayer->showHide(false);
    }
}

void GarageListLayer::showPartsAndSyanagoSwitch()
{
    auto toggleButtonFront = makeMenuItem("garage_list_syanago_part_bt.png", nullptr);
    auto label = Label::createWithTTF("車なご", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->enableShadow();
    label->setPosition(Vec2(
                           toggleButtonFront->getNormalImage()->getContentSize().width / 2,
                           toggleButtonFront->getNormalImage()->getContentSize().height / 2 - 12));
    toggleButtonFront->addChild(label);

    auto toggleButtonBack = makeMenuItem("garage_list_syanago_part_bt.png", nullptr);
    label = Label::createWithTTF("装備", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->enableShadow();
    label->setPosition(Vec2(toggleButtonBack->getNormalImage()->getContentSize().width / 2,
                            toggleButtonBack->getNormalImage()->getContentSize().height / 2 - 12));
    toggleButtonBack->addChild(label);

    _toggleCharacterAndPartsMenu = MenuItemToggle::createWithCallback(
        CC_CALLBACK_1(GarageListLayer::onTapPartsAndSyanagoSwitch, this),
        toggleButtonFront,
        toggleButtonBack,
        NULL);
    _toggleCharacterAndPartsMenu->setSelectedIndex(1);
    _toggleCharacterAndPartsMenu->setTag(GSPI_SYANAGO_PART);

    auto menu = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    Sprite* headerLine = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::HEADER_LINE));
    menu->setPosition(headerLine->getPosition() + Vec2(headerLine->getContentSize().width / 2 + 16 + toggleButtonBack->getNormalImage()->getContentSize().width / 2, 0));
    menu->addChild(_toggleCharacterAndPartsMenu);
    addChild(menu, 1);
}

void GarageListLayer::onTapPartsAndSyanagoSwitch(Ref* pSender)
{
    if (checkMenuReturnAndShowFairy()) {
        return;
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    if (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) {
        if ((int)PLAYERCONTROLLER->_playerParts.size() == 0) {
            _toggleCharacterAndPartsMenu->setSelectedIndex(1);
            _dialog = DialogView::createWithShortMessage("装備がありません", 1);
            addChild(_dialog, DIALOG_ZORDER);
            return;
        }
        garageMainLayer->updateholdingLabel(PARTS);
    } else {
        garageMainLayer->updateholdingLabel(SYANAGO);
    }
    garageMainLayer->_reportMess->setString(PLAYERCONTROLLER->_messages.at(15));
    if (static_cast<MenuItemToggle*>(pSender)->getSelectedIndex() == 1) {
        _currentCharacterAndPartsMenuStatus = GSPI_SYANAGO;
        sortCharacter();
    } else {
        _currentCharacterAndPartsMenuStatus = GSPI_PART;
        sortParts();
    }

    if (_sellLayer) {
        setTitleLabel();
        removeSellLayer();
    }
}

void GarageListLayer::showSortMenu()
{
    Size winSize = Director::getInstance()->getWinSize();

    auto button = makeMenuItem("garage_list_sort_bt.png", CC_CALLBACK_1(GarageListLayer::onTapSortButton, this));
    button->setTag(TAG_SPRITE::SORT_BUTTON);

    Label* label = Label::createWithTTF("取得", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->enableShadow();
    label->setPosition(Vec2(button->getNormalImage()->getContentSize().width / 2, button->getNormalImage()->getContentSize().height / 2 - 12));
    label->setTag(TAG_SPRITE::SORT_LABEL);
    button->addChild(label);

    auto menu = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    Sprite* headerLine = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::HEADER_LINE));
    menu->setPosition(Vec2(winSize.width - 14 - button->getNormalImage()->getContentSize().width / 2,
                           headerLine->getPositionY()));
    menu->setTag(TAG_SPRITE::SORT_MENU);
    menu->addChild(button);

    addChild(menu, 10);
}

void GarageListLayer::onTapSortButton(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    setTitleLabel();
    switch (_currentCharacterAndPartsMenuStatus) {
    case GSPI_SYANAGO: {
        CharacterSelectSortLayer* popup = CharacterSelectSortLayer::create(CC_CALLBACK_1(GarageListLayer::characterSortButtonCallback, this), PLAYERCONTROLLER->currentSelectedSortCharacterKey);
        popup->setTag(TAG_LAYER::CHARACTER_SORT);
        addChild(popup, ZORDER_POPUP);
        break;
    }
    case GSPI_PART: {
        GaragePartSortLayer* popup = GaragePartSortLayer::create(CC_CALLBACK_1(GarageListLayer::partsSortButtonCallback, this), PLAYERCONTROLLER->currentSelectedSortPartsKey);
        popup->setTag(TAG_LAYER::PARTS_SORT);
        addChild(popup, ZORDER_POPUP);
        break;
    }
    default:
        break;
    }
}

void GarageListLayer::characterSortButtonCallback(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    PLAYERCONTROLLER->currentSelectedSortCharacterKey = static_cast<SortPlayerCharacter::KEY>(static_cast<Node*>(pSender)->getTag());
    sortCharacter();
    removeChildByTag(TAG_LAYER::CHARACTER_SORT);
}

void GarageListLayer::partsSortButtonCallback(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    PLAYERCONTROLLER->currentSelectedSortPartsKey = static_cast<SortPlayerParts::KEY>(static_cast<Node*>(pSender)->getTag());
    sortParts();
    removeChildByTag(TAG_LAYER::PARTS_SORT);
}

void GarageListLayer::sortCharacter()
{
    if (_sellLayer) {
        saveSaleIDSelectedTemp();
    }

    SortPlayerCharacter::sort(PLAYERCONTROLLER->currentSelectedSortCharacterKey);
    Label* label = static_cast<Label*>(getChildByTag(TAG_SPRITE::SORT_MENU)
                                       ->getChildByTag(TAG_SPRITE::SORT_BUTTON)
                                       ->getChildByTag(TAG_SPRITE::SORT_LABEL)
                                       );
    label->setString(SortPlayerCharacter::getLabelName(PLAYERCONTROLLER->currentSelectedSortCharacterKey));
    if (label->getString() == "メーカー") {
        label->setString("メーカ");
    } else if (label->getString() == "お気に入り") {
        label->setString("お気に");
    }

    showDatas();
}



void GarageListLayer::sortParts()
{
    if (_sellLayer) {
        saveSaleIDSelectedTemp();
    }
    SortPlayerParts::sort(PLAYERCONTROLLER->currentSelectedSortPartsKey);

    static_cast<Label*>(getChildByTag(TAG_SPRITE::SORT_MENU)
                        ->getChildByTag(TAG_SPRITE::SORT_BUTTON)
                        ->getChildByTag(TAG_SPRITE::SORT_LABEL)
                        )->setString(SortPlayerParts::getLabelName(PLAYERCONTROLLER->currentSelectedSortPartsKey));
    showDatas();
}

void GarageListLayer::showDatas()
{
    _numberOfCharacter = (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) ? (int)PLAYERCONTROLLER->_playerCharactersSorted.size() : (int)PLAYERCONTROLLER->_playerPartsSorted.size();

    createScrollView();

    // check sell layer
    if (_sellLayer && _scrollView->getContentSize().height > _scrollView->getViewSize().height - _sellLayer->getContentSize().height) {
        // if have _sellLayer then add it into total height
        _scrollView->setContentSize(Size(_scrollView->getContentSize().width, _scrollView->getContentSize().height + _sellLayer->getContentSize().height));
        _scrollView->getContainer()->setPositionY(_scrollView->getContainer()->getPositionY() - _sellLayer->getContentSize().height);
        _container->setPositionY(_container->getPositionY() + _sellLayer->getContentSize().height);

        // recreate scrollbar
        if (_scrollBar) {
            _scrollBar->removeFromParent();
        }
        Size winSize = Director::getInstance()->getWinSize();
        _scrollBar = ScrollBar::initScrollBar(_scrollView->getViewSize().height - 30, _scrollView->getViewSize().height, _scrollView->getContainer()->getContentSize().height, _scrollView->minContainerOffset().y);
        _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 75));
        _scrollBar->updateScrollPoint(_scrollView->getContentOffset().y);
        addChild(_scrollBar);
    }

    setTitleLabel();

    if (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) {
        showDatasCharacter();
    } else if (_currentCharacterAndPartsMenuStatus == GSPI_PART) {
        showDatasPart();
    }
}

void GarageListLayer::createScrollView()
{
    if (_scrollView != nullptr) {
        _scrollView->removeFromParent();
    }
    const int row = _numberOfCharacter / kColumn + ((_numberOfCharacter % kColumn) > 0 ? 1 : 0);
    _container = LayerColor::create(Color4B(0, 0, 0, 0), kSizeCharacterIcon.width * kColumn, kSizeCharacterIcon.height * row);
    auto scrollContainer = LayerColor::create(Color4B(0, 0, 0, 0), _container->getContentSize().width, _container->getContentSize().height);
    scrollContainer->addChild(_container);

    const Size winSize = Director::getInstance()->getWinSize();
    _scrollView = ScrollViewPriority::create(Size(kSizeCharacterIcon.width * kColumn, winSize.height  - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 60), kPrioritySyanagoBaseScrollView, scrollContainer);
    Sprite* headerLine = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::HEADER_LINE));
    _scrollView->setPosition(Vec2(4, headerLine->getPositionY() - headerLine->getContentSize().height / 2 - 7 - _scrollView->getViewSize().height));
    _scrollView->setDirection(ScrollView::Direction::VERTICAL);
    _scrollView->setBounceable(false);
    _scrollView->setDelegate(this);
    addChild(_scrollView);

    scrollContainer->setPosition(_container->getPosition() + Vec2(0, -_container->getContentSize().height + _scrollView->getViewSize().height));

    if (_scrollBar != nullptr) {
        _scrollBar->removeFromParent();
    }
    _scrollBar = ScrollBar::initScrollBar(winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 60, _scrollView->getViewSize().height, _scrollView->getContainer()->getContentSize().height, _scrollView->getContentOffset().y);
    _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 75 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    addChild(_scrollBar);
}

void GarageListLayer::showDatasCharacter()
{
    _menuItems = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    _menuItems->setPosition(Vec2(0, _container->getContentSize().height));
    _menuItems->_scrollView = _scrollView;
    _container->addChild(_menuItems, 2);

    for (int i = 0; i < _numberOfCharacter; i++) {
        CharacterIconSprite* cell = CharacterIconSprite::create(i, CC_CALLBACK_1(GarageListLayer::onTapCharacterIcon, this));
        cell->showStatusLabel(i);
        _menuItems->addChild(cell);

        if (_sellLayer) {
            // 売却車なご選択時にソートが呼ばれた時の処理
            const CharacterIconSprite::USED_STATUS STATUS = cell->checkSyanagoIsNew();
            if (STATUS == CharacterIconSprite::USED_STATUS::USED ||
                PLAYERCONTROLLER->_playerCharactersSorted.at(i)->getLock() == 1) {
                cell->setEnabled(false);
            }
            for (int sIndex = 0; sIndex < _pcIDSelectedTemp.size(); sIndex++) {
                if (PLAYERCONTROLLER->_playerCharactersSorted.at(i)->getID() == _pcIDSelectedTemp.at(sIndex)) {
                    showSelectCounter(cell, sIndex);
                    _sellLayer->_tmpSelectedIndexs.at(sIndex) = i;
                }
            }
        }
    }
}

void GarageListLayer::showDatasPart()
{
    _menuItems = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    _menuItems->setPosition(Vec2(0, _container->getContentSize().height));
    _menuItems->_scrollView = _scrollView;
    _container->addChild(_menuItems, 2);

    for (int i = 0; i < _numberOfCharacter; i++) {
        PartsIconSprite* cell = PartsIconSprite::create(i, CC_CALLBACK_1(GarageListLayer::onTapPartsIcon, this));
        cell->showStatusLabel(i);
        _menuItems->addChild(cell);

        if (_sellLayer) {
            if (cell->checkPartsIsNew() == PartsIconSprite::USED_STATUS::USED) {
                cell->setEnabled(false);
            }
            for (int sIndex = 0; sIndex < _pcIDSelectedTemp.size(); sIndex++) {
                if (PLAYERCONTROLLER->_playerPartsSorted.at(i)->getID() == _pcIDSelectedTemp.at(sIndex)) {
                    showSelectCounter(cell, sIndex);
                    _sellLayer->_tmpSelectedIndexs.at(sIndex) = i;
                }
            }
        }
    }
}


void GarageListLayer::onTapCharacterIcon(cocos2d::Ref* pSender)
{
    if (checkMenuReturnAndShowFairy()) {
        return;
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    _pcSelectedIndex = static_cast<MenuItemSprite*>(pSender)->getTag() - kPlayerCharacterTagStart;

    // newlabel 削除
    if (static_cast<MenuItemSprite*>(pSender)->getChildByTag(2 * kPlayerCharacterTagStart + 2 * _pcSelectedIndex)) {
        static_cast<MenuItemSprite*>(pSender)->removeChildByTag(2 * kPlayerCharacterTagStart + 2 * _pcSelectedIndex);
        PLAYERCONTROLLER->_playerCharactersSorted.at(_pcSelectedIndex)->saveIsNew(0);
    }

    if (_sellLayer) {
        selectIconForSell(pSender);
        return;
    }
    auto popup = GarageCharacterPopupLayer::create(CC_CALLBACK_1(GarageListLayer::characterMenuButtonCallback, this),
                                                   CC_CALLBACK_0(GarageListLayer::onResponseLockCharacter, this),
                                                   CC_CALLBACK_1(GarageListLayer::showAwakeView, this),
                                                   _pcSelectedIndex);
    popup->setTag(TAG_LAYER::CHARACTER_MENU);
    addChild(popup, ZORDER_POPUP);
}

void GarageListLayer::onTapPartsIcon(Ref* pSender)
{
    if (checkMenuReturnAndShowFairy()) {
        return;
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    _pcSelectedIndex = static_cast<MenuItemSprite*>(pSender)->getTag() - kPlayerCharacterTagStart;

    MenuItemSprite* selectedIcon = static_cast<MenuItemSprite*>(pSender);
    if (selectedIcon->getChildByTag(2 * kPlayerCharacterTagStart + 2 * _pcSelectedIndex)) {
        selectedIcon->removeChildByTag(2 * kPlayerCharacterTagStart + 2 * _pcSelectedIndex);
        PLAYERCONTROLLER->_playerPartsSorted.at(_pcSelectedIndex)->saveIsNew(0);
    }

    if (_sellLayer) {
        selectIconForSell(pSender);
        return;
    }
    auto popup = GaragePartPopupLayer::create(CC_CALLBACK_1(GarageListLayer::partsMenuButtonCallback, this), _pcSelectedIndex);
    popup->setTag(TAG_LAYER::PARTS_MENU);
    addChild(popup, ZORDER_POPUP);
}

void GarageListLayer::selectIconForSell(cocos2d::Ref* pSender)
{
    int j = -1;
    for (int i = 0; i < _sellLayer->_tmpSelectedIndexs.size(); i++) {
        if (_pcSelectedIndex == _sellLayer->_tmpSelectedIndexs.at(i)) {
            j = i;
            _container->removeChildByTag(kPlayerBageTagStart + i);
        } else if (j != -1) {
            auto bage = _container->getChildByTag(kPlayerBageTagStart + i);
            bage->setTag(kPlayerBageTagStart + i - 1);
            static_cast<Label*>(bage->getChildByTag(123))->setString(StringUtils::format("%d", i));
        }
    }
    if (j == -1 && _sellLayer->_tmpSelectedIndexs.size() < kNumberSelectMax) {
        _sellLayer->_tmpSelectedIndexs.push_back(_pcSelectedIndex);
        showSelectCounter(static_cast<MenuItemSprite*>(pSender), static_cast<int>(_sellLayer->_tmpSelectedIndexs.size()) - 1);
        _sellLayer->updateInfomations();
    } else if (j != -1) {
        _sellLayer->_tmpSelectedIndexs.erase(_sellLayer->_tmpSelectedIndexs.begin() + j);
        _sellLayer->updateInfomations();
    }
}

void GarageListLayer::btCallback(int btIndex, void* object)
{
    if (btIndex == GSR_BT_YES) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        // check rarity and confirm
        if (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO && checkParts()) {
            std::vector<int>pId;
            for (int i = 0; i < _sellLayer->_tmpSelectedIndexs.size(); i++) {
                int ID = (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) ? PLAYERCONTROLLER->_playerCharactersSorted.at(_sellLayer->_tmpSelectedIndexs.at(i))->getID() : PLAYERCONTROLLER->_playerPartsSorted.at(_sellLayer->_tmpSelectedIndexs.at(i))->getID();
                pId.push_back(ID);
            }
            auto layer = GarageSellRarityAlert::createWithPartsType(pId);
            layer->_delegate = this;
            addChild(layer, 1000);
        } else {
            saleRequest();
        }
    } else if (btIndex == GSR_BT_YES_2) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        saleRequest();
    } else if (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) {
        if (btIndex != GarageCharacterPopupLayer::BUTTON_INDEX::COMPOSITION) {
            SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        }
        _pcSelectedIndex = -1;
    } else if (_currentCharacterAndPartsMenuStatus == GSPI_PART) {
        if (btIndex != GaragePartPopupLayer::BUTTON_INDEX::SELL) {
            SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        }
        _pcSelectedIndex = -1;
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }
}

void GarageListLayer::btDialogCallback(ButtonIndex tag)
{
    if (tag != BT_YES || _sellLayer->_tmpSelectedIndexs.size() <= 0) {
        return;
    }
    if (checkRarity()) {
        std::vector<int>pId;
        for (int i = 0; i < _sellLayer->_tmpSelectedIndexs.size(); i++) {
            int ID = (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) ? PLAYERCONTROLLER->_playerCharactersSorted.at(_sellLayer->_tmpSelectedIndexs.at(i))->getID() : PLAYERCONTROLLER->_playerPartsSorted.at(_sellLayer->_tmpSelectedIndexs.at(i))->getID();
            pId.push_back(ID);
        }
        auto layer = GarageSellRarityAlert::createWithType(_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO ? GST_CHARACTER : GST_PART, pId);
        layer->_delegate = this;
        addChild(layer, 1000);
        return;
    }
    if (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO && checkParts()) {
        std::vector<int>pId;
        for (int i = 0; i < _sellLayer->_tmpSelectedIndexs.size(); i++) {
            int ID = (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) ? PLAYERCONTROLLER->_playerCharactersSorted.at(_sellLayer->_tmpSelectedIndexs.at(i))->getID() : PLAYERCONTROLLER->_playerPartsSorted.at(_sellLayer->_tmpSelectedIndexs.at(i))->getID();
            pId.push_back(ID);
        }
        auto layer = GarageSellRarityAlert::createWithPartsType(pId);
        layer->_delegate = this;
        addChild(layer, 1000);
        return;
    }
    saleRequest();
}

bool GarageListLayer::checkRarity()
{
    for (int i = 0; i < _sellLayer->_tmpSelectedIndexs.size(); i++) {
        int index = _sellLayer->_tmpSelectedIndexs.at(i);
        if (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) {
            if (CharacterModel::find(PLAYERCONTROLLER->_playerCharactersSorted.at(index)->getCharactersId())->getRarity() >= 3) {
                return true;
            }
        } else if (PartModel::find(PLAYERCONTROLLER->_playerPartsSorted.at(index)->getPartsId())->getRarity() >= 3) {
            return true;
        }
    }
    return false;
}

bool GarageListLayer::checkParts()
{
    for (int i = 0; i < _sellLayer->_tmpSelectedIndexs.size(); i++) {
        if (_sellLayer->_tmpSelectedIndexs.at(i) >= 0 &&  PLAYERCONTROLLER->_playerCharactersSorted.at(_sellLayer->_tmpSelectedIndexs.at(i))->isAnyPartsEquiped()) {
            return true;
        }
    }
    return false;
}

void GarageListLayer::saleRequest()
{
    if (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) {
        requestSaleCharacter();
    } else {
        requestSaleParts();
    }
}

void GarageListLayer::requestSaleCharacter()
{
    std::map<int, std::string>saleIDs;
    for (int i = 0; i < _sellLayer->_tmpSelectedIndexs.size(); i++) {
        int ID = (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) ? PLAYERCONTROLLER->_playerCharactersSorted.at(_sellLayer->_tmpSelectedIndexs.at(i))->getID() : PLAYERCONTROLLER->_playerPartsSorted.at(_sellLayer->_tmpSelectedIndexs.at(i))->getID();
        saleIDs[ID] = "true";
    }
    api->saleCharacter(saleIDs, CC_CALLBACK_1(GarageListLayer::onResponseSaleCharacter, this));
}

void GarageListLayer::requestSaleParts()
{
    std::map<int, std::string>saleIDs;
    for (int i = 0; i < _sellLayer->_tmpSelectedIndexs.size(); i++) {
        int ID = PLAYERCONTROLLER->_playerPartsSorted.at(_sellLayer->_tmpSelectedIndexs.at(i))->getID();
        saleIDs[ID] = "true";
    }
    api->saleParts(saleIDs, CC_CALLBACK_1(GarageListLayer::onResponseSaleParts, this));
}

void GarageListLayer::onResponseSaleCharacter(Json* response)
{
    Json* data = Json_getItem(Json_getItem(response, "sale_character"), "data");

    // Save
    PLAYERCONTROLLER->_player->setFreeCoin(atoi(Json_getString(Json_getItem(data, "players"), "free_coin", "-1")));
    PLAYERCONTROLLER->_player->update();

    // Show
    garageMainLayer->updateInfoLayer();

    // remove character
    Json* jsonDeleteChara = Json_getItem(data,  "delete_character_ids");
    if (jsonDeleteChara && jsonDeleteChara->type == Json_Array) {
        Json* tempchild;
        for (tempchild = jsonDeleteChara->child; tempchild; tempchild = tempchild->next) {
            PLAYERCONTROLLER->deleteCharacter(atoi(tempchild->valueString));
        }
    }
    // remove parts equiped by sold characters
    Json* jsonDeleteParts = Json_getItem(data,  "delete_parts_ids");
    if (jsonDeleteParts && jsonDeleteParts->type == Json_Array) {
        Json* tempchild;
        for (tempchild = jsonDeleteParts->child; tempchild; tempchild = tempchild->next) {
            PLAYERCONTROLLER->deletePart(atoi(tempchild->valueString));
        }
    }
    // refresh
    removeGarageSellLayer();
    garageMainLayer->updateholdingLabel(SYANAGO);
}

void GarageListLayer::onResponseSaleParts(Json* response)
{
    Json* data = Json_getItem(Json_getItem(response, "sale_parts"), "data");

    // Save
    PLAYERCONTROLLER->_player->setFreeCoin(atoi(Json_getString(Json_getItem(data, "players"), "free_coin", "-1")));
    PLAYERCONTROLLER->_player->update();

    // Show
    garageMainLayer->updateInfoLayer();

    Json* jsonDeleteParts = Json_getItem(data,  "delete_parts_ids");
    if (jsonDeleteParts && jsonDeleteParts->type == Json_Array) {
        Json* tempchild;
        for (tempchild = jsonDeleteParts->child; tempchild; tempchild = tempchild->next) {
            PLAYERCONTROLLER->deletePart(atoi(tempchild->valueString));
        }
    }
    removeGarageSellLayer();
    garageMainLayer->updateholdingLabel(PARTS);
}

void GarageListLayer::onResponseLockCharacter()
{
    Node* menuItemSprite = _menuItems->getChildByTag(kPlayerCharacterTagStart + _pcSelectedIndex);
    if (menuItemSprite->getChildByName("lock_icon") == NULL) {
        Sprite* lockIcon = Sprite::create("lock_icon.png");
        lockIcon->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        lockIcon->setPosition(Point(10, menuItemSprite->getContentSize().height - 10));
        lockIcon->setName("lock_icon");
        menuItemSprite->addChild(lockIcon);
    } else {
        menuItemSprite->getChildByName("lock_icon")->removeFromParent();
    }
    sortCharacter();
}

void GarageListLayer::removeGarageSellLayer()
{
    _pcSelectedIndex = -1;
    removeSellLayer();

    // タイトルのテスクトをセット
    setTitleLabel();

    // 売却後は、売却前のソートで並んで表示される事。
    if (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) {
        sortCharacter();
    } else {
        sortParts();
    }
    garageMainLayer->_reportMess->setString(PLAYERCONTROLLER->_messages.at(15));// *
}

void GarageListLayer::showGarageSellCharacterLayer()
{
    _pcIDSelectedTemp.clear();

    _toggleCharacterAndPartsMenu->setVisible(false);

    _sellLayer = GarageSellLayer::create(GS_CHARACTER,
                                         CC_CALLBACK_1(GarageListLayer::saleSelectedCallbackOK, this),
                                         CC_CALLBACK_1(GarageListLayer::saleSelectedCallbackClear, this));
    _sellLayer->setPositionY(50);
    addChild(_sellLayer, 10);

    for (int i = 0; i < _numberOfCharacter; i++) {
        if (_container->getChildByTag(2 * kPlayerCharacterTagStart + 2 * i + 1)) {
            static_cast<MenuItemSprite*>(_menuItems->getChildByTag(kPlayerCharacterTagStart + i))->setEnabled(false);
        } else if (i == _pcSelectedIndex) {
            _sellLayer->_tmpSelectedIndexs.push_back(_pcSelectedIndex);
            showSelectCounter((MenuItemSprite*)(_menuItems->getChildByTag(kPlayerCharacterTagStart + i)), (int)_sellLayer->_tmpSelectedIndexs.size() - 1);
        } else if (PLAYERCONTROLLER->_playerCharactersSorted.at(i)->getLock() == 1) {
            static_cast<MenuItemSprite*>(_menuItems->getChildByTag(kPlayerCharacterTagStart + i))->setEnabled(false);
        }
    }
    _sellLayer->updateInfomations();

    // always add sizeOfSellLayer in to total Size don't need to any condition
    // if container has height bound out of _selllayer -> add height to container and change position
    if (_scrollView->getContentSize().height > _scrollView->getViewSize().height - _sellLayer->getContentSize().height) {
        _scrollView->setContentSize(Size(_scrollView->getContentSize().width, _scrollView->getContentSize().height + _sellLayer->getContentSize().height));
        _scrollView->getContainer()->setPositionY(_scrollView->getContainer()->getPositionY() - _sellLayer->getContentSize().height);
        _container->setPositionY(_container->getPositionY() + _sellLayer->getContentSize().height);
    }

    Size winSize = Director::getInstance()->getWinSize();
    // recreate scrollbar
    if (_scrollBar) {
        _scrollBar->removeFromParent();
    }
    _scrollBar = ScrollBar::initScrollBar(_scrollView->getViewSize().height - 30, _scrollView->getViewSize().height, _scrollView->getContainer()->getContentSize().height, _scrollView->minContainerOffset().y);
    _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 75));
    _scrollBar->updateScrollPoint(_scrollView->getContentOffset().y);
    addChild(_scrollBar);

    setTitleLabel();
}

void GarageListLayer::showGarageSellPartsLayer()
{
    _pcIDSelectedTemp.clear();

    _toggleCharacterAndPartsMenu->setVisible(false);

    _sellLayer = GarageSellLayer::create(GS_PART,
                                         CC_CALLBACK_1(GarageListLayer::saleSelectedCallbackOK, this),
                                         CC_CALLBACK_1(GarageListLayer::saleSelectedCallbackClear, this));
    addChild(_sellLayer, 10);
    _sellLayer->setPositionY(50);

    for (int i = 0; i < _numberOfCharacter; i++) {
        if (_container->getChildByTag(2 * kPlayerCharacterTagStart + 2 * i + 1)) {
            ((MenuItemSprite*)(_menuItems->getChildByTag(kPlayerCharacterTagStart + i)))->setEnabled(false);
        } else if (i == _pcSelectedIndex) {
            _sellLayer->_tmpSelectedIndexs.push_back(_pcSelectedIndex);
            showSelectCounter((MenuItemSprite*)(_menuItems->getChildByTag(kPlayerCharacterTagStart + i)), (int)_sellLayer->_tmpSelectedIndexs.size() - 1);
        }
    }
    _sellLayer->updateInfomations();

    // always add sizeOfSellLayer in to total Size don't need to any condition
    // if container has height bound out of _selllayer -> add height to container and change position
    if (_scrollView->getContentSize().height > _scrollView->getViewSize().height - _sellLayer->getContentSize().height) {
        _scrollView->setContentSize(Size(_scrollView->getContentSize().width, _scrollView->getContentSize().height + _sellLayer->getContentSize().height));
        _scrollView->getContainer()->setPositionY(_scrollView->getContainer()->getPositionY() - _sellLayer->getContentSize().height);
        _container->setPositionY(_container->getPositionY() + _sellLayer->getContentSize().height);
    }

    Size winSize = Director::getInstance()->getWinSize();
    // recreate scrollbar
    if (_scrollBar) {
        _scrollBar->removeFromParent();
    }
    _scrollBar = ScrollBar::initScrollBar(_scrollView->getViewSize().height - 30, _scrollView->getViewSize().height, _scrollView->getContainer()->getContentSize().height, _scrollView->minContainerOffset().y);
    _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 75));
    _scrollBar->updateScrollPoint(_scrollView->getContentOffset().y);
    addChild(_scrollBar);

    setTitleLabel();
}

void GarageListLayer::saleSelectedCallbackOK(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    int count = (int)_sellLayer->_tmpSelectedIndexs.size();
    int selected = 0;
    for (int i = 0; i < count; i++) {
        if (_sellLayer->_tmpSelectedIndexs.at(i) > -1) {
            selected++;
        }
    }
    if (selected > 0) {
        std::string message = "売却してよろしいでしょうか？";
        std::string title = "";
        _dialog = DialogView::createWithShortMessage(message, 2);
        addChild(_dialog, 30);
        _dialog->_delegate  = this;
    } else {
        std::string message;
        if (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) {
            message = "車なごが選択されていません";
        } else {
            message = "装備が選択されていません";
        }
        std::string title = "";
        _dialog = DialogView::createWithShortMessage(message, 1);
        _dialog->_delegate  = this;
        addChild(_dialog, 30);
    }
}

void GarageListLayer::saleSelectedCallbackClear(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    for (int i = 0; i < _sellLayer->_tmpSelectedIndexs.size(); i++) {
        _container->removeChildByTag(kPlayerBageTagStart + i);
    }
    _sellLayer->_tmpSelectedIndexs.clear();
    _sellLayer->updateInfomations();
}

void GarageListLayer::showSelectCounter(MenuItemSprite* item, int count)
{
    auto circle = makeSprite("garage_select_point.png");
    circle->setPosition(_container->convertToNodeSpace(item->getParent()->convertToWorldSpace(item->getPosition())));
    circle->setTag(kPlayerBageTagStart + count);

    auto counter = Label::createWithTTF(StringUtils::format("%d", count + 1), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    counter->enableShadow();
    counter->setPosition(Vec2(circle->getContentSize().width / 2, circle->getContentSize().height / 2 - 11));
    counter->setTag(123);

    circle->addChild(counter, 5);
    _container->addChild(circle, 5);
}

void GarageListLayer::saveSaleIDSelectedTemp()
{
    _pcIDSelectedTemp.clear();
    for (int i = 0; i < _sellLayer->_tmpSelectedIndexs.size(); i++) {
        int id;
        int index = _sellLayer->_tmpSelectedIndexs.at(i);
        if (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) {
            id = PLAYERCONTROLLER->_playerCharactersSorted.at(index)->getID();
        } else {
            id = PLAYERCONTROLLER->_playerPartsSorted.at(index)->getID();
        }
        _pcIDSelectedTemp.push_back(id);
    }
}

void GarageListLayer::scrollViewDidScroll(ScrollView* view)
{
    _scrollBar->updateScrollPoint(view->getContentOffset().y);
}

void GarageListLayer::scrollViewDidZoom(ScrollView* view) {}

void GarageListLayer::setTitleLabel()
{
    std::string title = (_sellLayer) ? "売却" : "ガレージ";
    title += (_currentCharacterAndPartsMenuStatus == GSPI_SYANAGO) ? "【車なご】" : "【装備】";
    static_cast<Label*>(getChildByTag(TAG_SPRITE::HEADER_LABEL))->setString(title);
}

void GarageListLayer::removeSellLayer()
{
    _toggleCharacterAndPartsMenu->setVisible(true);
    _sellLayer->removeFromParent();
    _sellLayer = nullptr;
}

bool GarageListLayer::checkMenuReturnAndShowFairy()
{
    garageMainLayer->_mainScene->homeLayer->showFairy();
    return garageMainLayer->isMoving() || garageMainLayer->_mainScene->_isShowing;
}

void GarageListLayer::characterMenuButtonCallback(Ref* sender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    removeChildByTag(TAG_LAYER::CHARACTER_MENU);

    int btIndex = static_cast<Node*>(sender)->getTag();
    if (btIndex == GarageCharacterPopupLayer::BUTTON_INDEX::COMPOSITION) {
        garageMainLayer->showLayer(GLI_COMPOSITION, _pcSelectedIndex);
    } else if (btIndex == GarageCharacterPopupLayer::BUTTON_INDEX::CUSTOMIZE) {
        garageMainLayer->showLayer(GLI_CUSTOMIZE, _pcSelectedIndex);
    } else if (btIndex == GarageCharacterPopupLayer::BUTTON_INDEX::SELL) {
        garageMainLayer->_reportMess->setString(PLAYERCONTROLLER->_messages.at(16));
        showGarageSellCharacterLayer();
    } else if (btIndex == GarageCharacterPopupLayer::BUTTON_INDEX::DETAIL) {
        garageMainLayer->showDetailPlayerCharacter(PLAYERCONTROLLER->_playerCharactersSorted.at(_pcSelectedIndex)->getID());
    }
    _pcSelectedIndex = -1;
}

void GarageListLayer::partsMenuButtonCallback(Ref* sender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);

    int btIndex = static_cast<Node*>(sender)->getTag();
    if (btIndex == GaragePartPopupLayer::BUTTON_INDEX::SELL) {
        garageMainLayer->_reportMess->setString(PLAYERCONTROLLER->_messages.at(16));
        showGarageSellPartsLayer();
    } else if (btIndex == GaragePartPopupLayer::BUTTON_INDEX::DETAIL) {
        garageMainLayer->showDetailParts(PLAYERCONTROLLER->_playerPartsSorted.at(_pcSelectedIndex)->getPartsId(), _pcSelectedIndex);
    }
    _pcSelectedIndex = -1;
    removeChildByTag(TAG_LAYER::PARTS_MENU);
}

void GarageListLayer::showAwakeView(const int playerCharacterId)
{
    garageMainLayer->showLayer(GLI_AWAKE_CHARACTER, playerCharacterId);
}