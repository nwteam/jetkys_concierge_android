#ifndef __syanago__GaragePartPopupLayer__
#define __syanago__GaragePartPopupLayer__

#include <functional>
#include "create_func.h"
#include "jCommon.h"
#include "LayerPriority.h"

class GaragePartPopupLayer : public create_func<GaragePartPopupLayer>, public LayerPriority
{
    
public:
    typedef std::function<void(Ref*)> PartsMenuButtonCallback;
    bool init(const PartsMenuButtonCallback& callback, int playerPartsIndex);
    using create_func::create;
    
    enum BUTTON_INDEX {
        SELL,
        DETAIL
    };
    
    CC_SYNTHESIZE(int, _playerPartIndex, PlayerPartIndex)  //index in _playerCharacters

private:
    void cancelButtonCallback(Ref* sender);
};

#endif /* defined(__syanago__GaragePartPopupLayer__) */
