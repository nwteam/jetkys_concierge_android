#ifndef __syanago__GarageListLayer__
#define __syanago__GarageListLayer__

#include "create_func.h"
#include "SyanagoBaseLayer.h"
#include "DialogView.h"
#include "DialogDefine.h"
#include "extensions/cocos-ext.h"
#include "GarageCharacterPopupLayer.h"
#include "CharacterSelectSortLayer.h"
#include "GaragePartSortLayer.h"
#include "GaragePartPopupLayer.h"
#include "GarageSellLayer.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"
#include "GarageMainLayer.h"
#include "ScrollBar.h"
#include "SortPlayerParts.h"


USING_NS_CC;
USING_NS_CC_EXT;

enum GarageSyanagoPartIndex {
    GSPI_SYANAGO_PART = 10,
    GSPI_SYANAGO, GSPI_PART
};

enum SaleModeTag{
    YES_SALE, NO_SALE
};

class GarageListLayer : public create_func<GarageListLayer>, public SyanagoBaseLayer, public DialogDelegate
{
public:
    GarageListLayer();
    ~GarageListLayer();
    
    bool init(GarageMainLayer* layer);
    using create_func::create;
    
    void showDatas();
    void onTapCharacterIcon(Ref *pSender);
    void onTapPartsIcon(Ref *pSender);
    void selectIconForSell(Ref *pSender);
    
    virtual void btCallback(int btIndex, void *object) override;
    
    void btDialogCallback(ButtonIndex aIndex);
    
    // development
    void showGarageSellCharacterLayer();
    void showGarageSellPartsLayer();
    void removeGarageSellLayer();
    void saleSelectedCallbackOK(Ref *pSender);
    void saleSelectedCallbackClear(Ref *pSender);
    
    
    bool checkRarity();
    bool checkParts();
    void requestSaleCharacter();
    void requestSaleParts();
    void saleRequest();
    
    //resave sale IDs
    void saveSaleIDSelectedTemp();
    
    void scrollViewDidScroll(ScrollView* view);
    void scrollViewDidZoom(ScrollView* view);
    
    void setTitleLabel();
    void removeSellLayer();
    
    
    GarageSellLayer *_sellLayer;
    //save index of _playerCharacters selected
    int _pcSelectedIndex;
    GarageMainLayer *garageMainLayer;    
    
private:
    void showBackGround();
    // development from SyanagoBaseLayer
    void showHeader();
    void showPartsAndSyanagoSwitch();
    void showSortMenu();
    void sortCharacter();
    void sortParts();
    void createScrollView();
    void showDatasCharacter();
    void showDatasPart();
    void showSelectCounter(MenuItemSprite *item, int counter);
    
    void onTapPartsAndSyanagoSwitch(Ref *pSender);
    void onTapBackButton(Ref *pSender);
    void onTapSortButton(Ref *pSender);
    
    void onResponseSaleCharacter(Json* response);
    void onResponseSaleParts(Json* response);
    void onResponseLockCharacter();
    //development
    bool checkMenuReturnAndShowFairy();
    
    void characterMenuButtonCallback(Ref* sender);
    void partsMenuButtonCallback(Ref* sender);
    void characterSortButtonCallback(Ref* pSender);
    void partsSortButtonCallback(Ref* pSender);
    
    void showAwakeView(const int playerCharacterId);
    
    //development
    enum TAG_SPRITE {
        HEADER_LINE = 200,
        HEADER_LABEL,
        SORT_MENU,
        SORT_BUTTON,
        SORT_LABEL
    };
    
    enum TAG_LAYER {
        CHARACTER_SORT = 300,
        PARTS_SORT,
        CHARACTER_MENU,
        PARTS_MENU,
        AWAKE_CHARACTER,
    };
    
    RequestAPI *api;
    DefaultProgress *progress;
    GarageSyanagoPartIndex _currentCharacterAndPartsMenuStatus;
    MenuItemToggle* _toggleCharacterAndPartsMenu;
    MenuPriority *_menuItems;
    std::vector<int> _pcIDSelectedTemp;
    
    
    
    //development
    ScrollBar* _scrollBar;
    ScrollViewPriority *_scrollView;
    Layer *_container;
    int _numberOfCharacter;
};

#endif /* defined(__syanago__GarageListLayer__) */