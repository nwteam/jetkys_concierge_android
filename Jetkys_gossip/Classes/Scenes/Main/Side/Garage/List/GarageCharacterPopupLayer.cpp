#include "GarageCharacterPopupLayer.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include "StringDefines.h"
#include "SoundHelper.h"
#include "CharacterModel.h"

GarageCharacterPopupLayer::GarageCharacterPopupLayer():
    progress(nullptr)
    , api(nullptr)
    , _awakeButton(nullptr)
{
    progress = new DefaultProgress();
    api = new RequestAPI(progress);
}

GarageCharacterPopupLayer::~GarageCharacterPopupLayer()
{
    delete progress;
    delete api;
}


bool GarageCharacterPopupLayer::init(const CharacterMenuButtonCallback &buttonCallback,
                                     const LockCharacterResponseCallback &responseCallback,
                                     const AwakeCharacterResponseCallback& awakeCharacterResposeCallback, int playerCharacterIndex)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _lockCharacterResponseCallback = responseCallback;
    _awakeCharacterResponseCallback = awakeCharacterResposeCallback;

    _playerCharactersModel = PLAYERCONTROLLER->_playerCharactersSorted.at(playerCharacterIndex);
    showBackground();
    showCharacter();
    showCharacterStatus();
    showButtons(buttonCallback);

    return true;
}

void GarageCharacterPopupLayer::showBackground()
{
    Sprite* background = Sprite::create("garage_popup_character_bg.png");
    background->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2, Director::getInstance()->getWinSize().height / 2));
    background->setTag(TAG_SPRITE::BACKGROUND);
    addChild(background);
}

void GarageCharacterPopupLayer::showCharacter()
{
    Size winSize = Director::getInstance()->getWinSize();
    Sprite* background = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BACKGROUND));

    auto nameLabel = Label::createWithTTF(CharacterModel::find(_playerCharactersModel->getCharactersId())->getCarName(), FONT_NAME_2, 30, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    nameLabel->setPosition(background->getPosition() + Vec2(0, background->getContentSize().height / 2 - 30));
    nameLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    nameLabel->setColor(COLOR_YELLOW);
    nameLabel->enableShadow();
    addChild(nameLabel);

    auto image = makeSprite(StringUtils::format(IMAGE_CHARACTER_ICON, _playerCharactersModel->getCharactersId()).c_str());
    image->setPosition(Vec2(winSize.width / 2 - image->getContentSize().width / 2 - 20, nameLabel->getPosition().y - 100));
    image->setTag(TAG_SPRITE::CHARACTER_IMAGE);
    addChild(image);
}

void GarageCharacterPopupLayer::showCharacterStatus()
{
    Size winSize = Director::getInstance()->getWinSize();
    Sprite* image = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::CHARACTER_IMAGE));

    Label* levelLabel = Label::createWithTTF(StringUtils::format("Lv %d", _playerCharactersModel->getLevel()), FONT_NAME_2, 22);
    levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    levelLabel->setPosition(image->getPosition() + Vec2(0, -image->getContentSize().height / 2 + 6 - 11));
    levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
    addChild(levelLabel, 4);

    Label* powerLabel = Label::createWithTTF("馬力 ", FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    powerLabel->setPosition(image->getPosition() + Vec2(image->getContentSize().width / 2 + 15, 30 - 10));
    powerLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    powerLabel->enableShadow();
    addChild(powerLabel);

    Label* powerValue = Label::createWithTTF(FormatWithCommas(_playerCharactersModel->getExercise()), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    powerValue->setPosition(powerLabel->getPosition() + Vec2(130,  0));
    powerValue->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    powerValue->enableShadow();
    addChild(powerValue);

    Label* techniqueLabel = Label::createWithTTF("技量 ", FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    techniqueLabel->setPosition(image->getPosition() + Vec2(image->getContentSize().width / 2 + 15, -10));
    techniqueLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    techniqueLabel->enableShadow();
    addChild(techniqueLabel);

    Label* techniqueValue = Label::createWithTTF(FormatWithCommas(_playerCharactersModel->getReaction()), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    techniqueValue->setPosition(techniqueLabel->getPosition() + Vec2(130,  0));
    techniqueValue->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    techniqueValue->enableShadow();
    addChild(techniqueValue);

    Label* brakeLabel = Label::createWithTTF("忍耐 ", FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    brakeLabel->setPosition(image->getPosition() + Vec2(image->getContentSize().width / 2 + 15, -30 - 10));
    brakeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    brakeLabel->enableShadow();
    addChild(brakeLabel);

    Label* brakeValue = Label::createWithTTF(FormatWithCommas(_playerCharactersModel->getDecision()), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    brakeValue->setPosition(brakeLabel->getPosition() + Vec2(130,  0));
    brakeValue->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    brakeValue->enableShadow();
    addChild(brakeValue);
}

void GarageCharacterPopupLayer::showButtons(const CharacterMenuButtonCallback &callback)
{
    Size winSize = Director::getInstance()->getWinSize();

    auto menu = MenuPriority::create();
    menu->setPosition(Vec2(winSize.width / 2, winSize.height / 2 - 50));
    menu->setTag(TAG_SPRITE::MENU);
    addChild(menu);

    // 合成
    MenuItemSprite* compositionButton = makeMenuItem("garage_popup_button.png", callback);
    compositionButton->setPosition(Point(-91, 84));
    compositionButton->setTag(BUTTON_INDEX::COMPOSITION);
    menu->addChild(compositionButton);

    Label* compositionLabel = Label::createWithTTF("合成", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    compositionLabel->setPosition(Vec2(compositionButton->getNormalImage()->getContentSize().width / 2, compositionButton->getNormalImage()->getContentSize().height / 2 - 12.5));
    compositionLabel->enableShadow();
    compositionButton->addChild(compositionLabel);
    if (isLevelMax()) {
        compositionButton->setEnabled(false);
    }

    // カスタマイズ
    MenuItemSprite* customizeButton = makeMenuItem("garage_popup_button.png", callback);
    customizeButton->setPosition(Point(71, 84));
    customizeButton->setTag(BUTTON_INDEX::CUSTOMIZE);
    menu->addChild(customizeButton);

    Label* customizeLabel = Label::createWithTTF("カスタム", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    customizeLabel->setPosition(Vec2(customizeButton->getNormalImage()->getContentSize().width / 2, customizeButton->getNormalImage()->getContentSize().height / 2 - 12.5));
    customizeLabel->enableShadow();
    customizeButton->addChild(customizeLabel);

    // 売却
    MenuItemSprite* sellButton = makeMenuItem("garage_popup_button.png", callback);
    sellButton->setTag(BUTTON_INDEX::SELL);
    sellButton->setPosition(Point(-91, 0));
    if (isLocked() || isTeamCharacter()) {
        sellButton->setEnabled(false);
    }
    menu->addChild(sellButton);

    Label* sellLabel = Label::createWithTTF("売却", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    sellLabel->enableShadow();
    sellLabel->setPosition(Vec2(sellButton->getNormalImage()->getContentSize().width / 2, sellButton->getNormalImage()->getContentSize().height / 2 - 12.5));
    sellButton->addChild(sellLabel);

    // お気に入り
    MenuItemSprite* favoriteButton = makeMenuItem("garage_popup_button.png", CC_CALLBACK_1(GarageCharacterPopupLayer::lockButtonCallback, this));
    favoriteButton->setPosition(Point(71, 0));
    favoriteButton->setTag(BUTTON_INDEX::FAVORITE);
    menu->addChild(favoriteButton);

    Label* favoriteLabel = Label::createWithTTF("お気に入り", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    if (isLocked()) {
        favoriteLabel->setString("解除");
    }
    favoriteLabel->setPosition(Vec2(favoriteButton->getNormalImage()->getContentSize().width / 2, favoriteButton->getNormalImage()->getContentSize().height / 2 - 12.5));
    favoriteLabel->enableShadow();
    favoriteLabel->setTag(TAG_SPRITE::FAVORITE_LABEL);
    favoriteButton->addChild(favoriteLabel);

    // 詳細
    MenuItemSprite* detailButton = makeMenuItem("garage_popup_button.png", callback);
    detailButton->setPosition(Point(-91, -84));
    detailButton->setTag(BUTTON_INDEX::DETAIL);
    menu->addChild(detailButton);

    Label* detailLabel = Label::createWithTTF("詳細", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    detailLabel->setPosition(Vec2(detailButton->getNormalImage()->getContentSize().width / 2, detailButton->getNormalImage()->getContentSize().height / 2 - 12.5));
    detailLabel->enableShadow();
    detailButton->addChild(detailLabel);

    // 覚醒
    _awakeButton = makeMenuItem("garage_popup_button.png", CC_CALLBACK_1(GarageCharacterPopupLayer::onTapAwakeButton, this));
    _awakeButton->setPosition(Point(71, -84));

    Label* arousalLabel = Label::createWithTTF("覚醒", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    arousalLabel->setPosition(Vec2(_awakeButton->getNormalImage()->getContentSize().width / 2, _awakeButton->getNormalImage()->getContentSize().height / 2 - 12.5));
    arousalLabel->enableShadow();
    _awakeButton->addChild(arousalLabel);
    _awakeButton->setEnabled(isCanAwake());
    menu->addChild(_awakeButton);

    // キャンセル
    auto cancelBtn = makeMenuItem("popupBack.png", CC_CALLBACK_1(GarageCharacterPopupLayer::cancelButtonCallback, this));
    cancelBtn->setPosition(Point(0, -static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BACKGROUND))->getContentSize().height / 2 + 30 + cancelBtn->getContentSize().height / 2));
    cancelBtn->setTag(TAG_SPRITE::CLOSE_MENU_BUTTON);
    auto canselMenu = MenuPriority::create();
    canselMenu->addChild(cancelBtn);
    canselMenu->setTag(TAG_SPRITE::CLOSE_MENU);
    addChild(canselMenu);
}

bool GarageCharacterPopupLayer::isLevelMax()
{
    return _playerCharactersModel->getLevel() >= 99;
}

bool GarageCharacterPopupLayer::isTeamCharacter()
{
    std::vector<int>pcIdsUsed = PLAYERCONTROLLER->getPlayerCharacterIdsUsed();
    std::vector<int>::iterator it = find(pcIdsUsed.begin(),
                                         pcIdsUsed.end(),
                                         _playerCharactersModel->getID());
    return it != pcIdsUsed.end();
}

bool GarageCharacterPopupLayer::isLocked()
{
    return _playerCharactersModel->getLock() == 1;
}

void GarageCharacterPopupLayer::lockButtonCallback(cocos2d::Ref* sender)
{
    bool lock = true;
    if (static_cast<Label*>(static_cast<MenuItemSprite*>(sender)->getChildByTag(TAG_SPRITE::FAVORITE_LABEL))->getString() == "解除") {
        lock = false;
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    setEnableAllButton(false);
    progress->onStart();
    api->lockCharacter(_playerCharactersModel->getID(),
                       lock,
                       CC_CALLBACK_1(GarageCharacterPopupLayer::onResponseLockCharacter, this));
}

void GarageCharacterPopupLayer::onResponseLockCharacter(Json* response)
{
    bool flgLock = true;
    if (_playerCharactersModel->getLock() == 1) {
        flgLock = false;
    }
    PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[_playerCharactersModel->getID()]->setLock(flgLock);
    _playerCharactersModel->setLock(flgLock);
    if (_awakeButton != nullptr) {
        _awakeButton->setEnabled(isCanAwake());
    }
    setEnableAllButton(true);
    toggleFavoriteLabel();
    toggleSellButton();
    _lockCharacterResponseCallback();
}

void GarageCharacterPopupLayer::cancelButtonCallback(Ref* sender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    removeFromParent();
}

void GarageCharacterPopupLayer::toggleSellButton()
{
    MenuItemSprite* menuItem = static_cast<MenuItemSprite*>(getChildByTag(TAG_SPRITE::MENU)->getChildByTag(BUTTON_INDEX::SELL));

    if (isLocked() || isTeamCharacter()) {
        menuItem->setEnabled(false);
    } else {
        menuItem->setEnabled(true);
    }
}

void GarageCharacterPopupLayer::toggleFavoriteLabel()
{
    Label* label = static_cast<Label*>(static_cast<MenuItemSprite*>(getChildByTag(TAG_SPRITE::MENU)->getChildByTag(BUTTON_INDEX::FAVORITE))->getChildByTag(TAG_SPRITE::FAVORITE_LABEL));

    if (label->getString() == "解除") {
        label->setString("お気に入り");
    } else {
        label->setString("解除");
    }
}

void GarageCharacterPopupLayer::setEnableAllButton(bool enable)
{
    Node* menu = getChildByTag(TAG_SPRITE::MENU);
    static_cast<MenuItemSprite*>(menu->getChildByTag(BUTTON_INDEX::COMPOSITION))->setEnabled(enable);
    static_cast<MenuItemSprite*>(menu->getChildByTag(BUTTON_INDEX::CUSTOMIZE))->setEnabled(enable);
    static_cast<MenuItemSprite*>(menu->getChildByTag(BUTTON_INDEX::SELL))->setEnabled(enable);
    static_cast<MenuItemSprite*>(menu->getChildByTag(BUTTON_INDEX::FAVORITE))->setEnabled(enable);
    static_cast<MenuItemSprite*>(menu->getChildByTag(BUTTON_INDEX::DETAIL))->setEnabled(enable);
    static_cast<MenuItemSprite*>(getChildByTag(TAG_SPRITE::CLOSE_MENU)->getChildByTag(TAG_SPRITE::CLOSE_MENU_BUTTON))->cocos2d::MenuItem::setEnabled(enable);
}

const bool GarageCharacterPopupLayer::isCanAwake()
{
    std::shared_ptr<CharacterModel>characterModel(CharacterModel::awakeCharacterFind(_playerCharactersModel->getCharactersId()));
    if (!characterModel ||
        _playerCharactersModel->getLevel() < characterModel->getAwakeLevel() ||
        _playerCharactersModel->getDearValue() < 250 ||
        _playerCharactersModel->getLock() == 1) {
        return false;
    }
    return true;
}

void GarageCharacterPopupLayer::onTapAwakeButton(Ref* pSender)
{
    _awakeCharacterResponseCallback(_playerCharactersModel->getID());
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
}