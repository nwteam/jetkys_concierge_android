#ifndef __syanago__GarageCharacterPopupLayer__
#define __syanago__GarageCharacterPopupLayer__

#include <functional>
#include "create_func.h"
#include "jCommon.h"
#include "LayerPriority.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"
#include "PlayerCharactersModel.h"

using namespace SyanagoAPI;

class GarageCharacterPopupLayer : public create_func<GarageCharacterPopupLayer>, public LayerPriority
{
    
public:
    typedef std::function<void(Ref*)> CharacterMenuButtonCallback;
    typedef std::function<void()> LockCharacterResponseCallback;
    typedef std::function<void(const int playerCharacterId)> AwakeCharacterResponseCallback;
    GarageCharacterPopupLayer();
    ~GarageCharacterPopupLayer();
    bool init(const CharacterMenuButtonCallback& buttonCallback, const LockCharacterResponseCallback& responseCallback, const AwakeCharacterResponseCallback& awakeCharacterResposeCallback, int playerCharacterIndex);
    using create_func::create;
    
    enum BUTTON_INDEX {
        COMPOSITION = 0,
        CUSTOMIZE,
        SELL,
        FAVORITE,
        DETAIL,
        AWAKE,
    };
    
private:
    void showBackground();
    void showCharacter();
    void showCharacterStatus();
    void showButtons(const CharacterMenuButtonCallback& callback);
    bool isLevelMax();
    bool isTeamCharacter();
    bool isLocked();
    void lockButtonCallback(Ref* sender);
    void onResponseLockCharacter(Json* response);
    void cancelButtonCallback(Ref* sender);
    void toggleSellButton();
    void toggleFavoriteLabel();
    void setEnableAllButton(bool enable);
    
    void onTapAwakeButton(Ref* pSender);
    const bool isCanAwake();
    
    enum TAG_SPRITE {
        BACKGROUND = 100,
        CHARACTER_IMAGE,
        MENU,
        FAVORITE_LABEL,
        CLOSE_MENU,
        CLOSE_MENU_BUTTON
    };
    
    LockCharacterResponseCallback _lockCharacterResponseCallback;
    AwakeCharacterResponseCallback _awakeCharacterResponseCallback;
    RequestAPI* api;
    DefaultProgress* progress;
    
    MenuItemSprite* _awakeButton;
    
    PlayerCharactersModel* _playerCharactersModel;
};

#endif /* defined(__syanago__GarageCharacterPopupLayer__) */
