#include "GaragePartPopupLayer.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include "SoundHelper.h"
#include "PartModel.h"

bool GaragePartPopupLayer::init(const PartsMenuButtonCallback &callback, int playerPartsIndex)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _playerPartIndex = playerPartsIndex;

    Size winSize = Director::getInstance()->getWinSize();
    // add bgr dialog
    auto bg = makeSprite("garage_popup_part_bg.png");
    addChild(bg);
    bg->setPosition(Vec2(winSize.width / 2, winSize.height / 2));

    std::vector<int>partIdsUsed = PLAYERCONTROLLER->getPlayerPartIdsUsed();
    std::vector<int>::iterator it;

    int isNew = 2;          // variable to check 装備 is used or not
    it = find (partIdsUsed.begin(), partIdsUsed.end(), PLAYERCONTROLLER->_playerPartsSorted.at(_playerPartIndex)->getID());
    if (it != partIdsUsed.end())
        isNew = 1;
    else
        isNew = 0;          // 0: no in use,1: in use,2: new

    std::string name = PartModel::find(PLAYERCONTROLLER->_playerPartsSorted.at(_playerPartIndex)->getPartsId())->getName();

    int rarity = PartModel::find(PLAYERCONTROLLER->_playerPartsSorted.at(_playerPartIndex)->getPartsId())->getRarity();

    auto nameLabel = Label::createWithTTF(name, FONT_NAME_2, 30, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(nameLabel);
    nameLabel->setPosition(bg->getPosition() + Vec2(0, bg->getContentSize().height / 2 - 30));
    nameLabel->setAnchorPoint(Vec2(0.5, 1));
    nameLabel->setColor(COLOR_YELLOW);
    nameLabel->enableShadow();

    auto image = makeSprite(StringUtils::format("%d_pag.png", PLAYERCONTROLLER->_playerPartsSorted.at(_playerPartIndex)->getPartsId()).c_str());
    addChild(image);
    image->setPosition(nameLabel->getPosition() + Vec2(0, -nameLabel->getContentSize().height - 10 - image->getContentSize().height / 2 + 20));

    // rarity
    auto node = Node::create();
    addChild(node);
    node->setPosition(image->getPosition() + Vec2(0, -image->getContentSize().height / 2));

    for (int i = 0; i < rarity; i++) {
        auto start = makeSprite("rarity.png");
        node->addChild(start);
        if (rarity % 2 == 1) {
            start->setPosition(Vec2(start->getContentSize().width * (i - rarity / 2), 0));
        } else {
            int j = i - rarity / 2;
            start->setPosition(Vec2(start->getContentSize().width * (0.5f + j), 0));
        }
    }

    auto menu2 = MenuPriority::create();
    addChild(menu2);
    menu2->setPosition(Vec2(winSize.width / 2, winSize.height / 2 - 90));

    // haianhnc20141110
    auto button = makeMenuItem("garage_popup_button.png", callback);
    if (isNew != 1) {
        button->setTag(GaragePartPopupLayer::BUTTON_INDEX::SELL);
    } else {
        button->setEnabled(false);
    }

    auto label = Label::createWithTTF("売却", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    button->addChild(label);
    label->setPosition(Vec2(button->getNormalImage()->getContentSize().width / 2, button->getNormalImage()->getContentSize().height / 2 - 16));
    label->enableShadow();
    menu2->addChild(button);

    button = makeMenuItem("garage_popup_button.png", callback);
    menu2->addChild(button);
    button->setTag(GaragePartPopupLayer::BUTTON_INDEX::DETAIL);

    label = Label::createWithTTF("詳細", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    button->addChild(label);
    label->setPosition(Vec2(button->getNormalImage()->getContentSize().width / 2, button->getNormalImage()->getContentSize().height / 2 - 16));
    label->enableShadow();

    button = makeMenuItem("te_popup_close_button.png", CC_CALLBACK_1(GaragePartPopupLayer::cancelButtonCallback, this));
    menu2->addChild(button);

    menu2->alignItemsVerticallyWithPadding(16);

    return true;
}

void GaragePartPopupLayer::cancelButtonCallback(Ref* sender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    removeFromParent();
}