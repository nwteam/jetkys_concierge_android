#include "GaragePartSortLayer.h"
#include "FontDefines.h"

bool GaragePartSortLayer::init(const SortButtonCallback &sortButtonCallback, SortPlayerParts::KEY nowPartsSortKey)
{
    if (!LayerPriority::init()) {
        return false;
    }

    showBackground();
    showButtons(sortButtonCallback, nowPartsSortKey);

    return true;
}

void GaragePartSortLayer::showBackground()
{
    Size winSize = Director::getInstance()->getWinSize();

    LayerColor* bg = LayerColor::create(Color4B(0, 0, 0, 180));
    bg->setPosition(Vec2(winSize.width / 2 - bg->getContentSize().width / 2, winSize.height / 2 - bg->getContentSize().height / 2));
    addChild(bg);

    Sprite* sortBg = Sprite::create("sort_parts_base.png");
    sortBg->setPosition(Point(winSize.width / 2, winSize.height / 2));
    sortBg->setTag(TAG_SPRITE::SORT_BG);
    addChild(sortBg);

    Label* sorttext = Label::createWithTTF("表示順を選んでください", FONT_NAME_2, 32);
    sorttext->setPosition(Point(sortBg->getContentSize().width / 2, sortBg->getContentSize().height - 30 - sorttext->getContentSize().height / 2));
    sorttext->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    sortBg->addChild(sorttext);
}

void GaragePartSortLayer::showButtons(const SortButtonCallback &sortButtonCallback, SortPlayerParts::KEY nowPartsSortKey)
{
    Size winSize = Director::getInstance()->getWinSize();

    MenuPriority* menu = MenuPriority::create();
    addChild(menu);
    menu->setPositionY(winSize.height / 2 + 15);

    for (int i = 0; i < SortPlayerParts::SORT_LABELS.size(); i++) {
        std::string fileName = (i == nowPartsSortKey) ? "sort_yellow_button.png" : "sort_pink_button.png";
        auto button = makeMenuItem(fileName,
                                   SortPlayerParts::SORT_LABELS.at(i),
                                   28,
                                   sortButtonCallback);
        button->setTag(i);
        button->setPosition(Vec2(-button->getContentSize().width / 2 - 20,
                                 button->getContentSize().height + 20) + Vec2((button->getContentSize().width + 20) * (i % 2), (-button->getContentSize().height - 20) * (i / 2))
                            );
        menu->addChild(button);
    }

    // キャンセル
    auto cancelBtn = makeMenuItem("popupBack.png", CC_CALLBACK_1(GaragePartSortLayer::cancelButtonCallback, this));
    cancelBtn->setPosition(Point(0, -static_cast<Sprite*>(getChildByTag(TAG_SPRITE::SORT_BG))->getContentSize().height / 2 + 30 + cancelBtn->getContentSize().height / 2));
    auto canselMenu = MenuPriority::create();
    canselMenu->addChild(cancelBtn);
    addChild(canselMenu);
}

void GaragePartSortLayer::cancelButtonCallback(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    removeFromParent();
}
