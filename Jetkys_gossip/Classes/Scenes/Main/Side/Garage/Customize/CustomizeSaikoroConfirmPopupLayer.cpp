#include "CustomizeSaikoroConfirmPopupLayer.h"
#include "jCommon.h"
#include "FontDefines.h"
#include "SystemSettingModel.h"

CustomizeSaikoroConfirmPopupLayer* CustomizeSaikoroConfirmPopupLayer::createWithSaikoro(int saikoro)
{
    auto layer = new CustomizeSaikoroConfirmPopupLayer();
    layer->initWithSaikoro(saikoro);
    layer->autorelease();

    return layer;
}

bool CustomizeSaikoroConfirmPopupLayer::initWithSaikoro(int saikoro)
{
    if (!LayerPriority::initWithPriority(kPriorityCustomizeSaikoroConfirmPopUpLayer)) {
        return false;
    }
    _saikoro = saikoro;
    _delegate = nullptr;

    Size winSize = Director::getInstance()->getWinSize();
    // add bgr dialog
    auto bg = makeSprite("customize_popup_bg.png");
    addChild(bg);
    bg->setPosition(Vec2(winSize.width / 2, winSize.height / 2));

    auto title = Label::createWithTTF("この目を刻印しますか？", FONT_NAME_2, 34, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(title);
    title->enableShadow();
    title->setAnchorPoint(Vec2(0.5, 0.5));
    title->setPosition(bg->getPosition() + Vec2(0, bg->getContentSize().height / 2 - 30 - 17));
    fixFontPosition(title);

    auto saikoroImg = makeSprite(StringUtils::format("customize_popup_saikoro_%d.png", _saikoro).c_str());
    addChild(saikoroImg);
    saikoroImg->setPosition(title->getPosition() + Vec2(0, -title->getPositionY() + bg->getPositionY() + 10));


    _message = Label::createWithTTF("", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(_message);
    _message->setColor(COLOR_YELLOW);
    _message->enableShadow();
    _message->setAnchorPoint(Vec2(0.5, 1));
    _message->setPosition(saikoroImg->getPosition() + Vec2(0, -saikoroImg->getContentSize().height / 2 - 30));
    fixFontPosition(_message);


    // add button close
    auto menu = MenuPriority::createWithPriority(kPriorityCustomizeSaikoroConfirmPopUpMenu);
    addChild(menu);

    auto button = makeMenuItem("customize_popup_button.png", "はい", 24, CC_CALLBACK_1(CustomizeSaikoroConfirmPopupLayer::callBackbtn, this));
    menu->addChild(button);
    button->setTag(CCS_BT_YES);

    button = makeMenuItem("customize_popup_button.png", "いいえ", 24, CC_CALLBACK_1(CustomizeSaikoroConfirmPopupLayer::callBackbtn, this));
    menu->addChild(button);
    button->setTag(CCS_BT_NO);

    menu->alignItemsHorizontallyWithPadding(50);
    menu->setPosition(Vec2(winSize.width / 2, bg->getPositionY() - bg->getContentSize().height / 2 + 30 + button->getNormalImage()->getContentSize().height / 2));

    return true;
}

void CustomizeSaikoroConfirmPopupLayer::callBackbtn(Ref* psender)
{
    // Destroy dialog
    int aIndex = ((Node*)psender)->getTag();

    PopUpDelegate* delegate = _delegate;
    int saikoro = _saikoro;
    removeFromParent();

    // call back
    if (delegate != nullptr) {
        auto obj = __String::createWithFormat("%d", saikoro);
        delegate->btCallback(aIndex, obj);
    }
}

void CustomizeSaikoroConfirmPopupLayer::setMessage(bool hasCharge)
{
    // check has charge
//    セットする時
//    初回：※注意※　元に戻すにはトークンが必要になります
//    ２回目以降：※注意※　刻印をするには３トークン消費します
    std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
    std::string messStr;
    if (hasCharge) {
        messStr = StringUtils::format("※注意※\n刻印をするには%sトークン消費します", systemSettingModel->getTokenOfDiceCustom());
    } else {
        messStr = "※注意※\n元に戻すにはトークンが必要になります";
    }
    _message->setString(messStr);
}
