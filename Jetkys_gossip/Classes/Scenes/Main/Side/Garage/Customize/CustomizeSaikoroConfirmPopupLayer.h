#ifndef __syanago__CustomizeSaikoroConfirmPopupLayer__
#define __syanago__CustomizeSaikoroConfirmPopupLayer__

#include "LayerPriority.h"
#include "PopUpDelegate.h"

#define kPriorityCustomizeSaikoroConfirmPopUpLayer -650
#define kPriorityCustomizeSaikoroConfirmPopUpMenu kPriorityCustomizeSaikoroConfirmPopUpLayer - 2

enum CustomizeConfirmSaikoro {
    CCS_BT_YES = 20,
    CCS_BT_NO,
};

class CustomizeSaikoroConfirmPopupLayer : public LayerPriority {
public:
    
    PopUpDelegate *_delegate;
    
    static CustomizeSaikoroConfirmPopupLayer *createWithSaikoro(int saikoro);
    bool initWithSaikoro(int saikoro);
    
    void callBackbtn(Ref *psender);
    
    void btCallback(int btIndex, void *object);
    void setMessage(bool hasCharge);
    
private:
    int _saikoro;
    Label *_message;
};

#endif /* defined(__syanago__CustomizeSaikoroConfirmPopupLayer__) */
