#ifndef __syanago__CustomizePartSelectLayer__
#define __syanago__CustomizePartSelectLayer__

#include "SyanagoBaseLayer.h"
#include "CharacterSelectSortLayer.h"
#include "GaragePartSortLayer.h"

//class GarageMainLayer;

class CustomizePartSelectLayer : public SyanagoBaseLayer {
public:
    CustomizePartSelectLayer();
    virtual ~CustomizePartSelectLayer();
    
    bool init();
    CREATE_FUNC(CustomizePartSelectLayer);
    
    //GarageMainLayer *_mainLayer;
    
    void btMenuItemCallback(cocos2d::Ref *pSender);
    void btItemSelectedCallback(Ref *pSender);

    void showDatas();
    
    void scrollViewDidScroll(ScrollView* view);
    void scrollViewDidZoom(ScrollView* view);
    
    void savepcIDSelectedTemp();
    int indexOfpcIDSelectedTemp(int pcID);

    CC_SYNTHESIZE(int, _pcSelectedIndex, PCSelectecedIndex) //save index of player character selected
    std::vector<int> _pcIDSelectedTemp;

private:
    void partsSortButtonCallback(Ref* pSender);
    
    enum TAG_LAYER {
        PARTS_SORT = 0
    };
};

#endif /* defined(__syanago__CustomizePartSelectLayer__) */
