#ifndef __syanago__CustomizeSaikoroPopupLayer__
#define __syanago__CustomizeSaikoroPopupLayer__

#include "LayerPriority.h"
#include "PopUpDelegate.h"

#define kPriorityCustomizeSaikoroPopUpLayer -600
#define kPriorityCustomizeSaikoroPopUpMenu kPriorityCustomizeSaikoroPopUpLayer - 2

enum CustomizeSaikoro {
    CS_SAIKORO1 = 1,
    CS_SAIKORO2,
    CS_SAIKORO3,
    CS_SAIKORO4,
    CS_SAIKORO5,
    CS_SAIKORO6,
    CS_CANCEl = 10
};

class CustomizeSaikoroPopupLayer : public LayerPriority, public PopUpDelegate {
public:
    
    PopUpDelegate *_delegate;
    
    bool init();
    CREATE_FUNC(CustomizeSaikoroPopupLayer);
    
    void callBackbtn(Ref *psender);
    
    void btCallback(int btIndex, void *object);
    
private:
    int _selectedIndex;
};

#endif /* defined(__syanago__CustomizeSaikoroPopupLayer__) */
