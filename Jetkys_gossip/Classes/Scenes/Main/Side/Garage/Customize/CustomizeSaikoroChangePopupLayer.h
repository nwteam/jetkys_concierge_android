#ifndef __syanago__CustomizeSaikoroChangePopupLayer__
#define __syanago__CustomizeSaikoroChangePopupLayer__

#include "DialogDefine.h"
#include "LayerPriority.h"
#include "PopUpDelegate.h"

enum CustomizeSaikoroChangePopupButtonIndex {
    BT_CSCP_CLOSE = 11,
    BT_CSCP_CHANGE,
    BT_CSCP_DELETE
};

class CustomizeSaikoroChangePopupLayer : public LayerPriority
{
    
public:
    PopUpDelegate *_delegate;
    
    
    bool init();
    CREATE_FUNC(CustomizeSaikoroChangePopupLayer);
    
    void callBackbtn(Ref *pSender);
    
    CC_SYNTHESIZE(int, _playerPartIndex, PlayerPartIndex)
};

#endif /* defined(__syanago__CustomizeSaikoroChangePopupLayer__) */
