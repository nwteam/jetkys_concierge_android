#include "CustomizeSaikoroChangePopupLayer.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include "jCommon.h"

bool CustomizeSaikoroChangePopupLayer::init()
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;

    Size winSize = Director::getInstance()->getWinSize();
    // add bgr dialog
    auto bg = makeSprite("customize_popup_confirm_bg.png");
    addChild(bg);
    bg->setPosition(Vec2(winSize.width / 2, winSize.height / 2));

    auto title = Label::createWithTTF("サイコロに目を刻印しますか？", FONT_NAME_2, 34, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(title);
    title->enableShadow();
    title->setAnchorPoint(Vec2(0.5, 1));
    title->setPosition(bg->getPosition() + Vec2(0, bg->getContentSize().height / 2 - 30));
    fixFontPosition(title);


    auto menu2 = MenuPriority::create();
    addChild(menu2);
    menu2->setPosition(Vec2(winSize.width / 2, winSize.height / 2 - 45));

    auto button = makeMenuItem("customize_popup_button_ping.png", "刻印する", 24, CC_CALLBACK_1(CustomizeSaikoroChangePopupLayer::callBackbtn, this));
    menu2->addChild(button);
    button->setTag(BT_CSCP_CHANGE);



    button = makeMenuItem("customize_popup_button_ping.png", "削除", 24, CC_CALLBACK_1(CustomizeSaikoroChangePopupLayer::callBackbtn, this));
    menu2->addChild(button);
    button->setTag(BT_CSCP_DELETE);


    button = makeMenuItem("customize_popup_button.png", "閉じる", 24, CC_CALLBACK_1(CustomizeSaikoroChangePopupLayer::callBackbtn, this));
    menu2->addChild(button);
    button->setTag(BT_CSCP_CLOSE);

    menu2->alignItemsVerticallyWithPadding(20);

    return true;
}

void CustomizeSaikoroChangePopupLayer::callBackbtn(Ref* psender)
{
    // Destroy dialog
    int aIndex = ((Node*)psender)->getTag();
    PopUpDelegate* delegate = _delegate;
    removeFromParent();

    // call back
    if (delegate != nullptr) {
        // auto obj = __String::createWithFormat("%d", _playerPartIndex);
        delegate->btCallback(aIndex, nullptr);
    }
}


