#include "CustomizeLayer.h"
#include "GarageMainLayer.h"
#include "CustomizeSaikoroPopupLayer.h"
#include "CustomizeSaikoroChangePopupLayer.h"
#include "CustomizeSaikoroDeleteConfirmPopupLayer.h"
#include "CustomizeSaikoroConfirmPopupLayer.h"
#include "StringDefines.h"
#include "ArtLtvMeasurement.h"
#include "MeasurementInformation.h"
#include "CharacterModel.h"
#include "BodyTypeModel.h"
#include "PartModel.h"
#include "LevelModel.h"
#include "SystemSettingModel.h"
#include "Native.h"

std::vector<int>CustomizeLayer::_saikoros = std::vector<int>();
std::vector<int>CustomizeLayer::_partIDs = std::vector<int>();  // (2,-1)
int CustomizeLayer::_partIndex = -1; // index of element in _partIDs: 0 -> 1
int CustomizeLayer::_pcIndex = -1;

#define TAG_CUSTOMIZE_SAIKORO_DEL 1234
#define TAG_CUSTOMIZE_SAIKORO_DEL_SUCCESS 1235
#define TAG_CUSTOMIZE_SAIKORO_DEL_FAIL 1236

CustomizeLayer::CustomizeLayer():
    _viewMasterCharacterId(0), _progress(nullptr), _request(nullptr)
{
    _title = "カスタム";
    _menuSaikoro = nullptr;
    _saikoroNode = nullptr;
    _partNode = nullptr;
    _saikoroSelectedIndex = -1;
    _flagChangeSaikoro = 0;

    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}

CustomizeLayer::~CustomizeLayer()
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_cus.plist", _viewMasterCharacterId).c_str());
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}


bool CustomizeLayer::init(int pcIndex)
{
    if (!SyanagoBaseLayer::init()) {
        return false;
    }

    if (_pcIndex < 0) {
        _pcIndex = pcIndex;
    }

    showCharacterInformations();
    showPartsOfSyanago();
    showSaikoro();

    return true;
}

void CustomizeLayer::showCharacterInformations()
{
    Size winSize = Director::getInstance()->getWinSize();

    PlayerCharactersModel* playerCharactersModel = PLAYERCONTROLLER->_playerCharactersSorted.at(_pcIndex);

    int characteID = playerCharactersModel->getCharactersId();
    int level = playerCharactersModel->getLevel();

    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(characteID));
    std::string name = characterModel->getCarName();

    int exercise = playerCharactersModel->getExercise();
    int reaction = playerCharactersModel->getReaction();
    int decision = playerCharactersModel->getDecision();
    int rarity = characterModel->getRarity();
    int body_type = characterModel->getBodyType();
    std::shared_ptr<BodyTypeModel>bodyTypeModel(BodyTypeModel::find(body_type));
    std::string bodyStr = bodyTypeModel->getName();

    int exercisePlus = 0;
    int reactionPlus = 0;
    int decisionPlus = 0;

    int ppID1 = playerCharactersModel->getPartsSlot1();
    int cpIndex1 =  ppID1 > 0 ? PLAYERCONTROLLER->findIndexOfPlayerPartSortedId(ppID1) : -1;
    int ppID2 = playerCharactersModel->getPartsSlot2();
    int cpIndex2 = ppID2 > 0 ? PLAYERCONTROLLER->findIndexOfPlayerPartSortedId(ppID2) : -1;

    int aPartID1 = cpIndex1 >= 0 ? PLAYERCONTROLLER->_playerPartsSorted.at(cpIndex1)->getPartsId() : -1;
    int aPartID2 = cpIndex2 >= 0 ? PLAYERCONTROLLER->_playerPartsSorted.at(cpIndex2)->getPartsId() : -1;

    // initialize at first
    if (_partIDs.size() == 0) {
        _partIDs.push_back(cpIndex1);
        _partIDs.push_back(cpIndex2);
    }

    for (int i = 0; i < _partIDs.size(); i++) {
        int aPartID;
        if (i == 0) {
            aPartID = aPartID1;
        } else if (i == 1) {
            aPartID = aPartID2;
        }


        if (aPartID > 0) {
            std::shared_ptr<PartModel>partsModel(PartModel::find(aPartID));
            exercise -= partsModel->getExercise();
            reaction -= partsModel->getReaction();
            decision -= partsModel->getDecision();
        }


        if (_partIDs.at(i) >= 0) {
            int ppID = PLAYERCONTROLLER->_playerPartsSorted.at(_partIDs.at(i))->getPartsId();
            std::shared_ptr<PartModel>partsModel(PartModel::find(ppID));
            exercisePlus += partsModel->getExercise();
            reactionPlus += partsModel->getReaction();
            decisionPlus += partsModel->getDecision();
        }
    }

    auto infoLayer = Node::create();
    addChild(infoLayer);

    _information_board = makeSprite("customize_status_bg.png");
    infoLayer->addChild(_information_board);
    _information_board->setPosition(Vec2(winSize.width / 2, _titleBg->getPositionY() - _titleBg->getContentSize().height / 2 - 30 - _information_board->getContentSize().height / 2));

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_cus.plist", characteID).c_str());
    auto image = makeSprite(StringUtils::format("%d_cus.png", characteID).c_str());
    infoLayer->addChild(image);
    image->setPosition(_information_board->getPosition() + Vec2(-_information_board->getContentSize().width / 2 - 1 + image->getContentSize().width / 2 * image->getScaleX(),
                                                                -_information_board->getContentSize().height / 2  + image->getContentSize().height / 2 * image->getScaleY()));
    _viewMasterCharacterId = characteID;


    // level, exp
    auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %d", level), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::TOP);
    infoLayer->addChild(levelLabel, 4);
    levelLabel->setPosition(_information_board->getPosition() + Vec2(-80, _information_board->getContentSize().height / 2 - 15));
    levelLabel->setAnchorPoint(Vec2(0, 1));
    levelLabel->enableShadow();
    fixFontPosition(levelLabel);

    // name
    auto nameLabel = Label::createWithTTF(name, FONT_NAME_2, 23, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::TOP);
    infoLayer->addChild(nameLabel);
    nameLabel->setPosition(levelLabel->getPosition() + Vec2(0, -levelLabel->getContentSize().height * 0.5f - 10));
    nameLabel->setAnchorPoint(Vec2(0, 1));
    nameLabel->enableShadow();
    fixFontPosition(nameLabel);

    // status
    auto starNode = createRarityNode(rarity);
    infoLayer->addChild(starNode);
    starNode->setScale(18.0f / 24.0f);
    starNode->setPosition(nameLabel->getPosition() + Vec2(starNode->getContentSize().width / 2 * starNode->getScale(), -nameLabel->getContentSize().height * 0.5f - 20));

    auto bodyType = Label::createWithTTF(StringUtils::format("ボディータイプ: %s", bodyStr.c_str()), FONT_NAME_2, 18, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::TOP);
    infoLayer->addChild(bodyType);
    bodyType->enableShadow();
    bodyType->setPosition(Vec2(nameLabel->getPositionX(), starNode->getPositionY() - 20));
    bodyType->setAnchorPoint(Vec2(0, 1));
    fixFontPosition(bodyType);


    // status: exercise, reaction, decision
    _exerciceLabel = Label::createWithTTF(FormatWithCommas(exercise), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::TOP);
    infoLayer->addChild(_exerciceLabel);
    _exerciceLabel->enableShadow();
    _exerciceLabel->setPosition(_information_board->getPosition() + Vec2(35, -20));
    _exerciceLabel->setAnchorPoint(Vec2(0, 1));
    fixFontPosition(_exerciceLabel);

    _exercisePlusLabel = Label::createWithTTF(StringUtils::format("(%s%d)", exercisePlus >= 0 ? "+" : "-", abs(exercisePlus)), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    infoLayer->addChild(_exercisePlusLabel);
    _exercisePlusLabel->setPosition(_exerciceLabel->getPosition() + Vec2(160, 0));
    _exercisePlusLabel->setAnchorPoint(Vec2(0, 1));
    _exercisePlusLabel->setColor(COLOR_PINK);
    _exercisePlusLabel->enableShadow();
    fixFontPosition(_exercisePlusLabel);

    _reactionLabel = Label::createWithTTF(FormatWithCommas(reaction), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::TOP);
    infoLayer->addChild(_reactionLabel);
    _reactionLabel->enableShadow();
    _reactionLabel->setPosition(_information_board->getPosition() + Vec2(35, -50));
    _reactionLabel->setAnchorPoint(Vec2(0, 1));
    fixFontPosition(_reactionLabel);

    _reactionPlusLabel = Label::createWithTTF(StringUtils::format("(%s%d)", reactionPlus >= 0 ? "+" : "-", abs(reactionPlus)), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    infoLayer->addChild(_reactionPlusLabel);
    _reactionPlusLabel->setPosition(_reactionLabel->getPosition() + Vec2(160, 0));
    _reactionPlusLabel->setAnchorPoint(Vec2(0, 1));
    _reactionPlusLabel->setColor(COLOR_PINK);
    _reactionPlusLabel->enableShadow();
    fixFontPosition(_reactionPlusLabel);


    _decisionLabel = Label::createWithTTF(FormatWithCommas(decision), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::TOP);
    infoLayer->addChild(_decisionLabel);
    _decisionLabel->enableShadow();
    _decisionLabel->setPosition(_information_board->getPosition() + Vec2(35, -80));
    _decisionLabel->setAnchorPoint(Vec2(0, 1));
    fixFontPosition(_decisionLabel);

    _decisionPlusLabel = Label::createWithTTF(StringUtils::format("(%s%d)", decisionPlus >= 0 ? "+" : "-", abs(decisionPlus)), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    infoLayer->addChild(_decisionPlusLabel);
    _decisionPlusLabel->setPosition(_decisionLabel->getPosition() + Vec2(160, 0));
    _decisionPlusLabel->setAnchorPoint(Vec2(0, 1));
    _decisionPlusLabel->setColor(COLOR_PINK);
    _decisionPlusLabel->enableShadow();
    fixFontPosition(_decisionPlusLabel);
}

void CustomizeLayer::showPartsOfSyanago()
{
    if (_partNode) {
        _partNode->removeFromParent();
    }
    _partNode = Node::create();
    addChild(_partNode);

    Size winSize = Director::getInstance()->getWinSize();

    auto menu = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    menu->setPosition(Vec2::ZERO);
    menu->setAnchorPoint(Vec2::ZERO);
    _partNode->addChild(menu, 2);

    for (int i = 0; i < _partIDs.size(); i++) {
        auto partBg = makeSprite("customize_part_bg.png");
        partBg->setPosition(_information_board->getPosition() + Vec2(0, -_information_board->getContentSize().height / 2 - 10 - partBg->getContentSize().height * 0.5f - (partBg->getContentSize().height + 10) * i));
        _partNode->addChild(partBg);

        auto title = Label::createWithTTF(StringUtils::format("装備%d", i + 1), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::TOP);
        title->setColor(COLOR_PINK);
        title->enableShadow();
        title->setAnchorPoint(Vec2(0, 0.5));
        title->setPosition(partBg->getPosition() + Vec2(-partBg->getContentSize().width * 0.5f + 10, partBg->getContentSize().height / 2 - 20));
        _partNode->addChild(title, 1);
        fixFontPosition(title);

        auto btChange = makeMenuItem("customize_part_change_button.png", "変更", 24, CC_CALLBACK_1(CustomizeLayer::onTapChangePartsButton, this));
        btChange->setPosition(partBg->getPosition() + Vec2(partBg->getContentSize().width / 2 - 20 - btChange->getNormalImage()->getContentSize().width / 2, -partBg->getContentSize().height / 2 + 29 + btChange->getContentSize().height / 2));
        btChange->setTag(kTagIndexOfPartID + i);
        menu->addChild(btChange);

        // information
        Sprite* partImg = nullptr;
        if (_partIDs.at(i) >= 0) {
            std::shared_ptr<PartModel>partsModel(PartModel::find(PLAYERCONTROLLER->_playerPartsSorted.at(_partIDs.at(i))->getPartsId()));
            std::string name = partsModel->getName();


            partImg = makeSprite(StringUtils::format("%d_pag.png", partsModel->getID()).c_str());
            partImg->setScale(0.8);
            partImg->setPosition(partBg->getPosition() + Vec2(-partBg->getContentSize().width / 2 + 50 + 1, -partBg->getContentSize().height / 2 + 50 + 1));
            _partNode->addChild(partImg);

            auto partName = Label::createWithTTF(name, FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::TOP);
            partName->setAnchorPoint(Vec2(0, 0.5));
            partName->setPosition(title->getPosition() + Vec2(80, 0));
            partName->enableShadow();
            _partNode->addChild(partName);
            fixFontPosition(partName);

            auto descritpionLabel = Label::createWithTTF(partsModel->getRemarks(), FONT_NAME_2, 19, Size(Vec2(390, 0)), TextHAlignment::LEFT, TextVAlignment::TOP);
            descritpionLabel->setHorizontalAlignment(TextHAlignment::LEFT);
            descritpionLabel->setColor(Color3B::BLACK);

            auto mainTextScrollView = ScrollView::create();
            mainTextScrollView->setViewSize(Size(400, 80));
            mainTextScrollView->setDirection(ScrollView::Direction::VERTICAL);
            mainTextScrollView->setAnchorPoint(Point(0, 0));
            mainTextScrollView->setPosition(partImg->getPosition() + Vec2(40 + 15, 35 - partImg->getContentSize().height + 15));
            mainTextScrollView->setBounceable(false);
            mainTextScrollView->setContentOffset(Point(10 - (descritpionLabel->getContentSize().width - mainTextScrollView->getViewSize().width),
                                                       0 - (descritpionLabel->getContentSize().height - mainTextScrollView->getViewSize().height)));
            mainTextScrollView->setContainer(descritpionLabel);
            _partNode->addChild(mainTextScrollView, 1);

            auto starNode = createRarityNode(partsModel->getRarity());
            starNode->setScale(16.0f / 24.0f);
            starNode->setPosition(Vec2(partBg->getPositionX() + partBg->getContentSize().width / 2 - 10 - starNode->getContentSize().width / 2 * starNode->getScale(), title->getPositionY()));
            _partNode->addChild(starNode, 1);
        }
    }
}

void CustomizeLayer::showSaikoro()
{
    if (_saikoroNode) {
        _saikoroNode->removeFromParent();
    }
    _saikoroNode = Node::create();
    addChild(_saikoroNode);

    auto saikoroBg = makeSprite("customize_saikoro_bg.png");
    _saikoroNode->addChild(saikoroBg);
    saikoroBg->setPosition(_information_board->getPosition() + Vec2(0, -_information_board->getContentSize().height / 2 - 294 - saikoroBg->getContentSize().height / 2));

    auto title = Label::createWithTTF("サイコロ", FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::TOP);
    _saikoroNode->addChild(title, 1);
    title->setColor(COLOR_PINK);
    title->enableShadow();
    title->setAnchorPoint(Vec2(0, 0.5));
    title->setPosition(saikoroBg->getPosition() + Vec2(-saikoroBg->getContentSize().width * 0.5f + 10, saikoroBg->getContentSize().height / 2 - 20));
    fixFontPosition(title);

    PlayerCharactersModel* playerCharactersModel = PLAYERCONTROLLER->_playerCharactersSorted.at(_pcIndex);

    if (_saikoros.size() == 0) {
        _saikoros.push_back(playerCharactersModel->getDiceValue1());
        _saikoros.push_back(playerCharactersModel->getDiceValue2());
        _saikoros.push_back(playerCharactersModel->getDiceValue3());
        _saikoros.push_back(playerCharactersModel->getDiceValue4());
        _saikoros.push_back(playerCharactersModel->getDiceValue5());
        _saikoros.push_back(playerCharactersModel->getDiceValue6());
        _saikoros.push_back(playerCharactersModel->getDiceValue7());
        _saikoros.push_back(playerCharactersModel->getDiceValue8());
        _saikoros.push_back(playerCharactersModel->getDiceValue9());
    }

    Size winSize = Director::getInstance()->getWinSize();

    _menuSaikoro = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    _saikoroNode->addChild(_menuSaikoro);
    _menuSaikoro->setPosition(Vec2::ZERO);
    _menuSaikoro->setAnchorPoint(Vec2::ZERO);

    // check add_dice with level
    int level = playerCharactersModel->getLevel();
    std::shared_ptr<LevelModel>levelModel(LevelModel::find(level));
    int addDice = levelModel->getAddDice();

    for (int i = 0; i < _saikoros.size(); i++) {
        if (_saikoros.at(i) > 0 && i < 6) {
            auto saikoroImg = makeSprite(StringUtils::format("customize_saikoro_%d.png", _saikoros.at(i)).c_str());
            _saikoroNode->addChild(saikoroImg);
            saikoroImg->setScale((float)40 / saikoroImg->getContentSize().width);
            saikoroImg->setPosition(saikoroBg->getPosition() + Vec2(-saikoroBg->getContentSize().width / 2 + 31.0f + (40.0f /*saikoroImg->getContentSize().width * saikoroImg->getScale()*/ + 4) * (0.5f + i) + 7 * i, -saikoroBg->getContentSize().height / 2 + 12 + 0.5f * 40 /*saikoroImg->getContentSize().height/2  * saikoroImg->getScale()*/));
        } else {
            MenuItemSprite* saikoroImg = nullptr;

            if (_saikoros.at(i) > 0) {
                saikoroImg = makeMenuItem(StringUtils::format("customize_saikoro_%d.png", _saikoros.at(i)).c_str(), CC_CALLBACK_1(CustomizeLayer::onTapChangeDiceValueButton, this));
            } else {
                if (i > 5 + addDice) {
                    saikoroImg = MenuItemSprite::create(makeSprite("customize_saikoro_x.png"), makeSprite("customize_saikoro_x.png"), CC_CALLBACK_1(CustomizeLayer::onTapChangeDiceValueButton, this));
                    saikoroImg->setEnabled(false);
                } else {
                    saikoroImg = makeMenuItem("customize_saikoro_empty.png", CC_CALLBACK_1(CustomizeLayer::onTapChangeDiceValueButton, this));
                }
            }

            _menuSaikoro->addChild(saikoroImg);
            saikoroImg->setTag(kTagSaikoro7MenuItem + i);
            saikoroImg->setPosition(saikoroBg->getPosition() + Vec2(saikoroBg->getContentSize().width / 2 - 31.0f - (saikoroImg->getNormalImage()->getContentSize().width + 6) * (0.5f + 8 - i) - 10 * (8 - i), -saikoroBg->getContentSize().height / 2 + 14 + saikoroImg->getNormalImage()->getContentSize().height / 2));
        }
    }
}


void CustomizeLayer::onTapChangePartsButton(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    int tag = ((Node*)pSender)->getTag();
    if ((int)PLAYERCONTROLLER->_playerParts.size() != 0) {
        _partIndex = tag - kTagIndexOfPartID;
        dynamic_cast<GarageMainLayer*>(_mainLayer)->showLayer(GLI_CUSTOMIZE_PART_SELECT);
    } else {
        _dialog = DialogView::createWithShortMessage("装備がありません", 1);
        addChild(_dialog, DIALOG_ZORDER);
    }
}

void CustomizeLayer::onTapChangeDiceValueButton(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    int tag = ((Node*)pSender)->getTag();
    // kTagSaikoro7MenuItem
    _saikoroSelectedIndex = tag - kTagSaikoro7MenuItem;
    int saikoro = _saikoros.at(_saikoroSelectedIndex);
    if (saikoro > 0) {
        //有料
        // show CustomizeSaikoroDeleteConfirmPopupLayer
        auto popup = CustomizeSaikoroDeleteConfirmPopupLayer::createWithSaikoro(_saikoros.at(_saikoroSelectedIndex));
        addChild(popup, ZORDER_POPUP);
        popup->_delegate = this;
    } else {
        // show pop select saikoro
        showCustomizeSaikoroPopupLayer();
    }
}

void CustomizeLayer::showCustomizeSaikoroPopupLayer()
{
    auto layer = CustomizeSaikoroPopupLayer::create();
    addChild(layer, 50);
    layer->_delegate = this;
}

void CustomizeLayer::btCallback(int btIndex, void* object)
{
    if (btIndex == CSDF_BT_NO || btIndex == CCS_BT_NO || btIndex == CS_CANCEl) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        _saikoroSelectedIndex = -1;
    } else if (btIndex == CSDF_BT_YES) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
        auto dialog = DialogView::createDialog(StringUtils::format("※注意※\n削除には%dトークン消費します", systemSettingModel->getTokenOfDiceReset()), "目を削除しますか?", 2, TAG_CUSTOMIZE_SAIKORO_DEL);
        addChild(dialog, DIALOG_ZORDER);
        dialog->_delegate = this;
    } else if (btIndex == CCS_BT_YES) {
        if (isSetDice() == true) {
            SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
            __String* value = (__String*)object;
            _saikoros.at(_saikoroSelectedIndex) = value->intValue();
            _flagChangeSaikoro = 1;
            requestSaveCustomizeChange();

            _saikoroSelectedIndex = -1;
        } else {
            showBuyTokenDialog();
            _saikoroSelectedIndex = -1;
        }
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        // BT_CSCP_CHANGE -> confirm (有料)
        // check 有料と無料
        // show CustomizeSaikoroPopupLayer
        auto popup = CustomizeSaikoroConfirmPopupLayer::createWithSaikoro(btIndex - CS_SAIKORO1 + 1);
        addChild(popup, ZORDER_POPUP);
        popup->_delegate = this;
        // popup->setMessage(checkHasCharge());
    }
}

void CustomizeLayer::onTapBackButton(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    if (checkHasChanged()) {
        _flagChangeSaikoro = 0;
        requestSaveCustomizeChange();
    } else {
        backToGarageList();
    }
}

void CustomizeLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    onTapBackButton(pSender);
}

void CustomizeLayer::btDialogCallback(ButtonIndex aIndex, int tag)
{
    if (aIndex == BT_YES) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        if (tag == TAG_CUSTOMIZE_SAIKORO_DEL) {
            if (isDeleteDice() == true) {
                int temp = _saikoros.at(_saikoroSelectedIndex);
                _saikoros.at(_saikoroSelectedIndex) = -1;
                _flagChangeSaikoro = 2;
                requestSaveCustomizeChange();
                _saikoros.at(_saikoroSelectedIndex) = temp;
            } else {
                showBuyTokenDialog();
                _saikoroSelectedIndex = -1;
            }
            return;
        } else if (tag == TAG_CUSTOMIZE_SAIKORO_DEL_SUCCESS) {
            _saikoros.at(_saikoroSelectedIndex) = -1;
            showSaikoro();
        } else if (tag == TAG_CUSTOMIZE_SAIKORO_DEL_FAIL) {
            // back to Shop
            // reset
            _partIDs.clear();
            _saikoros.clear();

            dynamic_cast<GarageMainLayer*>(_mainLayer)->showShopToken();
        }
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }
    _saikoroSelectedIndex = -1;
}

void CustomizeLayer::requestSaveCustomizeChange()
{
    PlayerCharactersModel* playerCharactersModel = PLAYERCONTROLLER->_playerCharactersSorted.at(_pcIndex);
    int partSlot1 = -1;
    int partSlot2 = -1;
    if (_flagChangeSaikoro) {
        // save saikoro only
        partSlot1 = playerCharactersModel->getPartsSlot1();
        partSlot2 = playerCharactersModel->getPartsSlot2();
    } else {
        // save all
        partSlot1 = _partIDs.at(0) < 0 ? -1 : PLAYERCONTROLLER->_playerPartsSorted.at(_partIDs.at(0))->getID();
        partSlot2 = _partIDs.at(1) < 0 ? -1 : PLAYERCONTROLLER->_playerPartsSorted.at(_partIDs.at(1))->getID();
    }

    _progress->onStart();
    _request->setCustomCharacter(playerCharactersModel->getID(), partSlot1, partSlot2, _saikoros.at(6), _saikoros.at(7), _saikoros.at(8), CC_CALLBACK_1(CustomizeLayer::onResponseCustom, this));
}

void CustomizeLayer::backToGarageList()
{
    _partIDs.clear();
    _saikoros.clear();
    _pcIndex = -1;
    _partIndex = -1;
    dynamic_cast<GarageMainLayer*>(_mainLayer)->showLayer(GLI_LIST);
}

bool CustomizeLayer::checkHasChanged()
{
    PlayerCharactersModel* playerCharactersModel = PLAYERCONTROLLER->_playerCharactersSorted.at(_pcIndex);
    int a =  playerCharactersModel->getPartsSlot1();
    a = a <= 0 ? -1 : a;
    int tmp = _partIDs.at(0) < 0 ? -1 : PLAYERCONTROLLER->_playerPartsSorted.at(_partIDs.at(0))->getID();
    if (a != tmp) {
        return true;
    }
    a =  playerCharactersModel->getPartsSlot2();
    a = a <= 0 ? -1 : a;
    tmp = _partIDs.at(1) < 0 ? -1 : PLAYERCONTROLLER->_playerPartsSorted.at(_partIDs.at(1))->getID();
    if (a != tmp) {
        return true;
    }

    a =  playerCharactersModel->getDiceValue7();
    a = a < 1 ? -1 : a;
    tmp = _saikoros.at(6);
    tmp = tmp < 1 ? -1 : tmp;
    if (a != tmp) {
        return true;
    }

    a =  playerCharactersModel->getDiceValue8();
    a = a < 1 ? -1 : a;
    tmp = _saikoros.at(7);
    tmp = tmp < 1 ? -1 : tmp;
    if (a != tmp) {
        return true;
    }

    a =  playerCharactersModel->getDiceValue9();
    a = a < 1 ? -1 : a;
    tmp = _saikoros.at(8);
    tmp = tmp < 1 ? -1 : tmp;
    if (a != tmp) {
        return true;
    }

    return false;
}


bool CustomizeLayer::checkHasCharge()
{
    PlayerCharactersModel* playerCharactersModel = PLAYERCONTROLLER->_playerCharactersSorted.at(_pcIndex);
    int freeCount = 0;
    if (_saikoroSelectedIndex == 6) {
        freeCount = playerCharactersModel->getFreeDice7Count();
    } else if (_saikoroSelectedIndex == 7) {
        freeCount = playerCharactersModel->getFreeDice8Count();
    } else if (_saikoroSelectedIndex == 8) {
        freeCount = playerCharactersModel->getFreeDice9Count();
    }
    if (freeCount > 0) {
        return false;
    }
    return true;
}

void CustomizeLayer::onResponseCustom(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "set_custom");
    std::string result = Json_getString(jsonData1, "result", "false");

    if (result == "true") {
        Json* jsonDataData = Json_getItem(jsonData1, "data");

        Json* mDatas = Json_getItem(jsonDataData, "players");
        int64_t player_id = atoll(Json_getString(mDatas, "id", "-1"));
        int free_token = atoi(Json_getString(mDatas, "free_token", "-1"));
        int pay_token = atoi(Json_getString(mDatas, "pay_token", "-1"));
        if (player_id == PLAYERCONTROLLER->_player->getID()) {
            PLAYERCONTROLLER->_player->setFreeToken(free_token);
            PLAYERCONTROLLER->_player->setPayToken(pay_token);
            PLAYERCONTROLLER->_player->update();
            dynamic_cast<GarageMainLayer*>(_mainLayer)->updateInfoLayer();
            mDatas = Json_getItem(jsonDataData, "player_characters");
            if (mDatas && mDatas->type == Json_Array) {
                for (Json* child = mDatas->child; child; child = child->next) {
                    PLAYERCONTROLLER->updateCharacter(child, atoiNull(Json_getString(child, "lock", "")));
                }
            }

            if (_flagChangeSaikoro == 0) {
                backToGarageList();
            } else {
                showSaikoro();
                if (_flagChangeSaikoro == 2) {
                    ArtLtvMeasurement::send(MEASURMENT_INFO::ART::RELEASED_DICE_EYE, (int)PLAYERCONTROLLER->_player->getID());
                    auto dialog = DialogView::createWithShortMessage("刻印された目を削除しました", 1, TAG_CUSTOMIZE_SAIKORO_DEL_SUCCESS);
                    addChild(dialog, DIALOG_ZORDER);
                    dialog->_delegate = this;
                } else {
                    ArtLtvMeasurement::send(MEASURMENT_INFO::ART::SETED_DICE_EYE, (int)PLAYERCONTROLLER->_player->getID());
                }
            }
        }
    }
}

const bool CustomizeLayer::isSetDice()
{
    return SystemSettingModel::getModel()->getTokenOfDiceCustom() <= PLAYERCONTROLLER->_player->getFreeToken() + PLAYERCONTROLLER->_player->getPayToken();
}

const bool CustomizeLayer::isDeleteDice()
{
    return SystemSettingModel::getModel()->getTokenOfDiceReset() <= PLAYERCONTROLLER->_player->getFreeToken() + PLAYERCONTROLLER->_player->getPayToken();
}

void CustomizeLayer::showBuyTokenDialog()
{
    auto dialog = DialogView::createDialog("トークンを購入しますか?", "トークンが不足しています", 2, TAG_CUSTOMIZE_SAIKORO_DEL_FAIL);
    dialog->_delegate = this;
    addChild(dialog, DIALOG_ZORDER);
}

