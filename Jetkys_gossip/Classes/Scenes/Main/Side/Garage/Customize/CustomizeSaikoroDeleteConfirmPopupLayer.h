#ifndef __syanago__CustomizeSaikoroDeleteConfirmPopupLayer__
#define __syanago__CustomizeSaikoroDeleteConfirmPopupLayer__

#include "LayerPriority.h"
#include "PopUpDelegate.h"

#define kPriorityCustomizeSaikoroDeleteConfirmPopupLayer -650

enum CustomizeSaikoroDeleteConfirm {
    CSDF_BT_YES = 15,
    CSDF_BT_NO,
};

class CustomizeSaikoroDeleteConfirmPopupLayer : public LayerPriority {
public:
    
    PopUpDelegate *_delegate;
    
    static CustomizeSaikoroDeleteConfirmPopupLayer *createWithSaikoro(int saikoro);
    bool initWithSaikoro(int saikoro);
    
    void callBackbtn(Ref *psender);
    
private:
    int _saikoro;
};

#endif /* defined(__syanago__CustomizeSaikoroDeleteConfirmPopupLayer__) */
