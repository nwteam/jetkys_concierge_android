#ifndef __syanago__CustomizeLayer__
#define __syanago__CustomizeLayer__

#include "SyanagoBaseLayer.h"
#include "DialogView.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"
#include "create_func.h"

using namespace SyanagoAPI;

#define kTagIndexOfPartID 100
#define kTagSaikoro7MenuItem 90

class CustomizeLayer : public SyanagoBaseLayer, public create_func<CustomizeLayer>, public DialogDelegate {
public:
    CustomizeLayer();
    ~CustomizeLayer();
    
    using create_func::create;
    bool init(int pcIndex);
    
    static std::vector<int> _saikoros;
    static std::vector<int> _partIDs;
    static int _partIndex;
    static int _pcIndex;
    
private:
    void showCharacterInformations();
    void showSaikoro();
    void showPartsOfSyanago();
    
    void onTapChangePartsButton(Ref *pSender);
    void onTapChangeDiceValueButton(Ref *pSender);
    void onTapBackButton(Ref* pSender);
    void btMenuItemCallback(cocos2d::Ref *pSender);
    
    void btCallback(int btIndex, void *object);
    void btDialogCallback(ButtonIndex aIndex, int tag);
    
    void requestSaveCustomizeChange();
    void showCustomizeSaikoroPopupLayer();
    
    void backToGarageList();
    bool checkHasChanged();
    bool checkHasCharge();
    
    
    void onResponseCustom(Json* response);
    const bool isSetDice();
    const bool isDeleteDice();
    void showBuyTokenDialog();
    
    DefaultProgress* _progress;
    RequestAPI* _request;
    
    Label *_exerciceLabel;
    Label *_reactionLabel;
    Label *_decisionLabel;
    
    Label *_exercisePlusLabel;
    Label *_reactionPlusLabel;
    Label *_decisionPlusLabel;
    
    int _saikoroSelectedIndex;
    Menu *_menuSaikoro;
    Sprite *_information_board;
    
    Node *_saikoroNode;
    Node *_partNode;
    
    //0: save when back, 1: 追加, 2: 削除
    int _flagChangeSaikoro;
    int _viewMasterCharacterId;
};


#endif /* defined(__syanago__CustomizeLayer__) */
