#include "CustomizeSaikoroPopupLayer.h"
#include "jCommon.h"
#include "CustomizeSaikoroConfirmPopupLayer.h"
#include "FontDefines.h"

bool CustomizeSaikoroPopupLayer::init()
{
    if (!LayerPriority::initWithPriority(kPriorityCustomizeSaikoroPopUpLayer)) {
        return false;
    }
    _delegate = nullptr;

    Size winSize = Director::getInstance()->getWinSize();
    // add bgr dialog
    auto bg = makeSprite("customize_popup_bg.png");
    addChild(bg);
    bg->setPosition(Vec2(winSize.width / 2, winSize.height / 2));

    auto title = Label::createWithTTF("選んだ目を刻印します", FONT_NAME_2, 34, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(title);
    title->enableShadow();
    title->setAnchorPoint(Vec2(0.5, 1));
    title->setPosition(bg->getPosition() + Vec2(0, bg->getContentSize().height / 2 - 30));
    fixFontPosition(title);

    // add button close
    auto menu = MenuPriority::createWithPriority(kPriorityCustomizeSaikoroPopUpMenu);
    menu->setPosition(bg->getPosition() + Vec2(0, 20));
    menu->setAnchorPoint(Vec2::ZERO);
    addChild(menu);

    auto closeButton = makeMenuItem("customize_popup_button.png", "閉じる", 24, CC_CALLBACK_1(CustomizeSaikoroPopupLayer::callBackbtn, this));
    menu->addChild(closeButton);
    closeButton->setTag(CS_CANCEl);
    closeButton->setPosition(Vec2(0, -bg->getContentSize().height / 2 + 10 + closeButton->getNormalImage()->getContentSize().height / 2));

    for (int i = 0; i < 6; i++) {
        int j = i / 3;
        int k = i % 3;

        auto saikoroImg = makeMenuItem(StringUtils::format("customize_popup_saikoro_%d.png", i + 1).c_str(), CC_CALLBACK_1(CustomizeSaikoroPopupLayer::callBackbtn, this));
        menu->addChild(saikoroImg);
        saikoroImg->setTag(CS_SAIKORO1 + i);

        Size kSizeSaikoroImg = saikoroImg->getContentSize();

        saikoroImg->setPosition(Vec2(-30.0f - kSizeSaikoroImg.width * 1.5f, 0) + Vec2(kSizeSaikoroImg.width * (0.5f + k) + 15.0f * k, (kSizeSaikoroImg.height + 10) * (-0.5f + j)));
    }


    return true;
}

void CustomizeSaikoroPopupLayer::callBackbtn(Ref* psender)
{
    // Destroy dialog
    int aIndex = ((Node*)psender)->getTag();
    PopUpDelegate* delegate = _delegate;
    removeFromParent();

    // call back
    if (delegate != nullptr) {
        // int saikoroIndex = atoi(((__String*) object)->getCString());
        delegate->btCallback(aIndex, nullptr);
    }

//    if (aIndex != CS_CANCEl) {
//        //show dialog confirm
//        auto popup = CustomizeSaikoroConfirmPopupLayer::createWithSaikoro(aIndex);
//        addChild(popup, 100);
//        popup->_delegate = this;
//    } else {
//        btCallback(aIndex, nullptr);
//    }
}

void CustomizeSaikoroPopupLayer::btCallback(int btIndex, void* object)
{
    PopUpDelegate* delegate = _delegate;
    removeFromParent();

    // call back
    if (delegate != nullptr) {
        int saikoroIndex = atoi(((__String*) object)->getCString());
        delegate->btCallback(saikoroIndex, nullptr);
    }
}
