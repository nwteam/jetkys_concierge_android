#include "CustomizePartSelectLayer.h"
#include "TeamEditMainLayer.h"
#include "PlayerController.h"
#include "CustomizeLayer.h"
#include "GarageMainLayer.h"
#include "GaragePartSortLayer.h"
#include "PartModel.h"
#include "PartsIconSprite.h"

CustomizePartSelectLayer::~CustomizePartSelectLayer()
{
    _pcIDSelectedTemp.clear();
}

CustomizePartSelectLayer::CustomizePartSelectLayer()
    : _pcSelectedIndex(-1)
{
    _title = "ガレージ【装備】";
    _pcIDSelectedTemp = std::vector<int>(CustomizeLayer::_partIDs.size(), -1);
}

bool CustomizePartSelectLayer::init()
{
    if (!SyanagoBaseLayer::init()) {
        return false;
    }

    showSortMenu();
    if (PLAYERCONTROLLER->_playerPartsSorted.size() == 0) {
        PLAYERCONTROLLER->resetPlayerPartsSorted();
    }

    savepcIDSelectedTemp();

    _sortLabel->setString(SortPlayerParts::getLabelName(PLAYERCONTROLLER->currentSelectedSortPartsKey));
    SortPlayerParts::sort(PLAYERCONTROLLER->currentSelectedSortPartsKey);

    showDatas();

    return true;
}

void CustomizePartSelectLayer::showDatas()
{
    _numberOfCharacter = (int)PLAYERCONTROLLER->_playerPartsSorted.size();
    SyanagoBaseLayer::showDatas();

    auto menu = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    menu->setPosition(Vec2(0, _container->getContentSize().height));
    menu->_scrollView = _scrollView;
    _container->addChild(menu, 2);

    // empty icon
    auto imageFrame = makeMenuItem("garage_icon_empty.png", CC_CALLBACK_1(CustomizePartSelectLayer::btItemSelectedCallback, this));
    imageFrame->setPosition(Vec2(kSizeCharacterIcon.width * (0.5f), -kSizeCharacterIcon.height * (0.5f)));
    imageFrame->setTag(kPlayerCharacterTagEmpty);
    menu->addChild(imageFrame);

    for (int i = 0; i < _numberOfCharacter; i++) {
        auto imageFrame = PartsIconSprite::create(i, CC_CALLBACK_1(CustomizePartSelectLayer::btItemSelectedCallback, this), true);
        imageFrame->showStatusLabel(i);
        menu->addChild(imageFrame);

        int playerPartsIndex =  indexOfpcIDSelectedTemp(PLAYERCONTROLLER->_playerPartsSorted.at(i)->getID());
        if (playerPartsIndex != -1) {
            CustomizeLayer::_partIDs.at(playerPartsIndex) = i;
            imageFrame->showUsedLabel(i);
            imageFrame->setEnabled(false);
        } else if (imageFrame->checkPartsIsNew() == PartsIconSprite::USED_STATUS::USED) {
            imageFrame->showUsedLabel(i);
            imageFrame->setEnabled(false);
        }
    }
}

void CustomizePartSelectLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    int tag = ((Node*)pSender)->getTag();
    if (tag == bt_sort_bt) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);

        auto popup = GaragePartSortLayer::create(CC_CALLBACK_1(CustomizePartSelectLayer::partsSortButtonCallback, this), PLAYERCONTROLLER->currentSelectedSortPartsKey);
        popup->setTag(TAG_LAYER::PARTS_SORT);

        addChild(popup, ZORDER_POPUP);
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        dynamic_cast<GarageMainLayer*>(_mainLayer)->showLayer(GLI_CUSTOMIZE);
    }
}

void CustomizePartSelectLayer::btItemSelectedCallback(cocos2d::Ref* pSender)
{
    if (checkMenuReturn()) {
        return;
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);

    MenuItemSprite* selectedIcon = static_cast<MenuItemSprite*>(pSender);
    _pcSelectedIndex = selectedIcon->getTag() - kPlayerCharacterTagStart;
    if (selectedIcon->getChildByTag(2 * kPlayerCharacterTagStart + 2 * _pcSelectedIndex)) {
        selectedIcon->removeChildByTag(2 * kPlayerCharacterTagStart + 2 * _pcSelectedIndex);
        PLAYERCONTROLLER->_playerPartsSorted.at(_pcSelectedIndex)->saveIsNew(0);
    }
    if (selectedIcon->getTag() == kPlayerCharacterTagEmpty) {
        CustomizeLayer::_partIDs.at(CustomizeLayer::_partIndex) = -1;
    } else {
        CustomizeLayer::_partIDs.at(CustomizeLayer::_partIndex) = _pcSelectedIndex;
    }
    dynamic_cast<GarageMainLayer*>(_mainLayer)->showLayer(GLI_CUSTOMIZE);
}


void CustomizePartSelectLayer::savepcIDSelectedTemp()
{
    for (int i = 0; i < CustomizeLayer::_partIDs.size(); i++) {
        int ppID = -1;
        if (CustomizeLayer::_partIDs.at(i) >= 0) {
            ppID = PLAYERCONTROLLER->_playerPartsSorted.at(CustomizeLayer::_partIDs.at(i))->getID();
        }
        _pcIDSelectedTemp.at(i) = ppID;
    }
}

int CustomizePartSelectLayer::indexOfpcIDSelectedTemp(int pcID)
{
    for (int i = 0; i < _pcIDSelectedTemp.size(); i++) {
        if (_pcIDSelectedTemp.at(i) == pcID) {
            return i;
        }
    }
    return -1;
}

void CustomizePartSelectLayer::scrollViewDidScroll(ScrollView* view)
{
    _scrollBar->updateScrollPoint(view->getContentOffset().y);
}
void CustomizePartSelectLayer::scrollViewDidZoom(ScrollView* view)
{}

void CustomizePartSelectLayer::partsSortButtonCallback(Ref* pSender)
{
    savepcIDSelectedTemp();
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    PLAYERCONTROLLER->currentSelectedSortPartsKey = static_cast<SortPlayerParts::KEY>(static_cast<Node*>(pSender)->getTag());
    _sortLabel->setString(SortPlayerParts::getLabelName(PLAYERCONTROLLER->currentSelectedSortPartsKey));
    SortPlayerParts::sort(PLAYERCONTROLLER->currentSelectedSortPartsKey);

    showDatas();

    removeChildByTag(TAG_LAYER::PARTS_SORT);
}