#include "CustomizeSaikoroDeleteConfirmPopupLayer.h"
#include "jCommon.h"
#include "FontDefines.h"

CustomizeSaikoroDeleteConfirmPopupLayer* CustomizeSaikoroDeleteConfirmPopupLayer::createWithSaikoro(int saikoro)
{
    auto layer = new CustomizeSaikoroDeleteConfirmPopupLayer();
    layer->initWithSaikoro(saikoro);
    layer->autorelease();

    return layer;
}

bool CustomizeSaikoroDeleteConfirmPopupLayer::initWithSaikoro(int saikoro)
{
    if (!LayerPriority::initWithPriority(kPriorityCustomizeSaikoroDeleteConfirmPopupLayer)) {
        return false;
    }
    _delegate = nullptr;
    _saikoro = saikoro;

    Size winSize = Director::getInstance()->getWinSize();
    // add bgr dialog
    auto bg = makeSprite("customize_popup_confirm_bg.png");
    addChild(bg);
    bg->setPosition(Vec2(winSize.width / 2, winSize.height / 2));

    auto title = Label::createWithTTF("刻印された目", FONT_NAME_2, 34, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(title);
    title->enableShadow();
    title->setAnchorPoint(Vec2(0.5, 1));
    title->setPosition(bg->getPosition() + Vec2(0, bg->getContentSize().height / 2 - 30));
    fixFontPosition(title);

    auto saikoroImg = makeSprite(StringUtils::format("customize_popup_saikoro_%d.png", _saikoro).c_str());
    addChild(saikoroImg);
    saikoroImg->setPosition(title->getPosition() + Vec2(0, -title->getContentSize().height * 0.5f - 30 - saikoroImg->getContentSize().height / 2));


    auto message = Label::createWithTTF("この目を削除しますか？", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    addChild(message);
    message->setColor(Color3B::YELLOW);
    message->enableShadow();
    message->setAnchorPoint(Vec2(0.5, 1));
    message->setPosition(saikoroImg->getPosition() + Vec2(0, -saikoroImg->getContentSize().height / 2 - 30));
    fixFontPosition(message);


    // add button close
    auto menu = MenuPriority::createWithPriority(kPriorityCustomizeSaikoroDeleteConfirmPopupLayer - 2);
    addChild(menu);

    auto button = makeMenuItem("customize_popup_button.png", "はい", 24, CC_CALLBACK_1(CustomizeSaikoroDeleteConfirmPopupLayer::callBackbtn, this));
    menu->addChild(button);
    button->setTag(CSDF_BT_YES);

    button = makeMenuItem("customize_popup_button.png", "いいえ", 24, CC_CALLBACK_1(CustomizeSaikoroDeleteConfirmPopupLayer::callBackbtn, this));
    menu->addChild(button);
    button->setTag(CSDF_BT_NO);

    menu->alignItemsHorizontallyWithPadding(50);
    menu->setPosition(Vec2(winSize.width / 2, bg->getPositionY() - bg->getContentSize().height / 2 + 30 + button->getNormalImage()->getContentSize().height / 2));

    return true;
}

void CustomizeSaikoroDeleteConfirmPopupLayer::callBackbtn(Ref* psender)
{
    // Destroy dialog
    int aIndex = ((Node*)psender)->getTag();

    PopUpDelegate* delegate = _delegate;
    int saikoro = _saikoro;
    removeFromParent();

    // call back
    if (delegate != nullptr) {
        auto obj = __String::createWithFormat("%d", saikoro);
        delegate->btCallback(aIndex, obj);
    }
}
