#ifndef __syanago__GarageMainLayer__
#define __syanago__GarageMainLayer__

#include "LayerPriority.h"
#include "DialogView.h"
#include "Loading.h"
#include "FontDefines.h"


enum GarageLayerIndex {
    GLI_LIST = 100,
    GLI_COMPOSITION = 101,
    GLI_COMPOSITION_CHARACTER_SELECT = 102,
    GLI_CUSTOMIZE = 103,
    GLI_CUSTOMIZE_PART_SELECT = 104,
    GLI_AWAKE_CHARACTER = 105,
};
enum HoldingView{
    SYANAGO,
    PARTS
};

class MainScene;

class GarageMainLayer : public Layer {
public:
    bool init();
    CREATE_FUNC(GarageMainLayer);

    GarageMainLayer();
    
    void showListLayer();
    void showSellLayer();
    
    void showHide(bool isShow);
    void showHideEnd();
    
    void showLayer(int layerIndex, int pcIndex = -1);
    void updateInfoLayer();
    void updateholdingLabel(HoldingView tag);
    
    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
    virtual void onTouchCancelled(Touch *touch, Event *unused_event);
    
    void showDetailPlayerCharacter(int pcID);
    void showDetailParts(int pID, int playerPId);
    
    void showShopToken();
    void showShopTokenCallback();
    void update(float dt);
    
    bool isShow() {
        return _isShow;
    }
    
    MainScene *_mainScene;
    Label* _reportMess;
    
    bool isMoving();

private:
    enum TAG_SPRITE {
        SIDE_BUTTON = 2
    };
    enum ACTION_TAG{
        BASE_HIDE = 1000,
    };
    
    void showGarageButton();
    bool checkTouchAvailable(Touch* touch);
    
    
    int _layerIndex;
    bool _isShow;
    
    Vec2 _deltaPoint;
    int _touchID;
    
    Label * _holdingLabel;
    Label * _holdingTile;
    
    bool _isMoving;
};

#endif /* defined(__syanago__GarageMainLayer__) */
