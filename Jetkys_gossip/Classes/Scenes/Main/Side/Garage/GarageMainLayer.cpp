#include "GarageMainLayer.h"
#include "GarageListLayer.h"
#include "CustomizeLayer.h"
#include "CompositionLayer.h"
#include "MainScene.h"
#include "CompositionLayer.h"
#include "CompositionCharacterSelectLayer.h"
#include "CustomizePartSelectLayer.h"
#include "AwakeCharacterLayer.h"


GarageMainLayer::GarageMainLayer():
    _layerIndex(-1)
    , _isShow(false)
    , _touchID(-1)
    , _isMoving(false) {}

bool GarageMainLayer::init()
{
    if (!Layer::init()) {
        return false;
    }

    setPosition(Vec2(Director::getInstance()->getWinSize().width, 0));

    showGarageButton();

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GarageMainLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GarageMainLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GarageMainLayer::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(GarageMainLayer::onTouchCancelled, this);

    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}

void GarageMainLayer::showGarageButton()
{
    auto sideButton = makeSprite("garage_button.png");
    sideButton->setPosition(Vec2(-sideButton->getContentSize().width / 2, Director::getInstance()->getWinSize().height / 2));
    sideButton->setTag(TAG_SPRITE::SIDE_BUTTON);
    addChild(sideButton);
}

bool GarageMainLayer::onTouchBegan(Touch* touch, Event* unused_event)
{
    if (!checkTouchAvailable(touch)) {
        return false;
    }
    _touchID = touch->getID();
    _deltaPoint = Vec2::ZERO;

    if (_layerIndex < 0 || (_layerIndex >= 0 && !getChildByTag(_layerIndex))) {
        _mainScene->changeSideZOrder(SLI_GARAGE);
        if (_layerIndex < 0) {
            showLayer(GLI_LIST);
        } else {
            showLayer(_layerIndex);
        }
    }

    stopAllActions();
    scheduleUpdate();

    _isMoving = true;

    return true;
}

bool GarageMainLayer::checkTouchAvailable(Touch* touch)
{
    if (!isVisible() ||
        _touchID != -1 ||
        _mainScene->checkMoving() ||
        _mainScene->isMainTutorial()) {
        return false;
    }

    Sprite* editSprite = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::SIDE_BUTTON));

    Rect editRect = Rect(
        editSprite->getPosition().x - editSprite->getContentSize().width / 2,
        editSprite->getPosition().y - editSprite->getContentSize().height / 2,
        editSprite->getContentSize().width,
        editSprite->getContentSize().height);

    Size winSize = Director::getInstance()->getWinSize();
    Rect layerRect = Rect(0, 0, winSize.width, winSize.height);

    if (layerRect.containsPoint(convertToNodeSpace(touch->getLocation())) ||
        editRect.containsPoint(convertToNodeSpace(touch->getLocation()))) {
        if (_layerIndex == GLI_CUSTOMIZE_PART_SELECT ||
            _layerIndex == GLI_COMPOSITION ||
            _layerIndex == GLI_COMPOSITION_CHARACTER_SELECT ||
            _layerIndex == GLI_CUSTOMIZE ||
            (_layerIndex == GLI_LIST && getChildByTag(_layerIndex) &&
             nullptr != static_cast<GarageListLayer*>(getChildByTag(_layerIndex))->_sellLayer)) {
            return false;
        }
        return true;
    }
    return false;
}


void GarageMainLayer::onTouchMoved(Touch* touch, Event* unused_event)
{
    if (touch->getID() == _touchID) {
        _deltaPoint = touch->getLocation() - touch->getPreviousLocation();
    }
}

void GarageMainLayer::onTouchEnded(Touch* touch, Event* unused_event)
{
    if (touch->getID() == _touchID) {
        _touchID = -1;
        if (getPosition().x == 0) {
            unscheduleUpdate();
            _isMoving = false;
        }
    }
}

void GarageMainLayer::onTouchCancelled(Touch* touch, Event* unused_event)
{
    if (touch->getID() == _touchID) {
        _touchID = -1;
        if (getPosition().x == 0) {
            unscheduleUpdate();
            _isMoving = false;
        }
    }
}

void GarageMainLayer::update(float dt)
{
    if (getPositionX() <= 0 && _reportMess && !_reportMess->isVisible()) {
        _reportMess->setVisible(true);
    } else if (_reportMess && _reportMess->isVisible()) {
        _reportMess->setVisible(false);
    }
    if (_touchID == -1) {
        unscheduleUpdate();
        showHide(_deltaPoint.x <= 0);
        return;
    }

    Size winSize = Director::getInstance()->getWinSize();
    float posX = getPositionX() + _deltaPoint.x;
    setPositionX(MIN(MAX(posX, 0), winSize.width));
}

void GarageMainLayer::showHide(bool isShow)
{
    if (_touchID != -1 && getChildByTag(_layerIndex)) {
        unscheduleUpdate();
        _touchID = -1;
    }
    _isShow = isShow;

    if (!_isShow) {
        _reportMess->setString("");
    }

    if (!getActionByTag(ACTION_TAG::BASE_HIDE)) {
        Size winSize = Director::getInstance()->getWinSize();
        Action* baseHide = Sequence::create(MoveTo::create(0.3f, Vec2(_isShow ? 0 : winSize.width, 0)), CallFunc::create(CC_CALLBACK_0(GarageMainLayer::showHideEnd, this)), NULL);
        baseHide->setTag(ACTION_TAG::BASE_HIDE);
        runAction(baseHide);
    }
}

void GarageMainLayer::showHideEnd()
{
    _isMoving = false;
    if (!_isShow) {
        removeChildByTag(_layerIndex);
        _layerIndex = -1;
    } else if (_reportMess && !_reportMess->isVisible()) {
        _reportMess->setVisible(true);
    }
}

void GarageMainLayer::showLayer(int layerIndex, int pcIndex /* = -1*/)
{
    Node* lastLayer = nullptr;
    if (_layerIndex >= 0 && getChildByTag(_layerIndex)) {
        _touchID = -1;
        _deltaPoint = Vec2(1, 0);
        lastLayer = getChildByTag(_layerIndex);
    }

    Size winSize = Director::getInstance()->getWinSize();

    auto teropbg = makeSprite("telop_bg.png");
    teropbg->setPosition(Point(winSize.width / 2, teropbg->getContentSize().height / 2));

    _reportMess = Label::createWithTTF("", FONT_NAME_2, 22);
    _reportMess->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    _reportMess->setColor(Color3B::WHITE);
    _reportMess->setAnchorPoint(Vec2(0, 0.75f));
    _reportMess->setPosition(Vec2(winSize.width, teropbg->getContentSize().height / 2));
    _reportMess->runAction(RepeatForever::create(Sequence::create(MoveBy::create(TEROP_SPEED, Vec2(-1500, 0)),
                                                                  CallFuncN::create([](Ref* sender) {
        auto node = (Node*) sender;
        node->setPositionX(node->getPositionX() + 1500);
    }), NULL)));

    Sprite* holdingBase = makeSprite("garage_list_holding_base.png");
    holdingBase->setPosition(Point(winSize.width - holdingBase->getContentSize().width / 2,
                                   teropbg->getPosition().y + teropbg->getContentSize().height / 2 + holdingBase->getContentSize().height / 2));

    _holdingTile = Label::createWithTTF("所持数", FONT_NAME_2, 18);
    _holdingTile->setAnchorPoint(Vec2(0.0, 0.5));
    _holdingTile->setPosition(Point(5, holdingBase->getContentSize().height / 2 - 9));
    holdingBase->addChild(_holdingTile);

    _holdingLabel = Label::createWithTTF(StringUtils::format("%d/%d",
                                                             (int)PLAYERCONTROLLER->_playerCharacterModels.size(),
                                                             PLAYERCONTROLLER->_player->getGarageSize()).c_str(), FONT_NAME_2, 18);
    _holdingLabel->setAnchorPoint(Vec2(1.0, 0.5));
    _holdingLabel->setPosition(Point(holdingBase->getContentSize().width - 5,
                                     holdingBase->getContentSize().height / 2 - 9));
    holdingBase->addChild(_holdingLabel);

    _layerIndex = layerIndex;
    switch (_layerIndex) {
    case GLI_LIST: {
        _reportMess->setString(PLAYERCONTROLLER->_messages.at(15));
        auto layer = GarageListLayer::create(this);
        layer->setTag(GLI_LIST);
        layer->addChild(teropbg);
        layer->addChild(holdingBase, 2);
        layer->addChild(_reportMess);
        addChild(layer);
        break;
    }
    case GLI_COMPOSITION: {
        _reportMess->setString(PLAYERCONTROLLER->_messages.at(21));
        auto layer = CompositionLayer::create(pcIndex, this);
        layer->setTag(GLI_COMPOSITION);
        layer->addChild(teropbg);
        layer->addChild(_reportMess);
        addChild(layer);
        break;
    }
    case GLI_COMPOSITION_CHARACTER_SELECT: {
        _reportMess->setString(PLAYERCONTROLLER->_messages.at(22));
        auto layer = CompositionCharacterSelectLayer::create();
        layer->_mainLayer = this;
        layer->setTag(GLI_COMPOSITION_CHARACTER_SELECT);
        layer->addChild(teropbg);
        layer->addChild(holdingBase, 2);
        layer->addChild(_reportMess);
        addChild(layer);
        break;
    }
    case GLI_CUSTOMIZE: {
        _reportMess->setString(PLAYERCONTROLLER->_messages.at(17));
        auto layer = CustomizeLayer::create(pcIndex);
        layer->_mainLayer = this;
        layer->setTag(GLI_CUSTOMIZE);
        layer->addChild(teropbg);
        layer->addChild(_reportMess);
        addChild(layer);
        break;
    }
    case GLI_CUSTOMIZE_PART_SELECT: {
        _reportMess->setString(PLAYERCONTROLLER->_messages.at(18));
        auto layer = CustomizePartSelectLayer::create();
        layer->setTag(GLI_CUSTOMIZE_PART_SELECT);
        layer->_mainLayer = this;
        layer->addChild(teropbg);
        layer->addChild(holdingBase, 2);
        _holdingLabel->setString(StringUtils::format("%d/%d",
                                                     (int)PLAYERCONTROLLER->_playerParts.size(),
                                                     PLAYERCONTROLLER->_player->getGarageSize() * 2).c_str());
        layer->addChild(_reportMess);
        addChild(layer);
        break;
    }
    case GLI_AWAKE_CHARACTER: {
        auto layer = AwakeCharacterLayer::create(pcIndex,
                                                 [this](const bool headervisible) {
                if (headervisible == true) {
                    _mainScene->footerLayer->setVisible(true);
                    _mainScene->headerLayer->setVisible(true);
                } else {
                    _mainScene->footerLayer->setVisible(false);
                    _mainScene->headerLayer->setVisible(false);
                }
            },
                                                 [this]() {
                if (_mainScene) {
                    _mainScene->homeLayer->updateCharacterImg();
                    _mainScene->headerLayer->updateInfoLayer();
                }
                showLayer(GLI_LIST);
            });
        layer->setTag(GLI_CUSTOMIZE_PART_SELECT);
        addChild(layer);
    }
    }
    if (lastLayer) {
        lastLayer->removeFromParent();
    }
}

void GarageMainLayer::updateInfoLayer()
{
    _mainScene->headerLayer->updateInfoLayer();
}

void GarageMainLayer::showDetailPlayerCharacter(int pcID)
{
    _mainScene->showDetailPlayerCharacter(pcID);
}

void GarageMainLayer::showDetailParts(int pID, int playerPId)
{
    _mainScene->showDetailParts(pID, playerPId);
}

void GarageMainLayer::showShopToken()
{
    _isShow = false;
    Size winSize = Director::getInstance()->getWinSize();
    runAction(Sequence::create(
                  MoveTo::create(0.3f, Vec2(winSize.width, 0)), CallFunc::create(CC_CALLBACK_0(GarageMainLayer::showHideEnd, this)),
                  CallFunc::create(CC_CALLBACK_0(GarageMainLayer::showShopTokenCallback, this)),
                  NULL));
}

void GarageMainLayer::showShopTokenCallback()
{
    _mainScene->goToTokenShop();
}

void GarageMainLayer::updateholdingLabel(HoldingView tag)
{
    int maxSize = PLAYERCONTROLLER->_player->getGarageSize();
    if (tag == PARTS) {
        maxSize *= 2;
    }
    _holdingLabel->setString(StringUtils::format("%d/%d",
                                                 static_cast<int>(PLAYERCONTROLLER->_playerCharacterModels.size()),
                                                 maxSize).c_str());
}


bool GarageMainLayer::isMoving()
{
    return _isMoving;
}

void GarageMainLayer::showSellLayer()
{
    _isMoving = true;
    showLayer(GLI_LIST);
    dynamic_cast<GarageListLayer*>(getChildByTag(GLI_LIST))->showGarageSellCharacterLayer();
    showHide(true);
}

void GarageMainLayer::showListLayer()
{
    _isMoving = true;
    showLayer(GLI_LIST);
    showHide(true);
}
