#ifndef __syanago__GaragePartSortLayer__
#define __syanago__GaragePartSortLayer__

#include <functional>
#include "create_func.h"
#include "LayerPriority.h"
#include "SyanagoBaseLayer.h"

class GaragePartSortLayer : public create_func<GaragePartSortLayer>, public LayerPriority
{
    
public:
    typedef std::function<void(Ref*)> SortButtonCallback;
    bool init(const SortButtonCallback& sortButtonCallback, SortPlayerParts::KEY nowPartsSortKey);
    using create_func::create;
    
private:
    void showBackground();
    void showButtons(const SortButtonCallback& sortButtonCallback, SortPlayerParts::KEY nowPartsSortKey);
    void cancelButtonCallback(Ref *pSender);
    
    enum TAG_SPRITE{
        SORT_BG = 0
    };
};

#endif /* defined(__syanago__GaragePartSortLayer__) */
