#ifndef __syanago__AwakeCharacterLayer__
#define __syanago__AwakeCharacterLayer__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "create_func.h"
#include "show_head_line.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"
#include "AwakeModel.h"
#include "AwakeResponseModel.h"

USING_NS_CC;
using namespace SyanagoAPI;

class AwakeCharacterLayer : public Layer, public create_func<AwakeCharacterLayer>, public show_head_line
{
public:
    typedef std::function<void(const bool)> CallHeaderVisible;
    typedef std::function<void()> EndCallback;
    AwakeCharacterLayer();
    ~AwakeCharacterLayer();
    using create_func::create;
    bool init(const int playerCharacterId, const CallHeaderVisible& callHeaderVisible, const EndCallback& endCallback);
    
private:
    enum TAG_SPRITE{
        BACKGROUND =0,
        CHARACTER_BEFORE,
        CHARACTER_AFTER ,
        ARROW,
        MATERIAL_CHARACTERS,
        BUTTON,
        LABEL,
        DETAIL_LAYER,
        CHARACTER_SELECT,
        WHITE_LAYER,
        BLACK_LAYER,
        DROP_EFFECT,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_CHARACTER_BEFORE,
        Z_CHARACTER_AFTER ,
        Z_ARROW,
        Z_MATERIAL_CHARACTERS,
        Z_BUTTON,
        Z_LABEL,
        Z_DETAIL_LAYER = 12,//Z_order HeadLine = 11
        Z_CHARACTER_SELECT,
        Z_WHITE_LAYER,
        Z_BLACK_LAYER,
        Z_DROP_EFFECT,
    };
    DefaultProgress* _progress;
    RequestAPI* _request;
    std::shared_ptr<AwakeModel> _awakeModel;
    std::shared_ptr<AwakeResponseModel> _awakeResponseModel;
    CallHeaderVisible _callHeaderVisible;
    EndCallback _endCallback;
    
    void showBackground();
    
    void showCanNotAwake();
    
    void onTapBackButton(Ref* pSender);
    void showCharacterBefore(const std::shared_ptr<AwakeModel::Character>& model);
    void onTapPlayerCharacterDetail(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    void showCharacterAfter(const std::shared_ptr<AwakeModel::Character>& model);
    void onTapMasterCharacterDetail(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    void endDetaileCallback();
    
    void showArrow();
    
    void showMaterialCharacter(const std::shared_ptr<AwakeModel>& model);
    void setMaterialCharacter();
    void onTapCharacterSelect(const int masterCharacterId, const int selectNumber);
    
    void showButton();
    void checkAwakeButton();
    void onTapAwakeButton(Ref* sender, ui::Widget::TouchEventType type);
    void responseAwake(Json* response);
    
    void showWhiteOutLayer();
    void showBlackOutLayer();
    void showDropLayer();
    void showDetailLayer();
    void endAwakeCallBack();
    
    void showAttentionLabel();
};

#endif