#ifndef __syanago__AwakeResponseModel__
#define __syanago__AwakeResponseModel__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"

USING_NS_CC;

class AwakeResponseModel
{
public:
    AwakeResponseModel(Json* response);
    ~AwakeResponseModel();
    
    CC_SYNTHESIZE_READONLY(int, playerCharacterId, PlayerCharacterId);
    CC_SYNTHESIZE_READONLY(int, masterCharacterId, MasterCharacterId);
    
private:
    
    void setAwakeCharacter(Json* child);
};

#endif
