#include "AwakeResponseModel.h"
#include "PlayerController.h"

AwakeResponseModel::AwakeResponseModel(Json* response)
{
    auto json = Json_getItem(response, "awake_character");
    if (strcmp(Json_getItem(json, "result")->valueString, "false") == 0) {
        return;
    }
    auto data = Json_getItem(json, "data");

    PLAYERCONTROLLER->_player->setFreeCoin(atoi(Json_getString(data, "free_coin", "-1")));

    Json* jsonDataChara = Json_getItem(data, "awoken_player_character");
    setAwakeCharacter(jsonDataChara);
    playerCharacterId = atoi(Json_getString(jsonDataChara, "id", ""));
    masterCharacterId = atoi(Json_getString(jsonDataChara, "characters_id", ""));

    Json* deleteCharacters = Json_getItem(data, "deleted_player_character_ids");
    if (deleteCharacters && deleteCharacters->type == Json_Array) {
        for (Json* child = deleteCharacters->child; child; child = child->next) {
            PLAYERCONTROLLER->deleteCharacter(atoi(child->valueString));
        }
    }

    Json* deletePars = Json_getItem(data, "deleted_player_parts_ids");
    if (deletePars && deletePars->type == Json_Array) {
        for (Json* child = deletePars->child; child; child = child->next) {
            PLAYERCONTROLLER->deletePart(atoi(child->valueString));
        }
    }
    PLAYERCONTROLLER->resetPlayerCharacterSorted();
}

AwakeResponseModel::~AwakeResponseModel()
{}

void AwakeResponseModel::setAwakeCharacter(Json* child)
{
    PLAYERCONTROLLER->updateCharacter(child, atoiNull(Json_getString(child, "lock", "")), true);
}









