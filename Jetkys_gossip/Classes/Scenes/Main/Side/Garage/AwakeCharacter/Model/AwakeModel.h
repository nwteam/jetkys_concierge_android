#ifndef __syanago__AwakeModel__
#define __syanago__AwakeModel__

#include "cocos2d.h"

USING_NS_CC;

class AwakeModel
{
public:
    AwakeModel(const int playerCharacterId);
    ~AwakeModel();
    
    void setMaterialCharacter(const int playerCharacterId, const int selectMaterialNumber);
    const std::string getMaterialIdsString();
    const bool isAwake();
    const bool isSetMaterial(const int selectNumber);
    
    CC_SYNTHESIZE_READONLY(int, _baseCharacterLevel, BaseCharacterLevel);
    CC_SYNTHESIZE_READONLY(int, _awakeCharacterLevel, AwakeCharacterLevel);
    CC_SYNTHESIZE_READONLY(std::vector<int>, _materialMasterCharacterIds, MaterialMasterCharacterIds);
    CC_SYNTHESIZE_READONLY(std::vector<int>, _materialPlayerCharacterIds, MaterialPlayerCharacterIds);
    
    enum AWAKE_TYPE{
        AFTER = 0,
        BEFORE,
    };
    class Character
    {
    public:
        Character(const int characterId, const AWAKE_TYPE type);
        CC_SYNTHESIZE_READONLY(std::string, _name, Name);
        CC_SYNTHESIZE_READONLY(std::string, _vehcleModelName, VehcleModelName);
        CC_SYNTHESIZE_READONLY(std::string, _bodyTypeName, BodyTypeName);
        CC_SYNTHESIZE_READONLY(int, _playerCharacterId, PlayerCharacterId);
        CC_SYNTHESIZE_READONLY(int, _masterCharacterId, MasterCharacterId);
        CC_SYNTHESIZE_READONLY(int, _rarity, Rarity);
        CC_SYNTHESIZE_READONLY(int, _cost, Cost);
        CC_SYNTHESIZE_READONLY(int, _power, Power);
        CC_SYNTHESIZE_READONLY(int, _technique, Technique);
        CC_SYNTHESIZE_READONLY(int, _brake, Brake);
        CC_SYNTHESIZE_READONLY(int, _dearValue, DearValue);
        CC_SYNTHESIZE_READONLY(int, _lock, Lock);
    };
    CC_SYNTHESIZE_READONLY(std::shared_ptr<AwakeModel::Character>, _beforeCharacterData, BeforeCharacterData);
    CC_SYNTHESIZE_READONLY(std::shared_ptr<AwakeModel::Character>, _afterCharacterData, AfterCharacterData);
    
private:
    const int materialChracterCount();
};

#endif
