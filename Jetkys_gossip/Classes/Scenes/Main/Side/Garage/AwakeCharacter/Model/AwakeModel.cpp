#include "AwakeModel.h"
#include "PlayerController.h"
#include "CharacterModel.h"
#include "BodyTypeModel.h"

AwakeModel::AwakeModel(const int playerCharacterId)
{
    _materialPlayerCharacterIds = {
        0, 0, 0, 0, 0
    };
    _materialMasterCharacterIds = {
        0, 0, 0, 0, 0
    };


    auto playerCharacterModel = PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId];
    _baseCharacterLevel = playerCharacterModel->getLevel();
    const int beforeMasterCharacterId = playerCharacterModel->getCharactersId();
    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(beforeMasterCharacterId));
    _awakeCharacterLevel = characterModel->getAwakeLevel();

    for (int i = 0; i < characterModel->getAwakeCharacterCount(); i++) {
        if (i > 4) {
            break;
        }
        _materialMasterCharacterIds.at(i) = beforeMasterCharacterId;
    }

    std::shared_ptr<AwakeModel::Character>beforeModel(new AwakeModel::Character(playerCharacterId, BEFORE));
    _beforeCharacterData = beforeModel;
    std::shared_ptr<CharacterModel>awakeCharacterModel(CharacterModel::awakeCharacterFind(beforeMasterCharacterId));
    if (awakeCharacterModel) {
        std::shared_ptr<AwakeModel::Character>afterModel(new AwakeModel::Character(awakeCharacterModel->getID(), AFTER));
        _afterCharacterData = afterModel;
    }
}

AwakeModel::~AwakeModel()
{}

AwakeModel::Character::Character(const int characterId, const AWAKE_TYPE type)
{
    if (type == BEFORE) {
        _playerCharacterId = characterId;
        auto playerCharacteModel = PLAYERCONTROLLER->_playerCharacterModels[characterId];
        _masterCharacterId = playerCharacteModel->getCharactersId();
        _dearValue = playerCharacteModel->getDearValue();
        _lock = playerCharacteModel->getLock();
    } else if (type == AFTER) {
        _playerCharacterId = 0;
        _masterCharacterId = characterId;
        _dearValue = 0;
        _lock = 0;
    }
    std::shared_ptr<CharacterModel>model(CharacterModel::find(_masterCharacterId));
    _name = model->getCarName();
    _vehcleModelName = model->getName();
    _rarity = model->getRarity();
    _cost = model->getCost();

    std::shared_ptr<BodyTypeModel>bodyTypeModel(BodyTypeModel::find(model->getBodyType()));
    _bodyTypeName = bodyTypeModel->getName();


    if (type == BEFORE) {
        auto playerCharacterModel = PLAYERCONTROLLER->_playerCharacterModels[characterId];
        _power = playerCharacterModel->getExercise();
        _technique = playerCharacterModel->getReaction();
        _brake = playerCharacterModel->getDecision();
    } else if (type == AFTER) {
        _power = model->getBaseExercise();
        _technique = model->getBaseReaction();
        _brake = model->getBaseDecision();
    }
}

void AwakeModel::setMaterialCharacter(const int playerCharacterId, const int selectMaterialNumber)
{
    if (selectMaterialNumber >= _materialMasterCharacterIds.size() ||
        selectMaterialNumber >= _materialPlayerCharacterIds.size() ||
        selectMaterialNumber < 0 ||
        selectMaterialNumber > 4 ||
        _materialMasterCharacterIds.at(selectMaterialNumber) == 0) {
        CC_ASSERT("[AwakeModel::setMaterialCharacter]error selectNumber is not found");
        return;
    }
    if (playerCharacterId == 0) {
        _materialPlayerCharacterIds.at(selectMaterialNumber) = 0;
        return;
    }

    if (_materialMasterCharacterIds.at(selectMaterialNumber) == PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getCharactersId()) {
        _materialPlayerCharacterIds.at(selectMaterialNumber) = playerCharacterId;
    } else {
        CC_ASSERT("[AwakeModel::setMaterialCharacter]error can not set playerCharacterId");
        return;
    }
}

const std::string AwakeModel::getMaterialIdsString()
{
    std::string result = "";
    const int materialCount = materialChracterCount();
    for (int i = 0; i < materialCount; i++) {
        if (_materialPlayerCharacterIds.at(i) == 0) {
            continue;
        }
        result += StringUtils::format("%d", _materialPlayerCharacterIds.at(i));
        if (i != (materialCount - 1)) {
            result += ", ";
        }
    }
    return result;
}

const int AwakeModel::materialChracterCount()
{
    int result = 0;
    for (int i = 0; i < _materialMasterCharacterIds.size(); i++) {
        if (_materialMasterCharacterIds.at(i) == 0) {
            continue;
        }
        result++;
    }
    return result;
}

const bool AwakeModel::isAwake()
{
    if (_baseCharacterLevel < _awakeCharacterLevel || _afterCharacterData == nullptr) {
        return false;
    }
    if (_beforeCharacterData->getDearValue() < 250) {
        return false;
    }
    if (_beforeCharacterData->getLock() == 1) {
        return false;
    }

    for (int i = 0; i < 4; i++) {
        if (_materialMasterCharacterIds.at(i) < 1) {
            continue;
        }

        if (_materialPlayerCharacterIds.at(i) < 1) {
            return false;
        }
        if (_materialMasterCharacterIds.at(i) != PLAYERCONTROLLER->_playerCharacterModels[_materialPlayerCharacterIds.at(i)]->getCharactersId()) {
            return false;
        }
    }
    return true;
}

const bool AwakeModel::isSetMaterial(const int selectNumber)
{
    if (_materialMasterCharacterIds.at(selectNumber) == 0) {
        return false;
    }
    return true;
}