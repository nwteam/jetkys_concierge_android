#include "AwakeCharacterSelectLayer.h"
#include "SoundHelper.h"
#include "PlayerController.h"
#include "HeaderStatus.h"
#include "ScrollBar.h"
#include "CharacterSelectSortLayer.h"

AwakeCharacterSelectLayer::AwakeCharacterSelectLayer():
    _awakeModel(nullptr)
    , _callback(nullptr)
    , _selectMaterialNumber(-1)
    , _materialMasterCharacterId(-1)
{}

AwakeCharacterSelectLayer::~AwakeCharacterSelectLayer()
{}

bool AwakeCharacterSelectLayer::init(const int materialMasterCharacterId,
                                     const int selectNumber,
                                     const std::shared_ptr<AwakeModel>& awakeModel,
                                     const EndCallback& callback)
{
    if (!Layer::create()) {
        return false;
    }
    _callback = callback;
    _awakeModel = awakeModel;
    _selectMaterialNumber = selectNumber;
    _materialMasterCharacterId = materialMasterCharacterId;

    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    if (PLAYERCONTROLLER->_playerCharactersSorted.size() == 0) {
        PLAYERCONTROLLER->resetPlayerCharacterSorted();
    }

    showBackground();
    showHeadLine(this, "素材選択", CC_CALLBACK_1(AwakeCharacterSelectLayer::onTapBackCallback, this));
    showCharacterList(materialMasterCharacterId);
    showSortButton();

    return true;
}

void AwakeCharacterSelectLayer::onTapBackCallback(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    _callback();
}

void AwakeCharacterSelectLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getWinSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void AwakeCharacterSelectLayer::showCharacterList(const int materialMasterCharacterId)
{
    auto scrollView = createScrollView((int)PLAYERCONTROLLER->_playerCharactersSorted.size(), materialMasterCharacterId);
    addChild(scrollView, Z_ORDER::Z_CHARACTER_LIST);


    const Size winSize = Director::getInstance()->getWinSize();
    auto scrollBar = ScrollBar::initScrollBar(winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 60, scrollView->getViewSize().height, scrollView->getContainer()->getContentSize().height, scrollView->getContentOffset().y);
    scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 75 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    scrollBar->setTag(TAG_SPRITE::SCROLL_BAR);
    addChild(scrollBar, Z_ORDER::Z_SCROLL_BAR);
}

ScrollView* AwakeCharacterSelectLayer::createScrollView(const int holdCharacterCount, const int materialMasterCharacterId)
{
    auto scrollContainer = createCharacterList(holdCharacterCount, materialMasterCharacterId);

    const Size winSize = Director::getInstance()->getWinSize();
    auto scrollView = ScrollView::create(Size(kSizeCharacterIcon.width * kColumn,
                                              winSize.height  - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 60),
                                         scrollContainer);
    scrollView->setDirection(ScrollView::Direction::VERTICAL);
    scrollView->setBounceable(false);
    scrollView->setDelegate(this);
    scrollView->setPosition(Vec2(4, winSize.height - HEADER_STATUS::SIZE::HEIGHT - 12 - 68 - 7 - scrollView->getViewSize().height));
    scrollView->setDelegate(this);
    scrollView->setTag(TAG_SPRITE::CHARACTER_LIST);

    scrollContainer->setPosition(scrollContainer->getPosition() + Vec2(0, -scrollContainer->getContentSize().height + scrollView->getViewSize().height));

    return scrollView;
}

LayerColor* AwakeCharacterSelectLayer::createCharacterList(const int holdCharacterCount, const int materialMasterCharacterId)
{
    const int row = holdCharacterCount / kColumn + ((holdCharacterCount % kColumn) > 0 ? 1 : 0);
    auto result = LayerColor::create(Color4B(0, 0, 0, 0), kSizeCharacterIcon.width * kColumn, kSizeCharacterIcon.height * row);

    int playerCharacterCount = 0;
    for (int i = 0; i < holdCharacterCount; i++) {
        int j = i % kColumn;
        int k = i / kColumn;

        if (i == 0) {
            auto enmpyIcon = ui::Button::create("garage_icon_empty.png");
            enmpyIcon->setPosition(Vec2(kSizeCharacterIcon.width * (0.5f + j), result->getContentSize().height - kSizeCharacterIcon.height * (0.5f + k)));
            enmpyIcon->addTouchEventListener(CC_CALLBACK_2(AwakeCharacterSelectLayer::onTapCharacterIcon, this, 0));
            result->addChild(enmpyIcon);
            continue;
        }

        auto playerCharacterModel = PLAYERCONTROLLER->_playerCharactersSorted.at(playerCharacterCount);
        const int playerCharacterId = playerCharacterModel->getID();

        auto icon = createCharacterIcon(playerCharacterId);
        icon->setPosition(Vec2(kSizeCharacterIcon.width * (0.5f + j), result->getContentSize().height - kSizeCharacterIcon.height * (0.5f + k)));
        if (isSelectCharacter(playerCharacterId, materialMasterCharacterId) == false) {
            icon->setEnabled(false);
            icon->setColor(Color3B::GRAY);
        }
        result->addChild(icon);

        playerCharacterCount++;
    }

    result->setPositionY(result->getPositionY() + 170);
    return result;
}

ui::Button* AwakeCharacterSelectLayer::createCharacterIcon(const int playerCharacterId)
{
    const auto playerCharacterModel = PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId];
    const int masterCharacterId = playerCharacterModel->getCharactersId();
    auto result = ui::Button::create(StringUtils::format("%d_i.png", masterCharacterId),
                                     StringUtils::format("%d_i.png", masterCharacterId),
                                     StringUtils::format("%d_i.png", masterCharacterId),
                                     ui::Button::TextureResType::PLIST);
    result->addTouchEventListener(CC_CALLBACK_2(AwakeCharacterSelectLayer::onTapCharacterIcon, this, playerCharacterId));

    if (isTeamUseCaracter(playerCharacterId) == true) {
        auto useSprite = Sprite::create("garage_list_using_label.png");
        useSprite->setPosition(Vec2(result->getContentSize()) - Vec2(20, 20));
        result->addChild(useSprite);
    } else if (playerCharacterModel->getIsNew() == 1) {
        auto newSprite = Sprite::create("garage_list_new_label.png");
        newSprite->setPosition(Vec2(result->getContentSize()) - Vec2(20, 20));
        result->addChild(newSprite);
    }
    if (playerCharacterModel->getLock() == 1) {
        Sprite* lockIcon = Sprite::create("lock_icon.png");
        lockIcon->setPosition(Point(10, result->getContentSize().height - 10));
        result->addChild(lockIcon);
    }
    setCharacterViewStatus(playerCharacterId, result);

    return result;
}

void AwakeCharacterSelectLayer::setCharacterViewStatus(const int playerCharacterId, ui::Button* characterIcon)
{
    const auto playerCharacterModel = PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId];
    std::string bottomStr = "";
    switch (PLAYERCONTROLLER->currentSelectedSortCharacterKey) {
    case SortPlayerCharacter::POWER:
        bottomStr = StringUtils::format("%d", playerCharacterModel->getExercise());
        {
            auto levelLabel = Label::createWithTTF(bottomStr, FONT_NAME_2, 16);
            levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
            levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
            levelLabel->enableShadow();
            levelLabel->setPosition(Vec2(characterIcon->getContentSize().width / 2, -2));
            characterIcon->addChild(levelLabel);
        }
        break;
    case SortPlayerCharacter::TECHNIQUE:
        bottomStr = StringUtils::format("%d", playerCharacterModel->getReaction());
        {
            auto levelLabel = Label::createWithTTF(bottomStr, FONT_NAME_2, 16);
            characterIcon->addChild(levelLabel);
            levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
            levelLabel->setPosition(Vec2(characterIcon->getContentSize().width / 2, -2));
            levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
            levelLabel->enableShadow();
        }
        break;
    case SortPlayerCharacter::BRAKE:
        bottomStr = StringUtils::format("%d", playerCharacterModel->getDecision());

        {
            auto levelLabel = Label::createWithTTF(bottomStr, FONT_NAME_2, 16);
            characterIcon->addChild(levelLabel);
            levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
            levelLabel->setPosition(Vec2(characterIcon->getContentSize().width / 2, -2));
            levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
            levelLabel->enableShadow();
        }
        break;
    case SortPlayerCharacter::COST: {
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(playerCharacterModel->getCharactersId()));
        bottomStr = StringUtils::format("%d", characterModel->getCost());

        auto levelLabel = Label::createWithTTF(bottomStr, FONT_NAME_2, 16);
        characterIcon->addChild(levelLabel);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(Vec2(characterIcon->getContentSize().width / 2, -2));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
        levelLabel->enableShadow();
        break;
    }

    case SortPlayerCharacter::RARITY: {
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(playerCharacterModel->getCharactersId()));
        int rarity = characterModel->getRarity();
        auto node = Node::create();
        for (int i = 0; i < rarity; i++) {
            auto start = makeSprite("rarity.png");
            node->addChild(start);
            if (rarity % 2 == 1) {
                start->setPosition(Vec2((start->getContentSize().width - 5) * (i - rarity / 2), 0));
            } else {
                int j = i - rarity / 2;
                start->setPosition(Vec2((start->getContentSize().width - 5) * (0.5f + j), 0));
            }
        }
        node->setContentSize(Size(24 * rarity, 23));
        characterIcon->addChild(node);
        node->setPosition(Vec2(characterIcon->getContentSize().width / 2, 0));
        break;
    }

    default: {
        // level
        auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %d", playerCharacterModel->getLevel()), FONT_NAME_2, 16);
        characterIcon->addChild(levelLabel);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(Vec2(characterIcon->getContentSize().width / 2, -2));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
        levelLabel->enableShadow();
        break;
    }
    }
}

const bool AwakeCharacterSelectLayer::isTeamUseCaracter(const int playerChracterId)
{
    const auto teamModel = PLAYERCONTROLLER->_playerTeams;
    if (teamModel->getLearderCharacterId() == playerChracterId) {
        return true;
    }
    if (teamModel->getSupport1CharacterId() == playerChracterId) {
        return true;
    }
    if (teamModel->getSupport2CharacterId() == playerChracterId) {
        return true;
    }
    if (teamModel->getSupport3CharacterId() == playerChracterId) {
        return true;
    }
    return false;
}

const bool AwakeCharacterSelectLayer::isSelectCharacter(const int playerCharacterId, const int materialMasterCharacterId)
{
    const auto playerCharacterModel = PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId];
    if (isTeamUseCaracter(playerCharacterId) == true) {
        return false;
    }
    if (playerCharacterModel->getLock() == 1) {
        return false;
    }
    if (materialMasterCharacterId != playerCharacterModel->getCharactersId()) {
        return false;
    }
    for (int i = 0; i < 5; i++) {
        if (_awakeModel->getMaterialPlayerCharacterIds().at(i) == playerCharacterId) {
            return false;
        }
    }
    if (playerCharacterId == _awakeModel->getBeforeCharacterData()->getPlayerCharacterId()) {
        return false;
    }

    return true;
}

void AwakeCharacterSelectLayer::scrollViewDidScroll(ScrollView* view)
{
    auto scrollBar = static_cast<ScrollBar*>(getChildByTag(TAG_SPRITE::SCROLL_BAR));
    if (scrollBar) {
        scrollBar->updateScrollPoint(view->getContentOffset().y);
    }
}

void AwakeCharacterSelectLayer::scrollViewDidZoom(ScrollView* view)
{}

void AwakeCharacterSelectLayer::onTapCharacterIcon(Ref* sender, ui::Widget::TouchEventType type, const int playerCharacterId)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        CCLOG("[AwakeCharacterSelectLayer::onTapCharacterIcon]set PlayerCharacterId=%d", playerCharacterId);
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _awakeModel->setMaterialCharacter(playerCharacterId, _selectMaterialNumber);
        _callback();
    }
}

void AwakeCharacterSelectLayer::showSortButton()
{
    auto sortButton = ui::Button::create("garage_list_sort_bt.png");
    sortButton->setTag(TAG_SPRITE::SORT_BUTTON);
    sortButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    sortButton->addTouchEventListener(CC_CALLBACK_2(AwakeCharacterSelectLayer::onTapSortButtonCallback, this));
    const Size winSize = Director::getInstance()->getWinSize();
    sortButton->setPosition(Vec2(winSize.width - 40, winSize.height - HEADER_STATUS::SIZE::HEIGHT - 44.5));
    addChild(sortButton, Z_ORDER::Z_SORT_BUTTON);

    std::string sortName = SortPlayerCharacter::getLabelName(PLAYERCONTROLLER->currentSelectedSortCharacterKey);
    if (sortName == "メーカー") {
        sortName = "メーカ";
    } else if (sortName == "お気に入り") {
        sortName = "お気に";
    }

    auto sortLabel = Label::createWithTTF(sortName, FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    sortLabel->setTag(TAG_SPRITE::SORT_LABEL);
    sortLabel->enableShadow();
    sortLabel->setPosition(sortButton->getPosition() - Vec2(sortButton->getContentSize().width / 2, 12));
    addChild(sortLabel, Z_ORDER::Z_SORT_LABEL);
}

void AwakeCharacterSelectLayer::onTapSortButtonCallback(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        CharacterSelectSortLayer* popup = CharacterSelectSortLayer::create(CC_CALLBACK_1(AwakeCharacterSelectLayer::selectSortCallback, this), PLAYERCONTROLLER->currentSelectedSortCharacterKey);
        popup->setTag(TAG_SPRITE::SORT_DIALOG);
        addChild(popup, Z_ORDER::Z_SORT_DIALOG);
    }
}

void AwakeCharacterSelectLayer::selectSortCallback(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    PLAYERCONTROLLER->currentSelectedSortCharacterKey = static_cast<SortPlayerCharacter::KEY>(static_cast<Node*>(pSender)->getTag());
    SortPlayerCharacter::sort(PLAYERCONTROLLER->currentSelectedSortCharacterKey);

    auto sortLabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::SORT_LABEL));
    sortLabel->setString(SortPlayerCharacter::getLabelName(PLAYERCONTROLLER->currentSelectedSortCharacterKey));
    if (sortLabel->getString() == "メーカー") {
        sortLabel->setString("メーカ");
    } else if (sortLabel->getString() == "お気に入り") {
        sortLabel->setString("お気に");
    }

    removeChildByTag(TAG_SPRITE::SCROLL_BAR);
    removeChildByTag(TAG_SPRITE::CHARACTER_LIST);

    showCharacterList(_materialMasterCharacterId);

    removeChildByTag(TAG_SPRITE::SORT_DIALOG);
}

