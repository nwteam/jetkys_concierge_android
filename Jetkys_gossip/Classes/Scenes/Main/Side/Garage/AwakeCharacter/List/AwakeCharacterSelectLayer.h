#ifndef __syanago__AwakeCharacterSelectLayer__
#define __syanago__AwakeCharacterSelectLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "create_func.h"
#include "AwakeModel.h"
#include "show_head_line.h"
#include "PlayerCharactersModel.h"
#include "CharacterModel.h"

USING_NS_CC;
USING_NS_CC_EXT;

class AwakeCharacterSelectLayer : public Layer, public create_func<AwakeCharacterSelectLayer>, public show_head_line, public ScrollViewDelegate
{
public:
    AwakeCharacterSelectLayer();
    ~AwakeCharacterSelectLayer();
    using create_func::create;
    typedef std::function<void()> EndCallback;
    bool init(const int materialMasterCharacterId,
              const int selectNumber,
              const std::shared_ptr<AwakeModel>& awakeModel,
              const EndCallback& callback);
    
    void scrollViewDidScroll(ScrollView *view);
    void scrollViewDidZoom(ScrollView *view);
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        CHARACTER_LIST,
        SCROLL_BAR,
        SORT_BUTTON,
        SORT_LABEL,
        SORT_DIALOG,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_CHARACTER_LIST,
        Z_SCROLL_BAR,
        Z_SORT_BUTTON,
        Z_SORT_LABEL,
        Z_SORT_DIALOG = 12,
    };
    const int kColumn = 5.0f;
    const Size kSizeCharacterIcon = Size(118.0f, 118.0f);
    
    EndCallback _callback;
    std::shared_ptr<AwakeModel> _awakeModel;
    int _selectMaterialNumber;
    int _materialMasterCharacterId;
    
    void onTapBackCallback(Ref* pSender);
    
    void showBackground();
    void showCharacterList(const int materialMasterCharacterId);
    ScrollView* createScrollView(const int holdCharacterCount, const int materialMasterCharacterId);
    LayerColor* createCharacterList(const int holdCharacterCount, const int materialMasterCharacterId);
    ui::Button* createCharacterIcon(const int playerCharacterId);
    void setCharacterViewStatus(const int playerCharacterId, ui::Button* characterIcon);
    
    const bool isTeamUseCaracter(const int playerChracterId);
    const bool isSelectCharacter(const int playerCharacterId, const int materialMasterCharacterId);
    
    void onTapCharacterIcon(Ref* sender, ui::Widget::TouchEventType type, const int playerCharacterId);
    
    void showSortButton();
    void onTapSortButtonCallback(Ref* sender, ui::Widget::TouchEventType type);
    void selectSortCallback(cocos2d::Ref *pSender);
};

#endif
