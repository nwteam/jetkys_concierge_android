#include "AwakeCharacterLayer.h"
#include "AwakeView.h"
#include "MaterialView.h"
#include "SoundHelper.h"
#include "FontDefines.h"
#include "DropEffectLayer.h"
#include "DetailLayer.h"
#include "HeaderStatus.h"
#include "ArtLtvMeasurement.h"
#include "MeasurementInformation.h"
#include "AwakeCharacterSelectLayer.h"
#include "Native.h"
#include "jCommon.h"

AwakeCharacterLayer::AwakeCharacterLayer():
    _progress(nullptr)
    , _request(nullptr)
    , _awakeModel(nullptr)
    , _awakeResponseModel(nullptr)
    , _callHeaderVisible(nullptr)
    , _endCallback(nullptr)
{
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}

AwakeCharacterLayer::~AwakeCharacterLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
    _awakeModel = nullptr;
    _callHeaderVisible = nullptr;
    _endCallback = nullptr;
}

bool AwakeCharacterLayer::init(const int playerCharacterId, const CallHeaderVisible& callHeaderVisible, const EndCallback& endCallback)
{
    if (!Layer::create()) {
        return false;
    }
    CCLOG("[AwakeCharacterLayer::init]awakeCharacterId=%d", playerCharacterId);
    _callHeaderVisible = callHeaderVisible;
    _endCallback = endCallback;

    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);


    std::shared_ptr<AwakeModel>model(new AwakeModel(playerCharacterId));
    _awakeModel = model;

    showBackground();
    showHeadLine(this, "覚醒", CC_CALLBACK_1(AwakeCharacterLayer::onTapBackButton, this));

    if (_awakeModel->getAfterCharacterData() != nullptr) {
        showCharacterBefore(_awakeModel->getBeforeCharacterData());
        showArrow();
        showCharacterAfter(_awakeModel->getAfterCharacterData());
        showMaterialCharacter(_awakeModel);
        showAttentionLabel();
        showButton();

        checkAwakeButton();
    } else {
        showCanNotAwake();
    }

    return true;
}

void AwakeCharacterLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getWinSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void AwakeCharacterLayer::showCanNotAwake()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto bgground = makeSprite("dialog_base.png");
    addChild(bgground);
    bgground->setPosition(winSize / 2);
    auto resultLabel = Label::createWithTTF("覚醒するキャラクターがありません", FONT_NAME_2, 30);
    resultLabel->setPosition(Point(winSize.width / 2, (winSize.height / 2 - 20)));
    resultLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(resultLabel);
}

void AwakeCharacterLayer::onTapBackButton(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    endAwakeCallBack();
}

void AwakeCharacterLayer::showCharacterBefore(const std::shared_ptr<AwakeModel::Character>& model)
{
    auto beforeCharacter = AwakeView::create(model);
    beforeCharacter->setTag(TAG_SPRITE::CHARACTER_BEFORE);
    beforeCharacter->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    beforeCharacter->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2,
                                      Director::getInstance()->getWinSize().height - HEADER_STATUS::SIZE::HEIGHT - beforeCharacter->getContentSize().height - 100));
    beforeCharacter->addTouchEventListener(CC_CALLBACK_2(AwakeCharacterLayer::onTapPlayerCharacterDetail, this));
    addChild(beforeCharacter, Z_ORDER::Z_CHARACTER_BEFORE);
}

void AwakeCharacterLayer::onTapPlayerCharacterDetail(Ref* sender, AwakeView::Widget::TouchEventType type)
{
    if (type == AwakeView::Widget::TouchEventType::ENDED) {
        _callHeaderVisible(false);
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        auto detailLayer = DetailLayer::create(DetailCharacterData(_awakeModel->getBeforeCharacterData()->getPlayerCharacterId()), CC_CALLBACK_0(AwakeCharacterLayer::endDetaileCallback, this));
        detailLayer->setTag(TAG_SPRITE::DETAIL_LAYER);
        addChild(detailLayer, Z_DETAIL_LAYER);
    }
}

void AwakeCharacterLayer::showArrow()
{
    auto arrow = Sprite::create("awake_arrow.png");
    arrow->setTag(TAG_SPRITE::ARROW);
    arrow->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    arrow->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2,
                            getChildByTag(TAG_SPRITE::CHARACTER_BEFORE)->getPositionY() - arrow->getContentSize().height - 6));
    addChild(arrow, Z_ORDER::Z_ARROW);
}

void AwakeCharacterLayer::showCharacterAfter(const std::shared_ptr<AwakeModel::Character>& model)
{
    auto afterCharacter = AwakeView::create(model);
    afterCharacter->setTag(TAG_SPRITE::CHARACTER_AFTER);
    afterCharacter->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    afterCharacter->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2,
                                     getChildByTag(TAG_SPRITE::ARROW)->getPositionY() - afterCharacter->getContentSize().height));
    afterCharacter->addTouchEventListener(CC_CALLBACK_2(AwakeCharacterLayer::onTapMasterCharacterDetail, this));
    addChild(afterCharacter, Z_ORDER::Z_CHARACTER_AFTER);
}

void AwakeCharacterLayer::onTapMasterCharacterDetail(Ref* sender, AwakeView::Widget::TouchEventType type)
{
    if (type == AwakeView::Widget::TouchEventType::ENDED) {
        _callHeaderVisible(false);
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        const int masterCharacterId = _awakeModel->getAfterCharacterData()->getMasterCharacterId();
        std::shared_ptr<CharacterModel>model(CharacterModel::find(masterCharacterId));
        auto detailLayer = DetailLayer::create(DetailCharacterData(masterCharacterId, 1, model->getBaseExercise(), model->getBaseReaction(), model->getBaseDecision()), CC_CALLBACK_0(AwakeCharacterLayer::endDetaileCallback, this));
        detailLayer->setTag(TAG_SPRITE::DETAIL_LAYER);
        addChild(detailLayer, Z_DETAIL_LAYER);
    }
}

void AwakeCharacterLayer::endDetaileCallback()
{
    if (getChildByTag(TAG_SPRITE::DETAIL_LAYER)) {
        removeChildByTag(TAG_SPRITE::DETAIL_LAYER);
    }
    _callHeaderVisible(true);
}

void AwakeCharacterLayer::showMaterialCharacter(const std::shared_ptr<AwakeModel>& model)
{
    auto materialView = MaterialView::create(model, CC_CALLBACK_2(AwakeCharacterLayer::onTapCharacterSelect, this));
    materialView->setTag(TAG_SPRITE::MATERIAL_CHARACTERS);
    materialView->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    materialView->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2,
                                   getChildByTag(TAG_SPRITE::CHARACTER_AFTER)->getPositionY() - materialView->getContentSize().height - 10));
    addChild(materialView, Z_ORDER::Z_MATERIAL_CHARACTERS);
}

void AwakeCharacterLayer::onTapCharacterSelect(const int masterCharacterId, const int selectNumber)
{
    if (getChildByTag(TAG_SPRITE::CHARACTER_SELECT) || _awakeModel->isSetMaterial(selectNumber) == false) {
        return;
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    auto selectLayer = AwakeCharacterSelectLayer::create(masterCharacterId, selectNumber, _awakeModel, CC_CALLBACK_0(AwakeCharacterLayer::setMaterialCharacter, this));
    selectLayer->setTag(TAG_SPRITE::CHARACTER_SELECT);
    addChild(selectLayer, Z_ORDER::Z_CHARACTER_SELECT);
}

void AwakeCharacterLayer::setMaterialCharacter()
{
    if (getChildByTag(TAG_SPRITE::CHARACTER_SELECT)) {
        removeChildByTag(TAG_SPRITE::CHARACTER_SELECT);
    }
    auto materialView = static_cast<MaterialView*>(getChildByTag(TAG_SPRITE::MATERIAL_CHARACTERS));
    materialView->refresh();
    checkAwakeButton();
}

void AwakeCharacterLayer::showAttentionLabel()
{
    auto attentionlabel = Label::createWithTTF("※覚醒後はレベル・親密度ともに初期化されます", FONT_NAME_2, 22);
    attentionlabel->setTag(TAG_SPRITE::LABEL);
    attentionlabel->setColor(Color3B::RED);
    attentionlabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    attentionlabel->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2,
                                     getChildByTag(TAG_SPRITE::MATERIAL_CHARACTERS)->getPositionY() - attentionlabel->getContentSize().height - 10));
    attentionlabel->runAction(RepeatForever::create(Sequence::create(FadeIn::create(0.4), DelayTime::create(1.2), FadeOut::create(0.4), NULL)));
    addChild(attentionlabel, Z_LABEL);
}

void AwakeCharacterLayer::showButton()
{
    auto awakeButton = ui::Button::create("awake_button.png");
    awakeButton->addTouchEventListener(CC_CALLBACK_2(AwakeCharacterLayer::onTapAwakeButton, this));
    awakeButton->setTag(TAG_SPRITE::BUTTON);
    awakeButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    awakeButton->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2,
                                  getChildByTag(TAG_SPRITE::LABEL)->getPositionY() - awakeButton->getContentSize().height + 7));
    addChild(awakeButton, Z_ORDER::Z_BUTTON);
}

void AwakeCharacterLayer::checkAwakeButton()
{
    auto awakeButton = static_cast<ui::Button*>(getChildByTag(TAG_SPRITE::BUTTON));
    if (!awakeButton) {
        CC_ASSERT("[AwakeCharacterLayer::checkAwake]error awakeButton not found");
        return;
    }

    if (_awakeModel->isAwake() == true) {
        awakeButton->setColor(Color3B::WHITE);
        awakeButton->setEnabled(true);
    } else {
        awakeButton->setColor(Color3B::GRAY);
        awakeButton->setEnabled(false);
    }
}

void AwakeCharacterLayer::onTapAwakeButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _progress->onStart();
        _request->awakeCharacter(_awakeModel->getBeforeCharacterData()->getPlayerCharacterId(),
                                 _awakeModel->getMaterialIdsString(),
                                 CC_CALLBACK_1(AwakeCharacterLayer::responseAwake, this));
    }
}

void AwakeCharacterLayer::responseAwake(Json* response)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    ArtLtvMeasurement::send(MEASURMENT_INFO::ART::AWAKE_CHARACTER, (int)PLAYERCONTROLLER->_player->getID());
    std::shared_ptr<AwakeResponseModel>model(new AwakeResponseModel(response));
    _awakeResponseModel = model;
    _callHeaderVisible(false);
    showWhiteOutLayer();
}

void AwakeCharacterLayer::showWhiteOutLayer()
{
    auto winSize = Director::getInstance()->getWinSize();
    auto whiteOut = LayerColor::create(Color4B::WHITE, winSize.width, winSize.height);
    whiteOut->setTag(TAG_SPRITE::WHITE_LAYER);
    whiteOut->setOpacity(0);
    whiteOut->runAction(Sequence::create(FadeIn::create(0.5), CallFunc::create([this]() {
        showDropLayer();
        removeChildByTag(TAG_SPRITE::WHITE_LAYER);
    }), NULL));
    addChild(whiteOut, Z_ORDER::Z_WHITE_LAYER);
}

void AwakeCharacterLayer::showDropLayer()
{
    auto dropEffectLayer = DropEffectLayer::create(DROP_CHARACTER, _awakeResponseModel->getMasterCharacterId(), true, CC_CALLBACK_0(AwakeCharacterLayer::showDetailLayer, this));
    dropEffectLayer->setTag(TAG_SPRITE::DROP_EFFECT);
    addChild(dropEffectLayer, Z_ORDER::Z_DROP_EFFECT);
}
void AwakeCharacterLayer::showBlackOutLayer()
{
    auto winSize = Director::getInstance()->getWinSize();
    auto whiteOut = LayerColor::create(Color4B::BLACK, winSize.width, winSize.height);
    whiteOut->setTag(TAG_SPRITE::BLACK_LAYER);
    addChild(whiteOut, Z_ORDER::Z_BLACK_LAYER);
}

void AwakeCharacterLayer::showDetailLayer()
{
    auto detailLayer = DetailLayer::create(DetailCharacterData(_awakeResponseModel->getPlayerCharacterId()), CC_CALLBACK_0(AwakeCharacterLayer::endAwakeCallBack, this));
    detailLayer->_gachaFlag = true;
    detailLayer->setTag(TAG_SPRITE::DETAIL_LAYER);
    addChild(detailLayer, Z_DETAIL_LAYER);

    if (!getChildByTag(TAG_SPRITE::BLACK_LAYER)) {
        return;
    }
    auto blackLayer = static_cast<LayerColor*>(getChildByTag(TAG_SPRITE::BLACK_LAYER));
    blackLayer->runAction(Sequence::create(FadeOut::create(0.5), CallFunc::create([this]() {
        if (getChildByTag(TAG_SPRITE::BLACK_LAYER)) {
            removeChildByTag(TAG_SPRITE::BLACK_LAYER);
        }
    }), NULL));
}

void AwakeCharacterLayer::endAwakeCallBack()
{
    endDetaileCallback();
    _endCallback();
    removeFromParent();
}

