#include "AwakeView.h"
#include "jCommon.h"
#include "FontDefines.h"
#include "MacroDefines.h"

AwakeView::AwakeView()
{}

AwakeView::~AwakeView()
{}

AwakeView* AwakeView::create(const std::shared_ptr<AwakeModel::Character>& model)
{
    AwakeView* btn = new (std::nothrow) AwakeView();
    if (btn && btn->init("awake_character_base.png")) {
        btn->initialize(model);
        btn->setZoomScale(0);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void AwakeView::initialize(const std::shared_ptr<AwakeModel::Character>& model)
{
    if (!model) {
        CC_ASSERT("[AwakeView::initialize]AwakeModel::Character not fond");
        return;
    }
    showIcon(model);
    showName(model);
    showRarity(model);
    showVehcleModel(model);
    showBodyTypeAndCost(model);
    showStatus(model);
}

void AwakeView::showIcon(const std::shared_ptr<AwakeModel::Character>& model)
{
    auto icon = makeSprite(StringUtils::format("%d_i.png", model->getMasterCharacterId()).c_str());
    icon->setTag(TAG_SPRITE::ICON);
    icon->setPosition(Vec2(14 + icon->getContentSize().width / 2, BASE_HEIGHT - icon->getContentSize().height / 2 - 15));
    addChild(icon, Z_ORDER::Z_ICON);
}

void AwakeView::showName(const std::shared_ptr<AwakeModel::Character>& model)
{
    auto nameLabel = Label::createWithTTF(model->getName(), FONT_NAME_2, 28);
    nameLabel->setTag(TAG_SPRITE::NAME);
    nameLabel->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    auto icon = getChildByTag(TAG_SPRITE::ICON);
    nameLabel->setPosition(icon->getPosition() + Vec2(icon->getContentSize().width / 2 + 10, icon->getContentSize().height / 2));
    nameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(nameLabel, Z_ORDER::Z_NAME);
}

void AwakeView::showRarity(const std::shared_ptr<AwakeModel::Character>& model)
{
    auto icon = getChildByTag(TAG_SPRITE::ICON);
    auto rankPoint = Point(icon->getPosition().x + (icon->getContentSize().width * 0.5) + 23, icon->getPosition().y);
    for (int i = 0; i < model->getRarity(); i++) {
        auto rarityStar = Sprite::create("other_rarity.png");
        rarityStar->setTag(TAG_SPRITE::RARITY + i);
        rarityStar->setPosition(rankPoint);
        addChild(rarityStar, Z_ORDER::Z_RARITY);
        rankPoint += Vec2(28, 0);
    }
}

void AwakeView::showVehcleModel(const std::shared_ptr<AwakeModel::Character>& model)
{
    auto vehcleModelLabel = Label::createWithTTF("型式　  " + model->getVehcleModelName(), FONT_NAME_2, 18);
    vehcleModelLabel->setTag(TAG_SPRITE::VEHICLE_MODEL);
    vehcleModelLabel->setPosition(Point(BASE_WIDTH - 22, getChildByTag(TAG_SPRITE::ICON)->getPosition().y - 9));
    vehcleModelLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    vehcleModelLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    addChild(vehcleModelLabel, Z_ORDER::Z_VEHICLE_MODEL);
}

void AwakeView::showBodyTypeAndCost(const std::shared_ptr<AwakeModel::Character>& model)
{
    auto bodyTypeBase = Sprite::create("detale_bodyType_base.png");
    bodyTypeBase->setTag(TAG_SPRITE::BODY_TYPE_AND_COST);
    bodyTypeBase->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    auto icon = getChildByTag(TAG_SPRITE::ICON);
    bodyTypeBase->setPosition(icon->getPosition() + Vec2(icon->getContentSize().width / 2 + 10, -icon->getContentSize().height / 2));
    addChild(bodyTypeBase, Z_ORDER::Z_BODY_TYPE_AND_COST);

    auto bodyTypeLabel = Label::createWithTTF(model->getBodyTypeName(), FONT_NAME_2, 22);
    bodyTypeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    bodyTypeLabel->setPosition(Vec2(150, bodyTypeBase->getContentSize().height / 2 - 11));
    bodyTypeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    bodyTypeBase->addChild(bodyTypeLabel);

    auto costLabel = Label::createWithTTF(StringUtils::format("%d", model->getCost()), FONT_NAME_2, 22);
    costLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    costLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    costLabel->setPosition(Vec2(bodyTypeBase->getContentSize().width - 5, bodyTypeLabel->getPosition().y));
    bodyTypeBase->addChild(costLabel);
}

void AwakeView::showStatus(const std::shared_ptr<AwakeModel::Character>& model)
{
    auto power = createStatusView("馬力", model->getPower(), TAG_SPRITE::POWER);
    power->cocos2d::Node::setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    power->setPosition(Point(14, 15 + power->getContentSize().height / 2));
    addChild(power, Z_ORDER::Z_POWER);

    auto technique = createStatusView("技量", model->getTechnique(), TAG_SPRITE::TECHNIQUE);
    technique->setPosition(Point(BASE_WIDTH / 2, power->getPositionY()));
    addChild(technique, Z_ORDER::Z_TECHNIQUE);

    auto brake = createStatusView("忍耐", model->getBrake(), TAG_SPRITE::BRAKE);
    brake->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    brake->setPosition(Point(BASE_WIDTH - 14, power->getPositionY()));
    addChild(brake, Z_ORDER::Z_BRAKE);
}

Sprite* AwakeView::createStatusView(const std::string statsuType, const int statusValue, const int tag)
{
    auto result = Sprite::create("detale_status_base.png");
    result->setTag(tag);

    auto statusLabel = Label::createWithTTF(statsuType, FONT_NAME_2, 22);
    statusLabel->setPosition(Point(5 + statusLabel->getContentSize().width / 2, result->getContentSize().height / 2 - 11));
    statusLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->addChild(statusLabel);

    auto statusValueLabel = Label::createWithTTF(FormatWithCommas(statusValue), FONT_NAME_2, 22);
    result->addChild(statusValueLabel);
    statusValueLabel->setPosition(Point(result->getContentSize().width - statusValueLabel->getContentSize().width / 2 - 5, result->getContentSize().height / 2 - 11));
    statusValueLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    return result;
}