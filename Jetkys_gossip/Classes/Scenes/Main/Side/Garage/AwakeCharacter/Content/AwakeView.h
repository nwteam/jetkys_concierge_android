#ifndef __syanago__AwakeView__
#define __syanago__AwakeView__

#include "cocos2d.h"
#include "AwakeModel.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

enum AWAKE_VIEW_TYPE{
    BEFORE,
    AFTER,
};

class AwakeView : public ui::Button
{
public:
    AwakeView();
    ~AwakeView();
    
    static AwakeView* create(const std::shared_ptr<AwakeModel::Character>& model);
    
private:
    enum TAG_SPRITE{
        ICON = 0,
        NAME,
        RARITY,// RARITY_TAG = 2 ~ 6
        VEHICLE_MODEL = 7,
        BODY_TYPE_AND_COST,
        POWER,
        TECHNIQUE,
        BRAKE,
    };
    enum Z_ORDER{
        Z_ICON = 0,
        Z_NAME,
        Z_RARITY,
        Z_VEHICLE_MODEL,
        Z_BODY_TYPE_AND_COST,
        Z_POWER,
        Z_TECHNIQUE,
        Z_BRAKE,
    };
    const float BASE_HEIGHT = 176;
    const float BASE_WIDTH = 612;
    
    void initialize(const std::shared_ptr<AwakeModel::Character>& model);
    
    void showIcon(const std::shared_ptr<AwakeModel::Character>& model);
    void showName(const std::shared_ptr<AwakeModel::Character>& model);
    void showRarity(const std::shared_ptr<AwakeModel::Character>& model);
    void showVehcleModel(const std::shared_ptr<AwakeModel::Character>& model);
    void showBodyTypeAndCost(const std::shared_ptr<AwakeModel::Character>& model);
    void showStatus(const std::shared_ptr<AwakeModel::Character>& model);
    Sprite* createStatusView(const std::string statsuType, const int statusValue, const int tag);
    
    
};

#endif
