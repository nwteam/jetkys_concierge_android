#include "MaterialView.h"
#include "jCommon.h"
#include "ui/UIWidget.h"

MaterialView::MaterialView():
    _selectCallBack(nullptr)
    , _awakeModel(nullptr)
{}

MaterialView::~MaterialView()
{
    _selectCallBack = nullptr;
    _awakeModel = nullptr;
}

MaterialView* MaterialView::create(const std::shared_ptr<AwakeModel>& model, const OnTapSelectCallback& selectCallback)
{
    MaterialView* btn = new (std::nothrow)  MaterialView();
    if (btn && btn->initWithFile("awake_material_base.png")) {
        btn->initialize(model, selectCallback);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void MaterialView::initialize(const std::shared_ptr<AwakeModel>& model, const OnTapSelectCallback& selectCallback)
{
    _selectCallBack = selectCallback;
    _awakeModel = model;

    showMaterialCharacter(model);
}

void MaterialView::showMaterialCharacter(const std::shared_ptr<AwakeModel>& model)
{
    Vec2 basePosition = Vec2(12, 10);
    for (int i = 0; i < 5; i++) {
        auto materialButton = createMaterialButton(model, i);
        materialButton->setPosition(basePosition);
        addChild(materialButton, Z_ORDER::Z_BUTTON);
        basePosition += Vec2(materialButton->getContentSize().width + 15, 0);
    }
}

ui::Button* MaterialView::createMaterialButton(const std::shared_ptr<AwakeModel>& model, const int viewNumber)
{
    if (viewNumber < 0 || 4 < viewNumber) {
        CC_ASSERT("[MaterialView::createMaterialButton]error viewNumber is not found");
        return nullptr;
    }
    ui::Button* result;
    if (model->getMaterialMasterCharacterIds().at(viewNumber) == 0) {
        result = ui::Button::create("awake_non_character_icon.png");
        result->setEnabled(false);
    } else {
        result = ui::Button::create(StringUtils::format("%d_i.png", model->getMaterialMasterCharacterIds().at(viewNumber)),
                                    StringUtils::format("%d_i.png", model->getMaterialMasterCharacterIds().at(viewNumber)),
                                    StringUtils::format("%d_i.png", model->getMaterialMasterCharacterIds().at(viewNumber)),
                                    ui::Button::TextureResType::PLIST);
    }
    result->addTouchEventListener(CC_CALLBACK_2(MaterialView::onTapCharacterButton, this, viewNumber));
    result->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    result->setTag(TAG_SPRITE::BUTTON + viewNumber);

    if (model->getMaterialPlayerCharacterIds().at(viewNumber) == 0) {
        result->setOpacity(100);
    }
    return result;
}

void MaterialView::onTapCharacterButton(Ref* sender, ui::Widget::TouchEventType type, const int selectNumber)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        if (selectNumber < 0 || 4 < selectNumber) {
            CC_ASSERT("[MaterialView::onTapCharacterButton]error selectNumber is not found");
            return;
        }
        _selectCallBack(_awakeModel->getMaterialMasterCharacterIds().at(selectNumber), selectNumber);
    }
}

void MaterialView::refresh()
{
    for (int i = 0; i < 5; i++) {
        if (!getChildByTag(TAG_SPRITE::BUTTON + i)) {
            continue;
        }
        removeChildByTag(TAG_SPRITE::BUTTON + i);
    }
    showMaterialCharacter(_awakeModel);
}