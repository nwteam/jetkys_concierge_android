#ifndef __syanago__MaterialView__
#define __syanago__MaterialView__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "AwakeModel.h"

USING_NS_CC;

class MaterialView : public Sprite
{
public:
    typedef std::function<void(const int, const int)> OnTapSelectCallback;
    MaterialView();
    ~MaterialView();
    
    static MaterialView* create(const std::shared_ptr<AwakeModel>& model, const OnTapSelectCallback& selectCallback);
    
    void refresh();
private:
    enum TAG_SPRITE{
        BUTTON = 0,// 0 ~ 4
    };
    enum Z_ORDER{
        Z_BUTTON = 0,
    };
    const float BASE_HIGHT = 146.0f;
    
    OnTapSelectCallback _selectCallBack;
    std::shared_ptr<AwakeModel> _awakeModel;
    
    void initialize(const std::shared_ptr<AwakeModel>& model, const OnTapSelectCallback& selectCallback);
    
    void showMaterialCharacter(const std::shared_ptr<AwakeModel>& model);
    ui::Button* createMaterialButton(const std::shared_ptr<AwakeModel>& model, const int viewNumber);
    void onTapCharacterButton(Ref* sender, ui::Widget::TouchEventType type, const int selectNumber);
};

#endif
