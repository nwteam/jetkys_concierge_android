#ifndef __syanago__CompositionRarityAlertForParts__
#define __syanago__CompositionRarityAlertForParts__

#include "LayerPriority.h"
#include "PopUpDelegate.h"
#include "create_func.h"
#include <functional>

#define kPriorityCompositionRarityAlertForParts -650
#define kPriorityCompositionRarityAlertForPartsMenu kPriorityCompositionRarityAlertForParts - 2

class CompositionRarityAlertForParts : public LayerPriority, create_func<CompositionRarityAlertForParts> {
public:
    using create_func::create;
    typedef std::function<void()> DialogCallback;
    bool init(std::vector<int> pcIndex, const DialogCallback& callback);
};

#endif /* defined(__syanago__CompositionRarityAlertForParts__) */
