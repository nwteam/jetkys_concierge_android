#ifndef __syanago__CompositionCharacterSelectLayer__
#define __syanago__CompositionCharacterSelectLayer__

#include "SyanagoBaseLayer.h"
#include "CharacterSelectSortLayer.h"

#define tag_predict_ok_bt 123
#define tag_predict_reset_bt 321

class CompositionCharacterSelectLayer : public SyanagoBaseLayer
{
public:
    CompositionCharacterSelectLayer();
    virtual ~CompositionCharacterSelectLayer();
    
    bool init();
    CREATE_FUNC(CompositionCharacterSelectLayer);
    
private:
    enum TAG_LAYER {
        CHARACTER_SORT = 0
    };
    void sortCharacter();
    void updateSortLabel();
    
    void btMenuItemCallback(cocos2d::Ref *pSender);
    void btItemSelectedCallback(Ref *pSender);
    
    void scrollViewDidScroll(ScrollView* view);
    void scrollViewDidZoom(ScrollView* view);
    
    void addBage(MenuItemSprite *item, int i);
    void showInformationLayer();
    void menuItemPredictCallback(Ref *pSender);
    void predictSelected();
    
    void showDatas();
    
    int indexOfpcIDSelectedTemp(int pcID);
    
    CC_SYNTHESIZE(int, _pcSelectedIndex, PCSelectecedIndex);
    std::vector<int> _pcIDSelectedTemp;
    
    void characterSortButtonCallback(Ref* pSender);
    
    
    
    LayerPriority *_informationLayer;
    Label *_coinValue;
    Label *_expValue;
    Label *_carryValue;
};

#endif /* defined(__syanago__CompositionCharacterSelectLayer__) */
