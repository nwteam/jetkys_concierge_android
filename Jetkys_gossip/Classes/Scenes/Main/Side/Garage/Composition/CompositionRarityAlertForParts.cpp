#include "CompositionRarityAlertForParts.h"
#include "PlayerController.h"
#include "FontDefines.h"
#include "StringDefines.h"
#include "jCommon.h"
#include "SoundHelper.h"
#include "CharacterModel.h"


bool CompositionRarityAlertForParts::init(std::vector<int>pcIndex, const DialogCallback& callback)
{
    if (!LayerPriority::initWithPriority(kPriorityCompositionRarityAlertForParts)) {
        return false;
    }
    Size winSize = Director::getInstance()->getWinSize();

    auto background = LayerColor::create(Color4B(0, 0, 0, 150), winSize.width, winSize.height);
    addChild(background);

    auto board = makeSprite("garage_sell_popup_bg.png");
    board->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(board);

    auto titleLabel = Label::createWithTTF("合成します", FONT_NAME_2, 30, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    titleLabel->setPosition(Vec2(winSize.width / 2, board->getPositionY() + board->getContentSize().height / 2 - 30 - titleLabel->getContentSize().height / 2 - 15));
    addChild(titleLabel);

    int count = (int)pcIndex.size();
    int rarityCount = 0;
    Sprite* image;
    auto imageLayer = Layer::create();

    for (int i = 0; i < count; i++) {
        // img player_character
        if (pcIndex.at(i) >= 0) {
            PlayerCharactersModel* playerCharactersModel = PLAYERCONTROLLER->_playerCharactersSorted.at(pcIndex.at(i));

            if (playerCharactersModel->isAnyPartsEquiped()) {
                rarityCount++;

                image = makeSprite(StringUtils::format(IMAGE_CHARACTER_ICON, playerCharactersModel->getCharactersId()).c_str());
                imageLayer->addChild(image);
                image->setScale(0.95f);
                image->setPosition(titleLabel->getPosition() + Vec2(((rarityCount - 1) * (image->getContentSize().width * image->getScale() + 10)),
                                                                    -titleLabel->getContentSize().height / 2 - image->getContentSize().height / 2 * image->getScale() + 5));

                int level = playerCharactersModel->getLevel();
                auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %d", level), FONT_NAME_2, 16);
                image->addChild(levelLabel);
                levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
                levelLabel->setPosition(Vec2(image->getContentSize().width / 2 * image->getScale(), 6 - 8));
                levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
            }
        }
    }
    imageLayer->setPosition(imageLayer->getPosition() + Vec2(-((rarityCount - 1) * (image->getContentSize().width * image->getScale() + 10) / 2), 0));
    addChild(imageLayer);

    auto messLabel = Label::createWithTTF("合成する車なごの中に", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    messLabel->setColor(COLOR_YELLOW);
    messLabel->setAnchorPoint(Vec2(0.5, 1));
    messLabel->setPosition(Vec2(winSize.width / 2,
                                image->getPositionY() - image->getContentSize().height / 2 - 20));
    addChild(messLabel);

    auto messLabel2 = Label::createWithTTF("装備が含まれています。", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    messLabel2->setColor(COLOR_YELLOW);
    messLabel2->setAnchorPoint(Vec2(0.5, 1));
    messLabel2->setPosition(Vec2(winSize.width / 2,
                                 messLabel->getPositionY() - messLabel->getContentSize().height / 2 - 5));
    addChild(messLabel2);

    auto messLabel3 = Label::createWithTTF("装備は売られてしまいますが、", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    messLabel3->setColor(COLOR_YELLOW);
    messLabel3->setAnchorPoint(Vec2(0.5, 1));
    messLabel3->setPosition(Vec2(winSize.width / 2,
                                 messLabel2->getPositionY() - messLabel2->getContentSize().height / 2 - 5));
    addChild(messLabel3);

    auto messLabel4 = Label::createWithTTF("合成してよろしいですか？", FONT_NAME_2, 26, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    messLabel4->setColor(COLOR_YELLOW);
    messLabel4->setAnchorPoint(Vec2(0.5, 1));
    messLabel4->setPosition(Vec2(winSize.width / 2,
                                 messLabel3->getPositionY() - messLabel3->getContentSize().height / 2 - 5));
    addChild(messLabel4);

    auto menu = MenuPriority::createWithPriority(kPriorityCompositionRarityAlertForPartsMenu);
    auto button = makeMenuItem("garage_sell_button.png", "はい", [this, callback](Ref* pSender) {
        removeFromParent();
        callback();
    });
    button->setTag(PU_BT_2);
    menu->addChild(button);

    button = makeMenuItem("garage_sell_button.png", "いいえ", [this](Ref* pSender) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        removeFromParent();
    });
    button->setTag(PU_BT_CANCEL);
    menu->addChild(button);
    addChild(menu);

    menu->alignItemsHorizontallyWithPadding(50);
    menu->setPosition(Vec2(winSize.width / 2, board->getPositionY() - board->getContentSize().height / 2 + 30 + button->getNormalImage()->getContentSize().height / 2));

    return true;
}











