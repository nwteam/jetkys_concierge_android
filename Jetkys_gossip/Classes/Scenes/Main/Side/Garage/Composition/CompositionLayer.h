#ifndef __syanago__CompsitionLayer__
#define __syanago__CompsitionLayer__

#include "SyanagoBaseLayer.h"
#include "DefaultProgress.h"
#include "create_func.h"
#include "PlayerCharactersModel.h"
#include "GarageMainLayer.h"
#include "show_head_line.h"

#define kTagIndexOfMarterialID 100

USING_NS_CC;
using namespace SyanagoAPI;

class CompositionLayer : public Layer, public create_func<CompositionLayer>, public show_head_line
{
public:
    CompositionLayer();
    ~CompositionLayer();
    
    using create_func::create;
    bool init(int plyaerCharacterIndex, GarageMainLayer* garageMainLayer);
    
    CC_SYNTHESIZE(bool, _isSuccess, IsSuccess);
    
    static std::vector<int> _materialIDs;
    static int _materialIndex;
    static int _pcIndex;
    
private:
    void showBackground();
    void showCharacterInformations();
    void showSelectMaterialPanelAndExpPanel();
    void showCompositionbuttons();
    
    void showSkipMenu();
    
    void onTapBackButton(Ref* pSender);
    void onTapCompositionButton(Ref* pSender);
    void onTapMultipleCompositionButton(Ref* pSender);
    void onTapMaterial(Ref* pSender);
    
    void levelUpPredict(int64_t expPredictInc);
    void levelUpSuccess();
    void showSuccessLabel();
    void updateExpProgress(float dt);
    
    int countNumberMaterial();
    
    void onTapConfirmWithRareCharacterButton();
    void onTapConfirmWithRarePartsButton();
    
    void composition();
    bool checkRarity();
    bool checkParts();
    
    void retryConposi(Ref* pSender);
    
    void addseikou();
    void runUplevelAnimation();
    
    void responseSyntheticRequest(Json* response);
    
    GarageMainLayer* _garageMainLayer;
    
    Label *_levelLabel;
    Label *_exerciceLabel;
    Label *_reactionLabel;
    Label *_decisionLabel;
    
    Label *_exercisePlusLabel;
    Label *_reactionPlusLabel;
    Label *_decisionPlusLabel;
    Label *_beyondExpLabel;
    
    Label *_realExpLabel;
    Label * _maxExpLabel;
    
    ProgressTimer *_expProgress;
    ProgressTimer *_expPredictProgress;
    
    Label *_coinLabel;
    Label *_expInsLabel;
    Menu *_menuMaterial;
    
    Sprite *_information_board;
    Sprite *_material_board;
    Sprite *_successLabel;
    Menu *_menu;
    MenuItemSprite* _sortBt;
    
    int _levelBefore;
    int _expBefore;
    bool _isLevelUp;
    bool _isGreat;
    
    int _viewMasterCharacterId;
    
    Layer* _container;
    Label* _sortLabel;
    
    DefaultProgress* _progress;
    RequestAPI* _request;
    PlayerCharactersModel* _playerCharactersModel;
    
    EventListenerTouchOneByOne* _listener;
};

#endif /* defined(__syanago__CompsitionLayer__) */
