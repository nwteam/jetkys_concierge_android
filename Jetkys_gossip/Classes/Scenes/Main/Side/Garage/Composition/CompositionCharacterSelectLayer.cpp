#include "CompositionCharacterSelectLayer.h"
#include "TeamEditMainLayer.h"
#include "PlayerController.h"
#include "CompositionLayer.h"
#include "GarageMainLayer.h"
#include "CompositionDefines.h"
#include "StringDefines.h"
#include "SoundHelper.h"
#include "CharacterModel.h"
#include "SystemSettingModel.h"
#include "CharacterIconSprite.h"

CompositionCharacterSelectLayer::~CompositionCharacterSelectLayer()
{
    _pcIDSelectedTemp.clear();
}

CompositionCharacterSelectLayer::CompositionCharacterSelectLayer():
    _pcSelectedIndex(-1)
    , _pcIDSelectedTemp(std::vector<int>(6, -1))
    , _informationLayer(nullptr)
{
    _title = "素材選択";
}

bool CompositionCharacterSelectLayer::init()
{
    if (!SyanagoBaseLayer::init()) {
        return false;
    }

    if (PLAYERCONTROLLER->_playerCharactersSorted.size() == 0) {
        PLAYERCONTROLLER->resetPlayerCharacterSorted();
    }

    showSortMenu();
    sortCharacter();
    updateSortLabel();

    _numberOfCharacter = (int)PLAYERCONTROLLER->_playerCharactersSorted.size();
    SyanagoBaseLayer::showDatas();

    showDatas();

    if (CompositionLayer::_materialIndex == -1) {
        showInformationLayer();
    }
    return true;
}

void CompositionCharacterSelectLayer::sortCharacter()
{
    for (int i = 0; i < CompositionLayer::_materialIDs.size(); i++) {
        int pcID = -1;
        if (CompositionLayer::_materialIDs.at(i) >= 0) {
            pcID = PLAYERCONTROLLER->_playerCharactersSorted.at(CompositionLayer::_materialIDs.at(i))->getID();
        }
        _pcIDSelectedTemp.at(i) = pcID;
    }
    _pcIDSelectedTemp.at(CompositionLayer::_materialIDs.size()) = PLAYERCONTROLLER->_playerCharactersSorted.at(CompositionLayer::_pcIndex)->getID();
    SortPlayerCharacter::sort(PLAYERCONTROLLER->currentSelectedSortCharacterKey);
}

void CompositionCharacterSelectLayer::updateSortLabel()
{
    _sortLabel->setString(SortPlayerCharacter::getLabelName(PLAYERCONTROLLER->currentSelectedSortCharacterKey));
    if (_sortLabel->getString() == "メーカー") {
        _sortLabel->setString("メーカ");
    } else if (_sortLabel->getString() == "お気に入り") {
        _sortLabel->setString("お気に");
    }
}

void CompositionCharacterSelectLayer::showDatas()
{
    std::vector<int>pcIdsUsed = PLAYERCONTROLLER->getPlayerCharacterIdsUsed();
    bool checkEmpty = false;
    if (CompositionLayer::_materialIndex != -1) {
        checkEmpty = true;
        _numberOfCharacter++;
    }

    if (_informationLayer && _scrollView->getContentSize().height > _scrollView->getViewSize().height - _informationLayer->getContentSize().height) {
        _scrollView->setContentSize(Size(_scrollView->getContentSize().width, _scrollView->getContentSize().height + _informationLayer->getContentSize().height));
        _scrollView->getContainer()->setPositionY(_scrollView->getContainer()->getPositionY() - _informationLayer->getContentSize().height);
        _container->setPositionY(_container->getPositionY() + _informationLayer->getContentSize().height);

        if (_scrollBar) {
            _scrollBar->removeFromParent();
        }
        Size winSize = Director::getInstance()->getWinSize();
        _scrollBar = ScrollBar::initScrollBar(_scrollView->getViewSize().height - 30, _scrollView->getViewSize().height, _scrollView->getContainer()->getContentSize().height, _scrollView->minContainerOffset().y);
        addChild(_scrollBar);
        _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 75));
        _scrollBar->updateScrollPoint(_scrollView->getContentOffset().y);
    }

    auto menu = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    menu->setPosition(Vec2(0, _container->getContentSize().height));
    menu->_scrollView = _scrollView;
    _container->addChild(menu, 2);

    int pcIndex = 0;
    for (int i = 0; i < _numberOfCharacter; i++) {
        int j = i % kColumn;
        int k = i / kColumn;

        if (i == 0 && checkEmpty == true) {
            auto imageFrame = makeMenuItem("garage_icon_empty.png", CC_CALLBACK_1(CompositionCharacterSelectLayer::btItemSelectedCallback, this));
            imageFrame->setPosition(Vec2(kSizeCharacterIcon.width * (0.5f + j), -kSizeCharacterIcon.height * (0.5f + k)));
            imageFrame->setTag(kPlayerCharacterTagEmpty);
            menu->addChild(imageFrame);
            continue;
        }
        CharacterIconSprite* imageFrame = CharacterIconSprite::create(pcIndex, CC_CALLBACK_1(CompositionCharacterSelectLayer::btItemSelectedCallback, this));
        menu->addChild(imageFrame);

        int aPcId = PLAYERCONTROLLER->_playerCharactersSorted.at(pcIndex)->getID();
        int aPcIdIndex = indexOfpcIDSelectedTemp(aPcId);

        if (aPcIdIndex != -1 && aPcIdIndex != _pcIDSelectedTemp.size() - 1) {
            addBage(imageFrame, aPcIdIndex);
        } else {
            imageFrame->showStatusLabel(pcIndex);
        }

        if (aPcIdIndex != -1 && aPcIdIndex == _pcIDSelectedTemp.size() - 1) {
            CompositionLayer::_pcIndex = pcIndex;
        } else if (aPcIdIndex != -1) {
            CompositionLayer::_materialIDs.at(aPcIdIndex) = pcIndex;
        }

        if (aPcIdIndex == _pcIDSelectedTemp.size() - 1 ||
            imageFrame->checkSyanagoIsNew() == CharacterIconSprite::USED_STATUS::USED  ||
            PLAYERCONTROLLER->_playerCharactersSorted.at(pcIndex)->getLock() == 1) {
            imageFrame->setEnabled(false);
        }
        pcIndex++;
    }
}


void CompositionCharacterSelectLayer::addBage(MenuItemSprite* item, int i)
{
    auto bage = makeSprite("garage_select_point.png");
    _container->addChild(bage, 5);
    bage->setPosition(_container->convertToNodeSpace(item->getParent()->convertToWorldSpace(item->getPosition())));
    bage->setTag(kPlayerBageTagStart + i);

    auto bage_label = Label::createWithTTF(StringUtils::format("%d", i + 1), FONT_NAME_2, 18, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    bage_label->enableShadow();
    bage->addChild(bage_label, 5);
    bage_label->setPosition(Vec2(bage->getContentSize().width / 2, bage->getContentSize().height / 2 - 9));
}

void CompositionCharacterSelectLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    int tag = ((Node*)pSender)->getTag();
    if (tag == bt_sort_bt) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        CharacterSelectSortLayer* popup = CharacterSelectSortLayer::create(CC_CALLBACK_1(CompositionCharacterSelectLayer::characterSortButtonCallback, this), PLAYERCONTROLLER->currentSelectedSortCharacterKey);
        popup->setTag(TAG_LAYER::CHARACTER_SORT);
        addChild(popup, ZORDER_POPUP);
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        dynamic_cast<GarageMainLayer*>(_mainLayer)->showLayer(GLI_COMPOSITION);
    }
}

void CompositionCharacterSelectLayer::btItemSelectedCallback(cocos2d::Ref* pSender)
{
    if (checkMenuReturn()) {
        return;
    }

    auto item ((MenuItemSprite*)pSender);
    int tag = item->getTag();
    _pcSelectedIndex = tag - kPlayerCharacterTagStart;
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    if (static_cast<MenuItemSprite*>(pSender)->getChildByTag(kPlayerCharacterTagStart + _pcSelectedIndex)) {
        static_cast<MenuItemSprite*>(pSender)->removeChildByTag(kPlayerCharacterTagStart + _pcSelectedIndex);
        PLAYERCONTROLLER->_playerCharactersSorted.at(_pcSelectedIndex)->saveIsNew(0);
    }

    // save _pcSelectedIndex
    if (CompositionLayer::_materialIndex < 0) {
        // multiselect
        int i = 0;
        int j = -1;
        for (i = 0; i < CompositionLayer::_materialIDs.size(); i++) {
            if (CompositionLayer::_materialIDs.at(i) < 0) {
                if (j == -1) {
                    j = i; // number of empty -> add
                }
            }
            if (CompositionLayer::_materialIDs.at(i) == _pcSelectedIndex) {
                // exist
                j = i;
                break;
            }
        }
        if (j == i) {
            // exist
            ////CCLOG("exist->removed");
            CompositionLayer::_materialIDs.at(j) = -1;
            _container->removeChildByTag(kPlayerBageTagStart + j);

            predictSelected();
        } else if (j != -1) {
            // add
            ////CCLOG("added");
            CompositionLayer::_materialIDs.at(j) = _pcSelectedIndex < 0 ? -1 : _pcSelectedIndex;
            addBage(item, j);

            predictSelected();
        } else {
            // full
            ////CCLOG("full slot");
        }
    } else {
        // one select
        // Check select other pc exist in _materialIDs
        if (_pcSelectedIndex >= 0) {
            int pcID = PLAYERCONTROLLER->_playerCharactersSorted.at(_pcSelectedIndex)->getID();
            int aPcIdIndex = indexOfpcIDSelectedTemp(pcID);
            if (aPcIdIndex >= 0) {
                CompositionLayer::_materialIDs.at(aPcIdIndex) = -1;
            }
        }

        CompositionLayer::_materialIDs.at(CompositionLayer::_materialIndex) = _pcSelectedIndex < 0 ? -1 : _pcSelectedIndex;
        dynamic_cast<GarageMainLayer*>(_mainLayer)->showLayer(GLI_COMPOSITION);
    }

    _pcSelectedIndex = -1;
}

void CompositionCharacterSelectLayer::scrollViewDidScroll(ScrollView* view)
{
    _scrollBar->updateScrollPoint(view->getContentOffset().y);
}

void CompositionCharacterSelectLayer::scrollViewDidZoom(ScrollView* view)
{}





int CompositionCharacterSelectLayer::indexOfpcIDSelectedTemp(int pcID)
{
    for (int i = 0; i < _pcIDSelectedTemp.size(); i++) {
        if (_pcIDSelectedTemp.at(i) == pcID) {
            return i;
        }
    }
    return -1;
}

void CompositionCharacterSelectLayer::showInformationLayer()
{
    Size winSize = Director::getInstance()->getWinSize();

    _informationLayer = LayerPriority::createWithPriority(kPrioritySyanagoBaseSaleMenu + 1);
    _informationLayer->setContentSize(Size(winSize.width, 170));
    addChild(_informationLayer, 100);

    _informationLayer->_touchListener->onTouchBegan = [this](Touch* touch, Event* event) {
                                                          if (Rect(0, 0, _informationLayer->getContentSize().width, _informationLayer->getContentSize().height).containsPoint(_informationLayer->convertToNodeSpace(touch->getLocation()))) {
                                                              ////CCLOG("Predict Touch %f, %f", touch->getLocation().x, touch->getLocation().y);
                                                              return true;
                                                          }
                                                          return false;
                                                      };

    auto board = makeSprite("garage_sell_board.png");
    _informationLayer->addChild(board);
    board->setPosition(Vec2(winSize.width / 2,
                            _informationLayer->getContentSize().height - board->getContentSize().height / 2 + 50));

    int coinPredict = 999;
    int expPredict = 999999;
    int carryPredict = (int)PLAYERCONTROLLER->_playerCharacterModels.size();
    int carryMax = PLAYERCONTROLLER->_player->getGarageSize();


    // Info
    auto predictLabel = Label::createWithTTF("必要なコイン", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    _informationLayer->addChild(predictLabel, 1);
    predictLabel->setAnchorPoint(Vec2(0, 0.5));
    predictLabel->enableShadow();
    predictLabel->setColor(COLOR_YELLOW);
    predictLabel->setPosition(board->getPosition() + Vec2(-board->getContentSize().width / 2 + 20, 40 - 12));

    _coinValue = Label::createWithTTF(FormatWithCommas(coinPredict), FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _informationLayer->addChild(_coinValue, 1);
    _coinValue->setAnchorPoint(Vec2(1, 0.5));
    _coinValue->enableShadow();
    _coinValue->setColor(COLOR_YELLOW);
    _coinValue->setPosition(predictLabel->getPosition() + Vec2(355, 0));

    predictLabel = Label::createWithTTF("得られる経験値", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    _informationLayer->addChild(predictLabel, 1);
    predictLabel->setAnchorPoint(Vec2(0, 0.5));
    predictLabel->enableShadow();
    predictLabel->setPosition(board->getPosition() + Vec2(-board->getContentSize().width / 2 + 20, -12));

    _expValue = Label::createWithTTF(FormatWithCommas(expPredict), FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _informationLayer->addChild(_expValue, 1);
    _expValue->setAnchorPoint(Vec2(1, 0.5));
    _expValue->enableShadow();
    _expValue->setPosition(predictLabel->getPosition() + Vec2(355, 0));

    predictLabel = Label::createWithTTF("所持数", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    _informationLayer->addChild(predictLabel, 1);
    predictLabel->setAnchorPoint(Vec2(0, 0.5));
    predictLabel->enableShadow();
    predictLabel->setPosition(board->getPosition() + Vec2(-board->getContentSize().width / 2 + 20, -12 - 40));

    _carryValue = Label::createWithTTF(StringUtils::format("%3d/%3d", carryPredict, carryMax), FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _informationLayer->addChild(_carryValue, 1);
    _carryValue->setAnchorPoint(Vec2(1, 0.5));
    _carryValue->enableShadow();
    _carryValue->setPosition(predictLabel->getPosition() + Vec2(355, 0));

    // Menu
    auto menu = MenuPriority::createWithPriority(kPrioritySyanagoBaseSaleMenu);
    _informationLayer->addChild(menu);
    menu->setPosition(board->getPosition() + Vec2(197, 0));

    auto button = makeMenuItem("garage_sell_button.png", CC_CALLBACK_1(CompositionCharacterSelectLayer::menuItemPredictCallback, this));
    menu->addChild(button);
    button->setTag(tag_predict_ok_bt);

    auto label = Label::createWithTTF("OK", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    button->addChild(label);
    label->setPosition(Vec2(button->getNormalImage()->getContentSize().width / 2, button->getNormalImage()->getContentSize().height / 2 - 12));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    button = makeMenuItem("garage_sell_button.png", CC_CALLBACK_1(CompositionCharacterSelectLayer::menuItemPredictCallback, this));
    menu->addChild(button);
    button->setTag(tag_predict_reset_bt);

    label = Label::createWithTTF("クリア", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    button->addChild(label);
    label->setPosition(Vec2(button->getNormalImage()->getContentSize().width / 2, button->getNormalImage()->getContentSize().height / 2 - 12));

    menu->alignItemsVerticallyWithPadding(20);

    predictSelected();
}

void CompositionCharacterSelectLayer::predictSelected()
{
    int64_t expPredictIncTotal = 0;
    int coinPredictTotal = 0;

    for (int i = 0; i < CompositionLayer::_materialIDs.size(); i++) {
        if (CompositionLayer::_materialIDs.at(i) >= 0) {
            // calc predict exp increase
            int aPcIndex = CompositionLayer::_materialIDs.at(i);
            int level = PLAYERCONTROLLER->_playerCharactersSorted.at(aPcIndex)->getLevel();
            std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(PLAYERCONTROLLER->_playerCharactersSorted.at(aPcIndex)->getCharactersId()));
            int rarity = characterModel->getRarity();

            std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
            auto mixValue1 = systemSettingModel->getMixedCoefficient1();
            if (mixValue1 == 0) {
                mixValue1 = kCompositionCompressRate;
            }
            auto mixValue2 = systemSettingModel->getMixedCoefficient2();
            if (mixValue2 == 0) {
                mixValue2 = kCompositionCompositedCase;
            }
            auto baseCoin = systemSettingModel->getCoinOfSynthesis();
            if (baseCoin == 0) {
                baseCoin = kCompositionCoinCompsumtion;
            }
            int64_t expPredict = ((int64_t)mixValue1) * rarity * level + mixValue2 * rarity;
            int coinComp = 1500 + ((600 * (rarity + 1)) + (((30 + rarity) * level) * rarity));
            // int coinComp = baseCoin * rarity;
            ////CCLOG("calcExpIncPredict: pcIndex %d, rarity %d, level %d, expPredict %lld, coin: %d", aPcIndex, rarity, level, expPredict, coinComp);

            expPredictIncTotal += expPredict;
            coinPredictTotal += coinComp;
        }
    }
    _coinValue->setString(FormatWithCommas(coinPredictTotal));
    _expValue->setString(FormatWithCommas((int)expPredictIncTotal));
}

void CompositionCharacterSelectLayer::menuItemPredictCallback(Ref* pSender)
{
    int tag = ((Node*)pSender)->getTag();
    if (tag == tag_predict_ok_bt) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        dynamic_cast<GarageMainLayer*>(_mainLayer)->showLayer(GLI_COMPOSITION);
    } else if (tag == tag_predict_reset_bt) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        _coinValue->setString("0");
        _expValue->setString("0");
        for (int i = 0; i < CompositionLayer::_materialIDs.size(); i++) {
            if (CompositionLayer::_materialIDs.at(i) >= 0) {
                _container->removeChildByTag(kPlayerBageTagStart + i);
                CompositionLayer::_materialIDs.at(i) = -1;
            }
        }
    }
}

void CompositionCharacterSelectLayer::characterSortButtonCallback(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    PLAYERCONTROLLER->currentSelectedSortCharacterKey = static_cast<SortPlayerCharacter::KEY>(static_cast<Node*>(pSender)->getTag());
    sortCharacter();
    updateSortLabel();
    showDatas();
    removeChildByTag(TAG_LAYER::CHARACTER_SORT);
}