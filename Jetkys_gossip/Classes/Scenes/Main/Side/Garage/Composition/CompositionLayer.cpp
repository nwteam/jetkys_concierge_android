#include "CompositionLayer.h"
#include "CompositionDefines.h"
#include "CompositionRarityAlert.h"
#include "CompositionRarityAlertForParts.h"
#include "StringDefines.h"
#include "CharacterModel.h"
#include "PartModel.h"
#include "LevelModel.h"
#include "SystemSettingModel.h"

#define SIZE_MATERIAL 110.0f

std::vector<int>CompositionLayer::_materialIDs = std::vector<int>(5, -1);
int CompositionLayer::_materialIndex = -1;
int CompositionLayer::_pcIndex = -1;

CompositionLayer::CompositionLayer():
    _viewMasterCharacterId(0)
    , _progress(nullptr)
    , _request(nullptr)
    , _isSuccess(false)
    , _isGreat(false)
{
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}

CompositionLayer::~CompositionLayer()
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_co.plist", _viewMasterCharacterId).c_str());

    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

bool CompositionLayer::init(int pcIndex, GarageMainLayer* garageMainLayer)
{
    if (!Layer::init()) {
        return false;
    }
    if (_pcIndex < 0) {
        _pcIndex = pcIndex;
    }
    _garageMainLayer = garageMainLayer;
    _playerCharactersModel = PLAYERCONTROLLER->_playerCharactersSorted.at(_pcIndex);
    _levelBefore = PLAYERCONTROLLER->_playerCharactersSorted.at(_pcIndex)->getLevel();
    _expBefore = PLAYERCONTROLLER->_playerCharactersSorted.at(_pcIndex)->getExp();

    showBackground();
    showHeadLine(this, "合成確認", CC_CALLBACK_1(CompositionLayer::onTapBackButton, this));
    showCharacterInformations();
    showSelectMaterialPanelAndExpPanel();
    showCompositionbuttons();

    _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    return true;
}

void CompositionLayer::showBackground()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto background = makeSprite("home_bg.png");
    background->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(background, -10);
}

void CompositionLayer::showCharacterInformations()
{
    Size winSize = Director::getInstance()->getWinSize();

    int exercise = _playerCharactersModel->getExercise();
    int reaction = _playerCharactersModel->getReaction();
    int decision = _playerCharactersModel->getDecision();

    _container  = Layer::create();
    addChild(_container);

    _information_board = makeSprite("composition_information_before_board.png");
    Node* titleBackground = getChildByTag(TAG_HEADLINE::HEADLINE_BACKGROUND);
    _information_board->setPosition(Vec2(winSize.width / 2, titleBackground->getPositionY() - titleBackground->getContentSize().height / 2 - 40 - _information_board->getContentSize().height / 2));
    _container->addChild(_information_board);

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_co.plist", _playerCharactersModel->getCharactersId()).c_str());
    auto image = makeSprite(StringUtils::format("%d_co.png", _playerCharactersModel->getCharactersId()).c_str());
    image->setPosition(_information_board->getPosition() + Vec2(-_information_board->getContentSize().width / 2 - 2 + 20 + image->getContentSize().width / 2,
                                                                -_information_board->getContentSize().height / 2 - 5 + 20 + image->getContentSize().height / 2));
    _container->addChild(image);

    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(_playerCharactersModel->getCharactersId()));
    auto nameLabel = Label::createWithTTF(characterModel->getCarName(), FONT_NAME_2, 30, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    nameLabel->setPosition(_information_board->getPosition() + Vec2(-130, 118 - 15));
    nameLabel->setAnchorPoint(Vec2(0, 0.5));
    nameLabel->enableShadow();
    _container->addChild(nameLabel);

    _levelLabel = Label::createWithTTF(FormatWithCommas(_playerCharactersModel->getLevel()), FONT_NAME_2, 24);
    _levelLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _levelLabel->setAnchorPoint(Vec2(1, 0.5));
    _levelLabel->setPosition(_information_board->getPosition() + Vec2(75, 62 - 12));
    _levelLabel->enableShadow();
    _container->addChild(_levelLabel);

    _beyondExpLabel = Label::createWithTTF("", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _beyondExpLabel->setColor(COLOR_YELLOW);
    _beyondExpLabel->enableShadow();
    _beyondExpLabel->setAnchorPoint(Vec2(1, 0.5));
    _beyondExpLabel->setPosition(_levelLabel->getPosition() + Vec2(200, 0));
    _container->addChild(_beyondExpLabel);

    _exerciceLabel = Label::createWithTTF(FormatWithCommas(exercise), FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _exerciceLabel->setPosition(_levelLabel->getPosition() + Vec2(0, -30));
    _exerciceLabel->setAnchorPoint(Vec2(1, 0.5));
    _exerciceLabel->enableShadow();
    _container->addChild(_exerciceLabel);

    _exercisePlusLabel = Label::createWithTTF("", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _exercisePlusLabel->setPosition(_exerciceLabel->getPosition() + Vec2(200, 0));
    _exercisePlusLabel->setAnchorPoint(Vec2(1, 0.5));
    _exercisePlusLabel->setColor(COLOR_YELLOW);
    _exercisePlusLabel->enableShadow();
    _container->addChild(_exercisePlusLabel);

    _reactionLabel = Label::createWithTTF(FormatWithCommas(reaction), FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _reactionLabel->setPosition(_levelLabel->getPosition() + Vec2(0, -60));
    _reactionLabel->setAnchorPoint(Vec2(1, 0.5));
    _reactionLabel->enableShadow();
    _container->addChild(_reactionLabel);

    _reactionPlusLabel = Label::createWithTTF("", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _reactionPlusLabel->setPosition(_reactionLabel->getPosition() + Vec2(200, 0));
    _reactionPlusLabel->setAnchorPoint(Vec2(1, 0.5));
    _reactionPlusLabel->setColor(COLOR_YELLOW);
    _reactionPlusLabel->enableShadow();
    _container->addChild(_reactionPlusLabel);

    _decisionLabel = Label::createWithTTF(FormatWithCommas(decision), FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _decisionLabel->setPosition(_levelLabel->getPosition() + Vec2(0, -90));
    _decisionLabel->setAnchorPoint(Vec2(1, 0.5));
    _decisionLabel->enableShadow();
    _container->addChild(_decisionLabel);

    _decisionPlusLabel = Label::createWithTTF("", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _decisionPlusLabel->setPosition(_decisionLabel->getPosition() + Vec2(200, 0));
    _decisionPlusLabel->setAnchorPoint(Vec2(1, 0.5));
    _decisionPlusLabel->setColor(COLOR_YELLOW);
    _decisionPlusLabel->enableShadow();
    _container->addChild(_decisionPlusLabel);
}

void CompositionLayer::showSelectMaterialPanelAndExpPanel()
{
    std::shared_ptr<LevelModel>levelModel(LevelModel::find(_playerCharactersModel->getLevel()));
    int expBefMax = (int)levelModel->getRequiredExp();
    std::shared_ptr<LevelModel>nextlevelModel(LevelModel::find(_playerCharactersModel->getLevel() + 1));
    int expMax = (int)nextlevelModel->getRequiredExp();

    _material_board = makeSprite("composition_material_board.png");
    _material_board->setPosition(_information_board->getPosition() + Vec2(0, -_information_board->getContentSize().height / 2 - 10 - _material_board->getContentSize().height / 2));
    _container->addChild(_material_board);

    _menuMaterial = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    _menuMaterial->setPosition(_material_board->getPosition() + Vec2(0, -30));
    _container->addChild(_menuMaterial);

    int64_t expPredictIncTotal = 0;
    int coinPredictTotal = 0;

    for (int i = 0; i < _materialIDs.size(); i++) {
        MenuItemSprite* materialImage = nullptr;
        if (_materialIDs.at(i) < 0) {
            materialImage = makeMenuItem("character_select_using_smask.png",
                                         CC_CALLBACK_1(CompositionLayer::onTapMaterial, this));
            _menuMaterial->addChild(materialImage);
        } else {
            // level
            int materialLevel = PLAYERCONTROLLER->_playerCharactersSorted.at(_materialIDs.at(i))->getLevel();
            int characterID = PLAYERCONTROLLER->_playerCharactersSorted.at(_materialIDs.at(i))->getCharactersId();
            materialImage = makeMenuItem(StringUtils::format(IMAGE_CHARACTER_ICON, characterID).c_str(), CC_CALLBACK_1(CompositionLayer::onTapMaterial, this));
            _menuMaterial->addChild(materialImage);

            auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %d", materialLevel), FONT_NAME_2, 16);
            levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
            levelLabel->setPosition(Vec2(materialImage->getNormalImage()->getContentSize().width / 2, 6 - 8));
            levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
            materialImage->addChild(levelLabel);
            // calc predict exp increase
            int aPcIndex = _materialIDs.at(i);
            int level = PLAYERCONTROLLER->_playerCharactersSorted.at(aPcIndex)->getLevel();
            std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(PLAYERCONTROLLER->_playerCharactersSorted.at(aPcIndex)->getCharactersId()));
            int rarity = characterModel->getRarity();

            // 【式２】 ＝ 素材車なご数 × （（１５ × レアリティ × 車なごLv） ＋ （(500) × レアリティ））
            std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
            auto mixValue1 = systemSettingModel->getMixedCoefficient1();
            if (mixValue1 == 0) {
                mixValue1 = kCompositionCompressRate;
            }
            auto mixValue2 = systemSettingModel->getMixedCoefficient2();
            if (mixValue2 == 0) {
                mixValue2 = kCompositionCompositedCase;
            }
            auto baseCoin = systemSettingModel->getCoinOfSynthesis();
            if (baseCoin == 0) {
                baseCoin = kCompositionCoinCompsumtion;
            }

            int64_t expPredict = (((int64_t) mixValue1) * rarity * level) + (mixValue2 * rarity);
            int coinComp = 1500 + ((600 * (rarity + 1)) + (((30 + rarity) * level) * rarity));
            // CCLOG("calcExpIncPredict: pcIndex %d, rarity %d, level %d, expPredict %lld, coin: %d", aPcIndex, rarity, level, expPredict, coinComp);

            expPredictIncTotal += expPredict;
            coinPredictTotal += coinComp;
        }

        materialImage->setPosition(Vec2(SIZE_MATERIAL * (i - 2), 0));
        materialImage->setTag(kTagIndexOfMarterialID + i);
    }

    // Coin
    _coinLabel = Label::createWithTTF(FormatWithCommas(coinPredictTotal), FONT_NAME_2, 24);
    _coinLabel->enableShadow();
    _coinLabel->setAnchorPoint(Vec2(1, 0.5f));
    _coinLabel->setPosition(_material_board->getPosition() + Vec2(265, 40 - 12));
    _container->addChild(_coinLabel);

    // Information board
    // Exp increase
    _expInsLabel = Label::createWithTTF(FormatWithCommas((int)expPredictIncTotal), FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _expInsLabel->setColor(COLOR_YELLOW);
    _expInsLabel->enableShadow();
    _expInsLabel->setAnchorPoint(Vec2(1, 0.5));
    _expInsLabel->setPosition(_information_board->getPosition() + Vec2(285, -70 - 12));
    _container->addChild(_expInsLabel);

    // Exp info
    _realExpLabel = Label::createWithTTF(FormatWithCommas(_playerCharactersModel->getExp()), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _realExpLabel->enableShadow();
    _realExpLabel->setAnchorPoint(Vec2(1, 0.5));
    _realExpLabel->setPosition(Vec2(_expInsLabel->getPosition() + Vec2(0, -26)));
    _container->addChild(_realExpLabel);

    std::string expMaxStr = "あと ";
    _maxExpLabel = Label::createWithTTF(expMaxStr + FormatWithCommas(expMax - _playerCharactersModel->getExp()), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    _maxExpLabel->enableShadow();
    _maxExpLabel->setAnchorPoint(Vec2(1, 0.5));
    _maxExpLabel->setPosition(_realExpLabel->getPosition() + Vec2(0, -23));
    _container->addChild(_maxExpLabel);

    // Exp progress
    auto expProgressFrame = makeSprite("composition_exp_frame.png");
    expProgressFrame->setPosition(_information_board->getPosition() + Vec2(110, -140));
    float expPercentage = ((float) _playerCharactersModel->getExp() - expBefMax) / ((float) (expMax - expBefMax)) * 100.0f;
    _container->addChild(expProgressFrame);

    auto expPredictSprite = makeSprite("composition_exp_predict_progress.png");
    _expPredictProgress = ProgressTimer::create(expPredictSprite);
    _expPredictProgress->setType(ProgressTimer::Type::BAR);
    _expPredictProgress->setMidpoint(Vec2(0, 1));
    _expPredictProgress->setBarChangeRate(Vec2(1, 0));
    _expPredictProgress->setPercentage(expPercentage);

    _expPredictProgress->setPosition(expProgressFrame->getPosition() + Vec2(-expProgressFrame->getContentSize().width / 2 + expPredictSprite->getContentSize().width / 2, expProgressFrame->getContentSize().height / 2 - expPredictSprite->getContentSize().height / 2));
    _container->addChild(_expPredictProgress);

    _expProgress = ProgressTimer::create(makeSprite("composition_exp_progress.png"));
    _expProgress->setType(ProgressTimer::Type::BAR);
    _expProgress->setMidpoint(Vec2(0, 1));
    _expProgress->setBarChangeRate(Vec2(1, 0));
    _expProgress->setPercentage(expPercentage);
    _expProgress->setPosition(_expPredictProgress->getPosition());
    _container->addChild(_expProgress);

    levelUpPredict(expPredictIncTotal);
}

void CompositionLayer::showCompositionbuttons()
{
    _menu = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    _container->addChild(_menu);

    MenuItemSprite* multipleCompsitionButton = makeMenuItem("composition_empty_button.png", CC_CALLBACK_1(CompositionLayer::onTapMultipleCompositionButton, this));
    _menu->addChild(multipleCompsitionButton);

    Label* label = Label::createWithTTF("まとめて追加", FONT_NAME_2, 30, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->setPosition(Vec2(multipleCompsitionButton->getNormalImage()->getContentSize().width / 2,
                            multipleCompsitionButton->getNormalImage()->getContentSize().height / 2 - 15));
    label->enableShadow();
    multipleCompsitionButton->addChild(label);

    MenuItemSprite* compositionButton = makeMenuItem("composition_composit_button.png", CC_CALLBACK_1(CompositionLayer::onTapCompositionButton, this));

    if (_levelBefore >= 99 || countNumberMaterial() == 0) {
        compositionButton->setEnabled(false);
    }
    _menu->addChild(compositionButton);
    _menu->alignItemsHorizontallyWithPadding(80);
    _menu->setPosition(_material_board->getPosition() + Vec2(0, -_material_board->getContentSize().height / 2 - 30 - compositionButton->getNormalImage()->getContentSize().height / 2));
}

void CompositionLayer::onTapBackButton(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    _materialIndex = -1;
    _pcIndex = -1;
    for (int i = 0; i < _materialIDs.size(); i++) {
        _materialIDs.at(i) = -1;
    }
    _garageMainLayer->showLayer(GLI_LIST);
}

void CompositionLayer::onTapMultipleCompositionButton(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    _materialIndex = -1;
    _garageMainLayer->showLayer(GLI_COMPOSITION_CHARACTER_SELECT);
}

void CompositionLayer::onTapMaterial(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    _materialIndex = ((Node*)pSender)->getTag() - kTagIndexOfMarterialID;
    _garageMainLayer->showLayer(GLI_COMPOSITION_CHARACTER_SELECT);
}

void CompositionLayer::onTapCompositionButton(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    if (countNumberMaterial() == 0) {
        return;
    }

    if (checkRarity()) {
        CompositionRarityAlert* layer = CompositionRarityAlert::create(_materialIDs, CC_CALLBACK_0(CompositionLayer::onTapConfirmWithRareCharacterButton, this));
        addChild(layer, 1000);
    } else if (checkParts()) {
        CompositionRarityAlertForParts* layer = CompositionRarityAlertForParts::create(_materialIDs, CC_CALLBACK_0(CompositionLayer::onTapConfirmWithRarePartsButton, this));
        addChild(layer, 1000);
    } else {
        composition();
    }
}

void CompositionLayer::onTapConfirmWithRareCharacterButton()
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    if (checkParts()) {
        auto layer = CompositionRarityAlertForParts::create(_materialIDs, CC_CALLBACK_0(CompositionLayer::onTapConfirmWithRarePartsButton, this));
        addChild(layer, 15);
    } else {
        composition();
    }
}

void CompositionLayer::onTapConfirmWithRarePartsButton()
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    composition();
}


bool CompositionLayer::checkRarity()
{
    for (int i = 0; i < _materialIDs.size(); i++) {
        if (_materialIDs.at(i) >= 0) {
            int characterID = PLAYERCONTROLLER->_playerCharactersSorted.at(_materialIDs.at(i))->getCharactersId();
            if (CharacterModel::find(characterID)->getRarity() >= 3) {
                return true;
            }
        }
    }
    return false;
}

bool CompositionLayer::checkParts()
{
    for (int i = 0; i < _materialIDs.size(); i++) {
        if (_materialIDs.at(i) != -1) {
            PlayerCharactersModel* selectedPlayerCharactersModel = PLAYERCONTROLLER->_playerCharactersSorted.at(_materialIDs.at(i));
            if (_materialIDs.at(i) >= 0 && selectedPlayerCharactersModel->isAnyPartsEquiped()) {
                return true;
            }
        }
    }
    return false;
}

void CompositionLayer::composition()
{
    int base_character_id = _playerCharactersModel->getID();
    std::vector<int>material_character_ids = std::vector<int>();
    for (int i = 0; i < _materialIDs.size(); i++) {
        if (_materialIDs.at(i) >= 0) {
            material_character_ids.push_back(PLAYERCONTROLLER->_playerCharactersSorted.at(_materialIDs.at(i))->getID());
        }
    }
    std::string idsStr = "";
    for (auto idMaterial : material_character_ids) {
        if (idsStr != "") {
            idsStr += ",";
        }
        idsStr += StringUtils::format("%d", idMaterial);
    }
    _progress->onStart();
    _request->setSyntheticRequest(base_character_id, idsStr, CC_CALLBACK_1(CompositionLayer::responseSyntheticRequest, this));
}



void CompositionLayer::levelUpPredict(int64_t expPredictInc)
{
    int64_t currentExp = _playerCharactersModel->getExp();
    int level = _playerCharactersModel->getLevel();
    int mstId = _playerCharactersModel->getCharactersId();
    int64_t predictExp = currentExp + expPredictInc;

    int64_t expNextLevel = 0;
    int nextLevel = level;
    do {
        nextLevel++;
        if (nextLevel > 99) {
            break;
        }
        std::shared_ptr<LevelModel>levelModel(LevelModel::find(nextLevel));
        expNextLevel = levelModel->getRequiredExp();
    } while (expNextLevel <= predictExp);

    nextLevel--;

    // if reach max level
    if (nextLevel == 99) {
        predictExp = expNextLevel;
    }

    if (nextLevel > level) {
        std::shared_ptr<LevelModel>levelModel(LevelModel::find(nextLevel));
        _beyondExpLabel->setString(StringUtils::format("%d", nextLevel));
        int exercisePlus = levelModel->getExercise();
        int reactionPlus = levelModel->getReaction();
        int decisionPlus = levelModel->getDecision();

        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(mstId));
        int baseEx = characterModel->getBaseExercise();
        int baseRe = characterModel->getBaseReaction();
        int baseDe = characterModel->getBaseDecision();

        int partsEx = 0;
        int partsRe = 0;
        int partsDe = 0;
        for (int i = 0; i < 2; i++) {
            int playerPartsId = _playerCharactersModel->getPartsSlot(0);
            if (playerPartsId > 0) {
                int pId = PLAYERCONTROLLER->findIndexOfPlayerPartId(playerPartsId);
                int mstId = PLAYERCONTROLLER->_playerParts.at(pId)->getPartsId();
                std::shared_ptr<PartModel>partsModel(PartModel::find(mstId));
                partsEx += partsModel->getExercise();
                partsRe += partsModel->getReaction();
                partsDe += partsModel->getDecision();
            }
        }

        _exercisePlusLabel->setString(FormatWithCommas(baseEx + partsEx + (baseEx * (exercisePlus * 0.01))));
        _reactionPlusLabel->setString(FormatWithCommas(baseRe + partsRe + (baseRe * (reactionPlus * 0.01))));
        _decisionPlusLabel->setString(FormatWithCommas(baseDe + partsDe + (baseDe * (decisionPlus * 0.01))));

        _expPredictProgress->setPercentage(100);
    } else {
        _beyondExpLabel->setString(StringUtils::format("%d", level));
        _exercisePlusLabel->setString(FormatWithCommas(_playerCharactersModel->getExercise()));
        _reactionPlusLabel->setString(FormatWithCommas(_playerCharactersModel->getReaction()));
        _decisionPlusLabel->setString(FormatWithCommas(_playerCharactersModel->getDecision()));
        float percent = (float)predictExp / expNextLevel;
        _expPredictProgress->setPercentage(percent);
    }
}

void CompositionLayer::levelUpSuccess()
{
    Size winSize = Director::getInstance()->getWinSize();

    _materialIndex = -1;
    for (int i = 0; i < _materialIDs.size(); i++) {
        _materialIDs.at(i) = -1;
    }

    int characteID = _playerCharactersModel->getCharactersId();
    int level = _playerCharactersModel->getLevel();
    int exp = _playerCharactersModel->getExp();

    _isLevelUp = level > _levelBefore ? true : false;

    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(characteID));
    _garageMainLayer->updateInfoLayer();
    _realExpLabel->setString(FormatWithCommas(exp));
    runAction(Sequence::create(DelayTime::create(1),
                               CallFunc::create(CC_CALLBACK_0(CompositionLayer::addseikou, this)),
                               DelayTime::create(1.2),
                               CallFunc::create(CC_CALLBACK_0(CompositionLayer::runUplevelAnimation, this)), NULL));

    _information_board->setTexture(Director::getInstance()->getTextureCache()->addImage("composition_information_after_board.png"));

    _expInsLabel->removeFromParent();
    _material_board->removeFromParent();
    _menu->removeFromParent();
    _coinLabel->removeFromParent();
    _menuMaterial->removeFromParent();

    showSkipMenu();
}

void CompositionLayer::addseikou()
{
    auto successLabel = makeSprite(_isGreat ? "composition_very_success_label.png" : "composition_success_label.png");
    addChild(successLabel, 200);
    successLabel->setPosition(_information_board->getPosition() + Vec2(0, -_information_board->getContentSize().height / 2 - 50 - successLabel->getContentSize().height / 2));
    successLabel->runAction(Sequence::create(ScaleTo::create(0.7, 2),
                                             ScaleTo::create(0.3, 1.5), NULL));
    _successLabel = successLabel;
}

void CompositionLayer::runUplevelAnimation()
{
    schedule(schedule_selector(CompositionLayer::updateExpProgress));
}

void CompositionLayer::updateExpProgress(float dt)
{
    _sortBt->setEnabled(isScheduled(schedule_selector(CompositionLayer::updateExpProgress)));
    int level = _playerCharactersModel->getLevel();
    int exp = _playerCharactersModel->getExp();
    int expMax = 0;
    int expBefMax = 0;

    if (_levelBefore <= level) {
        std::shared_ptr<LevelModel>levelModel(LevelModel::find(_levelBefore));
        expBefMax = (int)levelModel->getRequiredExp();
        std::shared_ptr<LevelModel>nextLevelModel(LevelModel::find(_levelBefore + 1));
        expMax = (int)nextLevelModel->getRequiredExp();
    }

    _expBefore += 40;
    if (_expBefore >= expMax) {
        // update level up
        _expBefore = expMax;
        _levelBefore++;
        if (_levelBefore == 99) {
            _expPredictProgress->setPercentage(100);
        } else {
            _expPredictProgress->setPercentage(0);
        }
    }
    if (_expBefore >= exp || _levelBefore == 99) {
        unschedule(schedule_selector(CompositionLayer::updateExpProgress));
        _expBefore = exp;
        _sortBt->setEnabled(isScheduled(schedule_selector(CompositionLayer::updateExpProgress)));
        auto delayAction = DelayTime::create(0.5);
        auto actionShowSuccessLabel = CallFunc::create(CC_CALLBACK_0(CompositionLayer::showSuccessLabel, this));
        runAction(Sequence::create(delayAction, actionShowSuccessLabel, NULL));
    }
    float expPercentage = ((float) (_expBefore - expBefMax)) / ((float) (expMax - expBefMax)) * 100.0f;

    _expProgress->setPercentage(expPercentage);
}

void CompositionLayer::showSuccessLabel()
{
    _isGreat = false;
    Point labelPosi = Point(_successLabel->getPosition());
    // check level up
    if (_isLevelUp) {
        auto levelUpLabel = makeSprite("composition_level_up_label.png");
        addChild(levelUpLabel, 201);
        levelUpLabel->setPosition(_successLabel->getPosition() + Vec2(0, -_successLabel->getContentSize().height / 2 - 50 - levelUpLabel->getContentSize().height / 2));
        labelPosi = Point(levelUpLabel->getPosition());
    }

    // check max level, can't re composition
    int level = _playerCharactersModel->getLevel();
    if (level < 99) {
        auto retryComposiBtn = makeMenuItem("composition_empty_button.png", CC_CALLBACK_1(CompositionLayer::retryConposi, this));
        retryComposiBtn->setPosition(labelPosi - Vec2(0, 120));
        auto menu = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
        menu->addChild(retryComposiBtn);
        addChild(menu, 202);
        menu->setPosition(Point::ZERO);

        auto retryComposiLabel = Label::createWithTTF("再合成", FONT_NAME_2, 30, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
        retryComposiBtn->addChild(retryComposiLabel);
        retryComposiLabel->setPosition(Vec2(retryComposiBtn->getNormalImage()->getContentSize().width / 2, retryComposiBtn->getNormalImage()->getContentSize().height / 2 - 15));
        retryComposiLabel->enableShadow();
    }
}

void CompositionLayer::retryConposi(Ref* pSender)
{
    removeFromParent();
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    _garageMainLayer->showLayer(GLI_COMPOSITION, _pcIndex);
}

void CompositionLayer::responseSyntheticRequest(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "set_synthetic_request");
    std::string result = Json_getString(jsonData1, "result", "false");

    if (result == "true") {
        Json* jsonDataData = Json_getItem(jsonData1, "data");

        // todo great_success
        std::string great_success = Json_getString(jsonDataData, "great_success", "");
        if (great_success.compare("true") == 0) {
            _isGreat = true;
        }

        Json* mDatas = Json_getItem(jsonDataData, "players");
        int64_t player_id = atoll(Json_getString(mDatas, "id", "-1"));
        int free_coin = atoi(Json_getString(mDatas, "free_coin", "-1"));
        int pay_coin = atoi(Json_getString(mDatas, "pay_coin", "-1"));
        if (player_id == PLAYERCONTROLLER->_player->getID()) {
            PLAYERCONTROLLER->_player->setFreeCoin(free_coin);
            PLAYERCONTROLLER->_player->setPayCoin(pay_coin);
            PLAYERCONTROLLER->_player->update();

            // player_character
            mDatas = Json_getItem(jsonDataData, "player_characters");
            if (mDatas && mDatas->type == Json_Array) {
                for (Json* child = mDatas->child; child; child = child->next) {
                    PLAYERCONTROLLER->updateCharacter(child, atoiNull(Json_getString(child, "lock", "")));
                }
            }
            mDatas = Json_getItem(jsonDataData, "delete_character_ids");
            if (mDatas && mDatas->type == Json_Array) {
                for (Json* child = mDatas->child; child; child = child->next) {
                    int pcID = atoi(child->valueString);
                    PLAYERCONTROLLER->deleteCharacter(pcID);
                }
            }
            levelUpSuccess();
        }
    }
}

int CompositionLayer::countNumberMaterial()
{
    int count = 0;
    for (int i = 0; i < _materialIDs.size(); i++) {
        if (_materialIDs.at(i) >= 0) {
            count++;
        }
    }
    return count;
}

void CompositionLayer::showSkipMenu()
{
    Size winSize = Director::getInstance()->getWinSize();

    // Sort button
    auto menuSort = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    addChild(menuSort, 10);

    _sortBt = makeMenuItem("composition_skip_button.png", [this](Ref* pSender) {
        if (isScheduled(schedule_selector(CompositionLayer::updateExpProgress))) {
            SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
            _expBefore = _playerCharactersModel->getExp();
            _levelBefore = _playerCharactersModel->getLevel();
            updateExpProgress(0);
        }
    });
    _sortBt->setEnabled(isScheduled(schedule_selector(CompositionLayer::updateExpProgress)));
    _sortBt->setTag(bt_sort_bt);
    menuSort->addChild(_sortBt);

    _sortLabel = Label::createWithTTF("スキップ", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    _sortLabel->enableShadow();
    _sortLabel->setPosition(Vec2(_sortBt->getNormalImage()->getContentSize().width / 2, _sortBt->getNormalImage()->getContentSize().height / 2 - 12));
    _sortBt->addChild(_sortLabel);
    Node* titleBackground = getChildByTag(TAG_HEADLINE::HEADLINE_BACKGROUND);
    menuSort->setPosition(Vec2(winSize.width - 14 - _sortBt->getNormalImage()->getContentSize().width / 2,
                               titleBackground->getPositionY()));
}













