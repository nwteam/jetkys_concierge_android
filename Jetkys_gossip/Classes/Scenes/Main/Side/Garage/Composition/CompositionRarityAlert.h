#ifndef __syanago__CompositionRarityAlert__
#define __syanago__CompositionRarityAlert__

#include "LayerPriority.h"
#include "PopUpDelegate.h"
#include "create_func.h"
#include <functional>

#define kPriorityCompositionRarityAlert -650
#define kPriorityCompositionRarityAlertMenu kPriorityCompositionRarityAlert - 2

class CompositionRarityAlert : public LayerPriority, create_func<CompositionRarityAlert> {
public:
    using create_func::create;
    typedef std::function<void()> DialogCallback;
    bool init(std::vector<int> pcIndex, const DialogCallback& callback);
};

#endif /* defined(__syanago__CompositionRarityAlert__) */
