#ifndef __syanago__TeamEditLayer__
#define __syanago__TeamEditLayer__

#include "SyanagoBaseLayer.h"
#include "PlayerTeamModel.h"

class TeamEditLayer : public SyanagoBaseLayer {
public:
    TeamEditLayer();
    
    bool init();
    CREATE_FUNC(TeamEditLayer);
    
    void btMenuItemCallback(cocos2d::Ref *pSender);
    void btSelectCharacterCallback(cocos2d::Ref *pSender);
    
    void leaderInfomation();
    void supportInformation(int supportIndex);
    void statusInformation();
    
private:
    int calculateTotalPower(PlayerTeamModel* playerTeamModel);
    int calculateTotalTechnique(PlayerTeamModel* playerTeamModel);
    int calculateTotalBrake(PlayerTeamModel* playerTeamModel);
    int calculateTotalCost(PlayerTeamModel* playerTeamModel);
    
    Menu *_menu;
    Sprite *leader_frame;
};

#endif /* defined(__syanago__TeamEditLayer__) */
