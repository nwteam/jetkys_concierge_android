#include "TeamEditMainLayer.h"
#include "TeamEditLayer.h"
#include "MainScene.h"
#include "TeamCharacterSelectLayer.h"

#define tag_bt_team_edit 1

bool TeamEditMainLayer::init()
{
    if (!Layer::init()) {
        return false;
    }
    _layerIndex = -1;
    _isShow = false;
    _touchID = -1;

    _isMoving = false;

    Size winSize = Director::getInstance()->getWinSize();
    Vec2 _currentPoint = Vec2(-winSize.width, 0);

    // Add side
    auto sideTeamEdit = makeSprite("edit_team_button.png");
    addChild(sideTeamEdit);
    sideTeamEdit->setPosition(
        Vec2(winSize.width + sideTeamEdit->getContentSize().width / 2,
             winSize.height / 2));
    sideTeamEdit->setTag(tag_bt_team_edit);

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);

    listener->onTouchBegan = CC_CALLBACK_2(TeamEditMainLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(TeamEditMainLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(TeamEditMainLayer::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(TeamEditMainLayer::onTouchCancelled, this);

    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    setPosition(_currentPoint);

    return true;
}

bool TeamEditMainLayer::onTouchBegan(Touch* touch, Event* unused_event)
{
    //////CCLOG("TeamEditMainLayer::onTouchBegan");
    if (!isVisible()) {
        return false;
    }

    if (_touchID != -1) {
        return false;
    }
    if (_mainScene->checkMoving() != 0) {
        return false;
    }

    if (_mainScene->isMainTutorial()) {
        return false;
    }


    auto editSprite = (Sprite*) getChildByTag(tag_bt_team_edit);

    Rect editRect = Rect(editSprite->getPosition().x - editSprite->getContentSize().width / 2, editSprite->getPosition().y - editSprite->getContentSize().height / 2, editSprite->getContentSize().width, editSprite->getContentSize().height);
    Size winSize = Director::getInstance()->getWinSize();
    Rect layerRect = Rect(0, 0, winSize.width, winSize.height);

    if (layerRect.containsPoint(convertToNodeSpace(touch->getLocation())) || editRect.containsPoint(convertToNodeSpace(touch->getLocation()))) {
        if (_layerIndex == TEI_CHARACTER_SELECT) {
            return false;
        }

        ////CCLOG("Touchbegin layer");
        _touchID = touch->getID();
        _deltaPoint = Vec2::ZERO;

        // Init  layer when havent layer with _layerIndex
        if (_layerIndex < 0 || (_layerIndex >= 0 && !getChildByTag(_layerIndex))) {
            // load texture packers

            _mainScene->changeSideZOrder(SLI_TEAM_EDIT);

            if (_layerIndex < 0) {
                showLayer(TEI_TEAM_EDIT);
            } else {
                showLayer(_layerIndex);
            }
        }

        stopAllActions();
        scheduleUpdate();

        _isMoving = true;
        ////CCLOG("TeamEdit isMoving");

        return true;
    }

    return false;
}

void TeamEditMainLayer::onTouchMoved(Touch* touch, Event* unused_event)
{
    //////CCLOG("TeamEditMainLayer::onTouchMoved");
    if (touch->getID() == _touchID) {
        _deltaPoint = touch->getLocation() - touch->getPreviousLocation();
    }
}
void TeamEditMainLayer::onTouchEnded(Touch* touch, Event* unused_event)
{
    ////CCLOG("TeamEditMainLayer::onTouchEnded");
    if (touch->getID() == _touchID) {
        _touchID = -1;
    }
}



void TeamEditMainLayer::onTouchCancelled(Touch* touch, Event* unused_event)
{
    ////CCLOG("TeamEditMainLayer::onTouchCancelled");
    if (touch->getID() == _touchID) {
        _touchID = -1;
    }
}

void TeamEditMainLayer::update(float dt)
{
    ////CCLOG("deltaPoint: %f, %f", _deltaPoint.x, _deltaPoint.y);
    if (_layerIndex == TEI_CHARACTER_SELECT) {
        if (getPositionX() >= 0) {
            if (_reportMess) {
                if (!_reportMess->isVisible()) {
                    _reportMess->setVisible(true);
                }
            }
        } else {
            if (_reportMess) {
                if (_reportMess->isVisible()) {
                    _reportMess->setVisible(false);
                }
            }
        }
    }
    if (_touchID == -1) {
        // endtouch
        ////CCLOG("End touch");
        unscheduleUpdate();

        showHide(_deltaPoint.x >= 0);

        return;
    }

    // update positoin when move left->right
    Size winSize = Director::getInstance()->getWinSize();
    float posX = getPositionX() + _deltaPoint.x;
    posX = MAX(posX, -winSize.width);
    posX = MIN(posX, 0);
    setPositionX(posX);
    // fix Android 20140918
    // _deltaPoint = Vec2::ZERO;
}

void TeamEditMainLayer::showHide(bool isShow)
{
    if (_touchID != -1 && getChildByTag(_layerIndex)) {
        unscheduleUpdate();
        _touchID = -1;
    }
    _isShow = isShow;
    if (!getActionByTag(ACTION_TAG::BASE_HIDE)) {
        Size winSize = Director::getInstance()->getWinSize();
        Action* hideAction = Sequence::create(MoveTo::create(0.3f, Vec2(_isShow ? 0 : -winSize.width, 0)),
                                              CallFunc::create(CC_CALLBACK_0(TeamEditMainLayer::showHideEnd, this)), NULL);
        hideAction->setTag(ACTION_TAG::BASE_HIDE);
        runAction(hideAction);
    }
}

void TeamEditMainLayer::showHideEnd()
{
    _isMoving = false;
    if (!_isShow) {
        removeChildByTag(_layerIndex);
    } else {
        if (_layerIndex == TEI_CHARACTER_SELECT) {
            if (_reportMess) {
                if (!_reportMess->isVisible()) {
                    _reportMess->setVisible(true);
                }
            }
        }
    }
}

void TeamEditMainLayer::showLayer(int layerIndex, int pcharacterTag /* = -1*/)
{
    Node* befLayer = nullptr;
    if (_layerIndex >= 0 && getChildByTag(_layerIndex)) {
        // reset touch
        // show true when change layer
        _touchID = -1;
        _deltaPoint = Vec2(1, 0);
        befLayer = getChildByTag(_layerIndex);
        // removeChildByTag(_layerIndex);
    }

    _layerIndex = layerIndex;
    switch (_layerIndex) {
    case TEI_TEAM_EDIT: {
        auto layer = TeamEditLayer::create();
        addChild(layer);
        layer->setTag(TEI_TEAM_EDIT);
        layer->_mainLayer = this;

        break;
    }
    case TEI_CHARACTER_SELECT: {
        auto layer = TeamCharacterSelectLayer::createWithPlayerCharacterTag(pcharacterTag);
        addChild(layer);
        layer->setTag(TEI_CHARACTER_SELECT);
        layer->_mainLayer = this;

        auto teropbg = makeSprite("telop_bg.png");


        _reportMess = Label::createWithTTF("お知らせが入ります！お知らせが入ります！お知らせが入ります！お知らせが入ります！", FONT_NAME_2, 22);
        _reportMess->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        _reportMess->setColor(Color3B::WHITE);
        _reportMess->setAnchorPoint(Vec2(0, 0.75f));

        if (pcharacterTag == 110) {
            _reportMess->setString(PLAYERCONTROLLER->_messages.at(19).c_str());
        } else {
            _reportMess->setString(PLAYERCONTROLLER->_messages.at(20).c_str());
        }

        layer->addChild(_reportMess);
        Size winSize = Director::getInstance()->getWinSize();
        // int length = (int)_reportMess->getString().length();
        ////CCLOG("Report message length: %d", length);

        _reportMess->setPosition(Vec2(winSize.width, teropbg->getContentSize().height / 2));
        _reportMess->runAction(RepeatForever::create(Sequence::create(MoveBy::create(TEROP_SPEED, Vec2(-1500, 0)), CallFuncN::create([](Ref* sender) {
                auto node = (Node*) sender;
                node->setPositionX(node->getPositionX() + 1500);
            }), NULL)));

        _holdingBase = makeSprite("garage_list_holding_base.png");
        _holdingTile = Label::createWithTTF("所持数",
                                            FONT_NAME_2,
                                            18);
        _holdingBase->addChild(_holdingTile);
        _holdingTile->setAnchorPoint(Vec2(0.0, 0.5));
        _holdingTile->setPosition(Point(5, _holdingBase->getContentSize().height / 2 - 9));

        _holdingLabel = Label::createWithTTF(StringUtils::format("%d/%d",
                                                                 (int)PLAYERCONTROLLER->_playerCharacterModels.size(),
                                                                 PLAYERCONTROLLER->_player->getGarageSize()).c_str(),
                                             FONT_NAME_2,
                                             18);
        _holdingBase->addChild(_holdingLabel);
        _holdingLabel->setAnchorPoint(Vec2(1.0, 0.5));
        _holdingLabel->setPosition(Point(_holdingBase->getContentSize().width - 5, _holdingBase->getContentSize().height / 2 - 9));

        _holdingBase->setPosition(Point(winSize.width - _holdingBase->getContentSize().width / 2,
                                        teropbg->getPosition().y + teropbg->getContentSize().height + _holdingBase->getContentSize().height / 2));
        layer->addChild(_holdingBase);
        break;
    }
    }
    if (befLayer) {
        befLayer->removeFromParent();
    }
}
void TeamEditMainLayer::showDetailPlayerCharacter(int pcID)
{
    _mainScene->showDetailPlayerCharacter(pcID);
}

void TeamEditMainLayer::updateLeaderCharacterImg()
{
    _mainScene->homeLayer->updateCharacterImg();
}
