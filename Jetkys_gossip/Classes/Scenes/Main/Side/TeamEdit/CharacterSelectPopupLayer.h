#ifndef __syanago__CharacterSelectPopupLayer__
#define __syanago__CharacterSelectPopupLayer__


#include "DialogDefine.h"
#include "LayerPriority.h"
#include "PopUpDelegate.h"
#include "create_func.h"


enum CSPopupButtonIndex {
    BT_CSP_OK, BT_CSP_DETAIL, BT_CSP_CLOSE
};

enum ViewType{
    LEDER_SELECT,
    MENMBER_SELECT
};

class CharacterSelectPopupLayer : public LayerPriority, public create_func<CharacterSelectPopupLayer>
{
    
public:
    using create_func::create;
    bool init(int playerCharacterIndex, ViewType type);
    
    void callBackbtn(Ref *pSender);
    
    PopUpDelegate *_delegate;
    
    CC_SYNTHESIZE(int, _playerCharacterIndex, PlayerCharacterIndex)
};

#endif /* defined(__syanago__CharacterSelectPopupLayer__) */
