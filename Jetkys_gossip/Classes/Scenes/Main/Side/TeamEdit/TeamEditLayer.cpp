#include "TeamEditLayer.h"
#include "TeamEditMainLayer.h"
#include "PlayerController.h"
#include "StringDefines.h"
#include "CharacterModel.h"
#include "RankModel.h"
#include "CharacterModel.h"
#include "LeaderSkillNameModel.h"
#include "SkillNameModel.h"

TeamEditLayer::TeamEditLayer()
{
    _title = "編成";
}

bool TeamEditLayer::init()
{
    if (!SyanagoBaseLayer::init()) {
        return false;
    }

    Size winSize = Director::getInstance()->getWinSize();

    _menu = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    _menu->setPosition(Vec2::ZERO);
    _menu->setAnchorPoint(Vec2::ZERO);
    addChild(_menu, -1);

    leaderInfomation();
    supportInformation(1);
    supportInformation(2);
    supportInformation(3);

    statusInformation();
    return true;
}

void TeamEditLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    checkMenuReturn();
    if (dynamic_cast<TeamEditMainLayer*>(_mainLayer)->isMoving()) {
        return;
    }

    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    dynamic_cast<TeamEditMainLayer*>(_mainLayer)->showHide(false);
}

void TeamEditLayer::leaderInfomation()
{
    Size winSize = Director::getInstance()->getWinSize();

    int characterID = PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]->getCharactersId();
    int level = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]->getLevel();

    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(characterID));
    int cost = characterModel->getCost();


    auto leader_bg = makeMenuItem("te_leader_bg.png", CC_CALLBACK_1(TeamEditLayer::btSelectCharacterCallback, this));
    leader_bg->setTag(PCT_LEADER);
    leader_bg->setPosition(Vec2(14.0f + leader_bg->getContentSize().width * 0.5f, _titleBg->getPositionY() - _titleBg->getContentSize().height / 2 - 12 - leader_bg->getContentSize().height / 2));
    leader_bg->setUserData(PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]);
    _menu->addChild(leader_bg);

    leader_frame = makeSprite("te_leader_mask.png");
    addChild(leader_frame, 1);
    leader_frame->setPosition(leader_bg->getPosition());
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_r.plist", characterID).c_str());
    auto leader_image = makeSprite(StringUtils::format(IMAGE_CHARACTER_ICON_LEADER, characterID).c_str());
    leader_image->setPosition(leader_frame->getPosition());
    addChild(leader_image);

    auto leader_label = Label::createWithTTF("リーダー", FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    leader_label->setPosition(leader_frame->getPosition() + Vec2(-leader_frame->getContentSize().width * 0.5f + 48.0f, leader_frame->getContentSize().height * 0.5f - 18.0f - 10));
    leader_label->enableShadow();
    addChild(leader_label, 2);

    auto lv_label = Label::createWithTTF("Lv", FONT_NAME_2, 18, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    lv_label->setAnchorPoint(Vec2(0, 0.5f));
    lv_label->setPosition(leader_frame->getPosition() + Vec2(-20, -leader_frame->getContentSize().height * 0.5f + 40 - 9));
    lv_label->enableShadow();
    addChild(lv_label, 2);

    auto cost_label = Label::createWithTTF("コスト", FONT_NAME_2, 18, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    cost_label->setAnchorPoint(Vec2(0, 0.5f));
    cost_label->setPosition(leader_frame->getPosition() + Vec2(-20, -leader_frame->getContentSize().height * 0.5f + 18 - 9));
    cost_label->enableShadow();
    addChild(cost_label, 2);

    auto lv_value = Label::createWithTTF(StringUtils::format("%d", level), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    addChild(lv_value, 2);
    lv_value->setAnchorPoint(Vec2(1, 0.5));
    lv_value->setPosition(lv_label->getPosition() + Vec2(leader_frame->getContentSize().width * 0.5 + 4, 0));
    lv_value->enableShadow();
    // lv_value->enableOutline(Color4B(Color3B::BLACK), 1);

    auto cost_value = Label::createWithTTF(StringUtils::format("%d", cost), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    addChild(cost_value, 2);
    cost_value->setAnchorPoint(Vec2(1, 0.5));
    cost_value->setPosition(cost_label->getPosition() + Vec2(leader_frame->getContentSize().width * 0.5 + 4, 0));
    cost_value->enableShadow();
    // cost_value->enableOutline(Color4B(Color3B::BLACK), 1);
}

void TeamEditLayer::supportInformation(int supportIndex)
{
    //////CCLOG("TeamEditLayer::supportInformation(int supportIndex) %d", supportIndex);
    Size winSize = Director::getInstance()->getWinSize();

    int playerCharacterID = -1;
    if (supportIndex == 1) {
        playerCharacterID = PLAYERCONTROLLER->_playerTeams->getSupport1CharacterId();
    } else if (supportIndex == 2) {
        playerCharacterID = PLAYERCONTROLLER->_playerTeams->getSupport2CharacterId();
    } else {
        playerCharacterID = PLAYERCONTROLLER->_playerTeams->getSupport3CharacterId();
    }

    auto support_bg = makeMenuItem("te_support_bg.png", CC_CALLBACK_1(TeamEditLayer::btSelectCharacterCallback, this));
    _menu->addChild(support_bg, -1);
    support_bg->setPosition(Vec2(winSize.width - 14.0f - support_bg->getContentSize().width * 0.5f - (support_bg->getContentSize().width + 5) * (3 - supportIndex),
                                 leader_frame->getPositionY() + leader_frame->getContentSize().height * 0.5f - support_bg->getContentSize().height * 0.5f));
    support_bg->setTag(PCT_LEADER + supportIndex); // PCT_SUPPORT1, PCT_SUPPORT2, PCT_SUPPORT3


    // frame
    auto support_frame = makeSprite("te_support_mask.png");
    addChild(support_frame, 1);
    support_frame->setPosition(support_bg->getPosition());

    if (playerCharacterID < 0) {
        // No support charracter
        return;
    }

    int characterID = PLAYERCONTROLLER->_playerCharacterModels[playerCharacterID]->getCharactersId();
    int level = PLAYERCONTROLLER->_playerCharacterModels[playerCharacterID]->getLevel();
    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(characterID));
    int cost = characterModel->getCost();

    support_bg->setUserData(PLAYERCONTROLLER->_playerCharacterModels[playerCharacterID]);

    // image
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_s.plist", characterID).c_str());
    auto support_image = makeSprite(StringUtils::format(IMAGE_CHARACTER_ICON_SUPPORT, characterID).c_str());
    addChild(support_image);
    support_image->setPosition(support_bg->getPosition());

    // Level label, cost label
    auto lv_label = Label::createWithTTF("Lv", FONT_NAME_2, 18, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(lv_label, 2);
    lv_label->setAnchorPoint(Vec2(0, 0.5f));
    lv_label->setPosition(support_bg->getPosition() + Vec2(-support_bg->getContentSize().width * 0.5 + 10, -support_bg->getContentSize().height * 0.5f + 42 - 9));
    lv_label->enableShadow();
    // lv_label->enableOutline(Color4B(Color3B::BLACK), 1);

    auto cost_label = Label::createWithTTF("コスト", FONT_NAME_2, 18, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(cost_label, 2);
    cost_label->setAnchorPoint(Vec2(0, 0.5f));
    cost_label->setPosition(support_bg->getPosition() + Vec2(-support_bg->getContentSize().width * 0.5 + 10, -support_bg->getContentSize().height * 0.5f + 18 - 9));
    cost_label->enableShadow();
    // cost_label->enableOutline(Color4B(Color3B::BLACK), 1);

    auto lv_value = Label::createWithTTF(StringUtils::format("%d", level), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    addChild(lv_value, 2);
    lv_value->setAnchorPoint(Vec2(1, 0.5));
    lv_value->setPosition(lv_label->getPosition() + Vec2(support_bg->getContentSize().width - 18, 0));
    // lv_value->enableOutline(Color4B(Color3B::BLACK), 1);
    lv_value->enableShadow();

    auto cost_value = Label::createWithTTF(StringUtils::format("%d", cost), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    addChild(cost_value, 2);
    cost_value->setAnchorPoint(Vec2(1, 0.5));
    cost_value->setPosition(cost_label->getPosition() + Vec2(support_bg->getContentSize().width - 18, 0));
    cost_value->enableShadow();
    // cost_value->enableOutline(Color4B(Color3B::BLACK), 1);
}


void TeamEditLayer::statusInformation()
{
    Size winSize = Director::getInstance()->getWinSize();

    PlayerTeamModel* playerTeamModel = PLAYERCONTROLLER->_playerTeams;

    PlayerCharactersModel* playerCharactersModel = PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getLearderCharacterId()];

    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(playerCharactersModel->getCharactersId()));
    std::string name0 = characterModel->getCarName();
    int skillID0 = characterModel->getLeaderSkillName();
    std::shared_ptr<LeaderSkillNameModel>leaderSkillNameModel(LeaderSkillNameModel::find(skillID0));
    std::string skillName0 = leaderSkillNameModel->getName();
    std::string skillDecripstion0 = leaderSkillNameModel->getDescription();

    int skillID01 =  characterModel->getSkill();
    std::shared_ptr<SkillNameModel>skillNameModel(SkillNameModel::find(skillID01));
    std::string skillName01 = skillNameModel->getName();
    std::string skillDecripstion01 = skillNameModel->getDescription();

    int saikoro[9];
    saikoro[0] = playerCharactersModel->getDiceValue1();
    saikoro[1] = playerCharactersModel->getDiceValue2();
    saikoro[2] = playerCharactersModel->getDiceValue3();
    saikoro[3] = playerCharactersModel->getDiceValue4();
    saikoro[4] = playerCharactersModel->getDiceValue5();
    saikoro[5] = playerCharactersModel->getDiceValue6();
    saikoro[6] = playerCharactersModel->getDiceValue7();
    saikoro[7] = playerCharactersModel->getDiceValue8();
    saikoro[8] = playerCharactersModel->getDiceValue9();

    auto saikoro_bg1 = makeSprite("te_saikoro_bg1.png");
    addChild(saikoro_bg1, -1);
    saikoro_bg1->setPosition(leader_frame->getPosition() + Vec2(-leader_frame->getContentSize().width * 0.5 + saikoro_bg1->getContentSize().width * 0.5f, -leader_frame->getContentSize().height * 0.5f - 10 - saikoro_bg1->getContentSize().height * 0.5));

    auto saikoro_bg2 = makeSprite("te_saikoro_bg2.png");
    addChild(saikoro_bg2, -1);
    saikoro_bg2->setPosition(Vec2(winSize.width - 15 - saikoro_bg2->getContentSize().width * 0.5f, saikoro_bg1->getPositionY()));

    for (int i = 0; i < 9; i++) {
        if (saikoro[i] < 1) {
            saikoro[i] = 0;
        }

        auto saikoroImg = makeSprite(StringUtils::format("te_saikoro%d.png", saikoro[i]).c_str());
        addChild(saikoroImg);
        if (i < 6) {
            saikoroImg->setPosition(Vec2(14.0f + (saikoroImg->getContentSize().width + 4) * (0.5f + i) + 5 * i, saikoro_bg1->getPositionY() + 2));
        } else {
            saikoroImg->setPosition(Vec2(winSize.width - 18.0f - (saikoroImg->getContentSize().width + 4) * (0.5f + 8 - i) - 5 * (8 - i), saikoro_bg2->getPositionY() + 2));
        }
    }

    auto cost_bg = makeSprite("te_totalcost_bg.png");
    addChild(cost_bg, -1);
    cost_bg->setPosition(Vec2(winSize.width - 15 - cost_bg->getContentSize().width * 0.5f, leader_frame->getPositionY() - leader_frame->getContentSize().height * 0.5 + cost_bg->getContentSize().height * 0.5));

    auto costTitle = Label::createWithTTF("総コスト", FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(costTitle, 2);
    costTitle->setAnchorPoint(Vec2(0, 0.5f));
    costTitle->setPosition(cost_bg->getPosition() + Vec2(-cost_bg->getContentSize().width * 0.5f + 10, -11));


    auto costLabel = Label::createWithTTF(StringUtils::format("%3d / %3d", calculateTotalCost(playerTeamModel), RankModel::find(PLAYERCONTROLLER->_player->getRank())->getMaxCost()), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    addChild(costLabel, 2);
    costLabel->setAnchorPoint(Vec2(1, 0.5f));
    costLabel->setPosition(cost_bg->getPosition() + Vec2(cost_bg->getContentSize().width * 0.5f - 10, -11));

    if (calculateTotalCost(playerTeamModel) > RankModel::find(PLAYERCONTROLLER->_player->getRank())->getMaxCost()) {
        costTitle->runAction(RepeatForever::create(Sequence::create(TintTo::create(0.8, 255, 0, 0), DelayTime::create(1.2), TintTo::create(0.8, 255, 255, 255), NULL)));
        costLabel->runAction(RepeatForever::create(Sequence::create(TintTo::create(0.8, 255, 0, 0), DelayTime::create(1.2), TintTo::create(0.8, 255, 255, 255), NULL)));
    } else {
        costTitle->enableShadow();
        costLabel->enableShadow();
    }

    auto exerciseBg = makeSprite("te_status_bg.png");
    addChild(exerciseBg, -1);
    exerciseBg->setPosition(Vec2(14.0f + exerciseBg->getContentSize().width * 0.5f, saikoro_bg1->getPositionY() - saikoro_bg1->getContentSize().height / 2 - 10 - exerciseBg->getContentSize().height * 0.5));

    auto exerciseLabel = Label::createWithTTF("馬力", FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(exerciseLabel, 1);
    exerciseLabel->setAnchorPoint(Vec2(0, 0.5));
    exerciseLabel->enableShadow();
    exerciseLabel->setPosition(exerciseBg->getPosition() + Vec2(-exerciseBg->getContentSize().width / 2 + 10, -11));

    auto exerciseValue = Label::createWithTTF(FormatWithCommas(calculateTotalPower(playerTeamModel)), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    addChild(exerciseValue, 1);
    exerciseValue->setAnchorPoint(Vec2(1, 0.5));
    exerciseValue->enableShadow();
    exerciseValue->setPosition(exerciseBg->getPosition() + Vec2(exerciseBg->getContentSize().width / 2 - 10, -11));


    auto techniqueBg = makeSprite("te_status_bg.png");
    addChild(techniqueBg, -1);
    techniqueBg->setPosition(Vec2(winSize.width * 0.5f, exerciseBg->getPositionY()));

    auto techniqueLabel = Label::createWithTTF("技量", FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(techniqueLabel, 1);
    techniqueLabel->setAnchorPoint(Vec2(0, 0.5));
    techniqueLabel->enableShadow();
    techniqueLabel->setPosition(techniqueBg->getPosition() + Vec2(-techniqueBg->getContentSize().width / 2 + 10, -11));

    auto techniqueValue = Label::createWithTTF(FormatWithCommas(calculateTotalTechnique(playerTeamModel)), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    addChild(techniqueValue, 1);
    techniqueValue->setAnchorPoint(Vec2(1, 0.5));
    techniqueValue->enableShadow();
    techniqueValue->setPosition(techniqueBg->getPosition() + Vec2(techniqueBg->getContentSize().width / 2 - 10, -11));


    auto brakeBg = makeSprite("te_status_bg.png");
    addChild(brakeBg, -1);
    brakeBg->setPosition(Vec2(winSize.width - 14.0f - brakeBg->getContentSize().width * 0.5f, exerciseBg->getPositionY()));

    auto brakeLabel = Label::createWithTTF("忍耐", FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(brakeLabel, 1);
    brakeLabel->setAnchorPoint(Vec2(0, 0.5));
    brakeLabel->enableShadow();
    brakeLabel->setPosition(brakeBg->getPosition() + Vec2(-brakeBg->getContentSize().width / 2 + 10, -11));

    auto brakeValue = Label::createWithTTF(FormatWithCommas(calculateTotalBrake(playerTeamModel)), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    addChild(brakeValue, 1);
    brakeValue->setAnchorPoint(Vec2(1, 0.5));
    brakeValue->enableShadow();
    brakeValue->setPosition(brakeBg->getPosition() + Vec2(brakeBg->getContentSize().width / 2 - 10, -11));

    auto leader_des_bg = makeSprite("te_leader_description_bg.png");
    addChild(leader_des_bg, -1);
    leader_des_bg->setPosition(Vec2(winSize.width / 2, exerciseBg->getPositionY() - exerciseBg->getContentSize().height * 0.5f - 10 - leader_des_bg->getContentSize().height * 0.5f));

    auto support_des_bg = makeSprite("te_support_description_bg.png");
    addChild(support_des_bg, -1);
    support_des_bg->setPosition(Vec2(winSize.width * 0.5f, leader_des_bg->getPositionY() - leader_des_bg->getContentSize().height * 0.5 - 5 - support_des_bg->getContentSize().height * 0.5f));

    auto nameLabel0 = Label::createWithTTF("リーダースキル", FONT_NAME_2, 25, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(nameLabel0, 1);
    nameLabel0->setAnchorPoint(Vec2(0, 0.5f));
    nameLabel0->setColor(Color3B(141, 250, 255));
    nameLabel0->enableShadow();
    nameLabel0->setPosition(leader_des_bg->getPosition() + Vec2(-leader_des_bg->getContentSize().width * 0.5 + 10, 13 - 12.5));

    auto skillNameLabel0 = Label::createWithTTF(skillName0.c_str(), FONT_NAME_2, 25, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(skillNameLabel0, 1);
    skillNameLabel0->setAnchorPoint(Vec2(0, 0.5f));
    skillNameLabel0->enableShadow();
    skillNameLabel0->setPosition(nameLabel0->getPosition() + Vec2(nameLabel0->getContentSize().width + 45, 0));

    auto skillDecripstionLabel0 = Label::createWithTTF(skillDecripstion0.c_str(), FONT_NAME_2, 19, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(skillDecripstionLabel0, 1);
    skillDecripstionLabel0->setAnchorPoint(Vec2(0, 0.5f));
    skillDecripstionLabel0->setColor(Color3B::BLACK);
    skillDecripstionLabel0->setPosition(leader_des_bg->getPosition() + Vec2(-leader_des_bg->getContentSize().width * 0.5 + 10, -13 - 9.5));


    float height = 86.0f;
    auto nameLabel1 = Label::createWithTTF(name0.c_str(), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(nameLabel1, 1);
    nameLabel1->setAnchorPoint(Vec2(0, 0.5f));
    nameLabel1->setColor(COLOR_YELLOW);
    nameLabel1->enableShadow();
    nameLabel1->setPosition(support_des_bg->getPosition() + Vec2(-support_des_bg->getContentSize().width * 0.5 + 10, height + 14 - 11));

    auto skillNameLabel1 = Label::createWithTTF(skillName01.c_str(), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(skillNameLabel1, 1);
    skillNameLabel1->setAnchorPoint(Vec2(0, 0.5f));
    skillNameLabel1->enableShadow();
    skillNameLabel1->setPosition(nameLabel1->getPosition() + Vec2(nameLabel1->getContentSize().width + 45, 0));

    auto skillDecripstionLabel1 = Label::createWithTTF(skillDecripstion01.c_str(), FONT_NAME_2, 19, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    addChild(skillDecripstionLabel1, 1);
    skillDecripstionLabel1->setAnchorPoint(Vec2(0, 0.5f));
    skillDecripstionLabel1->setColor(Color3B::BLACK);
    skillDecripstionLabel1->setPosition(support_des_bg->getPosition() + Vec2(-support_des_bg->getContentSize().width * 0.5 + 10, height - 13 - 9.5));

    if (playerTeamModel->getSupport1CharacterId() >= 0) {
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport1CharacterId()]->getCharactersId()));
        std::shared_ptr<SkillNameModel>skillNameModel(SkillNameModel::find(characterModel->getSkill()));

        float height = 29.0f;
        nameLabel1 = Label::createWithTTF(characterModel->getCarName(), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(nameLabel1, 1);
        nameLabel1->setAnchorPoint(Vec2(0, 0.5f));
        nameLabel1->setColor(COLOR_YELLOW);
        nameLabel1->enableShadow();
        nameLabel1->setPosition(support_des_bg->getPosition() + Vec2(-support_des_bg->getContentSize().width * 0.5 + 10, height + 13 - 11));

        skillNameLabel1 = Label::createWithTTF(skillNameModel->getName(), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(skillNameLabel1, 1);
        skillNameLabel1->setAnchorPoint(Vec2(0, 0.5f));
        skillNameLabel1->enableShadow();
        skillNameLabel1->setPosition(nameLabel1->getPosition() + Vec2(nameLabel1->getContentSize().width + 45, 0));

        skillDecripstionLabel1 = Label::createWithTTF(skillNameModel->getDescription(), FONT_NAME_2, 19, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(skillDecripstionLabel1, 1);
        skillDecripstionLabel1->setAnchorPoint(Vec2(0, 0.5f));
        skillDecripstionLabel1->setColor(Color3B::BLACK);
        skillDecripstionLabel1->setPosition(support_des_bg->getPosition() + Vec2(-support_des_bg->getContentSize().width * 0.5 + 10, height - 13 - 9.5));
    }
    if (playerTeamModel->getSupport2CharacterId() >= 0) {
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport2CharacterId()]->getCharactersId()));
        std::shared_ptr<SkillNameModel>skillNameModel(SkillNameModel::find(characterModel->getSkill()));

        float height = -27.0f;
        nameLabel1 = Label::createWithTTF(characterModel->getCarName(), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(nameLabel1, 1);
        nameLabel1->setAnchorPoint(Vec2(0, 0.5f));
        nameLabel1->setColor(COLOR_YELLOW);
        nameLabel1->enableShadow();
        nameLabel1->setPosition(support_des_bg->getPosition() + Vec2(-support_des_bg->getContentSize().width * 0.5 + 10, height + 13 - 11));

        skillNameLabel1 = Label::createWithTTF(skillNameModel->getName(), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(skillNameLabel1, 1);
        skillNameLabel1->setAnchorPoint(Vec2(0, 0.5f));
        skillNameLabel1->enableShadow();
        skillNameLabel1->setPosition(nameLabel1->getPosition() + Vec2(nameLabel1->getContentSize().width + 45, 0));

        skillDecripstionLabel1 = Label::createWithTTF(skillNameModel->getDescription(), FONT_NAME_2, 19, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(skillDecripstionLabel1, 1);
        skillDecripstionLabel1->setAnchorPoint(Vec2(0, 0.5f));
        skillDecripstionLabel1->setColor(Color3B::BLACK);
        skillDecripstionLabel1->setPosition(support_des_bg->getPosition() + Vec2(-support_des_bg->getContentSize().width * 0.5 + 10, height - 13 - 9.5));
    }
    if (playerTeamModel->getSupport3CharacterId() >= 0) {
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport3CharacterId()]->getCharactersId()));
        std::shared_ptr<SkillNameModel>skillNameModel(SkillNameModel::find(characterModel->getSkill()));

        float height = -84.0f;
        nameLabel1 = Label::createWithTTF(characterModel->getCarName().c_str(), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(nameLabel1, 1);
        nameLabel1->setAnchorPoint(Vec2(0, 0.5f));
        nameLabel1->setColor(COLOR_YELLOW);
        nameLabel1->enableShadow();
        nameLabel1->setPosition(support_des_bg->getPosition() + Vec2(-support_des_bg->getContentSize().width * 0.5 + 10, height + 13 - 11));

        skillNameLabel1 = Label::createWithTTF(skillNameModel->getName(), FONT_NAME_2, 22, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(skillNameLabel1, 1);
        skillNameLabel1->setAnchorPoint(Vec2(0, 0.5f));
        skillNameLabel1->enableShadow();
        skillNameLabel1->setPosition(nameLabel1->getPosition() + Vec2(nameLabel1->getContentSize().width + 45, 0));

        skillDecripstionLabel1 = Label::createWithTTF(skillNameModel->getDescription(), FONT_NAME_2, 19, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(skillDecripstionLabel1, 1);
        skillDecripstionLabel1->setAnchorPoint(Vec2(0, 0.5f));
        skillDecripstionLabel1->setColor(Color3B::BLACK);
        skillDecripstionLabel1->setPosition(support_des_bg->getPosition() + Vec2(-support_des_bg->getContentSize().width * 0.5 + 10, height - 13 - 9.5));
    }
}

void TeamEditLayer::btSelectCharacterCallback(cocos2d::Ref* pSender)
{
    if (checkMenuReturn()) {
        return;
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    int tag = ((Node*)pSender)->getTag();

    dynamic_cast<TeamEditMainLayer*>(_mainLayer)->showLayer(TEI_CHARACTER_SELECT, tag);
}

int TeamEditLayer::calculateTotalPower(PlayerTeamModel* playerTeamModel)
{
    int result = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getLearderCharacterId()]->getExercise();
    if (playerTeamModel->getSupport1CharacterId() >= 0) {
        result += PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport1CharacterId()]->getExercise();
    }

    if (playerTeamModel->getSupport2CharacterId() >= 0) {
        result += PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport2CharacterId()]->getExercise();
    }

    if (playerTeamModel->getSupport3CharacterId() >= 0) {
        result += PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport3CharacterId()]->getExercise();
    }

    return result;
}

int TeamEditLayer::calculateTotalTechnique(PlayerTeamModel* playerTeamModel)
{
    int result = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getLearderCharacterId()]->getReaction();
    if (playerTeamModel->getSupport1CharacterId() >= 0) {
        result += PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport1CharacterId()]->getReaction();
    }

    if (playerTeamModel->getSupport2CharacterId() >= 0) {
        result += PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport2CharacterId()]->getReaction();
    }

    if (playerTeamModel->getSupport3CharacterId() >= 0) {
        result += PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport3CharacterId()]->getReaction();
    }

    return result;
}

int TeamEditLayer::calculateTotalBrake(PlayerTeamModel* playerTeamModel)
{
    int result = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getLearderCharacterId()]->getDecision();
    if (playerTeamModel->getSupport1CharacterId() >= 0) {
        result += PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport1CharacterId()]->getDecision();
    }

    if (playerTeamModel->getSupport2CharacterId() >= 0) {
        result += PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport2CharacterId()]->getDecision();
    }

    if (playerTeamModel->getSupport3CharacterId() >= 0) {
        result += PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport3CharacterId()]->getDecision();
    }

    return result;
}

int TeamEditLayer::calculateTotalCost(PlayerTeamModel* playerTeamModel)
{
    int result = CharacterModel::find(PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getLearderCharacterId()]->getCharactersId())->getCost();
    if (playerTeamModel->getSupport1CharacterId() >= 0) {
        result += CharacterModel::find(PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport1CharacterId()]->getCharactersId())->getCost();
    }

    if (playerTeamModel->getSupport2CharacterId() >= 0) {
        result += CharacterModel::find(PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport2CharacterId()]->getCharactersId())->getCost();
    }

    if (playerTeamModel->getSupport3CharacterId() >= 0) {
        result += CharacterModel::find(PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerTeamModel->getSupport3CharacterId()]->getCharactersId())->getCost();
        ;
    }

    return result;
}