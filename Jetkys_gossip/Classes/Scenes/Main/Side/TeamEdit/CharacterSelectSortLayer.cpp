#include "CharacterSelectSortLayer.h"
#include "FontDefines.h"

bool CharacterSelectSortLayer::init(const SortButtonCallback &sortButtonCallback, SortPlayerCharacter::KEY currentSelectedSortKey)
{
    if (!LayerPriority::init()) {
        return false;
    }
    showBackground();
    showButtons(sortButtonCallback, currentSelectedSortKey);
    return true;
}

void CharacterSelectSortLayer::showBackground()
{
    Size winSize = Director::getInstance()->getWinSize();

    LayerColor* bg = LayerColor::create(Color4B(0, 0, 0, 180));
    bg->setPosition(Vec2(winSize.width / 2 - bg->getContentSize().width / 2,
                         winSize.height / 2 - bg->getContentSize().height / 2));
    addChild(bg);

    ui::Scale9Sprite* sortBg = ui::Scale9Sprite::create("dialog_base_sort.png");
    sortBg->setPosition(Point(winSize.width / 2, winSize.height / 2));
    sortBg->setTag(TAG_SPRITE::SORT_BG);
    addChild(sortBg);

    Label* sorttext = Label::createWithTTF("表示順を選んでください", FONT_NAME_2, 32);
    sorttext->setPosition(Point(sortBg->getContentSize().width / 2,
                                sortBg->getContentSize().height - 30 - sorttext->getContentSize().height / 2));
    sorttext->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    sortBg->addChild(sorttext);
}

void CharacterSelectSortLayer::showButtons(const SortButtonCallback &sortButtonCallback, SortPlayerCharacter::KEY currentSelectedSortKey)
{
    Size winSize = Director::getInstance()->getWinSize();

    MenuPriority* menu = MenuPriority::create();
    addChild(menu);
    menu->setPositionY(winSize.height / 2 + 50);

    for (int i = 0; i < SortPlayerCharacter::SORT_LABELS.size(); i++) {
        std::string fileName = (i == currentSelectedSortKey) ? "sort_yellow_button.png" : "sort_pink_button.png";
        auto button = makeMenuItem(fileName,
                                   SortPlayerCharacter::SORT_LABELS.at(i),
                                   26,
                                   sortButtonCallback);
        button->setTag(i);
        button->setPosition(Vec2(-button->getContentSize().width - 17,
                                 button->getContentSize().height + 17) + Vec2((button->getContentSize().width + 17) * (i % 3), (-button->getContentSize().height - 17) * (i / 3))
                            );
        menu->addChild(button);
    }

    // キャンセル
    auto cancelBtn = makeMenuItem("popupBack.png", CC_CALLBACK_1(CharacterSelectSortLayer::cancelButtonCallback, this));
    cancelBtn->setPosition(Point(0, -210));
    auto canselMenu = MenuPriority::create();
    canselMenu->addChild(cancelBtn);
    addChild(canselMenu);
}

void CharacterSelectSortLayer::cancelButtonCallback(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    removeFromParent();
}