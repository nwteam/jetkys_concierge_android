#include "CharacterSelectPopupLayer.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include "StringDefines.h"
#include "CharacterModel.h"
#include "jCommon.h"

bool CharacterSelectPopupLayer::init(int playerCharacterIndex, ViewType type)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;
    _playerCharacterIndex = playerCharacterIndex;

    Size winSize = Director::getInstance()->getWinSize();

    auto bg = makeSprite("te_popup_bg.png");

    bg->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(bg);

    if (_playerCharacterIndex >= 0) {
        int characteID = PLAYERCONTROLLER->_playerCharactersSorted.at(_playerCharacterIndex)->getCharactersId();
        int level = PLAYERCONTROLLER->_playerCharactersSorted.at(_playerCharacterIndex)->getLevel();

        std::shared_ptr<CharacterModel>model(CharacterModel::find(characteID));
        std::string name = model->getCarName();

        int exercise = PLAYERCONTROLLER->_playerCharactersSorted.at(_playerCharacterIndex)->getExercise();
        int reaction = PLAYERCONTROLLER->_playerCharactersSorted.at(_playerCharacterIndex)->getReaction();
        int decision = PLAYERCONTROLLER->_playerCharactersSorted.at(_playerCharacterIndex)->getDecision();

        auto image = makeSprite(StringUtils::format(IMAGE_CHARACTER_ICON, characteID).c_str());
        addChild(image);
        image->setPosition(Vec2(winSize.width / 2 - image->getContentSize().width / 2 - 20, winSize.height / 2 + 65 + image->getContentSize().height / 2));

        // level
        auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %d", level), FONT_NAME_2, 22);
        addChild(levelLabel, 4);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(image->getPosition() + Vec2(0, -image->getContentSize().height / 2 + 6 - 11));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);

        auto nameLabel = Label::createWithTTF(name, FONT_NAME_2, 30, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
        addChild(nameLabel);
        nameLabel->setAnchorPoint(Vec2(0.5, 0));
        nameLabel->setPosition(Vec2(winSize.width / 2, image->getPositionY() + image->getContentSize().height / 2  - 15));
        nameLabel->setColor(COLOR_YELLOW);
        nameLabel->enableShadow();

        auto statusLabel = Label::createWithTTF("馬力", FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(statusLabel);
        statusLabel->setPosition(image->getPosition() + Vec2(image->getContentSize().width / 2 + 15, 30 - 10));
        statusLabel->setAnchorPoint(Vec2(0, 0.5));
        statusLabel->enableShadow();

        auto statusValue = Label::createWithTTF(FormatWithCommas(exercise), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
        addChild(statusValue);
        statusValue->setPosition(statusLabel->getPosition() + Vec2(130,  0));
        statusValue->setAnchorPoint(Vec2(1, 0.5));
        statusValue->enableShadow();

        statusLabel = Label::createWithTTF("技量", FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(statusLabel);
        statusLabel->setPosition(image->getPosition() + Vec2(image->getContentSize().width / 2 + 15, -10));
        statusLabel->setAnchorPoint(Vec2(0, 0.5));
        statusLabel->enableShadow();

        statusValue = Label::createWithTTF(FormatWithCommas(reaction), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
        addChild(statusValue);
        statusValue->setPosition(statusLabel->getPosition() + Vec2(130,  0));
        statusValue->setAnchorPoint(Vec2(1, 0.5));
        statusValue->enableShadow();

        statusLabel = Label::createWithTTF("忍耐", FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
        addChild(statusLabel);
        statusLabel->setPosition(image->getPosition() + Vec2(image->getContentSize().width / 2 + 15, -30 - 10));
        statusLabel->setAnchorPoint(Vec2(0, 0.5));
        statusLabel->enableShadow();

        statusValue = Label::createWithTTF(FormatWithCommas(decision), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
        addChild(statusValue);
        statusValue->setPosition(statusLabel->getPosition() + Vec2(130,  0));
        statusValue->setAnchorPoint(Vec2(1, 0.5));
        statusValue->enableShadow();
    }

    auto menu = MenuPriority::create();
    menu->setPosition(Vec2(winSize.width / 2, winSize.height / 2 - 85));
    addChild(menu);

    // add button ok
    auto button = makeMenuItem("te_popup_button.png", CC_CALLBACK_1(CharacterSelectPopupLayer::callBackbtn, this));
    button->setTag(BT_CSP_OK);
    menu->addChild(button);
    // check leader

    Label* label;

    if (type == LEDER_SELECT) {
        label = Label::createWithTTF("リーダーにする", FONT_NAME_2, 28);
    } else {
        label = Label::createWithTTF("メンバーにする", FONT_NAME_2, 28);
    }
    button->addChild(label);
    label->setPosition(Vec2(button->getNormalImage()->getContentSize().width / 2,
                            button->getNormalImage()->getContentSize().height / 2 - 14));
    label->enableShadow();

    button = makeMenuItem("te_popup_button.png", CC_CALLBACK_1(CharacterSelectPopupLayer::callBackbtn, this));
    button->setTag(BT_CSP_DETAIL);
    menu->addChild(button);

    label = Label::createWithTTF("車なご詳細", FONT_NAME_2, 28);
    button->addChild(label);
    label->setPosition(Vec2(button->getNormalImage()->getContentSize().width / 2,
                            button->getNormalImage()->getContentSize().height / 2 - 14));
    label->enableShadow();

    button = makeMenuItem("te_popup_close_button.png", CC_CALLBACK_1(CharacterSelectPopupLayer::callBackbtn, this));
    button->setTag(BT_CSP_CLOSE);
    menu->addChild(button);

    menu->alignItemsVerticallyWithPadding(30);

    return true;
}

void CharacterSelectPopupLayer::callBackbtn(Ref* psender)
{
    int aIndex = ((Node*)psender)->getTag();

    PopUpDelegate* delegate = _delegate;
    removeFromParent();

    if (delegate != nullptr) {
        delegate->btCallback(aIndex, nullptr);
    }
}
