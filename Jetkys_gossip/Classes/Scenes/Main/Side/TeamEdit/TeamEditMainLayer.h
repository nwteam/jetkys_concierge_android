#ifndef __syanago__TeamEditMainLayer__
#define __syanago__TeamEditMainLayer__

#include "LayerPriority.h"
#include "DialogView.h"
#include "Loading.h"
#include "FontDefines.h"

enum TeamEditIndex {
    TEI_TEAM_EDIT = 100, TEI_CHARACTER_SELECT = 101
};

enum PlayerCharacterTag {
    PCT_LEADER = 110,
    PCT_SUPPORT1 = PCT_LEADER + 1,
    PCT_SUPPORT2 = PCT_LEADER + 2,
    PCT_SUPPORT3 = PCT_LEADER + 3
};


class MainScene;

class TeamEditMainLayer : public Layer {
public:
    bool init();
    CREATE_FUNC(TeamEditMainLayer);
    
    MainScene *_mainScene;
    
    void showHide(bool isShow);
    void showHideEnd();
    
    Label* _reportMess;
    
    /**
     *	@brief	遷移レイヤー
     *
     *	@param 	layerIndex 	teamEdit Index
     *	@param 	pcharacterTag 	pcharacterTag selected (場合：layerIndex == TEI_CHARACTER_SELECT)
     */
    void showLayer(int layerIndex, int pcharacterTag = -1);

    
    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
    virtual void onTouchCancelled(Touch *touch, Event *unused_event);
    
    void showDetailPlayerCharacter(int pcID);
    void updateLeaderCharacterImg();
    
    void update(float dt);
    
    bool isMoving() {
        return _isMoving;
    }
    
    bool isShow() {
        return _isShow;
    }
    
private:
    enum ACTION_TAG{
        BASE_HIDE = 1000,
    };
    int _layerIndex;
    bool _isShow;
    Vec2 _deltaPoint;
    int _touchID;
    Sprite * _holdingBase;
    Label * _holdingLabel;
    Label * _holdingTile;
    
    //QuynhNM 20141101
    bool _isMoving;
};

#endif /* defined(__syanago__TeamEditMainLayer__) */
