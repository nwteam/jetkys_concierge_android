#ifndef __syanago__CharacterSelectSortLayer__
#define __syanago__CharacterSelectSortLayer__

#include <functional>
#include "create_func.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "LayerPriority.h"
#include "SyanagoBaseLayer.h"
#include "SortPlayerCharacter.h"

class CharacterSelectSortLayer : public create_func<CharacterSelectSortLayer>, public LayerPriority
{
public:
    typedef std::function<void(Ref*)> SortButtonCallback;
    bool init(const SortButtonCallback& sortButtonCallback, SortPlayerCharacter::KEY currentSelectedSortKey);
    using create_func::create;
    
private:
    void showBackground();
    void showButtons(const SortButtonCallback& sortButtonCallback, SortPlayerCharacter::KEY currentSelectedSortKey);
    void cancelButtonCallback(Ref *pSender);
    
    enum TAG_SPRITE{
        SORT_BG = 0
    };
};

#endif /* defined(__syanago__CharacterSelectSortLayer__) */
