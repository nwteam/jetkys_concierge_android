#ifndef __syanago__TeamCharacterSelectLayer__
#define __syanago__TeamCharacterSelectLayer__

#include "SyanagoBaseLayer.h"
#include "extensions/cocos-ext.h"
#include "CharacterSelectPopupLayer.h"
#include "CharacterSelectSortLayer.h"
#include "DialogView.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC_EXT;
using namespace SyanagoAPI;

struct TeamComposition {
    TeamComposition(int team_no, int learder_character_id, int support1_character_id, int support2_character_id, int support3_character_id) {
        _team_no = team_no;
        _learder_character_id = learder_character_id;
        _support1_character_id = support1_character_id;
        _support2_character_id = support2_character_id;
        _support3_character_id = support3_character_id;
    }
    int _team_no; // < 0 -> null
    int _learder_character_id;
    int _support1_character_id; // < 0 -> null
    int _support2_character_id; // < 0 -> null
    int _support3_character_id; // < 0 -> null
};

class TeamCharacterSelectLayer: public SyanagoBaseLayer, public DialogDelegate {
public:
    TeamCharacterSelectLayer();
    virtual ~TeamCharacterSelectLayer();

    static int _playerCharacterTag;

    static TeamCharacterSelectLayer* createWithPlayerCharacterTag(int pcharacterTag);

    bool init();
    CREATE_FUNC(TeamCharacterSelectLayer);

    // TeamEditMainLayer *_mainLayer;

    void btMenuItemCallback(cocos2d::Ref* pSender);
    void btItemSelectedCallback(Ref* pSender);

    void scrollViewDidScroll(ScrollView* view);
    void scrollViewDidZoom(ScrollView* view);

    void btCallback(int btIndex, void* object);

    void showDatas();

    CC_SYNTHESIZE(int, _pcSelectedIndex, PCSelectecedIndex) // save index of player character selected

    int _costTotal;
    int _costMax;
    int _costMinus;

    bool checkCostOver(int characterID);

private:
    DefaultProgress* _progress;
    RequestAPI* _request;

    void sortCharacter();
    void characterSortButtonCallback(Ref* pSender);

    enum TAG_LAYER {
        CHARACTER_SORT = 0
    };

    void responseSetTeamComposition(Json* response);
};

#endif /* defined(__syanago__TeamCharacterSelectLayer__) */
