#include "TeamCharacterSelectLayer.h"
#include "TeamEditMainLayer.h"
#include "PlayerController.h"
#include "StringDefines.h"
#include "CharacterModel.h"
#include "RankModel.h"
#include "CharacterIconSprite.h"

int TeamCharacterSelectLayer::_playerCharacterTag = -1;

TeamCharacterSelectLayer::TeamCharacterSelectLayer():
    _progress(nullptr), _request(nullptr)
{
    _title = "選択";
    _pcSelectedIndex = -1;
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}

TeamCharacterSelectLayer::~TeamCharacterSelectLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

TeamCharacterSelectLayer* TeamCharacterSelectLayer::createWithPlayerCharacterTag(int pcharacterTag)
{
    auto layer = new TeamCharacterSelectLayer();
    if (pcharacterTag != -1) {
        _playerCharacterTag = pcharacterTag;
    }

    if (_playerCharacterTag == PCT_LEADER) {
        layer->_title = "リーダー選択";
    } else {
        layer->_title = "メンバー選択";
    }

    layer->init();
    layer->autorelease();

    return layer;
}

bool TeamCharacterSelectLayer::init()
{
    if (!SyanagoBaseLayer::init()) {
        return false;
    }
    showSortMenu();

    _costTotal = 0;
    _costMinus = 0;
    int pcIdx = PLAYERCONTROLLER->_playerTeams->getLearderCharacterId();
    if (pcIdx > 0) {
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(PLAYERCONTROLLER->_playerCharacterModels[pcIdx]->getCharactersId()));
        _costTotal += characterModel->getCost();
        if (_playerCharacterTag == PCT_LEADER) {
            _costMinus = characterModel->getCost();
        }
    }

    pcIdx = PLAYERCONTROLLER->_playerTeams->getSupport1CharacterId();
    if (pcIdx > 0) {
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(PLAYERCONTROLLER->_playerCharacterModels[pcIdx]->getCharactersId()));
        _costTotal += characterModel->getCost();
        if (_playerCharacterTag == PCT_SUPPORT1) {
            _costMinus = characterModel->getCost();
        }
    }

    pcIdx = PLAYERCONTROLLER->_playerTeams->getSupport2CharacterId();
    if (pcIdx > 0) {
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(PLAYERCONTROLLER->_playerCharacterModels[pcIdx]->getCharactersId()));
        _costTotal += characterModel->getCost();
        if (_playerCharacterTag == PCT_SUPPORT2) {
            _costMinus = characterModel->getCost();
        }
    }

    pcIdx = PLAYERCONTROLLER->_playerTeams->getSupport3CharacterId();
    if (pcIdx > 0) {
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(PLAYERCONTROLLER->_playerCharacterModels[pcIdx]->getCharactersId()));
        _costTotal += characterModel->getCost();
        if (_playerCharacterTag == PCT_SUPPORT3) {
            _costMinus = characterModel->getCost();
        }
    }


    int rank = PLAYERCONTROLLER->_player->getRank();
    std::shared_ptr<RankModel>rankModel(RankModel::find(rank));
    _costMax = rankModel->getMaxCost();


    // in select layer, Use _playerCharactersSorted, not use _playerCharacters
    PLAYERCONTROLLER->resetPlayerCharacterSorted();

    sortCharacter();

    Size winSize = Director::getInstance()->getWinSize();
    auto teropbg = makeSprite("telop_bg.png");
    addChild(teropbg);
    teropbg->setPosition(Point(winSize.width / 2, teropbg->getContentSize().height / 2));

    return true;
}

void TeamCharacterSelectLayer::sortCharacter()
{
    SortPlayerCharacter::sort(PLAYERCONTROLLER->currentSelectedSortCharacterKey);

    _sortLabel->setString(SortPlayerCharacter::getLabelName(PLAYERCONTROLLER->currentSelectedSortCharacterKey));
    if (_sortLabel->getString() == "メーカー") {
        _sortLabel->setString("メーカ");
    } else if (_sortLabel->getString() == "お気に入り") {
        _sortLabel->setString("お気に");
    }
    showDatas();
}

void TeamCharacterSelectLayer::showDatas()
{
    _numberOfCharacter = (int)PLAYERCONTROLLER->_playerCharactersSorted.size();

    SyanagoBaseLayer::showDatas();

    auto menu = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    menu->setPosition(Vec2(0, _container->getContentSize().height));
    menu->setAnchorPoint(Vec2::ZERO);
    menu->_scrollView = _scrollView;
    _container->addChild(menu, 2);

    const bool hasEmpty = _playerCharacterTag != PCT_LEADER;
    if (hasEmpty) {
        auto imageFrame = makeMenuItem("garage_icon_empty.png", CC_CALLBACK_1(TeamCharacterSelectLayer::btItemSelectedCallback, this));
        imageFrame->setPosition(Vec2(kSizeCharacterIcon.width * (0.5f), -kSizeCharacterIcon.height * (0.5f)));
        imageFrame->setTag(kPlayerCharacterTagEmpty);
        menu->addChild(imageFrame);
    }

    for (int i = 0; i < _numberOfCharacter; i++) {
        CharacterIconSprite* icon = CharacterIconSprite::create(i, CC_CALLBACK_1(TeamCharacterSelectLayer::btItemSelectedCallback, this), hasEmpty);
        icon->showStatusLabel(i);

        int characterID = PLAYERCONTROLLER->_playerCharactersSorted.at(i)->getCharactersId();
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(characterID));
        int cost = _costTotal - _costMinus + characterModel->getCost();
        if (icon->checkSyanagoIsNew() == CharacterIconSprite::USED_STATUS::USED) {
            icon->setEnabled(false);
        } else if (cost > _costMax) {
            auto mask = makeSprite("te_cost_over_mask.png");
            mask->setPosition(Vec2(mask->getContentSize().width / 2, mask->getContentSize().height / 2));
            icon->addChild(mask);
        }
        menu->addChild(icon);
    }
}

void TeamCharacterSelectLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    int tag = ((Node*)pSender)->getTag();
    if (tag == bt_sort_bt) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        // sort
        CharacterSelectSortLayer* popup = CharacterSelectSortLayer::create(CC_CALLBACK_1(TeamCharacterSelectLayer::characterSortButtonCallback, this), PLAYERCONTROLLER->currentSelectedSortCharacterKey);
        popup->setTag(TAG_LAYER::CHARACTER_SORT);
        addChild(popup, ZORDER_POPUP);
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        dynamic_cast<TeamEditMainLayer*>(_mainLayer)->showLayer(TEI_TEAM_EDIT);
    }
}

void TeamCharacterSelectLayer::btItemSelectedCallback(cocos2d::Ref* pSender)
{
    if (checkMenuReturn()) {
        return;
    }

    int tag = ((Node*)pSender)->getTag();
    _pcSelectedIndex = tag - kPlayerCharacterTagStart;

    if (static_cast<MenuItemSprite*>(pSender)->getChildByTag(kPlayerCharacterTagStart + _pcSelectedIndex)) {
        static_cast<MenuItemSprite*>(pSender)->removeChildByTag(kPlayerCharacterTagStart + _pcSelectedIndex);
        PLAYERCONTROLLER->_playerCharactersSorted.at(_pcSelectedIndex)->saveIsNew(0);
    }

    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    if (_pcSelectedIndex < 0) {
        btCallback(BT_CSP_OK, nullptr);
        return;
    } else {
        // check cost over
        if (checkCostOver(PLAYERCONTROLLER->_playerCharactersSorted.at(_pcSelectedIndex)->getCharactersId())) {
            // show detail
            btCallback(BT_CSP_DETAIL, nullptr);
        } else {
            // show confirm popup
            ViewType type;
            switch (_playerCharacterTag) {
            case PCT_LEADER:
                type = LEDER_SELECT;
                break;
            default:
                type = MENMBER_SELECT;
                break;
            }

            auto popup = CharacterSelectPopupLayer::create(_pcSelectedIndex, type);
            addChild(popup, ZORDER_POPUP);
            popup->_delegate = this;
        }
    }
}

void TeamCharacterSelectLayer::btCallback(int btIndex, void* object)
{
    if (btIndex == BT_CSP_OK) {
        int pcIDSelected = _pcSelectedIndex >= 0 ? PLAYERCONTROLLER->_playerCharactersSorted.at(_pcSelectedIndex)->getID() : -1;
        if (pcIDSelected > 0) {
            SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        }
        TeamComposition teamComp = TeamComposition(PLAYERCONTROLLER->_playerTeams->getTeamNo(),
                                                   PLAYERCONTROLLER->_playerTeams->getLearderCharacterId(),
                                                   PLAYERCONTROLLER->_playerTeams->getSupport1CharacterId(),
                                                   PLAYERCONTROLLER->_playerTeams->getSupport2CharacterId(),
                                                   PLAYERCONTROLLER->_playerTeams->getSupport3CharacterId()
                                                   );
        // 無い場合、外すを選択 -> サーバーに通信しない
        if (_playerCharacterTag == PCT_LEADER) {
            teamComp._learder_character_id = pcIDSelected;
        } else if (_playerCharacterTag == PCT_SUPPORT1) {
            if (_pcSelectedIndex < 0 && teamComp._support1_character_id <= 0) {
                dynamic_cast<TeamEditMainLayer*>(_mainLayer)->showLayer(TEI_TEAM_EDIT);
                return;
            }
            teamComp._support1_character_id = pcIDSelected;
        } else if (_playerCharacterTag == PCT_SUPPORT2) {
            if (_pcSelectedIndex < 0 && teamComp._support2_character_id <= 0) {
                dynamic_cast<TeamEditMainLayer*>(_mainLayer)->showLayer(TEI_TEAM_EDIT);
                return;
            }
            teamComp._support2_character_id = pcIDSelected;
        } else if (_playerCharacterTag == PCT_SUPPORT3) {
            if (_pcSelectedIndex < 0 && teamComp._support3_character_id <= 0) {
                dynamic_cast<TeamEditMainLayer*>(_mainLayer)->showLayer(TEI_TEAM_EDIT);
                return;
            }
            teamComp._support3_character_id = pcIDSelected;
        }

        std::vector<TeamComposition>datas;
        datas.push_back(teamComp);


        std::string dataString = "";
        const std::string SET_TEAM_COMPOSITION_INFO = "{\"team_no\": %s,\"learder_character_id\": %d,\"support1_character_id\": %s,\"support2_character_id\": %s,\"support3_character_id\": %s}";
        for (auto data : datas) {
            if (dataString != "") {
                dataString += ",";
            }
            dataString += StringUtils::format(SET_TEAM_COMPOSITION_INFO.c_str(),
                                              data._team_no < 0 ? "null" : StringUtils::format("%d", data._team_no).c_str(),
                                              data._learder_character_id,
                                              data._support1_character_id < 0 ? "null" : StringUtils::format("%d", data._support1_character_id).c_str(),
                                              data._support2_character_id < 0 ? "null" : StringUtils::format("%d", data._support2_character_id).c_str(),
                                              data._support3_character_id < 0 ? "null" : StringUtils::format("%d", data._support3_character_id).c_str());
        }
        _progress->onStart();
        _request->setTeamComposition(dataString, CC_CALLBACK_1(TeamCharacterSelectLayer::responseSetTeamComposition, this));
    } else if (btIndex == BT_CSP_DETAIL) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        // show Player character detail
        int characteID = PLAYERCONTROLLER->_playerCharactersSorted.at(_pcSelectedIndex)->getID();
        // int id = PLAYERCONTROLLER->_playerPartsSorted.at(_pcSelectedIndex)->getID();
        dynamic_cast<TeamEditMainLayer*>(_mainLayer)->showDetailPlayerCharacter(characteID);
        _pcSelectedIndex = -1;
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }

    _pcSelectedIndex = -1;
}

void TeamCharacterSelectLayer::responseSetTeamComposition(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "set_team_composition");

    std::string result = Json_getString(jsonData1, "result", "false");
    if (result == "true") {
        PLAYERCONTROLLER->updatePlayerTeam(Json_getItem(Json_getItem(jsonData1, "data"), "player_teams"));
        int leaderCharacterID = (PLAYERCONTROLLER->_playerTeams)->getLearderCharacterId();
        int mstCharacterId = PLAYERCONTROLLER->_playerCharacterModels[leaderCharacterID]->getCharactersId();
        UserDefault::getInstance()->setIntegerForKey("LeaderCharaMst", mstCharacterId);
        dynamic_cast<TeamEditMainLayer*>(_mainLayer)->updateLeaderCharacterImg();
        dynamic_cast<TeamEditMainLayer*>(_mainLayer)->showLayer(TEI_TEAM_EDIT);
    }
}

bool TeamCharacterSelectLayer::checkCostOver(int characterID)
{
    int totalCost = _costTotal;
    totalCost -= _costMinus;
    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(characterID));
    totalCost += characterModel->getCost();

    if (totalCost > _costMax) {
        return true;
    }

    return false;
}

void TeamCharacterSelectLayer::scrollViewDidScroll(ScrollView* view)
{
    _scrollBar->updateScrollPoint(view->getContentOffset().y);
}

void TeamCharacterSelectLayer::scrollViewDidZoom(ScrollView* view)
{}

void TeamCharacterSelectLayer::characterSortButtonCallback(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    PLAYERCONTROLLER->currentSelectedSortCharacterKey = static_cast<SortPlayerCharacter::KEY>(static_cast<Node*>(pSender)->getTag());
    sortCharacter();
    removeChildByTag(TAG_LAYER::CHARACTER_SORT);
}








