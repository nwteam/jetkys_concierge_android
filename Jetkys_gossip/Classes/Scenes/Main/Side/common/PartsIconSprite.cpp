#include "PartsIconSprite.h"

#include "PlayerController.h"
#include "PartModel.h"
#include "FontDefines.h"
#include "jCommon.h"

PartsIconSprite::PartsIconSprite() {};

PartsIconSprite* PartsIconSprite::create(int index, const OnResponceCallback& callback, bool hasEmpty)
{
    PartsIconSprite* btn = new (std::nothrow) PartsIconSprite();
    PlayerPartModel* playerPartModel = PLAYERCONTROLLER->_playerPartsSorted.at(index);

    std::string frameName = StringUtils::format("%d_pag.png", playerPartModel->getPartsId());
    Sprite* selected = Sprite::createWithSpriteFrameName(frameName);
    selected->setColor(Color3B::GRAY);
    Sprite* disabled = Sprite::createWithSpriteFrameName(frameName);
    disabled->setColor(Color3B::GRAY);

    if (btn && btn->initWithNormalSprite(Sprite::createWithSpriteFrameName(frameName), selected, disabled, callback)) {
        btn->setTag(TAG_START + index);
        if (hasEmpty) {
            index += 1;
        }
        const int MAX_COLUMN = 5;
        int column = index % MAX_COLUMN;
        int row = index / MAX_COLUMN;
        const Size SIZE_TOTAL = Size(118.0f, 118.0f);
        const float MARGIN = 0.5f;
        btn->setPosition(Vec2(SIZE_TOTAL.width * (MARGIN + column), -SIZE_TOTAL.height * (MARGIN + row)));
        btn->initialize(playerPartModel);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void PartsIconSprite::initialize(PlayerPartModel* playerPartModel)
{
    _playerPartModel = playerPartModel;
    setUserData(playerPartModel);
    showLabelsOnButtom();
}


void PartsIconSprite::showLabelsOnButtom()
{
    std::shared_ptr<PartModel>partsModel(PartModel::find(_playerPartModel->getPartsId()));

    std::string bottomStr = "";
    switch (PLAYERCONTROLLER->currentSelectedSortPartsKey) {
    case SortPlayerParts::POWER: {
        bottomStr = StringUtils::format("%d", partsModel->getExercise());
        auto levelLabel = Label::createWithTTF(bottomStr, FONT_NAME_2, 16);
        addChild(levelLabel, 4);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(Vec2(getContentSize().width / 2, -2));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
        levelLabel->enableShadow();
        break;
    }
    case SortPlayerParts::TECHNIQUE: {
        bottomStr = StringUtils::format("%d", partsModel->getReaction());
        auto levelLabel = Label::createWithTTF(bottomStr, FONT_NAME_2, 16);
        addChild(levelLabel, 4);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(Vec2(getContentSize().width / 2, -2));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
        levelLabel->enableShadow();
        break;
    }
    case SortPlayerParts::BRAKE: {
        bottomStr = StringUtils::format("%d", partsModel->getDecision());
        auto levelLabel = Label::createWithTTF(bottomStr, FONT_NAME_2, 16);
        addChild(levelLabel, 4);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(Vec2(getContentSize().width / 2, -2));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
        levelLabel->enableShadow();
        break;
    }

    default: {
        // default rarity
        int rarity = partsModel->getRarity();
        auto starNode = createRarityNode(rarity);
        addChild(starNode, 5);
        starNode->setPosition(Vec2(getContentSize().width / 2, 0));
        break;
    }
    }
}


Node* PartsIconSprite::createRarityNode(int rarity)
{
    auto node = Node::create();

    for (int i = 0; i < rarity; i++) {
        auto start = Sprite::create("rarity.png");
        node->addChild(start);
        if (rarity % 2 == 1) {
            start->setPosition(Vec2((start->getContentSize().width - 5) * (i - rarity / 2), 0));
        } else {
            int j = i - rarity / 2;
            start->setPosition(Vec2((start->getContentSize().width - 5) * (0.5f + j), 0));
        }
    }
    node->setContentSize(Size(24 * rarity, 23));

    return node;
}


void PartsIconSprite::showStatusLabel(int index)
{
    const USED_STATUS STATUS = checkPartsIsNew();
    if (STATUS == USED_STATUS::NEW || STATUS == USED_STATUS::USED) {
        Sprite* label;
        int tag = 2 * TAG_START + 2 * index;
        if (STATUS == USED_STATUS::NEW) {
            label = Sprite::create("garage_list_new_label.png");
        } else if (STATUS == USED_STATUS::USED) {
            label = Sprite::create("garage_list_using_label.png");
            tag += 1;
        }
        label->setPosition(Vec2(98 - 23, 99 - 15));
        label->setTag(tag);
        addChild(label, 4);
    }
}

void PartsIconSprite::showUsedLabel(int index)
{
    Sprite* label = Sprite::create("garage_list_using_label.png");
    label->setPosition(Vec2(98 - 23, 99 - 15));
    label->setTag(2 * TAG_START + 2 * index + 1);
    addChild(label, 4);
}


PartsIconSprite::USED_STATUS PartsIconSprite::checkPartsIsNew()
{
    if (checkCharacterInUse(PLAYERCONTROLLER->getPlayerPartIdsUsed(), _playerPartModel->getID())) {
        return USED_STATUS::USED;
    } else if (_playerPartModel->getIsNew() == 1) {
        return USED_STATUS::NEW;
    } else {
        return USED_STATUS::NORMAL;
    }
}


bool PartsIconSprite::checkCharacterInUse(std::vector<int>IDs, int ID)
{
    std::vector<int>::iterator it;
    it = find (IDs.begin(), IDs.end(), ID);
    return it != IDs.end();
}






