#ifndef __syanago__SyanagoBaseLayer__
#define __syanago__SyanagoBaseLayer__

#include "LayerPriority.h"
#include "DialogView.h"
#include "Loading.h"
#include "FontDefines.h"
#include "jCommon.h"
#include "extensions/cocos-ext.h"
#include "PlayerController.h"
#include "PopUpDelegate.h"
#include "ScrollBar.h"
#include "SoundHelper.h"
#include "SortPlayerCharacter.h"
#include "SortPlayerParts.h"

USING_NS_CC_EXT;


#define kPrioritySyanagoBaseLayer -500
#define kPrioritySyanagoBaseScrollView kPrioritySyanagoBaseLayer - 1
#define kPrioritySyanagoBaseMenu kPrioritySyanagoBaseLayer - 2
#define kPrioritySyanagoBaseSaleMenu kPrioritySyanagoBaseMenu - 2

#define kPlayerCharacterTagEmpty 999
#define kPlayerCharacterTagStart 1000
#define kPlayerBageTagStart 100

#define bt_sort_bt 123
#define bt_back_bt 12345

#define kColumn 5
#define kSizeCharacterIcon Size(118.0f, 118.0f)

enum SAleActionTag{
    SALE_ACT,RESET_ACT
};


class SyanagoBaseLayer : public LayerPriority, public ScrollViewDelegate, public PopUpDelegate {
public:
    static Node *createRarityNode(int rarity);
    
    SyanagoBaseLayer();
    virtual ~SyanagoBaseLayer();
    
    bool init();
    CREATE_FUNC(SyanagoBaseLayer);
    
    virtual void showHeader();
    
    virtual void showSortMenu();
    virtual void showDatas();
    
    virtual void btMenuItemCallback(Ref *pSender);
    
    virtual void btCallback(int btIndex, void *object) { };
    
    virtual void scrollViewDidScroll(ScrollView* view){};
    virtual void scrollViewDidZoom(ScrollView* view) {};
    
    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
    virtual void onTouchCancelled(Touch *touch, Event *unused_event);
    
    bool checkMenuReturn();
    
    Layer *_mainLayer;
    
    std::string _title;
    Sprite *_titleBg;
    
    DialogView *_dialog;
    
    ScrollViewPriority *_scrollView;
    ScrollBar* _scrollBar;
    Layer *_container;
    int _numberOfCharacter;

    Label* _titleLabel;
    Label* _sortLabel;
    
    bool _isMove;
    
};

#endif /* defined(__syanago__SyanagoBaseLayer__) */
