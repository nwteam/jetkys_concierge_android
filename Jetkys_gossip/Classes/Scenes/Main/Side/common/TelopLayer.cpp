#include "TelopLayer.h"
#include "jCommon.h"
#include "FontDefines.h"

TelopLayer* TelopLayer::createWithmessage(std::string &message)
{
    auto layer = new TelopLayer;
    layer->initialize(message);
    layer->autorelease();

    return layer;
}

bool TelopLayer::initialize(std::string &message)
{
    if (!Layer::init()) {
        return false;
    }

    Size winSize = Director::getInstance()->getWinSize();

    auto background = makeSprite("telop_bg.png");
    background->setPosition(Point(winSize.width / 2, background->getContentSize().height / 2));
    addChild(background);

    Label* label = Label::createWithTTF(message, FONT_NAME_2, 22);
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->setColor(Color3B::WHITE);
    label->setAnchorPoint(Vec2(0, 0.75f));
    addChild(label);

    float messageWidth = label->getContentSize().width;
    messageWidth += winSize.width;

    label->setPosition(Vec2(winSize.width, background->getContentSize().height / 2));
    float duration = TEROP_SPEED * (messageWidth) / 1500;
    label->runAction(RepeatForever::create(Sequence::create(MoveBy::create(duration, Vec2(-messageWidth, 0)), CallFuncN::create([this](Ref* sender) {
        Size winSize = Director::getInstance()->getWinSize();
        auto node = (Node*) sender;
        node->setPositionX(winSize.width);
    }), NULL)));

    return true;
}










