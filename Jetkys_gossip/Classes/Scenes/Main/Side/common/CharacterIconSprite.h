#ifndef __syanago__CharacterIconSprite__
#define __syanago__CharacterIconSprite__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "PlayerCharactersModel.h"

USING_NS_CC;


class CharacterIconSprite : public MenuItemSprite
{
public:
    enum USED_STATUS {
        NORMAL = 0,
        USED,
        NEW
    };
    const static int TAG_START = 1000;
    const Size SIZE = Size(98,99);
    
    CharacterIconSprite();
    
    typedef std::function<void(Ref* ref)> OnResponceCallback;
    static CharacterIconSprite* create(int index, const OnResponceCallback& callback, bool hasEmpty = false);
    void initialize(PlayerCharactersModel* playerCharactersModel);
    
    void showStatusLabel(int index);
    USED_STATUS checkSyanagoIsNew();
    
private:
    void showLabelsOnButtom();
    void showFavoriteIcon();
    
    bool checkCharacterInUse(std::vector<int>IDs, int ID);
    Node* createRarityNode(int rarity);
    
    PlayerCharactersModel* _playerCharactersModel;
    
};


#endif /* defined(__syanago__CharacterIconSprite__) */
