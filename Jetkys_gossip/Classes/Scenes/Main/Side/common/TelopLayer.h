#ifndef __syanago__TelopLayer__
#define __syanago__TelopLayer__

#include "cocos2d.h"
USING_NS_CC;

class TelopLayer : public Layer {
public:
    static TelopLayer *createWithmessage(std::string &mess);
    bool initialize(std::string& message);
    
};

#endif /* defined(__syanago__TelopLayer__) */
