#include "CharacterIconSprite.h"
#include "PlayerController.h"
#include "CharacterModel.h"
#include "FontDefines.h"
#include "jCommon.h"

CharacterIconSprite::CharacterIconSprite() {};

CharacterIconSprite* CharacterIconSprite::create(int index, const OnResponceCallback& callback, bool hasEmpty)
{
    CharacterIconSprite* btn = new (std::nothrow) CharacterIconSprite();
    PlayerCharactersModel* playerCharactersModel = PLAYERCONTROLLER->_playerCharactersSorted.at(index);

    std::string frameName = StringUtils::format(IMAGE_CHARACTER_ICON, playerCharactersModel->getCharactersId());
    Sprite* selected = Sprite::createWithSpriteFrameName(frameName);
    selected->setColor(Color3B::GRAY);
    Sprite* disabled = Sprite::createWithSpriteFrameName(frameName);
    disabled->setColor(Color3B::GRAY);

    if (btn && btn->initWithNormalSprite(Sprite::createWithSpriteFrameName(frameName), selected, disabled, callback)) {
        btn->setTag(TAG_START + index);
        if (hasEmpty) {
            index += 1;
        }
        const int MAX_COLUMN = 5;
        int column = index % MAX_COLUMN;
        int row = index / MAX_COLUMN;
        const Size SIZE_TOTAL = Size(118.0f, 118.0f);
        const float MARGIN = 0.5f;
        btn->setPosition(Vec2(SIZE_TOTAL.width * (MARGIN + column), -SIZE_TOTAL.height * (MARGIN + row)));
        btn->initialize(playerCharactersModel);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void CharacterIconSprite::initialize(PlayerCharactersModel* playerCharactersModel)
{
    _playerCharactersModel = playerCharactersModel;
    setUserData(playerCharactersModel);
    showLabelsOnButtom();
    if (_playerCharactersModel->getLock() == 1) {
        showFavoriteIcon();
    }
}


void CharacterIconSprite::showLabelsOnButtom()
{
    std::string bottomStr = "";
    std::shared_ptr<const CharacterModel>characterModel = CharacterModel::find(_playerCharactersModel->getCharactersId());
    switch (PLAYERCONTROLLER->currentSelectedSortCharacterKey) {
    case SortPlayerCharacter::POWER: {
        bottomStr = StringUtils::format("%d", _playerCharactersModel->getExercise());
        auto levelLabel = Label::createWithTTF(bottomStr, FONT_NAME_2, 16);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(Vec2(getContentSize().width / 2, -2));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
        levelLabel->enableShadow();
        addChild(levelLabel, 4);
        break;
    }
    case SortPlayerCharacter::TECHNIQUE: {
        bottomStr = StringUtils::format("%d", _playerCharactersModel->getReaction());
        auto levelLabel = Label::createWithTTF(bottomStr, FONT_NAME_2, 16);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(Vec2(getContentSize().width / 2, -2));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
        levelLabel->enableShadow();
        addChild(levelLabel, 4);
        break;
    }
    case SortPlayerCharacter::BRAKE: {
        bottomStr = StringUtils::format("%d", _playerCharactersModel->getDecision());
        auto levelLabel = Label::createWithTTF(bottomStr, FONT_NAME_2, 16);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(Vec2(getContentSize().width / 2, -2));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
        levelLabel->enableShadow();
        addChild(levelLabel, 4);
        break;
    }
    case SortPlayerCharacter::COST: {
        bottomStr = StringUtils::format("%d", characterModel->getCost());
        auto levelLabel = Label::createWithTTF(bottomStr, FONT_NAME_2, 16);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(Vec2(getContentSize().width / 2, -2));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
        levelLabel->enableShadow();
        addChild(levelLabel, 4);
        break;
    }

    case SortPlayerCharacter::RARITY: {
        int rarity = characterModel->getRarity();
        auto starNode = createRarityNode(rarity);
        starNode->setPosition(Vec2(SIZE.width / 2, 0));
        addChild(starNode, 5);
        break;
    }

    default: {
        auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %d", _playerCharactersModel->getLevel()), FONT_NAME_2, 16);
        addChild(levelLabel, 4);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(Vec2(SIZE.width / 2, -2));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
        levelLabel->enableShadow();
        break;
    }
    }
}

void CharacterIconSprite::showFavoriteIcon()
{
    Sprite* lockIcon = Sprite::create("lock_icon.png");
    lockIcon->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    lockIcon->setPosition(Point(10, getContentSize().height - 10));
    lockIcon->setName("lock_icon");
    addChild(lockIcon);
}


Node* CharacterIconSprite::createRarityNode(int rarity)
{
    auto node = Node::create();

    for (int i = 0; i < rarity; i++) {
        auto start = Sprite::create("rarity.png");
        node->addChild(start);
        if (rarity % 2 == 1) {
            start->setPosition(Vec2((start->getContentSize().width - 5) * (i - rarity / 2), 0));
        } else {
            int j = i - rarity / 2;
            start->setPosition(Vec2((start->getContentSize().width - 5) * (0.5f + j), 0));
        }
    }
    node->setContentSize(Size(24 * rarity, 23));

    return node;
}


void CharacterIconSprite::showStatusLabel(int index)
{
    const USED_STATUS STATUS = checkSyanagoIsNew();
    if (STATUS == USED_STATUS::NEW || STATUS == USED_STATUS::USED) {
        Sprite* label;
        int tag = 2 * TAG_START + 2 * index;
        if (STATUS == USED_STATUS::NEW) {
            label = Sprite::create("garage_list_new_label.png");
        } else if (STATUS == USED_STATUS::USED) {
            label = Sprite::create("garage_list_using_label.png");
            tag += 1;
        }
        label->setPosition(Vec2(98 - 23, 99 - 15));
        label->setTag(tag);
        addChild(label, 4);
    }
}

CharacterIconSprite::USED_STATUS CharacterIconSprite::checkSyanagoIsNew()
{
    if (checkCharacterInUse(PLAYERCONTROLLER->getPlayerCharacterIdsUsed(), _playerCharactersModel->getID())) {
        return USED_STATUS::USED;
    } else if (_playerCharactersModel->getIsNew() == 1) {
        return USED_STATUS::NEW;
    } else {
        return USED_STATUS::NORMAL;
    }
}

bool CharacterIconSprite::checkCharacterInUse(std::vector<int>IDs, int ID)
{
    std::vector<int>::iterator it;
    it = find (IDs.begin(), IDs.end(), ID);
    return it != IDs.end();
}






