#include "SyanagoBaseLayer.h"
#include "StringDefines.h"
#include "TeamEditMainLayer.h"
#include "GarageMainLayer.h"
#include "MainScene.h"
#include "HeaderStatus.h"
#include "PartModel.h"

SyanagoBaseLayer::SyanagoBaseLayer():
    _title("SyanagoBaseLayer")
    , _scrollView(nullptr)
    , _scrollBar(nullptr)
{}

SyanagoBaseLayer::~SyanagoBaseLayer() {}


bool SyanagoBaseLayer::init()
{
    if (!LayerPriority::initWithPriority(kPrioritySyanagoBaseLayer)) {
        return false;
    }
    showHeader();
    return true;
}

void SyanagoBaseLayer::showHeader()
{
    Size winSize = Director::getInstance()->getWinSize();

    auto background = makeSprite("home_bg.png");
    background->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(background, -10);

    _titleBg = makeSprite("head_line_no2.png");
    _titleBg->setPosition(Vec2(_titleBg->getContentSize().width / 2, winSize.height - HEADER_STATUS::SIZE::HEIGHT - 12 - _titleBg->getContentSize().height / 2));
    addChild(_titleBg);

    auto btBack = makeMenuItem("back_button.png", CC_CALLBACK_1(SyanagoBaseLayer::btMenuItemCallback, this));
    btBack->setTag(bt_back_bt);

    auto menuBack = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    menuBack->addChild(btBack);
    menuBack->setPosition(Vec2(_titleBg->getPosition().x - _titleBg->getContentSize().width / 2 + 14 + btBack->getNormalImage()->getContentSize().width / 2, _titleBg->getPosition().y));
    addChild(menuBack);

    _titleLabel = Label::createWithTTF(_title, FONT_NAME_2, 34, Size(Vec2::ZERO));
    _titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    _titleLabel->setAnchorPoint(Vec2(0, 0.5f));
    _titleLabel->setPosition(menuBack->getPosition() + Vec2(btBack->getNormalImage()->getContentSize().width / 2 + 10,  -17));
    _titleLabel->enableShadow();
    addChild(_titleLabel);
}

void SyanagoBaseLayer::showSortMenu()
{
    Size winSize = Director::getInstance()->getWinSize();

    auto button = makeMenuItem("garage_list_sort_bt.png", CC_CALLBACK_1(SyanagoBaseLayer::btMenuItemCallback, this));
    button->setTag(bt_sort_bt);

    _sortLabel = Label::createWithTTF("取得", FONT_NAME_2, 24, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    _sortLabel->enableShadow();
    _sortLabel->setPosition(Vec2(button->getNormalImage()->getContentSize().width / 2, button->getNormalImage()->getContentSize().height / 2 - 12));
    button->addChild(_sortLabel);

    // Sort button
    auto menuSort = MenuPriority::createWithPriority(kPrioritySyanagoBaseMenu);
    menuSort->setPosition(Vec2(winSize.width - 14 - button->getNormalImage()->getContentSize().width / 2,
                               _titleBg->getPositionY()));
    menuSort->addChild(button);
    addChild(menuSort, 10);
}


bool SyanagoBaseLayer::onTouchBegan(Touch* touch, Event* unused_event)
{
    if (!isVisible() || !_mainLayer->isVisible()) {
        return false;
    }
    if (dynamic_cast<TeamEditMainLayer*>(_mainLayer)) {
        dynamic_cast<TeamEditMainLayer*>(_mainLayer)->_mainScene->homeLayer->showFairy();
    }

    if (dynamic_cast<GarageMainLayer*>(_mainLayer)) {
        dynamic_cast<GarageMainLayer*>(_mainLayer)->_mainScene->homeLayer->showFairy();
    }

    _isMove = false;
    if (_mainLayer->onTouchBegan(touch, unused_event)) {
        return true;
    }

    return true;
}

void SyanagoBaseLayer::onTouchMoved(Touch* touch, Event* unused_event)
{
    _mainLayer->onTouchMoved(touch, unused_event);
    _isMove = true;
}

void SyanagoBaseLayer::onTouchEnded(Touch* touch, Event* unused_event)
{
    _mainLayer->onTouchEnded(touch, unused_event);
    _isMove = false;
}

void SyanagoBaseLayer::onTouchCancelled(Touch* touch, Event* unused_event)
{
    _mainLayer->onTouchCancelled(touch, unused_event);
    _isMove = false;
}

void SyanagoBaseLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    if (checkMenuReturn()) {
        return;
    }
}

void SyanagoBaseLayer::showDatas()
{
    if (_scrollView) {
        _scrollView->removeFromParent();
    }

    const int row = _numberOfCharacter / kColumn + ((_numberOfCharacter % kColumn) > 0 ? 1 : 0);
    _container = LayerColor::create(Color4B(0, 0, 0, 0), kSizeCharacterIcon.width * kColumn, kSizeCharacterIcon.height * (row + 2));
    auto scrollContainer = LayerColor::create(Color4B(0, 0, 0, 0), _container->getContentSize().width, _container->getContentSize().height);
    scrollContainer->addChild(_container);

    const Size winSize = Director::getInstance()->getWinSize();
    _scrollView = ScrollViewPriority::create(Size(kSizeCharacterIcon.width * kColumn, winSize.height  - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 60), kPrioritySyanagoBaseScrollView, scrollContainer);
    _scrollView->setDirection(ScrollView::Direction::VERTICAL);
    _scrollView->setBounceable(false);
    _scrollView->setDelegate(this);
    _scrollView->setPosition(Vec2(4, _titleBg->getPositionY() - _titleBg->getContentSize().height / 2 - 7 - _scrollView->getViewSize().height));
    _scrollView->setDelegate(this);
    addChild(_scrollView);

    scrollContainer->setPosition(_container->getPosition() + Vec2(0, -_container->getContentSize().height + _scrollView->getViewSize().height));

    if (_scrollBar) {
        _scrollBar->removeFromParent();
    }
    _scrollBar = ScrollBar::initScrollBar(winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 60, _scrollView->getViewSize().height, _scrollView->getContainer()->getContentSize().height, _scrollView->getContentOffset().y);
    _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 75 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    addChild(_scrollBar);
}


Node* SyanagoBaseLayer::createRarityNode(int rarity)
{
    auto node = Node::create();

    for (int i = 0; i < rarity; i++) {
        auto start = makeSprite("rarity.png");
        node->addChild(start);
        if (rarity % 2 == 1) {
            start->setPosition(Vec2((start->getContentSize().width - 5) * (i - rarity / 2), 0));
        } else {
            int j = i - rarity / 2;
            start->setPosition(Vec2((start->getContentSize().width - 5) * (0.5f + j), 0));
        }
    }
    node->setContentSize(Size(24 * rarity, 23));

    return node;
}



bool SyanagoBaseLayer::checkMenuReturn()
{
    if (!_mainLayer) {
        return false;
    }
    if (dynamic_cast<TeamEditMainLayer*>(_mainLayer)) {
        dynamic_cast<TeamEditMainLayer*>(_mainLayer)->_mainScene->homeLayer->showFairy();
        return dynamic_cast<TeamEditMainLayer*>(_mainLayer)->isMoving() || dynamic_cast<TeamEditMainLayer*>(_mainLayer)->_mainScene->_isShowing;
    }

    if (dynamic_cast<GarageMainLayer*>(_mainLayer)) {
        dynamic_cast<GarageMainLayer*>(_mainLayer)->_mainScene->homeLayer->showFairy();
        return dynamic_cast<GarageMainLayer*>(_mainLayer)->isMoving() || dynamic_cast<GarageMainLayer*>(_mainLayer)->_mainScene->_isShowing;
    }
    return false;
}