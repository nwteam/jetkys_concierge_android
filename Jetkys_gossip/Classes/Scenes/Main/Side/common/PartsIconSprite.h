#ifndef __syanago__PartsIconSprite__
#define __syanago__PartsIconSprite__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "PlayerPartModel.h"

USING_NS_CC;


class PartsIconSprite : public MenuItemSprite
{
public:
    enum USED_STATUS {
        NORMAL = 0,
        USED,
        NEW
    };
    const static int TAG_START = 1000;
    const Size SIZE = Size(98,99);
    
    PartsIconSprite();
    
    typedef std::function<void(Ref* ref)> OnResponceCallback;
    static PartsIconSprite* create(int index, const OnResponceCallback& callback, bool hasEmpty = false);
    void initialize(PlayerPartModel* playerPartModel);
    
    void showStatusLabel(int index);
    void showUsedLabel(int index);
    USED_STATUS checkPartsIsNew();
    
private:
    
    void showLabelsOnButtom();
    
    bool checkCharacterInUse(std::vector<int>IDs, int ID);
    Node* createRarityNode(int rarity);
    
    PlayerPartModel* _playerPartModel;
    
};

#endif /* defined(__syanago__PartsIconSprite__) */
