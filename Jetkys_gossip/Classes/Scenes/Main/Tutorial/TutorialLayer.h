#ifndef __syanago__TutorialLayer__
#define __syanago__TutorialLayer__

#include "cocos2d.h"
USING_NS_CC;

class MainScene;

class TutorialLayer: public Layer
{
public:
    TutorialLayer();
    bool init();
    CREATE_FUNC(TutorialLayer);
    ~TutorialLayer();
    
    void gachaTutorial();
    
    void showGachaCallBack();
    
    //param
    MainScene* _mainScene;
    
private:
    enum TAG_SPRITE {
        CONFIRM_LABEL = 0,
        CONFIRM_MENU,
        CONFIRM_BACKGROUND,
        FOOTER_HOME,
        FOOTER_SHOP,
        FOOTER_GACHA,
        FOOTER_FRIEND,
        FOOTER_OTHER,
        FOOTER_START,
        FOOTER_START_MENU,
        OTHER_BACKGROUND,
        OTHER_DUMMY_FOOTER,
        OTHER_LIBRARY_SYANAGO,
        OTHER_LIBRARY_TREASURE,
        MAP_START_BUTTON
        
    };
    
    void showBackGround();
    void showConfirm();
    void createTutorialBase();
    
    void onTapConfirmYes();
    void onTapConfirmNo();
    
    bool onTouchBegan(Touch *touch, Event *unused_event);
    void TouchEndedWhenHomeTutorial(Touch *touch, Event *unused_event);
    void TouchEndedWhenShopTutorial(Touch *touch, Event *unused_event);
    void TouchEndedWhenGachaTutorial(Touch *touch, Event *unused_event);
    void TouchEndedWhenFriendTutorial(Touch *touch, Event *unused_event);
    void TouchEndedWhenOthersTutorial(Touch *touch, Event *unused_event);
    void TouchEndedWhenStartTutorial(Touch *touch, Event *unused_event);
    typedef std::function<void(Touch *touch, Event *unused_event)> OnTouchCallback;
    void replaceTouchEvent(const OnTouchCallback& callback);
    
    void homeButtonTutorial(int progress);
    void shopButtonTutorial(int progress);
    void gachaButtonTutorial(int progress);
    void friendButtonTutorial(int progress);
    void otherButtonTutorial(int progress);
    void startButtonTutorial(int progress);
    void onTapStartButton();
    void createStartButtonOnTutorialMap();
    void mapTutorial();
    void onTapStartButtonOnTutorialMap();
    void startRaceTutorial();
    
    void moveArrow(Point point);
    
    int tutorialProgress;
    
    Size _winSize;
    Sprite* tutorialBackGround;
    Sprite* moveSprite;
    Sprite* shopSprite;
    Sprite* frCell;
    Label* titleShow1;
    Label* titleShow2;
    Label* titleShow3;
    Sprite* dialogNextPoint;
    
    //other
    Sprite* button1S;
    Sprite* button2S;
    Sprite* gachaImage;
    

};

#endif /* defined(__syanago__TutorialLayer__) */
