#include "TutorialLayer.h"
#include "jCommon.h"
#include "FontDefines.h"
#include "MainScene.h"
#include "RaceScene.h"
#include "UserDefaultManager.h"
#include "HelpPlayer.h"
#include "TeamStatus.h"
#include "HeaderStatus.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    #include "UtilsIOS.h"
#endif

TutorialLayer::TutorialLayer():
    tutorialProgress(0) {}

TutorialLayer::~TutorialLayer() {}

bool TutorialLayer::init()
{
    if (!Layer::init()) {
        return false;
    }
    setVisible(true);

    UserDefault::getInstance()->setBoolForKey("Tutorial", true);

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tuto_tap1.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tuto_tap2.plist");

    _winSize = Director::getInstance()->getWinSize();
    showBackGround();
    showConfirm();

    return true;
}

void TutorialLayer::showBackGround()
{
    Sprite* backGround = makeSprite("black_image.png");
    backGround->setScale(4);
    backGround->setOpacity(160);
    backGround->setPosition(Point(_winSize.width / 2, _winSize.height / 2));
    addChild(backGround, 2);
}

void TutorialLayer::showConfirm()
{
    Sprite* confirmBackGround = makeSprite("TutorialBgr.png");
    confirmBackGround->setTag(TAG_SPRITE::CONFIRM_BACKGROUND);
    confirmBackGround->setVisible(true);
    confirmBackGround->setPosition(Point(_winSize.width / 2, _winSize.height * 0.365));

    Label* label = Label::createWithTTF("チュートリアルを行いますか？", FONT_NAME_2, 35);
    label->setTag(TAG_SPRITE::CONFIRM_LABEL);
    label->enableShadow();
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->setPosition(Point(confirmBackGround->getContentSize().width / 2, confirmBackGround->getContentSize().height - label->getContentSize().height - 5 - 50));
    label->setColor(COLOR_YELLOW);
    confirmBackGround->addChild(label);

    MenuItemSprite* yesButton = makeMenuItem("TutorialBtn.png", CC_CALLBACK_0(TutorialLayer::onTapConfirmYes, this));
    yesButton->setPosition(Point(yesButton->getContentSize().width * 1.3, 30 + yesButton->getContentSize().height));

    auto yesLabel = Label::createWithTTF("はい", FONT_NAME_2, 30);
    yesLabel->enableShadow();
    yesLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    yesLabel->setPosition(Point(yesButton->getContentSize().width / 2, yesButton->getContentSize().height / 2 - 15));
    yesButton->addChild(yesLabel);

    auto noButton = makeMenuItem("TutorialBtn.png", CC_CALLBACK_0(TutorialLayer::onTapConfirmNo, this));
    noButton->setPosition(Point(-noButton->getContentSize().width * 1.3 + confirmBackGround->getContentSize().width, 30 + yesButton->getContentSize().height));

    auto noLabel = Label::createWithTTF("いいえ", FONT_NAME_2, 30);
    noLabel->enableShadow();
    noLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    noLabel->setPosition(Point(noButton->getContentSize().width / 2, noButton->getContentSize().height / 2 - 15));
    noButton->addChild(noLabel);

    auto menu = MenuTouch::create(yesButton, noButton, NULL);
    menu->setTag(TAG_SPRITE::CONFIRM_MENU);
    menu->setPosition(Point(0.0, 130));
    confirmBackGround->addChild(menu, 2);
    addChild(confirmBackGround, 2);
}

void TutorialLayer::createTutorialBase()
{
    tutorialBackGround = makeSprite("TutorialBgr1.png");
    tutorialBackGround->setVisible(false);
    tutorialBackGround->setPosition(Point(tutorialBackGround->getContentSize().width / 2, 1136 * 0.255 + 101));

    dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
    dialogNextPoint->setPosition(Point(tutorialBackGround->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                       dialogNextPoint->getContentSize().height / 2 + 10));
    tutorialBackGround->addChild(dialogNextPoint);

    // チュートリアルメッセージ１行目
    titleShow1 = Label::createWithTTF("", FONT_NAME_2, 25);
    titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
    titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
    titleShow1->setPosition(Point(10.0, tutorialBackGround->getContentSize().height - titleShow1->getContentSize().height * 1.5));
    tutorialBackGround->addChild(titleShow1);

    // チュートリアルメッセージ２行目
    titleShow2 = Label::createWithTTF("", FONT_NAME_2, 25);
    titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
    titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
    tutorialBackGround->addChild(titleShow2);

    // チュートリアルメッセージ３行目
    titleShow3 = Label::createWithTTF("", FONT_NAME_2, 25);
    titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
    titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    tutorialBackGround->addChild(titleShow3);

    //下向き矢印
    moveSprite = makeSprite("TutorialDown.png");
    moveSprite->setPosition(Point(tutorialBackGround->getContentSize().width / 2, -tutorialBackGround->getContentSize().height * 0.5 + 10.0));
    tutorialBackGround->addChild(moveSprite, 5);
    tutorialBackGround->setPosition(Point(_winSize.width / 2, 1136 * 0.3));

    addChild(tutorialBackGround, 4);
}

void TutorialLayer::onTapConfirmYes()
{
    auto confirmBackGround = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::CONFIRM_BACKGROUND));
    // remove yes no buttons
    static_cast<MenuTouch*>(confirmBackGround->getChildByTag(TAG_SPRITE::CONFIRM_MENU))->removeFromParent();

    Label* label = static_cast<Label*>(confirmBackGround->getChildByTag(TAG_SPRITE::CONFIRM_LABEL));
    label->setString("チュートリアルを開始いたします。");
    label->setScale(0.9);
    label->setPosition(label->getPosition() + Vec2(0, 0));
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);

    createTutorialBase();

    // prepare mainScene to startTutorial
    Director::getInstance()->getEventDispatcher()
    ->removeEventListenersForTarget(_mainScene->homeLayer);

    // wait user tap actions to next tutorial
    replaceTouchEvent(CC_CALLBACK_2(TutorialLayer::TouchEndedWhenHomeTutorial, this));
}

void TutorialLayer::onTapConfirmNo()
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap1.plist");
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap2.plist");
    UserDefault::getInstance()->setBoolForKey("NewPlayer", true);
    UserDefault::getInstance()->setBoolForKey("Tutorial", false);
    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::OPEN_GACHA, true);
    _mainScene->setMainTutorial(false);
    _mainScene->resetStartTab();
    setVisible(false);
}

bool TutorialLayer::onTouchBegan(Touch* touch, Event* unused_event)
{
    if (_mainScene->isMainTutorial()) {
        return true;
    }
    return false;
}

void TutorialLayer::TouchEndedWhenHomeTutorial(Touch* touch, Event* unused_event)
{
    homeButtonTutorial(tutorialProgress);
    tutorialProgress++;
    if (tutorialProgress > 2) {
        tutorialProgress = 0;
        replaceTouchEvent(CC_CALLBACK_2(TutorialLayer::TouchEndedWhenShopTutorial, this));
    }
}

void TutorialLayer::TouchEndedWhenShopTutorial(Touch* touch, Event* unused_event)
{
    shopButtonTutorial(tutorialProgress);
    tutorialProgress++;
    if (tutorialProgress > 2) {
        tutorialProgress = 0;
        replaceTouchEvent(CC_CALLBACK_2(TutorialLayer::TouchEndedWhenGachaTutorial, this));
    }
}

void TutorialLayer::TouchEndedWhenGachaTutorial(cocos2d::Touch* touch, cocos2d::Event* unused_event)
{
    gachaButtonTutorial(tutorialProgress);
    tutorialProgress++;
    if (tutorialProgress > 2) {
        tutorialProgress = 0;
        replaceTouchEvent(CC_CALLBACK_2(TutorialLayer::TouchEndedWhenFriendTutorial, this));
    }
}

void TutorialLayer::TouchEndedWhenFriendTutorial(cocos2d::Touch* touch, cocos2d::Event* unused_event)
{
    friendButtonTutorial(tutorialProgress);
    tutorialProgress++;
    if (tutorialProgress > 3) {
        tutorialProgress = 0;
        replaceTouchEvent(CC_CALLBACK_2(TutorialLayer::TouchEndedWhenOthersTutorial, this));
    }
}

void TutorialLayer::TouchEndedWhenOthersTutorial(cocos2d::Touch* touch, cocos2d::Event* unused_event)
{
    otherButtonTutorial(tutorialProgress);
    tutorialProgress++;
    if (tutorialProgress > 4) {
        tutorialProgress = 0;
        replaceTouchEvent(CC_CALLBACK_2(TutorialLayer::TouchEndedWhenStartTutorial, this));
    }
}


void TutorialLayer::TouchEndedWhenStartTutorial(Touch* touch, Event* unused_event)
{
    startButtonTutorial(tutorialProgress);
    tutorialProgress++;
}

void TutorialLayer::replaceTouchEvent(const OnTouchCallback& callback)
{
    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(TutorialLayer::onTouchBegan, this);
    listener->onTouchEnded = callback;

    Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}


void TutorialLayer::moveArrow(Point point)
{
    moveSprite->stopAllActions();
    moveSprite->setPosition(point);
    moveSprite->runAction(RepeatForever::create(
                              Sequence::create(
                                  MoveTo::create(0.5, Point(moveSprite->getPositionX(), -tutorialBackGround->getContentSize().height * 0.5)),
                                  MoveTo::create(0.5, Point(moveSprite->getPositionX(), -tutorialBackGround->getContentSize().height * 0.5 + 10.0)), NULL)
                              ));
}

void TutorialLayer::homeButtonTutorial(int progress)
{
    if (progress == 0) {
        // set up tutorial
        static_cast<Sprite*>(getChildByTag(TAG_SPRITE::CONFIRM_BACKGROUND))->setVisible(false);
        tutorialBackGround->setVisible(true);
        tutorialBackGround->setPosition(Point(tutorialBackGround->getContentSize().width / 2, 1136 * 0.255 + 101));

        //下向き矢印移動
        const Point homeButtonPosition = Point(
            moveSprite->getContentSize().width / 2 + 135.0,
            -tutorialBackGround->getContentSize().height * 0.5 + 10.0
            );
        moveArrow(homeButtonPosition);

        // show home button
        Sprite* homeButton = makeSprite("menu_home.png");
        homeButton->setTag(TAG_SPRITE::FOOTER_HOME);
        homeButton->setPosition(homeButton->getContentSize().width / 2 + 97.0,
                                -tutorialBackGround->getContentSize().width * 0.4);
        homeButton->runAction(RepeatForever::create(Sequence::create(
                                                        ScaleTo::create(0.5, 1.05),
                                                        ScaleTo::create(0.5, 1.0), NULL
                                                        )));
        tutorialBackGround->addChild(homeButton, 5);
        titleShow1->setString("私はホームボタンです。");
        titleShow1->setPosition(Point(10.0, tutorialBackGround->getContentSize().height - 30));
    } else if (progress == 1) {
        titleShow1->setString("ボタンに見えないけどボタンです。");
    } else if (progress == 2) {
        titleShow1->setString("いろんな画面からホーム画面に");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow2->setString("戻る事が出来ます。");
    }
}

void TutorialLayer::shopButtonTutorial(int progress)
{
    if (progress == 0) {
        static_cast<Sprite*>(tutorialBackGround->getChildByTag(TAG_SPRITE::FOOTER_HOME))->removeFromParent();

        tutorialBackGround->setPosition(Point(tutorialBackGround->getContentSize().width / 2, 1136 * 0.255));

        const Point shopButtonPosition = Point(moveSprite->getContentSize().width / 2,
                                               -tutorialBackGround->getContentSize().height * 0.5 + 10.0);
        moveArrow(shopButtonPosition);

        Sprite* shopButton = makeSprite("menu_shop.png");
        shopButton->setTag(TAG_SPRITE::FOOTER_SHOP);
        shopButton->setPosition(shopButton->getContentSize().width / 2, -tutorialBackGround->getContentSize().width * 0.4);
        shopButton->runAction(RepeatForever::create(Sequence::create(
                                                        ScaleTo::create(0.5, 1.05),
                                                        ScaleTo::create(0.5, 1.0),
                                                        NULL
                                                        )));
        tutorialBackGround->addChild(shopButton, 5);

        titleShow1->setString("ここはショップです。");
        titleShow2->setString("");
    } else if (progress == 1) {
        titleShow1->setString("ここではトークンを消費して");
        titleShow2->setString("ガレージや装備、燃料を購入");
        titleShow3->setString("出来ます。");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    } else if (progress == 2) {
        titleShow1->setString("トークンは無料でも獲得する");
        titleShow2->setString("ことが可能です。");
        titleShow3->setString("");
    }
}

void TutorialLayer::gachaButtonTutorial(int progress)
{
    if (progress == 0) {
        tutorialBackGround->getChildByTag(TAG_SPRITE::FOOTER_SHOP)->removeFromParent();
        tutorialBackGround->setPosition(Point(tutorialBackGround->getContentSize().width / 2 + 50.0, 1136 * 0.255));

        Sprite* gachaButton = makeSprite("menu_gacha.png");
        gachaButton->setTag(TAG_SPRITE::FOOTER_GACHA);
        gachaButton->setPosition(gachaButton->getContentSize().width / 2 + 70.0, -tutorialBackGround->getContentSize().width * 0.4);
        gachaButton->runAction(RepeatForever::create(Sequence::create(
                                                         ScaleTo::create(0.5, 1.05),
                                                         ScaleTo::create(0.5, 1.0),
                                                         NULL)));

        const Point gachaButtonPosition = Point(gachaButton->getContentSize().width / 2 + 70.0, -tutorialBackGround->getContentSize().height * 0.5 + 10.0);
        moveArrow(gachaButtonPosition);

        tutorialBackGround->addChild(gachaButton, 5);

        titleShow1->setString("ここはガチャ機能です。");
        titleShow2->setString("");
        titleShow3->setString("");
    } else if (progress == 1) {
        titleShow1->setString("ガチャには無料のフレンドガチャと");
        titleShow2->setString("トークンを消費する");
        titleShow3->setString("プレミアムガチャがあります。");
    } else if (progress == 2) {
        titleShow1->setString("なんとプレミアムガチャは");
        titleShow2->setString("★３以上の車なごが必ず出る");
        titleShow3->setString("のでお得です！");
    }
}

void TutorialLayer::friendButtonTutorial(int progress)
{
    if (progress == 0) {
        tutorialBackGround->setPosition(Point(_winSize.width - tutorialBackGround->getContentSize().width / 2 - 50.0, 1136 * 0.255));

        Sprite* friendButton = makeSprite("menu_friend.png");
        friendButton->setTag(TAG_SPRITE::FOOTER_FRIEND);
        friendButton->setPosition(tutorialBackGround->getContentSize().width - tutorialBackGround->getChildByTag(TAG_SPRITE::FOOTER_GACHA)->getContentSize().width / 2 - 70.0, -tutorialBackGround->getContentSize().width * 0.4);
        friendButton->runAction(RepeatForever::create(Sequence::create(
                                                          ScaleTo::create(0.5, 1.05),
                                                          ScaleTo::create(0.5, 1.0), NULL)));
        const Point friendArrowPosition = Point(tutorialBackGround->getContentSize().width - friendButton->getContentSize().width / 2 - 70.0, -tutorialBackGround->getContentSize().height * 0.5 + 10.0);
        moveArrow(friendArrowPosition);

        tutorialBackGround->getChildByTag(TAG_SPRITE::FOOTER_GACHA)->removeFromParent();
        tutorialBackGround->addChild(friendButton, 5);

        titleShow1->setString("ここはフレンド機能です。");
        titleShow2->setString("");
        titleShow3->setString("");
    } else if (progress == 1) {
        titleShow1->setString("フレンドはレースで");
        titleShow2->setString("優先して助っ人に選択する事が");
        titleShow3->setString("可能です。");
    } else if (progress == 2) {
        titleShow1->setString("フレンド登録しているユーザが");
        titleShow2->setString("助っ人になるとフレンドポイントを");
        titleShow3->setString("多く貰う事が可能です。");
    } else if (progress == 3) {
        titleShow1->setString("フレンドポイントは");
        titleShow2->setString("フレンドガチャで使用可能です。");
        titleShow3->setString("");
    }
}

void TutorialLayer::otherButtonTutorial(int progress)
{
    if (progress == 0) {
        tutorialBackGround->setPosition(Point(_winSize.width - tutorialBackGround->getContentSize().width / 2, 1136 * 0.255));
        tutorialBackGround->getChildByTag(TAG_SPRITE::FOOTER_FRIEND)->removeFromParent();

        Sprite* otherButton = makeSprite("menu_other.png");
        otherButton->setTag(TAG_SPRITE::FOOTER_OTHER);
        otherButton->setPosition(tutorialBackGround->getContentSize().width - otherButton->getContentSize().width / 2, -tutorialBackGround->getContentSize().width * 0.4);
        otherButton->runAction(RepeatForever::create(Sequence::create(
                                                         ScaleTo::create(0.5, 1.05),
                                                         ScaleTo::create(0.5, 1.0),
                                                         NULL)));
        tutorialBackGround->addChild(otherButton, 5);

        const Point otherButtonPosition = Point(tutorialBackGround->getContentSize().width - otherButton->getContentSize().width / 2, -tutorialBackGround->getContentSize().height * 0.5 + 10.0);
        moveArrow(otherButtonPosition);

        titleShow1->setString("ここはその他の機能です。");
        titleShow2->setString("");
        titleShow3->setString("");
    } else if (progress == 1) {
        tutorialBackGround->getChildByTag(TAG_SPRITE::FOOTER_OTHER)->removeFromParent();
        tutorialBackGround->setPosition(_winSize.width / 2, _winSize.height / 2 + 50.0);
        if (_winSize.height == 960) { // ipad,iphone4
            tutorialBackGround->setPosition(_winSize.width / 2, 960 / 2 - 40.0);
        }

        Sprite* otherBackGround = makeSprite("home_bg.png");
        otherBackGround->setTag(TAG_SPRITE::OTHER_BACKGROUND);
        otherBackGround->setPosition(Point(_winSize.width / 2,
                                           _winSize.height - otherBackGround->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT));
        auto otherScreenButtons = makeSprite("bgrOther.png");
        otherScreenButtons->setPosition(Point(otherBackGround->getContentSize().width / 2, otherBackGround->getContentSize().height - otherScreenButtons->getContentSize().height / 2));
        otherBackGround->addChild(otherScreenButtons);
        addChild(otherBackGround, 1);

        Sprite* dummyFooterButtons = makeSprite("FT.png");
        dummyFooterButtons->setTag(TAG_SPRITE::OTHER_DUMMY_FOOTER);
        dummyFooterButtons->setPosition(Point(_winSize.width / 2, dummyFooterButtons->getContentSize().height / 2));
        addChild(dummyFooterButtons, 1);

        Sprite* libraryButton = makeSprite("buttonOther1.png");
        libraryButton->setTag(TAG_SPRITE::OTHER_LIBRARY_SYANAGO);
        libraryButton->setPosition(tutorialBackGround->getContentSize().width / 2, tutorialBackGround->getContentSize().height / 2 + 230.0);
        libraryButton->runAction(RepeatForever::create(Sequence::create(
                                                           ScaleTo::create(0.5, 1.05),
                                                           ScaleTo::create(0.5, 1.0),
                                                           NULL)));
        tutorialBackGround->addChild(libraryButton, 1);

        const Point position = Point(tutorialBackGround->getContentSize().width / 2,
                                     tutorialBackGround->getContentSize().height + 70.0);

        moveSprite->stopAllActions();
        moveSprite->setRotation(180);
        moveSprite->setPosition(Point(tutorialBackGround->getContentSize().width / 2,
                                      tutorialBackGround->getContentSize().height + 70.0));
        moveSprite->runAction(RepeatForever::create(Sequence::create(
                                                        MoveTo::create(0.5, Point(moveSprite->getPositionX(), tutorialBackGround->getContentSize().height + 60)),
                                                        MoveTo::create(0.5, Point(moveSprite->getPositionX(), tutorialBackGround->getContentSize().height + 70.0)),
                                                        NULL)));

        titleShow1->setString("車なご図鑑には出会った車なご");
        titleShow2->setString("がコレクションされます。");
        titleShow3->setString("");
    } else if (progress == 2) {
        titleShow1->setString("ここでしか見る事の出来ない");
        titleShow2->setString("車なごの情報も載っています。");
        titleShow3->setString("");
    } else if (progress == 3) {
        tutorialBackGround->setPosition(_winSize.width / 2, _winSize.height / 2 - 58.0);
        if (_winSize.height == 960) { // ipad,iphone4
            tutorialBackGround->setPosition(_winSize.width / 2, 960 / 2 - HEADER_STATUS::SIZE::HEIGHT);
        }
        tutorialBackGround->getChildByTag(TAG_SPRITE::OTHER_LIBRARY_SYANAGO)->removeFromParent();

        Sprite* treasureButton = makeSprite("buttonOther2.png");
        treasureButton->setTag(TAG_SPRITE::OTHER_LIBRARY_TREASURE);
        treasureButton->setPosition(tutorialBackGround->getContentSize().width / 2 + 1.0, tutorialBackGround->getContentSize().height * 2 + 49.0);
        treasureButton->runAction(RepeatForever::create(Sequence::create(
                                                            ScaleTo::create(0.5, 1.05),
                                                            ScaleTo::create(0.5, 1.0),
                                                            NULL)));
        tutorialBackGround->addChild(treasureButton, 1);

        titleShow1->setString("宝物図鑑にはレースで拾った");
        titleShow2->setString("宝物がコレクションされます。");
        titleShow3->setString("");
    } else if (progress == 4) {
        titleShow1->setString("宝物は集めると不思議な効果");
        titleShow2->setString("を発揮する事もあります。。。");
        titleShow3->setString("");
    }
}

void TutorialLayer::startButtonTutorial(int progress)
{
    if (progress == 0) {
        getChildByTag(TAG_SPRITE::OTHER_BACKGROUND)->removeFromParent();
        getChildByTag(TAG_SPRITE::OTHER_DUMMY_FOOTER)->removeFromParent();
        tutorialBackGround->getChildByTag(TAG_SPRITE::OTHER_LIBRARY_TREASURE)->removeFromParent();
        tutorialBackGround->setPosition(Point(_winSize.width / 2, 1136 * 0.3));
        moveSprite->setRotation(0);
        moveArrow(Point(tutorialBackGround->getContentSize().width / 2, -30));

        MenuItem* startButton = makeMenuItem("menu_start.png", CC_CALLBACK_0(TutorialLayer::onTapStartButton, this));
        startButton->setPosition(Point(
                                     moveSprite->getPositionX(),
                                     moveSprite->getPositionY() - moveSprite->getPositionY() - 15.0
                                     - startButton->getContentSize().height - 1136 * 0.01));
        startButton->runAction(RepeatForever::create(Sequence::create(
                                                         ScaleTo::create(0.5, 1.05),
                                                         ScaleTo::create(0.5, 1.0),
                                                         NULL)));
        MenuTouch* menu = MenuTouch::create(startButton, NULL);
        menu->setTag(TAG_SPRITE::FOOTER_START_MENU);
        menu->setEnabled(false);
        menu->setPosition(Vec2::ZERO);
        tutorialBackGround->addChild(menu);

        titleShow1->setString("それでは車なごの世界");
        titleShow2->setString("に出てみましょう。");
        titleShow3->setString("");
    } else if (progress == 1) {
        MenuTouch* menu = static_cast<MenuTouch*>(tutorialBackGround->getChildByTag(TAG_SPRITE::FOOTER_START_MENU));
        menu->setEnabled(true);

        titleShow1->setString("ここをタップするとMAP");
        titleShow2->setString("に出る事が出来ます。");
        titleShow3->setString("ボタンをタップしてください。");
    }
}

void TutorialLayer::onTapStartButton()
{
    // remove previous tutorials
    tutorialBackGround->getChildByTag(TAG_SPRITE::FOOTER_START_MENU)->removeFromParent();
    Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
    moveSprite->stopAllActions();

    _mainScene->showStartTutorial();
    _mainScene->startLayer->setTouchEnabled(false);
    mapTutorial();
}

void TutorialLayer::mapTutorial()
{
    createStartButtonOnTutorialMap();

    tutorialBackGround->setPosition(Point(_winSize.width / 2 - 80.0, 1136 * 0.4 + 60.0));
    moveArrow(Point(tutorialBackGround->getContentSize().width / 2 - 85.0, -tutorialBackGround->getContentSize().height * 0.5 + 10.0));

    titleShow1->setString("ここが『車なご』の世界だよ！");
    titleShow2->setString("今回はセレナちゃんが");
    titleShow3->setString("走ってくれるよ！");
}

void TutorialLayer::createStartButtonOnTutorialMap()
{
    MenuItem* startButton = makeMenuItem("next_clear_marker.png", CC_CALLBACK_0(TutorialLayer::onTapStartButtonOnTutorialMap, this));
    startButton->setScale(2);
    startButton->setPosition(Point(HEADER_STATUS::SIZE::HEIGHT, 302));
    // ipad,iphone
    if (_winSize.height == 960) {
        startButton->setPosition(Point(140, 305));
    }
    startButton->runAction(RepeatForever::create(Sequence::create(
                                                     ScaleTo::create(0.5, 2.1),
                                                     ScaleTo::create(0.5, 2.0),
                                                     NULL)));

    Label* text = Label::createWithTTF("Start", FONT_NAME_2, 40);
    text->setColor(Color3B::BLACK);
    text->setScale(0.5);
    text->setPosition(Point(startButton->getContentSize().width / 2, startButton->getContentSize().height / 2 - 10));
    startButton->addChild(text);

    MenuTouch* menu = MenuTouch::create(startButton, NULL);
    menu->setPosition(Vec2::ZERO);
    menu->setTag(TAG_SPRITE::MAP_START_BUTTON);

    addChild(menu, 4);
}

void TutorialLayer::onTapStartButtonOnTutorialMap()
{
    getChildByTag(TAG_SPRITE::MAP_START_BUTTON)->removeFromParent();
    moveSprite->stopAllActions();
    tutorialBackGround->setVisible(false);
    startRaceTutorial();
}

void TutorialLayer::startRaceTutorial()
{
    std::string gridCell = "[{\"id\":\"37\",\"kind\":\"1\",\"score\":\"0\"},{\"id\":\"37\",\"kind\":\"1\",\"score\":\"0\"},{\"id\":\"2\",\"kind\":\"1\",\"score\":\"250\"},{\"id\":\"37\",\"kind\":\"1\",\"score\":\"0\"},{\"id\":\"31\",\"kind\":\"1\",\"score\":\"250\"},{\"id\":\"37\",\"kind\":\"1\",\"score\":\"0\"}]";
    Json* grid = Json_create(gridCell.c_str());

    std::vector<std::shared_ptr<const CharacterModel> >enemyCharacterModels;
    enemyCharacterModels.push_back(CharacterModel::find(1));
    enemyCharacterModels.push_back(CharacterModel::find(17));
    enemyCharacterModels.push_back(CharacterModel::find(20));

    std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
    playerCharacters.push_back(CharacterModel::find(14));

    Scene* scene = Scene::create();
    auto raceScene = RaceScene::create(CourseModel::find(1),
                                       grid,
                                       playerCharacters,
                                       new TeamStatus(7500, 7500, 7500),
                                       new HelpPlayer(0, 0, false, "", 1, 1),
                                       enemyCharacterModels);
    scene->addChild(raceScene);
    transitScene(scene);
}

void TutorialLayer::gachaTutorial()
{
    tutorialProgress = 102;
    static_cast<Sprite*>(getChildByTag(TAG_SPRITE::CONFIRM_BACKGROUND))->removeFromParent();
    auto layer = LayerPriority::create();
    addChild(layer);
    tutorialBackGround = makeSprite("TutorialBgr1.png");
    tutorialBackGround->setPosition(Point(_winSize.width / 2, _winSize.height * 0.62 + 30));
    addChild(tutorialBackGround, 4);
    dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
    dialogNextPoint->setPosition(Point(tutorialBackGround->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                       dialogNextPoint->getContentSize().height / 2 + 10));
    tutorialBackGround->addChild(dialogNextPoint);

    titleShow1 = Label::createWithTTF("プレミアムガチャを", FONT_NAME_2, 25);
    titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
    tutorialBackGround->addChild(titleShow1);
    titleShow1->setPosition(Point(10.0, tutorialBackGround->getContentSize().height  - 30));
    titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    titleShow2 = Label::createWithTTF("選択してください。", FONT_NAME_2, 25);
    titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
    tutorialBackGround->addChild(titleShow2);
    titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
    titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    titleShow3 = Label::createWithTTF("", FONT_NAME_2, 25);
    titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
    tutorialBackGround->addChild(titleShow3);
    titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);

    moveSprite = makeSprite("TutorialDown.png");
    moveSprite->setPosition(Point(tutorialBackGround->getContentSize().width / 2, -tutorialBackGround->getContentSize().height * 0.5 + 10.0));
    tutorialBackGround->addChild(moveSprite, 5);


    auto gachaImage1 = makeMenuItem("gacha_premium_button.png", CC_CALLBACK_0(TutorialLayer::showGachaCallBack, this));
    int delta = 19.0;
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        if (isWideScreen()) {
            delta = 3.0;
        }
    #endif
    gachaImage1->setPosition(Point(moveSprite->getPositionX(), moveSprite->getPositionY() - moveSprite->getContentSize().height - gachaImage1->getContentSize().height / 2 + delta));
    auto seqGacha = Sequence::create(ScaleTo::create(0.5, 1.02), ScaleTo::create(0.5, 1.0), NULL);
    auto sRep = RepeatForever::create(seqGacha);
    gachaImage1->runAction(sRep);
    auto menuGacha = MenuPriority::create();
    menuGacha->addChild(gachaImage1);
    menuGacha->setPosition(Point(0.0, 0.0));
    tutorialBackGround->addChild(menuGacha);
}

void TutorialLayer::showGachaCallBack()
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap5.plist");
    UserDefault::getInstance()->setBoolForKey("Tutorial", false);
    UserDefault::getInstance()->setBoolForKey("GachaFinish", true);
    UserDefault::getInstance()->setBoolForKey("NoHomeVoice", false);

    _mainScene->showGachaTutorial();
    _mainScene->setMainTutorial(false);

    Director::getInstance()->getEventDispatcher()->removeEventListenersForTarget(_mainScene->homeLayer);
    setVisible(false);
    removeFromParent();
}
