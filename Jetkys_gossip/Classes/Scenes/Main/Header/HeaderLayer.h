#ifndef __syanago__HeaderLayer__
#define __syanago__HeaderLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class MainScene;

class HeaderLayer : public Layer
{
public:
    HeaderLayer();
    ~HeaderLayer();
    bool init();
    CREATE_FUNC(HeaderLayer);
    
    MainScene *_mainScene;
    
    void changeMessage(const std::string message);
    
    void updateInfoLayer();
    int getFuleTime();
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        EXP_AND_FUEL,
        PLAYER_NAME_AND_PROPERTY,
        MESSAGE_BASE,
        MESSAGE,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_EXP_AND_FUEL,
        Z_PLAYER_NAME_AND_PROPERTY,
        Z_MESSAGE_BASE,
        Z_MESSAGE,
    };
    const std::string DEFAULT_MESSAGE = "DefaultMessage";
    
    void showBackground();
    void showExpAndFuel();
    void showPlayerNameAndProperty();
    void showMessageBase();
    void showMessage(const std::string message);
};

#endif
