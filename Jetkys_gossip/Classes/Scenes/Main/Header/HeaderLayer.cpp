#include "HeaderLayer.h"

#include "ExpAndFuel.h"
#include "PlayerNameAndProperty.h"
#include "TelopScrollView.h"

HeaderLayer::HeaderLayer()
{}

HeaderLayer::~HeaderLayer()
{}

bool HeaderLayer::init()
{
    if (!Layer::create()) {
        return false;
    }

    showBackground();
    showExpAndFuel();
    showPlayerNameAndProperty();
    showMessageBase();
    showMessage(DEFAULT_MESSAGE);

    return true;
}

void HeaderLayer::showBackground()
{
    auto background = Sprite::create("info_base.png");
    Size winSize = Director::getInstance()->getWinSize();
    background->setPosition(Point(winSize.width / 2, winSize.height - background->getContentSize().height / 2));
    background->setTag(TAG_SPRITE::BACKGROUND);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void HeaderLayer::showExpAndFuel()
{
    auto expAndFuelView = ExpAndFuel::createExpAndFuel();
    expAndFuelView->setTag(TAG_SPRITE::EXP_AND_FUEL);
    expAndFuelView->setPosition(Vec2(5.0f,
                                     Director::getInstance()->getWinSize().height - getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height + 5.0f));
    addChild(expAndFuelView, Z_ORDER::Z_EXP_AND_FUEL);
}

void HeaderLayer::showPlayerNameAndProperty()
{
    auto playerNameAndPropertyView = PlayerNameAndProperty::createPlayerNameAndProperty();
    playerNameAndPropertyView->setTag(TAG_SPRITE::PLAYER_NAME_AND_PROPERTY);
    Size winSize = Director::getInstance()->getWinSize();
    playerNameAndPropertyView->setPosition(Vec2(winSize.width - 5.0f,
                                                winSize.height - 5.0f));
    addChild(playerNameAndPropertyView, Z_ORDER::Z_PLAYER_NAME_AND_PROPERTY);
}

void HeaderLayer::showMessageBase()
{
    auto messageBase = Sprite::create("info_notice_base.png");
    messageBase->setTag(TAG_SPRITE::MESSAGE_BASE);
    Size winSize = Director::getInstance()->getWinSize();
    messageBase->setPosition(Vec2(winSize.width - 5.0f - messageBase->getContentSize().width / 2,
                                  winSize.height - getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height + 5.0f + messageBase->getContentSize().height / 2));
    addChild(messageBase, Z_ORDER::Z_MESSAGE_BASE);
}

void HeaderLayer::showMessage(const std::string message)
{
    auto telopMessage = TelopScrollView::initScrollTelop(message, 22, getChildByTag(TAG_SPRITE::MESSAGE_BASE)->getContentSize().width - 20, getChildByTag(TAG_SPRITE::MESSAGE_BASE)->getContentSize().height + 12, Color3B::WHITE);
    telopMessage->setTag(TAG_SPRITE::MESSAGE);
    telopMessage->setPosition(getChildByTag(TAG_SPRITE::MESSAGE_BASE)->getPosition() - Vec2(getChildByTag(TAG_SPRITE::MESSAGE_BASE)->getContentSize().width / 2 - 10, 10));
    addChild(telopMessage, Z_ORDER::Z_MESSAGE);
}

void HeaderLayer::updateInfoLayer()
{
    auto playerNameAndPropertyView = static_cast<PlayerNameAndProperty*>(getChildByTag(TAG_SPRITE::PLAYER_NAME_AND_PROPERTY));
    if (playerNameAndPropertyView) {
        playerNameAndPropertyView->updatePlayerNameAndProperty();
    }
    auto expAndfuel = static_cast<ExpAndFuel*>(getChildByTag(TAG_SPRITE::EXP_AND_FUEL));
    if (expAndfuel) {
        expAndfuel->updateExpAndFuel(true);
    }
}

int HeaderLayer::getFuleTime()
{
    auto expAndfuel = static_cast<ExpAndFuel*>(getChildByTag(TAG_SPRITE::EXP_AND_FUEL));
    if (!expAndfuel) {
        return 0;
    }
    return expAndfuel->getFuelCount();
}

void HeaderLayer::changeMessage(const std::string message)
{
    if (!getChildByTag(TAG_SPRITE::MESSAGE)) {
        return;
    }
    removeChildByTag(TAG_SPRITE::MESSAGE);
    showMessage(message);
}