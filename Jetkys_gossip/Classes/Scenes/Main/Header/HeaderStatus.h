#ifndef syanago_HeaderStatus_h
#define syanago_HeaderStatus_h

USING_NS_CC;

namespace HEADER_STATUS{
    namespace SIZE{
        static const float HEIGHT = 145.0f;
    }
    namespace DIFFERENCE{
        static const float HEIGHT_DIFF = 0.0f;//oldHeaderSize:145 ← このサイズとの差
    }
};

#endif
