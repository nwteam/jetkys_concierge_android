#ifndef __syanago__HeaderBaner__
#define __syanago__HeaderBaner__

#include "cocos2d.h"

USING_NS_CC;

class HeaderBaner : public Layer
{
public:
    HeaderBaner();
    ~HeaderBaner();
    
    static HeaderBaner* createHeaderBaner();
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        BANER_1,
        BANER_2,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_BANER,
    };
    const float BANER_CHAGE_TIME = 5.0f;
    const float BANER_MOVE_TIME = 0.5f;
    const float BANER_EASE_STATUS = 3.0f;
    
    std::vector<std::string> _banerFileName;
    
    bool init();
    void initBanerfile();
    
    void showBackground();
    Sprite* createBaner();
    
    void changeBanerSchedule(float deltatime);
};



#endif