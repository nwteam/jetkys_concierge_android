#include "HeaderBaner.h"

HeaderBaner::HeaderBaner()
{}

HeaderBaner::~HeaderBaner()
{}

HeaderBaner* HeaderBaner::createHeaderBaner()
{
    auto headerBaner = new HeaderBaner();
    headerBaner->init();
    headerBaner->autorelease();
    return headerBaner;
}

bool HeaderBaner::init()
{
    if (!Layer::create) {
        return false;
    }

    initBanerfile();

    showBackground();

    auto baner = createBaner();
    baner->setTag(TAG_SPRITE::BANER_1);
    auto background = getChildByTag(TAG_SPRITE::BACKGROUND);
    baner->setPosition(background->getPosition() - Vec2(baner->getContentSize().width / 2, 0));
    addChild(baner, Z_ORDER::Z_BANER);

    schedule(schedule_selector(HeaderBaner::changeBanerSchedule), BANER_CHAGE_TIME);

    return true;
}

void HeaderBaner::initBanerfile()
{
//    auto plistDic = Dictionary::createWithContentsOfFile("ItemList.plist");//TODO バナーのplist
//    auto Items = (Dictionary*)plistDic->objectForKey("frames");
}

void HeaderBaner::showBackground()
{
    auto background = Sprite::create("info_name_coin_token_base.png");
    background->setPosition(-Vec2(background->getContentSize() / 2));
    background->setTag(TAG_SPRITE::BACKGROUND);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

Sprite* HeaderBaner::createBaner()
{
    auto result = Sprite::create("info_name_coin_token_base.png");
    result->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);

    auto color = LayerColor::create(Color4B(rand() % 255, rand() % 255, rand() % 255, 255), result->getContentSize().width, result->getContentSize().height);
    result->addChild(color);

    return result;
}

void HeaderBaner::changeBanerSchedule(float deltatime)
{
    auto baner1 = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BANER_1));
    auto baner2 = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BANER_2));
    if (!baner1 && !baner2) {
        unschedule(schedule_selector(HeaderBaner::changeBanerSchedule));
        return;
    }
    int oldBanerTag = -1;
    int newBanerTag = -1;
    Sprite* oldBaner;
    if (baner1) {
        oldBanerTag = baner1->getTag();
        oldBaner = baner1;
        newBanerTag = TAG_SPRITE::BANER_2;
    } else if (baner2)    {
        oldBanerTag = baner2->getTag();
        oldBaner = baner2;
        newBanerTag = TAG_SPRITE::BANER_1;
    }

    const Point position = oldBaner->getPosition();

    auto newBaner = createBaner();
    newBaner->setTag(newBanerTag);
    newBaner->setPosition(position + Vec2(newBaner->getContentSize().width, 0));
    addChild(newBaner, Z_ORDER::Z_BANER);

    oldBaner->runAction(Sequence::create(EaseInOut::create(MoveBy::create(BANER_MOVE_TIME, Vec2(-oldBaner->getContentSize().width, 0.0f)), BANER_EASE_STATUS),
                                         CallFunc::create([this, oldBanerTag]() {
        removeChildByTag(oldBanerTag);
    }),
                                         NULL));
    newBaner->runAction(EaseInOut::create(MoveBy::create(BANER_MOVE_TIME, Vec2(-newBaner->getContentSize().width, 0.0f)), BANER_EASE_STATUS));
}