#include "PlayerNameAndProperty.h"
#include "FontDefines.h"
#include "MacroDefines.h"
#include "PlayerController.h"

PlayerNameAndProperty::PlayerNameAndProperty()
{}

PlayerNameAndProperty::~PlayerNameAndProperty()
{}

PlayerNameAndProperty* PlayerNameAndProperty::createPlayerNameAndProperty()
{
    auto playerNameAndProperty = new PlayerNameAndProperty();
    playerNameAndProperty->init();
    playerNameAndProperty->autorelease();
    return playerNameAndProperty;
}

bool PlayerNameAndProperty::init()
{
    if (!Layer::create()) {
        return false;
    }
    showBackGround();
    showPlayerName(PLAYERCONTROLLER->_player->getPlayerName());
    showHoldCoin(PLAYERCONTROLLER->_player->getFreeCoin() + PLAYERCONTROLLER->_player->getPayCoin());
    showHoldToken(PLAYERCONTROLLER->_player->getFreeToken() + PLAYERCONTROLLER->_player->getPayToken());

    return true;
}

void PlayerNameAndProperty::showBackGround()
{
    auto background = Sprite::create("info_name_coin_token_base.png");
    background->setPosition(-Vec2(background->getContentSize() / 2));
    background->setTag(TAG_SPRITE::BACKGROUND);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void PlayerNameAndProperty::showPlayerName(const std::string playerName)
{
    auto nameBase = Sprite::create("info_playername.png");
    nameBase->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 10.0f - nameBase->getContentSize().height / 2));
    addChild(nameBase, Z_ORDER::Z_PLAYERNAME_BASE);

    auto name = Label::createWithTTF(playerName, FONT_NAME_2, 22, nameBase->getContentSize(), TextHAlignment::CENTER, TextVAlignment::CENTER);
    name->setTag(TAG_SPRITE::PLAYERNAME);
    name->setPosition(Vec2(nameBase->getPosition()) + Vec2(0, 12));
    name->setColor(Color3B::BLACK);
    addChild(name, Z_ORDER::Z_PLAYERNAME);
}

void PlayerNameAndProperty::showHoldCoin(const int holdCoin)
{
    auto coinIcon = Sprite::create("coin.png");
    coinIcon->setTag(TAG_SPRITE::COIN_ICON);
    auto background = getChildByTag(TAG_SPRITE::BACKGROUND);
    coinIcon->setPosition(Vec2(background->getPosition()) - Vec2(background->getContentSize() / 2) + Vec2(coinIcon->getContentSize() / 2) + Vec2(10.0f, 13.0f));
    addChild(coinIcon, Z_ORDER::Z_COIN_ICON);

    auto coinLabel = Label::createWithTTF(FormatWithCommas(holdCoin), FONT_NAME_2, 22,  Size(background->getContentSize().width * 0.4f, background->getContentSize().height * 0.5f), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    coinLabel->setTag(TAG_SPRITE::COIN);
    coinLabel->enableOutline(Color4B(Color3B::BLACK), 1);
    coinLabel->setPosition(coinIcon->getPosition() + Vec2(coinIcon->getContentSize().width / 2 + 5, -12));
    coinLabel->setColor(Color3B(255, 231, 117));
    coinLabel->setAnchorPoint(Vec2(0, 0.5));
    addChild(coinLabel, Z_ORDER::Z_COIN);
}

void PlayerNameAndProperty::showHoldToken(const int holdToken)
{
    auto tokenIcon = Sprite::create("token.png");
    tokenIcon->setTag(TAG_SPRITE::TOKEN_ICON);
    auto background = getChildByTag(TAG_SPRITE::BACKGROUND);
    tokenIcon->setPosition(Vec2(background->getPosition()) + Vec2(60, -background->getContentSize().height / 2 + tokenIcon->getContentSize().height / 2 + 12.0f));
    addChild(tokenIcon, Z_ORDER::Z_TOKEN_ICON);

    auto tokenLabel = Label::createWithTTF(FormatWithCommas(holdToken), FONT_NAME_2, 22, Size(background->getContentSize().width * 0.25f, background->getContentSize().height * 0.5f), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    tokenLabel->setTag(TAG_SPRITE::TOKEN);
    tokenLabel->enableOutline(Color4B(Color3B::BLACK), 1);
    tokenLabel->setPosition(tokenIcon->getPosition() + Vec2(tokenIcon->getContentSize().width / 2 + 5, -12));
    tokenLabel->setColor(Color3B::GREEN);
    tokenLabel->setAnchorPoint(Vec2(0, 0.5));
    addChild(tokenLabel, Z_ORDER::Z_TOKEN);
}

void PlayerNameAndProperty::updatePlayerNameAndProperty()
{
    auto playerName = static_cast<Label*>(getChildByTag(TAG_SPRITE::PLAYERNAME));
    if (playerName) {
        playerName->setString(PLAYERCONTROLLER->_player->getPlayerName());
    }
    auto coinLabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::COIN));
    if (coinLabel) {
        coinLabel->setString(FormatWithCommas(PLAYERCONTROLLER->_player->getFreeCoin() + PLAYERCONTROLLER->_player->getPayCoin()));
    }
    auto tokenLabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::TOKEN));
    if (tokenLabel) {
        tokenLabel->setString(FormatWithCommas(PLAYERCONTROLLER->_player->getFreeToken() + PLAYERCONTROLLER->_player->getPayToken()));
    }
}