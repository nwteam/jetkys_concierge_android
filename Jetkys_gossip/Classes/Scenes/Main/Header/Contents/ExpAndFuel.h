#ifndef __syanago__ExpAndFuel__
#define __syanago__ExpAndFuel__

#include "cocos2d.h"
#include "RequestAPI.h"

USING_NS_CC;

using namespace SyanagoAPI;

class ExpAndFuel : public Layer
{
public:
    ExpAndFuel();
    ~ExpAndFuel();
    
    static ExpAndFuel* createExpAndFuel();
    
    void updateExpAndFuel(const bool isEfect);
    
    const int getFuelCount();
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        FUEL_BASE,
        FUEL_PROGRESS,
        FUEL_COVER,
        EXP_BASE,
        EXP_PROGRESS,
        EXP_COVER,
        FUEL_LABEL,
        FUEL_TOTAL_LABEL,
        FUEL_COUNT_SPRITE,
        FUEL_COUNT_LABEL,
        OWNER_RANK_SPRITE,
        OWNER_RANK_LABEL,
        UP_EFFECT,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_FUEL_BASE,
        Z_FUEL_PROGRESS,
        Z_FUEL_COVER,
        Z_EXP_BASE,
        Z_EXP_PROGRESS,
        Z_EXP_COVER,
        Z_FUEL_LABEL,
        Z_FUEL_TOTAL_LABEL,
        Z_FUEL_COUNT_SPRITE,
        Z_FUEL_COUNT_LABEL,
        Z_OWNER_RANK_SPRITE,
        Z_OWNER_RANK_LABEL,
        Z_UP_EFFECT,
    };
    const std::string STR_TIME_REMAIN_OF_FUEL = "%02d:%02d";
    const std::string STR_FUEL_TOTAL = "/%d";
    const float PER_FUEL_MAX = 51.0f;
    const float PER_EXP_MAX = 76.0f;
    const float PROGRESS_DELTA = 20.0f;
    const float PROGRESS_SCHEDULE_TIME = 0.01f;
    
    RequestAPI* _request;
    int _fuelTime;
    
    float _fuelProgressPercent;
    float _expProgressPercent;
    
    bool init();
    
    void showBackground();
    void showExp();
    void showOwnerRank(const int ownerRank);
    void showFuel();
    void showFuelLabel(const int holdFuel, const int totalFuel);
    
    void initFuelTimer();
    
    void updateFuelTime(float deltaTime);
    void stopFuelTimer();
    void updateBackgroundTimeAndRaceTime();
    void supplyWhitFuel();
    
    const int getPlayerMaxFuel();
    
    void responseSendFuel(Json* responce);
    
    void updateFuelProgress(const float progressPercent, const bool isEfect);
    void scheduleFuelProgress(float deltaTime);
    
    void updateExpProgress(const float progressPercent, const bool isEfect);
    void scheduleExpProgress(float deltaTime);
    
    void showUpEffect(const int upStatus, const Point position);
};

#endif