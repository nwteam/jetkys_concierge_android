#ifndef __syanago__PlayerNameAndProperty__
#define __syanago__PlayerNameAndProperty__

#include "cocos2d.h"

USING_NS_CC;

class PlayerNameAndProperty : public Layer
{
public:
    PlayerNameAndProperty();
    ~PlayerNameAndProperty();
    
    static PlayerNameAndProperty* createPlayerNameAndProperty();
    
    void updatePlayerNameAndProperty();
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        PLAYERNAME_BASE,
        PLAYERNAME,
        COIN_ICON,
        COIN,
        TOKEN_ICON,
        TOKEN,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_PLAYERNAME_BASE,
        Z_PLAYERNAME,
        Z_COIN_ICON,
        Z_COIN,
        Z_TOKEN_ICON,
        Z_TOKEN,
    };
    
    bool init();
    
    void showBackGround();
    void showPlayerName(const std::string playerName);
    void showHoldCoin(const int holdCoin);
    void showHoldToken(const int holdToken);
    
};

#endif