#include "ExpAndFuel.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include "TreasureItemEffectModel.h"
#include "RankModel.h"
#include "SystemSettingModel.h"
#include "UserDefaultManager.h"

ExpAndFuel::ExpAndFuel():
    _fuelTime(0)
    , _fuelProgressPercent(0.0f)
    , _request(nullptr)
{
    _request = new RequestAPI();
}

ExpAndFuel::~ExpAndFuel()
{
    delete _request;
    _request = nullptr;
}

ExpAndFuel* ExpAndFuel::createExpAndFuel()
{
    auto expAndFuel = new ExpAndFuel();
    expAndFuel->init();
    expAndFuel->autorelease();
    return expAndFuel;
}

bool ExpAndFuel::init()
{
    if (!Layer::create()) {
        return false;
    }

    initFuelTimer();

    showBackground();
    showExp();
    showOwnerRank(PLAYERCONTROLLER->_player->getRank());
    showFuel();
    showFuelLabel(PLAYERCONTROLLER->_player->getFuel(), getPlayerMaxFuel());

    updateExpAndFuel(false);

    schedule(schedule_selector(ExpAndFuel::updateFuelTime), 1.0f);
    return true;
}

void ExpAndFuel::showBackground()
{
    auto background = Sprite::create("info_exp_fuel_base.png");
    background->setPosition(Vec2(background->getContentSize() / 2));
    background->setTag(TAG_SPRITE::BACKGROUND);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void ExpAndFuel::showExp()
{
    auto base = Sprite::create("info_exp_gerge.png");
    base->setTag(TAG_SPRITE::EXP_BASE);
    auto background = getChildByTag(TAG_SPRITE::BACKGROUND);
    base->setPosition(background->getPosition() + Vec2(background->getContentSize().width / 2 - 12.0f - base->getContentSize().width / 2, 0));
    addChild(base, Z_ORDER::Z_EXP_BASE);

    auto progressTimer = ProgressTimer::create(Sprite::create("info_exp_hidden.png"));
    progressTimer->setTag(TAG_SPRITE::EXP_PROGRESS);
    progressTimer->setType(ProgressTimer::Type::RADIAL);
    progressTimer->setPercentage(0.0f);
    progressTimer->setPosition(Point(base->getPosition().x, base->getPosition().y - 2));
    progressTimer->setRotation(-138);
    addChild(progressTimer, Z_ORDER::Z_EXP_PROGRESS);

    auto cover = Sprite::create("info_exp_base.png");
    cover->setTag(TAG_SPRITE::EXP_COVER);
    cover->setPosition(Point(base->getPosition().x, base->getPosition().y - 10));
    addChild(cover, Z_ORDER::Z_EXP_COVER);
}

void ExpAndFuel::showOwnerRank(const int ownerRank)
{
    auto sprite = Sprite::create("info_rank.png");
    sprite->setTag(TAG_SPRITE::OWNER_RANK_SPRITE);
    auto base = getChildByTag(TAG_SPRITE::EXP_BASE);
    sprite->setPosition(Point(base->getPosition().x, base->getPosition().y + 13));
    addChild(sprite, Z_ORDER::Z_OWNER_RANK_SPRITE);

    auto label = Label::createWithTTF(StringUtils::format("%d", ownerRank), FONT_NAME_2, 34);
    label->setTag(TAG_SPRITE::OWNER_RANK_LABEL);
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->enableOutline(Color4B(Color3B::BLACK), 2);
    label->setPosition(Point(base->getPosition().x, base->getPosition().y - 34));
    label->setColor(COLOR_YELLOW);
    addChild(label, Z_ORDER::Z_OWNER_RANK_LABEL);
}

void ExpAndFuel::showFuel()
{
    auto base = Sprite::create("info_fuel_gerge.png");
    base->setTag(TAG_SPRITE::FUEL_BASE);
    auto background = getChildByTag(TAG_SPRITE::BACKGROUND);
    base->setPosition(background->getPosition() + Vec2(-background->getContentSize().width / 2 + 12.0f + base->getContentSize().width / 2, 0));
    addChild(base, Z_ORDER::Z_FUEL_BASE);

    auto progressTimer = ProgressTimer::create(Sprite::create("info_fuel_hidden.png"));
    progressTimer->setTag(TAG_SPRITE::FUEL_PROGRESS);
    progressTimer->setType(ProgressTimer::Type::RADIAL);
    progressTimer->setPercentage(0.0f);
    progressTimer->setPosition(Point(base->getPosition().x - 0.5, base->getPosition().y - 3));
    progressTimer->setRotation(-137);
    addChild(progressTimer, Z_ORDER::Z_FUEL_PROGRESS);

    auto cover = Sprite::create("info_fuel_base.png");
    cover->setTag(TAG_SPRITE::FUEL_COVER);
    cover->setPosition(Point(base->getPosition().x + 10, base->getPosition().y - 10));
    addChild(cover, Z_ORDER::Z_FUEL_COVER);
}

void ExpAndFuel::showFuelLabel(const int holdFuel, const int totalFuel)
{
    auto label = Label::createWithTTF(StringUtils::format("%d", holdFuel), FONT_NAME_2, 20, Size(100, 30));
    label->setTag(TAG_SPRITE::FUEL_LABEL);
    label->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    label->setColor(Color3B::ORANGE);
    label->enableOutline(Color4B(Color3B::BLACK), 2);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    auto base = getChildByTag(TAG_SPRITE::FUEL_BASE);
    label->setPosition(base->getPosition() + Vec2(0, 23));
    addChild(label, Z_ORDER::Z_FUEL_LABEL);

    auto totalLabel = Label::createWithTTF(StringUtils::format(STR_FUEL_TOTAL.c_str(), totalFuel), FONT_NAME_2, 18, Size(100, 30));
    totalLabel->setTag(TAG_SPRITE::FUEL_TOTAL_LABEL);
    totalLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    totalLabel->setColor(Color3B::RED);
    totalLabel->enableOutline(Color4B(Color3B::BLACK), 2);
    totalLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    totalLabel->setPosition(base->getPosition() + Vec2(0, 21));
    addChild(totalLabel, Z_ORDER::Z_FUEL_TOTAL_LABEL);

    auto countSprite = Sprite::create("info_nokori_time.png");
    countSprite->setTag(TAG_SPRITE::FUEL_COUNT_SPRITE);

    auto countLabel = Label::createWithTTF(StringUtils::format(STR_TIME_REMAIN_OF_FUEL.c_str(), _fuelTime / 60, _fuelTime % 60), FONT_NAME_2, 22);
    countLabel->setTag(TAG_SPRITE::FUEL_COUNT_LABEL);
    countLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    countLabel->setColor(Color3B::WHITE);
    countLabel->enableOutline(Color4B(Color3B::BLACK), 2);

    countLabel->setPosition(base->getPosition() + Vec2(countSprite->getContentSize().width / 2, -25));
    countSprite->setPosition(countLabel->getPosition() + Vec2(-(countSprite->getContentSize().width / 2 + countLabel->getContentSize().width / 2), 11));
    addChild(countLabel, Z_ORDER::Z_FUEL_COUNT_LABEL);
    addChild(countSprite, Z_ORDER::Z_FUEL_COUNT_SPRITE);
}

void ExpAndFuel::initFuelTimer()
{
    if (CCUserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::TIME_OF_HEADER) != 0) {
        _fuelTime = CCUserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::TIME_OF_HEADER);
        CCUserDefault::getInstance()->setIntegerForKey(UserDefaultManager::KEY::TIME_OF_HEADER, 0);
    } else {
        const std::shared_ptr<SystemSettingModel>model(SystemSettingModel::getModel());
        _fuelTime = model->getFuelRecoveryInterval() * 60;
    }
    updateBackgroundTimeAndRaceTime();
}

void ExpAndFuel::updateFuelTime(float deltaTime)
{
    if (PLAYERCONTROLLER->_player->getFuel() >= getPlayerMaxFuel()) {
        stopFuelTimer();
        return;
    }
    updateBackgroundTimeAndRaceTime();

    _fuelTime--;

    int fuelTimeMin;
    int fuelTimeSec;
    if (_fuelTime < 0) {
        fuelTimeMin = 0;
        fuelTimeSec = 0;
    } else {
        fuelTimeMin = _fuelTime / 60;
        fuelTimeSec = _fuelTime % 60;
    }

    auto fuelTimeLabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::FUEL_COUNT_LABEL));
    if (fuelTimeLabel) {
        fuelTimeLabel->setString(StringUtils::format(STR_TIME_REMAIN_OF_FUEL.c_str(), fuelTimeMin, fuelTimeSec));
    }

    if (_fuelTime <= 0) {
        supplyWhitFuel();
    }
}

void ExpAndFuel::stopFuelTimer()
{
    if (CCUserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::TIME_OF_RACE_START) != 0) {
        CCUserDefault::getInstance()->setIntegerForKey(UserDefaultManager::KEY::TIME_OF_RACE_START, 0);
    }
    if (CCUserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::TIME_OF_BACKGROUND_STAY) != 0) {
        CCUserDefault::getInstance()->setIntegerForKey(UserDefaultManager::KEY::TIME_OF_BACKGROUND_STAY, 0);
    }

    auto fuelLabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::FUEL_COUNT_LABEL));
    if (fuelLabel) {
        fuelLabel->setString(StringUtils::format(STR_TIME_REMAIN_OF_FUEL.c_str(), 0, 0));
    }
    unschedule(schedule_selector(ExpAndFuel::updateFuelTime));
}

void ExpAndFuel::updateBackgroundTimeAndRaceTime()
{
    if (CCUserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::TIME_OF_RACE_START) != 0) {
        _fuelTime -= (int)time(NULL) - CCUserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::TIME_OF_RACE_START);
        CCUserDefault::getInstance()->setIntegerForKey(UserDefaultManager::KEY::TIME_OF_RACE_START, 0);
    }

    if (CCUserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::TIME_OF_BACKGROUND_STAY) != 0) {
        _fuelTime -= CCUserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::TIME_OF_BACKGROUND_STAY);
        CCUserDefault::getInstance()->setIntegerForKey(UserDefaultManager::KEY::TIME_OF_BACKGROUND_STAY, 0);
    }

    if (_fuelTime < 0) {
        const std::shared_ptr<SystemSettingModel>model(SystemSettingModel::getModel());
        const int recoverCount = model->getFuelRecoveryInterval() * 60;
        int supplyWithFuel = abs(_fuelTime / recoverCount);
        int time = _fuelTime + recoverCount * supplyWithFuel;
        while (time <= 0) {
            supplyWithFuel++;
            time = _fuelTime + recoverCount * supplyWithFuel;
        }
        _fuelTime = time;
        if (getPlayerMaxFuel() < PLAYERCONTROLLER->_player->getFuel() + supplyWithFuel) {
            PLAYERCONTROLLER->_player->setFuel(getPlayerMaxFuel());
        } else {
            PLAYERCONTROLLER->_player->setFuel(PLAYERCONTROLLER->_player->getFuel() + supplyWithFuel);
        }
        _request->sendFuelRequest(PLAYERCONTROLLER->_player->getRank(), PLAYERCONTROLLER->_player->getFuel(), CC_CALLBACK_1(ExpAndFuel::responseSendFuel, this));
    }
}

void ExpAndFuel::supplyWhitFuel()
{
    if (getPlayerMaxFuel() < PLAYERCONTROLLER->_player->getFuel() + 1) {
        PLAYERCONTROLLER->_player->setFuel(getPlayerMaxFuel());
    } else {
        PLAYERCONTROLLER->_player->setFuel(PLAYERCONTROLLER->_player->getFuel() + 1);
    }
    const float fuelPer = (float) PLAYERCONTROLLER->_player->getFuel() / getPlayerMaxFuel() * PER_FUEL_MAX;
    const std::shared_ptr<SystemSettingModel>model(SystemSettingModel::getModel());
    const int recoverCount = model->getFuelRecoveryInterval() * 60;
    _fuelTime += recoverCount;

    updateFuelProgress(fuelPer, true);

    auto fuelLabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::FUEL_LABEL));
    if (fuelLabel) {
        fuelLabel->setString(StringUtils::format("%d", PLAYERCONTROLLER->_player->getFuel()));
    }
    _request->sendFuelRequest(PLAYERCONTROLLER->_player->getRank(), PLAYERCONTROLLER->_player->getFuel(), CC_CALLBACK_1(ExpAndFuel::responseSendFuel, this));
}

void ExpAndFuel::responseSendFuel(Json* responce)
{
    Json* jsonData = Json_getItem(responce, "log_action");
    std::string result = Json_getString(jsonData, "result", "false");
    if (result != "true") {
        CCASSERT(100, "ネットワークエラー[ExpAndFuel::responseSendFuel]result!=true");
    }
    updateExpAndFuel(true);
}

const int ExpAndFuel::getPlayerMaxFuel()
{
    std::shared_ptr<RankModel>rankModel(RankModel::find(PLAYERCONTROLLER->_player->getRank()));
    return rankModel->getMaxFuel() + TreasureItemEffectModel::getTreasureItemIncrementalDifferenceEffect(ITEM::EFFECT_TYPE::MAX_FUEL_UP);
}

void ExpAndFuel::updateFuelProgress(const float progressPercent, const bool isEfect)
{
    auto fuelProgressTimer = static_cast<ProgressTimer*>(getChildByTag(TAG_SPRITE::FUEL_PROGRESS));
    if (!fuelProgressTimer) {
        return;
    }
    if (isEfect == true) {
        _fuelProgressPercent = progressPercent;
        schedule(schedule_selector(ExpAndFuel::scheduleFuelProgress), PROGRESS_SCHEDULE_TIME);
    } else {
        fuelProgressTimer->setPercentage(progressPercent);
    }
}

void ExpAndFuel::scheduleFuelProgress(float deltaTime)
{
    auto fuelProgressTimer = static_cast<ProgressTimer*>(getChildByTag(TAG_SPRITE::FUEL_PROGRESS));
    if (!fuelProgressTimer) {
        unschedule(schedule_selector(ExpAndFuel::scheduleFuelProgress));
        return;
    }
    float percent = fuelProgressTimer->getPercentage();
    percent += PROGRESS_DELTA * deltaTime;
    if (percent > _fuelProgressPercent) {
        fuelProgressTimer->setPercentage(_fuelProgressPercent);
        unschedule(schedule_selector(ExpAndFuel::scheduleFuelProgress));
        _fuelProgressPercent = 0;
    } else {
        fuelProgressTimer->setPercentage(percent);
    }
}

void ExpAndFuel::updateExpProgress(const float progressPercent, const bool isEfect)
{
    auto expProgressTimer = static_cast<ProgressTimer*>(getChildByTag(TAG_SPRITE::EXP_PROGRESS));
    if (!expProgressTimer) {
        return;
    }
    if (isEfect == true) {
        _expProgressPercent = progressPercent;
        schedule(schedule_selector(ExpAndFuel::scheduleExpProgress), PROGRESS_SCHEDULE_TIME);
    } else {
        expProgressTimer->setPercentage(progressPercent);
    }
}

void ExpAndFuel::scheduleExpProgress(float deltaTime)
{
    auto expProgressTimer = static_cast<ProgressTimer*>(getChildByTag(TAG_SPRITE::EXP_PROGRESS));
    if (!expProgressTimer) {
        unschedule(schedule_selector(ExpAndFuel::scheduleExpProgress));
        return;
    }
    float percent = expProgressTimer->getPercentage();
    percent += PROGRESS_DELTA * deltaTime;
    if (percent > _expProgressPercent) {
        expProgressTimer->setPercentage(_expProgressPercent);
        unschedule(schedule_selector(ExpAndFuel::scheduleExpProgress));
        _expProgressPercent = 0;
    } else {
        expProgressTimer->setPercentage(percent);
    }
}

void ExpAndFuel::showUpEffect(const int upStatus, const Point position)
{
    if (upStatus <= 0) {
        return;
    }
    if (getChildByTag(TAG_SPRITE::UP_EFFECT)) {
        removeChildByTag(TAG_SPRITE::UP_EFFECT);
    }
    auto label = Label::createWithTTF(StringUtils::format("+%d", upStatus), FONT_NAME_2, 20);
    label->setTag(TAG_SPRITE::UP_EFFECT);
    label->setOpacity(0);
    label->setPosition(position - Vec2(0, 20));
    label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    label->runAction(Spawn::createWithTwoActions(EaseBackInOut::create(MoveBy::create(3.0f, Vec2(0, 20))),
                                                 Sequence::create(FadeIn::create(0.5f), DelayTime::create(1.0f), FadeOut::create(1.5f), NULL)));
    addChild(label, Z_ORDER::Z_UP_EFFECT);
}

void ExpAndFuel::updateExpAndFuel(const bool isEfect)
{
    const int rank = PLAYERCONTROLLER->_player->getRank();

    const int fuel = PLAYERCONTROLLER->_player->getFuel();
    const int fuelTotal = getPlayerMaxFuel();
    if (fuel >= fuelTotal) {
        stopFuelTimer();
    }
    const float fuelPer = (float) fuel / fuelTotal * PER_FUEL_MAX;

    const int64_t exp = PLAYERCONTROLLER->_player->getTotalExp();
    const std::shared_ptr<RankModel>rankModel(RankModel::find(rank));
    const int64_t expCurrentRank = rankModel->getRequiredExp();
    double expPercentage;
    if (rank == 99) {
        expPercentage = 1.0f;
    } else {
        const std::shared_ptr<RankModel>rankModel(RankModel::find(rank + 1));
        int64_t expNextRank = rankModel->getRequiredExp();
        expPercentage = ((double)(exp - expCurrentRank)) / (expNextRank - expCurrentRank);
    }

    auto fuelLabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::FUEL_LABEL));
    if (fuelLabel) {
        const int oldFuel = atoi(fuelLabel->getString().c_str());
        showUpEffect(fuel - oldFuel, fuelLabel->getPosition());
        fuelLabel->setString(StringUtils::format("%d", fuel));
    }
    auto fuelTotalLabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::FUEL_TOTAL_LABEL));
    if (fuelTotalLabel) {
        fuelTotalLabel->setString(StringUtils::format(STR_FUEL_TOTAL.c_str(), fuelTotal));
    }

    updateFuelProgress(fuelPer, isEfect);

    auto ownerRankLabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::OWNER_RANK_LABEL));
    if (ownerRankLabel) {
        ownerRankLabel->setString(StringUtils::format("%d", rank));
    }
    updateExpProgress((float) expPercentage * PER_EXP_MAX, isEfect);
}

const int ExpAndFuel::getFuelCount()
{
    return _fuelTime;
}