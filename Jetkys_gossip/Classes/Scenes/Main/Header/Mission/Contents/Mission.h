#ifndef __syanago__Mission__
#define __syanago__Mission__

#include "cocos2d.h"
#include "MissionList.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class Mission : public Layer
{
public:
    static Mission* createMissionView(std::shared_ptr<MissionList> mission, const int tag, const cocos2d::ui::Layout::ccWidgetTouchCallback& callback);
private:
    enum STATUS{
        UNCOMPLETE = 1,
        COMPLETE = 2,
    };
    
    void showMissionView(std::shared_ptr<MissionList> mission, const int tag, const cocos2d::ui::Layout::ccWidgetTouchCallback& callback);
    void showMissionClearCount(ui::Button* missionButton, const int status, const int clearCount, const int clearCountOfComplete);
    void showMissionName(ui::Button* missionButton, const std::string missionName);
    void showMissionEndTime(ui::Button* missionButton, const std::string endTime);
};

#endif
