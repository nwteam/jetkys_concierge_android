#include "MissionDetails.h"
#include "FontDefines.h"
#include "TimeConverter.h"
#include "TelopScrollView.h"
#include "CharacterModel.h"
#include "PartModel.h"

bool MissionDetails::init(std::shared_ptr<MissionList>mission)
{
    if (!Layer::init()) {
        return false;
    }

    showMissionEndTime(mission->getEndDatetime());

    auto base = Sprite::create("mission_details_base.png");
    base->setPosition(Vec2(BACKGROUND_SIZE / 2) + Vec2(0, 15));
    addChild(base, Z_ORDER::Z_BASE);

    showMissionId(base, mission->getMissionId());
    showMissionName(base, mission->getName());
    showMissionText(base, mission->getContent());
    showMissionCondition(base, mission->getCondition());
    showMissionClearCount(base, mission->getStatus(), mission->getClearCount(), mission->getClearCountForComplete());
    showMissionReward(base, mission);

    return true;
}

void MissionDetails::showMissionEndTime(const std::string endTime)
{
    auto label = Label::createWithTTF("残り時間：" + TimeConverter::convertEndTimeString(endTime), FONT_NAME_2, 22);
    label->setColor(Color3B::BLACK);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    label->setPosition(Vec2(BACKGROUND_SIZE.width - 30, BACKGROUND_SIZE.height - 105));
    label->setTag(TAG_SPRITE::END_TIME);
    addChild(label, Z_ORDER::Z_END_TIME);
}

void MissionDetails::showMissionId(Sprite* base, const int missionId)
{
    std::string missionIdString = "mission  ";
    if (missionId < 10) {
        missionIdString += "no.00" + StringUtils::format("%d", missionId);
    } else if (9 < missionId && missionId < 100) {
        missionIdString += "no.0" + StringUtils::format("%d", missionId);
    } else if (99 < missionId) {
        missionIdString += StringUtils::format("no.%d", missionId);
    }
    auto label = Label::createWithTTF(missionIdString, FONT_NAME_2, 22);
    label->setColor(Color3B::BLACK);
    label->setPosition(Vec2(base->getContentSize().width / 2, base->getContentSize().height - 40));
    base->addChild(label);
}

void MissionDetails::showMissionName(Sprite* base, const std::string missionName)
{
    const float viewMaxStringSize = 400;
    float delta = 0;
    const float labelSize = Label::createWithTTF(missionName, FONT_NAME_2, 26)->getContentSize().width;
    if (labelSize <= viewMaxStringSize) {
        delta = viewMaxStringSize - labelSize;
    }
    auto telop = TelopScrollView::initScrollTelop(missionName, 26, viewMaxStringSize, 26, Color3B::BLACK);
    telop->setPosition(Vec2((base->getContentSize().width - viewMaxStringSize + delta) / 2, base->getContentSize().height - 82));
    base->addChild(telop);
}

void MissionDetails::showMissionText(Sprite* base, const std::string missionText)
{
    auto text = createTextScrollView(missionText, Size(400, 120));
    text->setPosition(Vec2(base->getContentSize().width / 2 - 200, base->getContentSize().height - 220));
    base->addChild(text);
}

void MissionDetails::showMissionCondition(Sprite* base, const std::string missionCondition)
{
    const float viewMaxStringSize = 400;
    float delta = 0;
    const float labelSize = Label::createWithTTF(missionCondition, FONT_NAME_2, 26)->getContentSize().width;
    if (labelSize <= viewMaxStringSize) {
        delta = viewMaxStringSize - labelSize;
    }
    auto telop = TelopScrollView::initScrollTelop(missionCondition, 26, viewMaxStringSize, 26, Color3B::BLACK);
    telop->setPosition(Vec2((base->getContentSize().width - viewMaxStringSize + delta) / 2, base->getContentSize().height / 2 - 5));
    base->addChild(telop);
}

void MissionDetails::showMissionClearCount(Sprite* base, const int status, const int clearCount, const int clearCountOfComplete)
{
    Sprite* clearBase;
    if (status == 1) {
        clearBase = Sprite::create("mission_non_complete_label.png");
        auto clearCountLabel = Label::createWithTTF(StringUtils::format("%d", clearCount), FONT_NAME_2, 32);
        clearCountLabel->setPositionY(clearBase->getContentSize().height / 2 - 16);
        clearCountLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
        auto clearCountOfCompleteLabel = Label::createWithTTF(StringUtils::format("/ %d回", clearCountOfComplete), FONT_NAME_2, 24);
        clearCountOfCompleteLabel->setPositionY(clearBase->getContentSize().height / 2 - 13);
        clearCountOfCompleteLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);

        const float labelSize =  clearCountLabel->getContentSize().width + clearCountOfCompleteLabel->getContentSize().width + 10;
        clearCountLabel->setPositionX(clearBase->getContentSize().width / 2 - labelSize / 2);
        clearCountOfCompleteLabel->setPositionX(clearCountLabel->getPositionX() + clearCountLabel->getContentSize().width + 10);

        clearBase->addChild(clearCountLabel);
        clearBase->addChild(clearCountOfCompleteLabel);
    } else if (status == 2) {
        clearBase = Sprite::create("mission_complete_label.png");
    } else {
        return;
    }
    clearBase->setPosition(Vec2(base->getContentSize() / 2) - Vec2(0, 40));
    base->addChild(clearBase);
}

void MissionDetails::showMissionReward(Sprite* base, std::shared_ptr<MissionList>mission)
{
    std::string rewardText = "";
    if (mission->isGetReward() == true) {
        if (mission->getMasterCharacterId() > 0) {
            std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(mission->getMasterCharacterId()));
            rewardText += "・" + characterModel->getCarName() + "\n";
        }
        if (mission->getMasterPartsId() > 0) {
            std::shared_ptr<PartModel>partsModel(PartModel::find(mission->getMasterPartsId()));
            rewardText += "・" + partsModel->getName() + "\n";
        }
        if (mission->getFreeCoin() > 0) {
            std::string format = StringUtils::format("・コイン %d枚", mission->getFreeCoin());
            rewardText += format + "\n";
        }
        if (mission->getFreeToken() > 0) {
            std::string format = StringUtils::format("・トークン %d枚", mission->getFreeToken());
            rewardText += format + "\n";
        }
        if (mission->getFriendPoint() > 0) {
            std::string format = StringUtils::format("・フレンドポイント %d", mission->getFriendPoint());
            rewardText += format + "\n";
        }
        for (int i = 0; i < mission->getRewardItemIds().size(); i++) {
            rewardText += "・" + mission->getRewardItemIds().at(i)->getItemName() + StringUtils::format("：%d個", mission->getRewardItemIds().at(i)->getItemCount()) + "\n";
        }
        if (mission->getRewardTicket()) {
            rewardText += "・" + mission->getRewardTicket()->getName() + StringUtils::format("：%d個", mission->getRewardTicket()->getCount()) + "\n";
        }
    } else {
        rewardText = "・報酬無し";
    }
    auto text = createTextScrollView(rewardText, Size(400, 120));
    text->setPosition(Vec2(base->getContentSize().width / 2 - 200, base->getContentSize().height / 2 - 275));
    base->addChild(text);
}

ScrollView* MissionDetails::createTextScrollView(std::string text, Size scrollViewSize)
{
    auto result = ScrollView::create(scrollViewSize);
    result->setDirection(ScrollView::Direction::VERTICAL);
    result->setBounceable(false);
    auto label = Label::createWithTTF(text, FONT_NAME_2, 22);
    label->setColor(Color3B::BLACK);
    label->setDimensions(scrollViewSize.width, 0);
    label->setDimensions(label->getContentSize().width, label->getContentSize().height);
    label->setHorizontalAlignment(TextHAlignment::LEFT);
    result->setContainer(label);
    result->setContentOffset(Point(0, 0 - (label->getContentSize().height - scrollViewSize.height)));
    return result;
}