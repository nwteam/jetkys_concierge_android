#ifndef __syanago__MissionDetails__
#define __syanago__MissionDetails__

#include "cocos2d.h"
#include "create_func.h"
#include "MissionList.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class MissionDetails : public Layer, public create_func<MissionDetails>
{
public:
    using create_func::create;
    bool init(std::shared_ptr<MissionList> mission);
private:
    enum TAG_SPRITE{
        BASE = 0,
        END_TIME,
    };
    enum Z_ORDER{
        Z_BASE = 0,
        Z_END_TIME,
    };
    const Size BACKGROUND_SIZE = Size(576,860);
    
    void showMissionEndTime(const std::string endTime);
    
    void showMissionId(Sprite* base, const int missionId);
    void showMissionName(Sprite* base, const std::string missionName);
    void showMissionText(Sprite* base, const std::string missionText);
    void showMissionCondition(Sprite* base, const std::string missionCondition);
    void showMissionClearCount(Sprite* base, const int status, const int clearCount, const int clearCountOfComplete);
    void showMissionReward(Sprite* base, std::shared_ptr<MissionList> mission);
    
    ScrollView* createTextScrollView(std::string Text, Size ScrollViewSize);
};

#endif
