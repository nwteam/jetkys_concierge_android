#include "Mission.h"
#include "FontDefines.h"
#include "TelopScrollView.h"
#include "TimeConverter.h"

Mission* Mission::createMissionView(std::shared_ptr<MissionList>mission, const int tag, const cocos2d::ui::Layout::ccWidgetTouchCallback& callback)
{
    auto missionView = new Mission();
    missionView->showMissionView(mission, tag, callback);
    missionView->autorelease();
    return missionView;
}

void Mission::showMissionView(std::shared_ptr<MissionList>mission, const int tag, const cocos2d::ui::Layout::ccWidgetTouchCallback& callback)
{
    ui::Button* missionButton = ui::Button::create("mission_select_base.png");
    missionButton->addTouchEventListener(callback);
    missionButton->setTag(tag);
    missionButton->setAnchorPoint(Vec2::ZERO);
    missionButton->setZoomScale(0);
    missionButton->setSwallowTouches(false);
    showMissionName(missionButton, mission->getName());
    showMissionClearCount(missionButton, mission->getStatus(), mission->getClearCount(), mission->getClearCountForComplete());
    showMissionEndTime(missionButton, mission->getEndDatetime());
    addChild(missionButton);
}

void Mission::showMissionName(ui::Button* missionButton, const std::string missionName)
{
    const float viewMaxStringSize = 400;
    float delta = 0;
    const float labelSize = Label::createWithTTF(missionName, FONT_NAME_2, 26)->getContentSize().width;
    if (labelSize <= viewMaxStringSize) {
        delta = viewMaxStringSize - labelSize;
    }
    auto telop = TelopScrollView::initScrollTelop(missionName, 26, viewMaxStringSize, 26, Color3B::BLACK);
    telop->setPosition(Vec2((missionButton->getContentSize().width - viewMaxStringSize + delta) / 2, missionButton->getContentSize().height - 45));
    missionButton->addChild(telop);
}

void Mission::showMissionClearCount(ui::Button* missionButton, const int status, const int clearCount, const int clearCountOfComplete)
{
    Sprite* base;
    if (status == STATUS::UNCOMPLETE) {
        base = Sprite::create("mission_non_complete_label.png");
        auto clearCountLabel = Label::createWithTTF(StringUtils::format("%d", clearCount), FONT_NAME_2, 32);
        clearCountLabel->setPositionY(base->getContentSize().height / 2 - 16);
        clearCountLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
        auto clearCountOfCompleteLabel = Label::createWithTTF(StringUtils::format("/ %d回", clearCountOfComplete), FONT_NAME_2, 24);
        clearCountOfCompleteLabel->setPositionY(base->getContentSize().height / 2 - 13);
        clearCountOfCompleteLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);

        const float labelSize =  clearCountLabel->getContentSize().width + clearCountOfCompleteLabel->getContentSize().width + 10;
        clearCountLabel->setPositionX(base->getContentSize().width / 2 - labelSize / 2);
        clearCountOfCompleteLabel->setPositionX(clearCountLabel->getPositionX() + clearCountLabel->getContentSize().width + 10);

        base->addChild(clearCountLabel);
        base->addChild(clearCountOfCompleteLabel);
    } else if (status == STATUS::COMPLETE)    {
        base = Sprite::create("mission_complete_label.png");
    } else   {
        return;
    }
    base->setPosition(Vec2(missionButton->getContentSize() / 2));
    missionButton->addChild(base);
}

void Mission::showMissionEndTime(ui::Button* missionButton, const std::string endTime)
{
    auto label = Label::createWithTTF("残り時間：" + TimeConverter::convertEndTimeString(endTime), FONT_NAME_2, 26);
    label->setColor(Color3B::BLACK);
    label->setPosition(Vec2(missionButton->getContentSize().width / 2, 30));
    missionButton->addChild(label);
}