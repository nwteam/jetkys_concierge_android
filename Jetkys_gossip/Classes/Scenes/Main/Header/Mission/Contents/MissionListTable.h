#ifndef __syanago__MissionListTable__
#define __syanago__MissionListTable__

#include "cocos2d.h"
#include "create_func.h"
#include "MissionList.h"
#include <functional>
#include "cocos-ext.h"

USING_NS_CC_EXT;

class MissionListTable : public Layer, public create_func<MissionListTable>, public TableViewDelegate, public TableViewDataSource
{
public:
    using create_func::create;
    
    MissionListTable():
    _onTapDetailsCallback(nullptr),
    buttonMoveFlag(false)
    {}
    
    typedef std::function<void(std::shared_ptr<MissionList>)> OnResponceCallback;
    bool init(std::vector<std::shared_ptr<MissionList>> missions, const OnResponceCallback& onTapDetailsCallback);
    
    virtual void scrollViewDidScroll(ScrollView* view);
    virtual void scrollViewDidZoom(ScrollView* view){};
    virtual Size cellSizeForTable(TableView* table);
    virtual TableViewCell* tableCellAtIndex(TableView* table,ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(TableView* table);
    virtual void tableCellTouched(TableView* table,TableViewCell* cell){};
private:
    enum Z_ORDER{
        Z_LABEL = 0,
        Z_TABEL,
    };
    enum TAG_SPRITE{
        LABEL = 0,
        TABLE,
        BAR,
    };
    
    const int TOUCH_AREA_TOP = 530;
    const int TOUCH_AREA_BUTTOM = -100;
    const int BUTTON_MOVE_DEF = 5;
    
    const Size BACKGROUND_SIZE = Size(576,860);
    const Size CELL_SIZE = Size(496,206);
    const Size TABLE_SIZE = Size(496, 615);
    OnResponceCallback _onTapDetailsCallback;
    std::vector<std::shared_ptr<MissionList>> _missions;
    bool buttonMoveFlag;
    int _buttonMovePositionY;
    
    void showMissionExplanation();
    void showTableView(std::vector<std::shared_ptr<MissionList>> missions);
    void showScrollBar();
    void onTapMission(Ref* sender, ui::Widget::TouchEventType type);
};

#endif 
