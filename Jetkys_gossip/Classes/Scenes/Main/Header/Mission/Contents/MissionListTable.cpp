#include "MissionListTable.h"
#include "FontDefines.h"
#include "ScrollBar.h"
#include "Mission.h"


bool MissionListTable::init(std::vector<std::shared_ptr<MissionList> >missions, const OnResponceCallback& onTapDetailsCallback)
{
    if (!Layer::init()) {
        return false;
    }
    _onTapDetailsCallback = onTapDetailsCallback;
    _missions = missions;

    showMissionExplanation();
    showTableView(missions);
    showScrollBar();
    return true;
}

void MissionListTable::showMissionExplanation()
{
    auto label = Label::createWithTTF("※タップして詳細を確認することができます", FONT_NAME_2, 22);
    label->setColor(Color3B::BLACK);
    label->setPosition(Vec2(BACKGROUND_SIZE.width / 2, BACKGROUND_SIZE.height - 105));
    label->setTag(TAG_SPRITE::LABEL);
    addChild(label, Z_ORDER::Z_LABEL);
}

void MissionListTable::showTableView(std::vector<std::shared_ptr<MissionList> >missionDetails)
{
    TableView* tableView = TableView::create(this, TABLE_SIZE);
    tableView->setDirection(TableView::Direction::VERTICAL);
    tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
    tableView->setDelegate(this);
    tableView->setBounceable(false);
    tableView->setPosition(Vec2((BACKGROUND_SIZE.width - TABLE_SIZE.width) / 2 - 10, BACKGROUND_SIZE.height - TABLE_SIZE.height - 110));
    tableView->setTag(TAG_SPRITE::TABLE);
    addChild(tableView, Z_ORDER::Z_TABEL);
    tableView->reloadData();
}

void MissionListTable::showScrollBar()
{
    TableView* tableView = static_cast<TableView*>(getChildByTag(TAG_SPRITE::TABLE));
    ScrollBar* scrollBar = ScrollBar::initScrollBar(TABLE_SIZE.height,
                                                    TABLE_SIZE.height,
                                                    tableView->getContainer()->getContentSize().height,
                                                    tableView->getContentOffset().y
                                                    );
    scrollBar->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    scrollBar->setPosition(tableView->getContentSize().width + 25,
                           BACKGROUND_SIZE.height / 2 + 15);
    scrollBar->setTag(TAG_SPRITE::BAR);
    addChild(scrollBar, Z_ORDER::Z_TABEL);
}

void MissionListTable::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
    ScrollBar* scrollBar = static_cast<ScrollBar*>(getChildByTag(TAG_SPRITE::BAR));
    if (scrollBar) {
        scrollBar->updateScrollPoint(view->getContentOffset().y);
    }
}

Size MissionListTable::cellSizeForTable(TableView* table)
{
    return CELL_SIZE;
}

TableViewCell* MissionListTable::tableCellAtIndex(TableView* table, ssize_t idx)
{
    TableViewCell* cell = table->dequeueCell();
    cell = new TableViewCell();
    cell->autorelease();
    auto missionView = Mission::createMissionView(_missions.at(idx), (int)idx, CC_CALLBACK_2(MissionListTable::onTapMission, this));
    missionView->setAnchorPoint(Vec2::ZERO);
    missionView->setPosition(Vec2(0, 15));
    cell->addChild(missionView);
    return cell;
}

void MissionListTable::onTapMission(Ref* sender, ui::Widget::TouchEventType type)
{
    auto table = static_cast<TableView*>(getChildByTag(TAG_SPRITE::TABLE));
    auto cell = static_cast<TableView*>(getChildByTag(TAG_SPRITE::TABLE))->cellAtIndex(((Node*)sender)->getTag());
    if (table && cell) {
        float touchPoint = cell->getPositionY() + table->getContentOffset().y;
        if (type == ui::Widget::TouchEventType::BEGAN) {
            buttonMoveFlag = false;
            _buttonMovePositionY = touchPoint;
            return;
        }
        if (type == ui::Widget::TouchEventType::MOVED) {
            if (_buttonMovePositionY + BUTTON_MOVE_DEF < touchPoint || _buttonMovePositionY - BUTTON_MOVE_DEF > touchPoint) {
                buttonMoveFlag = true;
            }
            return;
        }
        if (type == ui::Widget::TouchEventType::ENDED) {
            if (touchPoint < TOUCH_AREA_BUTTOM || touchPoint >  TOUCH_AREA_TOP) {
                buttonMoveFlag = true;
            }
            if (_buttonMovePositionY + BUTTON_MOVE_DEF < touchPoint || _buttonMovePositionY - BUTTON_MOVE_DEF > touchPoint) {
                buttonMoveFlag = true;
            }
            if (buttonMoveFlag == false) {
                _onTapDetailsCallback(_missions.at(((Node*)sender)->getTag()));
            }
            buttonMoveFlag = false;
            return;
        }
    }
}

ssize_t MissionListTable::numberOfCellsInTableView(TableView* table)
{
    return _missions.size();
}
