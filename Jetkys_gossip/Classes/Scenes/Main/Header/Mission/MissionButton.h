#ifndef __syanago__MissionButton__
#define __syanago__MissionButton__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class MissionButton : public ui::Button
{
public:
    typedef std::function<void()> OnResponceCallback;
    MissionButton();
    static MissionButton* create(const OnResponceCallback& callBak);
    void initialize(const OnResponceCallback& callBak);
    void onTapMissionButton(Ref *sender, Widget::TouchEventType type);
private:
    enum TAG_SPRITE{
        BUTTON = 0,
    };
    OnResponceCallback _missionDialogCallBack;
    
    void showButtonLabel();
};

#endif
