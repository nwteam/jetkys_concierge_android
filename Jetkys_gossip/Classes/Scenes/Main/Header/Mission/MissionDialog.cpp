#include "MissionDialog.h"
#include "SoundHelper.h"
#include "FontDefines.h"
#include "MissionListTable.h"
#include "MissionDetails.h"
#include "PlayerController.h"

MissionDialog::MissionDialog():
    progress(nullptr)
    , request(nullptr)
{
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
}
MissionDialog::~MissionDialog()
{
    delete progress;
    delete request;
    progress = nullptr;
    request = nullptr;
}

bool MissionDialog::init()
{
    if (!Layer::init()) {
        return false;
    }
    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    auto black = LayerColor::create(Color4B(Color3B::BLACK));
    black->setOpacity(180);
    addChild(black);

    auto backGround = Sprite::create("mission_base.png");
    Size winSize = Director::getInstance()->getWinSize();
    backGround->setPosition(Point(winSize.width / 2, winSize.height / 2));
    backGround->setTag(TAG_SPRITE::BACKGROUND);
    addChild(backGround);

    showMissionSprite("mission_list_label.png");
    showButton(CC_CALLBACK_2(MissionDialog::onTouchCloseButton, this), "閉じる");

    getMissionList();
    return true;
}

void MissionDialog::showMissionSprite(const std::string filePath)
{
    auto sprite = Sprite::create(filePath);
    sprite->setPosition(Vec2(sprite->getContentSize().width / 2 - 15,
                             getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height - sprite->getContentSize().height / 2 + 10));
    sprite->setTag(TAG_SPRITE::MISSION_LABEL);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(sprite, Z_ORDER::Z_LABEL);
}

void MissionDialog::showButton(const cocos2d::ui::Layout::ccWidgetTouchCallback& callback, const std::string labelString)
{
    auto closeButton = ui::Button::create("gacha_dialog_button.png");
    closeButton->addTouchEventListener(callback);
    closeButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    closeButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2, 40));
    closeButton->setTag(TAG_SPRITE::BUTTON);
    auto closeLabel = Label::createWithTTF(labelString, FONT_NAME_2, 28);
    closeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    closeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    closeLabel->setPosition(Vec2(closeButton->getContentSize() / 2) - Vec2(0, 14));
    closeButton->addChild(closeLabel);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(closeButton, Z_ORDER::Z_BUTTON);
}

void MissionDialog::onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        getEventDispatcher()->removeEventListener(_listener);
        removeFromParent();
    }
}

void MissionDialog::getMissionList()
{
    progress->onStart();
    request->getMissionList(CC_CALLBACK_1(MissionDialog::onResponseMissionList, this));
}

void MissionDialog::onResponseMissionList(Json* response)
{
    PLAYERCONTROLLER->setUncompleteMissionCourses(Json_getItem(Json_getItem(response, "get_mission_list"), "data"));
    Json* jsonMissionList = Json_getItem(Json_getItem(Json_getItem(response, "get_mission_list"), "data"), "mission_list");
    if (jsonMissionList->size > 0) {
        std::vector<std::shared_ptr<MissionList> >missions;
        for (Json* child = jsonMissionList->child; child; child = child->next) {
            std::shared_ptr<MissionList>model(new MissionList(child));
            missions.push_back(model);
        }
        std::sort(missions.begin(), missions.end(), [](const std::shared_ptr<MissionList>& left, const std::shared_ptr<MissionList>& right) {
            return (left->getStatus() == right->getStatus()) ? (left->getMissionId() < right->getMissionId()) : (left->getStatus() < right->getStatus());
        });
        showMissionList(missions);
    } else {
        showNonMissionListLabel();
    }
}

void MissionDialog::showMissionList(std::vector<std::shared_ptr<MissionList> >missions)
{
    auto content = MissionListTable::create(missions, CC_CALLBACK_1(MissionDialog::onDetailsOpenCallBack, this));
    content->setTag(TAG_SPRITE::VIEW_CONTENT);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(content, Z_ORDER::Z_CONTENT);
}

void MissionDialog::showNonMissionListLabel()
{
    auto label = Label::createWithTTF("現在ミッションはありません", FONT_NAME_2, 30);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize() / 2));
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(label);
}

void MissionDialog::onDetailsOpenCallBack(std::shared_ptr<MissionList>missionDetail)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    changeMissionView(CC_CALLBACK_2(MissionDialog::onTouchBackButton, this), "戻る", "mission_details_label.png");
    auto content = MissionDetails::create(missionDetail);
    content->setTag(TAG_SPRITE::VIEW_CONTENT);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(content, Z_ORDER::Z_CONTENT);
}

void MissionDialog::onTouchBackButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        changeMissionView(CC_CALLBACK_2(MissionDialog::onTouchCloseButton, this), "閉じる", "mission_list_label.png");
        getMissionList();
    }
}

void MissionDialog::changeMissionView(const cocos2d::ui::Layout::ccWidgetTouchCallback& callback, const std::string buttonString, const std::string LabelPath)
{
    if (getChildByTag(TAG_SPRITE::BACKGROUND)->getChildByTag(TAG_SPRITE::BUTTON) != NULL) {
        getChildByTag(TAG_SPRITE::BACKGROUND)->getChildByTag(TAG_SPRITE::BUTTON)->removeFromParent();
    }
    showButton(callback, buttonString);

    if (getChildByTag(TAG_SPRITE::BACKGROUND)->getChildByTag(TAG_SPRITE::MISSION_LABEL) != NULL) {
        getChildByTag(TAG_SPRITE::BACKGROUND)->getChildByTag(TAG_SPRITE::MISSION_LABEL)->removeFromParent();
    }
    showMissionSprite(LabelPath);

    if (getChildByTag(TAG_SPRITE::BACKGROUND)->getChildByTag(TAG_SPRITE::VIEW_CONTENT) != NULL) {
        getChildByTag(TAG_SPRITE::BACKGROUND)->getChildByTag(TAG_SPRITE::VIEW_CONTENT)->removeFromParent();
    }
}
