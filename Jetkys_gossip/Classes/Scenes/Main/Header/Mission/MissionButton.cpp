#include "MissionButton.h"
#include "SoundHelper.h"

MissionButton::MissionButton():
    _missionDialogCallBack(nullptr)
{}

MissionButton* MissionButton::create(const OnResponceCallback& callBak)
{
    MissionButton* btn = new (std::nothrow) MissionButton();
    if (btn && btn->init("mission_button_base.png")) {
        btn->initialize(callBak);
        btn->setZoomScale(0);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void MissionButton::initialize(const OnResponceCallback& callBak)
{
    _missionDialogCallBack = callBak;
    addTouchEventListener(CC_CALLBACK_2(MissionButton::onTapMissionButton, this));

    showButtonLabel();
}

void MissionButton::onTapMissionButton(Ref* sender, Widget::TouchEventType type)
{
    if (type == TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _missionDialogCallBack();
    }
}

void MissionButton::showButtonLabel()
{
    auto sprite = Sprite::create("mission_button_label.png");
    sprite->setPosition(Vec2(106, 40));
    addChild(sprite);
}