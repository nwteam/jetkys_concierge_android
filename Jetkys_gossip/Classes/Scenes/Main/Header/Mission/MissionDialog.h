#ifndef __syanago__MissionDialog__
#define __syanago__MissionDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"
#include "MissionList.h"

USING_NS_CC;
using namespace SyanagoAPI;

class MissionDialog : public Layer, public create_func<MissionDialog>
{
public:
    using create_func::create;
    bool init();
    
    MissionDialog();
    ~MissionDialog();
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        BUTTON,
        VIEW_CONTENT,
        MISSION_LABEL,
    };
    enum Z_ORDER{
        Z_BUTTON = 0,
        Z_CONTENT,
        Z_LABEL,
    };
    
    EventListenerTouchOneByOne* _listener;
    DefaultProgress* progress;
    RequestAPI* request;
    
    void showMissionSprite(const std::string filePath);
    void showButton(const cocos2d::ui::Layout::ccWidgetTouchCallback& callback, const std::string labelString);
    void showNonMissionListLabel();
    
    void getMissionList();
    void onResponseMissionList(Json* response);
    void showMissionList(std::vector<std::shared_ptr<MissionList>> missions);
    void onDetailsOpenCallBack(std::shared_ptr<MissionList> missionDetails);
    
    void changeMissionView(const cocos2d::ui::Layout::ccWidgetTouchCallback& callback, const std::string buttonString, const std::string LabelPath);
    
    void onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type);
    void onTouchBackButton(Ref* sender, ui::Widget::TouchEventType type);
};

#endif
