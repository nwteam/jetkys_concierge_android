#include "MissionList.h"
#include "itemModel.h"

MissionList::MissionList(Json* data):
    rewardTicket(nullptr)
{
    missionId = atoi(Json_getString(data, "id", ""));
    name = Json_getString(data, "name", "");
    content = Json_getString(data, "content", "");
    condition = Json_getString(data, "condition", "");
    startDatetime = Json_getString(data, "start_datetime", "");
    endDatetime = Json_getString(data, "end_datetime", "");
    clearCount = atoi(Json_getString(data, "clear_count", ""));
    clearCountForComplete = atoi(Json_getString(data, "clear_count_for_complete", ""));
    status = atoi(Json_getString(data, "status", ""));
    masterCharacterId = atoi(Json_getString(data, "character_id", ""));
    masterPartsId = atoi(Json_getString(data, "parts_id", ""));
    freeToken = atoi(Json_getString(data, "free_token", ""));
    freeCoin = atoi(Json_getString(data, "free_coin", ""));
    friendPoint = atoi(Json_getString(data, "friend_point", ""));
    Json* jsonRewardItemIds = Json_getItem(data, "reward_item_ids");
    if (jsonRewardItemIds->size > 0) {
        for (Json* child = jsonRewardItemIds->child; child; child = child->next) {
            const int itemId = atoi(child->valueString);
            int vectorId = getSameItemId(itemId);
            if (vectorId > -1) {
                rewardItemIds.at(vectorId)->itemCountUp();
            } else {
                std::shared_ptr<RewardItemId>model(new RewardItemId(itemId));
                rewardItemIds.push_back(model);
            }
        }
    }
    Json* ticket = Json_getItem(data, "reward_ticket");
    if (ticket->type != Json_String) {
        rewardTicket = std::shared_ptr<RewardTicket>(new RewardTicket(ticket));
    }
}

int MissionList::getSameItemId(const int itemId)
{
    for (int i = 0; i < rewardItemIds.size(); i++) {
        if (rewardItemIds.at(i)->getItemId() == itemId) {
            return i;
        }
    }
    return -1;
}

bool MissionList::isGetReward()
{
    if (rewardItemIds.size() > 0 || masterCharacterId > 0 || masterPartsId > 0 || freeCoin > 0 || freeToken > 0 || friendPoint > 0 || rewardTicket != nullptr) {
        return true;
    }
    return false;
}

MissionList::RewardItemId::RewardItemId(const int _itemId)
{
    itemId = _itemId;
    std::shared_ptr<ItemModel>itemModel(ItemModel::find(_itemId));
    if (itemModel != nullptr) {
        itemName = itemModel->getName();
    } else {
        itemName = "？？？？";
    }
    itemCount = 1;
}

void MissionList::RewardItemId::itemCountUp()
{
    itemCount++;
}

MissionList::RewardTicket::RewardTicket(Json* ticketJson)
{
    name = Json_getString(ticketJson, "name", "");
    count = atoi(Json_getString(ticketJson, "count", ""));
}

