#include "UncompletedMissionCourse.h"
#include "AreaModel.h"

UncompletedMissionCourse::UncompletedMissionCourse(Json* data)
{
    placeOfMissions.clear();
    Json* uncompletedMissionCourse = Json_getItem(data, "uncompleted_mission_course");
    if (!uncompletedMissionCourse) {
        return;
    }

    if (uncompletedMissionCourse->size > 0) {
        for (Json* child = uncompletedMissionCourse->child; child; child = child->next) {
            std::shared_ptr<PlaceOfMission>model(new UncompletedMissionCourse::PlaceOfMission(child));
            placeOfMissions.push_back(model);
        }
    }
}

UncompletedMissionCourse::PlaceOfMission::PlaceOfMission(Json* child)
{
    mapId = atoi(Json_getString(child, "map_id", ""));
    areaId = atoi(Json_getString(child, "area_id", ""));
    courseId = atoi(Json_getString(child, "course_id", ""));
    isEvent = false;
    std::shared_ptr<AreaModel>areaModel(AreaModel::find(areaId));
    if (areaModel->getAreaType() == 2) {
        isEvent = true;
    }
}

bool UncompletedMissionCourse::isPlaceOfMissionWithCourseId(const int courseId)
{
    if (placeOfMissions.size() < 1) {
        return false;
    }
    for (int i = 0; i < placeOfMissions.size(); i++) {
        if (placeOfMissions.at(i)->getIsEvent() == true) {
            continue;
        }
        if (placeOfMissions.at(i)->getCourseId() == courseId) {
            return true;
        }
    }
    return false;
}

bool UncompletedMissionCourse::isPlaceOfMissionWithAreaId(const int areaId)
{
    if (placeOfMissions.size() < 1) {
        return false;
    }
    for (int i = 0; i < placeOfMissions.size(); i++) {
        if (placeOfMissions.at(i)->getAreaId() == areaId) {
            return true;
        }
    }
    return false;
}

bool UncompletedMissionCourse::isPlaceOfMissionWithMapId(const int mapId)
{
    if (placeOfMissions.size() < 1) {
        return false;
    }
    for (int i = 0; i < placeOfMissions.size(); i++) {
        if (placeOfMissions.at(i)->getMapId() == mapId) {
            return true;
        }
    }
    return false;
}