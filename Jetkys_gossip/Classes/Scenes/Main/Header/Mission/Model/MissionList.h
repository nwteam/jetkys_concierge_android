#ifndef __syanago__MissionList__
#define __syanago__MissionList__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"


USING_NS_CC;

class MissionList
{
public:
    
    MissionList(Json* data);
    
    class RewardItemId
    {
    public:
        RewardItemId(const int _itemId);
        void itemCountUp();
        
        CC_SYNTHESIZE_READONLY(int, itemId, ItemId);
        CC_SYNTHESIZE_READONLY(std::string, itemName, ItemName);
        CC_SYNTHESIZE_READONLY(int, itemCount, ItemCount);
    };
    
    class RewardTicket
    {
    public:
        RewardTicket(Json* ticketJson);
        
        CC_SYNTHESIZE_READONLY(std::string, name, Name);
        CC_SYNTHESIZE_READONLY(int, count, Count);
    };
    
    
    CC_SYNTHESIZE_READONLY(int, missionId, MissionId);
    CC_SYNTHESIZE_READONLY(std::string, name, Name);
    CC_SYNTHESIZE_READONLY(std::string, content, Content);
    CC_SYNTHESIZE_READONLY(std::string, condition, Condition);
    CC_SYNTHESIZE_READONLY(std::string, startDatetime, StartDatetime);
    CC_SYNTHESIZE_READONLY(std::string, endDatetime, EndDatetime);
    CC_SYNTHESIZE_READONLY(int, clearCount, ClearCount);
    CC_SYNTHESIZE_READONLY(int, clearCountForComplete, ClearCountForComplete);
    CC_SYNTHESIZE_READONLY(int, status, Status);
    CC_SYNTHESIZE_READONLY(int, masterCharacterId, MasterCharacterId);
    CC_SYNTHESIZE_READONLY(int, masterPartsId, MasterPartsId);
    CC_SYNTHESIZE_READONLY(int, freeToken, FreeToken);
    CC_SYNTHESIZE_READONLY(int, freeCoin, FreeCoin);
    CC_SYNTHESIZE_READONLY(int, friendPoint, FriendPoint);
    CC_SYNTHESIZE_READONLY(std::vector<std::shared_ptr<RewardItemId>>, rewardItemIds, RewardItemIds);
    CC_SYNTHESIZE_READONLY(std::shared_ptr<RewardTicket>, rewardTicket, RewardTicket);
    
    bool isGetReward();
private:
    int getSameItemId(const int itemId);
};
#endif