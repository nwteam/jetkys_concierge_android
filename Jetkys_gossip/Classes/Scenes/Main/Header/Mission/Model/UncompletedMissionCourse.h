#ifndef __syanago__UncompletedMissionCourse__
#define __syanago__UncompletedMissionCourse__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"

USING_NS_CC;

class UncompletedMissionCourse
{
public:
    UncompletedMissionCourse(Json* data);
    
    class PlaceOfMission
    {
    public:
        PlaceOfMission(Json* child);
        
        CC_SYNTHESIZE_READONLY(int, mapId, MapId);
        CC_SYNTHESIZE_READONLY(int, areaId, AreaId);
        CC_SYNTHESIZE_READONLY(int, courseId, CourseId);
        CC_SYNTHESIZE_READONLY(bool, isEvent, IsEvent);
    };
    CC_SYNTHESIZE_READONLY(std::vector<std::shared_ptr<PlaceOfMission>>, placeOfMissions, PlaceOfMissions);
    
    bool isPlaceOfMissionWithCourseId(const int courseId);
    bool isPlaceOfMissionWithAreaId(const int areaId);
    bool isPlaceOfMissionWithMapId(const int mapId);
};

#endif
