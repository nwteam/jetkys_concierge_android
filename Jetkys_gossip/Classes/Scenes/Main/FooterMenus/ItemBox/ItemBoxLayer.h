#ifndef __syanago__ItemBoxLayer__
#define __syanago__ItemBoxLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "create_func.h"
#include "show_head_line.h"
#include "ItemListModel.h"
#include "ItemKindChangeView.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"
#include "ItemBoxGahcaCallBackModel.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace SyanagoAPI;

class ItemBoxLayer : public Layer, public create_func<ItemBoxLayer>, public show_head_line
{
public:
    ItemBoxLayer();
    ~ItemBoxLayer();
    typedef std::function<void()> OnHeaderUpdateCallback;
    typedef std::function<void(const std::shared_ptr<ItemListModel::Item>& itemModel)> OnShowItemBoxDialogCallback;
    bool init(const backButtonCallback& backCallback,
              const OnHeaderUpdateCallback& updateCallback,
              const OnShowItemBoxDialogCallback& showItemBoxDialog,
              const std::shared_ptr<ItemBoxGahcaCallbackModel>& callbackModel);
    using create_func::create;
    
    void useItemCallBack(const std::shared_ptr<ItemListModel::Item>& itemModel);
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        ITEM_LIST,
        ITEM_KIND_CHANGE_BUTTON,
        GACHA_RESULT,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_ITEM_LIST = 12,
        Z_ITEM_KIND_CHANGE_BUTTON,
        Z_GACHA_RESULT,
    };
    
    DefaultProgress* _progress;
    RequestAPI* _request;
    std::shared_ptr<ItemListModel> _itemListModel;
    OnHeaderUpdateCallback _headerUpdateCallback;
    OnShowItemBoxDialogCallback _showItemBoxDialog;
    std::shared_ptr<ItemBoxGahcaCallbackModel> _gachaCallbackModel;
    
    void showBackground();
    
    void showViewItemButton(const std::shared_ptr<ItemListModel>& model);
    void onTapViewContentChangeButton(const ItemKindChangeView::TAP_DIRECTION direction);
    
    void showNonItemView();
    
    void getItemList();
    void onResponseConsumptionItem(Json* response);
    void showItemList(const std::shared_ptr<ItemListModel>& model);
    void onTapItemSelect(const std::shared_ptr<ItemListModel::Item>& itemModel);
    void responseUseTicket(Json* response, const std::shared_ptr<ItemListModel::Item>& itemModel);
    void refreshList();
    EventListenerTouchOneByOne* _listener;
};

#endif