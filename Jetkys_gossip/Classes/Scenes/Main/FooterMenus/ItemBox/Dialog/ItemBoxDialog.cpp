#include "ItemBoxDialog.h"
#include "FontDefines.h"
#include "SoundHelper.h"
#include "TelopScrollView.h"

ItemBoxDialog::ItemBoxDialog():
    _onUseItemCallBack(nullptr), _itemModel(nullptr)
{}

ItemBoxDialog::~ItemBoxDialog()
{}

bool ItemBoxDialog::init(const std::shared_ptr<ItemListModel::Item>& model,
                         const OnUseItemCallback& useCallBack)
{
    if (!Layer::create()) {
        return false;
    }

    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    showBackground();
    showName(model->getName());
    showHoldCount(model->getCount());
    showDescription(model->getDescription());

    if (model->getItemType() == ItemListModel::ITEM_KIND::TICKET) {
        showUseItemButton();
    }
    showCloseButton(model->getItemType());

    _itemModel = model;
    _onUseItemCallBack = useCallBack;

    return true;
}

void ItemBoxDialog::showBackground()
{
    auto black = LayerColor::create(Color4B(Color3B::BLACK));
    black->setOpacity(180);
    addChild(black);
    auto backGround = Sprite::create("dialog_base.png");
    Size winSize = Director::getInstance()->getWinSize();
    backGround->setPosition(Point(winSize.width / 2, winSize.height / 2));
    backGround->setTag(TAG_SPRITE::BACKGROUND);
    addChild(backGround);
}

void ItemBoxDialog::showName(const std::string name)
{
    TelopScrollView* telopScrollView = TelopScrollView::initScrollTelop(name,
                                                                        32,
                                                                        getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width - 200,
                                                                        32,
                                                                        COLOR_YELLOW);
    telopScrollView->setTag(TAG_SPRITE::NAME);
    telopScrollView->setPosition(Vec2(55, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height - 87));
    telopScrollView->setFontShadow(Color3B::BLACK, 2);
    telopScrollView->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(telopScrollView, Z_ORDER::Z_NAME);
}

void ItemBoxDialog::showHoldCount(const int itemCount)
{
    auto label = Label::createWithTTF(StringUtils::format("%d", itemCount), FONT_NAME_2, 32);
    label->setTag(TAG_SPRITE::NAME);
    label->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width - 55,
                            getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height - 55));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setColor(COLOR_YELLOW);
    label->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(label, Z_ORDER::Z_NAME);
}

void ItemBoxDialog::showDescription(const std::string description)
{
    auto label = Label::createWithTTF(description, FONT_NAME_2, 25);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    float descriptionWidth = getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width - 70;
    if (label->getContentSize().width > descriptionWidth) {
        label->setDimensions(descriptionWidth, 0);
    }
    label->setTag(TAG_SPRITE::DESCRIPTION);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize() / 2) + Vec2(0, label->getContentSize().height / 2));
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(label, Z_ORDER::Z_DESCRIPTION);
}

void ItemBoxDialog::showUseItemButton()
{
    auto useButton = ui::Button::create("gacha_dialog_button.png");
    useButton->addTouchEventListener(CC_CALLBACK_2(ItemBoxDialog::onTouchUseItemButton, this));
    useButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    useButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 - 15, 25));
    useButton->setTag(TAG_SPRITE::USE_BUTTON);
    auto useLabel = Label::createWithTTF("使用する", FONT_NAME_2, 28);
    useLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    useLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    useLabel->setPosition(Vec2(useButton->getContentSize() / 2) - Vec2(0, 14));
    useButton->addChild(useLabel);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(useButton, Z_ORDER::Z_USE_BUTTON);
}

void ItemBoxDialog::onTouchUseItemButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        getEventDispatcher()->removeEventListener(_listener);
        _onUseItemCallBack(_itemModel);
        removeFromParent();
    }
}

void ItemBoxDialog::showCloseButton(ItemListModel::ITEM_KIND type)
{
    auto closeButton = ui::Button::create("gacha_dialog_button.png");
    closeButton->addTouchEventListener(CC_CALLBACK_2(ItemBoxDialog::onTouchCloseButton, this));
    if (type == ItemListModel::ITEM_KIND::TICKET) {
        closeButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        closeButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 + 15, 25));
    } else {
        closeButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
        closeButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2, 25));
    }
    closeButton->setTag(TAG_SPRITE::CLOSE_BUTTON);
    auto closeLabel = Label::createWithTTF("閉じる", FONT_NAME_2, 28);
    closeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    closeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    closeLabel->setPosition(Vec2(closeButton->getContentSize() / 2) - Vec2(0, 14));
    closeButton->addChild(closeLabel);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(closeButton, Z_ORDER::Z_CLOSE_BUTTON);
}

void ItemBoxDialog::onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        getEventDispatcher()->removeEventListener(_listener);
        removeFromParent();
    }
}