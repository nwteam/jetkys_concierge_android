#ifndef __syanago__ItemBoxDialog__
#define __syanago__ItemBoxDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"
#include "ItemListModel.h"

USING_NS_CC;

class ItemBoxDialog : public Layer, public create_func<ItemBoxDialog>
{
public:
    ItemBoxDialog();
    ~ItemBoxDialog();
    typedef std::function<void(const std::shared_ptr<ItemListModel::Item>& model)> OnUseItemCallback;

    using create_func::create;
    bool init(const std::shared_ptr<ItemListModel::Item>& model,
              const OnUseItemCallback& useCallBack);

private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        USE_BUTTON,
        CLOSE_BUTTON,
        NAME,
        DESCRIPTION,
    };
    enum Z_ORDER{
        Z_USE_BUTTON = 0,
        Z_CLOSE_BUTTON,
        Z_NAME,
        Z_DESCRIPTION,
    };
    EventListenerTouchOneByOne* _listener;
    std::shared_ptr<ItemListModel::Item> _itemModel;
    OnUseItemCallback _onUseItemCallBack;
    
    void showBackground();
    void showName(const std::string name);
    void showHoldCount(const int itemCount);
    void showDescription(const std::string description);
    
    void showUseItemButton();
    void onTouchUseItemButton(Ref* sender, ui::Widget::TouchEventType type);
    void showCloseButton(ItemListModel::ITEM_KIND type);
    void onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type);
};

#endif
