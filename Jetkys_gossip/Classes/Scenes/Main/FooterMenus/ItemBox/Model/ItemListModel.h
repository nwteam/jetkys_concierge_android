#ifndef __syanago__ItemListModel__
#define __syanago__ItemListModel__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"

USING_NS_CC;

namespace ICON_PATH{
    namespace TICKET{
        static const std::string UNKNOWN = "";
        static const std::string FUEL = "itembox_ticket_fuel.png";
        static const std::string GACHA = "itembox_ticket_gacha.png";
        static const std::string EXP = "itembox_ticket_exp.png";
        static const std::string COIN = "itembox_ticket_coin.png";
        static const std::string TOKEN = "itembox_ticket_token.png";
        static const std::string DEAR_VALUE = "itembox_ticket_dearvalue.png";
    };
    
};

class ItemListModel
{
public:
    enum ITEM_KIND{
        UNKNOWN = 0,
        TICKET,
        WORKS_SHOP_ITEM,
    };
    ItemListModel();
    ~ItemListModel();
    
    void setJsonItems(Json* response);
    
    CC_SYNTHESIZE(ITEM_KIND, _viewItemKind, ViewItemKind);
    
    void changeNextItem();
    void changePrevItem();
    
    const std::string getViewItemKIndName();
    
    class Item
    {
    public:
        Item(Json* data, ITEM_KIND itemType);
        ~Item();
        enum TICKET_TYPE{
            UNKNOWN_TICKET = 0,
            FUEL,
            GACHA,
        };
        enum WORKS_SHOP_TYPE{
            UNKNOWN_WORK_SHOP = 0,
            ENGINE,
        };
        
        CC_SYNTHESIZE_READONLY(int, _id, Id);
        
        CC_SYNTHESIZE_READONLY(ITEM_KIND, _itemType, ItemType);
        CC_SYNTHESIZE_READONLY(TICKET_TYPE, _ticketType, TicketType);
        CC_SYNTHESIZE_READONLY(WORKS_SHOP_TYPE, _worksShopType, WorksShopType);
        
        CC_SYNTHESIZE_READONLY(int, _count, Count);
        CC_SYNTHESIZE_READONLY(std::string, _name, Name);
        CC_SYNTHESIZE_READONLY(std::string, _description, Description);
        CC_SYNTHESIZE_READONLY(std::string, _iconFilePath, IconFilePath);
    };
    
    const std::vector<std::shared_ptr<Item>> getViewItems();
private:
    std::vector<std::shared_ptr<Item>> _tickets;
    std::vector<std::shared_ptr<Item>> _workshopItems;
};

#endif
