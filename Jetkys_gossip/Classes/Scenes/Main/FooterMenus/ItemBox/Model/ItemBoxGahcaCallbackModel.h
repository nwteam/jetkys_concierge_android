#ifndef __syanago__ItemBoxGahcaCallbackModel__
#define __syanago__ItemBoxGahcaCallbackModel__

#include <cocos2d.h>

USING_NS_CC;

class ItemBoxGahcaCallbackModel
{
public:
    typedef std::function<void()> OnMenuViewCallback;
    typedef std::function<void()> OnMenuHiddenCallback;
    typedef std::function<void(const int playerCharacterId)> OnCharacterGachaDetailseViewCallback;
    typedef std::function<void(const int playerCharacterId)> OnCharacterDetailseViewCallback;
    typedef std::function<void(const int playerPartsId, const int masterPartsId)> OnPartsGachaDetailseViewCallback;
    typedef std::function<void(const int playerPartsId, const int masterPartsId)> OnPartsDetailseViewCallback;
    
    ItemBoxGahcaCallbackModel(const OnMenuViewCallback& onMenuViewCallback,
                              const OnMenuHiddenCallback& onMenuHiddenCallback,
                              const OnCharacterGachaDetailseViewCallback& onCharacterGachaDetailseViewCallback,
                              const OnCharacterDetailseViewCallback& onCharacterDetailseViewCallback,
                              const OnPartsGachaDetailseViewCallback& onPartsGachaDetailseViewCallback,
                              const OnPartsDetailseViewCallback& onPartsDetailseViewCallback);
    
    CC_SYNTHESIZE_READONLY(OnMenuViewCallback, _onMenuViewCallback, MenuViewCallback);
    CC_SYNTHESIZE_READONLY(OnMenuHiddenCallback, _onMenuHiddenCallback, MenuHiddenCallback);
    CC_SYNTHESIZE_READONLY(OnCharacterGachaDetailseViewCallback, _onCharacterGachaDetailseViewCallback, CharacterGachaDetailseViewCallback);
    CC_SYNTHESIZE_READONLY(OnCharacterDetailseViewCallback, _onCharacterDetailseViewCallback, CharacterDetailseViewCallback);
    CC_SYNTHESIZE_READONLY(OnPartsGachaDetailseViewCallback, _onPartsGachaDetailseViewCallback, PartsGachaDetailseViewCallback)
    CC_SYNTHESIZE_READONLY(OnPartsDetailseViewCallback, _onPartsDetailseViewCallback, PartsDetailseViewCallback);
};

#endif
