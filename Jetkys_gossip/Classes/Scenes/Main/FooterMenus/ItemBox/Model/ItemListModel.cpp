#include "ItemListModel.h"


ItemListModel::ItemListModel():
    _viewItemKind(ITEM_KIND::UNKNOWN)
{}

ItemListModel::~ItemListModel()
{}

void ItemListModel::setJsonItems(Json* response)
{
    _tickets.clear();
    _workshopItems.clear();

    auto data = Json_getItem(Json_getItem(response, "get_consumption_item"), "data");
    Json* ticketData = Json_getItem(data, "ticket");
    if (ticketData->size > 0) {
        for (Json* child = ticketData->child; child; child = child->next) {
            std::shared_ptr<ItemListModel::Item>model(new ItemListModel::Item(child, ITEM_KIND::TICKET));
            _tickets.push_back(model);
        }
    }

    // TODO 工房アイテムもチケット同様に処理

    if (_tickets.size() > 0) {
        _viewItemKind = ITEM_KIND::TICKET;
    } else if (_workshopItems.size() > 0) {
        _viewItemKind = ITEM_KIND::WORKS_SHOP_ITEM;
    }
}

void ItemListModel::changeNextItem()
{
    if (_viewItemKind == ITEM_KIND::TICKET && _workshopItems.size() > 0) {
        _viewItemKind = ITEM_KIND::WORKS_SHOP_ITEM;
    } else if (_viewItemKind == ITEM_KIND::WORKS_SHOP_ITEM && _tickets.size() > 0) {
        _viewItemKind = ITEM_KIND::TICKET;
    }
}

void ItemListModel::changePrevItem()
{
    if (_viewItemKind == ITEM_KIND::TICKET && _workshopItems.size() > 0) {
        _viewItemKind = ITEM_KIND::WORKS_SHOP_ITEM;
    } else if (_viewItemKind == ITEM_KIND::WORKS_SHOP_ITEM && _tickets.size() > 0) {
        _viewItemKind = ITEM_KIND::TICKET;
    }
}

const std::string ItemListModel::getViewItemKIndName()
{
    if (_viewItemKind == ITEM_KIND::TICKET) {
        return "各種チケット";
    } else if (_viewItemKind == ITEM_KIND::WORKS_SHOP_ITEM) {
        return "工房アイテム";
    }
    return "";
}

const std::vector<std::shared_ptr<ItemListModel::Item> >ItemListModel::getViewItems()
{
    std::vector<std::shared_ptr<ItemListModel::Item> >result;
    if (_viewItemKind == ITEM_KIND::TICKET) {
        result = _tickets;
    } else if (_viewItemKind == ITEM_KIND::WORKS_SHOP_ITEM) {
        result = _workshopItems;
    }
    ;
    return result;
}

ItemListModel::Item::Item(Json* data, ITEM_KIND itemType):
    _id(0)
    , _itemType(ITEM_KIND::UNKNOWN)
    , _ticketType(TICKET_TYPE::UNKNOWN_TICKET)
    , _worksShopType(WORKS_SHOP_TYPE::UNKNOWN_WORK_SHOP)
    , _count(0)
    , _name("")
    , _description("")
    , _iconFilePath("")
{
    _itemType = itemType;
    if (_itemType == ITEM_KIND::TICKET) {
        _id = atoi(Json_getString(data, "player_ticket_id", "-1"));
        _count = atoi(Json_getString(data, "count", "-1"));
        _name = Json_getString(data, "name", "");
        _description = Json_getString(data, "description", "");

        const std::string kind = Json_getString(data, "kind", "none");
        if (kind == "fuel") {
            _ticketType = TICKET_TYPE::FUEL;
            _iconFilePath = ICON_PATH::TICKET::FUEL;
        } else if (kind == "gacha") {
            _ticketType = TICKET_TYPE::GACHA;
            _iconFilePath = ICON_PATH::TICKET::GACHA;
        }
    }
}

ItemListModel::Item::~Item()
{}









