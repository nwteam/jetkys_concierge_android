#include "ItemBoxGahcaCallBackModel.h"

ItemBoxGahcaCallbackModel::ItemBoxGahcaCallbackModel(const OnMenuViewCallback& onMenuViewCallback,
                          const OnMenuHiddenCallback& onMenuHiddenCallback,
                          const OnCharacterGachaDetailseViewCallback& onCharacterGachaDetailseViewCallback,
                          const OnCharacterDetailseViewCallback& onCharacterDetailseViewCallback,
                          const OnPartsGachaDetailseViewCallback& onPartsGachaDetailseViewCallback,
                          const OnPartsDetailseViewCallback& onPartsDetailseViewCallback)
{
    _onMenuViewCallback = onMenuViewCallback;
    _onMenuHiddenCallback = onMenuHiddenCallback;
    _onCharacterGachaDetailseViewCallback = onCharacterGachaDetailseViewCallback;
    _onCharacterDetailseViewCallback = onCharacterDetailseViewCallback;
    _onPartsGachaDetailseViewCallback = onPartsGachaDetailseViewCallback;
    _onPartsDetailseViewCallback = onPartsDetailseViewCallback;
}