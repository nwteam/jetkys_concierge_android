#include "ItemBoxLayer.h"
#include "SoundHelper.h"
#include "ItemBoxList.h"
#include "ItemBoxDialog.h"
#include "PlayerController.h"
#include "ItemBoxGachaResultLayer.h"

ItemBoxLayer::ItemBoxLayer():
    _progress(nullptr)
    , _request(nullptr)
    , _itemListModel(nullptr)
    , _headerUpdateCallback(nullptr)
    , _showItemBoxDialog(nullptr)
    , _gachaCallbackModel(nullptr)
    , _listener(nullptr)
{
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}

ItemBoxLayer::~ItemBoxLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

bool ItemBoxLayer::init(const backButtonCallback& backCallback,
                        const OnHeaderUpdateCallback& updateCallback,
                        const OnShowItemBoxDialogCallback& showItemBoxDialog,
                        const std::shared_ptr<ItemBoxGahcaCallbackModel>& callbackModel)
{
    if (!Layer::create()) {
        return false;
    }

    std::shared_ptr<ItemListModel>model(new ItemListModel());
    _itemListModel = model;
    _gachaCallbackModel = callbackModel;
    _headerUpdateCallback = updateCallback;
    _showItemBoxDialog = showItemBoxDialog;

    showBackground();
    showHeadLine(this, "アイテムボックス", backCallback);
    getItemList();
    return true;
}

void ItemBoxLayer::showBackground()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void ItemBoxLayer::getItemList()
{
    _progress->onStart();
    _request->getConsumptionItem(CC_CALLBACK_1(ItemBoxLayer::onResponseConsumptionItem, this));
}

void ItemBoxLayer::onResponseConsumptionItem(Json* response)
{
    _itemListModel->setJsonItems(response);
    if (_itemListModel->getViewItemKind() != ItemListModel::ITEM_KIND::UNKNOWN) {
        showItemList(_itemListModel);
        showViewItemButton(_itemListModel);
    } else {
        showNonItemView();
    }
    if (_listener == nullptr) {
        _listener = EventListenerTouchOneByOne::create();
        _listener->setSwallowTouches(true);
        _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                      return true;
                                  };
        _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
        _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
        _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
        getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);
    }
}

void ItemBoxLayer::showNonItemView()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto bgground = Sprite::create("dialog_base.png");
    addChild(bgground);
    bgground->setPosition(winSize / 2);
    auto resultLabel = Label::createWithTTF("所持アイテムがありません", FONT_NAME_2, 40);
    resultLabel->setPosition(Point(winSize.width / 2, (winSize.height / 2 - 20)));
    resultLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(resultLabel);
}

void ItemBoxLayer::showItemList(const std::shared_ptr<ItemListModel>& model)
{
    ItemBoxList* itemBoxList = ItemBoxList::create(model, CC_CALLBACK_1(ItemBoxLayer::onTapItemSelect, this));
    itemBoxList->setTag(TAG_SPRITE::ITEM_LIST);
    itemBoxList->setPosition(Vec2::ZERO);
    addChild(itemBoxList, Z_ORDER::Z_ITEM_LIST);
}

void ItemBoxLayer::onTapItemSelect(const std::shared_ptr<ItemListModel::Item>& itemModel)
{
    _showItemBoxDialog(itemModel);
}

void ItemBoxLayer::useItemCallBack(const std::shared_ptr<ItemListModel::Item>& itemModel)
{
    if (itemModel->getItemType() == ItemListModel::ITEM_KIND::TICKET) {
        _progress->onStart();
        _request->useTicket(itemModel->getId(), CC_CALLBACK_1(ItemBoxLayer::responseUseTicket, this, itemModel));
    }
}

void ItemBoxLayer::responseUseTicket(Json* response, const std::shared_ptr<ItemListModel::Item>& itemModel)
{
    auto data = Json_getItem(Json_getItem(response, "use_ticket"), "data");
    if (itemModel->getTicketType() == ItemListModel::Item::TICKET_TYPE::FUEL) {
        PLAYERCONTROLLER->_player->setFuel(atoi(Json_getString(Json_getItem(data, "player"), "fuel", "-1")));
        _headerUpdateCallback();
    } else if (itemModel->getTicketType() == ItemListModel::Item::TICKET_TYPE::GACHA) {
        auto gachaTicketResult = ItemBoxGachaResultLayer::create(_gachaCallbackModel, data);
        gachaTicketResult->setTag(TAG_SPRITE::GACHA_RESULT);
        addChild(gachaTicketResult, Z_ORDER::Z_GACHA_RESULT);
    }
    refreshList();
}

void ItemBoxLayer::refreshList()
{
    removeChildByTag(TAG_SPRITE::ITEM_LIST);
    removeChildByTag(TAG_SPRITE::ITEM_KIND_CHANGE_BUTTON);
    getItemList();
}

void ItemBoxLayer::showViewItemButton(const std::shared_ptr<ItemListModel>& model)
{
    ItemKindChangeView* itemKindChangeView = ItemKindChangeView::create(model, CC_CALLBACK_1(ItemBoxLayer::onTapViewContentChangeButton, this));
    itemKindChangeView->setTag(TAG_SPRITE::ITEM_KIND_CHANGE_BUTTON);
    itemKindChangeView->setPosition(Vec2::ZERO);
    addChild(itemKindChangeView, Z_ORDER::Z_ITEM_KIND_CHANGE_BUTTON);
}

void ItemBoxLayer::onTapViewContentChangeButton(const ItemKindChangeView::TAP_DIRECTION direction)
{
    if (direction == ItemKindChangeView::TAP_DIRECTION::LEFT) {
        _itemListModel->changePrevItem();
    } else if (direction == ItemKindChangeView::TAP_DIRECTION::RIGHT) {
        _itemListModel->changeNextItem();
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    removeChildByTag(TAG_SPRITE::ITEM_LIST);
    removeChildByTag(TAG_SPRITE::ITEM_KIND_CHANGE_BUTTON);
    showViewItemButton(_itemListModel);
    showItemList(_itemListModel);
}

