#include "ItemContent.h"
#include "FontDefines.h"
#include "TelopScrollView.h"

ItemContent::ItemContent():
    _callback(nullptr), _itemModel(nullptr)
{}

ItemContent* ItemContent::create(const std::shared_ptr<ItemListModel::Item>& itemModel, const OnItemSelectCallback& callBak)
{
    ItemContent* btn = new (std::nothrow) ItemContent();
    if (btn && btn->init("itembox_list_base.png")) {
        btn->initialize(itemModel, callBak);
        btn->setZoomScale(0);
        btn->autorelease();
        btn->setSwallowTouches(false);
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void ItemContent::initialize(const std::shared_ptr<ItemListModel::Item>& itemModel, const OnItemSelectCallback& callBak)
{
    _callback = callBak;
    _itemModel = itemModel;
    addTouchEventListener(CC_CALLBACK_2(ItemContent::onTapItemBoxButton, this));

    showIcon(_itemModel);
    showName(_itemModel);
    showHoldCount(_itemModel);
}

void ItemContent::showIcon(const std::shared_ptr<ItemListModel::Item>& itemModel)
{
    Sprite* icon = Sprite::create(itemModel->getIconFilePath().c_str());
    icon->setTag(TAG_SPRITE::ICON);
    icon->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    icon->setPosition(Vec2(10, getContentSize().height / 2));
    addChild(icon, Z_ORDER::Z_ICON);
}

void ItemContent::showName(const std::shared_ptr<ItemListModel::Item>& itemModel)
{
    TelopScrollView* telopScrollView = TelopScrollView::initScrollTelop(itemModel->getName(),
                                                                        30,
                                                                        getContentSize().width - 280,
                                                                        30,
                                                                        Color3B::WHITE);
    telopScrollView->setTag(TAG_SPRITE::ITEM_NAME);
    telopScrollView->setFontShadow(Color3B::BLACK, 2);
    telopScrollView->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    auto icon = getChildByTag(TAG_SPRITE::ICON);
    telopScrollView->setPosition(icon->getPosition() + Vec2(icon->getContentSize().width + 30, -15));
    addChild(telopScrollView, Z_ITEM_NAME);
}

void ItemContent::showHoldCount(const std::shared_ptr<ItemListModel::Item>& itemModel)
{
    Label* holdCount = Label::createWithTTF(StringUtils::format("%d", itemModel->getCount()), FONT_NAME_2, 30);
    holdCount->setTag(TAG_SPRITE::ITEM_COUNT);
    holdCount->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    holdCount->setPosition(Vec2(getContentSize().width - 30, getChildByTag(TAG_SPRITE::ITEM_NAME)->getPosition().y));
    holdCount->setColor(COLOR_YELLOW);
    holdCount->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(holdCount, Z_ITEM_COUNT);
}

void ItemContent::onTapItemBoxButton(Ref* sender, Widget::TouchEventType type)
{
    _callback(sender, type, _itemModel);
}