#ifndef __syanago__ItemKindChangeView__
#define __syanago__ItemKindChangeView__

#include "cocos2d.h"
#include "create_func.h"
#include "ItemListModel.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class ItemKindChangeView : public Layer, public create_func<ItemKindChangeView>
{
public:
    ItemKindChangeView();
    ~ItemKindChangeView();
    enum TAP_DIRECTION{
        LEFT,
        RIGHT,
    };
    using create_func::create;
    typedef std::function<void(const ItemKindChangeView::TAP_DIRECTION direction)> OnChangeContentCallback;
    bool init(const std::shared_ptr<ItemListModel>& model, const OnChangeContentCallback& changeCallback);
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        ITEM_KIND_NAME,
        LEFT_BUTTON,
        RIGHT_BUTTON,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_ITEM_KIND_NAME,
        Z_LEFT_BUTTON,
        Z_RIGHT_BUTTON,
    };
    
    OnChangeContentCallback _callBack;
    std::shared_ptr<ItemListModel> _itemListModel;
    
    void showBackground();
    void showItemKindName(const std::shared_ptr<ItemListModel>& model);
    void showChangeButton(const std::shared_ptr<ItemListModel>& model);
    
    void onTapLeftButton(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    void onTapRightButton(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
};

#endif
