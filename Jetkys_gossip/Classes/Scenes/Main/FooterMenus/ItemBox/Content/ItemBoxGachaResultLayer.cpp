#include "ItemBoxGachaResultLayer.h"
#include "GachaEffectLayer.h"
#include "GachaEffectModel.h"
#include "DropEffectLayer.h"
#include "GachaResultLayer.h"

bool ItemBoxGachaResultLayer::init(const std::shared_ptr<ItemBoxGahcaCallbackModel>& callbackModel,
                                   Json* data)
{
    if (!Layer::create()) {
        return false;
    }

    std::shared_ptr<GachaResponseModel>model(new GachaResponseModel(data));
    _gachaResponseModel = model;
    _gachaCallbackModel = callbackModel;

    showGachaResult(model);

    std::shared_ptr<GachaEffectModel>effectModel(new GachaEffectModel(model->getMaxRarity(), GACHA_TYPE::PREMIUM));
    auto gachaEffectLayer = GachaEffectLayer::create(effectModel, CC_CALLBACK_0(ItemBoxGachaResultLayer::gachaEffectCallback, this));
    gachaEffectLayer->setTag(TAG_SPRITE::EFFECT);
    addChild(gachaEffectLayer, Z_ORDER::Z_EFFECT);

    auto callback = _gachaCallbackModel->getMenuHiddenCallback();
    callback();

    return true;
}

ItemBoxGachaResultLayer::ItemBoxGachaResultLayer():
    _gachaResponseModel(nullptr), _gachaCallbackModel(nullptr)
{}

ItemBoxGachaResultLayer::~ItemBoxGachaResultLayer()
{}

void ItemBoxGachaResultLayer::showGachaResult(std::shared_ptr<GachaResponseModel>& model)
{
    auto resultLayer = GachaResultLayer::create(model, CC_CALLBACK_2(ItemBoxGachaResultLayer::onCharacterDetailCallback, this), CC_CALLBACK_2(ItemBoxGachaResultLayer::onPartsDetailCallback, this));
    resultLayer->setTag(TAG_SPRITE::RESULT);
    addChild(resultLayer, Z_ORDER::Z_RESULT);
}

void ItemBoxGachaResultLayer::onCharacterDetailCallback(const int playerCharacterId, const int masterCharacterId)
{
    auto callback = _gachaCallbackModel->getCharacterDetailseViewCallback();
    callback(playerCharacterId);
}

void ItemBoxGachaResultLayer::onPartsDetailCallback(const int playerPartsId, const int masterPartsId)
{
    auto callback = _gachaCallbackModel->getPartsDetailseViewCallback();
    callback(masterPartsId, playerPartsId);
}

void ItemBoxGachaResultLayer::gachaEffectCallback()
{
    auto dropEffectLayer = DropEffectLayer::create(_gachaResponseModel->getContentType(), _gachaResponseModel->getMaxRarityContentMasterId(), false, CC_CALLBACK_0(ItemBoxGachaResultLayer::dropEffectCallBack, this));
    dropEffectLayer->setTag(TAG_SPRITE::EFFECT);
    addChild(dropEffectLayer, Z_ORDER::Z_EFFECT);
}

void ItemBoxGachaResultLayer::dropEffectCallBack()
{
    auto viewMenuCallback = _gachaCallbackModel->getMenuViewCallback();
    viewMenuCallback();
    if (_gachaResponseModel->getContentType() == DROP_CHARACTER) {
        auto callback = _gachaCallbackModel->getCharacterGachaDetailseViewCallback();
        callback(_gachaResponseModel->getMaxRarityContentPlayerId());
    } else if (_gachaResponseModel->getContentType() == DROP_PART) {
        auto callback = _gachaCallbackModel->getPartsGachaDetailseViewCallback();
        callback(_gachaResponseModel->getMaxRarityContentMasterId(), _gachaResponseModel->getMaxRarityContentPlayerId());
    }
}