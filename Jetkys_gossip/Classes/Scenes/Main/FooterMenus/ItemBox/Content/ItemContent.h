#ifndef __syanago__ItemContent__
#define __syanago__ItemContent__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "ItemListModel.h"

USING_NS_CC;

class ItemContent : public ui::Button
{
public:
    typedef std::function<void(Ref *sender, Widget::TouchEventType type, const std::shared_ptr<ItemListModel::Item>& itemModel)> OnItemSelectCallback;
    ItemContent();
    static ItemContent* create(const std::shared_ptr<ItemListModel::Item>& itemModel, const OnItemSelectCallback& callBak);
    void initialize(const std::shared_ptr<ItemListModel::Item>& itemModel, const OnItemSelectCallback& callBak);
    void onTapItemBoxButton(Ref *sender, Widget::TouchEventType type);
private:
    enum TAG_SPRITE{
        ICON = 0,
        ITEM_NAME,
        ITEM_COUNT,
    };
    enum Z_ORDER{
        Z_ICON = 0,
        Z_ITEM_NAME,
        Z_ITEM_COUNT,
    };
    OnItemSelectCallback _callback;
    std::shared_ptr<ItemListModel::Item> _itemModel;
    
    void showIcon(const std::shared_ptr<ItemListModel::Item>& itemModel);
    void showName(const std::shared_ptr<ItemListModel::Item>& itemModel);
    void showHoldCount(const std::shared_ptr<ItemListModel::Item>& itemModel);
};

#endif
