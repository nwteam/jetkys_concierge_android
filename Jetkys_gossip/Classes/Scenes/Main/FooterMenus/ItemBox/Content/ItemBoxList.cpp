#include "ItemBoxList.h"
#include "ScrollBar.h"
#include "ItemContent.h"
#include "SoundHelper.h"

ItemBoxList::ItemBoxList():
    _callBack(nullptr), buttonMoveFlag(false), _buttonMovePositionY(0)
{}

ItemBoxList::~ItemBoxList()
{
    _callBack = nullptr;
}

bool ItemBoxList::init(const std::shared_ptr<ItemListModel>& model, const OnSelectCallback& callback)
{
    if (!Layer::create()) {
        return false;
    }
    _callBack = callback;
    _itemListModel = model;
    showItemTable(_itemListModel);
    showScrollBar();
    return true;
}

void ItemBoxList::showItemTable(const std::shared_ptr<ItemListModel>& model)
{
    TableView* tableView = TableView::create(this, TABLE_SIZE);
    tableView->setDirection(TableView::Direction::VERTICAL);
    tableView->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
    tableView->setDelegate(this);
    tableView->setBounceable(false);
    auto winSize = Director::getInstance()->getWinSize();
    tableView->setPosition(Vec2((winSize.width - TABLE_SIZE.width) / 2 - 10, winSize.height - TABLE_SIZE.height - 360));
    tableView->setTag(TAG_SPRITE::TABLE);
    addChild(tableView, Z_ORDER::Z_TABLE);
    tableView->reloadData();
}

void ItemBoxList::showScrollBar()
{
    TableView* tableView = static_cast<TableView*>(getChildByTag(TAG_SPRITE::TABLE));
    ScrollBar* scrollBar = ScrollBar::initScrollBar(TABLE_SIZE.height,
                                                    TABLE_SIZE.height,
                                                    tableView->getContainer()->getContentSize().height,
                                                    tableView->getContentOffset().y
                                                    );
    scrollBar->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    scrollBar->setPosition(tableView->getContentSize().width + 25,
                           Director::getInstance()->getWinSize().height / 2 - 15);
    scrollBar->setTag(TAG_SPRITE::BAR);
    addChild(scrollBar, Z_ORDER::Z_TABLE);
}

void ItemBoxList::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
    ScrollBar* scrollBar = static_cast<ScrollBar*>(getChildByTag(TAG_SPRITE::BAR));
    if (scrollBar) {
        scrollBar->updateScrollPoint(view->getContentOffset().y);
    }
}

Size ItemBoxList::cellSizeForTable(TableView* table)
{
    return CELL_SIZE;
}

TableViewCell* ItemBoxList::tableCellAtIndex(TableView* table, ssize_t idx)
{
    TableViewCell* cell = table->dequeueCell();
    cell = new TableViewCell();
    cell->autorelease();
    auto itemContent = ItemContent::create(_itemListModel->getViewItems().at(idx), CC_CALLBACK_3(ItemBoxList::onTapItemList, this));
    itemContent->setAnchorPoint(Vec2::ZERO);
    itemContent->setPosition(Vec2(0, 0));
    cell->addChild(itemContent);
    return cell;
}

void ItemBoxList::onTapItemList(Ref* sender, ItemContent::Widget::TouchEventType type, const std::shared_ptr<ItemListModel::Item>& itemModel)
{
    auto table = static_cast<TableView*>(getChildByTag(TAG_SPRITE::TABLE));
    if (table) {
        float touchPoint = table->getContentOffset().y;
        if (type == ui::Widget::TouchEventType::BEGAN) {
            buttonMoveFlag = false;
            _buttonMovePositionY = touchPoint;
            return;
        }
        if (type == ui::Widget::TouchEventType::MOVED) {
            if (_buttonMovePositionY + BUTTON_MOVE_DEF < touchPoint || _buttonMovePositionY - BUTTON_MOVE_DEF > touchPoint) {
                buttonMoveFlag = true;
            }
            return;
        }
        if (type == ui::Widget::TouchEventType::ENDED) {
            if (_buttonMovePositionY + BUTTON_MOVE_DEF < touchPoint || _buttonMovePositionY - BUTTON_MOVE_DEF > touchPoint) {
                buttonMoveFlag = true;
            }
            if (buttonMoveFlag == false) {
                SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
                _callBack(itemModel);
            }
            buttonMoveFlag = false;
            return;
        }
    }
}

ssize_t ItemBoxList::numberOfCellsInTableView(TableView* table)
{
    return _itemListModel->getViewItems().size();
}