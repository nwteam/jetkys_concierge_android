#ifndef __syanago__ItemBoxList__
#define __syanago__ItemBoxList__

#include "cocos2d.h"
#include "create_func.h"
#include "ItemListModel.h"
#include "cocos-ext.h"

USING_NS_CC_EXT;
USING_NS_CC;

class ItemBoxList : public Layer, public create_func<ItemBoxList>, public TableViewDelegate, public TableViewDataSource
{
public:
    ItemBoxList();
    ~ItemBoxList();
    typedef std::function<void(const std::shared_ptr<ItemListModel::Item>& itemModel)> OnSelectCallback;
    using create_func::create;
    bool init(const std::shared_ptr<ItemListModel>& model, const OnSelectCallback& callback);
    
    virtual void scrollViewDidScroll(ScrollView* view);
    virtual void scrollViewDidZoom(ScrollView* view){};
    virtual Size cellSizeForTable(TableView* table);
    virtual TableViewCell* tableCellAtIndex(TableView* table,ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(TableView* table);
    virtual void tableCellTouched(TableView* table,TableViewCell* cell){};
    
private:
    enum TAG_SPRITE {
        TABLE = 0,
        BAR,
    };
    enum Z_ORDER{
        Z_TABLE = 0,
    };
    const int BUTTON_MOVE_DEF = 5;
    const Size CELL_SIZE = Size(562, 102);
    const Size TABLE_SIZE = Size(562,412);
    
    bool buttonMoveFlag;
    float _buttonMovePositionY;
    OnSelectCallback _callBack;
    std::shared_ptr<ItemListModel> _itemListModel;
    
    void showItemKindChangeView(const std::shared_ptr<ItemListModel>& model);
    void showItemTable(const std::shared_ptr<ItemListModel>& model);
    void showScrollBar();
    
    void onTapItemList(Ref *sender, cocos2d::ui::Widget::TouchEventType type, const std::shared_ptr<ItemListModel::Item>& itemModel);
};

#endif
