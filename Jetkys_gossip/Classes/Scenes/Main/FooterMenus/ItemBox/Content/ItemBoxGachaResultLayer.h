#ifndef __syanago__ItemBoxGachaResultLayer__
#define __syanago__ItemBoxGachaResultLayer__

#include "cocos2d.h"
#include "create_func.h"
#include "ItemBoxGahcaCallBackModel.h"
#include "editor-support/spine/Json.h"
#include "GachaResponseModel.h"

USING_NS_CC;

class ItemBoxGachaResultLayer: public Layer, public create_func<ItemBoxGachaResultLayer>
{
public:
    using create_func::create;
    bool init(const std::shared_ptr<ItemBoxGahcaCallbackModel>& callbackModel,
              Json* data);
    ItemBoxGachaResultLayer();
    ~ItemBoxGachaResultLayer();
    
private:
    enum TAG_SPRITE{
        RESULT = 0,
        EFFECT,
    };
    enum Z_ORDER{
        Z_RESULT = 0,
        Z_EFFECT,
    };

    std::shared_ptr<ItemBoxGahcaCallbackModel> _gachaCallbackModel;
    std::shared_ptr<GachaResponseModel> _gachaResponseModel;
    
    void showGachaResult(std::shared_ptr<GachaResponseModel>& model);
    void onCharacterDetailCallback(const int playerCharacterId, const int masterCharacterId);
    void onPartsDetailCallback(const int playerPartsId, const int masterPartsId);
    void gachaEffectCallback();
    void dropEffectCallBack();
};

#endif
