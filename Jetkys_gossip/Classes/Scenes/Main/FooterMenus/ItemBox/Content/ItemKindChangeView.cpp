#include "ItemKindChangeView.h"
#include "HeaderStatus.h"
#include "FontDefines.h"

ItemKindChangeView::ItemKindChangeView():
    _callBack(nullptr)
{}

ItemKindChangeView::~ItemKindChangeView()
{
    _callBack = nullptr;
}

bool ItemKindChangeView::init(const std::shared_ptr<ItemListModel>& model, const OnChangeContentCallback& changeCallback)
{
    if (!Layer::create()) {
        return false;
    }
    _callBack = changeCallback;

    showBackground();
    showItemKindName(model);
    showChangeButton(model);
    return true;
}

void ItemKindChangeView::showBackground()
{
    auto background = Sprite::create("itembox_title_base.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    background->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2, Director::getInstance()->getWinSize().height - background->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 120));
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void ItemKindChangeView::showItemKindName(const std::shared_ptr<ItemListModel>& model)
{
    auto itemKindNameLabel = Label::createWithTTF(model->getViewItemKIndName(), FONT_NAME_2, 32);
    itemKindNameLabel->setColor(COLOR_YELLOW);
    itemKindNameLabel->setTag(TAG_SPRITE::ITEM_KIND_NAME);
    itemKindNameLabel->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() - Vec2(0, 16));
    itemKindNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(itemKindNameLabel, Z_ORDER::Z_ITEM_KIND_NAME);
}

void ItemKindChangeView::showChangeButton(const std::shared_ptr<ItemListModel>& model)
{
    ui::Button* leftButton = ui::Button::create("itembox_left_arrow.png");
    leftButton->setTag(TAG_SPRITE::LEFT_BUTTON);
    leftButton->addTouchEventListener(CC_CALLBACK_2(ItemKindChangeView::onTapLeftButton, this));
    leftButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    leftButton->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() + Vec2(-10 - getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2, 0));
    leftButton->setVisible(false);
    addChild(leftButton, Z_ORDER::Z_LEFT_BUTTON);

    ui::Button* rightButton = ui::Button::create("itembox_right_arrow.png");
    rightButton->setTag(TAG_SPRITE::RIGHT_BUTTON);
    rightButton->addTouchEventListener(CC_CALLBACK_2(ItemKindChangeView::onTapRightButton, this));
    rightButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    rightButton->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() + Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 + 10, 0));
    rightButton->setVisible(false);
    addChild(rightButton, Z_ORDER::Z_RIGHT_BUTTON);
}

void ItemKindChangeView::onTapLeftButton(Ref* pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
        _callBack(TAP_DIRECTION::LEFT);
    }
}

void ItemKindChangeView::onTapRightButton(Ref* pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
        _callBack(TAP_DIRECTION::RIGHT);
    }
}
