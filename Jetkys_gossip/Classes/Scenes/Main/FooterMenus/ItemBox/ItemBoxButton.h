#ifndef __syanago__ItemBoxButton__
#define __syanago__ItemBoxButton__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class ItemBoxButton : public ui::Button
{
public:
    ItemBoxButton();
    static ItemBoxButton* create();
    void initialize();
    void startItemBoxButtonAnimation();
    enum TAG_SPRITE{
        BUTTON = 0,
        ITEM_BOX_BODY,
        ITEM_BOX_COMMENT
    };
private:
    
    enum Z_ORDER{
        Z_BUTTON = 0,
        Z_ITEM_BOX_BODY,
        Z_ITEM_BOX_COMMENT,
    };
    void showButtonImage();
    
    void finishSdAnimation();
};

#endif