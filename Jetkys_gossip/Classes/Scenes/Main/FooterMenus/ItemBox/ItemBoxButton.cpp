#include "ItemBoxButton.h"
#include "SoundHelper.h"

ItemBoxButton::ItemBoxButton() {}

ItemBoxButton* ItemBoxButton::create()
{
    ItemBoxButton* btn = new (std::nothrow) ItemBoxButton();
    if (btn && btn->init("footer_home_button_base.png")) {
        btn->initialize();
        btn->setZoomScale(0);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void ItemBoxButton::initialize()
{
    showButtonImage();
}


void ItemBoxButton::showButtonImage()
{
    auto buttonImage = Sprite::create("itembox_button.png");
    buttonImage->setTag(TAG_SPRITE::BUTTON);
    buttonImage->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    addChild(buttonImage, Z_ORDER::Z_BUTTON);
}

void ItemBoxButton::startItemBoxButtonAnimation()
{
    getChildByTag(TAG_SPRITE::BUTTON)->setVisible(false);

    // CCAnimationの初期化
    auto sdcharacterAnimationStart = Animation::create();
    auto sdcharacterAnimation = Animation::create();
    auto sdcharacterAnimationEnd = Animation::create();
    auto sdcharacterCommentAnimation = Animation::create();

    // 画像を配列に代入
    for (int i = 1; i < 5; i++) {
        sdcharacterAnimationStart->addSpriteFrameWithFile(StringUtils::format("itembox_sd_%d.png", i));
        if (i != 1) {
            sdcharacterAnimationEnd->addSpriteFrameWithFile(StringUtils::format("itembox_sd_%d.png", 5 - i));
        }
        sdcharacterCommentAnimation->addSpriteFrameWithFile(StringUtils::format("itembox_comment_%d.png", i));
    }
    for (int i = 0; i < 3; i++) {
        sdcharacterAnimation->addSpriteFrameWithFile("itembox_sd_3.png");
        sdcharacterAnimation->addSpriteFrameWithFile("itembox_sd_4.png");
    }
    sdcharacterAnimationStart->setDelayPerUnit(0.1f); // アニメの動く時間を設定
    sdcharacterAnimation->setDelayPerUnit(0.1f);
    sdcharacterAnimationEnd->setDelayPerUnit(0.1f);
    sdcharacterCommentAnimation->setDelayPerUnit(0.1f);

    auto sdSprite = Sprite::create("itembox_button.png");
    sdSprite->setTag(TAG_SPRITE::ITEM_BOX_BODY);
    sdSprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    sdSprite->runAction(Sequence::create(Animate::create(sdcharacterAnimationStart),
                                         Animate::create(sdcharacterAnimation),
                                         Animate::create(sdcharacterAnimationEnd),
                                         CallFunc::create(CC_CALLBACK_0(ItemBoxButton::finishSdAnimation, this)),
                                         NULL));
    addChild(sdSprite, Z_ITEM_BOX_BODY);

    auto sdCommentSprite = Sprite::create();
    sdCommentSprite->setTag(TAG_SPRITE::ITEM_BOX_COMMENT);
    sdCommentSprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    sdCommentSprite->runAction(Sequence::create(CCDelayTime::create(1),
                                                Animate::create(sdcharacterCommentAnimation),
                                                NULL));
    addChild(sdCommentSprite, Z_ITEM_BOX_COMMENT);
}

void ItemBoxButton::finishSdAnimation()
{
    removeChildByTag(TAG_SPRITE::ITEM_BOX_BODY);
    removeChildByTag(TAG_SPRITE::ITEM_BOX_COMMENT);
    getChildByTag(TAG_SPRITE::BUTTON)->setVisible(true);
}
