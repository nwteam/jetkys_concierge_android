#ifndef __syanago__StartLayerCheckPlayerTryCourseModel__
#define __syanago__StartLayerCheckPlayerTryCourseModel__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;

class StartLayerCheckPlayerTryCourseModel : public cocos2d::Layer
{
public:
    StartLayerCheckPlayerTryCourseModel();
    ~StartLayerCheckPlayerTryCourseModel();
    
    /**
     *
     入力したコースにの状態を調べる事ができるメソッド。以下を調べる事ができる
     ・プレイヤーが入力したコースを挑戦できるか？
     ・入力したコースのドロップ（スライム）画像ファイル名を調べる事ができる
     ・入力したコースのドロップ（スライム）背景画像ファイル名を調べる
     ・入力したコースのバッジの数を調べる事ができます
     *
     **/
    static StartLayerCheckPlayerTryCourseModel* createCourseChecker(int courseId);
    
    
    /**
     *
     コース入力してあるモデルから入力したコースの挑戦できるかの情報を取得できる。
     *
     **/
    bool getCourseTryStatus();
    
    /**
     *
     コース入力してあるモデルから入力したコースのドロップの画像のファイル名を取得できる
     *
     **/
    std::string getCourseDropImageFilePath();
    
    /**
     *
     コース入力してあるモデルから入力したコースのドロップの画像のファイル名を取得できる
     *
     **/
    std::string getCourseDropBackGroundImageFilePath();
    
    /**
     *
     コース入力してあるモデルから入力したコースのバッジの数を知る事ができまる
     *
     **/
    int getCourseBageCount();
    
    /**
     *
     コース入力してあるモデルから入力したコースがCommingSoon状態なのかを調べる事ができる
     *
     **/
    bool getCommingSoonFlag();
    
    /**
     *
     コースタイプの情報を回収
     *
     **/
    int getCourseType();
    
private:
    std::string _dropFilePathName;
    std::string _dropBackGroundFilePathName;
    bool _tryStatus;
    int _bageCount;
    bool _commingSoonFlag;
    int _courseType;
    
    /**
     *
     コースの処理を行うメソッド
     *
     **/
    void setCheckTryCourse(int courseId);
    
};

#endif /* defined(__syanago__StartLayerCheckPlayerTryCourseModel__) */
