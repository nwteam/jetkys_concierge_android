#ifndef __syanago__DecisionRace__
#define __syanago__DecisionRace__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Loading.h"
#include "RaceUIControll.h"
#include "create_func.h"
#include "show_head_line.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"
#include "TeamStatus.h"
#include "TeamStatusCalculationModel.h"

using namespace SyanagoAPI;
class StartLayer;

class DecisionRace : public create_func<DecisionRace>, public show_head_line, public cocos2d::Layer
{
public:
    using create_func::create;
    DecisionRace();
    ~DecisionRace();
    
    virtual bool init(StartLayer* startLayler, int power, int technique, int brake, int coseId,int helpCharacterId, bool isFriend);
    void createDecisionView(int courseId, int helpCharacterId, bool isFriend ,TeamStatusCalculationModel* teamStatusCalculationModel);
    
private:
    enum TAG_SPRITE{
        HEADLINE_BASE = 0,
        GO_BACK_BUTTON,
        HEDALINE_MENU,
        SCLOLL_MESSAGE,
        COURSE_DISCRIPTION,
    };
    StartLayer *_startLayer;
    
    bool _goToRaceFlag;
    int _masterCourseId;
    int _leaderPlayerCharacterId;
    
    DefaultProgress* progress;
    RequestAPI* request;
    TeamStatus* teamStatus;
    
    std::vector<std::shared_ptr<const CharacterModel>> _playerCharacters;
    
    using show_head_line::showHeadLine;
    
    void pageBack(Ref* pSender);
    void goToRaceCallBack(Ref* pSender);
    void onRasponesSetTry();
    void goToRace();
    
    Layer* createCourseNameInfomation(std::shared_ptr<CourseModel> courseModel);
    Label* createCourseInfomationGuideLabel(const cocos2d::Point designPosition);
    
    Sprite* createLeaderCharacterInfomation(const int masterCharacterId);
    Sprite* createHelpCharacterInfomation(const int masterCharacterId, const bool isFriend);
    Sprite* createSupportCharacterView(const int playerCharacterId);
    
    Sprite* createDicisionSattus(TeamStatusCalculationModel* teamStatusCalculationModel);
    Sprite* createStatusView(const std::string statusName, const int status);
    
    Sprite* createDiceInfomation(const int playerCharacterId);
    
    Menu* createDecisionButton(Sprite* buttonPosition);
    void showScrollMessage();
    
};

#endif /* defined(__syanago__DecisionRace__) */
