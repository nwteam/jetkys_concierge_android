#ifndef __syanago__MapScene__
#define __syanago__MapScene__

#include "create_func.h"
#include "cocos2d.h"
#include "MapLayer.h"
#include "DialogView.h"
#include "DialogDefine.h"
#include "show_head_line.h"

enum{
    CHECK_COSE_APPLY = 1,
    CHECK_FUEL = 2
};

class StartLayer;

class MapScene : public create_func<MapScene>, public show_head_line, public cocos2d::Layer, public MapLayerDelegate, public DialogDelegate
{
public:
    bool init(StartLayer* startLayer, int areaID, bool isTutorial);
    using create_func::create;
    
private:
    enum TAG_SPRITE{
        RANKING_LAYER = 0
    };
    
    using show_head_line::showHeadLine;
    void showRankingButton();
    void onTapBackButton(Ref* pSender);
    
    void btSelectCose(int coseId);
    bool checkTryRace(int Tag);
    void nextScene();
    void nextDialogScene();
    
    void showDialogView(int dialogKind, int coseId);
    void btDialogCallback(ButtonIndex aIndex);
    
    void getfule();
    
    void onGetFuelByTokenTouched(Ref* sender, ui::Widget::TouchEventType type);
    void onTweetAndGetFuelEnded();
    
    bool checkPlayerFuel(int coseId);
    
    
    StartLayer *_startLayer;
    int _areaId;
    int _dialogKind;
    DialogView* _dialog;
    int _coseId;
    std::string _coseName;
    std::string _coseInfo;
    
    void onCallBackRanking(Ref* pSender);
    
};

#endif /* defined(__syanago__MapScene__) */
