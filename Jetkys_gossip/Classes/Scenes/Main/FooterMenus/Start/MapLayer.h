#ifndef __syanago__MapLayer__
#define __syanago__MapLayer__

#include "create_func.h"
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "PlayerController.h"
#include "LayerPriority.h"
#include "StartLayerCheckPlayerTryCourseModel.h"
USING_NS_CC;
USING_NS_CC_EXT;

class MapScene;

class MapLayerDelegate {
public:
    virtual void btSelectCose(int coseId){};
};

class MapLayer : public create_func<MapLayer>, public ScrollViewDelegate, public LayerPriority
{
public:
    ~MapLayer();
    bool init(MapLayerDelegate* mapScene, int areaId);
    using create_func::create;
    
    void setScrollEnable(bool flag);
    
private:
    void showMap(int areaId);
    Sprite* createMapSprite(int areaId);
    void showStars(MenuItemSprite* courseSprite, StartLayerCheckPlayerTryCourseModel* model, const int courseId);
    Sprite* createStar(float delta, const int courseId);
    void showPlaceOfMission(MenuItemSprite* slime, const int courseId);
    
    void scrollViewDidScroll(ScrollView *view);
    void scrollViewDidZoom(ScrollView *view);
    
    void pushCosePoint(Ref* pSender);
    void update(float dt);
    void scrolDidAutoZoom();
    // zoom function
    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
    virtual void onTouchCancelled(Touch *touch, Event *unused_event);
    
    MapLayerDelegate *_mapDelegate;    ScrollView* pScrollView;
    std::vector<Touch*> _touches;
    Sprite* _mapSheet;
    Menu *coseList;
    int _mstId;
    bool _double;
    bool _scaleAuto;
    bool _zoomin;
    bool _zoomout;
    Point _pPosi;
    int _doubleTapTime;
    int _areaId;
    
};


#endif /* defined(__syanago__MapLayer__) */
