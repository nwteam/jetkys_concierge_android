#ifndef __syanago__SelectArea__
#define __syanago__SelectArea__

#include <map>
#include "create_func.h"
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "StartLayer.h"
#include "ScrollBar.h"
#include "MenuTouch.h"


USING_NS_CC;
USING_NS_CC_EXT;

class Cell;

class SelectArea : public Layer, public create_func<SelectArea>, public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate
{
public:
    SelectArea();
    ~SelectArea();
    typedef std::function<void()> OnBackCallback;
    typedef std::function<void(const int areaId)> OnSelectAreaCallback;
    typedef std::function<void(const bool isVisible)> OnViewFooterCallback;
    
    bool init(int mapId, const OnBackCallback& backCallback, const OnSelectAreaCallback& selectCallBack, const OnViewFooterCallback& visibleCallback);
    using create_func::create;
    
   
    
private:
    enum TAG_SPRITE {
        LIST_BASE = 0,
        RANKING,
    };
    enum Z_ORDER{
        Z_RANKING = 100,
    };
    enum DATA_KEY {
        AREA_ID = 0,
        CLEARED,
        COMMING_SOON
    };
    OnBackCallback _onBackCallback;
    OnSelectAreaCallback _onSelectAreaCallback;
    OnViewFooterCallback _onVisibleCallback;
    EventListenerTouchOneByOne* _listener;
    
    
    std::vector<std::map<DATA_KEY, int>> tableData;
    
    void showBackGround();
    void showHeadLine(int mapId);
    void showTable();
    void showRankingButton();
    void onCallBackRanking(Ref* pSender);
    
    void setDataForTable(int mapId);
    
    void showEmptyErrorDialog();
    void showPlaceOfMission(Sprite* selectAreaMenu, bool isClearArea);
    
    void onTapBackButton(Ref* pSender);
    virtual void tableCellTouched(TableView* table,TableViewCell* cell);
    
    virtual Size cellSizeForTable(TableView* table);
    virtual TableViewCell* tableCellAtIndex(TableView* table,ssize_t idx);
    virtual void tableCellHighlight(TableView* table, TableViewCell * cell);
    virtual void tableCellUnhighlight(TableView* table, TableViewCell *  cell);
    virtual ssize_t numberOfCellsInTableView(TableView* table);
    Cell * cellWithIdx(int idx);
    
    virtual void scrollViewDidZoom(ScrollView* view){};
    void scrollViewDidScroll(ScrollView* view);
    
    
    TableView* _tableView;
    ScrollBar* _scrollBar;
    MenuTouch* _menu;
    Label* _buttonLabel;
    
};

#endif /* defined(__syanago__SelectArea__) */
