#include "DecisionRace.h"
#include "PlayerController.h"
#include "MainScene.h"
#include "FontDefines.h"
#include "TutorialLayer.h"
#include "MenuTouch.h"
#include "SystemSettingModel.h"
#include "UserDefaultManager.h"
#include "HeaderStatus.h"
#include "MapModel.h"
#include "LevelModel.h"
#include "LeaderSkillNameModel.h"

USING_NS_CC;

DecisionRace::DecisionRace():
    _goToRaceFlag(false), teamStatus(nullptr) {}

DecisionRace::~DecisionRace()
{
    for (int i = 0; i < _playerCharacters.size(); i++) {
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_hk.plist", _playerCharacters.at(i)->getID()).c_str());
    }
}

bool DecisionRace::init(StartLayer* startLayler, int power, int technique, int brake, int courseId, int helpCharacterId, bool isFriend)
{
    if (!Layer::init()) {
        return false;
    }
    showHeadLine(this, "編成確認", CC_CALLBACK_1(DecisionRace::pageBack, this));
    setTag(ST_DECISION);

    _startLayer = startLayler;
    _masterCourseId = courseId;

    TeamStatusCalculationModel* teamStatusCalculationModel = new TeamStatusCalculationModel(power, technique, brake, helpCharacterId, isFriend);

    if (teamStatus != nullptr) {
        delete teamStatus;
        teamStatus = nullptr;
    }
    teamStatus = new TeamStatus(teamStatusCalculationModel->getTotalPower(),
                                teamStatusCalculationModel->getTotalTechnique(),
                                teamStatusCalculationModel->getTotalBrake());

    createDecisionView(courseId, helpCharacterId, isFriend, teamStatusCalculationModel);

    delete teamStatusCalculationModel;
    teamStatusCalculationModel = nullptr;

    return true;
}

void DecisionRace::pageBack(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    if (!_startLayer->_mainScene->isMainTutorial()) {
        _startLayer->showLayer(ST_SELECT);
    }
}

void DecisionRace::createDecisionView(int courseId, int helpCharacterId, bool isFriend, TeamStatusCalculationModel* teamStatusCalculationModel)
{
    int playerCharacterId[] = {
        PLAYERCONTROLLER->_playerTeams->getLearderCharacterId(),
        PLAYERCONTROLLER->_playerTeams->getSupport1CharacterId(),
        PLAYERCONTROLLER->_playerTeams->getSupport2CharacterId(),
        PLAYERCONTROLLER->_playerTeams->getSupport3CharacterId()
    };

    for (int i = 0; i < 4; i++) {
        if (playerCharacterId[i] > 0) {
            int masterCharacterId = PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[i]]->getCharactersId();
            if (masterCharacterId != 0) {
                _playerCharacters.push_back(CharacterModel::find(masterCharacterId));
            }
        }
    }
    _leaderPlayerCharacterId = playerCharacterId[0];

    auto contentLayer = Layer::create();
    Size winSize = Director::getInstance()->getWinSize();
    contentLayer->setContentSize(Size(winSize.width, 706));
    {
        std::shared_ptr<CourseModel>courseModel(CourseModel::find(courseId));
        auto labelLayer = createCourseNameInfomation(courseModel);
        contentLayer->addChild(labelLayer);

        auto decisionBg = createDicisionSattus(teamStatusCalculationModel);
        decisionBg->setPosition(Point(winSize.width / 2,
                                      labelLayer->getChildByTag(TAG_SPRITE::COURSE_DISCRIPTION)->getPosition().y - labelLayer->getChildByTag(TAG_SPRITE::COURSE_DISCRIPTION)->getContentSize().height / 2 - decisionBg->getContentSize().height / 2 + 15));
        {
            auto lederBase = createLeaderCharacterInfomation(PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[0]]->getCharactersId());
            decisionBg->addChild(lederBase);
            lederBase->setPosition(Point(14 + lederBase->getContentSize().width / 2, decisionBg->getContentSize().height - lederBase->getContentSize().height / 2 - 14));

            auto friendBase = createHelpCharacterInfomation(helpCharacterId, isFriend);
            friendBase->setPosition(Point(lederBase->getPosition().x, lederBase->getPosition().y - 70));
            decisionBg->addChild(friendBase);

            auto suportBase1 = createSupportCharacterView(playerCharacterId[1]);
            suportBase1->setPosition(Point(132 + suportBase1->getContentSize().width / 2,
                                           lederBase->getPosition().y - lederBase->getContentSize().height / 2 + suportBase1->getContentSize().height / 2));
            decisionBg->addChild(suportBase1);

            auto suportBase2 = createSupportCharacterView(playerCharacterId[2]);
            suportBase2->setPosition(Point(suportBase1->getPosition().x + suportBase1->getContentSize().width / 2 + suportBase2->getContentSize().width / 2 + 6,
                                           suportBase1->getPosition().y));
            decisionBg->addChild(suportBase2);

            auto suportBase3 = createSupportCharacterView(playerCharacterId[3]);
            suportBase3->setPosition(Point(suportBase2->getPosition().x + suportBase2->getContentSize().width / 2 + suportBase3->getContentSize().width / 2 + 6,
                                           suportBase2->getPosition().y));
            decisionBg->addChild(suportBase3);

            auto diceBase = createDiceInfomation(playerCharacterId[0]);
            diceBase->setPosition(Point(14 + lederBase->getContentSize().width / 2,
                                        friendBase->getPosition().y - friendBase->getContentSize().height / 2 - 10 - diceBase->getContentSize().height / 2));
            decisionBg->addChild(diceBase);
        }
        contentLayer->addChild(decisionBg);

        auto btnList = createDecisionButton(decisionBg);
        contentLayer->addChild(btnList);

        showScrollMessage();

        float deltaHeight = winSize.height - contentLayer->getContentSize().height - getChildByTag(TAG_SPRITE::SCLOLL_MESSAGE)->getContentSize().height - 222;
        if (deltaHeight > 0) {
            contentLayer->setPositionY(-deltaHeight / 2);
            labelLayer->setPositionY(deltaHeight / 4);
            btnList->setPositionY(-deltaHeight / 4);
        }
    }
    addChild(contentLayer);
}

Layer* DecisionRace::createCourseNameInfomation(std::shared_ptr<CourseModel>courseModel)
{
    auto result = Layer::create();
    std::shared_ptr<MapModel>mapModel(MapModel::find(courseModel->getMapID()));
    auto courseNameLabel = Label::createWithTTF(mapModel->getName() + " " + courseModel->getName(), FONT_NAME_2, 30);
    courseNameLabel->setColor(Color3B::BLACK);
    Size winSize = Director::getInstance()->getWinSize();
    courseNameLabel->setPosition(Point(winSize.width * 0.5,
                                       winSize.height - courseNameLabel->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 12 - 17 - 55 - 10));
    courseNameLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    result->addChild(courseNameLabel);

    auto coseInfoLabel = Label::createWithTTF(courseModel->getDescription(), FONT_NAME_2, 26);
    coseInfoLabel->setPosition(Point(winSize.width * 0.5, courseNameLabel->getPosition().y - 15 - 5 - 10));
    coseInfoLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    coseInfoLabel->setColor(Color3B::BLACK);
    coseInfoLabel->setTag(TAG_SPRITE::COURSE_DISCRIPTION);
    result->addChild(coseInfoLabel);

    int designWidth = 0;
    if (courseNameLabel->getContentSize().width / 2 > coseInfoLabel->getContentSize().width / 2) {
        designWidth = courseNameLabel->getContentSize().width / 2;
    } else {
        designWidth = coseInfoLabel->getContentSize().width / 2;
    }

    int designHeightPosi = courseNameLabel->getPosition().y - ((courseNameLabel->getPosition().y - coseInfoLabel->getPosition().y) / 2);

    result->addChild(createCourseInfomationGuideLabel(Point(winSize.width / 2 + (designWidth + 25), designHeightPosi)));
    result->addChild(createCourseInfomationGuideLabel(Point(winSize.width / 2 - (designWidth + 25), designHeightPosi)));

    return result;
}

Label* DecisionRace::createCourseInfomationGuideLabel(const Point designPosition)
{
    auto result = Label::createWithTTF("—", FONT_NAME_2, 30);
    result->setPosition(designPosition);
    result->setColor(Color3B::BLACK);
    return result;
}

Sprite* DecisionRace::createDicisionSattus(TeamStatusCalculationModel* teamStatusCalculationModel)
{
    auto result = makeSprite("decision_base.png");
    Size winSize = Director::getInstance()->getWinSize();

    auto status1 = createStatusView("技量", teamStatusCalculationModel->getViewTechnique());
    status1->setPosition(Point(result->getContentSize().width / 2,
                               status1->getContentSize().height / 2 + 14));
    result->addChild(status1);

    auto status2 = createStatusView("馬力", teamStatusCalculationModel->getViewPower());
    status2->setPosition(Point(14 + status1->getContentSize().width / 2,
                               status1->getPosition().y));
    result->addChild(status2);

    auto status3 = createStatusView("忍耐", teamStatusCalculationModel->getViewBrake());
    status3->setPosition(Point(result->getContentSize().width - 14 - status3->getContentSize().width / 2,
                               status1->getPosition().y));
    result->addChild(status3);
    return result;
}

Sprite* DecisionRace::createStatusView(const std::string statusName, const int status)
{
    auto result = makeSprite("decision_status_base.png");
    auto statusLabel = Label::createWithTTF(statusName.c_str(), FONT_NAME_2, 22);
    result->addChild(statusLabel);
    statusLabel->setPosition(Point(5 + statusLabel->getContentSize().width / 2, result->getContentSize().height / 2 - 11));
    auto statusLabelStatus = Label::createWithTTF(FormatWithCommas(status), FONT_NAME_2, 22);
    statusLabelStatus->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->addChild(statusLabelStatus);
    statusLabelStatus->setPosition(Point(result->getContentSize().width - statusLabelStatus->getContentSize().width / 2 - 5,
                                         result->getContentSize().height / 2 - 11));
    statusLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    return result;
}

Sprite* DecisionRace::createLeaderCharacterInfomation(const int masterCharacterId)
{
    std::shared_ptr<LeaderSkillNameModel>myLeaderSkillNameModel(LeaderSkillNameModel::getLeaderSkillNameByCharacterId(masterCharacterId));
    auto result = makeSprite("decision_leder_base.png.png");

    // テクスチャーパック
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_hk.plist", masterCharacterId).c_str());
    auto lederchara = makeSprite(StringUtils::format("%d_hk.png", masterCharacterId).c_str());
    result->addChild(lederchara);
    lederchara->setAnchorPoint(Vec2::ZERO);

    auto lederFrame = makeSprite("decision_leder_frame.png");
    result->addChild(lederFrame, 2);
    lederFrame->setAnchorPoint(Vec2::ZERO);
    lederFrame->setPosition(Point(0, -2));

    auto lSkillLabel = Label::createWithTTF("リーダースキル ", FONT_NAME_2, 22);
    result->addChild(lSkillLabel);
    lSkillLabel->setColor(COLOR_YELLOW);
    lSkillLabel->setPosition(Point(4 + lSkillLabel->getContentSize().width / 2,
                                   result->getContentSize().height - 14 - 4 - 11));
    lSkillLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto lSkillNameLabel = Label::createWithTTF(myLeaderSkillNameModel->getName(), FONT_NAME_2, 22);
    result->addChild(lSkillNameLabel);
    lSkillNameLabel->setPosition(Point(lSkillLabel->getPosition().x + lSkillLabel->getContentSize().width / 2 + lSkillNameLabel->getContentSize().width / 2,
                                       lSkillLabel->getPosition().y));
    lSkillNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto lSkillInfoLabel = Label::createWithTTF(myLeaderSkillNameModel->getDescription(), FONT_NAME_2, 19);
    result->addChild(lSkillInfoLabel);
    lSkillInfoLabel->setPosition(Point(4 + lSkillInfoLabel->getContentSize().width / 2,
                                       lSkillLabel->getPosition().y - lSkillLabel->getContentSize().height / 2));
    lSkillInfoLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    return result;
}

Sprite* DecisionRace::createHelpCharacterInfomation(const int masterCharacterId, const bool isFriend)
{
    std::shared_ptr<LeaderSkillNameModel>helperLeaderSkillNameModel(LeaderSkillNameModel::getLeaderSkillNameByCharacterId(masterCharacterId));
    auto result = makeSprite("decision_friend_base.png");
    // テクスチャーパック
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_hk.plist", masterCharacterId).c_str());
    auto friendchara = makeSprite(StringUtils::format("%d_hk.png", masterCharacterId).c_str());
    friendchara->setAnchorPoint(Vec2(1.0f, 1.0f));
    friendchara->setPosition(Point(result->getContentSize().width, result->getContentSize().height));
    result->addChild(friendchara);

    auto friendFrame = makeSprite("decision_friend_frame.png");
    friendFrame->setAnchorPoint(Vec2::ZERO);
    result->addChild(friendFrame, 2);

    if (isFriend == false) {
        auto fSkillInfoLabel = Label::createWithTTF("ゲストのリーダースキルは発動しません", FONT_NAME_2, 25);
        fSkillInfoLabel->setColor(Color3B(200, 200, 200));
        fSkillInfoLabel->setPosition(Point(result->getContentSize().width - 10 - fSkillInfoLabel->getContentSize().width / 2,
                                           fSkillInfoLabel->getContentSize().height / 2 - 15));
        fSkillInfoLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        result->addChild(fSkillInfoLabel);
    } else {
        auto fSkillInfoLabel = Label::createWithTTF(helperLeaderSkillNameModel->getDescription(), FONT_NAME_2, 19);
        fSkillInfoLabel->setPosition(Point(result->getContentSize().width - 4 - fSkillInfoLabel->getContentSize().width / 2,
                                           fSkillInfoLabel->getContentSize().height / 2 - 9.5));
        fSkillInfoLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        result->addChild(fSkillInfoLabel);

        auto fSkillNameLabel = Label::createWithTTF(helperLeaderSkillNameModel->getName(), FONT_NAME_2, 22);
        fSkillNameLabel->setPosition(Point(result->getContentSize().width - 4 - fSkillNameLabel->getContentSize().width / 2,
                                           fSkillInfoLabel->getPosition().y + fSkillInfoLabel->getContentSize().height / 2 + fSkillNameLabel->getContentSize().height / 2 - 17));
        fSkillNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        result->addChild(fSkillNameLabel);

        auto fSkillLabel = Label::createWithTTF("フレンドリーダースキル ", FONT_NAME_2, 22);
        fSkillLabel->setColor(COLOR_YELLOW);
        fSkillLabel->setPosition(Point(fSkillNameLabel->getPosition().x - fSkillNameLabel->getContentSize().width / 2 - fSkillLabel->getContentSize().width / 2,
                                       fSkillNameLabel->getPosition().y));
        fSkillLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        result->addChild(fSkillLabel);
    }
    return result;
}

Sprite* DecisionRace::createSupportCharacterView(const int playerCharacterId)
{
    auto result = makeSprite("decision_suport_base.png");
    if (playerCharacterId > 0) {
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_hk.plist", PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getCharactersId()).c_str());
        auto characterIcon = makeSprite(StringUtils::format("%d_hk.png", PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getCharactersId()).c_str());
        characterIcon->setAnchorPoint(Vec2::ZERO);
        result->addChild(characterIcon);
    }
    auto suportFrame = makeSprite("decision_suport_frame.png");
    suportFrame->setAnchorPoint(Vec2::ZERO);
    result->addChild(suportFrame);
    return result;
}

Sprite* DecisionRace::createDiceInfomation(const int playerCharacterId)
{
    auto result = makeSprite("decision_dice_base.png");

    int lederLevel = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getLevel();
    std::shared_ptr<LevelModel>levelModel(LevelModel::find(lederLevel));
    int lederAddDice = levelModel->getAddDice();

    std::vector<int>dice = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getDiceValues();
    Sprite* saikoroImg;
    int diceCount = (int)dice.size();
    int saikorowhid = -2;
    for (int i = 0; i < diceCount; i++) {
        if (i == 6) {
            saikorowhid += 5;
        }
        if (dice.at(i) < 1) {
            if (lederAddDice < 1) {
                saikoroImg = makeSprite("decision_dice_0.png");
                result->addChild(saikoroImg);
                saikoroImg->setPosition(Point(5 + saikorowhid + saikoroImg->getContentSize().width / 2,
                                              result->getContentSize().height / 2 + 2));
                saikorowhid += 11 + saikoroImg->getContentSize().width;
            } else {
                lederAddDice--;
                saikoroImg = makeSprite("decision_dice_non.png");
                result->addChild(saikoroImg);
                saikoroImg->setPosition(Point(5 + saikorowhid + saikoroImg->getContentSize().width / 2,
                                              result->getContentSize().height / 2 + 2));
                saikorowhid += 11 + saikoroImg->getContentSize().width;
            }
            continue;
        } else {
            saikoroImg = makeSprite(StringUtils::format("decision_dice_%d.png", dice.at(i)).c_str());
            result->addChild(saikoroImg);
            saikoroImg->setPosition(Point(5 + saikorowhid + saikoroImg->getContentSize().width / 2,
                                          result->getContentSize().height / 2 + 2));
            saikorowhid += 11 + saikoroImg->getContentSize().width;
            if (i > 5) {
                lederAddDice--;
            }
        }
    }
    dice.clear();
    return result;
}

Menu* DecisionRace::createDecisionButton(Sprite* buttonPosition)
{
    MenuItemImage* OkBtn = MenuItemImage::create("decision_next_button.png", "decision_next_button.png", CC_CALLBACK_1(DecisionRace::goToRaceCallBack, this));
    OkBtn->setPosition(Point(Director::getInstance()->getWinSize().width / 2,
                             buttonPosition->getPosition().y - buttonPosition->getContentSize().height / 2 - OkBtn->getContentSize().height / 2 - 10));
    Menu* btnList = MenuTouch::create();

    btnList->setPosition(Vec2::ZERO);
    btnList->addChild(OkBtn);
    return btnList;
}

void DecisionRace::showScrollMessage()
{
    auto teropbg = makeSprite("telop_bg.png");
    Size winSize = Director::getInstance()->getWinSize();
    teropbg->setPosition(Point(winSize.width / 2, teropbg->getContentSize().height / 2));
    teropbg->setTag(TAG_SPRITE::SCLOLL_MESSAGE);
    addChild(teropbg);

    auto reportMess = Label::createWithTTF(PLAYERCONTROLLER->_messages.at(14).c_str(), FONT_NAME_2, 22);
    reportMess->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    reportMess->setColor(Color3B::WHITE);
    reportMess->setAnchorPoint(Vec2(0, 0.75f));
    addChild(reportMess);

    reportMess->setPosition(Vec2(winSize.width, teropbg->getContentSize().height / 2));
    reportMess->runAction(RepeatForever::create(Sequence::create(MoveBy::create(TEROP_SPEED, Vec2(-1500, 0)), CallFuncN::create([](Ref* sender) {
        auto node = (Node*) sender;
        node->setPositionX(node->getPositionX() + 1500);
    }), NULL)));
}

void DecisionRace::goToRaceCallBack(Ref* pSender)
{
    if (_goToRaceFlag == false) {
        _goToRaceFlag = true;
        SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);

        // TO DO　テストコースの挑戦回数もデータとして欲しいならこのメソッドはいらない
        if (CourseModel::find(_masterCourseId)->getCourseType() == 4) {
            goToRace();
            return;
        }

        progress = new DefaultProgress();
        request = new RequestAPI(progress);
        progress->onStart();
        request->setTryRace(PLAYERCONTROLLER->_player->getRank(), _masterCourseId, CC_CALLBACK_0(DecisionRace::onRasponesSetTry, this));
    }
}

void DecisionRace::goToRace()
{
    _startLayer->goToRaceEfect();

    int leaderCharacterID = PLAYERCONTROLLER->_playerTeams->getLearderCharacterId();
    int mstCharacterId = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[leaderCharacterID]->getCharactersId();

    SoundHelper::shareHelper()->playCharacterVoiceEfect(mstCharacterId, 16);
    runAction(Sequence::create(DelayTime::create(3), CallFunc::create([this]() {
        // レース画面へ
        UserDefault* userDefalt = CCUserDefault::getInstance();
        userDefalt->setIntegerForKey(UserDefaultManager::KEY::TIME_OF_HEADER, _startLayer->getNowTime());
        userDefalt->setIntegerForKey(UserDefaultManager::KEY::TIME_OF_RACE_START, (int)time(NULL));
        userDefalt->flush();

        _startLayer->setTryCose(_masterCourseId,
                                teamStatus,
                                _leaderPlayerCharacterId,
                                _playerCharacters
                                );
        _startLayer->showLayer(ST_RACE);
    }), NULL));
}

void DecisionRace::onRasponesSetTry()
{
    delete progress;
    delete request;
    progress = 0;
    request = 0;
    goToRace();
}
