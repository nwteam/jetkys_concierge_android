#ifndef __syanago__StartSelectMapLayer__
#define __syanago__StartSelectMapLayer__

#include "create_func.h"
#include "cocos2d.h"
#include "StartLayer.h"
#include "SelectArea.h"
#include "DialogView.h"

using namespace cocos2d;

enum swipeDirection{
    LEFT_SWIPE,
    RIGHT_SWIPE
};

class StartSelectMapLayer : public cocos2d::Layer, public create_func<StartSelectMapLayer>, public DialogDelegate
{
public:
    bool init(StartLayer* startLayer, int mapGroupId, int mapId, int areaId);
    using create_func::create;
    
    StartSelectMapLayer();
    ~StartSelectMapLayer();
    
    void setInitialMapGroup(int mapGroupId, int mapId, int areaId);
    
    void onTapBackButton();
    void onTapAreaButton(int areaId);
    void onVisibleFooter(const bool isVisible);
    
    StartLayer* _startLayer;
private:
    enum TAG_SPRITE{
        SEA = 0,
        MAP_LAYER,
        ANIMATION_LAYER
    };
    enum Z_ORDER{
        Z_MAP_LAYER = 1,
        Z_MAP_ANIMATION_LAYER = 3,
        Z_HEADLIN,
        Z_EVENT_BUTTON,
        Z_SELECT_AREA = 100,
        Z_DIALOG,
    };
    enum MAP_LAYER_Z_ORDER{
        Z_SEA = 1,
        Z_AREA_CURSOR = 10,
    };
    enum SEA_Z_ORDER{
        Z_MAP = 2,
        Z_MAP_SIGN,
    };
    
    void showTitle();
    void showEventButton();
    
    void showMap(int mapGroupId, int mapId, int areaId, bool showAreaFlag);
    void showSea(int mapGroupId);
    void showCursor();
    Sequence* getCursorAnimation(int positionX, int moveTo);
    void showSelectArea(int mapId, int areaId);
    void showAreaMap(int mapId);
    
    
    void onTapEventMapMenuButton(Ref* pSender);
    
    void onTapMapIcon(Ref* pSender);

    void changeMapImage();
    int checkNextMapGrope(int mapGropeId);
    int checkBackMapGrope(int mapGropeId);
    bool onTouchBegan(cocos2d::Touch* touch,cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event){};
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    void swipeGesture(swipeDirection direction);
    void onSwipeEnded();
    
    int _currentAreaGroupId;
    std::vector<int> _vectorMapId;
    
    int _mapGropeId;
    int _mapId;
    int _areaId;
    
    Point _touchPoint;
    //SelectAreaを上に表示した時の処理
    bool _touchFlag;
};

#endif /* defined(__syanago__StartSelectMapLayer__) */
