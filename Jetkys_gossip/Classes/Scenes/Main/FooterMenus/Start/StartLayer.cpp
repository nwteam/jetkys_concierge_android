#include "StartLayer.h"
#include "MainScene.h"
#include "MapScene.h"
#include "DecisionRace.h"
#include "RaceScene.h"
#include "StartSelectMapLayer.h"
#include "AreaModel.h"
#include "MapModel.h"


StartLayer::StartLayer():
    _tryArea(0), _startIndex(ST_MAIN), _totalTeamStatus(nullptr), _helpPlayer(nullptr)
{
    _enemyCharacterModels.clear();
    _playerCharacters.clear();
}

bool StartLayer::init()
{
    if (!Layer::init()) {
        return false;
    }
    // 助っ人選択画面用の背景
    Size winSize = Director::getInstance()->getWinSize();
    auto background = Sprite::create("home_bg.png");
    background->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(background, -1);

    return true;
}

// 初期化
void StartLayer::initWithPlayerController()
{
    int mapGroup = 1;
    int mapId = 0;
    if (PLAYERCONTROLLER->_player != NULL) {
        // 前回終了時のcourse_idからmap_idとmst_maps.group_idを取得
        const int courseId = PLAYERCONTROLLER->_player->getCurrentCourseId();
        if (courseId > 0) {
            std::shared_ptr<CourseModel>courseModel = CourseModel::find(courseId);
            if (courseModel != nullptr) {
                mapId = courseModel->getMapID();
                std::shared_ptr<AreaModel>areaModel = AreaModel::find(courseModel->getAreaID());
                if (areaModel != NULL) {
                    if (areaModel->getAreaType() == 1) {
                        mapGroup = MapModel::find(mapId)->getGroupId();
                    }
                }
            }
        }
    }
    setMapGroupId(mapGroup, mapId);
}

void StartLayer::showLayer(StartIndex nextLayer)
{
    auto currentLayer = getChildByTag(_startIndex);

    _startIndex = nextLayer;

    switch (_startIndex) {
    case ST_MAIN: {
        addChild(StartSelectMapLayer::create(this, _mapGroupId, _mapId, _tryArea));
        _mainScene->footerLayer->setVisible(true);
        break;
    }
    case ST_MAP: {
        addChild(MapScene::create(this, _tryArea, _mainScene->isMainTutorial()));
        _mainScene->footerLayer->setVisible(true);
        break;
    }
    case ST_SELECT: {
        auto layer = SelectFriend::create();
        if (_mainScene->isMainTutorial()) {
            frLayer = layer;
        }
        addChild(layer);
        layer->setTag(ST_SELECT);
        layer->_startLayer = this;
        layer->createFriendList(_trycoseId);
        _mainScene->footerLayer->setVisible(false);
        break;
    }
    case ST_DECISION: {
        addChild(DecisionRace::create(this, _tryfriendExercise, _tryfriendReaction, _tryfriendDecision, _trycoseId, _helpPlayer->getCharacterId(), _helpPlayer->getIsFriend()));
        _mainScene->footerLayer->setVisible(false);
        break;
    }
    case ST_RACE: {
        if (_tryGridCell != NULL) {
            Director::getInstance()->getTextureCache()->removeAllTextures();
            auto raceScene = RaceScene::create(CourseModel::find(_trycoseId), _tryGridCell, _playerCharacters, _totalTeamStatus, _helpPlayer,  _enemyCharacterModels);
            Scene* scene = Scene::create();
            scene->addChild(raceScene);
            _mainScene->releaseMesod();
            transitScene(scene);
        } else {
            ////CCLOG("no cell in response");
        }
        break;
    }
    default:
        break;
    }
    if (currentLayer) {
        currentLayer->removeFromParent();
    }
}

void StartLayer::startTutorial()
{
    resetPageTag();
    setTryArea(1);
    showLayer(ST_MAP);
    setVisible(true);
}

void StartLayer::setCose(int coseId, std::string coseName, std::string coseInfo)
{
    _trycoseId = coseId;
    _trycoseName = coseName;
    _trycoseInfo = coseInfo;
}

void StartLayer::setFriend(int exercise, int Reaction, int Decision, HelpPlayer* helpPlayer)
{
    if (_helpPlayer != nullptr) {
        delete _helpPlayer;
        _helpPlayer = nullptr;
    }
    _helpPlayer = helpPlayer;

    _tryfriendExercise = exercise;
    _tryfriendReaction = Reaction;
    _tryfriendDecision = Decision;
}

void StartLayer::setTryCose(int coseId, TeamStatus* teamStatus, int tryLeader, std::vector<std::shared_ptr<const CharacterModel> >playerCharacters)
{
    _trycoseId = coseId;
    _tryLeaderID = tryLeader;

    if (_totalTeamStatus != nullptr) {
        delete _totalTeamStatus;
        _totalTeamStatus = nullptr;
    }
    _totalTeamStatus = teamStatus;

    if (_playerCharacters.size() > 0) {
        _playerCharacters.clear();
    }
    std::copy(playerCharacters.begin(), playerCharacters.end(), std::back_inserter(_playerCharacters));
}

void StartLayer::setJsonRespon(Json* grid, std::vector<std::shared_ptr<const CharacterModel> >enemyCharacterModels)
{
    _tryGridCell = grid;
    if (_enemyCharacterModels.size() > 0) {
        _enemyCharacterModels.clear();
    }
    std::copy(enemyCharacterModels.begin(), enemyCharacterModels.end(), std::back_inserter(_enemyCharacterModels));
}

void StartLayer::GoFulePage()
{
    _mainScene->footerLayer->setVisible(true);
    _mainScene->headerLayer->setVisible(true);
    _mainScene->goTofuelShop();
}

void StartLayer::resetPageTag()
{
    removeAllChildren();
    init();
}
int StartLayer::getNowTime()
{
    return _mainScene->headerLayer->getFuleTime();
}

void StartLayer::showFriendCharacter(int playerMstCharacterId, int level, int exercise, int reaction, int decision)
{
    _mainScene->showFriendCharacter(playerMstCharacterId, level, exercise, reaction, decision);
}

void StartLayer::goToRaceEfect()
{
    _mainScene->goToRaceEfectMain();
}

void StartLayer::resetStartTab()
{
    _mainScene->resetStartTab();
}

void StartLayer::setTryArea(int areaId)
{
    _tryArea = areaId;
}

void StartLayer::setMapGroupId(int groupId, int mapId)
{
    if (groupId < 1) {
        return;
    }
    _mapGroupId = groupId;
    _mapId = mapId;
}

void StartLayer::setFooterVisible(const bool isVisible)
{
    _mainScene->footerLayer->setVisible(isVisible);
}
