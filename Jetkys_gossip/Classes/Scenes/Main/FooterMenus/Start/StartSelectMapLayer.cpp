#include "StartSelectMapLayer.h"
#include "FontDefines.h"
#include "MenuTouch.h"
#include "HeaderStatus.h"
#include "MapModel.h"
#include "AreaModel.h"
#include "CourseModel.h"
#include "PlayerController.h"

#define CLOUD_SPEED 0.4

#define CLOUD1_POINT_R Point(winSize.width / 2 - 50, winSize.height / 2)
#define CLOUD2_POINT_R Point(winSize.width / 2 - 200, winSize.height / 2)
#define CLOUD3_POINT_R Point(winSize.width / 2 + 200, winSize.height / 2)
#define CLOUD4_POINT_R Point(winSize.width / 2 + 250, winSize.height / 2)
#define CLOUD1_POINT_L Point(winSize.width / 2 + 50, winSize.height / 2)
#define CLOUD2_POINT_L Point(winSize.width / 2 + 200, winSize.height / 2)
#define CLOUD3_POINT_L Point(winSize.width / 2 - 200, winSize.height / 2)
#define CLOUD4_POINT_L Point(winSize.width / 2 - 250, winSize.height / 2)

#define CLOUD1_DELTA 220
#define CLOUD2_DELTA 300
#define CLOUD3_DELTA 250
#define CLOUD4_DELTA 320

StartSelectMapLayer::StartSelectMapLayer():
    _touchFlag(false),
    _currentAreaGroupId(0){}

StartSelectMapLayer::~StartSelectMapLayer(){
    SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_select_area_sea.plist",_currentAreaGroupId).c_str());
    for(int i = 0; i < _vectorMapId.size(); i++){
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_map_sign.plist",_vectorMapId.at(i)).c_str());
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_map_clear.plist",_vectorMapId.at(i)).c_str());
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_map.plist",_vectorMapId.at(i)).c_str());
    }
    SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_next_map_grope_non_clear.plist",_mapGropeId).c_str());
    SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_next_map_grope_clear.plist",_mapGropeId).c_str());
}

bool StartSelectMapLayer::init(StartLayer* startLayer, int mapGroupId, int mapId, int areaId){
    if ( !Layer::init() ) {
        return false;
    }
    _startLayer = startLayer;
    if(mapGroupId < 1){
        mapGroupId = 1;
        mapId = 0;
    }
    _mapGropeId = mapGroupId;
    _mapId = mapId;
    _areaId = areaId;
    
    setTag(StartIndex::ST_MAIN);
    
    showTitle();
    showEventButton();
    showMap(mapGroupId, mapId, areaId, true);
    return true;
}

void StartSelectMapLayer::onTapBackButton()
{
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    _touchFlag = false;
}

void StartSelectMapLayer::showTitle()
{
    auto winSize = Director::getInstance()->getWinSize();
    auto othersTitleBg = makeSprite("head_line_no1.png");
    othersTitleBg->setPosition(Vec2(othersTitleBg->getContentSize().width / 2,
                                    winSize.height - othersTitleBg->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 12));
    addChild(othersTitleBg, Z_ORDER::Z_HEADLIN);
    
    auto title = Label::createWithTTF("マップ選択", FONT_NAME_2, 34);
    title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    title->setAnchorPoint(Vec2(0.0f,0.5f));
    title->setPosition(Point(14,othersTitleBg->getPosition().y - 17));
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(title, Z_ORDER::Z_HEADLIN);
}

void StartSelectMapLayer::showEventButton()
{
    auto winSize = Director::getInstance()->getWinSize();
    auto eventButton = makeMenuItem("start_select_event_button.png", CC_CALLBACK_1(StartSelectMapLayer::onTapEventMapMenuButton, this));
    auto menuEvent = MenuTouch::create(eventButton, NULL);
    menuEvent->setPosition(Vec2(winSize.width - 10 - eventButton->getContentSize().width / 2, winSize.height - 44.5 - HEADER_STATUS::SIZE::HEIGHT));
    addChild(menuEvent, Z_ORDER::Z_EVENT_BUTTON);
}

void StartSelectMapLayer::showMap(int mapGroupId, int mapId, int areaId, bool showAreaFlag)
{
    auto layer = Layer::create();
    layer->setTag(TAG_SPRITE::MAP_LAYER);
    addChild(layer, Z_ORDER::Z_MAP_LAYER);
    
    //前に作ったlayerに使った画像をメモリから削除
    if(_currentAreaGroupId > 0){
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_select_area_sea.plist",_currentAreaGroupId).c_str());
        for(int i = 0; i < _vectorMapId.size(); i++){
            SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_map_sign.plist",_vectorMapId.at(i)).c_str());
            SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_map_clear.plist",_vectorMapId.at(i)).c_str());
            SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_map.plist",_vectorMapId.at(i)).c_str());
        }
    }
    _currentAreaGroupId = mapGroupId;
    _vectorMapId.clear();
    
    showSea(mapGroupId);
    showAreaMap(mapGroupId);
    if(showAreaFlag == true){
        showSelectArea(mapId, areaId);
    }
    
    if(checkNextMapGrope(mapGroupId) > 0 || checkBackMapGrope(mapGroupId) > 0){
        showCursor();
        auto listener = EventListenerTouchOneByOne::create();
        listener->setSwallowTouches(true);
        listener->onTouchBegan = CC_CALLBACK_2(StartSelectMapLayer::onTouchBegan, this);
        listener->onTouchMoved = CC_CALLBACK_2(StartSelectMapLayer::onTouchMoved,this);
        listener->onTouchEnded = CC_CALLBACK_2(StartSelectMapLayer::onTouchEnded, this);
        Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    }
    
    if(UserDefault::getInstance()->getBoolForKey("OPEN_NEXT_MAP_FLAG")){
        int nextMapId = UserDefault::getInstance()->getIntegerForKey("OPEN_NEXT_MAP_ID");
        if(MapModel::find(nextMapId) != NULL && nextMapId > 0){
            auto dialog = DialogView::createNextMapOpenDialog();
            dialog->_delegate = this;
            addChild(dialog, Z_ORDER::Z_DIALOG);
        }
        return;
    }
}

void StartSelectMapLayer::showSea(int mapGroupId)
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_select_area_sea.plist",mapGroupId).c_str());
    
    auto winSize = Director::getInstance()->getWinSize();
    auto mapImage = makeSprite(StringUtils::format("%d_select_area_sea.png",mapGroupId).c_str());
    mapImage->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    mapImage->setTag(TAG_SPRITE::SEA);
    mapImage->setPosition(Point(winSize.width * 0.5,winSize.height - HEADER_STATUS::SIZE::HEIGHT));
    getChildByTag(TAG_SPRITE::MAP_LAYER)->addChild(mapImage, MAP_LAYER_Z_ORDER::Z_SEA);
}

void StartSelectMapLayer::showCursor()
{
    auto winSize = Director::getInstance()->getWinSize();
    if(checkNextMapGrope(_currentAreaGroupId) > 0){
        auto rightImage = makeSprite("start_select_swipe_image_right.png");
        rightImage->setPosition(Point(winSize.width - rightImage->getContentSize().width / 2 - 10,winSize.height / 2));
        rightImage->getPositionX();
        rightImage->runAction(RepeatForever::create(getCursorAnimation(rightImage->getPositionX(), -5)));
        getChildByTag(TAG_SPRITE::MAP_LAYER)->addChild(rightImage, MAP_LAYER_Z_ORDER::Z_AREA_CURSOR);
    }
    
    if(checkBackMapGrope(_currentAreaGroupId) > 0){
        auto leftImage = makeSprite("start_select_swipe_image_left.png");
        leftImage->setPosition(Point(leftImage->getContentSize().width / 2 + 10,winSize.height / 2));
        leftImage->runAction(RepeatForever::create(getCursorAnimation(leftImage->getPositionX(), 5)));
        getChildByTag(TAG_SPRITE::MAP_LAYER)->addChild(leftImage,MAP_LAYER_Z_ORDER::Z_AREA_CURSOR);
    }
}

Sequence* StartSelectMapLayer::getCursorAnimation(int positionX, int moveTo)
{
    auto winSize = Director::getInstance()->getWinSize();
    return Sequence::create(
         CCEaseSineInOut::create(MoveTo::create(0.5,Point(positionX + moveTo, winSize.height / 2))),
         CCEaseSineInOut::create(MoveTo::create(0.5,Point(positionX + moveTo * 3, winSize.height / 2))),
         NULL);
}

void StartSelectMapLayer::onTapEventMapMenuButton(Ref* pSender){
    if(_touchFlag == true){
        return;
    }
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    
    _touchFlag = true;
    auto winSize = Director::getInstance()->getWinSize();
    auto selectArea = SelectArea::create(-1,
                                         CC_CALLBACK_0(StartSelectMapLayer::onTapBackButton, this),
                                         CC_CALLBACK_1(StartSelectMapLayer::onTapAreaButton, this),
                                         CC_CALLBACK_1(StartSelectMapLayer::onVisibleFooter, this));
    selectArea->setPosition(Point::ZERO);
    addChild(selectArea, Z_ORDER::Z_SELECT_AREA);
    _mapId = 0;
}

void StartSelectMapLayer::showSelectArea(int mapId, int areaId)
{
    std::shared_ptr<MapModel>mapModel(MapModel::find(mapId));
    if(mapId > 0 && mapModel != NULL)
    {
        std::shared_ptr<AreaModel>areaModel(AreaModel::find(areaId));
        if(areaId < 1 && areaModel == NULL)
        {
            _touchFlag = false;
            return;
        }
        _touchFlag = true;
        bool eventAreaFlag = false;
        if(areaModel->getAreaType() == 2){
            eventAreaFlag = true;
        }
        SelectArea* selectArea;
        if(eventAreaFlag){
            selectArea = SelectArea::create(-1,
                                            CC_CALLBACK_0(StartSelectMapLayer::onTapBackButton, this),
                                            CC_CALLBACK_1(StartSelectMapLayer::onTapAreaButton, this),
                                            CC_CALLBACK_1(StartSelectMapLayer::onVisibleFooter, this));
        }else{
            selectArea = SelectArea::create(mapId,
                                            CC_CALLBACK_0(StartSelectMapLayer::onTapBackButton, this),
                                            CC_CALLBACK_1(StartSelectMapLayer::onTapAreaButton, this),
                                            CC_CALLBACK_1(StartSelectMapLayer::onVisibleFooter, this));
        }
        selectArea->setPosition(Point::ZERO);
        addChild(selectArea, Z_ORDER::Z_SELECT_AREA);
    }
}

void StartSelectMapLayer::showAreaMap(int mapGroupId)
{
    auto mapList = MenuTouch::create();
    mapList->setPosition(Vec2::ZERO);
    getChildByTag(TAG_SPRITE::MAP_LAYER)->getChildByTag(TAG_SPRITE::SEA)->addChild(mapList, SEA_Z_ORDER::Z_MAP_SIGN);

    Sprite* mapImage;
    
    auto winSize = Director::getInstance()->getWinSize();
    bool allMapOpenFlag = false;
    auto mapGroupModels = MapModel::findMapGroupId(mapGroupId);
    for(int i = 0; i < mapGroupModels.size(); i++){
        std::shared_ptr<MapModel>mapModel(mapGroupModels.at(i));
        if(mapModel == NULL){
            continue;
        }
        if(mapGroupId != mapModel->getGroupId()){
            continue;
        }
        int mapId = mapModel->getID();
        _vectorMapId.push_back(mapId);
        
        int areaId = AreaModel::getFirstAreaId(mapId);
        std::shared_ptr<AreaModel>areaModel(AreaModel::find(areaId));
        int preAreaId = areaModel->getPrevId();
        int courseId = CourseModel::getFirstCourseIdRemoveEventAndTest(areaId);
        int preCourseId = CourseModel::getLastCourseId(preAreaId);
        
        if(PLAYERCONTROLLER->isCleared(courseId) ||
           PLAYERCONTROLLER->isCleared(preCourseId) ||
           preAreaId == 0){
            std::shared_ptr<MapModel>mapModel(MapModel::find(mapId));
            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_map_sign.plist",mapModel->getID()).c_str());
            std::string signName;
            if(PLAYERCONTROLLER->getUncompleteMissionCourses()->isPlaceOfMissionWithMapId(mapModel->getID()) == true)
            {
                signName = StringUtils::format("%d_map_sign_with_mission.png",mapModel->getID());
            }
            else
            {
                signName = StringUtils::format("%d_map_sign.png",mapModel->getID());
            }
            auto mapPoint = makeMenuItem(signName.c_str(), CC_CALLBACK_1(StartSelectMapLayer::onTapMapIcon, this));
            mapPoint->setPosition(Point(mapModel->getPositionX(),mapModel->getPositionY()));
            mapPoint->setTag(mapId);
            mapList->addChild(mapPoint);
            
            auto couresModels = CourseModel::findMapId(mapId);
            int maxBage = 0;
            int getBage = 0;
            for(int j = 0; j < couresModels.size(); j++){
                std::shared_ptr<CourseModel>courseModel(couresModels.at(j));
                if(courseModel == NULL){
                    continue;
                }
                if(courseModel->getCourseType() == 0){
                    continue;
                }
                if(courseModel->getCourseType() == 3){
                    continue;
                }
                std::shared_ptr<AreaModel>areaModel(AreaModel::find(courseModel->getAreaID()));
                if(areaModel->getAreaType() != 1){
                    continue;
                }
                maxBage += 1;
                if(PLAYERCONTROLLER->getClearCourseBage(courseModel->getID()) > 0){
                    getBage +=PLAYERCONTROLLER->getClearCourseBage(courseModel->getID());
                }
            }
            if(maxBage == getBage){
                SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_map_clear.plist",mapId).c_str());
                mapImage = makeSprite(StringUtils::format("%d_map_clear.png",mapId).c_str());
            }else{
                SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_map.plist",mapId).c_str());
                mapImage = makeSprite(StringUtils::format("%d_map.png",mapId).c_str());
            }
        } else {
            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_map_non_clear.plist",mapId).c_str());
            mapImage = makeSprite(StringUtils::format("%d_map_non_clear.png",mapId).c_str());
            allMapOpenFlag = true;
        }
        mapImage->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
        mapImage->setPosition(Point(winSize.width * 0.5, getChildByTag(TAG_SPRITE::MAP_LAYER)->getChildByTag(TAG_SPRITE::SEA)->getContentSize().height));
        getChildByTag(TAG_SPRITE::MAP_LAYER)->getChildByTag(TAG_SPRITE::SEA)->addChild(mapImage, SEA_Z_ORDER::Z_MAP);
    }
    
    if(allMapOpenFlag){
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_next_map_grope_non_clear.plist",mapGroupId).c_str());
        mapImage = makeSprite(StringUtils::format("%d_next_map_grope_non_clear.png",mapGroupId).c_str());
    } else {
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_next_map_grope_clear.plist",mapGroupId).c_str());
        mapImage = makeSprite(StringUtils::format("%d_next_map_grope_clear.png",mapGroupId).c_str());
    }
    mapImage->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    mapImage->setPosition(Point(winSize.width * 0.5, getChildByTag(TAG_SPRITE::MAP_LAYER)->getChildByTag(TAG_SPRITE::SEA)->getContentSize().height));
    getChildByTag(TAG_SPRITE::MAP_LAYER)->getChildByTag(TAG_SPRITE::SEA)->addChild(mapImage, SEA_Z_ORDER::Z_MAP);
}

void StartSelectMapLayer::onTapMapIcon(Ref* pSender){
    if(_touchFlag == true){
        return;
    }
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    auto item = (MenuItem*) pSender;
    int mapId = item->getTag();
    
    _touchFlag = true;
    auto winSize = Director::getInstance()->getWinSize();
    _mapId = mapId;
    auto areaMenu = SelectArea::create(mapId,
                                       CC_CALLBACK_0(StartSelectMapLayer::onTapBackButton, this),
                                       CC_CALLBACK_1(StartSelectMapLayer::onTapAreaButton, this),
                                       CC_CALLBACK_1(StartSelectMapLayer::onVisibleFooter, this));
    addChild(areaMenu, Z_ORDER::Z_SELECT_AREA);
    areaMenu->setPosition(Point::ZERO);
}

void StartSelectMapLayer::onTapAreaButton(int areaId)
{
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    removeFromParent();
    _startLayer->setTryArea(areaId);
    _startLayer->showLayer(ST_MAP);
}

void StartSelectMapLayer::onVisibleFooter(const bool isVisible)
{
    _startLayer->setFooterVisible(isVisible);
}

//use if Player can play next area
int StartSelectMapLayer::checkNextMapGrope(int mapGroupId){
    int result = -1;
    int tempNextMapGrope = mapGroupId + 1;
    if(tempNextMapGrope < 1){
        return result;
    }
    
    int mapId = MapModel::getFirstMapId(tempNextMapGrope);
    if(mapId < 1){
        return -1;
    }
    int areaId = AreaModel::getFirstAreaId(mapId);
    if(areaId < 1){
        return -1;
    }
    int courseId = CourseModel::getFirstCourseIdRemoveEventAndTest(areaId);
    if(courseId < 1){
        return -1;
    }
    if(PLAYERCONTROLLER->isCleared(courseId)){
        result = tempNextMapGrope;
    }
    std::shared_ptr<AreaModel>model(AreaModel::find(areaId));
    int preAreaId = model->getPrevId();
    int preCourseId = CourseModel::getLastCourseId(preAreaId);
    if(PLAYERCONTROLLER->isCleared(preCourseId)){
        result = tempNextMapGrope;
    }
    return result;
}

int StartSelectMapLayer::checkBackMapGrope(int mapGroupeId)
{
    int result = -1;
    int tempNextMapGrope = mapGroupeId - 1;
    if(tempNextMapGrope < 1){
        return result;
    }
    
    int mapId = MapModel::getLastMapId(tempNextMapGrope);
    if(mapId < 1){
        return -1;
    }
    int areaId = AreaModel::getLastAreaId(mapId);
    if(areaId < 1){
        return -1;
    }
    int courseId = CourseModel::getLastCourseId(areaId);
    if(courseId < 1){
        return -1;
    }
    if(PLAYERCONTROLLER->isCleared(courseId)){
        result = tempNextMapGrope;
    }
    return result;
}

bool StartSelectMapLayer::onTouchBegan(cocos2d::Touch* touch,cocos2d::Event* event)
{
    if(_touchFlag){
        return false;
    }
    _touchPoint = touch->getLocation();
    return true;
}

void StartSelectMapLayer::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    if(_touchFlag){
        return;
    }
    _touchFlag = true;
    auto tempTouchPoint = touch->getLocation();
    //スワイプした距離設定
    if(abs(_touchPoint.x - tempTouchPoint.x) < 100){
        _touchFlag = false;
        return;
    }
    if((tempTouchPoint.x - _touchPoint.x) > 0){
        swipeGesture(LEFT_SWIPE);
    }else{
        swipeGesture(RIGHT_SWIPE);
    }
}

void StartSelectMapLayer::swipeGesture(swipeDirection direction)
{
    
    auto winSize = Director::getInstance()->getWinSize();
    auto animationLayer = Layer::create();
    animationLayer->setTag(TAG_SPRITE::ANIMATION_LAYER);
    addChild(animationLayer, Z_ORDER::Z_MAP_ANIMATION_LAYER);
    
    auto cloud1 = makeSprite("cloud_big.png");
    animationLayer->addChild(cloud1);
    auto cloud2 = makeSprite("cloud_small.png");
    animationLayer->addChild(cloud2);
    
    auto cloud3 = makeSprite("cloud_invisible_1.png");
    animationLayer->addChild(cloud3);
    auto cloud4 = makeSprite("cloud_invisible_2.png");
    animationLayer->addChild(cloud4);
    
    switch (direction) {
        case RIGHT_SWIPE:{
            
            int tempMapGropeId = checkNextMapGrope(_mapGropeId);
            if(tempMapGropeId < 0){
                onSwipeEnded();
                return;
            }
            
            cloud1->setPosition(CLOUD1_POINT_R + Vec2(winSize.width + CLOUD1_DELTA, 0));
            cloud2->setPosition(CLOUD2_POINT_R + Vec2(winSize.width + CLOUD2_DELTA, 0));
            cloud3->setPosition(CLOUD3_POINT_R + Vec2(winSize.width + CLOUD3_DELTA, 0));
            cloud4->setPosition(CLOUD4_POINT_R + Vec2(winSize.width + CLOUD4_DELTA, 0));
            
            _mapGropeId = tempMapGropeId;
            _startLayer->setMapGroupId(_mapGropeId,0);
            cloud1->runAction(Sequence::create(CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD1_POINT_R)),
                                               CallFunc::create(CC_CALLBACK_0(StartSelectMapLayer::changeMapImage,this)),
                                               CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD1_POINT_R - Vec2(winSize.width + CLOUD1_DELTA, 0))),
                                               DelayTime::create(0.5),
                                               CallFunc::create(CC_CALLBACK_0(StartSelectMapLayer::onSwipeEnded,this)),
                                               NULL)
                                         );
            cloud2->runAction(Sequence::create(CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD2_POINT_R)),
                                               CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD2_POINT_R - Vec2(winSize.width + CLOUD2_DELTA, 0))),
                                               NULL)
                              );
            cloud3->runAction(Sequence::create(CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD3_POINT_R)),
                                               CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD3_POINT_R - Vec2(winSize.width + CLOUD3_DELTA, 0))),
                                               NULL)
                              );
            cloud4->runAction(Sequence::create(CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD4_POINT_R)),
                                               CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD4_POINT_R - Vec2(winSize.width + CLOUD4_DELTA, 0))),
                                               NULL)
                              );
        
            break;
        }
        case LEFT_SWIPE:{
            
            int tempMapGropeId = checkBackMapGrope(_mapGropeId);
            if(tempMapGropeId < 0){
                onSwipeEnded();
                return;
            }
            
            cloud1->setPosition(CLOUD1_POINT_L - Vec2(winSize.width + CLOUD1_DELTA, 0));
            cloud2->setPosition(CLOUD2_POINT_L - Vec2(winSize.width + CLOUD2_DELTA, 0));
            cloud3->setPosition(CLOUD3_POINT_L - Vec2(winSize.width + CLOUD3_DELTA, 0));
            cloud4->setPosition(CLOUD4_POINT_L - Vec2(winSize.width + CLOUD4_DELTA, 0));
            
            _mapGropeId = tempMapGropeId;
            _startLayer->setMapGroupId(_mapGropeId,0);
            cloud1->runAction(Sequence::create(CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD1_POINT_L)),
                                               CallFunc::create(CC_CALLBACK_0(StartSelectMapLayer::changeMapImage,this)),
                                               CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD1_POINT_L + Vec2(winSize.width + CLOUD1_DELTA, 0))),
                                               DelayTime::create(0.5),
                                               CallFunc::create(CC_CALLBACK_0(StartSelectMapLayer::onSwipeEnded,this)),
                                                          NULL)
                                         );
            cloud2->runAction(Sequence::create(CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD2_POINT_L)),
                                               CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD2_POINT_L + Vec2(winSize.width + CLOUD2_DELTA, 0))),
                                               NULL)
                              );
            cloud3->runAction(Sequence::create(CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD3_POINT_L)),
                                               CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD3_POINT_L + Vec2(winSize.width + CLOUD3_DELTA, 0))),
                                               NULL)
                              );
            cloud4->runAction(Sequence::create(CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD4_POINT_L)),
                                               CCEaseSineInOut::create(MoveTo::create(CLOUD_SPEED / 2, CLOUD4_POINT_L + Vec2(winSize.width + CLOUD4_DELTA, 0))),
                                               NULL)
                              );
        
            break;
        }
        default:
            break;
    }
}

void StartSelectMapLayer::onSwipeEnded(){
    removeChildByTag(TAG_SPRITE::ANIMATION_LAYER);
    _touchFlag = false;
}

void StartSelectMapLayer::changeMapImage(){
    removeChildByTag(TAG_SPRITE::MAP_LAYER);
    showMap(_mapGropeId, _mapId, _areaId, false);
}






