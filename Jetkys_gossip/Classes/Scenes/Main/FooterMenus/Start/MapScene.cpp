#include "MapScene.h"
#include "MainScene.h"
#include "StartLayerCheckPlayerTryCourseModel.h"
#include "RankingLayer.h"
#include "SoundHelper.h"
#include "GetFuelDialog.h"
#include "TweetMessage.h"

#include "AreaModel.h"
#include "MapModel.h"
#include "CourseModel.h"


USING_NS_CC;

bool MapScene::init(StartLayer* startLayer, int areaId, bool isTutorial)
{
    if (!Layer::init()) {
        return false;
    }
    setTag(StartIndex::ST_MAIN);

    _startLayer = startLayer;
    _areaId = areaId;

    auto mapLayer = MapLayer::create(this, areaId);
    mapLayer->setScrollEnable(!isTutorial);
    addChild(mapLayer);


    std::shared_ptr<AreaModel>areaModel(AreaModel::find(areaId));
    showHeadLine(this, areaModel->getName(), CC_CALLBACK_1(MapScene::onTapBackButton, this));
    showRankingButton();
    return true;
}

void MapScene::showRankingButton()
{
    Sprite* rankingSelected = Sprite::create("button_orange.png");
    rankingSelected->setColor(Color3B::GRAY);

    MenuItem* rankingMenuItem = MenuItemSprite::create(Sprite::create("button_orange.png"),
                                                       rankingSelected,
                                                       [&](Ref* pSender) {
        SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        auto layer = RankingLayer::create(CC_CALLBACK_1(MapScene::onCallBackRanking, this));
        layer->setTag(TAG_SPRITE::RANKING_LAYER);
        addChild(layer, 13);
        _startLayer->_mainScene->footerLayer->setVisible(false);
    });
    Menu* rankingMenu = Menu::create(rankingMenuItem, NULL);
    rankingMenu->setPosition(Director::getInstance()->getWinSize().width - 90, Director::getInstance()->getWinSize().height - rankingSelected->getContentSize().height / 2 - 12 - HEADER_STATUS::SIZE::HEIGHT);
    addChild(rankingMenu);

    Label* rankingLabel = Label::createWithTTF("ランキング", FONT_NAME_2, 28);
    rankingLabel->enableShadow();
    rankingLabel->setPosition(rankingMenu->getPosition().x, rankingMenu->getPosition().y - 14);
    addChild(rankingLabel);
}

void MapScene::onCallBackRanking(Ref* pSender)
{
    _startLayer->_mainScene->footerLayer->setVisible(true);
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    getChildByTag(TAG_SPRITE::RANKING_LAYER)->removeFromParent();
}

void MapScene::btSelectCose(int coseId)
{
    std::shared_ptr<CourseModel>courseModel(CourseModel::find(coseId));
    if (courseModel->getCourseType() == 0) {
        return;
    }
    bool tryOk = checkTryRace(coseId);
    if (tryOk == true) {
        _coseId = coseId;
        showDialogView(CHECK_COSE_APPLY, coseId);
    }
}

bool MapScene::checkTryRace(int tag)
{
    // 1,コースに挑戦できるかのチェック(イベントコースかそうでないか？)
    auto model = StartLayerCheckPlayerTryCourseModel::createCourseChecker(tag);
    return model->getCourseTryStatus();
}

void MapScene::nextScene()
{
    _startLayer->setCose(_coseId, _coseName, _coseInfo);
    std::shared_ptr<AreaModel>areaModel(AreaModel::find(_areaId));
    int mapId = areaModel->getMapID();
    std::shared_ptr<MapModel>mapModel(MapModel::find(mapId));
    _startLayer->setMapGroupId(mapModel->getGroupId(), mapId);
    _startLayer->showLayer(ST_SELECT);
    removeFromParent();
}


void MapScene::showDialogView(int dialogKind, int courseId)
{
    std::string titile;
    std::string message;
    std::shared_ptr<CourseModel>courseModel = CourseModel::find(courseId);
    std::string coseName = courseModel->getName();
    std::string coseInfo = courseModel->getDescription();
    SoundHelper* soundManeger;
    soundManeger->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    switch (dialogKind) {
    case CHECK_COSE_APPLY:
        titile = coseName + " " + coseInfo;
        _coseName = coseName;
        _coseInfo = coseInfo;
        ////CCLOG("コース名: %s", coseName.c_str());
        message = "";

        break;
    case CHECK_FUEL:
        titile = "燃料不足";
        message = "ガソリンスタンドで給油するか、\n１日１回だけTwitterで給油できます！";
        break;
    default:
        titile = "エラー";
        message = "予期せぬエラーが発生しました";
        break;
    }
    if (dialogKind == CHECK_COSE_APPLY) {
        auto tweetMessage = TweetMessage::getFuelTweetMessage(courseId, PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]->getCharactersId());
        _dialog = DialogView::createWithCoseDialog(message, titile, courseId);
        addChild(_dialog);
        _dialogKind = dialogKind;
        _dialog->_delegate = this;
    } else {
        auto dialog = GetFuelDialog::create(
            CC_CALLBACK_0(MapScene::onTweetAndGetFuelEnded, this),
            CC_CALLBACK_2(MapScene::onGetFuelByTokenTouched, this),
            TweetMessage::getFuelTweetMessage(courseId, PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]->getCharactersId()),
            message,
            titile);
        addChild(dialog);
    }
}

void MapScene::onGetFuelByTokenTouched(cocos2d::Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        std::shared_ptr<AreaModel>areaModel(AreaModel::find(_areaId));
        int mapId = areaModel->getMapID();
        std::shared_ptr<MapModel>mapModel(MapModel::find(mapId));
        _startLayer->setMapGroupId(mapModel->getGroupId(), mapId);
        _startLayer->GoFulePage();
    }
}

void MapScene::onTweetAndGetFuelEnded()
{
    _startLayer->_mainScene->headerLayer->updateInfoLayer();
}

void MapScene::btDialogCallback(ButtonIndex aIndex)
{
    _startLayer->resetStartTab();
    if (aIndex == BT_YES) {
        switch (_dialogKind) {
        case CHECK_COSE_APPLY:
            // レース画面へいく
            ////CCLOG("助っ人選択画面へ続く");

            // need to add check Fuel here
            if (checkPlayerFuel(_coseId)) {
                nextScene();
            }
            break;
        case CHECK_FUEL:
            ////CCLOG("給油画面へ続く");
            getfule();
            // 給油画面へ
            break;
        default:
            break;
        }
    }
}

void MapScene::getfule()
{
    std::shared_ptr<AreaModel>areaModel(AreaModel::find(_areaId));
    int mapId = areaModel->getMapID();
    std::shared_ptr<MapModel>mapModel(MapModel::find(mapId));
    _startLayer->setMapGroupId(mapModel->getGroupId(), mapId);
    _startLayer->GoFulePage();
}

void MapScene::onTapBackButton(cocos2d::Ref* pSender)
{
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    if (_startLayer->_mainScene->isMainTutorial()) {
        return;
    }
    if (_startLayer->_mainScene->_isShowing) {
        return;
    }
    std::shared_ptr<AreaModel>areaModel(AreaModel::find(_areaId));
    if (areaModel->getAreaType() != 2) {
        int mapId = areaModel->getMapID();
        std::shared_ptr<MapModel>mapModel(MapModel::find(mapId));
        _startLayer->setMapGroupId(mapModel->getGroupId(), mapId);
    }
    _startLayer->showLayer(ST_MAIN);
}

bool MapScene::checkPlayerFuel(int courseId)
{
    // 2,燃料が足りているかのチェック
    std::shared_ptr<CourseModel>courseModel = CourseModel::find(courseId);
    if (courseModel->getCourseType() == 4) {
        return true;
    }
    int coseTryFuel = courseModel->getRequiredFuel();
    int playerFuel = PLAYERCONTROLLER->_player->getFuel();
    // for test GetFuelButton;
    if (playerFuel < coseTryFuel) {
        ////CCLOG("燃料が足りません:現在の燃料: %d",playerFuel);
        showDialogView(CHECK_FUEL, courseId);
        return false;
    }
    return true;
}