#ifndef __syanago__SelectFriendView__
#define __syanago__SelectFriendView__

#include "cocos2d.h"
#include "SelectFriendModel.h"

USING_NS_CC;

class SelectFriendView : public Sprite
{
public:
    SelectFriendView();
    ~SelectFriendView();
    
    static SelectFriendView* create(const std::shared_ptr<SelectFriendModel::HelpPlayer>& helpPlayerModel);
private:
    enum TAG_SPRITE{
        
    };
    enum Z_ORDER{
        
    };
    void showStatus(const std::shared_ptr<SelectFriendModel::HelpPlayer>& helpPlayerModel);
    
    
};

#endif
