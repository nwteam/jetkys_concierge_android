#include "SelectFriendView.h"
#include "FontDefines.h"
#include "jCommon.h"

SelectFriendView::SelectFriendView()
{}

SelectFriendView::~SelectFriendView()
{}

SelectFriendView*  SelectFriendView::create(const std::shared_ptr<SelectFriendModel::HelpPlayer> &helpPlayerModel)
{
    SelectFriendView* sprite = new SelectFriendView();
    if (sprite && sprite->initWithFile("itemFrBgr.png")) {
        sprite->showStatus(helpPlayerModel);
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

void SelectFriendView::showStatus(const std::shared_ptr<SelectFriendModel::HelpPlayer>& helpPlayerModel)
{
    auto name = Label::createWithTTF(helpPlayerModel->getName().c_str(), FONT_NAME_2, 25);
    name->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    name->setColor(COLOR_YELLOW);
    
    auto icon = makeSprite(StringUtils::format("%d_i.png", helpPlayerModel->getMasterCharacterId()).c_str());
    
    
    auto playerRankLabel = Label::createWithTTF(StringUtils::format("ランク %zd", helpPlayerModel->getOwnerRank()).c_str(), FONT_NAME_2, 25);
    playerRankLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto sendPointLabel = Label::createWithTTF(StringUtils::format("%zd フレンドポイント", helpPlayerModel->getTransferFriendPoint()).c_str(), FONT_NAME_2, 25);
    sendPointLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %zd", helpPlayerModel->getLevel()).c_str(), FONT_NAME_2, 18);
    levelLabel->enableOutline(Color4B(0, 0, 0, 255), 2);
    // levelLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto exerciseLabel = Label::createWithTTF(("馬力 " + FormatWithCommas(helpPlayerModel->getPower())).c_str(), FONT_NAME_2, 18);
    exerciseLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto reactionLabel = Label::createWithTTF(("技量 " + FormatWithCommas(helpPlayerModel->getTechnique())).c_str(), FONT_NAME_2, 18);
    reactionLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto decisionLabel = Label::createWithTTF(("忍耐 " + FormatWithCommas(helpPlayerModel->getBrake())).c_str(), FONT_NAME_2, 18);
    decisionLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    
    
    icon->setPosition(icon->getContentSize().width / 2 + 12, getContentSize().height / 2);
    addChild(icon);
    
    name->setAnchorPoint(Point(0, 0.5));
    name->setPosition(Point(icon->getPositionX() + icon->getContentSize().width / 2 + 12, getContentSize().height * 0.82 - 14.5));
    addChild(name, 2);
    
    exerciseLabel->setAnchorPoint(Point(0, 0.5));
    exerciseLabel->setPosition(Point(name->getPositionX(), name->getPositionY() - name->getContentSize().height / 2 - 5.0));
    reactionLabel->setAnchorPoint(Point(0, 0.5));
    reactionLabel->setPosition(Point(exerciseLabel->getPositionX(), exerciseLabel->getPositionY() - exerciseLabel->getContentSize().height / 2 - 3.0));
    decisionLabel->setAnchorPoint(Point(0, 0.5));
    decisionLabel->setPosition(Point(reactionLabel->getPositionX(), reactionLabel->getPositionY() - reactionLabel->getContentSize().height / 2 - 3.0));
    addChild(exerciseLabel);
    addChild(reactionLabel);
    addChild(decisionLabel);
    
    levelLabel->setAnchorPoint(Point(0.5, 0.5));
    levelLabel->setPosition(Point(icon->getPositionX(), levelLabel->getContentSize().height / 2 + 5.0 - 18));
    playerRankLabel->setAnchorPoint(Point(1.0, 0.5));
    playerRankLabel->setPosition(Point(getContentSize().width - 5.0 - 10, name->getPositionY()));
    sendPointLabel->setAnchorPoint(Point(1.0, 0.5));
    sendPointLabel->setPosition(Point(getContentSize().width - 5.0 - 10, decisionLabel->getPositionY()));
    Sprite* friendFlagImg;
    if (helpPlayerModel->getIsFriend() == true) {
        sendPointLabel->setColor(COLOR_ORANGEE);
        friendFlagImg = Sprite::create("friend.png");
    } else {
        friendFlagImg = Sprite::create("guest.png");
    }
    
    friendFlagImg->setAnchorPoint(Point(1.0, 0.5));
    friendFlagImg->setPosition(Point(getContentSize().width - 5.0 - 10, (decisionLabel->getPositionY() + playerRankLabel->getPositionY()) * 0.5 + 12.5));
    addChild(levelLabel);
    addChild(playerRankLabel);
    addChild(sendPointLabel);
    addChild(friendFlagImg);

}
