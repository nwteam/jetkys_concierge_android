#include "SelectFriendModel.h"

SelectFriendModel::SelectFriendModel(Json* response)
{
    Json* data = Json_getItem(Json_getItem(response, "get_race_course"), "data");


    _helpPlayers.clear();
    Json* helpPlayer = Json_getItem(data, "help_player");
    if (helpPlayer) {
        for (Json* child = helpPlayer->child; child; child = child->next) {
            std::shared_ptr<HelpPlayer>helpPlayerModel(new HelpPlayer(child));
            _helpPlayers.push_back(helpPlayerModel);
        }
    }

    Json* enemyCharacterIds = Json_getItem(data, "characters");
    if (enemyCharacterIds) {
        if (enemyCharacterIds->size > 0) {
            for (Json* child = enemyCharacterIds->child; child; child = child->next) {
                std::string id = child->valueString;
                _enemyCharacterModels.push_back(CharacterModel::find(atoi(id.c_str())));
            }
        }
    }
    _gridData = Json_getItem(data, "grid");
    _isEventCourse = atoi(Json_getString(data, "event_id", 0)) > 0;
}

SelectFriendModel::~SelectFriendModel()
{}

SelectFriendModel::HelpPlayer::HelpPlayer(Json* friendJson)
{
    _playerId = atoi(Json_getString(friendJson, "player_id", ""));
    _name = Json_getString(friendJson, "player_name", "");
    _ownerRank = atoi(Json_getString(friendJson, "rank", ""));
    _masterCharacterId = atoi(Json_getString(friendJson, "mst_characters_id", ""));
    _level = atoi(Json_getString(friendJson, "level", ""));
    _power = atoi(Json_getString(friendJson, "exercise", ""));
    _technique = atoi(Json_getString(friendJson, "reaction", ""));
    _brake = atoi(Json_getString(friendJson, "decision", ""));

    _isFriend = atoi(Json_getString(friendJson, "friend_flag", "")) > 0;
    _transferFriendPoint = atoi(Json_getString(friendJson, "fp_of_help", ""));
}

SelectFriendModel::HelpPlayer::~HelpPlayer()
{}