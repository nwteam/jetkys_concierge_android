#ifndef __syanago__SelectFriendModel__
#define __syanago__SelectFriendModel__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"
#include "CharacterModel.h"

USING_NS_CC;

class SelectFriendModel
{
public:
    SelectFriendModel(Json * response);
    ~SelectFriendModel();
    
    CC_SYNTHESIZE_READONLY(std::vector<std::shared_ptr<const CharacterModel>> , _enemyCharacterModels, EnemyCharacterModels);
    CC_SYNTHESIZE_READONLY(Json*, _gridData, GridData);
    CC_SYNTHESIZE_READONLY(bool, _isEventCourse, IsEventCourse);
    
    class HelpPlayer
    {
    public:
        HelpPlayer(Json* friendJson);
        ~HelpPlayer();
        
        
        CC_SYNTHESIZE_READONLY(int, _playerId, PlayerId);
        CC_SYNTHESIZE_READONLY(std::string, _name, Name);
        CC_SYNTHESIZE_READONLY(int, _ownerRank, OwnerRank);
        CC_SYNTHESIZE_READONLY(int, _masterCharacterId, MasterCharacterId);
        CC_SYNTHESIZE_READONLY(int, _level, Level);
        CC_SYNTHESIZE_READONLY(int, _power, Power);
        CC_SYNTHESIZE_READONLY(int, _technique, Technique);
        CC_SYNTHESIZE_READONLY(int, _brake, Brake);
        
        CC_SYNTHESIZE_READONLY(bool, _isFriend, IsFriend);
        CC_SYNTHESIZE_READONLY(int, _transferFriendPoint, TransferFriendPoint);
    };
    CC_SYNTHESIZE_READONLY(std::vector<std::shared_ptr<HelpPlayer>>, _helpPlayers, HelpPlayers);
    
};

#endif
