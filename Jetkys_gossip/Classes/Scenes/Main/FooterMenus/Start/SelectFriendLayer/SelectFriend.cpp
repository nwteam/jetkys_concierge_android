#include "SelectFriend.h"
#include "MainScene.h"
#include "FontDefines.h"
#include "UserDefaultManager.h"
#include "HeaderStatus.h"
#include "ScrollBar.h"
#include "SelectFriendDialog.h"
#include "SelectFriendView.h"

USING_NS_CC;

#define tag_friend_popup_action 222

SelectFriend::SelectFriend():
    progress(nullptr)
    , request(nullptr)
    , _selectFriendModel(nullptr)
{
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
}

SelectFriend::~SelectFriend()
{
    delete progress;
    delete request;
    progress = nullptr;
    request = nullptr;
}

bool SelectFriend::init()
{
    if (!Layer::init()) {
        return false;
    }

    showHeadLine(this, "助っ人選択", [this](Ref* pSender) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        if (_startLayer->_mainScene->isMainTutorial()) {
            return;
        }
        if (_startLayer->_mainScene->_isShowing) {
            return;
        }
        _startLayer->showLayer(ST_MAP);
    });

    return true;
}

void SelectFriend::createFriendList(int coseId)
{
    progress->onStart();
    request->getRaceCourse(coseId, CC_CALLBACK_1(SelectFriend::onRasponesGetRaceCourse, this));
}

void SelectFriend::onRasponesGetRaceCourse(Json* response)
{
    std::shared_ptr<SelectFriendModel>selectFriendModel(new SelectFriendModel(response));
    _startLayer->setJsonRespon(selectFriendModel->getGridData(), selectFriendModel->getEnemyCharacterModels());
    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::IS_EVENT_RACE, selectFriendModel->getIsEventCourse());
    _selectFriendModel = selectFriendModel;
    
    if (Json_getItem(Json_getItem(Json_getItem(response, "get_race_course"), "data"), "help_player")) {
        showTableView(selectFriendModel);
    }
}

void SelectFriend::showTableView(const std::shared_ptr<SelectFriendModel>& selectFriendModel)
{
    float changeSizeTable = (float) selectFriendModel->getHelpPlayers().size() * 140.0;
    Size winSize = Director::getInstance()->getWinSize();
    float tableHeight = winSize.height - 155 - HEADER_STATUS::SIZE::HEIGHT;
    float deltaHeigh = 0;

    if (changeSizeTable < tableHeight) {
        tableHeight = changeSizeTable;
        deltaHeigh = (winSize.height - 155 - HEADER_STATUS::SIZE::HEIGHT) - changeSizeTable;
    }

    TableView* tableView = TableView::create(this, Size(610.0, tableHeight));
    tableView->setTag(TAG_SPRITE::TABLE_VIEW);
    tableView->setPosition(Point(10.0, 70.0 + deltaHeigh));
    tableView->setDirection(ScrollView::Direction::VERTICAL);
    tableView->setDelegate(this);
    tableView->setBounceable(false);
    tableView->reloadData();
    addChild(tableView, Z_ORDER::Z_TABLE_VIEW);

    auto scrollBar = ScrollBar::initScrollBar(tableView->getViewSize().height, tableView->getViewSize().height, tableView->getContainer()->getContentSize().height, tableView->getContentOffset().y);
    scrollBar->setTag(TAG_SPRITE::SCROLL_BAR);
    scrollBar->setPosition(Point(winSize.width - 298.5, winSize.height / 2 - 75 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    addChild(scrollBar, Z_ORDER::Z_SCROLL_BAR);
}

Size SelectFriend::tableCellSizeForIndex(TableView* table, ssize_t idx)
{
    return Size(600, 140);
}

ssize_t SelectFriend::numberOfCellsInTableView(TableView* table)
{
    return _selectFriendModel->getEnemyCharacterModels().size();
}

TableViewCell* SelectFriend::tableCellAtIndex(TableView* table, ssize_t idx)
{
    TableViewCell* cell = table->dequeueCell();
    cell = new TableViewCell();
    cell->autorelease();
    auto helpPlayerView = SelectFriendView::create(_selectFriendModel->getHelpPlayers().at(idx));
    helpPlayerView->setPosition(Point(helpPlayerView->getContentSize().width / 2, 70));
    cell->addChild(helpPlayerView);
    if (_startLayer->_mainScene->isMainTutorial() && idx ==  _selectFriendModel->getEnemyCharacterModels().size() - 1) {
        cell->setVisible(false);
    }

    return cell;
}

void SelectFriend::tableCellHighlight(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell)
{
    int tryFriendId = (int)cell->getIdx();
    _selectIndex = tryFriendId;
    auto seq = Sequence::create(
        DelayTime::create(0.5),
        CallFuncN::create([this](Ref* sender) {
        auto helpPlayer = _selectFriendModel->getHelpPlayers().at(_selectIndex);
        showFriendDetailCallback(helpPlayer);
    }), nullptr);
    seq->setTag(tag_friend_popup_action);

    runAction(seq);
}


void SelectFriend::tableCellTouched(TableView* table, TableViewCell* cell)
{
    if (_startLayer->_mainScene->isMainTutorial()) {
        return;
    }
    if (getActionByTag(tag_friend_popup_action)) {
        stopActionByTag(tag_friend_popup_action);
    }
    if (_startLayer->_mainScene->_isShowing) {
        return;
    }
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    int tryFriendId = (int)cell->getIdx();
    _selectIndex = tryFriendId;
    auto dialog = SelectFriendDialog::create(_selectFriendModel->getHelpPlayers().at(tryFriendId),
                                             CC_CALLBACK_1(SelectFriend::selectFriendCallback, this),
                                             CC_CALLBACK_1(SelectFriend::showFriendDetailCallback, this));
    dialog->setTag(TAG_SPRITE::DIALOG);
    addChild(dialog, Z_ORDER::Z_DIALOG);
}


void SelectFriend::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
    auto scrollBar = static_cast<ScrollBar*>(getChildByTag(TAG_SPRITE::SCROLL_BAR));
    if (scrollBar) {
        scrollBar->updateScrollPoint(view->getContentOffset().y);
    }

    if (getActionByTag(tag_friend_popup_action)) {
        stopActionByTag(tag_friend_popup_action);
    }
}

void SelectFriend::showFriendDetailCallback(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model)
{
    _startLayer->showFriendCharacter(model->getMasterCharacterId(),
                                     model->getLevel(),
                                     model->getPower(),
                                     model->getTechnique(),
                                     model->getBrake());
}

void SelectFriend::selectFriendCallback(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model)
{
    HelpPlayer* helpPlayer =  new HelpPlayer(model->getPlayerId(),
                                             model->getMasterCharacterId(),
                                             model->getIsFriend(),
                                             model->getName(),
                                             model->getOwnerRank(),
                                             model->getLevel());
    _startLayer->setFriend(model->getPower(),
                           model->getTechnique(),
                           model->getBrake(),
                           helpPlayer);
    _startLayer->showLayer(ST_DECISION);
}
