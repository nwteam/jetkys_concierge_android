#ifndef __syanago__SelectFriendDialog__
#define __syanago__SelectFriendDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "SelectFriendModel.h"

USING_NS_CC;

class SelectFriendDialog : public Layer, public create_func<SelectFriendDialog>
{
public:
    SelectFriendDialog();
    ~SelectFriendDialog();
    typedef std::function<void(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model)> OnTapSelectFriend;
    typedef std::function<void(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model)> OnTapShowFriendDetail;
    using create_func::create;
    bool init(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model,
              const OnTapSelectFriend& onTapSelectFriend,
              const OnTapShowFriendDetail& onTapShowFriendDetail);
    
private:
    enum TAG_SPRITE{
        BLACK_LAYER = 0,
        BACKGROUND,
        PLAYER_NAME,
        CHARACTER_NAME,
        ICON,
        LEVEL,
        POWER,
        TECHNIQUE,
        BRAKE,
        CANCEL_BUTTON,
        DETAIL_BUTTON,
        SELECT_BUTTON,
    };
    enum Z_ORDER{
        Z_BLACK_LAYER = 0,
        Z_BACKGROUND,
        Z_PLAYER_NAME,
        Z_CHARACTER_NAME,
        Z_ICON,
        Z_LEVEL,
        Z_POWER,
        Z_TECHNIQUE,
        Z_BRAKE,
        Z_CANCEL_BUTTON,
        Z_DETAIL_BUTTON,
        Z_SELECT_BUTTON,
    };
    
    void showBlacLayer();
    void showBackground();
    void showPlayerName(const std::string name);
    void showCharacterName(const std::string name);
    void showIcon(const int masterCharacterId);
    void showLevel(const int level);
    Layer* createStatus(const std::string statusName, const int statusValue, const float deltaPositionY);
    void showSelectButton(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model, const OnTapSelectFriend& onTapSelectFriend);
    void showDetailButton(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model, const OnTapShowFriendDetail& onTapShowFriendDetail);
    void showCancelButton();
};

#endif
