#include "SelectFriendDialog.h"
#include "FontDefines.h"
#include "CharacterModel.h"
#include "jCommon.h"
#include "ui/cocosGUI.h"
#include "SoundHelper.h"

SelectFriendDialog::SelectFriendDialog()
{}

SelectFriendDialog::~SelectFriendDialog()
{}

bool SelectFriendDialog::init(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model,
                              const OnTapSelectFriend& onTapSelectFriend,
                              const OnTapShowFriendDetail& onTapShowFriendDetail)
{
    if (!Layer::create) {
        return false;
    }
    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    showBlacLayer();
    showBackground();
    showPlayerName(model->getName());
    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(model->getMasterCharacterId()));
    showCharacterName(characterModel->getCarName());
    showIcon(model->getMasterCharacterId());
    showLevel(model->getLevel());

    auto powerLabel = createStatus("馬力", model->getPower(), 30);
    powerLabel->setTag(TAG_SPRITE::POWER);
    addChild(powerLabel, Z_ORDER::Z_POWER);

    auto techniqueLabel = createStatus("技量", model->getTechnique(), 0);
    techniqueLabel->setTag(TAG_SPRITE::TECHNIQUE);
    addChild(techniqueLabel, Z_ORDER::Z_TECHNIQUE);

    auto brakeLabel = createStatus("忍耐", model->getBrake(), -30);
    brakeLabel->setTag(TAG_SPRITE::BRAKE);
    addChild(brakeLabel, Z_ORDER::Z_BRAKE);
    
    showCancelButton();
    showDetailButton(model, onTapShowFriendDetail);
    showSelectButton(model, onTapSelectFriend);

    return true;
}

void SelectFriendDialog::showBlacLayer()
{
    auto blackLayer = LayerColor::create(Color4B(Color3B::BLACK));
    blackLayer->setTag(TAG_SPRITE::BLACK_LAYER);
    blackLayer->setOpacity(180);
    addChild(blackLayer, Z_ORDER::Z_BLACK_LAYER);
}

void SelectFriendDialog::showBackground()
{
    auto background = Sprite::create("friend_popup_base.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void SelectFriendDialog::showPlayerName(const std::string name)
{
    auto playerNameLabel = Label::createWithTTF(name, FONT_NAME_2, 32);
    playerNameLabel->setTag(TAG_SPRITE::PLAYER_NAME);
    playerNameLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    playerNameLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    auto background = getChildByTag(TAG_SPRITE::BACKGROUND);
    playerNameLabel->setPosition(background->getPosition() + Vec2(0,background->getContentSize().height / 2 - 30));
    playerNameLabel->setColor(COLOR_YELLOW);
    playerNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(playerNameLabel, Z_ORDER::Z_PLAYER_NAME);
}

void SelectFriendDialog::showCharacterName(const std::string name)
{
    auto charaNameLabel = Label::createWithTTF(name, FONT_NAME_2, 28);
    charaNameLabel->setTag(TAG_SPRITE::CHARACTER_NAME);
    charaNameLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    charaNameLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    charaNameLabel->setPosition(getChildByTag(TAG_SPRITE::PLAYER_NAME)->getPosition() - Vec2(0, 43));
    charaNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(charaNameLabel, Z_ORDER::Z_CHARACTER_NAME);
}

void SelectFriendDialog::showIcon(const int masterCharacterId)
{
    auto icon = makeSprite(StringUtils::format("%d_i.png", masterCharacterId).c_str());
    icon->setTag(TAG_SPRITE::ICON);
    auto playerNameLabel = getChildByTag(TAG_SPRITE::PLAYER_NAME);
    auto background = getChildByTag(TAG_SPRITE::BACKGROUND);
    icon->setPosition(Vec2(background->getPosition().x - icon->getContentSize().width / 2 - 20,
                           playerNameLabel->getPositionY() - playerNameLabel->getContentSize().height - icon->getContentSize().height / 2 - 21));
    addChild(icon, Z_ORDER::Z_ICON);
}

void SelectFriendDialog::showLevel(const int level)
{
    auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %d", level), FONT_NAME_2, 22);
    levelLabel->setTag(TAG_SPRITE::LEVEL);
    levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    auto icon = getChildByTag(TAG_SPRITE::ICON);
    levelLabel->setPosition(icon->getPosition() + Vec2(0, -icon->getContentSize().height / 2 - 5));
    levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
    addChild(levelLabel, Z_ORDER::Z_LEVEL);
}

Layer* SelectFriendDialog::createStatus(const std::string statusName, const int statusValue, const float deltaPositionY)
{
    auto result = Layer::create();
    auto statusLabel = Label::createWithTTF(statusName, FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    auto icon = getChildByTag(TAG_SPRITE::ICON);
    statusLabel->setPosition(icon->getPosition() + Vec2(icon->getContentSize().width / 2 + 10, deltaPositionY - 10));
    statusLabel->setAnchorPoint(Vec2(0, 0.5));
    statusLabel->enableShadow();
    result->addChild(statusLabel);
    auto statusValueLabel = Label::createWithTTF(FormatWithCommas(statusValue), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    statusValueLabel->setPosition(statusLabel->getPosition() + Vec2(125, 0));
    statusValueLabel->setAnchorPoint(Vec2(1, 0.5));
    statusValueLabel->enableShadow();
    result->addChild(statusValueLabel);
    return result;
}

void SelectFriendDialog::showCancelButton()
{
    auto cancelButton = ui::Button::create("te_popup_close_button.png");
    cancelButton->setTag(TAG_SPRITE::CANCEL_BUTTON);
    cancelButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    cancelButton->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 +cancelButton->getContentSize().height + 30));
    cancelButton->addTouchEventListener([this](Ref* pSender, ui::Widget::TouchEventType type) {
        if (type != ui::Widget::TouchEventType::ENDED) {
            return;
        }
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        removeFromParent();
    });
    addChild(cancelButton, Z_ORDER::Z_CANCEL_BUTTON);
}

void SelectFriendDialog::showDetailButton(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model, const OnTapShowFriendDetail& onTapShowFriendDetail)
{
    auto detailButton = ui::Button::create("te_popup_button.png");
    detailButton->setTag(TAG_SPRITE::DETAIL_BUTTON);
    detailButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    auto detailLabel = Label::createWithTTF("詳細情報", FONT_NAME_2, 32);
    detailLabel->setPosition(Point(detailButton->getContentSize().width / 2,
                                   detailButton->getContentSize().height / 2 - 16));
    detailLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    detailButton->addChild(detailLabel);
    detailButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::CANCEL_BUTTON)->getPosition()) + Vec2(0, 30 + detailButton->getContentSize().height));
    detailButton->addTouchEventListener([this, model, onTapShowFriendDetail](Ref* pSender, ui::Widget::TouchEventType type) {
        if (type != ui::Widget::TouchEventType::ENDED) {
            return;
        }
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        onTapShowFriendDetail(model);
    });
    addChild(detailButton, Z_ORDER::Z_DETAIL_BUTTON);
}

void SelectFriendDialog::showSelectButton(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model, const OnTapSelectFriend& onTapSelectFriend)
{
    auto selectButton = ui::Button::create("te_popup_button.png");
    selectButton->setTag(TAG_SPRITE::SELECT_BUTTON);
    selectButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    auto selectLabel = Label::createWithTTF("選択する", FONT_NAME_2, 32);
    selectLabel->setPosition(Point(selectButton->getContentSize().width / 2,
                                   selectButton->getContentSize().height / 2 - 16));
    selectLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    selectButton->addChild(selectLabel);
    selectButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::DETAIL_BUTTON)->getPosition()) + Vec2(0, 30 + selectButton->getContentSize().height));
    selectButton->addTouchEventListener([this, model, onTapSelectFriend](Ref* pSender, ui::Widget::TouchEventType type) {
        if (type != ui::Widget::TouchEventType::ENDED) {
            return;
        }
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        onTapSelectFriend(model);
    });
    addChild(selectButton, Z_ORDER::Z_SELECT_BUTTON);
}