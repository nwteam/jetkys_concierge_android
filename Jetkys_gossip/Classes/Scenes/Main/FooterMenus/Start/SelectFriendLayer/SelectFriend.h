#ifndef __syanago__SelectFriend__
#define __syanago__SelectFriend__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "create_func.h"
#include "show_head_line.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"
#include "SelectFriendModel.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace SyanagoAPI;

class StartLayer;

class SelectFriend : public Layer, public create_func<SelectFriend>, public show_head_line, public TableViewDataSource, public TableViewDelegate
{
public:
    SelectFriend();
    ~SelectFriend();
    using create_func::create;
    bool init();
    
    void createFriendList(int courseId);
    
    
    
    void scrollViewDidScroll(ScrollView* view);
    virtual void scrollViewDidZoom(ScrollView* view) {};
    void tableCellHighlight(TableView *table, TableViewCell *cell);
    virtual void tableCellTouched(TableView* table, TableViewCell* cell);
    virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);
    virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(TableView *table);
    
    int _selectIndex;
    StartLayer *_startLayer;
private:
    enum TAG_SPRITE{
        TABLE_VIEW = 0,
        SCROLL_BAR,
        DIALOG,
    };
    enum Z_ORDER{
        Z_TABLE_VIEW = 0,
        Z_SCROLL_BAR,
        Z_DIALOG = 12,
    };
    DefaultProgress* progress;
    RequestAPI* request;
    std::shared_ptr<SelectFriendModel> _selectFriendModel;
    
    void onRasponesGetRaceCourse(Json* response);
    void showTableView(const std::shared_ptr<SelectFriendModel>& selectFriendModel);
    
    void showFriendDetailCallback(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model);
    void selectFriendCallback(const std::shared_ptr<SelectFriendModel::HelpPlayer>& model);
};

#endif /* defined(__syanago__SelectFriend__) */
