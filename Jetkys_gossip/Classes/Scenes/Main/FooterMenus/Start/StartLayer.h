#ifndef __syanago__StartLayer__
#define __syanago__StartLayer__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"
#include "SelectFriend.h"
#include "TeamStatus.h"
#include "HelpPlayer.h"

USING_NS_CC;

class MainScene;

enum StartIndex {
    ST_MAIN , ST_SELECT, ST_DECISION, ST_RACE, ST_MAP
};

class StartLayer : public Layer
{
public:
    StartLayer();
    bool init();
    CREATE_FUNC(StartLayer);
    MainScene *_mainScene;
    SelectFriend* frLayer;
    
    CC_SYNTHESIZE(StartIndex, _startIndex, SIndex);
    
    void showLayer(StartIndex nextLayer);
    void startTutorial();
    
    int _trycoseId;
    std::string _trycoseName;
    std::string _trycoseInfo;
    
    int _tryfriendExercise;
    int _tryfriendReaction;
    int _tryfriendDecision;
    
    int _tryTotalExercise;
    int _tryTotalReaction;
    int _tryTotalDecision;
    int _tryFrCharacterID;
    
    int _tryLeaderID;
    
    void resetPageTag();
    
    void setCose(int coseid, std::string coseName, std::string coseInfo);
    void setFriend(int exercise, int Reaction, int Decision, HelpPlayer* helpPlayer);
    void setTryCose(int coseId, TeamStatus* teamStatus, int tryLeader, std::vector<std::shared_ptr<const CharacterModel>> playerCharacters);
    void setJsonRespon(Json* grid, std::vector<std::shared_ptr<const CharacterModel>> enemyCharacterModels);
    void GoFulePage();
    
    void showFriendCharacter(int playerMstCharacterId, int level, int exercise, int reaction, int decision);
    
    int getNowTime();
    void goToRaceEfect();
    
    void resetStartTab();
    
    void setTryArea(int areaId);
    
    void initWithPlayerController();
    void setMapGroupId(int groupId, int mapId);
    
    
    void setFooterVisible(const bool isVisible);
private:
    enum TAG_SPRITE {
        BACKGROUND_HOME = 0
    };
    
    Menu* _btnList;
    
    Json* _tryGridCell;
    
    int _tryArea;
    
    int _mapGroupId;
    int _mapId;
    
    TeamStatus* _totalTeamStatus;
    HelpPlayer* _helpPlayer;
    std::vector<std::shared_ptr<const CharacterModel>> _enemyCharacterModels;
    std::vector<std::shared_ptr<const CharacterModel>> _playerCharacters;
};


#endif /* defined(__syanago__StartLayer__) */
