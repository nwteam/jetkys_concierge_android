#include "StartLayerCheckPlayerTryCourseModel.h"
#include "CourseModel.h"
#include "PlayerController.h"


StartLayerCheckPlayerTryCourseModel::StartLayerCheckPlayerTryCourseModel(){
    _tryStatus = false;
    _commingSoonFlag = false;
    _dropFilePathName = "";
    _dropFilePathName = "";
    _bageCount = 0;
    _courseType = 0;
}
StartLayerCheckPlayerTryCourseModel::~StartLayerCheckPlayerTryCourseModel(){
    
}

StartLayerCheckPlayerTryCourseModel* StartLayerCheckPlayerTryCourseModel::createCourseChecker(int courseId){
    auto model = new StartLayerCheckPlayerTryCourseModel();
    model->setCheckTryCourse(courseId);
    model->autorelease();
    return model;
}


int StartLayerCheckPlayerTryCourseModel::getCourseType(){
    int courseType = 0;
    courseType = _courseType;
    return courseType;
}

std::string StartLayerCheckPlayerTryCourseModel::getCourseDropImageFilePath(){
    std::string filePath;
    filePath = _dropFilePathName;
    return filePath;
}

std::string StartLayerCheckPlayerTryCourseModel::getCourseDropBackGroundImageFilePath(){
    std::string filePath;
    filePath = _dropBackGroundFilePathName;
    return filePath;
}

bool StartLayerCheckPlayerTryCourseModel::getCourseTryStatus(){
    bool status;
    status = _tryStatus;
    return status;
}

int StartLayerCheckPlayerTryCourseModel::getCourseBageCount(){
    int bageCount = 0;
    bageCount = _bageCount;
    return bageCount;
}

bool StartLayerCheckPlayerTryCourseModel::getCommingSoonFlag(){
    bool flag;
    flag = _commingSoonFlag;
    return flag;
}

//**********************************************************************

void StartLayerCheckPlayerTryCourseModel::setCheckTryCourse(int courseId){
    std::shared_ptr<CourseModel> courseModel = CourseModel::find(courseId);
    _courseType = courseModel->getCourseType();
    int courseAreaId = courseModel->getAreaID();
    int courseImageId = courseModel->getImageId();
    int prevTryCourseId = courseModel->getPrevId();
    int areaFirstCourseId = CourseModel::getFirstCourseIdRemoveEventAndTest(courseAreaId);
    if(_courseType == 0){
        //CommingSoon
        _dropFilePathName = "comming_soon_marker.png";
        _dropBackGroundFilePathName = StringUtils::format("%d_normal_image.png",courseImageId);
        _tryStatus = false;
        _bageCount = 0;
        _commingSoonFlag = true;
    }else{
        _commingSoonFlag = false;
        if(PLAYERCONTROLLER->isCleared(courseId)){
            //クリア済み
            if(_courseType == 1){
                //通常コース
                _dropFilePathName = "clear_marker.png";
                _dropBackGroundFilePathName = StringUtils::format("%d_normal_image.png",courseImageId);
                _tryStatus = true;
                _bageCount = PLAYERCONTROLLER->getClearCourseBage(courseId);
            }else if(_courseType == 2){
                //ボスコース
                _dropFilePathName = "clear_boss.png";
                _dropBackGroundFilePathName = StringUtils::format("%d_boss_image.png",courseImageId);
                _tryStatus = true;
                _bageCount = PLAYERCONTROLLER->getClearCourseBage(courseId);
            }else{
                //イベントコースとテストコースとその他
                _dropFilePathName = "event_marker.png";
                _dropBackGroundFilePathName = StringUtils::format("%d_normal_image.png",courseImageId);
                _tryStatus = true;
                _bageCount = PLAYERCONTROLLER->getClearCourseBage(courseId);
            }
        }else{
            if(areaFirstCourseId == courseId || PLAYERCONTROLLER->isCleared(prevTryCourseId)){
                //挑戦できるコース
                if(_courseType == 1){
                    //通常コース
                    _dropFilePathName = "next_clear_marker.png";
                    _dropBackGroundFilePathName = StringUtils::format("%d_normal_image.png",courseImageId);
                    _tryStatus = true;
                    _bageCount = 0;
                }else if(_courseType == 2){
                    //ボスコース
                    _dropFilePathName = "next_clear_boss.png";
                    _dropBackGroundFilePathName = StringUtils::format("%d_boss_image.png",courseImageId);
                    _tryStatus = true;
                    _bageCount = 0;
                }else{
                    //イベントコースとその他
                    _dropFilePathName = "event_marker.png";
                    _dropBackGroundFilePathName = StringUtils::format("%d_normal_image.png",courseImageId);
                    _tryStatus = true;
                    _bageCount = 0;
                }
            }else{
                //未クリア
                if(_courseType == 1){
                    //通常コース
                    _dropFilePathName = "non_clear_marker.png";
                    _dropBackGroundFilePathName = StringUtils::format("%d_normal_image.png",courseImageId);
                    _tryStatus = false;
                    _bageCount = 0;
                }else if(_courseType == 2){
                    //ボスコース
                    _dropFilePathName = "non_clear_boss.png";
                    _dropBackGroundFilePathName = StringUtils::format("%d_boss_image.png",courseImageId);
                    _tryStatus = false;
                    _bageCount = 0;
                }else{
                    //イベントコースとテストコースとその他
                    _dropFilePathName = "event_marker.png";
                    _dropBackGroundFilePathName = StringUtils::format("%d_normal_image.png",courseImageId);
                    _tryStatus = true;
                    _bageCount = 0;
                }
            }
        }
    }
}