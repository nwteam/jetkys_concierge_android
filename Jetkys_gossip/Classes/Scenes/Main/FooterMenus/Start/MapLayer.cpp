#include "MapLayer.h"
#include "cocos2d.h"
#include "FontDefines.h"
#include "jCommon.h"
#include "HeaderStatus.h"
#include "CourseModel.h"

#define MAP_INITIAL_SCALE 1.0

MapLayer::~MapLayer()
{
    if (_mstId > 0) {
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_ca.plist", _mstId).c_str());
    }
    SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_area.plist", _areaId).c_str());
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("map_slime_image.plist");
}

bool MapLayer::init(MapLayerDelegate* mapScene, int areaId)
{
    if (!Layer::init()) {
        return false;
    }
    _mapDelegate = mapScene;
    _areaId = areaId;
    _pPosi = Point(0, 0);
    _doubleTapTime = 0;

    showMap(areaId);

    // ダブルタップで固定倍率にズームアウト・ズームインできるようにする。
    auto dispatcher = Director::getInstance()->getEventDispatcher();

    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(MapLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(MapLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(MapLayer::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(MapLayer::onTouchCancelled, this);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    scheduleUpdate();
    return true;
}

void MapLayer::showMap(int areaId)
{
    Size winSize = Director::getInstance()->getWinSize();
    pScrollView = ScrollView::create(Size(winSize.width, winSize.height - HEADER_STATUS::SIZE::HEIGHT));
    pScrollView->setDelegate(this);

    // スクロールビューに入れるスプライトを用意
    auto* pSprite = createMapSprite(areaId);
    pScrollView->setContainer(pSprite);
    pScrollView->setContentSize(pSprite->getContentSize());
    pScrollView->setBounceable(false);
    pScrollView->setMinScale(1.0);
    pScrollView->setMaxScale(2.0);
    addChild(pScrollView);

    if (UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        std::shared_ptr<CourseModel>courseModel(CourseModel::find(1));
        int x = courseModel->getPositionX();
        int y = courseModel->getPositionY();
        const int MAP_TUTORIAL_SCALE = 2;
        pScrollView->setContentOffset(-(Point(x, y) * MAP_TUTORIAL_SCALE - Point(HEADER_STATUS::SIZE::HEIGHT, 302)));
        return;
    }
    pScrollView->setContentOffset(-((_pPosi * MAP_INITIAL_SCALE) - Point(winSize.width / 2, (winSize.height - 140) / 2)));
}

void MapLayer::setScrollEnable(bool flag)
{
    pScrollView->setTouchEnabled(flag);
}

Sprite* MapLayer::createMapSprite(int areaId)
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_area.plist", areaId).c_str());

    auto* pSprite = makeSprite(StringUtils::format("%d_area.png", areaId).c_str());
    if (UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        pSprite->cocos2d::Node::setScale(2);
    } else {
        pSprite->cocos2d::Node::setScale(MAP_INITIAL_SCALE);
    }

    coseList = MenuTouch::create();
    coseList->setPosition(Vec2::ZERO);
    pSprite->addChild(coseList, 2);


    // ボタンの生成（このボタンを押してコースを選択する。）
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("map_slime_image.plist");
    auto courseModels = CourseModel::findAreaId(areaId);
    for (int i = 0; i < courseModels.size(); i++) {
        std::shared_ptr<CourseModel>courseModel(courseModels.at(i));
        if (courseModel == NULL) {
            continue;
        }

        StartLayerCheckPlayerTryCourseModel* model = StartLayerCheckPlayerTryCourseModel::createCourseChecker(courseModel->getID());
        // コースボタン
        MenuItemSprite* cosePoint = makeMenuItem(model->getCourseDropImageFilePath().c_str(), CC_CALLBACK_1(MapLayer::pushCosePoint, this));
        cosePoint->setPosition(Point(courseModel->getPositionX(), courseModel->getPositionY()));
        cosePoint->setTag(courseModel->getID());
        auto coseImage = makeSprite(model->getCourseDropBackGroundImageFilePath().c_str());
        if (model->getCommingSoonFlag() == false) {
            pSprite->addChild(coseImage, 1);
        }

        if (model->getCourseType() != 4) {
            showStars(cosePoint, model, courseModel->getID());
        }

        if (PLAYERCONTROLLER->getUncompleteMissionCourses()->isPlaceOfMissionWithCourseId(courseModel->getID()) == true) {
            showPlaceOfMission(cosePoint, courseModel->getID());
        }

        coseList->addChild(cosePoint);
        coseImage->setPosition(cosePoint->getPosition());

        if (PLAYERCONTROLLER->_playerTeams == nullptr &&
            PLAYERCONTROLLER->_playerCharacterModels.size() != 0 &&
            PLAYERCONTROLLER->_player->getCurrentCourseId() == courseModel->getID()) {
            _mstId = PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]->getCharactersId();
            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_ca.plist", _mstId).c_str());
            auto chara = makeSprite(StringUtils::format("%d_sd.png", _mstId).c_str());
            pSprite->addChild(chara, 2);
            chara->setAnchorPoint(Vec2(0.5f, 0.0f));
            _pPosi = Point(courseModel->getPositionX(), courseModel->getPositionY());
            chara->setScale(0.75);
            chara->setPosition(_pPosi);
        }
    }

    if (_pPosi == Point(0, 0)) {
        int coseId = CourseModel::getFirstCourseId(_areaId);
        if (coseId > 0) {
            std::shared_ptr<CourseModel>courseModel(CourseModel::find(coseId));
            if (courseModel != NULL) {
                _pPosi = Point(courseModel->getPositionX(), courseModel->getPositionY());
            }
        }
    }
    return pSprite;
}

void MapLayer::showStars(MenuItemSprite* courseSprite, StartLayerCheckPlayerTryCourseModel* model, const int courseId)
{
    const float WIDTH_STAR = 24.0;
    switch (model->getCourseBageCount()) {
    case 3:
        courseSprite->addChild(createStar(0, courseId));
        break;
    case 2:
        courseSprite->addChild(createStar(-WIDTH_STAR / 2 - 2.0, courseId));
        courseSprite->addChild(createStar(WIDTH_STAR / 2 + 2.0, courseId));
        break;
    case 1:
        courseSprite->addChild(createStar(-WIDTH_STAR - 2.0, courseId));
        courseSprite->addChild(createStar(WIDTH_STAR + 2.0, courseId));
        // 真ん中が一番上
        courseSprite->addChild(createStar(2, courseId));
        break;
    }
}

Sprite* MapLayer::createStar(float delta, const int courseId)
{
    float y = 0.0;
    float x = 0.0;
    if (CourseModel::find(courseId)->getCourseType() == 2) {
        y = 40;
        x = 15;
    }
    Sprite* result = makeSprite("rarity.png");
    const float WIDTH_COURSE_SPRITE = 71.0;
    result->setPosition(Point(WIDTH_COURSE_SPRITE / 2 + delta + x, y));
    return result;
}

void MapLayer::showPlaceOfMission(MenuItemSprite* slime, const int courseId)
{
    float y = 0.0;
    if (CourseModel::find(courseId)->getCourseType() == 2) {
        y = 40;
    }
    auto sprite = Sprite::create("mission_label.png");
    sprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    sprite->setPosition(Vec2(0, slime->getContentSize().height - y));
    sprite->setScale(0.5);
    slime->addChild(sprite);
}

void MapLayer::update(float dt)
{
    if (_double) {
        _doubleTapTime++;
        if (_doubleTapTime >= 15) {
            _doubleTapTime = 0;
            _double = false;
        }
    } else {
        if (_zoomout) {
            if (pScrollView->getZoomScale() + 2 * dt <= 2.0) {
                pScrollView->setZoomScale(pScrollView->getZoomScale() + 2 * dt);
                if (pScrollView->getZoomScale() + 2 * dt >= 2.0) {
                    pScrollView->setZoomScale(2.0);
                }
            } else {
                _doubleTapTime = 0;
                _zoomout = false;
                _zoomin = false;
                _scaleAuto = false;
                pScrollView->setTouchEnabled(true);
            }
        }

        if (_zoomin) {
            if (pScrollView->getZoomScale() - 2 * dt >= 0.5) {
                pScrollView->setZoomScale(pScrollView->getZoomScale() - 2 * dt);
                if (pScrollView->getZoomScale() - 2 * dt < 0.5) {
                    pScrollView->setZoomScale(0.5);
                }
            } else {
                _doubleTapTime = 0;
                _zoomout = false;
                _zoomin = false;
                _scaleAuto = false;
                pScrollView->setTouchEnabled(true);
            }
        }
    }
}

void MapLayer::pushCosePoint(cocos2d::Ref* pSender)
{
    if (!pScrollView->isTouchEnabled()) {
        return;
    }
    auto item = (MenuItem*) pSender;
    int tag = item->getTag();
    //////CCLOG("TAG : %d",tag);
    if (_mapDelegate != nullptr) {
        _mapDelegate->btSelectCose(tag);
    }
}


void MapLayer::scrollViewDidScroll(ScrollView* view)
{
    //////CCLOG("スクロール！");
}

void MapLayer::scrollViewDidZoom(ScrollView* view)
{
    //////CCLOG("ズーム！");
}

void MapLayer::scrolDidAutoZoom()
{
    _scaleAuto = false;
}

bool MapLayer::onTouchBegan(Touch* touch, Event* unused_event)
{
    if (UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        return false;
    }
    ////CCLOG("touch size map: %lu",_touches.size());
    if (_touches.size() == 0) {
        _touches.push_back(touch);

        if (!_double) {
            _double = true;
        } else {
            _scaleAuto = true;
            _double = false;
            //                unscheduleUpdate();

            _doubleTapTime = 0;
            ////CCLOG("scrollview scale: %f, %f", pScrollView->getZoomScale(), getScale());
            if (pScrollView->getZoomScale() < 2.0 && pScrollView->getZoomScale() >= 0.5) {
                _zoomin = false;
                _zoomout = true;
                pScrollView->setTouchEnabled(false);
            } else if (pScrollView->getZoomScale() == 2.0) {
                _zoomin = true;
                _zoomout = false;
                pScrollView->setTouchEnabled(false);
            }
        }

        return true;
    }
    return true;
}

void MapLayer::onTouchMoved(Touch* touch, Event* unused_event)
{
    if (UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        return;
    }
}

void MapLayer::onTouchEnded(Touch* touch, Event* unused_event)
{
    if (_touches.size() == 1) {
        _touches.clear();
    }
}

void MapLayer::onTouchCancelled(Touch* touch, Event* unused_event)
{
    if (UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        return;
    }
    if (!isVisible()) {
        return;
    }

    auto touchIter = std::find(_touches.begin(), _touches.end(), touch);
    _touches.erase(touchIter);

    if (_touches.size() == 0) {}
}
