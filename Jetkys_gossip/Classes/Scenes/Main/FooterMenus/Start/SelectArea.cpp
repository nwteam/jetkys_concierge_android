#include "SelectArea.h"
#include "Cell.h"
#include "HeaderStatus.h"
#include "RankingLayer.h"
#include "MapModel.h"
#include "AreaModel.h"
#include "CourseModel.h"
#include "SoundHelper.h"
#include "PlayerController.h"

#define SIZE_AREA_CELL Size(563.0f, 125.0f)

SelectArea::SelectArea() {}

SelectArea::~SelectArea()
{
    for (int i = 0; i < tableData.size(); i++) {
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_select_area_menu.plist", tableData.at(i)[DATA_KEY::AREA_ID]).c_str());
    }
}

bool SelectArea::init(int mapId, const OnBackCallback& backCallback, const OnSelectAreaCallback& selectCallBack, const OnViewFooterCallback& visibleCallback)
{
    if (!Layer::init()) {
        return false;
    }
    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};

    _onBackCallback = backCallback;
    _onSelectAreaCallback = selectCallBack;
    _onVisibleCallback = visibleCallback;

    showBackGround();
    showHeadLine(mapId);
    showRankingButton();

    setDataForTable(mapId);

    if (tableData.size() > 0) {
        showTable();
    } else {
        showEmptyErrorDialog();
    }
    return true;
}

void SelectArea::showBackGround()
{
    auto winSize = Director::getInstance()->getWinSize();
    auto background = LayerColor::create(Color4B(Color3B::BLACK), winSize.width, winSize.height);
    background->setOpacity(120);
    background->setPosition(Point::ZERO);
    background->setOpacity(0);
    background->runAction(FadeTo::create(0.2, 200));
    addChild(background);
}

void SelectArea::showHeadLine(int mapId)
{
    Size winSize = Director::getInstance()->getWinSize();

    auto background = makeSprite("head_line_no1.png");
    background->setPosition(Vec2(background->getContentSize().width / 2,
                                 winSize.height - background->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 12));
    addChild(background);

    auto backButton = makeMenuItem("back_button.png", CC_CALLBACK_1(SelectArea::onTapBackButton, this));
    auto menuBack = MenuTouch::create(backButton, NULL);
    menuBack->setPosition(Vec2(background->getPosition().x - background->getContentSize().width / 2 + 14 + backButton->getNormalImage()->getContentSize().width / 2,
                               background->getPosition().y));
    addChild(menuBack, 10);

    std::string areaName = "イベントマップ";
    if (mapId > 0) {
        areaName = MapModel::find(mapId)->getName();
    }
    auto title = Label::createWithTTF(areaName.c_str(), FONT_NAME_2, 34);
    title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    title->setAnchorPoint(Vec2(0.0f, 0.5f));
    title->setPosition(menuBack->getPosition() + Point(backButton->getContentSize().width / 2 + 10, -15));
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(title);
}

void SelectArea::showRankingButton()
{
    Sprite* rankingSelected = Sprite::create("button_orange.png");
    rankingSelected->setColor(Color3B::GRAY);

    MenuItem* rankingMenuItem = MenuItemSprite::create(Sprite::create("button_orange.png"),
                                                       rankingSelected,
                                                       [&](Ref* pSender) {
        SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        auto layer = RankingLayer::create(CC_CALLBACK_1(SelectArea::onCallBackRanking, this));
        layer->setTag(TAG_SPRITE::RANKING);
        addChild(layer, Z_ORDER::Z_RANKING);
        _onVisibleCallback(false);
    });
    Menu* rankingMenu = Menu::create(rankingMenuItem, NULL);
    rankingMenu->setPosition(Director::getInstance()->getWinSize().width - 90, Director::getInstance()->getWinSize().height - rankingSelected->getContentSize().height / 2 - 12 - HEADER_STATUS::SIZE::HEIGHT);
    addChild(rankingMenu);

    Label* rankingLabel = Label::createWithTTF("ランキング", FONT_NAME_2, 28);
    rankingLabel->enableShadow();
    rankingLabel->setPosition(rankingMenu->getPosition().x, rankingMenu->getPosition().y - 14);
    addChild(rankingLabel);
}

void SelectArea::onCallBackRanking(Ref* pSender)
{
    _onVisibleCallback(true);
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    getChildByTag(TAG_SPRITE::RANKING)->removeFromParent();
}

void SelectArea::onTapBackButton(Ref* pSender)
{
    getEventDispatcher()->removeEventListener(_listener);
    _onBackCallback();
    removeFromParent();
}

void SelectArea::setDataForTable(int mapId)
{
    if (mapId < 1) {
        // for event
        for (int area_id : AreaModel::getEventIds()) {
            std::shared_ptr<AreaModel>areaModel = AreaModel::find(area_id);
            std::map<DATA_KEY, int>data;
            data[DATA_KEY::AREA_ID] = area_id;
            data[DATA_KEY::CLEARED] = PLAYERCONTROLLER->isCleared(CourseModel::getLastCourseId(area_id));
            data[DATA_KEY::COMMING_SOON] = CourseModel::isFirstCourseCommingSoon(area_id);
            tableData.push_back(data);
        }
        for (int area_id : AreaModel::getTestIds()) {
            std::shared_ptr<AreaModel>areaModel = AreaModel::find(area_id);
            std::map<DATA_KEY, int>data;
            data[DATA_KEY::AREA_ID] = area_id;
            data[DATA_KEY::CLEARED] = false;
            data[DATA_KEY::COMMING_SOON] = CourseModel::isFirstCourseCommingSoon(area_id);
            tableData.push_back(data);
        }
        return;
    }
    for (int area_id : AreaModel::getNormalIds(mapId)) {
        std::shared_ptr<AreaModel>areaModel = AreaModel::find(area_id);
        std::map<DATA_KEY, int>data;
        if (PLAYERCONTROLLER->isCleared(CourseModel::getFirstCourseId(area_id))) {
            data[DATA_KEY::AREA_ID] = area_id;
            data[DATA_KEY::CLEARED] = PLAYERCONTROLLER->isCleared(CourseModel::getLastCourseId(area_id));
            data[DATA_KEY::COMMING_SOON] = CourseModel::isFirstCourseCommingSoon(area_id);
            tableData.push_back(data);
        } else if (PLAYERCONTROLLER->isCleared(CourseModel::getLastCourseId(areaModel->getPrevId())) ||
                   (areaModel->getPrevId() == 0 && areaModel->getAreaType() == 1)) {
            data[DATA_KEY::AREA_ID] = area_id;
            data[DATA_KEY::CLEARED] = false;
            data[DATA_KEY::COMMING_SOON] = CourseModel::isFirstCourseCommingSoon(area_id);
            tableData.push_back(data);
        }
    }
}


void SelectArea::showEmptyErrorDialog()
{
    auto winSize = Director::getInstance()->getWinSize();
    auto dialog = makeSprite("dialog_base.png");
    dialog->setPosition(winSize / 2);
    addChild(dialog);
    auto text = Label::createWithTTF("現在挑戦できるエリアは有りません", FONT_NAME_2, 30);
    text->setPosition(Point(winSize.width / 2, (winSize.height / 2 - 20)));
    text->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(text);
    return;
}

void SelectArea::showTable()
{
    Layer* listBase = Layer::create();
    listBase->setTag(TAG_SPRITE::LIST_BASE);

    auto winSize = Director::getInstance()->getWinSize();
    _tableView = TableView::create(this, Size(SIZE_AREA_CELL.width, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65));
    _tableView->setDirection(ScrollView::Direction::VERTICAL);
    _tableView->setPosition(Point(winSize.width / 2 - SIZE_AREA_CELL.width * 0.5, 0));
    _tableView->setDelegate(this);
    _tableView->setBounceable(false);
    listBase->addChild(_tableView);

    _scrollBar = ScrollBar::initScrollBar(winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 180 - 30, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65, _tableView->getContainer()->getContentSize().height, _tableView->getContentOffset().y);
    _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 15));
    listBase->addChild(_scrollBar);
    addChild(listBase);

    if (winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 <= _tableView->getContainer()->getContentSize().height) {
        _tableView->setPosition(_tableView->getPosition() + Point(-20, 0));
    }
    _tableView->reloadData();
}

Size SelectArea::cellSizeForTable(TableView* table)
{
    return SIZE_AREA_CELL;
}

ssize_t SelectArea::numberOfCellsInTableView(TableView* table)
{
    return tableData.size() + 2;
}

TableViewCell* SelectArea::tableCellAtIndex(TableView* table, ssize_t idx)
{
    Cell* cell = (Cell*)table->dequeueCell();
    if (cell) {
        cell->removeFromParent();
    }
    cell = cellWithIdx((int)idx);
    return cell;
}

Cell* SelectArea::cellWithIdx(int idx)
{
    auto cell = new Cell();
    if (idx > 1) {
        const int DATA_INDEX = idx - 2;
        int areaId = tableData.at(DATA_INDEX)[DATA_KEY::AREA_ID];
        if (areaId > 0) {
            cell->setTag(DATA_INDEX);

            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_select_area_menu.plist", areaId).c_str());

            // button
            auto selectAreaButton = makeSprite(StringUtils::format("%d_select_area_menu.png", areaId).c_str());
            selectAreaButton->setPosition(Vec2(SIZE_AREA_CELL.width / 2, SIZE_AREA_CELL.height / 2));

            // comming soon
            if (tableData.at(DATA_INDEX)[DATA_KEY::COMMING_SOON]) {
                auto coverbg = makeSprite(StringUtils::format("%d_select_area_menu.png", areaId).c_str());
                coverbg->setColor(Color3B::BLACK);
                coverbg->setOpacity(130);
                coverbg->setPosition(Vec2(selectAreaButton->getContentSize().width / 2, selectAreaButton->getContentSize().height / 2));
                selectAreaButton->addChild(coverbg);
            }
            // clear label
            if (tableData.at(DATA_INDEX)[DATA_KEY::CLEARED]) {
                auto clabel = makeSprite("area_clear_image.png");
                clabel->setPosition(Point(2 + clabel->getContentSize().width / 2,
                                          selectAreaButton->getContentSize().height - 2 - clabel->getContentSize().height / 2));
                selectAreaButton->addChild(clabel);
            }
            cell->addChild(selectAreaButton);

            if (PLAYERCONTROLLER->getUncompleteMissionCourses()->isPlaceOfMissionWithAreaId(areaId) == true) {
                showPlaceOfMission(selectAreaButton, tableData.at(DATA_INDEX)[DATA_KEY::CLEARED]);
            }

            // onTapImage
            auto onTapSprite = makeSprite(StringUtils::format("%d_select_area_menu.png", areaId).c_str());
            onTapSprite->setPosition(Vec2(SIZE_AREA_CELL.width / 2, SIZE_AREA_CELL.height / 2));
            onTapSprite->setTag(-100);
            onTapSprite->setOpacity(120);
            onTapSprite->setColor(Color3B::BLACK);
            onTapSprite->setVisible(false);
            cell->addChild(onTapSprite);

            // add clear status
            std::shared_ptr<AreaModel>areaModel(AreaModel::find(areaId));
            if (areaModel->getAreaType() != 4) {
                int getBage = 0;
                std::vector<int>courseIds = CourseModel::getIds(areaId);
                if (courseIds.size() > 0) {
                    for (int courseId : courseIds) {
                        switch (PLAYERCONTROLLER->getClearCourseBage(courseId)) {
                        case 1:
                            getBage += 3;
                            break;
                        case 2:
                            getBage += 2;
                            break;
                        case 3:
                            getBage += 1;
                            break;
                        default:
                            break;
                        }
                    }
                    auto clearedAndMaxLabel = Label::createWithTTF(StringUtils::format("%d/%lu", getBage, courseIds.size() * 3), FONT_NAME_2, 18);
                    clearedAndMaxLabel->setAnchorPoint(Vec2(1, 0.5));
                    clearedAndMaxLabel->setPosition(Point(selectAreaButton->getContentSize().width - 20, 5 + clearedAndMaxLabel->getContentSize().height / 2 - 5));
                    clearedAndMaxLabel->enableShadow();
                    selectAreaButton->addChild(clearedAndMaxLabel, 2);

                    auto star = makeSprite("rarity.png");
                    star->setPosition(Point(clearedAndMaxLabel->getPosition().x - clearedAndMaxLabel->getContentSize().width - 10 - star->getContentSize().width / 2, clearedAndMaxLabel->getPosition().y + 9));
                    selectAreaButton->addChild(star, 2);
                }
            }
        }
    }
    cell->autorelease();
    return cell;
}

void SelectArea::showPlaceOfMission(Sprite* selectAreaMenu, bool isClearArea)
{
    auto sprite = Sprite::create("mission_label.png");
    float positionY = selectAreaMenu->getContentSize().height - 2 - sprite->getContentSize().height / 2;
    if (isClearArea == true) {
        positionY = 2 + sprite->getContentSize().height / 2;
    }
    sprite->setPosition(Vec2(2 + sprite->getContentSize().width / 2, positionY));
    selectAreaMenu->addChild(sprite);
}

void SelectArea::tableCellHighlight (TableView* table, TableViewCell* cell)
{
    auto bg = (Sprite*)cell->getChildByTag(-100);
    if (bg != NULL) {
        bg->setVisible(true);
    }
}

void SelectArea::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
    auto bg = (Sprite*)cell->getChildByTag(-100);
    if (bg != NULL) {
        bg->setVisible(false);
    }
}

void SelectArea::tableCellTouched(TableView* table, TableViewCell* cell)
{
    if (-1 < cell->getTag() && cell->getTag() < tableData.size()) {
        std::map<DATA_KEY, int>data = tableData.at(cell->getTag());
        if (data[DATA_KEY::AREA_ID] < 1 || data[DATA_KEY::COMMING_SOON]) {
            return;
        }
        int selectedAreaId = data[DATA_KEY::AREA_ID];
        runAction(Sequence::create(DelayTime::create(0.2),
                                         CallFunc::create([&, selectedAreaId]() {
            getEventDispatcher()->removeEventListener(_listener);
            _onSelectAreaCallback(selectedAreaId);
            removeFromParent();
        }), NULL));
    }
}

void SelectArea::scrollViewDidScroll(ScrollView* view)
{
    _scrollBar->updateScrollPoint(_tableView->getContentOffset().y);
}