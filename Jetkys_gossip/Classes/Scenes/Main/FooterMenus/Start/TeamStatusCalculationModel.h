#ifndef __syanago__TeamStatusCalculationModel__
#define __syanago__TeamStatusCalculationModel__

#include "cocos2d.h"
#include <map>
#include "LeaderSkill.h"
#include "TeamStatus.h"

namespace STATUS{
    static const std::string POWOER = "powoer";
    static const std::string TECHNIQUE = "technique";
    static const std::string BRAKE = "brake";
};

namespace ITEM_EFECT{
    static const int POWOER = 11;
    static const int TECHNIQUE = 12;
    static const int BRAKE = 10;
};

class TeamStatusCalculationModel
{
public:
    TeamStatusCalculationModel(const int powoer, const int trchnique, const int brake, const int masterCharacterIdOfHelpPlayer, const bool isFriend);
    
    CC_SYNTHESIZE_READONLY(int, totalPower, TotalPower);
    CC_SYNTHESIZE_READONLY(int, totalTechnique, TotalTechnique);
    CC_SYNTHESIZE_READONLY(int, totalBrake, TotalBrake);
    
    CC_SYNTHESIZE_READONLY(int, viewPower, ViewPower);
    CC_SYNTHESIZE_READONLY(int, viewTechnique, ViewTechnique);
    CC_SYNTHESIZE_READONLY(int, viewBrake, ViewBrake);
    
    CC_SYNTHESIZE_READONLY(bool, isFriend, IsFriend);
private:
    int playerCharacterId[4];
    
    void setPlayerSelectTeamStatus(const int powoer, const int technique, const int brake, const int mstfrID, const bool frFlag);
    
    std::map<std::string, int> getPlayerLeaderCharacterStatus(LeaderSkill* leaderSkill);
    std::map<std::string, int> getPlayerTeamCharacterStatus(LeaderSkill* leaderSkill);
    std::map<std::string, int> getHelpCharacterStatus(const int mstCharacterId, const int powoer, const int technique, const int brake, LeaderSkill* leaderSkill);
    std::map<std::string, int> convertStatusEffectOfLeaderSkill(const int masterCharacterId, const int powoer, const int technique, const int brake, LeaderSkill* leaderSkill);
    std::map<std::string, float> getPlayerCharacterTreasuresEfectStatusRate();
    float getTreasuresEfectStatusRate(const int itemId);

};

#endif /* defined(__syanago__TeamStatusCalculationModel__) */
