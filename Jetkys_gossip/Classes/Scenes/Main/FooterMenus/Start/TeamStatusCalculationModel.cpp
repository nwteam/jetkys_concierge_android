#include "TeamStatusCalculationModel.h"
#include "PlayerController.h"
#include "SystemSettingModel.h"
#include "ItemModel.h"
#include "ItemUnitModel.h"
#include "BodyTypeModel.h"

TeamStatusCalculationModel::TeamStatusCalculationModel(const int power, const int trchnique, const int brake, const int masterCharacterIdOfHelpPlayer, const bool isFriend):
    totalPower(0), totalTechnique(0), totalBrake(0), viewPower(0), viewTechnique(0), viewBrake(0), isFriend(false)
{
    setPlayerSelectTeamStatus(power, trchnique, brake, masterCharacterIdOfHelpPlayer, isFriend);
};


void TeamStatusCalculationModel::setPlayerSelectTeamStatus(const int powoer, const int trchnique, const int brake, const int masterCharacterIdOfHelpPlayer, const bool isFriend)
{
    playerCharacterId[0] = (PLAYERCONTROLLER->_playerTeams)->getLearderCharacterId();
    playerCharacterId[1] = (PLAYERCONTROLLER->_playerTeams)->getSupport1CharacterId();
    playerCharacterId[2] = (PLAYERCONTROLLER->_playerTeams)->getSupport2CharacterId();
    playerCharacterId[3] = (PLAYERCONTROLLER->_playerTeams)->getSupport3CharacterId();

    int masterCharacterId = PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[0]]->getCharactersId();

    auto leaderSkill = new LeaderSkill(masterCharacterId, masterCharacterIdOfHelpPlayer, isFriend);
    // leaderSkill->viewLog();

    auto playerMenberStatus = getPlayerTeamCharacterStatus(leaderSkill);
    auto leaderCharacterStatus = getPlayerLeaderCharacterStatus(leaderSkill);
    auto helpPlayerCharacterStatus = getHelpCharacterStatus(masterCharacterIdOfHelpPlayer, powoer, trchnique, brake, leaderSkill);

    delete leaderSkill;
    leaderSkill = nullptr;
    std::shared_ptr<SystemSettingModel>systemSetting(SystemSettingModel::getModel());
    float plessParaCoef = systemSetting->getSupportCompressionRate();
    std::map<std::string, float>treasuresEfectRate = getPlayerCharacterTreasuresEfectStatusRate();

    viewPower     = leaderCharacterStatus[STATUS::POWOER]    + playerMenberStatus[STATUS::POWOER]    + helpPlayerCharacterStatus[STATUS::POWOER];
    viewTechnique = leaderCharacterStatus[STATUS::TECHNIQUE] + playerMenberStatus[STATUS::TECHNIQUE] + helpPlayerCharacterStatus[STATUS::TECHNIQUE];
    viewBrake     = leaderCharacterStatus[STATUS::BRAKE]     + playerMenberStatus[STATUS::BRAKE]     + helpPlayerCharacterStatus[STATUS::BRAKE];

    totalPower     = (leaderCharacterStatus[STATUS::POWOER]    + (helpPlayerCharacterStatus[STATUS::POWOER]    + playerMenberStatus[STATUS::POWOER])    * plessParaCoef) * treasuresEfectRate[STATUS::POWOER];
    totalTechnique = (leaderCharacterStatus[STATUS::TECHNIQUE] + (helpPlayerCharacterStatus[STATUS::TECHNIQUE] + playerMenberStatus[STATUS::TECHNIQUE]) * plessParaCoef) * treasuresEfectRate[STATUS::TECHNIQUE];
    totalBrake     = (leaderCharacterStatus[STATUS::BRAKE]     + (helpPlayerCharacterStatus[STATUS::BRAKE]     + playerMenberStatus[STATUS::BRAKE])     * plessParaCoef) * treasuresEfectRate[STATUS::BRAKE];
}

std::map<std::string, int>TeamStatusCalculationModel::getPlayerLeaderCharacterStatus(LeaderSkill* leaderSkill)
{
    std::map<std::string, int>result;
    result[STATUS::POWOER] = 0;
    result[STATUS::TECHNIQUE] = 0;
    result[STATUS::BRAKE] = 0;

    if (playerCharacterId[0] > 0) {
        auto convertCharacterData = convertStatusEffectOfLeaderSkill(PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[0]]->getCharactersId(),
                                                                     PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[0]]->getExercise(),
                                                                     PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[0]]->getReaction(),
                                                                     PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[0]]->getDecision(),
                                                                     leaderSkill);
        result[STATUS::POWOER] = convertCharacterData[STATUS::POWOER];
        result[STATUS::TECHNIQUE] = convertCharacterData[STATUS::TECHNIQUE];
        result[STATUS::BRAKE] = convertCharacterData[STATUS::BRAKE];
    }

    return result;
}

std::map<std::string, int>TeamStatusCalculationModel::getPlayerTeamCharacterStatus(LeaderSkill* leaderSkill)
{
    std::map<std::string, int>result;
    result[STATUS::POWOER] = 0;
    result[STATUS::TECHNIQUE] = 0;
    result[STATUS::BRAKE] = 0;
    for (int i = 0; i < 4; i++) {
        if (playerCharacterId[i] > 0) {
            auto convertCharacterData = convertStatusEffectOfLeaderSkill(PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[i]]->getCharactersId(),
                                                                         PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[i]]->getExercise(),
                                                                         PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[i]]->getReaction(),
                                                                         PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[i]]->getDecision(),
                                                                         leaderSkill);
            if (PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId[i]]->getID() != playerCharacterId[0]) {
                result[STATUS::POWOER] += convertCharacterData[STATUS::POWOER];
                result[STATUS::TECHNIQUE] += convertCharacterData[STATUS::TECHNIQUE];
                result[STATUS::BRAKE] += convertCharacterData[STATUS::BRAKE];
            }
        }
    }
    return result;
}

std::map<std::string, int>TeamStatusCalculationModel::convertStatusEffectOfLeaderSkill(const int masterCharacterId, const int powoer, const int technique, const int brake, LeaderSkill* leaderSkill)
{
    std::map<std::string, int>result;
    result[STATUS::POWOER] = powoer;
    result[STATUS::TECHNIQUE] = technique;
    result[STATUS::BRAKE] = brake;

    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(masterCharacterId));
    int bodyType = characterModel->getBodyType();
    int allBodyType = (BodyTypeModel::getMaxBodyTypeCount() + 1);
    if (bodyType == (int)leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::BODY_TYPE]) {
        result[STATUS::POWOER] = result[STATUS::POWOER] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::POWOER_RATE];
        result[STATUS::TECHNIQUE] = result[STATUS::TECHNIQUE] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::TECHNIQUE_RATE];
        result[STATUS::BRAKE] = result[STATUS::BRAKE] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::BRAKE_RATE];
    }

    if (allBodyType == (int)leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::BODY_TYPE]) {
        result[STATUS::POWOER] = result[STATUS::POWOER] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::POWOER_RATE];
        result[STATUS::TECHNIQUE] = result[STATUS::TECHNIQUE] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::TECHNIQUE_RATE];
        result[STATUS::BRAKE] = result[STATUS::BRAKE] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::BRAKE_RATE];
    }

    if (bodyType == (int)leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::BODY_TYPE]) {
        result[STATUS::POWOER] = result[STATUS::POWOER] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::POWOER_RATE];
        result[STATUS::TECHNIQUE] = result[STATUS::TECHNIQUE] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::TECHNIQUE_RATE];
        result[STATUS::BRAKE] = result[STATUS::BRAKE] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::BRAKE_RATE];
    }
    if (allBodyType == (int)leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::BODY_TYPE]) {
        result[STATUS::POWOER] = result[STATUS::POWOER] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::POWOER_RATE];
        result[STATUS::TECHNIQUE] = result[STATUS::TECHNIQUE] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::TECHNIQUE_RATE];
        result[STATUS::BRAKE] = result[STATUS::BRAKE] * leaderSkill->statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::BRAKE_RATE];
    }

    return result;
}

std::map<std::string, int>TeamStatusCalculationModel::getHelpCharacterStatus(const int masterCharacterId, int powoer, int technique, int brake, LeaderSkill* leaderSkill)
{
    return convertStatusEffectOfLeaderSkill(masterCharacterId, powoer, technique, brake, leaderSkill);
}

std::map<std::string, float>TeamStatusCalculationModel::getPlayerCharacterTreasuresEfectStatusRate()
{
    std::map<std::string, float>result;

    result[STATUS::POWOER] = 0;
    result[STATUS::TECHNIQUE] = 0;
    result[STATUS::BRAKE] = 0;

    const int itemCount = ItemModel::getMaxItemCount();
    for (int i = 1; i <= itemCount; i++) {
        std::shared_ptr<ItemModel>itemModel(ItemModel::find(i));
        switch (itemModel->getEffectId()) {
        case ITEM_EFECT::BRAKE: {
            result[STATUS::BRAKE] += getTreasuresEfectStatusRate(itemModel->getID());
        }
        case ITEM_EFECT::POWOER: {
            result[STATUS::POWOER] += getTreasuresEfectStatusRate(itemModel->getID());
        }
        case ITEM_EFECT::TECHNIQUE: {
            result[STATUS::TECHNIQUE] += getTreasuresEfectStatusRate(itemModel->getID());
        }
        default:
            break;
        }
    }
    result[STATUS::POWOER] = (result[STATUS::POWOER] * 0.01) + 1;
    result[STATUS::TECHNIQUE] = (result[STATUS::TECHNIQUE] * 0.01) + 1;
    result[STATUS::BRAKE] = (result[STATUS::BRAKE] * 0.01) + 1;
    return result;
}

float TeamStatusCalculationModel::getTreasuresEfectStatusRate(const int itemId)
{
    float result = 0.0;
    if (itemId > 0) {
        return result;
    }
    const int itemUnitCount = ItemUnitModel::getMaxItemUnitCount();
    const int playerItemUnitCount = (int)PLAYERCONTROLLER->_playerItems.size();
    int itemTotalStar = 0;
    int itemCount = 0;
    for (int j = 1; j <= itemUnitCount; j++) {
        std::shared_ptr<ItemUnitModel>itemUnitModel(ItemUnitModel::find(j));
        if (itemId == itemUnitModel->getItemId()) {
            const int itemUnitId = itemUnitModel->getID();
            for (int k = 0; k < playerItemUnitCount; k++) {
                if (itemUnitId == PLAYERCONTROLLER->_playerItems.at(k)->getItemUnitId()) {
                    itemCount++;
                    itemTotalStar += PLAYERCONTROLLER->_playerItems.at(k)->getItemUnitKind();
                }
            }
        }
    }
    if (itemCount == 3) {
        std::shared_ptr<ItemModel>itemModel(ItemModel::find(itemId));
        switch (itemTotalStar) {
        case 3: {
            result = itemModel->getStar3();
            break;
        }
        case 4: {
            result = itemModel->getStar4();
            break;
        }
        case 5: {
            result = itemModel->getStar5();
            break;
        }
        case 6: {
            result = itemModel->getStar6();
            break;
        }
        case 7: {
            result = itemModel->getStar7();
            break;
        }
        case 8: {
            result = itemModel->getStar8();
            break;
        }
        case 9: {
            result = itemModel->getStar9();
            break;
        }
        default:
            result = 0.0;
            break;
        }
    }
    return result;
}