#include "FriendSearchModalLayer.h"
#include "FriendLayer.h"
#include "FriendSearchLayer.h"
#include "FontDefines.h"
#include "DialogView.h"
#include "PlayerController.h"

#define FONT_SIZE_1 45
#define FONT_SIZE_2 30


void FriendSearchModalLayerDelegate::removeCallback() {}

FriendSearchModalLayer::FriendSearchModalLayer():
    _progress(nullptr)
    , _request(nullptr)
{
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}

FriendSearchModalLayer::~FriendSearchModalLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

FriendSearchModalLayer* FriendSearchModalLayer::createLayer(Json* json)
{
    auto layer = new FriendSearchModalLayer();
    layer->_friendJson = json;
    layer->init();
    layer->autorelease();
    // layer->setOpacity(128);
    return layer;
}
bool FriendSearchModalLayer::init()
{
    if (!Layer::init()) {
        return false;
    }
    _delegate = nullptr;

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [](Touch* touch, Event* event) -> bool {
                                 return true;
                             };
    auto dip = Director::getInstance()->getEventDispatcher();
    dip->addEventListenerWithSceneGraphPriority(listener, this);
    dip->setPriority(listener, -1);

    showInformation();


    return true;
}


void FriendSearchModalLayer::showInformation()
{
    Size winSize = Director::getInstance()->getWinSize();

    auto background = makeSprite("dialog_base.png");
    addChild(background, 0);
    background->setPosition(Point(winSize.width / 2, winSize.height / 2));

    // マスター車なご情報を表示
    // フレンド申請ラベル
    auto titleLabel = Label::createWithTTF("フレンド申請", FONT_NAME_2, 28);
    titleLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    titleLabel->setAnchorPoint(Vec2(0.5, 1));
    titleLabel->setPosition(Point(background->getContentSize().width / 2,
                                  background->getContentSize().height - 30 - titleLabel->getContentSize().height / 2));
    background->addChild(titleLabel);

    // アイコン
    int mst_id = atoi(Json_getString(_friendJson, "mst_characters_id", ""));
    ////CCLOG("ID: %D",mst_id);
    // auto iconImage = );
    auto icon = makeMenuItem(StringUtils::format("%d_i.png", mst_id).c_str(), CC_CALLBACK_1(FriendSearchModalLayer::iconCallback, this));
    icon->setAnchorPoint(Point(0, 1));
    icon->setPosition(Point(Point(30, titleLabel->getPosition().y - 14 - titleLabel->getContentSize().height / 2)));
    background->addChild(icon);

    // プレイヤー名
    auto levelLabel = Label::createWithTTF("名前:" + std::string(Json_getString(_friendJson, "player_name", "")), FONT_NAME_2, 22);
    levelLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    levelLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    background->addChild(levelLabel);
    levelLabel->setAnchorPoint(Vec2(0, 0.5));
    levelLabel->setPosition(Point(25.0f + icon->getPosition().x + icon->getContentSize().width,
                                  icon->getPosition().y - 22));

    // オーナーランク
    auto exerciseLabel = Label::createWithTTF("オーナーランク:" + std::string(Json_getString(_friendJson, "rank", "")), FONT_NAME_2, 22);
    exerciseLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    exerciseLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    background->addChild(exerciseLabel);
    exerciseLabel->setAnchorPoint(Vec2(0, 0.5));
    exerciseLabel->setPosition(Point(levelLabel->getPositionX(),
                                     levelLabel->getPositionY() - 27 - 11));

    // 最終ログイン日時を表示
    auto lastLoginLabel = Label::createWithTTF("最終プレイ：" + FriendLayer::lastLoginTime(_friendJson), FONT_NAME_2, 22);
    lastLoginLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    lastLoginLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    background->addChild(lastLoginLabel);
    lastLoginLabel->setAnchorPoint(Vec2(0, 0.5));
    lastLoginLabel->setPosition(Point(exerciseLabel->getPositionX(),
                                      exerciseLabel->getPositionY() - 27 - 11));

    //「はい」ボタン
    auto yesBtn = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(FriendSearchModalLayer::yesBtnCallback, this));
    auto yesBtnLabel = Label::createWithTTF("フレンド申請", FONT_NAME_2, 22);
    yesBtnLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    yesBtn->addChild(yesBtnLabel);
    yesBtnLabel->setPosition(Point(yesBtn->getContentSize().width / 2, yesBtn->getContentSize().height / 2 - 11));

    //「いいえ」ボタン
    auto noBtn = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(FriendSearchModalLayer::noBtnCallback, this));
    auto noBtnLabel = Label::createWithTTF("キャンセル", FONT_NAME_2, 22);
    noBtnLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    noBtn->addChild(noBtnLabel);
    noBtnLabel->setPosition(Point(noBtn->getContentSize().width / 2, noBtn->getContentSize().height / 2 - 11));

    // メニュー作成
    auto menu = MenuTouch::create(yesBtn, noBtn, NULL);
    addChild(menu);
    menu->setAnchorPoint(Point(0.5, 1));
    menu->setPosition(Point(winSize.width / 2,
                            background->getPosition().y - background->getContentSize().height / 2 + 30 + noBtn->getContentSize().height / 2));
    menu->alignItemsHorizontallyWithPadding(30);
}
// マスター車なごの詳細に遷移
void FriendSearchModalLayer::iconCallback(Ref* pSender)
{
    //
}

// フレンド申請リクエストを送信
void FriendSearchModalLayer::yesBtnCallback(Ref* pSender)
{
    _progress->onStart();
    _request->setFriendRequest(atoi(Json_getString(_friendJson, "player_id", "")), CC_CALLBACK_1(FriendSearchModalLayer::responseSetFriendRequest, this));
}

void FriendSearchModalLayer::responseSetFriendRequest(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "set_friend_request");
    std::string result = Json_getString(jsonData1, "result", "false");

    if (result == "true") {
        //「成功」ダイアログを表示
        _dialog = DialogView::createDialog("フレンド申請を送りました", "フレンド申請", 1);
        addChild(_dialog, DIALOG_ZORDER);
        _dialog->_delegate  = this;
    }
}

// 戻る
void FriendSearchModalLayer::noBtnCallback(Ref* pSender)
{
    ////CCLOG("FriendListModalLayer::noBtnCallback");
    removeFromParent();
}

void FriendSearchModalLayer::btDialogCallback(ButtonIndex aIndex)
{
    // モーダルレイヤーを削除
    removeFromParent();
    if (_delegate != nullptr) {
        _delegate->removeCallback();
    }
}
