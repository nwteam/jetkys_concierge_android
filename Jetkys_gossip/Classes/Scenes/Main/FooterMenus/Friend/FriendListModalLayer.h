#ifndef __Syanago__FriendListModalLayer__
#define __Syanago__FriendListModalLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "DialogView.h"
#include "jcommon.h"
#include "DetailLayer.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace SyanagoAPI;

class FriendListModalLayerDelegate {
public:
    virtual void removeCallback();
    virtual void deleteCallback();
    virtual void viewMenuCallBack();
    virtual void hiddenMenuCallBack();
};

class FriendListModalLayer: public Layer, public DialogDelegate
{
public:
    static FriendListModalLayer* createLayer(Json* json);
    FriendListModalLayer();
    virtual ~FriendListModalLayer();
    bool init();
    CREATE_FUNC(FriendListModalLayer);

    void btInformationCallback(Ref* pSender);
    void btDeleteCallback(Ref* pSender);
    void btBackCallback(Ref* pSender);
    void btDialogCallback(ButtonIndex aIndex);
    FriendListModalLayerDelegate* _delegate;
    Json* _friendJson;
    DialogView* _dialog;
private:
    DefaultProgress* _progress;
    RequestAPI* _request;

    void responseApproveFriends(Json* response);
};

#endif /* defined(__Syanago__Loading__) */