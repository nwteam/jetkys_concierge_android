#include "FriendLayer.h"
#include "jCommon.h"
#include "FontDefines.h"
#include "MainScene.h"

#include "FriendMainLayer.h"
#include "FriendSearchLayer.h"
#include "FriendListLayer.h"
#include "FriendApproveLayer.h"
#include "FriendInvateLayer.h"
#include "RankingLayer.h"
#include "BrowserLauncher.h"
#include "HeaderStatus.h"
#include "TimeConverter.h"

#define tag_dialog_invate 1


FriendLayer::FriendLayer():
    _mainScene(nullptr)
{}

bool FriendLayer::init()
{
    if (!Layer::init()) {
        return false;
    }
    _friendIndex = FI_MAIN;

    Size winSize = Director::getInstance()->getWinSize();

    // background
    showLayer(FI_MAIN);

    // 動作する背景を追加
    auto BgSp = Sprite::create("home_bg.png");
    addChild(BgSp, -1);
    BgSp->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    /*
       MoveTiledSprite* tileBG = MoveTiledSprite::createWithSprite("roopBackground.png" , 1 , 10);
       addChild(tileBG, -1);*/

    return true;
}

void FriendLayer::resetPageTag()
{
    removeAllChildren();
    init();
}

void FriendLayer::updateInfoLayer()
{
    _mainScene->headerLayer->updateInfoLayer();
}

void FriendLayer::showLayer(FriendIndex nextLayer)
{
    if (nextLayer == _friendIndex && getChildByTag(_friendIndex)) {
        return;
    }

    auto currentLayer = getChildByTag(_friendIndex);
    if (currentLayer) {
        currentLayer->removeFromParent();
    }
    bool footerVisible = true;
    _friendIndex = nextLayer;
    switch (_friendIndex) {
    case FI_MAIN: {
        auto layer = FriendMainLayer::create();
        addChild(layer);
        layer->setTag(FI_MAIN);
        layer->_friendLayer = this;
        break;
    }
    case FI_LIST: {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        auto layer = FriendListLayer::create();
        addChild(layer);
        layer->setTag(FI_LIST);
        layer->_friendLayer = this;
        break;
    }
    case FI_APPROVAL: {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        auto layer = FriendApproveLayer::create();
        addChild(layer);
        layer->setTag(FI_APPROVAL);
        layer->_friendLayer = this;
        break;
    }

    case FI_ID_SEARCH: {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        auto layer = FriendSearchLayer::create();
        addChild(layer);
        layer->setTag(FI_ID_SEARCH);
        layer->_friendLayer = this;
        break;
    }
    case FI_INVATE: {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        auto dialog = DialogView::createBrowserConfirm(tag_dialog_invate);
        addChild(dialog, LOADING_ZORDER);
        dialog->_delegate = this;
        showLayer(FI_MAIN);
        break;
    }
    case FI_RANKING: {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        auto layer = RankingLayer::create(CC_CALLBACK_1(FriendLayer::showMainLayer, this));
        layer->setTag(FI_RANKING);
        addChild(layer);
        footerVisible = false;
    }
    default:
        break;
    }
    if (_mainScene != nullptr) {
        _mainScene->footerLayer->setVisible(footerVisible);
    }
}

void FriendLayer::showMainLayer(Ref* pSender)
{
    if (FI_MAIN == _friendIndex && getChildByTag(_friendIndex)) {
        return;
    }
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    auto currentLayer = getChildByTag(_friendIndex);
    if (currentLayer) {
        currentLayer->removeFromParent();
    }
    auto layer = FriendMainLayer::create();
    layer->setTag(FI_MAIN);
    layer->_friendLayer = this;
    addChild(layer);

    _friendIndex = FI_MAIN;

    if (_mainScene != nullptr) {
        _mainScene->footerLayer->setVisible(true);
    }
}



// 最終ログインからの日数を算出
std::string FriendLayer::lastLoginTime(Json* json)
{
    // 2014-08-26 13 : 07 : 43
    //上記の様な値を年月日に分ける
    std::string stdLoginTime = Json_getString(json, "last_logged", "");
    return TimeConverter::convertLastLogInTimeString(stdLoginTime);
}

void FriendLayer::hiddenMenu()
{
    _mainScene->footerLayer->setVisible(false);
    _mainScene->headerLayer->setVisible(false);
}

void FriendLayer::viewMenu()
{
    _mainScene->footerLayer->setVisible(true);
    _mainScene->headerLayer->setVisible(true);
}

void FriendLayer::showFriendCharacter(int playerMstCharacterId, int level, int exercise, int reaction, int decision)
{
    _mainScene->showFriendCharacter(playerMstCharacterId, level, exercise, reaction, decision);
}

void FriendLayer::playSoundEfect(MainSceneSoundType type, bool sloop)
{
    SOUND_HELPER->playeMainSceneEffect(type, sloop);
}

void FriendLayer::updateBage()
{
    _mainScene->footerLayer->setBage();
}

// FriendLayerBase
FriendLayerBase::FriendLayerBase()
{
    _title = "FriendLayerBase";
}

bool FriendLayerBase::init()
{
    if (!Layer::init()) {
        return false;
    }

    Size winSize = Director::getInstance()->getWinSize();

    auto shopTitleBg = makeSprite("head_line_no1.png");
    addChild(shopTitleBg);
    int widthBg = shopTitleBg->getContentSize().width / 2;
    shopTitleBg->setPosition(Vec2(widthBg,
                                  winSize.height - shopTitleBg->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 12));

    auto btBack = makeMenuItem("back_button.png", CC_CALLBACK_1(FriendLayerBase::btMenuItemCallback, this));
    auto menuBack = MenuTouch::create(btBack, NULL);
    addChild(menuBack);
    menuBack->setPosition(Vec2(shopTitleBg->getPosition().x - shopTitleBg->getContentSize().width / 2 + 14 + btBack->getNormalImage()->getContentSize().width / 2,
                               shopTitleBg->getPosition().y));

    auto title = Label::createWithTTF(_title, FONT_NAME_2, 34);
    title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    addChild(title);
    title->setAnchorPoint(Vec2(0.0f, 0.5f));
    title->setPosition(menuBack->getPosition() + Point(btBack->getContentSize().width / 2 + 10, -17));
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    return true;
}

void FriendLayerBase::btMenuItemCallback(cocos2d::Ref* pSender)
{
    _friendLayer->playSoundEfect(SOUND_TAP_FALSE, false);
    _friendLayer->showLayer(FI_MAIN);
}

void FriendLayer::btDialogCallback(ButtonIndex aIndex, int tag)
{
    if (aIndex == BT_YES) {
        std::string url;
        if (tag == tag_dialog_invate) {
            url = UserDefault::getInstance()->getStringForKey("domein") + "/webview/invitation_reward_form";
            Cocos2dExt::BrowserLauncher::launchUrl(url.c_str());
        }
    }
}


