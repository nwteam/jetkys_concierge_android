#include "FriendListModalLayer.h"
#include "FriendLayer.h"
#include "FriendListLayer.h"
#include "FontDefines.h"
#include "DialogView.h"
#include "PlayerController.h"
#include "SoundHelper.h"
#include "CharacterModel.h"

#define FONT_SIZE_1 32
#define FONT_SIZE_2 30


void FriendListModalLayerDelegate::removeCallback() {}

void FriendListModalLayerDelegate::hiddenMenuCallBack() {}

void FriendListModalLayerDelegate::viewMenuCallBack() {}

void FriendListModalLayerDelegate::deleteCallback() {}

FriendListModalLayer::FriendListModalLayer():
    _progress(nullptr)
    , _request(nullptr)
{
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}

FriendListModalLayer::~FriendListModalLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

FriendListModalLayer* FriendListModalLayer::createLayer(Json* json)
{
    auto layer = new FriendListModalLayer();
    layer->_friendJson = json;
    layer->init();
    layer->autorelease();
    return layer;
}
bool FriendListModalLayer::init()
{
    if (!Layer::init()) {
        return false;
    }
    _delegate = nullptr;
    Size winSize = Director::getInstance()->getWinSize();

    auto background = makeSprite("friend_popup_base.png");
    addChild(background, 0);
    background->setPosition(Point(winSize.width / 2, winSize.height / 2));

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [](Touch* touch, Event* event) -> bool {
                                 return true;
                             };
    auto dip = Director::getInstance()->getEventDispatcher();
    dip->addEventListenerWithSceneGraphPriority(listener, this);
    dip->setPriority(listener, -1);

    // マスター車なご情報を表示
    // プレイヤー名
    auto playerNameLabel = Label::createWithTTF(Json_getString(_friendJson, "player_name", ""), FONT_NAME_2, FONT_SIZE_1);
    playerNameLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    background->addChild(playerNameLabel);
    playerNameLabel->setAnchorPoint(Vec2(0.5, 1));
    playerNameLabel->setPosition(Vec2(background->getContentSize()) + Vec2(-background->getContentSize().width / 2, -30));
    playerNameLabel->setColor(COLOR_YELLOW);
    playerNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    // 車なご名
    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(atoi(Json_getString(_friendJson, "mst_characters_id", ""))));
    auto charaName = characterModel->getCarName();
    auto charaNameLabel = Label::createWithTTF(charaName, FONT_NAME_2, 28);
    charaNameLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    background->addChild(charaNameLabel);
    charaNameLabel->setAnchorPoint(Vec2(0.5, 1));
    charaNameLabel->setPosition(Point(playerNameLabel->getPosition().x,
                                      playerNameLabel->getPosition().y - 25 - 18));
    charaNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    // アイコン
    int mst_id = atoi(Json_getString(_friendJson, "mst_characters_id", ""));
    auto icon = makeSprite(StringUtils::format("%d_i.png", mst_id).c_str());
    background->addChild(icon);
    icon->setPosition(Vec2(background->getContentSize().width / 2 - icon->getContentSize().width / 2 - 20, playerNameLabel->getPositionY() - playerNameLabel->getContentSize().height - 16 - icon->getContentSize().height / 2 - 5));


    // レベル
    auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %s", Json_getString(_friendJson, "level", "")), FONT_NAME_2, 22);
    icon->addChild(levelLabel, 4);
    levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    levelLabel->setPosition(Vec2(icon->getContentSize().width / 2, 6 - 11));
    levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);

    // ステータス
    auto statusLabel = Label::createWithTTF("馬力", FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    background->addChild(statusLabel);
    statusLabel->setPosition(icon->getPosition() + Vec2(icon->getContentSize().width / 2 + 10, 30 - 10));
    statusLabel->setAnchorPoint(Vec2(0, 0.5));
    statusLabel->enableShadow();

    auto statusValue = Label::createWithTTF(FormatWithCommas(atoi(Json_getString(_friendJson, "exercise", ""))), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    background->addChild(statusValue);
    statusValue->setPosition(statusLabel->getPosition() + Vec2(125, 0));
    statusValue->setAnchorPoint(Vec2(1, 0.5));
    statusValue->enableShadow();

    statusLabel = Label::createWithTTF("技量", FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    background->addChild(statusLabel);
    statusLabel->setPosition(icon->getPosition() + Vec2(icon->getContentSize().width / 2 + 10, -10));
    statusLabel->setAnchorPoint(Vec2(0, 0.5));
    statusLabel->enableShadow();

    statusValue = Label::createWithTTF(FormatWithCommas(atoi(Json_getString(_friendJson, "reaction", ""))), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    background->addChild(statusValue);
    statusValue->setPosition(statusLabel->getPosition() + Vec2(125, 0));
    statusValue->setAnchorPoint(Vec2(1, 0.5));
    statusValue->enableShadow();

    statusLabel = Label::createWithTTF("忍耐", FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::LEFT, TextVAlignment::CENTER);
    background->addChild(statusLabel);
    statusLabel->setPosition(icon->getPosition() + Vec2(icon->getContentSize().width / 2 + 10, -30 - 10));
    statusLabel->setAnchorPoint(Vec2(0, 0.5));
    statusLabel->enableShadow();

    statusValue = Label::createWithTTF(FormatWithCommas(atoi(Json_getString(_friendJson, "decision", ""))), FONT_NAME_2, 20, Size(Vec2::ZERO), TextHAlignment::RIGHT, TextVAlignment::CENTER);
    background->addChild(statusValue);
    statusValue->setPosition(statusLabel->getPosition() + Vec2(125,  0));
    statusValue->setAnchorPoint(Vec2(1, 0.5));
    statusValue->enableShadow();

    // 情報ボタン
    auto btInformation = makeMenuItem("te_popup_button.png", CC_CALLBACK_1(FriendListModalLayer::btInformationCallback, this));
    auto informationLabel = Label::createWithTTF("詳細情報", FONT_NAME_2, 32);
    btInformation->addChild(informationLabel);
    informationLabel->setPosition(Point(btInformation->getContentSize().width / 2,
                                        btInformation->getContentSize().height / 2 - 16));
    informationLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    // 削除ボタン
    auto btDelete = makeMenuItem("te_popup_button.png", CC_CALLBACK_1(FriendListModalLayer::btDeleteCallback, this));
    auto deleteLabel = Label::createWithTTF("フレンド削除", FONT_NAME_2, 32);
    btDelete->addChild(deleteLabel);
    deleteLabel->setPosition(Point(btDelete->getContentSize().width / 2,
                                   btDelete->getContentSize().height / 2 - 16));
    deleteLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    // 戻るボタン
    auto btBack = makeMenuItem("te_popup_close_button.png", CC_CALLBACK_1(FriendListModalLayer::btBackCallback, this));

    auto menu = MenuTouch::create(btInformation, btDelete, btBack, NULL);
    addChild(menu);
    menu->setAnchorPoint(Point(0.5, 1));
    menu->setPosition(Point(winSize.width / 2,
                            winSize.height / 2 - background->getContentSize().height / 2 + 30 + btBack->getContentSize().height * 1.5 + 30));
    menu->alignItemsVerticallyWithPadding(30);

    return true;
}

// 車なごの詳細情報を表示
void FriendListModalLayer::btInformationCallback(Ref* pSender)
{
    ////CCLOG("FriendListModalLayer::btInformationCallback");
    SoundHelper* soundManeger;
    soundManeger->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    // ここに情報を表示するメソッドが入る
    ((FriendListLayer*)_delegate)->showFriendCharacter(atoi(Json_getString(_friendJson, "mst_characters_id", "")), atoi(Json_getString(_friendJson, "level", "")), atoi(Json_getString(_friendJson, "exercise", "")), atoi(Json_getString(_friendJson, "reaction", "")), atoi(Json_getString(_friendJson, "decision", "")));
}

// プレイヤーをフレンドリストから削除
void FriendListModalLayer::btDeleteCallback(Ref* pSender)
{
    ////CCLOG("FriendListModalLayer::btDeleteCallback");
    SoundHelper* soundManeger;
    soundManeger->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    _dialog = DialogView::createDialog("本当に削除しますか？", "", 2);
    addChild(_dialog, DIALOG_ZORDER);
    _dialog->_delegate  = this;
}

void FriendListModalLayer::btDialogCallback(ButtonIndex aIndex)
{
    SoundHelper* soundManeger;


    if (aIndex == BT_YES) {
        soundManeger->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        // フレンドリスト削除リクエスト送信
        _progress->onStart();
        _request->approveFriends(3, atoi(Json_getString(_friendJson, "player_id", "")), CC_CALLBACK_1(FriendListModalLayer::responseApproveFriends, this));
    } else {
        soundManeger->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }
}

void FriendListModalLayer::responseApproveFriends(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "approve_friends");
    std::string result = Json_getString(jsonData1, "result", "false");
    if (result == "true") {
        // このレイヤーを削除
        removeFromParent();
        if (_delegate != nullptr) {
            _delegate->deleteCallback();
        }
    }
}

// 前のレイヤーに戻る
void FriendListModalLayer::btBackCallback(Ref* pSender)
{
    ////CCLOG("FriendListModalLayer::btBackCallback");
    SoundHelper* soundManeger;
    soundManeger->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    // モーダルレイヤーを削除
    removeFromParent();
    if (_delegate != nullptr) {
        _delegate->removeCallback();
    }
}
