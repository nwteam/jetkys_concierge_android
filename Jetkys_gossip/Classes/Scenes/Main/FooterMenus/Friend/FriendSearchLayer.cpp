#include <time.h>

#include "FriendSearchLayer.h"
#include "FriendSearchModalLayer.h"
#include "FriendLayer.h"
#include "FontDefines.h"
#include "Cell.h"
#include "DialogView.h"
#include "PlayerController.h"
#include "TweetButton.h"
#include "TweetMessage.h"

#define SIZE_PLAYER_CELL Size(550.0f, 140.0f)



FriendSearchLayer::FriendSearchLayer():
    _progress(nullptr)
    , _request(nullptr)
{
    _title = " フレンド検索";
    _progress = new  DefaultProgress();
    _request = new RequestAPI(_progress);
}

FriendSearchLayer::~FriendSearchLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

bool FriendSearchLayer::init()
{
    if (!FriendLayerBase::init()) {
        return false;
    }

    auto winSize = Director::getInstance()->getWinSize();

    _player = std::vector<int>();

    auto friendBase = makeSprite("friend_idserch_base.png");
    addChild(friendBase);
    friendBase->setPosition(Point(winSize.width / 2,
                                  winSize.height / 2));

    // 自身のIDを表示
    auto yourIDLabel = Label::createWithTTF("あなたのID", FONT_NAME_2, 28);
    yourIDLabel->setAnchorPoint(Point(0.5, 0.5));
    yourIDLabel->setColor(COLOR_YELLOW);
    yourIDLabel->setPosition(Point(friendBase->getContentSize().width / 2, friendBase->getContentSize().height - 30 - yourIDLabel->getContentSize().height / 2));
    friendBase->addChild(yourIDLabel);
    yourIDLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto yourIDbase = makeSprite("friend_input_base.png");
    friendBase->addChild(yourIDbase);
    yourIDbase->setPosition(Point(friendBase->getContentSize().width / 2,
                                  friendBase->getContentSize().height - 30 - 14 - 14 - 14 - yourIDbase->getContentSize().height / 2));

    auto yourIDValue = Label::createWithTTF(PLAYERCONTROLLER->_player->getContactCode(), FONT_NAME_2, 28);
    yourIDValue->setColor(Color3B::BLACK);
    yourIDValue->setAnchorPoint(Point(0.5, 0.5));
    yourIDValue->setPosition(Point(yourIDbase->getContentSize().width / 2, yourIDbase->getContentSize().height / 2 - 14));
    yourIDbase->addChild(yourIDValue);

    // フレンド検索の文章を表示
    auto searchLabel = Label::createWithTTF("フレンド検索", FONT_NAME_2, 28);
    searchLabel->setColor(COLOR_YELLOW);
    searchLabel->setAnchorPoint(Point(0.5, 0.5));
    searchLabel->setPosition(Point(yourIDbase->getPosition().x,
                                   yourIDbase->getPosition().y - yourIDbase->getContentSize().height / 2 - 30 - searchLabel->getContentSize().height / 2));
    friendBase->addChild(searchLabel);
    searchLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto searchInfo1Label = Label::createWithTTF("友達のIDを入力して", FONT_NAME_2, 22);
    friendBase->addChild(searchInfo1Label);
    searchInfo1Label->setPosition(Point(yourIDbase->getPosition().x,
                                        searchLabel->getPosition().y - 5 - searchLabel->getContentSize().height / 2));

    auto searchInfo2Label = Label::createWithTTF("フレンド申請をしよう!!", FONT_NAME_2, 22);
    friendBase->addChild(searchInfo2Label);
    searchInfo2Label->setPosition(Point(yourIDbase->getPosition().x,
                                        searchInfo1Label->getPosition().y - 5 - searchInfo1Label->getContentSize().height / 2));

    // 入力ボックスを表示
    _editBox = cocos2d::ui::EditBox::create(Size(270, 46), Scale9Sprite::create("friend_input_base.png"));
    _editBox->setText("");
    friendBase->addChild(_editBox);
    _editBox->setPosition(Point(yourIDbase->getPosition().x,
                                searchInfo2Label->getPosition().y - 10 - searchInfo2Label->getContentSize().height / 2));
    _editBox->setPlaceHolder("");
    _editBox->setMaxLength(10);
    _editBox->setInputMode(EditBox::InputMode::SINGLE_LINE);
    _editBox->setFontColor(Color3B::BLACK);
    _editBox->setReturnType(EditBox::KeyboardReturnType::DONE);

    // 検索ボタンを表示
    auto searchBtn = makeMenuItem("friend_ok_button.png", CC_CALLBACK_1(FriendSearchLayer::btMenuItemCallback, this));
    searchBtn->setPosition(Point(winSize.width / 2,
                                 friendBase->getPosition().y - friendBase->getContentSize().height / 2 + 20 + searchBtn->getContentSize().height / 2));
    auto menu = MenuTouch::create(searchBtn, NULL);
    addChild(menu);
    menu->setPosition(Point::ZERO);


    auto searchBtnLabel = Label::createWithTTF("OK", FONT_NAME_2, 22);
    searchBtn->addChild(searchBtnLabel);
    searchLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    searchBtnLabel->setPosition(Point(searchBtn->getContentSize().width / 2, searchBtn->getContentSize().height / 2 - 11));
    searchBtnLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    Twitter::TweetButton* tweetButton = Twitter::TweetButton::create(TweetMessage::getFriendTweetMessage(), "tweet_button.png");
    tweetButton->setPosition(friendBase->getPosition() + Vec2(0, -(friendBase->getContentSize().height / 2 + 30 + tweetButton->getContentSize().height / 2)));
    tweetButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    addChild(tweetButton);

    return true;
}

void FriendSearchLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    _friendLayer->playSoundEfect(SOUND_TAP_TRUE, false);
    ////CCLOG("FriendSearchLayer::btMenuItemCallback");
    ////CCLOG("input id = %s", _editBox->getText());
    std::string sendID = _editBox->getText();
    // 空欄時の処理を入れる
    ////CCLOG("自分のコード:[%s],入力コード:[%s]", PLAYERCONTROLLER->_player->getContactCode().c_str(),_editBox->getText());
    int ret = strcmp(sendID.c_str(), PLAYERCONTROLLER->_player->getContactCode().c_str());
    int ret2 = strcmp(sendID.c_str(), "");
    if (ret == 0 || ret2 == 0) {
        std::string mes;
        if (ret == 0) {
            mes = "自分を検索する事は出来ません";
        } else if (ret2 == 0) {
            mes = "IDを入力してください";
        }
        _dialog = DialogView::createWithShortMessage(mes, 1);
        addChild(_dialog, DIALOG_ZORDER);
        return;
    }

    _progress->onStart();
    _request->getFriendList(0, sendID, CC_CALLBACK_1(FriendSearchLayer::responseFriendList, this));
}

void FriendSearchLayer::responseFriendList(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "get_friend_list");
    std::string result = Json_getString(jsonData1, "result", "false");

    if (result == "true") {
        Json* mDatas = Json_getItem(jsonData1, "data");
        if (mDatas && mDatas->type == Json_Array) {
            // モーダルレイヤを生成
            auto layer = FriendSearchModalLayer::createLayer(mDatas->child);
            addChild(layer);
            layer->_delegate = this;
        }
    }
}

void FriendSearchLayer::removeCallback()
{
    _editBox->setText("");
}
