#ifndef __syanago__FriendInvateLayer__
#define __syanago__FriendInvateLayer__

#include "cocos2d.h"
#include "FriendLayer.h"
#include "DialogView.h"
#include "FontDefines.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
using namespace SyanagoAPI;

#include "ui/CocosGUI.h"
using namespace cocos2d::ui;

class FriendLayer;

class FriendInviteLayer : public FriendLayerBase{
public:
    FriendInviteLayer();
    ~FriendInviteLayer();
    bool init();
    CREATE_FUNC(FriendInviteLayer);
    
    void menuItemCallback(Ref* pSender);
    
    void makeInvateView();
private:
    DefaultProgress* _progress;
    RequestAPI* _request;
    
    cocos2d::ui::EditBox *_editBox;
    
    void responseGetInvitationReward(Json* response);
    
    
};

#endif /* defined(__syanago__FriendInvateLayer__) */
