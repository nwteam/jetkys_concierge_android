#ifndef __syanago__FriendApproveLayer__
#define __syanago__FriendApproveLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Loading.h"
#include "FriendLayer.h"
#include "ScrollBar.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace SyanagoAPI;

class Cell;

class FriendApproveLayer : public FriendLayerBase, public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate{
public:
    FriendApproveLayer();
    virtual ~FriendApproveLayer();
    
    bool init();
    CREATE_FUNC(FriendApproveLayer);
    
    void onEnterTransitionDidFinish();
    
    void showTable();
    
    void btMenuItemTableCallback(Ref *pSender);
    
    //create cell with idx of table view
    Cell * cellWithIdx(int idx);
    
    //Table view
    void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {}
    virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
    virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);
    
    void btDialogCallback(ButtonIndex aIndex);
    
    std::string lastLoginTime(Json *json);
    Layer *_layer;
    
    void showNoListLabel();
    
private:
    DefaultProgress* _progress;
    RequestAPI* _request;
    
    void responseFriendList(Json* response);
    void responseApprove(Json* response);
    
    int _numberOfCell;
    std::vector<int> _player;
    std::vector<Json*> _friends;
    int _friendIdx;
    int _approveOrReject;
    ScrollBar* _scrollBar;
    
    void showFriendCount(int friendCount);
    
    bool _getFriendListFlag;
};

#endif /* defined(__syanago__FriendApproveLayer__) */
