#ifndef __Syanago__FriendSearchModalLayer__
#define __Syanago__FriendSearchModalLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "DialogView.h"
#include "Loading.h"
#include "jcommon.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace SyanagoAPI;

class FriendSearchModalLayerDelegate{
public:
    virtual void removeCallback();
};

class FriendSearchModalLayer : public Layer, public DialogDelegate {
public:
    static FriendSearchModalLayer *createLayer(Json *json);

    bool init();
    CREATE_FUNC(FriendSearchModalLayer);
    FriendSearchModalLayer();
    virtual ~FriendSearchModalLayer();
    
    void iconCallback(Ref *pSender);
    void yesBtnCallback(Ref *pSender);
    void noBtnCallback(Ref *pSender);

    void btDialogCallback(ButtonIndex aIndex);
    FriendSearchModalLayerDelegate *_delegate;
    Json* _friendJson;
    DialogView *_dialog;
    
private:
    DefaultProgress* _progress;
    RequestAPI* _request;
    
    int _friendsCount;
    void showInformation();
    
    void responseSetFriendRequest(Json* response);
};

#endif /* defined(__Syanago__FriendSearchModalLayer__) */