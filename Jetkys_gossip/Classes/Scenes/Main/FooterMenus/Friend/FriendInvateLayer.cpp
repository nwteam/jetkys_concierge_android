#include "FriendInvateLayer.h"
#include "PlayerController.h"
#include "SystemSettingModel.h"

USING_NS_CC;
USING_NS_CC_EXT;

FriendInviteLayer::FriendInviteLayer():
    _progress(nullptr)
    , _request(nullptr)
{
    _title = "招待コード";

    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}

FriendInviteLayer::~FriendInviteLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

bool FriendInviteLayer::init()
{
    if (!FriendLayerBase::init()) {
        return false;
    }

    makeInvateView();
    return true;
}

void FriendInviteLayer::makeInvateView()
{
    Size winSize = Director::getInstance()->getWinSize();


    auto friendBase = makeSprite("friend_invate_base.png");
    addChild(friendBase);
    friendBase->setPosition(Point(winSize.width / 2,
                                  winSize.height / 2));

    // 自身のIDを表示
    auto yourIDLabel = Label::createWithTTF("あなたの招待コード", FONT_NAME_2, 28);
    yourIDLabel->setAnchorPoint(Point(0.5, 0.5));
    yourIDLabel->setColor(COLOR_YELLOW);
    yourIDLabel->setPosition(Point(friendBase->getContentSize().width / 2, friendBase->getContentSize().height - 30 - yourIDLabel->getContentSize().height / 2));
    friendBase->addChild(yourIDLabel);
    yourIDLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto yourIDbase = makeSprite("other_invite_input.png");
    friendBase->addChild(yourIDbase);
    yourIDbase->setPosition(Point(friendBase->getContentSize().width / 2,
                                  friendBase->getContentSize().height - 30 - 14 - 14 - 14 - yourIDbase->getContentSize().height / 2));

    auto yourIDValue = Label::createWithTTF(PLAYERCONTROLLER->_player->getContactCode(), FONT_NAME_2, 28);
    yourIDValue->setColor(Color3B::BLACK);
    yourIDValue->setAnchorPoint(Point(0.5, 0.5));
    yourIDValue->setPosition(Point(yourIDbase->getContentSize().width / 2, yourIDbase->getContentSize().height / 2 - 14));
    yourIDbase->addChild(yourIDValue);
    std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
    int maxInvateCount = systemSettingModel->getMaxNumberOfInvitationSuccess();
    int invateSuccess = PLAYERCONTROLLER->_player->getNumberOfInvitation();
    int nowInvitaion = maxInvateCount - invateSuccess;
    if (nowInvitaion > 0) {
        auto atoTopLabel = Label::createWithTTF("あと", FONT_NAME_2, 22);
        auto countLabel = Label::createWithTTF(StringUtils::format("%d", nowInvitaion), FONT_NAME_2, 22);
        auto atoUnderLabel = Label::createWithTTF("人分まで特典受け取り可能", FONT_NAME_2, 22);
        auto invateLayer = LayerColor::create(Color4B(0, 0, 0, 0), atoTopLabel->getContentSize().width + atoUnderLabel->getContentSize().width + countLabel->getContentSize().width, 22);
        invateLayer->addChild(atoTopLabel);
        invateLayer->addChild(countLabel);
        invateLayer->addChild(atoUnderLabel);
        atoTopLabel->setAnchorPoint(Vec2::ZERO);
        countLabel->setAnchorPoint(Vec2::ZERO);
        atoUnderLabel->setAnchorPoint(Vec2::ZERO);
        atoTopLabel->setPosition(Vec2::ZERO);
        countLabel->setPosition(Vec2(atoTopLabel->getContentSize().width, 0));
        atoUnderLabel->setPosition(Vec2(atoTopLabel->getContentSize().width + countLabel->getContentSize().width, 0));
        atoTopLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        countLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        atoUnderLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        countLabel->setColor(COLOR_PINK);
        friendBase->addChild(invateLayer);
        invateLayer->setPosition(Point(yourIDbase->getPosition().x - invateLayer->getContentSize().width / 2,
                                       yourIDbase->getPosition().y - yourIDbase->getContentSize().height / 2 - 22 - 35));
    }

    // フレンド検索の文章を表示
    auto searchLabel = Label::createWithTTF("友達の招待コード入力", FONT_NAME_2, 28);
    searchLabel->setColor(COLOR_YELLOW);
    searchLabel->setAnchorPoint(Point(0.5, 0.5));
    searchLabel->setPosition(Point(yourIDbase->getPosition().x,
                                   yourIDbase->getPosition().y - yourIDbase->getContentSize().height / 2 - 55 - searchLabel->getContentSize().height / 2));
    friendBase->addChild(searchLabel);
    searchLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto searchInfo1Label = Label::createWithTTF("友達の招待コードを入力して", FONT_NAME_2, 22);
    friendBase->addChild(searchInfo1Label);
    searchInfo1Label->setPosition(Point(yourIDbase->getPosition().x,
                                        searchLabel->getPosition().y - 5 - searchLabel->getContentSize().height / 2));
    searchInfo1Label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto searchInfo2Label = Label::createWithTTF("トークンを貰おう!!", FONT_NAME_2, 22);
    friendBase->addChild(searchInfo2Label);
    searchInfo2Label->setPosition(Point(yourIDbase->getPosition().x,
                                        searchInfo1Label->getPosition().y - 5 - searchInfo1Label->getContentSize().height / 2));
    searchInfo2Label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto searchInfo3Label = Label::createWithTTF("(1回だけ利用する事ができます)", FONT_NAME_2, 22);
    friendBase->addChild(searchInfo3Label);
    searchInfo3Label->setPosition(Point(yourIDbase->getPosition().x,
                                        searchInfo2Label->getPosition().y - 5 - searchInfo2Label->getContentSize().height / 2));
    searchInfo3Label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    // 入力ボックスを表示
    _editBox = cocos2d::ui::EditBox::create(Size(270, 46), Scale9Sprite::create("other_invite_input.png"));
    _editBox->setText("");
    friendBase->addChild(_editBox);
    _editBox->setPosition(Point(yourIDbase->getPosition().x,
                                searchInfo3Label->getPosition().y - 10 - searchInfo3Label->getContentSize().height / 2));
    _editBox->setPlaceHolder("");
    _editBox->setMaxLength(10);
    _editBox->setInputMode(EditBox::InputMode::SINGLE_LINE);
    _editBox->setFontColor(Color3B::BLACK);
    _editBox->setReturnType(EditBox::KeyboardReturnType::DONE);

    // 検索ボタンを表示
    auto searchBtn = makeMenuItem("other_invite_button.png", CC_CALLBACK_1(FriendInviteLayer::menuItemCallback, this));
    searchBtn->setPosition(Point(winSize.width / 2,
                                 friendBase->getPosition().y - friendBase->getContentSize().height / 2 + 20 + searchBtn->getContentSize().height / 2));
    auto menu = MenuTouch::create(searchBtn, NULL);
    addChild(menu);
    menu->setPosition(Point::ZERO);


    auto searchBtnLabel = Label::createWithTTF("OK", FONT_NAME_2, 22);
    searchBtn->addChild(searchBtnLabel);
    searchLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    searchBtnLabel->setPosition(Point(searchBtn->getContentSize().width / 2, searchBtn->getContentSize().height / 2 - 11));
    searchBtnLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
}

void FriendInviteLayer::menuItemCallback(Ref* pSender)
{
    _friendLayer->playSoundEfect(SOUND_TAP_TRUE, false);
    std::string invitationCode = _editBox->getText();
    int ret = strcmp(invitationCode.c_str(), PLAYERCONTROLLER->_player->getContactCode().c_str());
    int ret2 = strcmp(invitationCode.c_str(), "");
    if (ret == 0 || ret2 == 0) {
        std::string mes;
        if (ret == 0) {
            mes = "自分を検索する事は出来ません";
        } else if (ret2 == 0) {
            mes = "コードを入力してください";
        }
        _dialog = DialogView::createWithShortMessage(mes, 1);
        addChild(_dialog, DIALOG_ZORDER);
        return;
    }

    _progress->onStart();
    _request->getInvitationReward(invitationCode, CC_CALLBACK_1(FriendInviteLayer::responseGetInvitationReward, this));
}

void FriendInviteLayer::responseGetInvitationReward(Json* response)
{
    Json* jsonData2 = Json_getItem(response, "get_invitation_reward");

    std::string result = Json_getString(jsonData2, "result", "false");
    if (result == "true") {
        Json* jsonDataData = Json_getItem(jsonData2, "data");
        // std::string player = Json_getString(jsonDataData, "players", "");
        //////CCLOG("FriendInvite : players: %s",player.c_str());
        Json* jsonDataDataData = Json_getItem(jsonDataData, "players");
        std::string freeToken = Json_getString(jsonDataDataData, "free_token", "");
        ////CCLOG("FriendInvite : players : free_token : %s",freeToken.c_str());
        int intfreeToken = atoi(Json_getString(jsonDataDataData, "free_token", ""));
        int oldToken = PLAYERCONTROLLER->_player->getFreeToken();
        int getToken = intfreeToken - oldToken;

        PLAYERCONTROLLER->_player->setFreeToken(intfreeToken);
        _friendLayer->updateInfoLayer();

        auto dialog = DialogView::createWithShortMessage(StringUtils::format("%dトークンを受け取りました", getToken), 1);
        addChild(dialog, DIALOG_ZORDER);
    }
}
