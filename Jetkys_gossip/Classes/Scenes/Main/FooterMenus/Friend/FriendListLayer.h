#ifndef __syanago__FriendListLayer__
#define __syanago__FriendListLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Loading.h"
#include "FriendLayer.h"
#include "FriendListModalLayer.h"
#include "ScrollBar.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace SyanagoAPI;

class Cell;

class FriendListLayer : public FriendLayerBase, public FriendListModalLayerDelegate, public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate{
public:
    FriendListLayer();
    virtual ~FriendListLayer();
    bool init();
    CREATE_FUNC(FriendListLayer);
    
    void initLayer();
    void showTable();
    
    //void btMenuItemTableCallback(Ref *pSender);
    
    //create cell with idx of table view
    Cell * cellWithIdx(int idx);
    
    //Table view
    void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {}
    virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
    virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);
    
    void removeCallback();
    void deleteCallback();
    void viewMenuCallBack();
    void hiddenMenuCallBack();
    
    void showNoListLabel();
    void showFriendCharacter(int playerMstCharacterId, int level, int exercise, int reaction, int decision);
    Layer *_layer;
    
private:
    int _numberOfCell;
    std::vector<int> _player;
    std::vector<Json*> _friends;
    int _friendIdx;
    int _approveOrReject;
    ScrollBar* _scrollBar;
    TableView * _tableView;
    Vec2 _tableViewPosition;
    DefaultProgress* _progress;
    RequestAPI* _request;
    
    void showFriendCount(int friendCount);
    
    void responseFriendList(Json* response);
};

#endif /* defined(__syanago__FriendListLayer__) */

