#include "FriendMainLayer.h"

#include "FriendLayer.h"
#include "FontDefines.h"
#include "BrowserLauncher.h"
#include "PlayerController.h"
#include "RankingLayer.h"
#include "HeaderStatus.h"

#include "extensions/cocos-ext.h"
USING_NS_CC_EXT;

bool FriendMainLayer::init()
{
    if (!Layer::init()) {
        return false;
    }

    Size winSize = Director::getInstance()->getWinSize();

    // メニューアイテム作成
    auto friendTitleBg = makeSprite("head_line_no1.png");
    addChild(friendTitleBg);
    friendTitleBg->setPosition(Vec2(friendTitleBg->getContentSize().width / 2,
                                    winSize.height - friendTitleBg->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 12));

    auto title = Label::createWithTTF("フレンド", FONT_NAME_2, 34);
    title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    addChild(title);
    title->setAnchorPoint(Vec2(0.0f, 0.5f));
    title->setPosition(Point(14,
                             friendTitleBg->getPosition().y - 17));
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto btList = makeMenuItem("button_no1.png", CC_CALLBACK_1(FriendMainLayer::btMenuItemCallback, this));
    btList->setTag(FI_LIST);
    auto listLabel = Label::createWithTTF("フレンドリスト", FONT_NAME_2, 35);
    btList->addChild(listLabel);
    listLabel->setPosition(Point(btList->getContentSize().width / 2,
                                 btList->getContentSize().height / 2 - 17));
    listLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto btApproval = makeMenuItem("button_no1.png", CC_CALLBACK_1(FriendMainLayer::btMenuItemCallback, this));
    btApproval->setTag(FI_APPROVAL);
    auto ApprovalLabel = Label::createWithTTF("フレンド承認", FONT_NAME_2, 35);
    btApproval->addChild(ApprovalLabel);
    ApprovalLabel->setPosition(Point(btApproval->getContentSize().width / 2,
                                     btApproval->getContentSize().height / 2 - 17));
    ApprovalLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto btIdSearch = makeMenuItem("button_no1.png", CC_CALLBACK_1(FriendMainLayer::btMenuItemCallback, this));
    btIdSearch->setTag(FI_ID_SEARCH);
    auto IdSearchLabel = Label::createWithTTF("フレンド検索", FONT_NAME_2, 35);
    btIdSearch->addChild(IdSearchLabel);
    IdSearchLabel->setPosition(Point(btIdSearch->getContentSize().width / 2,
                                     btIdSearch->getContentSize().height / 2 - 17));
    IdSearchLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto rankingButton = makeMenuItem("button_no1.png", CC_CALLBACK_1(FriendMainLayer::btMenuItemCallback, this));
    rankingButton->setTag(FI_RANKING);
    auto rankingLabel = Label::createWithTTF("ランキング", FONT_NAME_2, 35);
    rankingLabel->setPosition(Point(rankingButton->getContentSize().width / 2,
                                    rankingButton->getContentSize().height / 2 - 17));
    rankingLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    rankingButton->addChild(rankingLabel);

    // メニュー作成
    MenuTouch* menu = MenuTouch::create(btList, btApproval, btIdSearch, rankingButton, NULL);
    menu->setPosition(Point(menu->getPosition().x, menu->getPosition().y - 50));
    menu->alignItemsVerticallyWithPadding(14);
    addChild(menu);

    // add friend badge
    if (PLAYERCONTROLLER->_numberOfPendingFriend > 0) {
        auto friendBadge = addBadgeNode(btApproval, "itemSpr.png", PLAYERCONTROLLER->_numberOfPendingFriend);
        friendBadge->setPosition(Vec2(btApproval->getContentSize().width - 5, btApproval->getContentSize().height - 5));
    }

    return true;
}

void FriendMainLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    int tag = ((Node*)pSender)->getTag();
    _friendLayer->showLayer((FriendIndex) tag);
}

Node* FriendMainLayer::addBadgeNode(Node* node, std::string imgBadge, int number)
{
    auto bage = makeSprite(imgBadge.c_str());
    node->addChild(bage);

    auto label = Label::createWithTTF(StringUtils::format("%d", number).c_str(), FONT_NAME_2, 22);
    bage->addChild(label);
    label->setPosition(Vec2(bage->getContentSize().width / 2, bage->getContentSize().height / 2 - 11));
    label->setTag(1);

    return bage;
}
