#ifndef __syanago__FriendLayer__
#define __syanago__FriendLayer__

#include "cocos2d.h"
#include "DialogView.h"
#include "Loading.h"
#include "extensions/cocos-ext.h"
#include "spine/Json.h"
#include "MenuTouch.h"


USING_NS_CC;
USING_NS_CC_EXT;

class MainScene;

enum FriendIndex{
    FI_MAIN,
    FI_LIST,
    FI_APPROVAL,
    FI_ID_SEARCH,
    FI_INFORMATON,
    FI_INVATE,
    FI_RANKING
};

class FriendLayer : public Layer, public DialogDelegate {
public:
    FriendLayer();
    bool init();
    CREATE_FUNC(FriendLayer);

    MainScene *_mainScene;
    
    CC_SYNTHESIZE(FriendIndex, _friendIndex, FIndex);
    
    void showLayer(FriendIndex nextLayer);
    void showMainLayer(Ref *pSender);
    
    
    static std::string lastLoginTime(Json *json);
    
    void hiddenMenu();
    void viewMenu();
    
    void showFriendCharacter(int playerMstCharacterId, int level, int exercise, int reaction, int decision);
    
    void resetPageTag();
    
    void playSoundEfect(MainSceneSoundType type, bool sloop);
    
    void updateInfoLayer();
    void btDialogCallback(ButtonIndex aIndex, int tag);
    
    void updateBage();
};

class FriendLayer;
class FriendLayerBase : public Layer, public DialogDelegate{
public:
    FriendLayerBase();
    
    bool init();
    CREATE_FUNC(FriendLayerBase);
    
    FriendLayer *_friendLayer;
    
    void btMenuItemCallback(Ref *pSender);
    
    std::string _title;
    
    DialogView *_dialog;
};

#endif /* defined(__syanago__FriendLayer__) */
