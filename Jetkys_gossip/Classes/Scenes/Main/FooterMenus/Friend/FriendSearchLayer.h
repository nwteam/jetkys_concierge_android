#ifndef __syanago__FriendSearchLayer__
#define __syanago__FriendSearchLayer__

#include "cocos2d.h"
#include "Loading.h"
#include "FriendLayer.h"
#include "FriendSearchModalLayer.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
using namespace SyanagoAPI;

#include "ui/CocosGUI.h"
using namespace cocos2d::ui;

class Cell;

class FriendSearchLayer : public FriendLayerBase, public FriendSearchModalLayerDelegate{
public:
    FriendSearchLayer();
    ~FriendSearchLayer();
    
    bool init();
    CREATE_FUNC(FriendSearchLayer);
    
    void btMenuItemCallback(cocos2d::Ref *pSender);
    
    void removeCallback();
    
    Layer *_layer;
    
    DialogView *_dialog;
    
private:
    std::vector<int> _player;
    cocos2d::ui::EditBox *_editBox;
    int _approveOrReject;
    DefaultProgress* _progress;
    RequestAPI* _request;
    
    void responseFriendList(Json* response);
};

#endif /* defined(__syanago__FriendSearchLayer__) */