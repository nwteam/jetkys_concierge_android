#include <time.h>

#include "FriendListLayer.h"
#include "FriendListModalLayer.h"
#include "FriendLayer.h"
#include "FontDefines.h"
#include "Cell.h"
#include "DialogView.h"
#include "PlayerController.h"
#include "TreasureItemEffectModel.h"
#include "HeaderStatus.h"
#include "RankModel.h"

#define SIZE_PLAYER_CELL Size(570.0f, 115.0f)

FriendListLayer::FriendListLayer():
    _progress(nullptr)
    , _request(nullptr)
{
    _title = "フレンドリスト";
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}
FriendListLayer::~FriendListLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

bool FriendListLayer::init()
{
    if (!FriendLayerBase::init()) {
        return false;
    }
    initLayer();
    return true;
}

void FriendListLayer::initLayer()
{
    // initialize

    _numberOfCell = 0;
    _friendIdx = -1;
    _approveOrReject = 2;
    _player = std::vector<int>();
    _friends = std::vector<Json*>();

    _progress->onStart();
    _request->getFriendList(3, "", CC_CALLBACK_1(FriendListLayer::responseFriendList, this));
}

void FriendListLayer::responseFriendList(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "get_friend_list");
    std::string result = Json_getString(jsonData1, "result", "false");

    if (result == "true") {
        Json* mDatas = Json_getItem(jsonData1, "data");
        if (mDatas && mDatas->type == Json_Array) {
            Json* child;

            // フレンドリストを配列に格納
            for (child = mDatas->child; child; child = child->next) {
                _friends.push_back(child);
            }
            // テーブルビュー生成
            showTable();
            showFriendCount((int)_friends.size());
        } else {
            showNoListLabel();
            showFriendCount(0);
        }
    }
}

void FriendListLayer::showTable()
{
    // table view

    _layer = Layer::create();
    addChild(_layer);

    _numberOfCell = (int)_friends.size();

    Size winSize = Director::getInstance()->getWinSize();

    _tableView = TableView::create(this, Size(SIZE_PLAYER_CELL.width, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65));
    _tableView->setDirection(ScrollView::Direction::VERTICAL);
    _tableView->setPosition(Point(winSize.width / 2 - SIZE_PLAYER_CELL.width * 0.5, 0));
    _tableView->setBounceable(false);
    _tableView->setDelegate(this);
    _layer->addChild(_tableView);
    _scrollBar = ScrollBar::initScrollBar(winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 180 - 30, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65, _tableView->getContainer()->getContentSize().height, _tableView->getContentOffset().y);
    addChild(_scrollBar);
    _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 15 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    if (winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 <= _tableView->getContainer()->getContentSize().height) {
        _tableView->setPosition(_tableView->getPosition() + Point(-20, 0));
    }
    _tableView->reloadData();
}

void FriendListLayer::showNoListLabel()
{
    _layer = Layer::create();
    addChild(_layer);

    Size winSize = Director::getInstance()->getWinSize();

    auto bgground = makeSprite("dialog_base.png");
    addChild(bgground);
    bgground->setPosition(winSize / 2);
    auto resultLabel = Label::createWithTTF("プレイヤーはいません", FONT_NAME_2, 40);
    // resultLabel->setDimensions(winSize.width, 0);
    resultLabel->setPosition(Point(winSize.width / 2, (winSize.height / 2 - 20)));
    resultLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(resultLabel);
}

Size FriendListLayer::tableCellSizeForIndex(TableView* table, ssize_t idx)
{
    return SIZE_PLAYER_CELL;
}

TableViewCell* FriendListLayer::tableCellAtIndex(TableView* table, ssize_t idx)
{
    //    auto string = String::createWithFormat("%ld", idx);
    Cell* cell = (Cell*)table->dequeueCell();
    if (cell)
        cell->removeFromParent();

    // Cell UI
    cell = cellWithIdx((int)idx);

    ////CCLOG("tableCellAtIndex: %ld, %f, %f", idx, cell->getPosition().x, cell->getPosition().y);

    return cell;
}

ssize_t FriendListLayer::numberOfCellsInTableView(TableView* table)
{
    return _numberOfCell + 2;
}

Cell* FriendListLayer::cellWithIdx(int idx)
{
    auto cell = new Cell();

    _friendIdx = _numberOfCell + 1 - idx;
    int count = (int)_friends.size();
    if (_friendIdx < count) {
        Json* friendPlayer = _friends.at(_friendIdx);

        // セルを作成
        auto bg = makeSprite("friend_List_base.png");
        cell->addChild(bg);
        bg->setPosition(Vec2(SIZE_PLAYER_CELL.width / 2, SIZE_PLAYER_CELL.height / 2));
        int mst_id = atoi(Json_getString(friendPlayer, "mst_characters_id", ""));
        auto icon = makeSprite(StringUtils::format("%d_i.png", mst_id).c_str());
        cell->addChild(icon);
        icon->setPosition(Vec2(icon->getContentSize().width / 2,
                               SIZE_PLAYER_CELL.height / 2));

        // プレイヤー名を表示
        auto playerNameLabel = Label::createWithTTF(Json_getString(friendPlayer, "player_name", ""), FONT_NAME_2, 30);
        playerNameLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
        cell->addChild(playerNameLabel);
        playerNameLabel->setColor(COLOR_YELLOW);
        playerNameLabel->setAnchorPoint(Vec2(0, 0.5));
        playerNameLabel->setPosition(Point(10.0f + icon->getContentSize().width, SIZE_PLAYER_CELL.height * 0.7 - 20 + 5));
        playerNameLabel->enableShadow();

        // オーナーランクを表示
        std::string stdRank = "ランク ";
        auto ownerRankLabel = Label::createWithTTF(stdRank + Json_getString(friendPlayer, "rank", ""), FONT_NAME_2, 24);
        cell->addChild(ownerRankLabel);
        ownerRankLabel->setAnchorPoint(Vec2(1, 1));
        ownerRankLabel->setPosition(Point(SIZE_PLAYER_CELL.width - 20, SIZE_PLAYER_CELL.height - 20));
        ownerRankLabel->enableShadow();

        // 最終ログイン日時を表示
        auto lastLoginLabel = Label::createWithTTF("最終プレイ " + FriendLayer::lastLoginTime(friendPlayer), FONT_NAME_2, 22);
        lastLoginLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
        cell->addChild(lastLoginLabel);
        lastLoginLabel->setAnchorPoint(Vec2(1, 0));
        lastLoginLabel->setPosition(Point(SIZE_PLAYER_CELL.width  - 20, SIZE_PLAYER_CELL.height * 0.2 - 11 - 10));
        lastLoginLabel->enableShadow();

        auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %s", Json_getString(friendPlayer, "level", "")), FONT_NAME_2, 22);
        cell->addChild(levelLabel, 4);
        levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        levelLabel->setPosition(icon->getPosition() + Vec2(0, -icon->getContentSize().height / 2 + 6 - 11));
        levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
        levelLabel->enableShadow();
    }
    cell->autorelease();

    return cell;
}

void FriendListLayer::tableCellTouched(TableView* table, TableViewCell* cell)
{
    ////CCLOG("cell touched at index: %ld", cell->getIdx());
    // モーダルレイヤを生成
    if ((_numberOfCell - cell->getIdx() + 1) > -1) {
        SoundHelper* soundManeger;
        if ((_numberOfCell - cell->getIdx() + 1) < _friends.size()) {
            soundManeger->playeMainSceneEffect(SOUND_TAP_TRUE, false);
            auto layer = FriendListModalLayer::createLayer(_friends.at((_numberOfCell - cell->getIdx() + 1)));
            addChild(layer);
            layer->_delegate = this;

            // スクロール位置を保存
            _tableViewPosition = table->getContentOffset();
        }
    }
}

void FriendListLayer::removeCallback()
{
    _layer->removeFromParent();
    showTable();

    //
    _tableView->setContentOffset(_tableViewPosition);
}

void FriendListLayer::deleteCallback()
{
    _layer->removeFromParent();

    initLayer();
}


void FriendListLayer::hiddenMenuCallBack()
{
    _friendLayer->hiddenMenu();
}

void FriendListLayer::viewMenuCallBack()
{
    _friendLayer->viewMenu();
}

void FriendListLayer::showFriendCharacter(int playerMstCharacterId, int level, int exercise, int reaction, int decision)
{
    _friendLayer->showFriendCharacter(playerMstCharacterId, level, exercise, reaction, decision);
}

void FriendListLayer::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
    _scrollBar->updateScrollPoint(view->getContentOffset().y);
}

void FriendListLayer::showFriendCount(int friendCount)
{
    Size winSize = Director::getInstance()->getWinSize();
    int rank = PLAYERCONTROLLER->_player->getRank();
    std::shared_ptr<RankModel>rankModel(RankModel::find(rank));
    int friendMaxCount = rankModel->getMaxFriend() + TreasureItemEffectModel::getTreasureItemIncrementalDifferenceEffect(ITEM::EFFECT_TYPE::MAX_FRIENDS_UP);

    auto countFriendHoldbg = makeSprite("carry_number_bg.png");
    _layer->addChild(countFriendHoldbg);
    countFriendHoldbg->setPosition(Point(winSize.width - countFriendHoldbg->getContentSize().width / 2,
                                         winSize.height - 189.5));
    auto hodlLabel = Label::createWithTTF("フレンド数", FONT_NAME_2, 18);
    countFriendHoldbg->addChild(hodlLabel);
    hodlLabel->setAnchorPoint(Vec2(0, 0.5));
    hodlLabel->setPosition(Point(5, countFriendHoldbg->getContentSize().height / 2 - 9));

    auto holdValueLabel = Label::createWithTTF(StringUtils::format("%d/%d", friendCount, friendMaxCount), FONT_NAME_2, 18);
    countFriendHoldbg->addChild(holdValueLabel);
    holdValueLabel->setAnchorPoint(Vec2(1, 0.5));
    holdValueLabel->setPosition(Point(countFriendHoldbg->getContentSize().width - 5, hodlLabel->getPosition().y));
}
