#ifndef __syanago__FriendMainLayer__
#define __syanago__FriendMainLayer__

#include "cocos2d.h"
USING_NS_CC;

class FriendLayer;

class FriendMainLayer : public Layer {
public:
    bool init();
    CREATE_FUNC(FriendMainLayer);
    
    FriendLayer *_friendLayer;
    
    void btMenuItemCallback(Ref *pSender);
    void btLunchBrowser(Ref *pSender);
private:
    Node *addBadgeNode(Node *node, std::string imgBadge, int number);
    
};

#endif /* defined(__syanago__FriendMainLayer__) */
