#include <time.h>

#include "FriendApproveLayer.h"
#include "FriendLayer.h"
#include "FontDefines.h"
#include "Cell.h"
#include "DialogView.h"
#include "PlayerController.h"
#include "MenuTouch.h"
#include "JDate.h"
#include "TreasureItemEffectModel.h"
#include "HeaderStatus.h"
#include "TimeConverter.h"
#include "RankModel.h"

// #define SIZE_PLAYER_CELL Size(570.0f, 115.0f)
#define SIZE_PLAYER_CELL Size(581, 120)


FriendApproveLayer::FriendApproveLayer():
    _progress(nullptr)
    , _request(nullptr)
{
    _title = "フレンド承認";
    _progress = new DefaultProgress;
    _request = new RequestAPI(_progress);
}

FriendApproveLayer::~FriendApproveLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}
bool FriendApproveLayer::init()
{
    if (!FriendLayerBase::init()) {
        return false;
    }
    return true;
}

void FriendApproveLayer::onEnterTransitionDidFinish()
{
    // initialize
    _getFriendListFlag = false;
    _numberOfCell = 0;
    _friendIdx = -1;
    _approveOrReject = 2;
    _player = std::vector<int>();
    _friends = std::vector<Json*>();

    _progress->onStart();
    _request->getFriendList(2, "", CC_CALLBACK_1(FriendApproveLayer::responseFriendList, this));
}


void FriendApproveLayer::responseFriendList(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "get_friend_list");
    std::string result = Json_getString(jsonData1, "result", "false");

    if (result == "true") {
        Json* mDatas = Json_getItem(jsonData1, "data");
        if (mDatas && mDatas->type == Json_Array) {
            if (_getFriendListFlag == false) {
                Json* child;
                // フレンドリストを配列に格納
                for (child = mDatas->child; child; child = child->next) {
                    _friends.push_back(child);
                }

                // テーブルビュー生成
                showTable();
                _getFriendListFlag = true;

                _progress->onStart();
                _request->getFriendList(3, "", CC_CALLBACK_1(FriendApproveLayer::responseFriendList, this));
            } else {
                Json* child;
                int count = 0;
                for (child = mDatas->child; child; child = child->next) {
                    count++;
                }
                showFriendCount(count);
            }
        } else {
            if (_getFriendListFlag == false) {
                showNoListLabel();
                _getFriendListFlag = true;

                _progress->onStart();
                _request->getFriendList(3, "", CC_CALLBACK_1(FriendApproveLayer::responseFriendList, this));
            } else {
                showFriendCount(0);
            }
        }
        // update badge
        if (PLAYERCONTROLLER->_numberOfPendingFriend != (int)_friends.size()) {
            PLAYERCONTROLLER->_numberOfPendingFriend = (int)_friends.size();
            _friendLayer->updateBage();
        }
    }
}

void FriendApproveLayer::tableCellTouched(TableView* table, TableViewCell* cell)
{
    ////CCLOG("cell touched at index: %ld", cell->getIdx());
}

Size FriendApproveLayer::tableCellSizeForIndex(TableView* table, ssize_t idx)
{
    return SIZE_PLAYER_CELL;
}

TableViewCell* FriendApproveLayer::tableCellAtIndex(TableView* table, ssize_t idx)
{
    //    auto string = String::createWithFormat("%ld", idx);
    Cell* cell = (Cell*)table->dequeueCell();
    if (cell)
        cell->removeFromParent();

    // Cell UI
    cell = cellWithIdx((int)idx);

    ////CCLOG("tableCellAtIndex: %ld, %f, %f", idx, cell->getPosition().x, cell->getPosition().y);

    return cell;
}

ssize_t FriendApproveLayer::numberOfCellsInTableView(TableView* table)
{
    return _numberOfCell;
}

Cell* FriendApproveLayer::cellWithIdx(int idx)
{
    auto cell = new Cell();

    _friendIdx = _numberOfCell - 1 - idx;
    Json* friendPlayer = _friends.at(_friendIdx);

    // セルを作成
    auto bg = makeSprite("friend_List_base.png");
    cell->addChild(bg);
    bg->setPosition(Vec2(SIZE_PLAYER_CELL.width / 2, SIZE_PLAYER_CELL.height / 2));

    int mst_id = atoi(Json_getString(friendPlayer, "mst_characters_id", ""));
    ////CCLOG("ID: %D",mst_id);
    // auto iconImage = );
    auto icon = makeSprite(StringUtils::format("%d_i.png", mst_id).c_str());
    cell->addChild(icon);
    icon->setPosition(Vec2(icon->getContentSize().width / 2,
                           SIZE_PLAYER_CELL.height / 2));

    // プレイヤー名を表示
    auto playerNameLabel = Label::createWithTTF(Json_getString(friendPlayer, "player_name", ""), FONT_NAME_2, 30);
    playerNameLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    cell->addChild(playerNameLabel);
    playerNameLabel->setColor(COLOR_YELLOW);
    playerNameLabel->setAnchorPoint(Vec2(0, 0.5));
    playerNameLabel->setPosition(Point(10.0f + icon->getContentSize().width, SIZE_PLAYER_CELL.height * 0.7 - 20 + 5));
    playerNameLabel->enableShadow();

    // オーナーランクを表示
    std::string stdRank = "ランク ";
    auto ownerRankLabel = Label::createWithTTF(stdRank + Json_getString(friendPlayer, "rank", ""), FONT_NAME_2, 24);
    cell->addChild(ownerRankLabel);
    ownerRankLabel->setAnchorPoint(Vec2(1, 1));
    ownerRankLabel->setPosition(Point(SIZE_PLAYER_CELL.width - 15, SIZE_PLAYER_CELL.height - 15));
    ownerRankLabel->enableShadow();

    // 最終ログイン日時を表示
    auto lastLoginLabel = Label::createWithTTF(lastLoginTime(friendPlayer), FONT_NAME_2, 22);
    lastLoginLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    cell->addChild(lastLoginLabel);
    lastLoginLabel->setAnchorPoint(Vec2(0, 0));
    lastLoginLabel->setPosition(Point(10.0f + icon->getContentSize().width, 5 - 5));
    lastLoginLabel->enableShadow();

    // 拒否ボタン
    auto btReject = makeMenuItem("friend_ok_button.png", CC_CALLBACK_1(FriendApproveLayer::btMenuItemTableCallback, this));
    btReject->setAnchorPoint(Point(1, 0));
    btReject->setPosition(Point(SIZE_PLAYER_CELL.width - 5 - 12 + 5,
                                btReject->getContentSize().height / 2 + 2 - 5));
//    btReject->setTag(2);
    btReject->setTag(-_friendIdx - 1);              // -1 to prevent friendIdx = 0 make same with btApprove

    auto reject = Label::createWithTTF("拒否", FONT_NAME_2, 24);
    btReject->addChild(reject);
    reject->setPosition(Point(btReject->getContentSize().width / 2,
                              btReject->getContentSize().height / 2 - 12));
    reject->enableShadow();

    // 承認ボタン
    auto btApprove = makeMenuItem("friend_ok_button.png", CC_CALLBACK_1(FriendApproveLayer::btMenuItemTableCallback, this));
    btApprove->setAnchorPoint(Point(1, 0));
    btApprove->setPosition(Point(SIZE_PLAYER_CELL.width - btReject->getContentSize().width - 10 - 5 - 12 + 5,
                                 btReject->getContentSize().height / 2 + 2 - 5));
//    btApprove->setTag(1);
    btApprove->setTag(_friendIdx + 1);          // +1 to prevent friendIdx = 0 make same with btReject

    auto approve = Label::createWithTTF("承認", FONT_NAME_2, 24);
    btApprove->addChild(approve);
    approve->setPosition(Point(btApprove->getContentSize().width / 2,
                               btApprove->getContentSize().height / 2 - 12));
    approve->enableShadow();

    // メニュー作成
    auto menu = MenuTouch::create(btApprove, btReject, NULL);
    cell->addChild(menu);
    menu->setPosition(Point::ZERO);

    auto levelLabel = Label::createWithTTF(StringUtils::format("Lv %s", Json_getString(friendPlayer, "level", "")), FONT_NAME_2, 22);
    cell->addChild(levelLabel, 4);
    levelLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    levelLabel->setPosition(icon->getPosition() + Vec2(0, -icon->getContentSize().height / 2 + 6 - 11));
    levelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);
    levelLabel->enableShadow();


    cell->autorelease();

    return cell;
}


void FriendApproveLayer::btMenuItemTableCallback(Ref* pSender)
{
    std::string message;
    std::string title;

    _approveOrReject = ((MenuItem*)pSender)->getTag();
    SoundHelper* soundManeger;
    soundManeger->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    ////CCLOG("承認・拒否フラグ:%d",_approveOrReject);
    if (_approveOrReject > 0) {
        message = "フレンド申請を承認します。\nよろしいですか？";
        title = "フレンド承認確認";
        _friendIdx = _approveOrReject - 1;           // save the friendIdx is processing now
    } else {
        message = "フレンド申請を拒否します。\nよろしいですか？";
        title = "フレンド拒否確認";
        _friendIdx = -_approveOrReject - 1;         // save the friendIdx is processing now
    }
    _dialog = DialogView::createFriendApplyDialog(message, title);
    addChild(_dialog, DIALOG_ZORDER);
    _dialog->_delegate  = this;
}

void FriendApproveLayer::showNoListLabel()
{
    _layer = Layer::create();
    addChild(_layer);

    Size winSize = Director::getInstance()->getWinSize();

    auto bgground = makeSprite("dialog_base.png");
    addChild(bgground);
    bgground->setPosition(winSize / 2);
    auto resultLabel = Label::createWithTTF("プレイヤーはいません", FONT_NAME_2, 40);
    // resultLabel->setDimensions(winSize.width, 0);
    resultLabel->setPosition(Point(winSize.width / 2, (winSize.height / 2 - 20)));
    resultLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    addChild(resultLabel);
}

void FriendApproveLayer::showTable()
{
    // table view

    _layer = Layer::create();
    addChild(_layer);

    _numberOfCell = (int)_friends.size();

    Size _winSize = Director::getInstance()->getWinSize();
    TableView* tableView = TableView::create(this, Size(SIZE_PLAYER_CELL.width, _winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 28 - 150));  // -150
    tableView->setDirection(ScrollView::Direction::VERTICAL);
    tableView->setPosition(Point(_winSize.width / 2 - SIZE_PLAYER_CELL.width * 0.5,
                                 180));  // 180
    tableView->setDelegate(this);
    tableView->setBounceable(false);
    _layer->addChild(tableView);
    _scrollBar = ScrollBar::initScrollBar(_winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 180 - 30 - 28 + 30, _winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 28, tableView->getContainer()->getContentSize().height + SIZE_PLAYER_CELL.height, tableView->getContentOffset().y);
    _layer->addChild(_scrollBar);
    _scrollBar->setPosition(Point(_winSize.width - ((_winSize.width / 2) - (567 / 2)) - 10, _winSize.height / 2 - 15 - 28 + 30 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    if (_winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 28 <= tableView->getContainer()->getContentSize().height) {
        tableView->setPosition(tableView->getPosition() + Point(-20, 0));
    }
    tableView->reloadData();
}

void FriendApproveLayer::btDialogCallback(ButtonIndex aIndex)
{
    if (aIndex == BT_YES) {
        int status;
        if (_approveOrReject > 0) {
            status = 1;       // 承認
        } else {
            status = 2;       // Rejected
        }
        _progress->onStart();
        _request->approveFriends(status, atoi(Json_getString(_friends[_friendIdx], "player_id", "")), CC_CALLBACK_1(FriendApproveLayer::responseApprove, this));
        _approveOrReject = status;
    }
    _friendIdx = -1;             // now have no friend is processing
}

void FriendApproveLayer::responseApprove(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "approve_friends");
    std::string result = Json_getString(jsonData1, "result", "false");

    if (result == "true") {
        std::string message;
        if (_approveOrReject == 1) {
            message = "フレンド申請を承認しました";
        } else {
            message = "フレンド申請を拒否しました";
        }
        if (PLAYERCONTROLLER->_numberOfPendingFriend > 0) {
            PLAYERCONTROLLER->_numberOfPendingFriend--;
        }
        _friendLayer->updateBage();

        _dialog = DialogView::createWithShortMessage(message, 1);
        addChild(_dialog, DIALOG_ZORDER);

        _layer->removeFromParent();
        onEnterTransitionDidFinish();
    }
}

// 最終ログインからの日数を算出
std::string FriendApproveLayer::lastLoginTime(Json* json)
{
    // 2014-08-26 13:07:43
    //上記の様な値を年月日に分ける
    std::string stdLoginTime = Json_getString(json, "last_logged", "");

    return TimeConverter::convertLastLogInTimeString(stdLoginTime);
}
void FriendApproveLayer::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
    _scrollBar->updateScrollPoint(view->getContentOffset().y);
}

void FriendApproveLayer::showFriendCount(int friendCount)
{
    Size winSize = Director::getInstance()->getWinSize();
    int rank = PLAYERCONTROLLER->_player->getRank();
    std::shared_ptr<RankModel>rankModel(RankModel::find(rank));
    int friendMaxCount = rankModel->getMaxFriend() + TreasureItemEffectModel::getTreasureItemIncrementalDifferenceEffect(ITEM::EFFECT_TYPE::MAX_FRIENDS_UP);

    auto countFriendHoldbg = makeSprite("carry_number_bg.png");
    _layer->addChild(countFriendHoldbg);
    countFriendHoldbg->setPosition(Point(winSize.width - countFriendHoldbg->getContentSize().width / 2,
                                         winSize.height - 189.5));
    auto hodlLabel = Label::createWithTTF("フレンド数", FONT_NAME_2, 18);
    countFriendHoldbg->addChild(hodlLabel);
    hodlLabel->setAnchorPoint(Vec2(0, 0.5));
    hodlLabel->setPosition(Point(5, countFriendHoldbg->getContentSize().height / 2 - 9));

    auto holdValueLabel = Label::createWithTTF(StringUtils::format("%d/%d", friendCount, friendMaxCount), FONT_NAME_2, 18);
    countFriendHoldbg->addChild(holdValueLabel);
    holdValueLabel->setAnchorPoint(Vec2(1, 0.5));
    holdValueLabel->setPosition(Point(countFriendHoldbg->getContentSize().width - 5, hodlLabel->getPosition().y));
}