#include "RankingLayer.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include "JDate.h"
#include "BrowserLauncher.h"
#include "SoundHelper.h"
#include "TweetButton.h"
#include "TweetMessage.h"
#include "HeaderStatus.h"

RankingLayer::RankingLayer()
    : progress(nullptr)
    , api(nullptr)
    , rankingListColumnName("current")
    , rankingList(nullptr)
{
    windowSize = Director::getInstance()->getWinSize();
    progress = new DefaultProgress();
    api = new RequestAPI(progress);
}

RankingLayer::~RankingLayer()
{
    delete progress;
    delete api;

    Director::getInstance()->getTextureCache()->removeAllTextures();
}

bool RankingLayer::init(const backButtonCallback& callback)
{
    if (!Layer::init()) {
        return false;
    }

    showBackGround();
    showHeadLine(this, "イベントランキング", callback);
    preventTapEvent();
    requestRankingInit(CC_CALLBACK_1(RankingLayer::onResponseGetCurrentRanking, this));

    return true;
}

void RankingLayer::showBackGround()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto background = Sprite::create("home_bg.png");
    background->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(background, 9);
}

void RankingLayer::preventTapEvent()
{
    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [](Touch* touch, Event* event) -> bool {
                                 return true;
                             };
    EventDispatcher* dispatcher = Director::getInstance()->getEventDispatcher();
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void RankingLayer::onResponseGetCurrentRanking(Json* response)
{
    if (rankingList != nullptr) {
        delete rankingList;
        rankingList = nullptr;
    }
    rankingList = new RankingList(response);

    if (rankingList->getRankings().size() == 0) {
        rankingListColumnName = "last_time";
        requestRankingInit(CC_CALLBACK_1(RankingLayer::onResponseGetLastRanking, this));
        return;
    }

    showRankingToggleButton();
    showRankingInfo();
    showRankingTable();
    showScrollBar();

    TableView* tableView = static_cast<TableView*>(getChildByTag(TAG_SPRITE::RANKING_LIST));
    tableView->setContentOffset(Vec2(0, TABLE_CELL_HEIGHT * rankingList->getSelfPlayerRankingIndex() + tableView->getContentOffset().y));
}

void RankingLayer::onResponseGetLastRanking(Json* response)
{
    if (rankingList != nullptr) {
        delete rankingList;
        rankingList = nullptr;
    }
    rankingList = new RankingList(response);

    if (rankingList->getRankings().size() == 0) {
        showNoRankingView();
        return;
    }

    rankingList->setEventTitle(StringUtils::format("[イベント結果] %s", rankingList->getEventTitle().c_str()));

    showRankingToggleButton();
    showRankingInfo();
    showRankingTable();
    showScrollBar();

    TableView* tableView = static_cast<TableView*>(getChildByTag(TAG_SPRITE::RANKING_LIST));
    tableView->setContentOffset(Vec2(0, TABLE_CELL_HEIGHT * rankingList->getSelfPlayerRankingIndex() + tableView->getContentOffset().y));
}

void RankingLayer::onResponseGetRanking(Json* response)
{
    if (rankingList != nullptr) {
        delete rankingList;
        rankingList = nullptr;
    }
    rankingList = new RankingList(response);

    TableView* tableView = static_cast<TableView*>(getChildByTag(TAG_SPRITE::RANKING_LIST));
    tableView->reloadData();
    // @todo scrollbar refactoring
    removeChildByTag(TAG_SPRITE::SCROLL_BAR);
    showScrollBar();
    tableView->setContentOffset(Vec2(0, TABLE_CELL_HEIGHT * rankingList->getSelfPlayerRankingIndex() + tableView->getContentOffset().y));

    static_cast<RankingToggleButton*>(getChildByTag(TAG_SPRITE::SWITCH_RANKING_BUTTON))->setEnabled(true);
}

void RankingLayer::showRankingToggleButton()
{
    RankingToggleButton* button = RankingToggleButton::create("button_orange.png",
                                                              "button_orange.png",
                                                              CC_CALLBACK_0(RankingLayer::requestTopRanking, this),
                                                              CC_CALLBACK_0(RankingLayer::requestSelfRanking, this));
    button->setPosition(Point(windowSize.width - 90, windowSize.height - button->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 12));
    button->setTag(TAG_SPRITE::SWITCH_RANKING_BUTTON);
    addChild(button, 12);
}

void RankingLayer::showRankingInfo()
{
    Sprite* sprite = Sprite::create("ranking_header_bg.png");
    sprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    sprite->setPosition(0, windowSize.height - 155 - HEADER_STATUS::SIZE::HEIGHT);
    addChild(sprite, 12);

    Label* label = Label::createWithTTF(rankingList->getEventTitle(), FONT_NAME_2, 26);
    label->setLineHeight(26);
    label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    label->setPosition(30, 35);
    label->setColor(Color3B::RED);
    sprite->addChild(label);

    Label* term = Label::createWithTTF("※イベント期間" + rankingList->getEventStartDate() + "〜" + rankingList->getEventEndDate(), FONT_NAME_2, 20);
    term->setLineHeight(20);
    term->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    term->setPosition(30, 5);
    term->setColor(Color3B(0, 0, 0));
    sprite->addChild(term);

    ui::Button* helpButton = ui::Button::create("ranking_help_button.png");
    helpButton->setPosition(Point(sprite->getContentSize().width - 60, sprite->getContentSize().height / 2));
    helpButton->addTouchEventListener(CC_CALLBACK_2(RankingLayer::onHelpButtonTapEndedCallback, this));
    sprite->addChild(helpButton);

    int eventTermFromNow = JDate::calculateTimeStringDiffOnDay(rankingList->getEventEndDateTime(),
                                                               JDate::getTimeWithFormat("%F %T"),
                                                               "%F %T");
    if (0 < eventTermFromNow) {
        int offset = label->getStringLength();
        label->setString(label->getString() + "開催中！");
        changeColorOfLabelPartString(label, Color3B(0, 0, 0), offset, label->getStringLength() - offset);
    }
}

void RankingLayer::onHelpButtonTapEndedCallback(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        Cocos2dExt::BrowserLauncher::launchUrl(EVENT_HELP_URL);
    }
}

void RankingLayer::showRankingTable()
{
    TableView* tableView = TableView::create(this, Size(583, windowSize.height - 155 - HEADER_STATUS::SIZE::HEIGHT));
    tableView->setDirection(TableView::Direction::VERTICAL);
    tableView->setBounceable(false);
    tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
    tableView->setDelegate(this);
    tableView->setTag(TAG_SPRITE::RANKING_LIST);
    tableView->reloadData();
    addChild(tableView, 13);
}

void RankingLayer::showScrollBar()
{
    TableView* tableView = static_cast<TableView*>(getChildByTag(TAG_SPRITE::RANKING_LIST));
    ScrollBar* scrollBar = ScrollBar::initScrollBar(windowSize.height - 195 - HEADER_STATUS::SIZE::HEIGHT,
                                                    windowSize.height - 165 - HEADER_STATUS::SIZE::HEIGHT,
                                                    tableView->getContainer()->getContentSize().height,
                                                    tableView->getContentOffset().y
                                                    );
    scrollBar->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    scrollBar->setPosition(tableView->getContentSize().width + 10, (windowSize.height - 155 - HEADER_STATUS::SIZE::HEIGHT) / 2);
    scrollBar->setTag(TAG_SPRITE::SCROLL_BAR);
    addChild(scrollBar, 13);
}

void RankingLayer::showNoRankingView()
{
    Sprite* bg = Sprite::create("dialog_base.png");
    bg->setPosition(windowSize / 2);
    addChild(bg, 10);

    Label* label = Label::createWithTTF("ランキングがありません", FONT_NAME_2, 40);
    label->setPosition(Point(windowSize.width / 2, (windowSize.height / 2 - 20 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2)));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(label, 10);
}

Size RankingLayer::cellSizeForTable(TableView* table)
{
    return Size(windowSize.width, TABLE_CELL_HEIGHT);
}

ssize_t RankingLayer::numberOfCellsInTableView(TableView* table)
{
    return rankingList->getRankings().size();
}

TableViewCell* RankingLayer::tableCellAtIndex(TableView* table, ssize_t idx)
{
    TableViewCell* cell = table->dequeueCell();
    cell = TableViewCell::create();

    RankingList::Ranking ranking = rankingList->getRankings().at(idx);

    Sprite* background = Sprite::create("ranking_user_bg.png");
    background->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    background->setPosition(20, 10);
    cell->addChild(background);

    Sprite* icon = Sprite::createWithSpriteFrameName(StringUtils::format("%d_i.png", ranking.getMstCharacterId()));
    icon->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    icon->setPosition(0, -2);
    background->addChild(icon);

    Label* characterLevel = Label::createWithTTF(StringUtils::format("Lv %d", ranking.getLevel()), FONT_NAME_2, 22);
    characterLevel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    characterLevel->setPosition(Point(icon->getContentSize().width / 2, -5));
    characterLevel->enableOutline(Color4B(0, 0, 0, 255), 1);
    characterLevel->enableShadow();
    background->addChild(characterLevel);

    Label* eventRankingLabel = Label::createWithTTF(StringUtils::format("%d位", ranking.getRanking()), FONT_NAME_2, 22);
    eventRankingLabel->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    eventRankingLabel->setPosition(Point(icon->getContentSize().width + 10, 91));
    eventRankingLabel->enableShadow();
    background->addChild(eventRankingLabel);

    if (ranking.getRanking() <= 3) {
        Sprite* crown = Sprite::create(StringUtils::format("crown_rank%d.png", ranking.getRanking()));
        crown->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
        crown->setPosition(icon->getContentSize().width + 10, 89);
        background->addChild(crown);

        eventRankingLabel->setPosition(Point(eventRankingLabel->getPosition().x + crown->getContentSize().width + 8, eventRankingLabel->getPosition().y));
    }

    Label* ownerRank = Label::createWithTTF(StringUtils::format("ランク%d", ranking.getOwnerRank()), FONT_NAME_2, 24);
    ownerRank->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    ownerRank->setPosition(Point(background->getContentSize().width - 10, 90));
    ownerRank->enableShadow();
    background->addChild(ownerRank);

    Label* name = Label::createWithTTF(ranking.getPlayerName(), FONT_NAME_2, 28);
    name->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    name->setPosition(Point(icon->getContentSize().width + 10, 50));
    name->enableShadow();
    background->addChild(name);

    Label* point = Label::createWithTTF(StringUtils::format("%dpt", ranking.getPoint()), FONT_NAME_2, 24);
    point->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    point->setPosition(Point(background->getContentSize().width - 10, 40));
    point->setColor(Color3B::YELLOW);
    point->enableShadow();
    background->addChild(point);

    if (isSelfPlayer(ranking.getPlayerId())) {
        showTweetButtonOnCell(background, ownerRank, ranking.getRanking());
        changeToOwnerCell(background, point);
    }

    return cell;
}

void RankingLayer::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
    ScrollBar* scrollBar = static_cast<ScrollBar*>(getChildByTag(TAG_SPRITE::SCROLL_BAR));
    if (scrollBar) {
        scrollBar->updateScrollPoint(view->getContentOffset().y);
    }
}

bool RankingLayer::isSelfPlayer(int playerId)
{
    return atoi(PLAYERCONTROLLER->getPlayerID().c_str()) == playerId;
}

void RankingLayer::showTweetButtonOnCell(cocos2d::Sprite* cellBackGround, cocos2d::Label* ownerRank, const int ranking)
{
    Twitter::TweetButton* tweetButton = Twitter::TweetButton::create(TweetMessage::getRankingTweetMessage(ranking, rankingList->getEventTitle()), "ranking_tweet_button.png");
    tweetButton->setPosition(ownerRank->getPosition() - Vec2(ownerRank->getContentSize().width + 5, 0));
    tweetButton->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    cellBackGround->addChild(tweetButton);
}

void RankingLayer::changeToOwnerCell(cocos2d::Sprite* cellBackGround, cocos2d::Label* point)
{
    cellBackGround->setTexture("ranking_user_bg_self.png");

    if (rankingList->hasLastTimeRanking()) {
        point->setPosition(Point(point->getPosition().x, point->getPosition().y + 20));

        std::string arrowFileName = "";
        std::string labelText = "";
        int diff = rankingList->diffMyCurrentRankingAndMyLastRanking();
        if (diff > 0) {
            arrowFileName = "arrow_up.png";
            labelText = StringUtils::format("%d位", diff);
        } else if (diff == 0) {
            arrowFileName = "arrow_horizon.png";
            labelText = "変動無し";
        } else if (diff < 0) {
            arrowFileName = "arrow_down.png";
            labelText = StringUtils::format("%d位", diff);
        }

        Sprite* arrow = Sprite::create(arrowFileName);
        arrow->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
        arrow->setPosition(Point(cellBackGround->getContentSize().width - 10, 28));
        cellBackGround->addChild(arrow);

        Label* rankDiffLabel = Label::createWithTTF(labelText, FONT_NAME_2, 24);
        rankDiffLabel->setColor(Color3B::YELLOW);
        rankDiffLabel->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
        rankDiffLabel->setPosition(Point(cellBackGround->getContentSize().width - 45, 30));
        rankDiffLabel->enableShadow();
        cellBackGround->addChild(rankDiffLabel);
    }
}

void RankingLayer::changeColorOfLabelPartString(Label* label, const Color3B& color3b, int offset, int length)
{
    Sprite* s;
    int labelTextLength = label->getStringLength();
    for (int i = offset; i < offset + length; i++) {
        if (labelTextLength < i) {
            return;
        }
        s = label->getLetter(i);
        s->setColor(color3b);
    }
}

void RankingLayer::requestRankingInit(const OnResponseCallback &onResponseCallback)
{
    progress->onStart();
    api->getRankingList(rankingListColumnName, 0, 50, onResponseCallback, NULL);
}

void RankingLayer::requestTopRanking()
{
    progress->onStart();
    api->getRankingList(rankingListColumnName, 0, 50, CC_CALLBACK_1(RankingLayer::onResponseGetRanking, this), NULL);
}

void RankingLayer::requestSelfRanking()
{
    progress->onStart();
    api->getRankingList(rankingListColumnName, 0, 50, CC_CALLBACK_1(RankingLayer::onResponseGetRanking, this), atoi(PLAYERCONTROLLER->getPlayerID().c_str()));
}