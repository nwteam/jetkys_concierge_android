#ifndef __syanago__RankingToggleButton__
#define __syanago__RankingToggleButton__

#include <functional>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class RankingToggleButton : public ui::Button
{
public:
    typedef std::function<void(void)> ButtonCallback;
    static RankingToggleButton* create(const std::string& file, const std::string& onTapAndEnablefile, const ButtonCallback& onTapTopRankingCallback, const ButtonCallback& onTapSelfRankingCallback);
    void initialize(const ButtonCallback& onTapTopRankingCallback, const ButtonCallback& onTapSelfRankingCallback);
    void showLabel();

private:
    void onTapButton(Ref *sender, Widget::TouchEventType type);
    bool isSelfRankingRequest();
    
    const std::string LABEL_TOP = "上位";
    const std::string LABEL_SELF = "あなた";
    
    ButtonCallback _onTapTopRankingCallback;
    ButtonCallback _onTapSelfRankingCallback;
    Label* label;
};

#endif /* defined(__syanago__RankingToggleButton__) */
