#ifndef __syanago__RankingLayer__
#define __syanago__RankingLayer__

#include <functional>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "create_func.h"
#include "show_head_line.h"
#include "ui/CocosGUI.h"
#include "ScrollBar.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"
#include "RankingList.h"
#include "RankingToggleButton.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace SyanagoAPI;

class RankingLayer : public create_func<RankingLayer>, public show_head_line, public Layer, public TableViewDataSource, public TableViewDelegate
{
public:
    typedef std::function<void(Json* response)> OnResponseCallback;
    RankingLayer();
    ~RankingLayer();
    
    bool init(const backButtonCallback&);
    using create_func::create;
    virtual Size cellSizeForTable(TableView* table) override;
    virtual ssize_t numberOfCellsInTableView(TableView* table) override;
    virtual TableViewCell* tableCellAtIndex(TableView* table,ssize_t idx) override;
    virtual void tableCellTouched(TableView* table,TableViewCell* cell){};
    virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view) override;
    
private:
    void showBackGround();
    using show_head_line::showHeadLine;
    void preventTapEvent();
    void onResponseGetCurrentRanking(Json* response);
    void onResponseGetLastRanking(Json* response);
    void onResponseGetRanking(Json* response);
    void showRankingToggleButton();
    void showRankingInfo();
    void onHelpButtonTapEndedCallback(Ref* sender, ui::Widget::TouchEventType type);
    void showRankingTable();
    void showScrollBar();
    void showNoRankingView();
    bool isSelfPlayer(int playerId);
    void showTweetButtonOnCell(Sprite* cellBackGround, Label* ownerRank, const int ranking);
    void changeToOwnerCell(Sprite* cellBackGround, Label* point);
    void changeColorOfLabelPartString(Label *label, const Color3B &color3b, int offset, int length);
    void requestRankingInit(const OnResponseCallback &onResponseCallback);
    void requestTopRanking();
    void requestSelfRanking();
    
    enum TAG_SPRITE {
        SWITCH_RANKING_BUTTON = 0,
        RANKING_LIST,
        SCROLL_BAR,
        CELL_SELF_OWNER_RANK,
        CELL_SELF_POINT
    };
    const char *EVENT_HELP_URL = "http://syanago.com/ranking/";
    const int TABLE_CELL_HEIGHT = 120;
    
    Size windowSize;
    RequestAPI* api;
    DefaultProgress* progress;
    std::string rankingListColumnName;
    RankingList* rankingList;
};

#endif /* defined(__syanago__RankingLayer__) */