#include "RankingToggleButton.h"
#include "FontDefines.h"
#include "SoundHelper.h"

RankingToggleButton* RankingToggleButton::create(const std::string &file, const std::string& onTapAndEnablefile, const ButtonCallback &onTapTopRankingCallback, const ButtonCallback &onTapSelfRankingCallback)
{
    RankingToggleButton* btn = new (std::nothrow) RankingToggleButton();
    if (btn && btn->init(file, onTapAndEnablefile, onTapAndEnablefile)) {
        btn->initialize(onTapTopRankingCallback, onTapSelfRankingCallback);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void RankingToggleButton::initialize(const ButtonCallback &onTapTopRankingCallback, const ButtonCallback &onTapSelfRankingCallback)
{
    _onTapTopRankingCallback = onTapTopRankingCallback;
    _onTapSelfRankingCallback = onTapSelfRankingCallback;
    addTouchEventListener(CC_CALLBACK_2(RankingToggleButton::onTapButton, this));

    showLabel();
}

void RankingToggleButton::onTapButton(Ref* sender, Widget::TouchEventType type)
{
    if (type == TouchEventType::ENDED) {
        setEnabled(false);
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        if (isSelfRankingRequest()) {
            _onTapSelfRankingCallback();
            label->setString(LABEL_TOP);
        } else {
            _onTapTopRankingCallback();
            label->setString(LABEL_SELF);
        }
    }
}

void RankingToggleButton::showLabel()
{
    label = Label::createWithTTF(LABEL_SELF, FONT_NAME_2, 34);
    label->setLineHeight(34);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    label->setPosition(Point(getContentSize().width / 2, getContentSize().height / 2));
    addChild(label);
}

bool RankingToggleButton::isSelfRankingRequest()
{
    return label->getString() == LABEL_SELF;
}