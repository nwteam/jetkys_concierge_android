#ifndef __syanago__RankingList__
#define __syanago__RankingList__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"

USING_NS_CC;

class RankingList
{
public:
    RankingList(Json* json);
    
    class Ranking
    {
    public:
        Ranking(Json* json);
        
        CC_SYNTHESIZE_READONLY(int, level, Level);
        CC_SYNTHESIZE_READONLY(int, mstCharacterId, MstCharacterId);
        CC_SYNTHESIZE_READONLY(int, playerId, PlayerId);
        CC_SYNTHESIZE_READONLY(std::string, playerName, PlayerName);
        CC_SYNTHESIZE_READONLY(int, point, Point);
        CC_SYNTHESIZE_READONLY(int, ownerRank, OwnerRank);
        CC_SYNTHESIZE_READONLY(int, ranking, Ranking);
    };
    
    CC_SYNTHESIZE_READONLY(int, eventId,EventId);
    CC_SYNTHESIZE_READONLY(std::string, eventEndDateTime, EventEndDateTime);
    CC_SYNTHESIZE_READONLY(std::string, eventStartDateTime, EventStartDateTime);
    CC_SYNTHESIZE(std::string, eventTitle, EventTitle);
    CC_SYNTHESIZE_READONLY(int, myCurrentRanking, MyCurrentRanking);
    CC_SYNTHESIZE_READONLY(int, myLastTimeRanking, MyLastTimeRanking);
    CC_SYNTHESIZE_READONLY(std::vector<Ranking>, rankings, Rankings);
    
    std::string getEventStartDate();
    std::string getEventEndDate();
    int getSelfPlayerRankingIndex();
    bool hasLastTimeRanking();
    int diffMyCurrentRankingAndMyLastRanking();



    
};


#endif /* defined(__syanago__RankingList__) */
