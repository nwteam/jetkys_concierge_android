#include "RankingList.h"
#include "PlayerController.h"

RankingList::RankingList(Json* json)
{
    Json* data = Json_getItem(Json_getItem(json, "get_ranking_list"), "data");
    eventTitle = Json_getString(data, "event_title", "");
    eventId = atoi(Json_getString(data, "event_id", ""));
    eventStartDateTime = Json_getString(data, "event_start_datetime", "");
    eventEndDateTime = Json_getString(data, "event_end_datetime", "");
    myLastTimeRanking = atoi(Json_getString(data, "my_last_time_ranking", ""));
    myCurrentRanking = atoi(Json_getString(data, "my_current_ranking", ""));

    Json* rankingDatas = Json_getItem(data, "ranking_list");
    for (Json* child = rankingDatas->child; child; child = child->next) {
        rankings.push_back(Ranking(child));
    }
}

RankingList::Ranking::Ranking(Json* json)
{
    level = atoi(Json_getString(json, "level", "0"));
    mstCharacterId = atoi(Json_getString(json, "mst_character_id", "0"));
    playerId = atoi(Json_getString(json, "player_id", "0"));
    playerName = Json_getString(json, "player_name", "0");
    point = atoi(Json_getString(json, "point", "0"));
    ownerRank = atoi(Json_getString(json, "rank", "0"));
    ranking = atoi(Json_getString(json, "ranking", "0"));
}

std::string RankingList::getEventStartDate()
{
    return eventStartDateTime.substr(0, 10);
}

std::string RankingList::getEventEndDate()
{
    return eventEndDateTime.substr(0, 10);
}

int RankingList::getSelfPlayerRankingIndex()
{
    for (int i = 0; i < rankings.size(); i++) {
        if (atoi((PLAYERCONTROLLER->getPlayerID().c_str())) == rankings.at(i).getPlayerId()) {
            return i;
        }
    }
    return 0;
}

bool RankingList::hasLastTimeRanking()
{
    return myLastTimeRanking > 0;
}

int RankingList::diffMyCurrentRankingAndMyLastRanking()
{
    return myLastTimeRanking - myCurrentRanking;
}