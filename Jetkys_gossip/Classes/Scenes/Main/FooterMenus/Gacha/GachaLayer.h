#ifndef __syanago__GachaLayer__
#define __syanago__GachaLayer__

#include "cocos2d.h"
#include "create_func.h"
#include "GachaModel.h"
#include "GachaResponseModel.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"

USING_NS_CC;

using namespace SyanagoAPI;

class MainScene;

class GachaLayer : public Layer, public create_func<GachaLayer>
{
public:
    typedef std::function<void(int playerCharacterId)> ShowCharacterDetailCallback;
    typedef std::function<void(int masterPartsId, int playerPartsId)> ShowPartsDetailCallback;
    typedef std::function<void(int playerCharacterId)> ShowGachaCharacterDetailCallback;
    typedef std::function<void(int masterPartsId, int playerPartsId)> ShowGachaPartsDetailCallback;
    typedef std::function<void()> HeaderAndFooterHideCallBack;
    typedef std::function<void()> HeaderAndFooterViewCallBack;
    using create_func::create;
    bool init();
    
    GachaLayer();
    ~GachaLayer();
    
    void resetPageTag();//comment 初期化を行っているが、必要性は不明(他に良い方法がある)
    void playTutorialGacha();
    
    MainScene* _mainScene;
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        HEADLINE,
        PREMIUM_BUTTON,
        FRIEND_BUTTON,
        BAGE,
        DIALOG,
        RESULT,
        EFFECT,
        
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_HEADLINE,
        Z_PREMIUM_BUTTON,
        Z_FRIEND_BUTTON,
        Z_BAGE,
        Z_DIALOG,
        Z_RESULT,
        Z_EFFECT,
    };
    RequestAPI* request;
    DefaultProgress* progress;
    
    int _logActionTryCount;
    GACHA_TYPE _TryGachaType;
    std::shared_ptr<GachaResponseModel> _gachaResponseModel;
    
    void showBackground();
    void showHeadLine();
    void showGachaButton();
    
    void showFriendGachaBage();
    const int getTryFriendGachaCount();
    
    void onTapGachaButton(const GACHA_TYPE type);
    void gachaSelectDialogCallback(const GACHA_TYPE type, const int tryCount);
    void errorCallback(GAHCA_ERROR gachaError);
    void goToTokenShopCallback();
    void successCallback(const GACHA_TYPE type, const int tryCount);
    
    void onResponsePayGachaResult(Json *response);
    void onResponseLogAction(Json *response);
    void onResponseFreeGachaResult(Json *response);
    void setGachaResultData(Json* data);
    
    void showGachaResult(std::shared_ptr<GachaResponseModel>& model);
    void onCharacterDetailCallback(const int playerCharacterId, const int masterCharacterId);
    void onPartsDetailCallback(const int playerPartsId, const int masterPartsId);
    
    void gachaEffectCallback();
    void dropEffectCallBack();
};



#endif