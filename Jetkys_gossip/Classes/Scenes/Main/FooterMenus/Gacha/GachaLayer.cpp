#include "GachaLayer.h"
#include "MainScene.h"
#include "HeaderStatus.h"
#include "FontDefines.h"
#include "GachaButton.h"
#include "PlayerController.h"
#include "UserDefaultManager.h"
#include "Native.h"
#include "ArtLtvMeasurement.h"
#include "MeasurementInformation.h"
#include "SelectGachaDialog.h"
#include "GachaEffectModel.h"
#include "GachaEffectLayer.h"
#include "DropEffectLayer.h"
#include "GachaResultLayer.h"
#include "OverHoldContentDialog.h"
#include "ShortMessageDialog.h"
#include "NotEnoughForTokenDialog.h"
#include "SystemSettingModel.h"

GachaLayer::GachaLayer():
    _mainScene(nullptr)
    , request(nullptr)
    , progress(nullptr)
    , _logActionTryCount(0)
{
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
}

GachaLayer::~GachaLayer()
{
    delete progress;
    delete request;
    progress = nullptr;
    request = nullptr;
    _gachaResponseModel = nullptr;
}

bool GachaLayer::init()
{
    if (!Layer::create()) {
        return false;
    }

    showBackground();
    showHeadLine();
    showGachaButton();
    showFriendGachaBage();

    return true;
}

void GachaLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getWinSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void GachaLayer::showHeadLine()
{
    auto headLineBackground = Sprite::create("head_line_no1.png");
    headLineBackground->setPosition(Vec2(headLineBackground->getContentSize().width / 2,
                                         Director::getInstance()->getWinSize().height - headLineBackground->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 12));
    headLineBackground->setTag(TAG_SPRITE::HEADLINE);
    addChild(headLineBackground, Z_ORDER::Z_HEADLINE);
    auto titleLabel = Label::createWithTTF("ガチャ", FONT_NAME_2, 34);
    titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    titleLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
    titleLabel->setPosition(Point(14, -17 + headLineBackground->getContentSize().height / 2));
    titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    headLineBackground->addChild(titleLabel);
}

void GachaLayer::showGachaButton()
{
    auto friendButton = GachaButton::create(FRIEND, "gacha_friend_button.png", CC_CALLBACK_1(GachaLayer::onTapGachaButton, this));
    friendButton->setTag(TAG_SPRITE::FRIEND_BUTTON);
    Size winSize = Director::getInstance()->getWinSize();
    friendButton->setPosition(Vec2(winSize.width / 2, winSize.height / 2 + 15 + friendButton->getContentSize().height / 2));
    addChild(friendButton, Z_ORDER::Z_FRIEND_BUTTON);

    auto premiumButton = GachaButton::create(PREMIUM, "gacha_premium_button.png", CC_CALLBACK_1(GachaLayer::onTapGachaButton, this));
    premiumButton->setTag(TAG_SPRITE::PREMIUM_BUTTON);
    premiumButton->setPosition(Vec2(winSize.width / 2, friendButton->getPosition().y - friendButton->getContentSize().height / 2 - 30 - premiumButton->getContentSize().height / 2));
    addChild(premiumButton, Z_ORDER::Z_PREMIUM_BUTTON);
}

void GachaLayer::showFriendGachaBage()
{
    if (getChildByTag(TAG_SPRITE::BAGE)) {
        removeChildByTag(TAG_SPRITE::BAGE);
    }
    const int tryFrindGachaCount = getTryFriendGachaCount();
    if (tryFrindGachaCount < 1) {
        return;
    }

    auto bage = Sprite::create("itemSpr.png");
    bage->setTag(TAG_SPRITE::BAGE);
    auto friendGachaButton = getChildByTag(TAG_SPRITE::FRIEND_BUTTON);
    bage->setPosition(friendGachaButton->getPosition() + (friendGachaButton->getContentSize() / 2) - Vec2(5, 5));
    addChild(bage, Z_ORDER::Z_BAGE);

    auto bageLabel = Label::createWithTTF(StringUtils::format("%d", tryFrindGachaCount).c_str(), FONT_NAME_2, 22);
    bageLabel->setPosition(Point(bage->getContentSize().width / 2, bage->getContentSize().height / 2 - 11));
    bage->addChild(bageLabel);
}

const int GachaLayer::getTryFriendGachaCount()
{
    std::shared_ptr<SystemSettingModel>model(SystemSettingModel::getModel());
    const int result = PLAYERCONTROLLER->_player->getFriendPoint() / model->getFpForFreeGacha();
    if (result > 99) {
        return 99;
    }
    return result;
}

void GachaLayer::onTapGachaButton(const GACHA_TYPE type)
{
    auto dialog = SelectGachaDialog::create(type, CC_CALLBACK_2(GachaLayer::gachaSelectDialogCallback, this));
    dialog->setTag(TAG_SPRITE::DIALOG);
    addChild(dialog, Z_ORDER::Z_DIALOG);
}

void GachaLayer::gachaSelectDialogCallback(const GACHA_TYPE type, const int tryCount)
{
    std::shared_ptr<GachaModel>model(new GachaModel(type, tryCount, CC_CALLBACK_2(GachaLayer::successCallback, this), CC_CALLBACK_1(GachaLayer::errorCallback, this)));
}

void GachaLayer::errorCallback(GAHCA_ERROR gachaError)
{
    if (gachaError == OVER_HOLD_CHRACTER) {
        _mainScene->showOverHoldDialog("[車なご]がいっぱいです。");
        return;
    }
    if (gachaError == OVER_HOLD_PARTS) {
        _mainScene->showOverHoldDialog("[装備]がいっぱいです。");
        return;
    }
    if (gachaError == OVER_HOLD_CHRACTER_AND_PARTS) {
        _mainScene->showOverHoldDialog("[車なご][装備]がいっぱいです。");
        return;
    }
    if (gachaError == NOT_ENOUGH_TOKEN) {
        auto dialog = NotEnoughForTokenDialog::create(CC_CALLBACK_0(GachaLayer::goToTokenShopCallback, this));
        dialog->setTag(TAG_SPRITE::DIALOG);
        addChild(dialog, Z_ORDER::Z_DIALOG);
        return;
    }
    if (gachaError == NOT_ENOUGH_FRIEND_POINT) {
        auto dialog = ShortMessageDialog::create("フレンドポイントが足りません", [this]() {});
        dialog->setTag(TAG_SPRITE::DIALOG);
        addChild(dialog, Z_ORDER::Z_DIALOG);
        return;
    }
    CCASSERT(0, "不明なエラーが発生しました");
}

void GachaLayer::goToTokenShopCallback()
{
    if (_mainScene == nullptr) {
        return;
    }
    _mainScene->goToTokenShop();
}

void GachaLayer::successCallback(const GACHA_TYPE type, const int tryCount)
{
    _TryGachaType = type;
    if (type == PREMIUM) {
        if (PLAYERCONTROLLER->_player->getGameStep() == 1) {
            _logActionTryCount = tryCount;
            ArtLtvMeasurement::send(MEASURMENT_INFO::ART::TRIED_FIRST_TIME_PAY_GACHA, (int)PLAYERCONTROLLER->_player->getID());
            progress->onStart();
            request->setFirstGacha(PLAYERCONTROLLER->_player->getRank(), CC_CALLBACK_1(GachaLayer::onResponseLogAction, this));
        } else {
            if (tryCount == 10) {
                ArtLtvMeasurement::send(MEASURMENT_INFO::ART::TRIED_10_COUNT_PAY_GACHA, (int)PLAYERCONTROLLER->_player->getID());
            } else {
                ArtLtvMeasurement::send(MEASURMENT_INFO::ART::TRIED_PAY_GACHA, (int)PLAYERCONTROLLER->_player->getID());
            }
            progress->onStart();
            request->getPayGachaResult(tryCount, 1, CC_CALLBACK_1(GachaLayer::onResponsePayGachaResult, this));
        }
    } else if (type == FRIEND) {
        ArtLtvMeasurement::send(MEASURMENT_INFO::ART::TRIED_FREE_GACHA, (int)PLAYERCONTROLLER->_player->getID());
        progress->onStart();
        request->getFreeGachaResult(tryCount, CC_CALLBACK_1(GachaLayer::onResponseFreeGachaResult, this));
    }
}

void GachaLayer::onResponseLogAction(Json* response)
{
    if (strcmp(Json_getItem(Json_getItem(response, "log_action"), "result")->valueString, "false") == 0) {
        return;
    }
    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::OPEN_GACHA, true);
    progress->onStart();
    request->getPayGachaResult(_logActionTryCount, 1, CC_CALLBACK_1(GachaLayer::onResponsePayGachaResult, this));
}

void GachaLayer::onResponsePayGachaResult(Json* response)
{
    if (strcmp(Json_getItem(Json_getItem(response, "get_pay_gacha_result"), "result")->valueString, "false") == 0) {
        return;
    }
    auto data = Json_getItem(Json_getItem(response, "get_pay_gacha_result"), "data");
    PLAYERCONTROLLER->_player->setFreeToken(atoi(Json_getString(Json_getItem(data, "players"), "free_token", "-1")));
    PLAYERCONTROLLER->_player->setPayToken(atoi(Json_getString(Json_getItem(data, "players"), "pay_token", "-1")));
    setGachaResultData(data);
}

void GachaLayer::onResponseFreeGachaResult(Json* response)
{
    if (strcmp(Json_getItem(Json_getItem(response, "get_free_gacha_result"), "result")->valueString, "false") == 0) {
        return;
    }
    auto data = Json_getItem(Json_getItem(response, "get_free_gacha_result"), "data");
    PLAYERCONTROLLER->_player->setFriendPoint(atoi(Json_getString(Json_getItem(data, "players"), "friend_point", "-1")));
    setGachaResultData(data);
}

void GachaLayer::setGachaResultData(Json* data)
{
    std::shared_ptr<GachaResponseModel>model(new GachaResponseModel(data));
    _gachaResponseModel = model;
    showGachaResult(model);

    std::shared_ptr<GachaEffectModel>effectModel(new GachaEffectModel(model->getMaxRarity(), _TryGachaType));
    auto gachaEffectLayer = GachaEffectLayer::create(effectModel, CC_CALLBACK_0(GachaLayer::gachaEffectCallback, this));
    gachaEffectLayer->setTag(TAG_SPRITE::EFFECT);
    addChild(gachaEffectLayer, Z_ORDER::Z_EFFECT);
    if (_mainScene == nullptr) {
        return;
    }
    _mainScene->footerLayer->setVisible(false);
    _mainScene->headerLayer->setVisible(false);
}

void GachaLayer::showGachaResult(std::shared_ptr<GachaResponseModel>& model)
{
    auto resultLayer = GachaResultLayer::create(model, CC_CALLBACK_2(GachaLayer::onCharacterDetailCallback, this), CC_CALLBACK_2(GachaLayer::onPartsDetailCallback, this));
    resultLayer->setTag(TAG_SPRITE::RESULT);
    addChild(resultLayer, Z_ORDER::Z_RESULT);
}

void GachaLayer::onCharacterDetailCallback(const int playerCharacterId, const int masterCharacterId)
{
    if (_mainScene == nullptr) {
        return;
    }
    _mainScene->showDetailPlayerCharacter(playerCharacterId);
}

void GachaLayer::onPartsDetailCallback(const int playerPartsId, const int masterPartsId)
{
    if (_mainScene == nullptr) {
        return;
    }
    _mainScene->showDetailParts(masterPartsId, playerPartsId);
}

void GachaLayer::gachaEffectCallback()
{
    auto dropEffectLayer = DropEffectLayer::create(_gachaResponseModel->getContentType(), _gachaResponseModel->getMaxRarityContentMasterId(), false, CC_CALLBACK_0(GachaLayer::dropEffectCallBack, this));
    dropEffectLayer->setTag(TAG_SPRITE::EFFECT);
    addChild(dropEffectLayer, Z_ORDER::Z_EFFECT);
}

void GachaLayer::dropEffectCallBack()
{
    if (_mainScene == nullptr) {
        return;
    }
    showFriendGachaBage();
    _mainScene->footerLayer->setVisible(true);
    _mainScene->headerLayer->setVisible(true);
    _mainScene->footerLayer->setBage();
    _mainScene->headerLayer->updateInfoLayer();
    if (_gachaResponseModel->getContentType() == DROP_CHARACTER) {
        _mainScene->showGachaDetailPlayerCharacter(_gachaResponseModel->getMaxRarityContentPlayerId());
    } else if (_gachaResponseModel->getContentType() == DROP_PART) {
        _mainScene->showGachaDetailParts(_gachaResponseModel->getMaxRarityContentMasterId(), _gachaResponseModel->getMaxRarityContentPlayerId());
    }
}

void GachaLayer::resetPageTag()
{
    removeAllChildren();
    init();
}

void GachaLayer::playTutorialGacha()
{
    successCallback(PREMIUM, 1);
}
