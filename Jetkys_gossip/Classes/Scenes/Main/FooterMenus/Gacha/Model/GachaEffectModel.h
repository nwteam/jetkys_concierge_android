#ifndef __syanago__GachaEffectModel__
#define __syanago__GachaEffectModel__

#include "cocos2d.h"
#include "GachaModel.h"

USING_NS_CC;

class GachaEffectModel
{
public:
    GachaEffectModel(const int rarity, const GACHA_TYPE type);
    ~GachaEffectModel();
    
    CC_SYNTHESIZE_READONLY(int, rarity, Rarity);
    CC_SYNTHESIZE_READONLY(std::string, carFilePath, CarFilePath);
    CC_SYNTHESIZE_READONLY(std::string, anversFilePath, AnversFilePath);
    CC_SYNTHESIZE_READONLY(std::string, cameramanFilePath, CameramanFilePath);
    CC_SYNTHESIZE_READONLY(Animation*, anversAnimation, AnversAnimation);
private:
    enum CAR_COLOR{
        GOALD,
        BLACK,
    };
    const float ANIMATION_DELAY = 0.2f;
    
    void setFilePath(const int rarity, const GACHA_TYPE type);
    void setAnimation(const GACHA_TYPE type);
};

#endif
