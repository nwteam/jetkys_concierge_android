#include "GachaModel.h"
#include "PlayerController.h"
#include "SystemSettingModel.h"
#include "MeasurementInformation.h"
#include "Native.h"
#include "UserDefaultManager.h"

GachaModel::GachaModel(const GACHA_TYPE type, const int tryCount, const OnSuccessCallback& successCallback, const OnErrorCallback& errorCallback):
    _successCallback(successCallback), _errorCallback(errorCallback)
{
    chackOverHold(type, tryCount);
}

GachaModel::~GachaModel()
{}

void GachaModel::chackOverHold(const GACHA_TYPE type, const int tryCount)
{
    bool character = false;
    bool parts = false;
    if (PLAYERCONTROLLER->_playerCharacterModels.size() >= PLAYERCONTROLLER->_player->getGarageSize()) {
        if (type == PREMIUM) {
            character = true;
        }
        if (PLAYERCONTROLLER->_playerParts.size() >= (PLAYERCONTROLLER->_player->getGarageSize()) * 2) {
            parts = true;
        }
    }
    if (character == true && parts == true) {
        _errorCallback(OVER_HOLD_CHRACTER_AND_PARTS);
        return;
    }
    if (character == true) {
        _errorCallback(OVER_HOLD_CHRACTER);
        return;
    }
    if (parts == true) {
        _errorCallback(OVER_HOLD_PARTS);
        return;
    }

    chackHoldProperty(type, tryCount);
}

void GachaModel::chackHoldProperty(const GACHA_TYPE type, const int tryCount)
{
    std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
    if (type == PREMIUM) {
        const int totalToken = PLAYERCONTROLLER->_player->getFreeToken() + PLAYERCONTROLLER->_player->getPayToken();
        const int payToken = systemSettingModel->getTokenForPayGacha() * tryCount;
        if (totalToken < payToken) {
            _errorCallback(NOT_ENOUGH_TOKEN);
            return;
        }
    } else if (type == FRIEND)    {
        const int totalFp = PLAYERCONTROLLER->_player->getFriendPoint();
        const int payFp = systemSettingModel->getFpForFreeGacha();
        if (totalFp < payFp) {
            _errorCallback(NOT_ENOUGH_FRIEND_POINT);
            return;
        }
    }
    _successCallback(type, tryCount);
}