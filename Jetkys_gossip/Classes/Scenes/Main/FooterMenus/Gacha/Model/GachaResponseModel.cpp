#include "GachaResponseModel.h"
#include "PlayerController.h"
#include "CharacterModel.h"
#include "PartModel.h"

GachaResponseModel::GachaResponseModel(Json* data):
    maxRarity(0)
{
    Json* jsonDataChara = Json_getItem(data, "player_characters");
    if (jsonDataChara != NULL) {
        for (Json* child = jsonDataChara->child; child; child = child->next) {
            PLAYERCONTROLLER->addCharacter(child, false, true);
            const int masterCharacterId = atoi(Json_getString(child, "characters_id", ""));
            const int playerCharacterId = atoi(Json_getString(child, "id", ""));
            masterCharacterIds.push_back(masterCharacterId);
            playerCharacterIds.push_back(playerCharacterId);
            std::shared_ptr<CharacterModel>model(CharacterModel::find(masterCharacterId));
            if (maxRarity < model->getRarity()) {
                contentType = DROP_CHARACTER;
                maxRarity = model->getRarity();
                maxRarityContentMasterId = masterCharacterId;
                maxRarityContentPlayerId = playerCharacterId;
            }
        }
    }

    Json* jsonDataParts = Json_getItem(data, "player_parts");
    if (jsonDataParts != NULL) {
        for (Json* child = jsonDataParts->child; child; child = child->next) {
            setPlayerPartsData(child);
            const int masterPartsId = atoi(Json_getString(child, "parts_id", ""));
            const int playerPartsId = atoi(Json_getString(child, "id", ""));
            masterPartsIds.push_back(masterPartsId);
            playerPartsIds.push_back(playerPartsId);
            std::shared_ptr<PartModel>model(PartModel::find(masterPartsId));
            if (maxRarity < model->getRarity() && contentType != DROP_CHARACTER) {
                contentType = DROP_PART;
                maxRarity = model->getRarity();
                maxRarityContentMasterId = masterPartsId;
                maxRarityContentPlayerId = playerPartsId;
            }
        }
    }
}

GachaResponseModel::~GachaResponseModel()
{}


void GachaResponseModel::setPlayerPartsData(Json* child)
{
    PLAYERCONTROLLER->addPart(atoi(Json_getString(child, "id", "")),
                              atoi(Json_getString(child, "player_id", "")),
                              atoi(Json_getString(child, "parts_id", "")),
                              Json_getString(child, "created", ""),
                              Json_getString(child, "modified", ""), 1);
}
