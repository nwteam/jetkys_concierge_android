#ifndef __syanago__GachaResponseModel__
#define __syanago__GachaResponseModel__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"
#include "DropEffectLayer.h"

USING_NS_CC;

class GachaResponseModel
{
public:
    GachaResponseModel(Json* data);
    ~GachaResponseModel();
    
    CC_SYNTHESIZE_READONLY(std::vector<int>, masterCharacterIds, MasterCharacterIds);
    CC_SYNTHESIZE_READONLY(std::vector<int>, playerCharacterIds, PlayerCharacterIds);
    CC_SYNTHESIZE_READONLY(std::vector<int>, masterPartsIds, MasterPartsIds);
    CC_SYNTHESIZE_READONLY(std::vector<int>, playerPartsIds, PlayerPartsIds);
    CC_SYNTHESIZE_READONLY(int, maxRarity, MaxRarity);
    CC_SYNTHESIZE_READONLY(int, maxRarityContentMasterId, MaxRarityContentMasterId);
    CC_SYNTHESIZE_READONLY(int, maxRarityContentPlayerId, MaxRarityContentPlayerId);
    CC_SYNTHESIZE_READONLY(GET_TYPE, contentType, ContentType);
    
private:
    void setPlayerPartsData(Json* child);
};

#endif