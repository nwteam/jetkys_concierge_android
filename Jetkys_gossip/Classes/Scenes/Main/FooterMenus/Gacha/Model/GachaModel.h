#ifndef __syanago__GachaModel__
#define __syanago__GachaModel__

#include "cocos2d.h"
#include "GachaResponseModel.h"

USING_NS_CC;

enum GACHA_TYPE {
    FRIEND,
    PREMIUM,
};

enum GAHCA_ERROR {
    OVER_HOLD_CHRACTER,
    OVER_HOLD_PARTS,
    OVER_HOLD_CHRACTER_AND_PARTS,
    NOT_ENOUGH_TOKEN,
    NOT_ENOUGH_FRIEND_POINT,
};

class GachaModel
{
public:
    typedef std::function<void(GAHCA_ERROR gachaError)> OnErrorCallback;
    typedef std::function<void(const GACHA_TYPE type, const int tryCount)> OnSuccessCallback;
    GachaModel(const GACHA_TYPE type, const int tryCount, const OnSuccessCallback& successCallback, const OnErrorCallback& errorCallback);
    ~GachaModel();
    
private:
    const OnSuccessCallback& _successCallback;
    const OnErrorCallback& _errorCallback;
    
    void chackOverHold(const GACHA_TYPE type, const int tryCount);
    void chackHoldProperty(const GACHA_TYPE type, const int tryCount);
};

#endif
