#include "GachaEffectModel.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "UtilsIOS.h"
#endif

GachaEffectModel::GachaEffectModel(const int _rarity, const GACHA_TYPE type)
{
    rarity = _rarity;
    setFilePath(_rarity, type);
    setAnimation(type);
}

GachaEffectModel::~GachaEffectModel()
{
    
}

void GachaEffectModel::setFilePath(const int rarity, const GACHA_TYPE type)
{
    if(rarity > 3)
    {
        carFilePath = "gacha_goldcar.png";
    }
    else
    {
        carFilePath = "gacha_blackcar.png";
    }
    
    if(type == PREMIUM)
    {
        anversFilePath = "gacha_goldcloth_1.png";
    }
    else if(type == FRIEND)
    {
        anversFilePath = "gacha_normalcloth_1.png";
    }
    cameramanFilePath = "gacha_people.png";
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (isWideScreen()) {
        cameramanFilePath = "gacha_people_1136.png";
    }
#endif
}

void GachaEffectModel::setAnimation(const GACHA_TYPE type)
{
    anversAnimation = Animation::create();
    for( int i=1;i<=5;i++)
    {
        std::string filePath;
        if(type == PREMIUM)
        {
            filePath = StringUtils::format("gacha_goldcloth_%d.png", i);
        }
        else if(type == FRIEND)
        {
            filePath =  StringUtils::format("gacha_normalcloth_%d.png", i);
        }
        auto sprite = Sprite::create(filePath.c_str());
        anversAnimation->addSpriteFrame(sprite->getSpriteFrame());
    }
    anversAnimation->setDelayPerUnit(ANIMATION_DELAY);
    anversAnimation->setRestoreOriginalFrame(false);
}
