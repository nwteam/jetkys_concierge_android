#ifndef __syanago__GachaButton__
#define __syanago__GachaButton__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "GachaModel.h"

USING_NS_CC;

class GachaButton : public ui::Button
{
public:
    typedef std::function<void(const GACHA_TYPE type)> OnResponceCallback;
    GachaButton();
    static GachaButton* create(const GACHA_TYPE type, const std::string buttonFileName,const OnResponceCallback& callBak);
    void initialize(const GACHA_TYPE type, const OnResponceCallback& callBak);
    void onTapGachaButton(Ref *sender, Widget::TouchEventType type);
private:
    enum TAG_SPRITE{
        BUTTON = 0,
    };
    OnResponceCallback _callback;
    GACHA_TYPE _type;
};

#endif