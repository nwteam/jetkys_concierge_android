#include "GachaButton.h"
#include "SoundHelper.h"


GachaButton::GachaButton():
    _callback(nullptr)
{}

GachaButton* GachaButton::create(const GACHA_TYPE type, const std::string buttonFileName, const OnResponceCallback& callBak)
{
    GachaButton* btn = new (std::nothrow) GachaButton();
    if (btn && btn->init(buttonFileName.c_str())) {
        btn->initialize(type, callBak);
        btn->setZoomScale(0);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void GachaButton::initialize(const GACHA_TYPE type, const OnResponceCallback& callBak)
{
    _callback = callBak;
    _type = type;
    addTouchEventListener(CC_CALLBACK_2(GachaButton::onTapGachaButton, this));
}

void GachaButton::onTapGachaButton(Ref* sender, Widget::TouchEventType type)
{
    if (type == TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _callback(_type);
    }
}