#include "GachaResultLayer.h"
#include "jCommon.h"
#include "SoundHelper.h"

GachaResultLayer::GachaResultLayer()
{}

GachaResultLayer::~GachaResultLayer()
{
    _gachaResponseModel = nullptr;
}

bool GachaResultLayer::init(const std::shared_ptr<GachaResponseModel>& model, const OnTapCharacterDetail& characterDetailCallback, const OnTapPartsDetail& partsDetailCallback)
{
    if (!Layer::create()) {
        return false;
    }
    _gachaResponseModel = model;
    _characterDetailCallback = characterDetailCallback;
    _partsDetailCallback = partsDetailCallback;

    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    showbackground();
    showHeadLine(this, "ガチャリザルト", CC_CALLBACK_1(GachaResultLayer::backButtoncallBack, this));
    showResult(model);

    return true;
}

void GachaResultLayer::showbackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getWinSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void GachaResultLayer::backButtoncallBack(Ref* pSender)
{
    removeFromParent();
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
}

void GachaResultLayer::showResult(const std::shared_ptr<GachaResponseModel>& model)
{
    auto getItems = createResultBase(model);
    addChild(getItems, Z_ORDER::Z_RESULT_BASE);

    int libonHeight = 0;
    Sprite* characterRibon = nullptr;
    if (model->getMasterCharacterIds().size() > 0) {
        characterRibon = createResultRibon("車なごが仲間になりました!");
        addChild(characterRibon, Z_ORDER::Z_RESULT_RIBON);
        libonHeight += (characterRibon->getContentSize().height + 17) / 2;
    }

    Sprite* partsRibon = nullptr;
    if (model->getMasterPartsIds().size() > 0) {
        partsRibon = createResultRibon("装備を手に入れました!");
        addChild(partsRibon, Z_ORDER::Z_RESULT_RIBON);
        libonHeight += (partsRibon->getContentSize().height + 17) / 2;
    }

    getItems->setPosition(Point(Director::getInstance()->getWinSize() / 2) - Point(0, libonHeight));
    auto posi = Point(getItems->getPosition() + Point(0, getItems->getContentSize().height / 2));
    if (model->getMasterPartsIds().size() > 0) {
        partsRibon->setPosition(Point(0, posi.y + 17 + partsRibon->getContentSize().height / 2));
        posi = partsRibon->getPosition() + Point(0, partsRibon->getContentSize().height / 2);
    }
    if (model->getMasterCharacterIds().size() > 0) {
        characterRibon->setPosition(Point(0, posi.y + 17 + characterRibon->getContentSize().height / 2));
    }
}

Sprite* GachaResultLayer::createResultRibon(const std::string message)
{
    Sprite* result = makeSprite("gacha_text_base.png");
    result->setTag(TAG_SPRITE::RESULT_RIBON);
    result->setAnchorPoint(Vec2(0.0f, 0.5f));
    auto messageLabel = Label::createWithTTF(message, FONT_NAME_2, 30);
    messageLabel->setPosition(Point(Director::getInstance()->getWinSize().width / 2, result->getContentSize().height / 2 - 15));
    messageLabel->setColor(Color3B::WHITE);
    messageLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->addChild(messageLabel);
    return result;
}

Sprite* GachaResultLayer::createResultBase(const std::shared_ptr<GachaResponseModel>& model)
{
    Sprite* resultBase = Sprite::create("gacha_result_base.png");
    resultBase->setTag(TAG_SPRITE::RESULT_BASE);
    const long itemCount = model->getMasterCharacterIds().size() + model->getMasterPartsIds().size();

    Vector<MenuItem*>menuGetItems;
    MenuItem* menuItem = MenuItem::create();
    Sprite* pPicture = Sprite::create();

    int count = 0, oldY =  ((itemCount <= 5 ? 0 : 2)  + 1) * (pictureSize.height + spaceColumn) - 15;
    int partsCount = 0;
    int CharacterCount = 0;
    for (int y = 0; y < (itemCount / 5) + 1; y++) {
        int oldX = 15;
        for (int x = 0; x < 5; x++) {
            count++;
            if (count > itemCount) break;
            if (model->getMasterCharacterIds().size() > 0 && CharacterCount < model->getMasterCharacterIds().size()) {
                pPicture = makeSprite(StringUtils::format("%d_i.png", model->getMasterCharacterIds().at(CharacterCount)).c_str());
                menuItem = MenuItemSprite::create(pPicture, pPicture, CC_CALLBACK_1(GachaResultLayer::onTapCharacterCallback, this));
                menuItem->setTag(CharacterCount);
                CharacterCount++;
            } else if (model->getMasterPartsIds().size() > 0 && partsCount < model->getMasterPartsIds().size()) {
                pPicture = makeSprite(StringUtils::format("%d_pag.png", model->getMasterPartsIds().at(partsCount)).c_str());
                menuItem = MenuItemSprite::create(pPicture, pPicture, CC_CALLBACK_1(GachaResultLayer::onTapPartsCallBack, this));
                menuItem->setTag(partsCount);
                partsCount++;
            }
            auto size = menuItem->getContentSize();
            menuItem->setPosition(Point(oldX + size.width + spaceRow, oldY - size.height - spaceColumn));
            oldX = menuItem->getPosition().x;
            menuGetItems.pushBack(menuItem);
        }
        oldY = menuItem->getPosition().y;
    }

    Menu* menu = Menu::createWithArray(menuGetItems);
    if (itemCount > 5) {
        menu->setPosition(Point(-53, -50));
        menu->setAnchorPoint(Point(0.0f, 1.0f));
    } else {
        menu->setPosition(Point(-53, -50 + 240 - 14));
        menu->setAnchorPoint(Point(0.0f, 1.0f));
    }
    resultBase->addChild(menu);
    return resultBase;
}

void GachaResultLayer::onTapCharacterCallback(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    const int tag = ((Node*)pSender)->getTag();
    _characterDetailCallback(_gachaResponseModel->getPlayerCharacterIds().at(tag), _gachaResponseModel->getMasterCharacterIds().at(tag));
}

void GachaResultLayer::onTapPartsCallBack(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    const int tag = ((Node*)pSender)->getTag();
    _partsDetailCallback(_gachaResponseModel->getPlayerPartsIds().at(tag), _gachaResponseModel->getMasterPartsIds().at(tag));
}