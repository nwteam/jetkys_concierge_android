#ifndef __syanago__GachaResultLayer__
#define __syanago__GachaResultLayer__

#include "cocos2d.h"
#include "create_func.h"
#include "GachaResponseModel.h"
#include "show_head_line.h"

USING_NS_CC;

class GachaResultLayer : public Layer, public create_func<GachaResultLayer>, public show_head_line
{
public:
    typedef std::function<void(const int playerCharacterId, const int masterCharacterId)> OnTapCharacterDetail;
    typedef std::function<void(const int playerPartsId, const int masterPartsId)> OnTapPartsDetail;
    using create_func::create;
    bool init(const std::shared_ptr<GachaResponseModel>& model, const OnTapCharacterDetail& characterDetailCallback, const OnTapPartsDetail& partsDetailCallback);
    
    GachaResultLayer();
    ~GachaResultLayer();
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        RESULT_BASE,
        RESULT_RIBON,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_RESULT_BASE,
        Z_RESULT_RIBON,
    };
    const Size pictureSize = Size(106, 106);
    const int spaceRow = 10;
    const int spaceColumn = 10;
    
    std::shared_ptr<GachaResponseModel> _gachaResponseModel;
    OnTapPartsDetail _partsDetailCallback;
    OnTapCharacterDetail _characterDetailCallback;
    
    void showbackground();
    void backButtoncallBack(Ref* pSender);
    void showResult(const std::shared_ptr<GachaResponseModel>& model);
    Sprite* createResultRibon(const std::string message);
    Sprite* createResultBase(const std::shared_ptr<GachaResponseModel>& model);
    void onTapCharacterCallback(Ref* pSender);
    void onTapPartsCallBack(Ref* pSender);
};

#endif