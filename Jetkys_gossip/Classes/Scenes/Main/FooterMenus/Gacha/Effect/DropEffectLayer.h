#ifndef __syanago__DropEffectLayer__
#define __syanago__DropEffectLayer__

#include "cocos2d.h"
#include "create_func.h"

USING_NS_CC;

enum GET_TYPE{
    DROP_CHARACTER,
    DROP_PART,
};

class DropEffectLayer : public Layer, public create_func<DropEffectLayer>
{
public:
    typedef std::function<void()> OnEndEffectCallback;
    using create_func::create;
    bool init(const GET_TYPE getType, const int viewContentId, const bool isEndBlackOutEffect, const OnEndEffectCallback& callback);
    
    DropEffectLayer();
    ~DropEffectLayer();
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        GET_CONTENT,
        TELOP_MESSAGE,
        CONFETTI,
        RARITY,
        CHARACTER_WORD,
        WHITE_IN_LAYER,
        BLACK_OUT_LAYER,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_GET_CONTENT,
        Z_TELOP_MESSAGE,
        Z_CONFETTI,
        Z_RARITY,
        Z_CHARACTER_WORD,
        Z_WHITE_IN_LAYER,
        Z_BLACK_OUT_LAYER,
    };
    float efectTime = 2.5f;
    float scaleTotime = 0.3f;
    float rotateTime = 0.3f;
    int rotateCount = 2;
    float fadeInTime = 0.2f;
    float nextTime = 0.1f;
    float spase = 20.0;
    
    const float EFFECT_FADE_OUT_TIME = 1.0f;
    const float EFFECT_FADE_IN_TIME = 1.0f;
    const float EFFECT_MOVE_TIME = 1.0f;
    const float EFFECT_MOVE_DELTA = 50.0f;
    const float BLACK_OUT_TIME = 0.3f;
    
    OnEndEffectCallback _callback;
    EventListenerTouchOneByOne* _listener;
    GET_TYPE _getType;
    int _viewContentId;
    
    void showWhiteLayer();
    void showBackground();
    void showGetContent(const GET_TYPE type, const int Id);
    void showConfetti();
    void showRarityStar(const int rarity);
    Sprite* createrRarityStar(const int Tag, const float effectStartDilayTime, const Vec2 position);
    void showTelopMessage(const std::string telopString);
    void playDropSE();
    
    void setTouchDetect(const cocos2d::EventListenerTouchOneByOne::ccTouchCallback& callback);
    void onTouchRarityStarEnded(const GET_TYPE type, const bool isEndBlackOutEffect);
    void onTouchVoiceWordEnded(const bool isEndBlackOutEffect);
    
    void removeRarityStar();
    void showCharacterWord(const int masterCharacterId);
    
    
    void endEffectCallback();
    void showBlackOuteffect(const bool isEndBlackOutEffect);
};

#endif