#ifndef __syanago__GachaEffectLayer__
#define __syanago__GachaEffectLayer__

#include "cocos2d.h"
#include "create_func.h"
#include "GachaModel.h"
#include "GachaEffectModel.h"

USING_NS_CC;

class GachaEffectLayer : public Layer, public create_func<GachaEffectLayer>
{
public:
    typedef std::function<void()> OnEndEffectCallback;
    using create_func::create;
    bool init(const std::shared_ptr<GachaEffectModel>& model, const OnEndEffectCallback& callback);
    
    GachaEffectLayer();
    ~GachaEffectLayer();
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        CAR,
        ANVERS,
        SPOTLIGHT_RIGHT,
        SPOTLIGHT_LEFT,
        CAMELA_FLASH,
        CAMELA_MAN,
        CAMELA_MAN_EFFECT,
        ARROW,
        CONFETTI,
        WHITE_OUT,
    };
    enum Z_ORDER{
        Z_BACKGOUND = 0,
        Z_CAR,
        Z_ANVERS,
        Z_SPOTLIGHT_RIGHT,
        Z_SPOTLIGHT_LEFT,
        Z_CAMELA_FLASH,
        Z_CAMELA_MAN,
        Z_CAMELA_MAN_EFFECT,
        Z_ARROW,
        Z_CONFETTI,
        Z_WHITE_OUT,
    };
    enum TAG_ANIMATION{
        ARROW_JUMP = 0,
    };
    enum CAMERA_FLASH{
        LEFT,
        RIGHT,
    };
    const int LIGHT_OPACITY = 127;
    const std::string ANVERS_ANIMATION = "animation_cloth";
    const float DIMENSION_HEIHT = 150.0f;
    const float TURN_ON_RIGHT_LIGHT_DELAY_TIME = 0.03f;
    const float TURN_ON_LEFT_LIGHT_DELAY_TIME = 0.2f;
    const float VIEW_ARROW_DELAY_TIME = 0.2f;
    const float ARROW_JUMP_DELAY_TIME = 0.5f;
    const float CAMELA_FLASH_DELAY = 0.1f;
    const float EXCLAMATION_FADE_IN_TIME = 0.2f;
    const float EXCLAMATION_MAX_SCALE = 1.2f;
    const float EXCLAMATION_MAX_SCALE_TIME = 0.1f;
    const float EXCLAMATION_DEFAULT_SCALE = 1.0f;
    const float EXCLAMATION_DEFAULT_SCALE_TIME = 0.2f;
    const float WHITE_OUT_START_DELAY_TIME = 0.8f;
    const float WHITE_OUT_FADE_IN_TIME = 0.5f;
    const float NEXT_SCENE_DELAY_TIME = 0.7f;
    
    int _soundId;
    bool _isEndTouchAnimation;
    std::shared_ptr<GachaEffectModel> _gachaEffectModel;
    OnEndEffectCallback _callback;
    
    void showBackground();
    void showCar(const std::string filePath);
    void showAnvers(const std::string filePath);
    void showCameraMan(const std::string filePath);
    void callGachaVoice();
    
    void startAnimation(const std::shared_ptr<GachaEffectModel>& effectModel);
    void showRightOfSpotLight();
    void showLeftOfSpotLight();
    void showArrow();
    
    void setTouchDetect();
    bool onTouchBegan(cocos2d::Touch* touch,cocos2d::Event* event);
    void onTouchMoved(Touch *touch, Event *unused_event);
    void onTouchEnded(Touch *touch, Event *unused_event);
    
    void endAnimation();
    void showConfetti();
    void startCameraFlashAnimation();
    void showCameraFlash(const CAMERA_FLASH type);
    void startCameramanEffectAnimation();
    void showCameramanEffect(const Vec2 position, const float startAnimationDelay);
    
    void endEffectCallback();
};

#endif