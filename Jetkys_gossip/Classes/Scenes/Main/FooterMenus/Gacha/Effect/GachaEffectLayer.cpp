#include "GachaEffectLayer.h"
#include "SoundHelper.h"
#include "PlayerController.h"

GachaEffectLayer::GachaEffectLayer():
    _gachaEffectModel(nullptr), _callback(nullptr), _soundId(0), _isEndTouchAnimation(false)
{}

GachaEffectLayer::~GachaEffectLayer()
{
    AnimationCache::getInstance()->removeAnimation(ANVERS_ANIMATION);
    Director::getInstance()->getTextureCache()->removeTextureForKey("gacha_background.png");
    Director::getInstance()->getTextureCache()->removeTextureForKey("gacha_spotlight_1.png");
    Director::getInstance()->getTextureCache()->removeTextureForKey("gacha_spotlight_2.png");
    Director::getInstance()->getTextureCache()->removeTextureForKey("gacha_blackcar.png");
    Director::getInstance()->getTextureCache()->removeTextureForKey("gacha_goldcar.png");
    Director::getInstance()->getTextureCache()->removeTextureForKey("gacha_blight_1.png");
    Director::getInstance()->getTextureCache()->removeTextureForKey("gacha_blight_2.png");
    Director::getInstance()->getTextureCache()->removeTextureForKey("gacha_people_1136.png");
    Director::getInstance()->getTextureCache()->removeTextureForKey("gacha_people.png");
    Director::getInstance()->getTextureCache()->removeTextureForKey("gacha_arrow.png");
    Director::getInstance()->getTextureCache()->removeTextureForKey("gacha_pull.png");
    std::string viewfilePath = _gachaEffectModel->getCarFilePath();
    for (int i = 1; i <= 5; i++) {
        std::string filePath;
        if (viewfilePath == "gacha_goldcloth_1.png") {
            filePath = StringUtils::format("gacha_goldcloth_%d.png", i);
        } else if (viewfilePath == "gacha_normalcloth_1.png") {
            filePath =  StringUtils::format("gacha_normalcloth_%d.png", i);
        }
        Director::getInstance()->getTextureCache()->removeTextureForKey(filePath);
    }
    _gachaEffectModel = nullptr;
}

bool GachaEffectLayer::init(const std::shared_ptr<GachaEffectModel>& model, const OnEndEffectCallback& callback)
{
    if (!Layer::create()) {
        return false;
    }
    setTouchDetect();
    _callback = callback;
    _gachaEffectModel = model;

    showBackground();
    showCar(model->getCarFilePath());
    showAnvers(model->getAnversFilePath());
    showCameraMan(model->getCameramanFilePath());

    callGachaVoice();

    startAnimation(model);

    return true;
}

void GachaEffectLayer::showBackground()
{
    auto background = Sprite::create("gacha_background.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    Size winSize = Director::getInstance()->getWinSize();
    background->setPosition(Vec2(winSize.width / 2, winSize.height - background->getContentSize().height / 2));
    addChild(background, Z_ORDER::Z_BACKGOUND);
}

void GachaEffectLayer::showCar(const std::string filePath)
{
    auto car = Sprite::create(filePath.c_str());
    car->setTag(TAG_SPRITE::CAR);
    car->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition());
    addChild(car, Z_ORDER::Z_CAR);
}

void GachaEffectLayer::showAnvers(const std::string filePath)
{
    auto anvers = Sprite::create(filePath.c_str());
    anvers->setTag(TAG_SPRITE::ANVERS);
    anvers->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition());
    addChild(anvers, Z_ORDER::Z_ANVERS);
}

void GachaEffectLayer::showCameraMan(const std::string filePath)
{
    auto cameraman = Sprite::create(filePath.c_str());
    cameraman->setTag(TAG_SPRITE::CAMELA_MAN);
    cameraman->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    addChild(cameraman, Z_ORDER::Z_CAMELA_MAN);
}

void GachaEffectLayer::callGachaVoice()
{
    runAction(Sequence::create(DelayTime::create(0.5), CallFunc::create([]() {
        SoundHelper::shareHelper()->playCharacterVoiceEfect(PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]->getCharactersId(), 13);
    }), NULL));
}

void GachaEffectLayer::startAnimation(const std::shared_ptr<GachaEffectModel>& effectModel)
{
    AnimationCache::getInstance()->addAnimation(effectModel->getAnversAnimation(), ANVERS_ANIMATION);
    runAction(Sequence::create(DelayTime::create(TURN_ON_RIGHT_LIGHT_DELAY_TIME),
                               CallFunc::create([this]() {
        SOUND_HELPER->playeMainSceneEffect(SOUND_GACHA_1, false);
        showRightOfSpotLight();
    }),
                               DelayTime::create(TURN_ON_LEFT_LIGHT_DELAY_TIME),
                               CallFunc::create([this]() {
        SOUND_HELPER->playeMainSceneEffect(SOUND_GACHA_1, false);
        showLeftOfSpotLight();
    }),
                               DelayTime::create(VIEW_ARROW_DELAY_TIME),
                               CallFunc::create([this]() {
        showArrow();
        _soundId = SOUND_HELPER->playeMainSceneEffect(SOUND_GACHA_2, true);
    }), NULL));
}

void GachaEffectLayer::showRightOfSpotLight()
{
    auto light = Sprite::create("gacha_spotlight_2.png");
    light->setOpacity(LIGHT_OPACITY);
    light->setTag(TAG_SPRITE::SPOTLIGHT_RIGHT);
    light->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition());
    addChild(light, Z_ORDER::Z_SPOTLIGHT_RIGHT);
}

void GachaEffectLayer::showLeftOfSpotLight()
{
    auto light = Sprite::create("gacha_spotlight_1.png");
    light->setOpacity(LIGHT_OPACITY);
    light->setTag(TAG_SPRITE::SPOTLIGHT_LEFT);
    light->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition());
    addChild(light, Z_ORDER::Z_SPOTLIGHT_LEFT);
}

void GachaEffectLayer::showArrow()
{
    auto arrow = Sprite::create("gacha_arrow.png");
    arrow->setTag(TAG_SPRITE::ARROW);
    arrow->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition());
    auto jumpAnimation = RepeatForever::create(JumpBy::create(ARROW_JUMP_DELAY_TIME, Vec2(0, 0), 30, 1));
    jumpAnimation->setTag(TAG_ANIMATION::ARROW_JUMP);
    arrow->runAction(jumpAnimation);
    addChild(arrow, Z_ORDER::Z_ARROW);

    auto pullUpSprite = Sprite::create("gacha_pull.png");
    pullUpSprite->setPosition(Vec2(arrow->getContentSize() / 2));
    arrow->addChild(pullUpSprite);
}

void GachaEffectLayer::setTouchDetect()
{
    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GachaEffectLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GachaEffectLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GachaEffectLayer::onTouchEnded, this);
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

bool GachaEffectLayer::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (getChildByTag(TAG_SPRITE::ARROW) && _isEndTouchAnimation == true) {
        getChildByTag(TAG_SPRITE::ARROW)->stopActionByTag(TAG_ANIMATION::ARROW_JUMP);
    }
    return true;
}

void GachaEffectLayer::onTouchMoved(Touch* touch, Event* unused_event)
{
    auto arrow = getChildByTag(TAG_SPRITE::ARROW);
    if (!arrow || _isEndTouchAnimation == true) {
        return;
    }
    arrow->setPositionY(arrow->getPositionY() + (touch->getLocation() - touch->getPreviousLocation()).y);
    if (arrow->getPositionY() < getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition().y) {
        arrow->setPositionY(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition().y);
    } else if (arrow->getPositionY() >= getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition().y + DIMENSION_HEIHT) {
        endAnimation();
    }
}

void GachaEffectLayer::onTouchEnded(Touch* touch, Event* unused_event)
{
    auto arrow = getChildByTag(TAG_SPRITE::ARROW);
    if (!arrow || _isEndTouchAnimation == true) {
        return;
    }
    if (arrow->getPositionY() > getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition().y && arrow->getPositionY() < getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition().y + DIMENSION_HEIHT) {
        float kankaku = arrow->getPositionY() - getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition().y;
        float duration = kankaku / DIMENSION_HEIHT * 0.2f;
        arrow->runAction(Sequence::create(MoveBy::create(duration, Vec2(0, -kankaku)),
                                          CallFunc::create([this]() {
            getChildByTag(TAG_SPRITE::ARROW)->runAction(RepeatForever::create(JumpBy::create(ARROW_JUMP_DELAY_TIME, Vec2(0, 0), 30, 1)));
        })
                                          , NULL));
    } else if (arrow->getPositionY() == getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition().y) {
        arrow->runAction(RepeatForever::create(JumpBy::create(ARROW_JUMP_DELAY_TIME, Vec2(0, 0), 30, 1)));
    }
}

void GachaEffectLayer::endAnimation()
{
    _isEndTouchAnimation = true;

    showConfetti();
    getChildByTag(TAG_SPRITE::ARROW)->runAction(MoveBy::create(0.2f, Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition().y + DIMENSION_HEIHT + 10)));
    getChildByTag(TAG_SPRITE::ANVERS)->runAction(Animate::create(AnimationCache::getInstance()->getAnimation(ANVERS_ANIMATION)));
    startCameraFlashAnimation();

    if (_gachaEffectModel->getRarity() >= 5) {
        startCameramanEffectAnimation();
    }

    Size winSize = Director::getInstance()->getWinSize();
    auto whiteoutLayer = LayerColor::create(Color4B(Color3B::WHITE), winSize.width, winSize.height);
    whiteoutLayer->setTag(TAG_SPRITE::WHITE_OUT);
    whiteoutLayer->setPosition(Point::ZERO);
    whiteoutLayer->setOpacity(0);
    whiteoutLayer->runAction(Sequence::create(DelayTime::create(WHITE_OUT_START_DELAY_TIME), FadeIn::create(WHITE_OUT_FADE_IN_TIME), DelayTime::create(NEXT_SCENE_DELAY_TIME), CallFunc::create(CC_CALLBACK_0(GachaEffectLayer::endEffectCallback, this)), NULL));
    addChild(whiteoutLayer, Z_ORDER::Z_WHITE_OUT);
    SOUND_HELPER->stopEffect(_soundId);
    SOUND_HELPER->playeMainSceneEffect(SOUND_GACHA_3, false);
}

void GachaEffectLayer::showConfetti()
{
    ParticleSystemQuad* particle = ParticleSystemQuad::create("confetti.plist");
    particle->setTag(TAG_SPRITE::CONFETTI);
    Size winSize = Director::getInstance()->getWinSize();
    particle->setPosition(Vec2(winSize.width / 2, winSize.height));
    particle->setDuration(WHITE_OUT_START_DELAY_TIME + WHITE_OUT_FADE_IN_TIME + NEXT_SCENE_DELAY_TIME);
    particle->setAutoRemoveOnFinish(true);
    addChild(particle, Z_ORDER::Z_CONFETTI);
}

void GachaEffectLayer::startCameraFlashAnimation()
{
    runAction(RepeatForever::create(Sequence::create(DelayTime::create(CAMELA_FLASH_DELAY),
                                                     CallFunc::create([this]() {showCameraFlash(CAMERA_FLASH::LEFT); }),
                                                     DelayTime::create(CAMELA_FLASH_DELAY),
                                                     CallFunc::create([this]() {showCameraFlash(CAMERA_FLASH::RIGHT); }), NULL)));
}

void GachaEffectLayer::showCameraFlash(const CAMERA_FLASH type)
{
    if (getChildByTag(TAG_SPRITE::CAMELA_FLASH)) {
        removeChildByTag(TAG_SPRITE::CAMELA_FLASH);
    }
    std::string filePath;
    if (type == LEFT) {
        filePath = "gacha_blight_1.png";
    } else if (type == RIGHT) {
        filePath = "gacha_blight_2.png";
    }
    auto flash = Sprite::create(filePath.c_str());
    flash->setTag(TAG_SPRITE::CAMELA_FLASH);
    flash->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition());
    addChild(flash, Z_ORDER::Z_CAMELA_FLASH);
}

void GachaEffectLayer::startCameramanEffectAnimation()
{
    auto cameraman = getChildByTag(TAG_SPRITE::CAMELA_MAN);
    showCameramanEffect(cameraman->getPosition() - (cameraman->getContentSize() / 2) + Point(282, 336), 0.1f);
    showCameramanEffect(cameraman->getPosition() - (cameraman->getContentSize() / 2) + Point(146, 382), 0.2f);
    showCameramanEffect(cameraman->getPosition() - (cameraman->getContentSize() / 2) + Point(464, 502), 0.3f);
    showCameramanEffect(cameraman->getPosition() - (cameraman->getContentSize() / 2) + Point(86, 486), 0.4f);
    showCameramanEffect(cameraman->getPosition() - (cameraman->getContentSize() / 2) + Point(586, 634), 0.5f);
}

void GachaEffectLayer::showCameramanEffect(const Vec2 position, const float startAnimationDelay)
{
    auto exclamation = Sprite::create("Exclamation.png");
    exclamation->setTag(TAG_SPRITE::CAMELA_MAN_EFFECT);
    exclamation->setPosition(position);
    exclamation->setOpacity(0);
    exclamation->runAction(Sequence::create(DelayTime::create(startAnimationDelay), FadeIn::create(EXCLAMATION_FADE_IN_TIME), ScaleTo::create(EXCLAMATION_MAX_SCALE_TIME, EXCLAMATION_MAX_SCALE, EXCLAMATION_MAX_SCALE), ScaleTo::create(EXCLAMATION_DEFAULT_SCALE_TIME, EXCLAMATION_DEFAULT_SCALE, EXCLAMATION_DEFAULT_SCALE), NULL));
    addChild(exclamation, Z_ORDER::Z_CAMELA_MAN_EFFECT);
}

void GachaEffectLayer::endEffectCallback()
{
    _callback();
    removeFromParent();
}
