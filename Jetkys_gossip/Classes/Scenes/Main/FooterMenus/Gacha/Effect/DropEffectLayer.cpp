#include "DropEffectLayer.h"
#include "CharacterModel.h"
#include "PartModel.h"
#include "FontDefines.h"
#include "SoundHelper.h"
#include "TelopLayer.h"
#include "editor-support/spine/Json.h"
#include "jCommon.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

DropEffectLayer::DropEffectLayer():
    _callback(nullptr)
    , _listener(nullptr)
    , _viewContentId(0)
{}

DropEffectLayer::~DropEffectLayer()
{
    if (_viewContentId > 0) {
        if (_getType == DROP_CHARACTER) {
            SpriteFrameCache::getInstance()->removeSpriteFramesFromFile(StringUtils::format("%d_ca.plist", _viewContentId));
        } else if (_getType == DROP_PART) {
            SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("icon_pab.plist");
        }
    }
}

bool DropEffectLayer::init(const GET_TYPE getType, const int viewContentId, const bool isEndBlackOutEffect, const OnEndEffectCallback& callback)
{
    if (!Layer::create()) {
        return false;
    }

    int rarity;
    std::string telopString;
    if (getType == DROP_CHARACTER) {
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(viewContentId));
        rarity = characterModel->getRarity();
        telopString = characterModel->getCarName();
        telopString += "と出会いました";
    } else if (getType == DROP_PART) {
        std::shared_ptr<PartModel>partsModel(PartModel::find(viewContentId));
        rarity = partsModel->getRarity();
        telopString = partsModel->getName();
        telopString += "を手に入れました";
    }

    _callback = callback;
    _getType = getType;
    _viewContentId = viewContentId;

    showWhiteLayer();
    showConfetti();
    showBackground();
    showGetContent(getType, viewContentId);
    showTelopMessage(telopString);
    showRarityStar(rarity);

    setTouchDetect(CC_CALLBACK_0(DropEffectLayer::onTouchRarityStarEnded, this, getType, isEndBlackOutEffect));

    runAction(Sequence::create(DelayTime::create(1.0), CallFunc::create(CC_CALLBACK_0(DropEffectLayer::playDropSE, this)), NULL));
    return true;
}

void DropEffectLayer::showWhiteLayer()
{
    auto winSize = Director::getInstance()->getWinSize();
    auto whiteLayer = LayerColor::create(Color4B::WHITE, winSize.width, winSize.height);
    whiteLayer->setTag(TAG_SPRITE::WHITE_IN_LAYER);
    whiteLayer->setPosition(Vec2::ZERO);
    addChild(whiteLayer, Z_ORDER::Z_WHITE_IN_LAYER);
    whiteLayer->runAction(Sequence::create(DelayTime::create(1.0f), FadeTo::create(1.0f, 0), NULL));
}

void DropEffectLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getWinSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void DropEffectLayer::showGetContent(const GET_TYPE type, const int Id)
{
    std::string filePath;
    if (type == DROP_CHARACTER) {
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_ca.plist", Id));
        filePath = StringUtils::format("%d_t.png", Id);
    } else if (type == DROP_PART) {
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon_pab.plist");
        filePath = StringUtils::format("%d_pab.png", Id);
    }
    auto character = makeSprite(filePath.c_str());
    character->setTag(TAG_SPRITE::GET_CONTENT);
    auto winSize = Director::getInstance()->getWinSize();
    character->setPosition(winSize / 2);
    float maxScale = winSize.width / character->getContentSize().width;
    if ((winSize.height / maxScale * character->getContentSize().height) < 1) {
        maxScale =  winSize.height / character->getContentSize().height;
    }
    character->setScale(maxScale);
    addChild(character, Z_ORDER::Z_GET_CONTENT);
}

void DropEffectLayer::showConfetti()
{
    ParticleSystemQuad* particle = ParticleSystemQuad::create("confetti.plist");
    particle->setTag(TAG_SPRITE::CONFETTI);
    Size winSize = Director::getInstance()->getWinSize();
    particle->setPosition(Vec2(winSize.width / 2, winSize.height));
    particle->setDuration(10);
    particle->setAutoRemoveOnFinish(true);
    addChild(particle, Z_ORDER::Z_CONFETTI);
}

void DropEffectLayer::showRarityStar(const int rarity)
{
    auto rarityStarLayer = Layer::create();
    rarityStarLayer->setTag(TAG_SPRITE::RARITY);
    auto character = getChildByTag(TAG_SPRITE::GET_CONTENT);
    switch (rarity) {
    case 1: {
        rarityStarLayer->addChild(createrRarityStar(1, 0.0f, character->getPosition() + Point(0, 0)));
        break;
    }
    case 2: {
        rarityStarLayer->addChild(createrRarityStar(1, 0.0f, character->getPosition() + Point(-66.5 - spase / 2, 0)));
        rarityStarLayer->addChild(createrRarityStar(2, nextTime, character->getPosition() + Point(66.5 + spase / 2, 0)));
        break;
    }
    case 3: {
        rarityStarLayer->addChild(createrRarityStar(1, 0.0f, character->getPosition() + Point(-135 - spase, 0)));
        rarityStarLayer->addChild(createrRarityStar(2, nextTime, character->getPosition() + Point(0, 0)));
        rarityStarLayer->addChild(createrRarityStar(3, nextTime * 2, character->getPosition() + Point(135 + spase, 0)));
        break;
    }
    case 4: {
        rarityStarLayer->addChild(createrRarityStar(1, 0.0f, character->getPosition() + Point(-66.5 - spase / 2, 66.5 + spase / 2)));
        rarityStarLayer->addChild(createrRarityStar(2, nextTime, character->getPosition() + Point(66.5 + spase / 2, 66.5 + spase / 2)));
        rarityStarLayer->addChild(createrRarityStar(3, nextTime * 2, character->getPosition() + Point(-66.5 - spase / 2, -66.5 - spase / 2)));
        rarityStarLayer->addChild(createrRarityStar(4, nextTime * 3, character->getPosition() + Point(66.5 + spase / 2, -66.5 - spase / 2)));
        break;
    }
    case 5: {
        rarityStarLayer->addChild(createrRarityStar(1, 0.0f, character->getPosition() + Point(-133 - spase, 66.5 + spase / 2)));
        rarityStarLayer->addChild(createrRarityStar(2, nextTime, character->getPosition() + Point(0, 66.5 + spase / 2)));
        rarityStarLayer->addChild(createrRarityStar(3, nextTime * 2, character->getPosition() + Point(133 + spase, 66.5 + spase / 2)));
        rarityStarLayer->addChild(createrRarityStar(4, nextTime * 3, character->getPosition() + Point(-66.5 - spase / 2, -66.5 - spase / 2)));
        rarityStarLayer->addChild(createrRarityStar(5, nextTime * 4, character->getPosition() + Point(66.5 + spase / 2, -66.5 - spase / 2)));
        break;
    }
    default:
        break;
    }
    addChild(rarityStarLayer, Z_ORDER::Z_RARITY);
}

Sprite* DropEffectLayer::createrRarityStar(const int Tag, const float effectStartDilayTime, const Vec2 position)
{
    auto result = Sprite::create("bigStar.png");
    setTag(Tag);
    result->setPosition(position);
    result->setScale(10);
    result->setOpacity(0);
    result->runAction(Spawn::create(Sequence::create(DelayTime::create(efectTime - scaleTotime + effectStartDilayTime), ScaleTo::create(scaleTotime, 1.0f, 1.0f), NULL),
                                    Sequence::create(DelayTime::create(efectTime - rotateTime + effectStartDilayTime), RotateTo::create(rotateTime, 360 * rotateCount), NULL),
                                    Sequence::create(DelayTime::create(efectTime - fadeInTime + effectStartDilayTime), FadeIn::create(fadeInTime), NULL),
                                    Sequence::create(DelayTime::create(efectTime + effectStartDilayTime), CallFunc::create([this]() {
        if (getChildByTag(TAG_SPRITE::RARITY)) {
            SOUND_HELPER->playEffect(SKILL_CHARGE_END_EFFECT, false);
        }
    }), NULL), NULL));
    return result;
}

void DropEffectLayer::showTelopMessage(const std::string telopString)
{
    auto string = telopString;
    auto telopLayer = TelopLayer::createWithmessage(string);
    telopLayer->setTag(TAG_SPRITE::TELOP_MESSAGE);
    addChild(telopLayer, Z_ORDER::Z_TELOP_MESSAGE);
}

void DropEffectLayer::playDropSE()
{
    int rarity = -1;
    if (_getType == DROP_CHARACTER) {
        std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(_viewContentId));
        rarity = characterModel->getRarity();
    } else if (_getType == DROP_PART) {
        std::shared_ptr<PartModel>partsModel(PartModel::find(_viewContentId));
        rarity = partsModel->getRarity();
    }
    if (rarity <= 3) {
        SOUND_HELPER->playEffect(DROP_NOMAL_EFFECT, false);
    } else if (rarity == 4) {
        SOUND_HELPER->playEffect(DROP_GOOD_EFFECT, false);
    } else if (rarity == 5) {
        SOUND_HELPER->playEffect(DROP_EXCELLENT_EFFECT, false);
    }
}

void DropEffectLayer::setTouchDetect(const cocos2d::EventListenerTouchOneByOne::ccTouchCallback& callback)
{
    _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchEnded = callback;
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);
}

void DropEffectLayer::onTouchRarityStarEnded(const GET_TYPE type, const bool isEndBlackOutEffect)
{
    getEventDispatcher()->removeEventListener(_listener);
    removeRarityStar();

    if (type == DROP_CHARACTER) {
        setTouchDetect(CC_CALLBACK_0(DropEffectLayer::onTouchVoiceWordEnded, this, isEndBlackOutEffect));
        showCharacterWord(_viewContentId);
    } else {
        showBlackOuteffect(isEndBlackOutEffect);
    }
}

void DropEffectLayer::onTouchVoiceWordEnded(const bool isEndBlackOutEffect)
{
    getEventDispatcher()->removeEventListener(_listener);
    showBlackOuteffect(isEndBlackOutEffect);
}

void DropEffectLayer::removeRarityStar()
{
    auto rarityStarlayer = getChildByTag(TAG_SPRITE::RARITY);
    if (!rarityStarlayer) {
        return;
    }
    for (int i = 1; i <= 5; i++) {
        auto rarityStar = rarityStarlayer->getChildByTag(i);
        if (rarityStar) {
            rarityStar->runAction(CCFadeOut::create(EFFECT_FADE_OUT_TIME));
        }
    }
    rarityStarlayer->runAction(MoveBy::create(EFFECT_MOVE_TIME, Vec2(0, -EFFECT_MOVE_DELTA)));
    rarityStarlayer->setVisible(false);
}

void DropEffectLayer::showCharacterWord(const int masterCharacterId)
{
    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(masterCharacterId));

    runAction(Sequence::create(DelayTime::create(0.8), CallFunc::create([masterCharacterId]() {
        SOUND_HELPER->playCharacterVoiceEfect(masterCharacterId, 14);
    }), NULL));

    auto commentBase = Sprite::create("gacha_comment_base.png");
    commentBase->setTag(TAG_SPRITE::CHARACTER_WORD);
    Size winSize = Director::getInstance()->getWinSize();
    commentBase->setPosition(Point(winSize.width / 2, winSize.height / 2 - EFFECT_MOVE_DELTA));
    commentBase->setOpacity(0);
    commentBase->runAction(Spawn::createWithTwoActions(MoveBy::create(EFFECT_MOVE_TIME, Vec2(0, EFFECT_MOVE_DELTA)),
                                                       FadeIn::create(EFFECT_FADE_IN_TIME)));
    auto mainTextScrollView = ScrollView::create();
    mainTextScrollView->setViewSize(Size(commentBase->getContentSize().width - 26, 130));
    mainTextScrollView->setDirection(ScrollView::Direction::VERTICAL);
    mainTextScrollView->setAnchorPoint(Point(0, 0));
    mainTextScrollView->setPosition(Point(0, 8));

    auto mainTextLabel = Label::createWithTTF(removeSpaces(characterModel->getGachaText()), FONT_NAME_4, 24);
    mainTextLabel->setDimensions(mainTextScrollView->getViewSize().width - 26,
                                 0);
    mainTextLabel->setHorizontalAlignment(TextHAlignment::LEFT);
    mainTextScrollView->setContainer(mainTextLabel);
    mainTextScrollView->setBounceable(false);
    mainTextScrollView->setContentOffset(Point(0 - (mainTextLabel->getContentSize().width - mainTextScrollView->getViewSize().width),
                                               0 - (mainTextLabel->getContentSize().height - mainTextScrollView->getViewSize().height)));
    mainTextScrollView->setBounceable(false);
    commentBase->addChild(mainTextScrollView);
    addChild(commentBase, Z_ORDER::Z_CHARACTER_WORD);
}

void DropEffectLayer::showBlackOuteffect(const bool isEndBlackOutEffect)
{
    if (isEndBlackOutEffect == true) {
        if (getChildByTag(TAG_SPRITE::BLACK_OUT_LAYER)) {
            return;
        }
        auto winSize = Director::getInstance()->getWinSize();
        auto blackLayer = LayerColor::create(Color4B::BLACK, winSize.width, winSize.height);
        blackLayer->setTag(TAG_SPRITE::BLACK_OUT_LAYER);
        blackLayer->setOpacity(0);
        blackLayer->runAction(Sequence::create(FadeIn::create(BLACK_OUT_TIME), CallFunc::create(CC_CALLBACK_0(DropEffectLayer::endEffectCallback, this)), NULL));
        addChild(blackLayer, Z_ORDER::Z_BLACK_OUT_LAYER);
    } else {
        endEffectCallback();
    }
}

void DropEffectLayer::endEffectCallback()
{
    SOUND_HELPER->stopVoiceEfect();
    _callback();
    removeFromParent();
}