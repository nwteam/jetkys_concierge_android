#ifndef __syanago__SelectGachaDialog__
#define __syanago__SelectGachaDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "GachaModel.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class SelectGachaDialog : public Layer, public create_func<SelectGachaDialog>
{
public:
    typedef std::function<void(const GACHA_TYPE type, const int tryCont)> OnResponceCallback;
    using create_func::create;
    bool init(const GACHA_TYPE type, const OnResponceCallback& callback);
    
    SelectGachaDialog();
    ~SelectGachaDialog();
    
private:
    enum TAG_SPRITE{
        BLACK_LAYER = 0,
        BACKGROUND,
        CLOSE_BUTTON
    };
    enum Z_ORDER{
        Z_BLACK_LAYER = 0,
        Z_BACKGROUND,
        Z_BUTTON,
    };
    OnResponceCallback _callBack;
    GACHA_TYPE _type;
    
    void showBackground();
    void showMessage(const GACHA_TYPE type, const int maxTryCount);
    void showButton(const GACHA_TYPE type, const int maxTryCount);
    
    void onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type);
    void onTouchOnceButton(Ref* sender, ui::Widget::TouchEventType type);
    void onTouchTryButton(Ref* sender, ui::Widget::TouchEventType type);
    
    const int tryGachaCount(const GACHA_TYPE type);
};

#endif
