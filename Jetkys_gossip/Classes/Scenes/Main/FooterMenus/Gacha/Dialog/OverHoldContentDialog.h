#ifndef __syanago__OverHoldContentDialog__
#define __syanago__OverHoldContentDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class OverHoldContentDialog : public Layer, public create_func<OverHoldContentDialog>
{
public:
    typedef std::function<void()> OnGoCompositionCallback;
    typedef std::function<void()> OnGoSellCallback;
    typedef std::function<void()> OnGoExtensionCallback;
    using create_func::create;
    bool init(const std::string title, const OnGoCompositionCallback& goCompositionCallback, const OnGoSellCallback& goSellCallback, const OnGoExtensionCallback& goExtensionCallback);
    
    OverHoldContentDialog();
    ~OverHoldContentDialog();
    
private:
    enum TAG_SPRITE{
        BLACK_LAYER = 0,
        BACKGROUND,
        TITLE,
        MESSAGE,
        BUTTON,
    };
    enum Z_ORDER{
        Z_BLACK_LAYER = 0,
        Z_BACKGROUND,
        Z_TITLE,
        Z_MESSAGE,
        Z_BUTTON,
    };
    OnGoCompositionCallback _goCompositionCallback;
    OnGoSellCallback _goSellCallback;
    OnGoExtensionCallback _goExtensionCallback;
    
    void showBackground();
    void showTitle(const std::string title);
    void showMessage();
    void showButton();
    ui::Button* createButton(const std::string buttonString);
    
    void compositionButtonCallBack(Ref* sender, ui::Widget::TouchEventType type);
    void sellButtonCallBack(Ref* sender, ui::Widget::TouchEventType type);
    void extensionButtonCallBack(Ref* sender, ui::Widget::TouchEventType type);
    
};

#endif
