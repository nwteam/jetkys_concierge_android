#include "NotEnoughForTokenDialog.h"
#include "FontDefines.h"
#include "SoundHelper.h"

NotEnoughForTokenDialog::NotEnoughForTokenDialog()
{}

NotEnoughForTokenDialog::~NotEnoughForTokenDialog()
{}

bool NotEnoughForTokenDialog::init(const OnGoToTokenShopCallback& goToTokenShopCallback)
{
    if (!Layer::create()) {
        return false;
    }
    _goToTokenShopCallback = goToTokenShopCallback;

    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    showBackground();
    showTitle();
    showMessage();
    showButton();

    return true;
}


void NotEnoughForTokenDialog::showBackground()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto blackLayer = LayerColor::create(Color4B(Color3B::BLACK), winSize.width, winSize.height);
    blackLayer->setOpacity(128);
    blackLayer->setTag(TAG_SPRITE::BLACK_LAYER);
    addChild(blackLayer);

    auto background = Sprite::create("dialog_small_base.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Vec2(winSize / 2));
    addChild(background);
}

void NotEnoughForTokenDialog::showTitle()
{
    auto label = Label::createWithTTF("トークンが足りません", FONT_NAME_2, 32);
    label->setTag(TAG_SPRITE::TITLE);
    label->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 25));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setColor(Color3B::YELLOW);
    label->setAnchorPoint(Vec2(0.5, 1.0f));
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(label, Z_ORDER::Z_TITLE);
}

void NotEnoughForTokenDialog::showMessage()
{
    auto label = Label::createWithTTF("トークンを購入しますか？", FONT_NAME_2, 25);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    float messageWidth = getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width - 70;
    if (label->getContentSize().width > messageWidth) {
        label->setDimensions(messageWidth, 0);
    }
    label->setTag(TAG_SPRITE::MESSAGE);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize() / 2) + Vec2(0, label->getContentSize().height / 2));
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(label, Z_ORDER::Z_MESSAGE);
}

void NotEnoughForTokenDialog::showButton()
{
    auto yesButton = ui::Button::create("gacha_dialog_button.png");
    yesButton->addTouchEventListener(CC_CALLBACK_2(NotEnoughForTokenDialog::onTouchYesButton, this));
    yesButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    yesButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 - 15, 25));
    yesButton->setTag(TAG_SPRITE::BUTTON);
    auto yesLabel = Label::createWithTTF("はい", FONT_NAME_2, 28);
    yesLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    yesLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    yesLabel->setPosition(Vec2(yesButton->getContentSize() / 2) - Vec2(0, 14));
    yesButton->addChild(yesLabel);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(yesButton, Z_ORDER::Z_BUTTON);

    auto noButton = ui::Button::create("gacha_dialog_button.png");
    noButton->addTouchEventListener(CC_CALLBACK_2(NotEnoughForTokenDialog::onTouchNoButton, this));
    noButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    noButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 + 15, 25));
    noButton->setTag(TAG_SPRITE::BUTTON);
    auto noLabel = Label::createWithTTF("いいえ", FONT_NAME_2, 28);
    noLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    noLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    noLabel->setPosition(Vec2(noButton->getContentSize() / 2) - Vec2(0, 14));
    noButton->addChild(noLabel);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(noButton, Z_ORDER::Z_BUTTON);
}

void NotEnoughForTokenDialog::onTouchYesButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _goToTokenShopCallback();
        removeFromParent();
    }
}

void NotEnoughForTokenDialog::onTouchNoButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        removeFromParent();
    }
}