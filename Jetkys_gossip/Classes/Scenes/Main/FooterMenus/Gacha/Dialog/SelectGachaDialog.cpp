#include "SelectGachaDialog.h"
#include "PlayerController.h"
#include "SystemSettingModel.h"
#include "FontDefines.h"
#include "jCommon.h"
#include "SoundHelper.h"

SelectGachaDialog::SelectGachaDialog():
    _callBack(nullptr)
{}

SelectGachaDialog::~SelectGachaDialog()
{}

bool SelectGachaDialog::init(const GACHA_TYPE type, const OnResponceCallback& callback)
{
    if (!Layer::create()) {
        return false;
    }
    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    _callBack = callback;
    _type = type;

    showBackground();
    showMessage(type, tryGachaCount(type));
    showButton(type, tryGachaCount(type));

    return true;
}

void SelectGachaDialog::showBackground()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto blackLayer = LayerColor::create(Color4B(Color3B::BLACK), winSize.width, winSize.height);
    blackLayer->setOpacity(128);
    blackLayer->setTag(TAG_SPRITE::BLACK_LAYER);
    addChild(blackLayer, Z_ORDER::Z_BLACK_LAYER);

    auto background = Sprite::create("dialog_base.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Vec2(winSize / 2));
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

const int SelectGachaDialog::tryGachaCount(const GACHA_TYPE type)
{
    if (type == PREMIUM) {
        return 10;
    }
    std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
    int friendPoint = PLAYERCONTROLLER->_player->getFriendPoint();
    int result = floor(friendPoint / systemSettingModel->getFpForFreeGacha());
    if (result == 0) {
        return 1;
    }
    if (result > 10) {
        return 10;
    }
    return result;
}


void SelectGachaDialog::showMessage(const GACHA_TYPE type, const int maxTryCount)
{
    if (type == PREMIUM) {
        int holdToken = PLAYERCONTROLLER->_player->getFreeToken() + PLAYERCONTROLLER->_player->getPayToken();
        std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
        int oneTry = systemSettingModel->getTokenForPayGacha();

        auto titleLabel = Label::createWithTTF("プレミアムガチャ", FONT_NAME_2, 34);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(titleLabel, 1);
        titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
        titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        titleLabel->setPosition(Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2,
                                      getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height - 30 - 34));


        auto text1 = Label::createWithTTF("1回", FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(text1, 1);
        text1->setHorizontalAlignment(TextHAlignment::CENTER);
        text1->setColor(COLOR_YELLOW);
        text1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        text1->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

        auto tokenImage1 = makeSprite("token_small.png");
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(tokenImage1, 1);

        auto tokenLabel1 = Label::createWithTTF(StringUtils::format("%d", oneTry), FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(tokenLabel1, 1);
        tokenLabel1->setHorizontalAlignment(TextHAlignment::CENTER);
        tokenLabel1->setColor(COLOR_YELLOW);
        tokenLabel1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        tokenLabel1->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);


        auto text2 = Label::createWithTTF("10連続", FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(text2, 1);
        text2->setHorizontalAlignment(TextHAlignment::CENTER);
        text2->setColor(COLOR_YELLOW);
        text2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        text2->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

        auto tokenImage2 = makeSprite("token_small.png");
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(tokenImage2, 1);

        auto tokenLabel2 = Label::createWithTTF(StringUtils::format("%d", oneTry * 10), FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(tokenLabel2, 1);
        tokenLabel2->setHorizontalAlignment(TextHAlignment::CENTER);
        tokenLabel2->setColor(COLOR_YELLOW);
        tokenLabel2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        tokenLabel2->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);


        auto text3 = Label::createWithTTF("所持", FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(text3, 1);
        text3->setHorizontalAlignment(TextHAlignment::CENTER);
        text3->setColor(COLOR_YELLOW);
        text3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        text3->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

        auto tokenImage3 = makeSprite("token.png");
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(tokenImage3, 1);

        auto tokenLabel3 = Label::createWithTTF(StringUtils::format("%d", holdToken), FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(tokenLabel3, 1);
        tokenLabel3->setHorizontalAlignment(TextHAlignment::CENTER);
        tokenLabel3->setColor(COLOR_YELLOW);
        tokenLabel3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        tokenLabel3->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

        text1->setPosition(Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 - tokenImage1->getContentSize().width / 2 - tokenLabel1->getContentSize().width / 2 + text1->getContentSize().width / 2,
                                 titleLabel->getPosition().y - 56));
        tokenImage1->setPosition(Point(text1->getPosition().x + tokenImage1->getContentSize().width / 2 + 5,
                                       text1->getPosition().y + 13));
        tokenLabel1->setPosition(Point(tokenImage1->getPosition().x + tokenImage1->getContentSize().width / 2 + tokenLabel1->getContentSize().width + 5,
                                       text1->getPosition().y));


        text2->setPosition(Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 - tokenImage2->getContentSize().width / 2 - tokenLabel2->getContentSize().width / 2 + text2->getContentSize().width / 2,
                                 text1->getPosition().y - 40));
        tokenImage2->setPosition(Point(text2->getPosition().x + tokenImage2->getContentSize().width / 2 + 5,
                                       text2->getPosition().y + 13));
        tokenLabel2->setPosition(Point(tokenImage2->getPosition().x + tokenImage2->getContentSize().width / 2 + tokenLabel2->getContentSize().width + 5,
                                       text2->getPosition().y));


        text3->setPosition(Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 - tokenImage3->getContentSize().width / 2 - tokenLabel3->getContentSize().width / 2 + text3->getContentSize().width / 2,
                                 text2->getPosition().y - 55));
        tokenImage3->setPosition(Point(text3->getPosition().x + tokenImage3->getContentSize().width / 2 + 5,
                                       text3->getPosition().y + 13));
        tokenLabel3->setPosition(Point(tokenImage3->getPosition().x + tokenImage3->getContentSize().width / 2 + tokenLabel3->getContentSize().width + 5,
                                       text3->getPosition().y));

        int needLabelWidth = text2->getContentSize().width + tokenImage2->getContentSize().width + tokenLabel2->getContentSize().width;
        int holdLabelWidth = text3->getContentSize().width + tokenImage3->getContentSize().width + tokenLabel3->getContentSize().width;
        if (needLabelWidth > holdLabelWidth) {
            text1->setPositionX(text2->getPositionX());
            tokenImage1->setPositionX(tokenImage2->getPositionX());
            tokenLabel1->setPositionX(tokenLabel2->getPositionX());
            text3->setPositionX(text2->getPositionX());
            tokenImage3->setPositionX(tokenImage2->getPositionX());
            tokenLabel3->setPositionX(tokenLabel2->getPositionX());
        } else  {
            text1->setPositionX(text3->getPositionX());
            tokenImage1->setPositionX(tokenImage3->getPositionX());
            tokenLabel1->setPositionX(tokenLabel3->getPositionX());
            text2->setPositionX(text3->getPositionX());
            tokenImage2->setPositionX(tokenImage3->getPositionX());
            tokenLabel2->setPositionX(tokenLabel3->getPositionX());
        }
    } else if (type == FRIEND)    {
        int hodlFriendPoint = PLAYERCONTROLLER->_player->getFriendPoint();
        std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
        int tryPoint = systemSettingModel->getFpForFreeGacha();

        auto titleLabel = Label::createWithTTF("フレンドガチャ", FONT_NAME_2, 34);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(titleLabel, 1);
        titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
        titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        titleLabel->setPosition(Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2,
                                      getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height - 30 - 34));

        auto text1 = Label::createWithTTF("1回", FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(text1, 1);
        text1->setHorizontalAlignment(TextHAlignment::CENTER);
        text1->setColor(COLOR_YELLOW);
        text1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        text1->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

        auto fpLabel1 = Label::createWithTTF(StringUtils::format(" %dフレンドポイント", tryPoint), FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(fpLabel1, 1);
        fpLabel1->setHorizontalAlignment(TextHAlignment::CENTER);
        fpLabel1->setColor(COLOR_YELLOW);
        fpLabel1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        fpLabel1->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

        fpLabel1->setPosition(Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 + (fpLabel1->getContentSize().width + text1->getContentSize().width) / 2,
                                    titleLabel->getPosition().y - 56));
        text1->setPosition(fpLabel1->getPosition() - Point(fpLabel1->getContentSize().width + 10, 0));

        auto text2 = Label::createWithTTF(StringUtils::format("%d連続", maxTryCount), FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(text2, 1);
        text2->setHorizontalAlignment(TextHAlignment::CENTER);
        text2->setColor(COLOR_YELLOW);
        text2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        text2->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

        auto fpLabel2 = Label::createWithTTF(StringUtils::format(" %dフレンドポイント", tryPoint * maxTryCount), FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(fpLabel2, 1);
        fpLabel2->setHorizontalAlignment(TextHAlignment::CENTER);
        fpLabel2->setColor(COLOR_YELLOW);
        fpLabel2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        fpLabel2->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

        fpLabel2->setPosition(Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 + (fpLabel2->getContentSize().width + text2->getContentSize().width) / 2,
                                    text1->getPosition().y - 40));
        text2->setPosition(fpLabel2->getPosition() - Point(fpLabel2->getContentSize().width + 10, 0));

        auto text3 = Label::createWithTTF("所持", FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(text3, 1);
        text3->setHorizontalAlignment(TextHAlignment::CENTER);
        text3->setColor(COLOR_YELLOW);
        text3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        text3->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

        auto fpLabel3 = Label::createWithTTF(StringUtils::format(" %dフレンドポイント", hodlFriendPoint), FONT_NAME_2, 26);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(fpLabel3, 1);
        fpLabel3->setHorizontalAlignment(TextHAlignment::CENTER);
        fpLabel3->setColor(COLOR_YELLOW);
        fpLabel3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        fpLabel3->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

        fpLabel3->setPosition(Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 + (fpLabel3->getContentSize().width + text3->getContentSize().width) / 2,
                                    text2->getPosition().y - 55));
        text3->setPosition(fpLabel3->getPosition() - Point(fpLabel3->getContentSize().width + 10, 0));


        int holdFpWidth = text3->getContentSize().width + fpLabel3->getContentSize().width + 10;
        if (maxTryCount > 1) {
            int needFpWidth = text2->getContentSize().width + fpLabel2->getContentSize().width + 10;
            if (needFpWidth > holdFpWidth) {
                text1->setPositionX(text2->getPositionX());
                fpLabel1->setPositionX(fpLabel2->getPositionX());
                text3->setPositionX(text2->getPositionX());
                fpLabel3->setPositionX(fpLabel2->getPositionX());
            } else  {
                text1->setPositionX(text3->getPositionX());
                fpLabel1->setPositionX(fpLabel3->getPositionX());
                text2->setPositionX(text3->getPositionX());
                fpLabel2->setPositionX(fpLabel3->getPositionX());
            }
        } else  {
            int needFpWidth = text1->getContentSize().width + fpLabel1->getContentSize().width + 10;
            text2->setVisible(false);
            fpLabel2->setVisible(false);
            text1->setPositionY(titleLabel->getPosition().y - 76);
            fpLabel1->setPositionY(text1->getPositionY());
            if (needFpWidth > holdFpWidth) {
                text3->setPositionX(text1->getPositionX());
                fpLabel3->setPositionX(fpLabel1->getPositionX());
            } else  {
                text1->setPositionX(text3->getPositionX());
                fpLabel1->setPositionX(fpLabel3->getPositionX());
            }
        }
    }
}

void SelectGachaDialog::showButton(const GACHA_TYPE type, const int maxTryCount)
{
    std::string onceString = "まわす";
    Point closeButtonPosition = Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 + 30, 40);
    Point onceButtonPosition = Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 - 30, 40);
    if (maxTryCount > 1) {
        onceString = "1回";
        closeButtonPosition = Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 + 86, 40);
        onceButtonPosition = Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 - 86, 40);
    }

    auto onceButton = ui::Button::create("gacha_dialog_button.png");
    onceButton->addTouchEventListener(CC_CALLBACK_2(SelectGachaDialog::onTouchOnceButton, this));
    onceButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    onceButton->setPosition(onceButtonPosition);
    onceButton->setTag(TAG_SPRITE::CLOSE_BUTTON);
    auto onceLabel = Label::createWithTTF(onceString, FONT_NAME_2, 28);
    onceLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    onceLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    onceLabel->setPosition(Vec2(onceButton->getContentSize() / 2) - Vec2(0, 14));
    onceButton->addChild(onceLabel);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(onceButton, Z_ORDER::Z_BUTTON);
    if (maxTryCount > 1) {
        auto tryButton = ui::Button::create("gacha_dialog_button.png");
        tryButton->addTouchEventListener(CC_CALLBACK_2(SelectGachaDialog::onTouchTryButton, this));
        tryButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
        tryButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2, 40));
        tryButton->setTag(TAG_SPRITE::CLOSE_BUTTON);
        auto tryLabel = Label::createWithTTF(StringUtils::format("%d回", maxTryCount), FONT_NAME_2, 28);
        tryLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        tryLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        tryLabel->setPosition(Vec2(tryButton->getContentSize() / 2) - Vec2(0, 14));
        tryButton->addChild(tryLabel);
        getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(tryButton, Z_ORDER::Z_BUTTON);
    }
    auto closeButton = ui::Button::create("gacha_dialog_button.png");
    closeButton->addTouchEventListener(CC_CALLBACK_2(SelectGachaDialog::onTouchCloseButton, this));
    closeButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    closeButton->setPosition(closeButtonPosition);
    closeButton->setTag(TAG_SPRITE::CLOSE_BUTTON);
    auto closeLabel = Label::createWithTTF("閉じる", FONT_NAME_2, 28);
    closeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    closeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    closeLabel->setPosition(Vec2(closeButton->getContentSize() / 2) - Vec2(0, 14));
    closeButton->addChild(closeLabel);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(closeButton, Z_ORDER::Z_BUTTON);
}

void SelectGachaDialog::onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        removeFromParent();
    }
}

void SelectGachaDialog::onTouchOnceButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _callBack(_type, 1);
        removeFromParent();
    }
}

void SelectGachaDialog::onTouchTryButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _callBack(_type, tryGachaCount(_type));
        removeFromParent();
    }
}