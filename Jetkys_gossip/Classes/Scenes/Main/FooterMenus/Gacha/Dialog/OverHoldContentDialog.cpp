#include "OverHoldContentDialog.h"
#include "FontDefines.h"
#include "SoundHelper.h"

OverHoldContentDialog::OverHoldContentDialog()
{}

OverHoldContentDialog::~OverHoldContentDialog()
{}

bool OverHoldContentDialog::init(const std::string title, const OnGoCompositionCallback& goCompositionCallback, const OnGoSellCallback& goSellCallback, const OnGoExtensionCallback& goExtensionCallback)
{
    if (!Layer::create()) {
        return false;
    }
    _goCompositionCallback = goCompositionCallback;
    _goSellCallback = goSellCallback;
    _goExtensionCallback = goExtensionCallback;

    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    showBackground();
    showTitle(title);
    showMessage();
    showButton();

    return true;
}

void OverHoldContentDialog::showBackground()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto blackLayer = LayerColor::create(Color4B(Color3B::BLACK), winSize.width, winSize.height);
    blackLayer->setOpacity(128);
    blackLayer->setTag(TAG_SPRITE::BLACK_LAYER);
    addChild(blackLayer, Z_ORDER::Z_BLACK_LAYER);

    auto background = Sprite::create("dialog_base.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Vec2(winSize / 2));
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void OverHoldContentDialog::showTitle(const std::string title)
{
    auto label = Label::createWithTTF(title, FONT_NAME_2, 32);
    label->setTag(TAG_SPRITE::TITLE);
    label->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 25));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setColor(Color3B::YELLOW);
    label->setAnchorPoint(Vec2(0.5, 1.0f));
    addChild(label, Z_ORDER::Z_TITLE);
}

void OverHoldContentDialog::showMessage()
{
    auto label = Label::createWithTTF("売却、合成、ガレージ拡張\nのどれかを行ってください", FONT_NAME_2, 22);
    label->setTag(TAG_SPRITE::MESSAGE);
    label->setDimensions(500, 0);
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    auto winSize = Director::getInstance()->getWinSize();
    label->setPosition(Point(winSize.width / 2, winSize.height / 2 + 11));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(label, Z_ORDER::Z_MESSAGE);
}

void OverHoldContentDialog::showButton()
{
    auto compositionButton = createButton("合成する");
    compositionButton->addTouchEventListener(CC_CALLBACK_2(OverHoldContentDialog::compositionButtonCallBack, this));
    compositionButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    compositionButton->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(-86, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + 40));
    addChild(compositionButton, Z_ORDER::Z_BUTTON);

    auto sellButton = createButton("売却する");
    sellButton->addTouchEventListener(CC_CALLBACK_2(OverHoldContentDialog::sellButtonCallBack, this));
    sellButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    sellButton->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + 40));
    addChild(sellButton, Z_ORDER::Z_BUTTON);

    auto extensionButton = createButton("拡張する");
    extensionButton->addTouchEventListener(CC_CALLBACK_2(OverHoldContentDialog::extensionButtonCallBack, this));
    extensionButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    extensionButton->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(86, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + 40));
    addChild(extensionButton, Z_ORDER::Z_BUTTON);
}

ui::Button* OverHoldContentDialog::createButton(const std::string buttonString)
{
    auto result = ui::Button::create("gacha_dialog_button.png");
    result->setTag(TAG_SPRITE::BUTTON);
    auto label = Label::createWithTTF(buttonString, FONT_NAME_2, 28);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    label->setPosition(Vec2(result->getContentSize() / 2) - Vec2(0, 14));
    result->addChild(label);
    return result;
}

void OverHoldContentDialog::compositionButtonCallBack(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _goCompositionCallback();
        if (!this) {
            return;
        }
        removeFromParent();
    }
}

void OverHoldContentDialog::sellButtonCallBack(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _goSellCallback();
        if (!this) {
            return;
        }
        removeFromParent();
    }
}

void OverHoldContentDialog::extensionButtonCallBack(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _goExtensionCallback();
        if (!this) {
            return;
        }
        removeFromParent();
    }
}

