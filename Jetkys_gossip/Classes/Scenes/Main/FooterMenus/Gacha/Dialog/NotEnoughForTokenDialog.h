#ifndef __syanago__NotEnoughForTokenDialog__
#define __syanago__NotEnoughForTokenDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/cocosGUI.h"

USING_NS_CC;

class NotEnoughForTokenDialog : public Layer, public create_func<NotEnoughForTokenDialog>
{
public:
    typedef std::function<void()> OnGoToTokenShopCallback;
    using create_func::create;
    bool init(const OnGoToTokenShopCallback& goToTokenShopCallback);
    
    NotEnoughForTokenDialog();
    ~NotEnoughForTokenDialog();
private:
    enum TAG_SPRITE{
        BLACK_LAYER = 0,
        BACKGROUND,
        BUTTON,
        TITLE,
        MESSAGE ,
    };
    enum Z_ORDER{
        Z_BUTTON = 0,
        Z_TITLE,
        Z_MESSAGE,
    };
    OnGoToTokenShopCallback _goToTokenShopCallback;
    
    void showBackground();
    void showButton();
    void showTitle();
    void showMessage();
    void onTouchYesButton(Ref* sender, ui::Widget::TouchEventType type);
    void onTouchNoButton(Ref* sender, ui::Widget::TouchEventType type);
};

#endif