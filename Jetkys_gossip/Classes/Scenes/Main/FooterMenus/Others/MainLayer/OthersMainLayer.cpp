#include "OthersMainLayer.h"

#include "OthersLayer.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include "HeaderStatus.h"

#define THRESHOLD 20

bool OthersMainLayer::init()
{
    if (!Layer::init()) {
        return false;
    }

    showBackground();
    showHeadLine();
    showMenu();

    return true;
}

void OthersMainLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    //background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getVisibleSize() / 2);
    addChild(background/*, Z_ORDER::Z_BACKGROUND*/);
}

void OthersMainLayer::showHeadLine()
{
    auto othersTitleBg = makeSprite("head_line_no1.png");
    addChild(othersTitleBg);
    othersTitleBg->setPosition(Vec2(othersTitleBg->getContentSize().width / 2,
                                    Director::getInstance()->getWinSize().height - othersTitleBg->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 12));

    auto title = Label::createWithTTF("その他", FONT_NAME_2, 34);
    title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    addChild(title);
    title->setAnchorPoint(Vec2(0.0f, 0.5f));
    title->setPosition(Point(14,
                             othersTitleBg->getPosition().y - 17));
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
}

void OthersMainLayer::showMenu()
{
    auto btInfo = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btInfo->setTag(OI_INFOMATION);
    auto labelInfo = Label::createWithTTF("インフォメーション", FONT_NAME_2, 34);
    labelInfo->setPosition(Point(btInfo->getContentSize().width / 2, btInfo->getContentSize().height / 2 - 17));
    labelInfo->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    btInfo->addChild(labelInfo);

    auto btEvent = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btEvent->setTag(OI_EVENT);
    auto labelEvent = Label::createWithTTF("イベント", FONT_NAME_2, 34);
    labelEvent->setPosition(Point(btEvent->getContentSize().width / 2, btEvent->getContentSize().height / 2 - 17));
    labelEvent->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    btEvent->addChild(labelEvent);

    auto btColection = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btColection->setTag(OI_COLECTION);
    auto labelColection = Label::createWithTTF("車なご図鑑", FONT_NAME_2, 34);
    labelColection->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelColection->setPosition(Point(btColection->getContentSize().width / 2, btColection->getContentSize().height / 2 - 17));
    btColection->addChild(labelColection);

    auto btTreasure = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btTreasure->setTag(OI_TREASURES);
    auto labelTreasure = Label::createWithTTF("宝物図鑑", FONT_NAME_2, 34);
    labelTreasure->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelTreasure->setPosition(Point(btTreasure->getContentSize().width / 2, btTreasure->getContentSize().height / 2 - 17));
    btTreasure->addChild(labelTreasure);

    auto btCampaign = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btCampaign->setTag(OI_CAMPAIGN);
    auto labelCampaign = Label::createWithTTF("キャンペーンコード", FONT_NAME_2, 34);
    labelCampaign->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelCampaign->setPosition(Point(btCampaign->getContentSize().width / 2, btCampaign->getContentSize().height / 2 - 17));
    btCampaign->addChild(labelCampaign);

    auto btOption = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btOption->setTag(OI_OPTION);
    auto labelOption = Label::createWithTTF("オプション", FONT_NAME_2, 34);
    labelOption->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelOption->setPosition(Point(btOption->getContentSize().width / 2, btOption->getContentSize().height / 2 - 17));
    btOption->addChild(labelOption);

    auto btHelp = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btHelp->setTag(OI_HELP);
    auto labelHelp = Label::createWithTTF("遊びかた", FONT_NAME_2, 34);
    labelHelp->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelHelp->setPosition(Point(btHelp->getContentSize().width / 2, btHelp->getContentSize().height / 2 - 17));
    btHelp->addChild(labelHelp);

    auto btTutolial = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btTutolial->setTag(OI_TUTOLI);
    auto labelTutolial = Label::createWithTTF("チュートリアル", FONT_NAME_2, 34);
    labelTutolial->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelTutolial->setPosition(Point(btTutolial->getContentSize().width / 2, btTutolial->getContentSize().height / 2 - 17));
    btTutolial->addChild(labelTutolial);

    auto btBuylog = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btBuylog->setTag(OI_BUYLOG);
    auto labelBuylog = Label::createWithTTF("購入情報", FONT_NAME_2, 34);
    labelBuylog->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelBuylog->setPosition(Point(btBuylog->getContentSize().width / 2, btBuylog->getContentSize().height / 2 - 17));
    btBuylog->addChild(labelBuylog);

    auto btContact = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btContact->setTag(OI_CONTACT);
    auto labelContact = Label::createWithTTF("お問い合わせ", FONT_NAME_2, 34);
    labelContact->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelContact->setPosition(Point(btContact->getContentSize().width / 2, btContact->getContentSize().height / 2 - 17));
    btContact->addChild(labelContact);

    auto btModelChange = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btModelChange->setTag(OI_MODELCHANGE);
    auto labelModelChange = Label::createWithTTF("引き継ぎコード発行", FONT_NAME_2, 34);
    labelModelChange->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelModelChange->setPosition(Point(btModelChange->getContentSize().width / 2, btModelChange->getContentSize().height / 2 - 17));
    btModelChange->addChild(labelModelChange);

    auto btRule = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btRule->setTag(OI_RULE);
    auto labelRule = Label::createWithTTF("利用規約", FONT_NAME_2, 34);
    labelRule->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelRule->setPosition(Point(btRule->getContentSize().width / 2, btRule->getContentSize().height / 2 - 17));
    btRule->addChild(labelRule);

    auto btSettlement = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btSettlement->setTag(OI_SETTLEMENT);
    auto labelSettlement = Label::createWithTTF("資金決済法", FONT_NAME_2, 34);
    labelSettlement->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelSettlement->setPosition(Point(btSettlement->getContentSize().width / 2, btSettlement->getContentSize().height / 2 - 17));
    btSettlement->addChild(labelSettlement);

    auto btNotice = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btNotice->setTag(OI_NOTICE);
    auto labelNotice = Label::createWithTTF("運営からのお知らせ", FONT_NAME_2, 34);
    labelNotice->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelNotice->setPosition(Point(btNotice->getContentSize().width / 2, btNotice->getContentSize().height / 2 - 17));
    btNotice->addChild(labelNotice);

    auto btPresent = makeMenuItem("button_no1.png", CC_CALLBACK_1(OthersMainLayer::btMenuItemCallback, this));
    btPresent->setTag(OI_PRESENT);
    auto labelPresent = Label::createWithTTF("プレゼントBOX", FONT_NAME_2, 34);
    labelPresent->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    labelPresent->setPosition(Point(btPresent->getContentSize().width / 2, btPresent->getContentSize().height / 2 - 17));
    btPresent->addChild(labelPresent);

    if (UserDefault::getInstance()->getBoolForKey("demandMode") == true) {
        _menu = MenuTouch::create(btNotice, btPresent, btInfo, btColection, btTreasure, btOption, btHelp, btBuylog, btContact, btModelChange /*,btPlayerName*/, btRule, btSettlement, NULL);
    } else {
        _menu = MenuTouch::create(btNotice, btPresent, btInfo, btEvent, btColection, btTreasure /*,btCampaign*/, btOption, btHelp, btTutolial, btBuylog, btContact, btModelChange /*,btPlayerName*/, btRule, btSettlement, NULL);
    }


    // 画面サイズでスクロールビューを作る
    // 隙間の量を指定
    int padding = 14;

    _menu->alignItemsVerticallyWithPadding(padding);
    Size winSize = Director::getInstance()->getWinSize();
    _menu->setContentSize(Size(winSize.width,
                               ((int)_menu->getChildrenCount() + 0.5f) * (padding + _menu->getChildren().at(0)->getContentSize().height) + HEADER_STATUS::SIZE::HEIGHT));

    if (UserDefault::getInstance()->getBoolForKey("demandMode") == true) {
        _menu->setPositionY(900 + HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF);
    } else {
        // _menu->setPositionY(1080);//チュートリアル,キャンペーンコード込み
        _menu->setPositionY(1020 + HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF);// チュートリアルメニュー込み
    }

    // add bage notice + present
    // add friend badge
    if (PLAYERCONTROLLER->_numberOfNotice > 0) {
        if (_noticeBage != nullptr) {
            _noticeBage->setVisible(true);
        }
        _noticeBage = addBadgeNode(btNotice, "itemSpr.png", PLAYERCONTROLLER->_numberOfNotice);
        _noticeBage->setPosition(Vec2(btNotice->getContentSize().width - 5, btNotice->getContentSize().height - 5));
    }

    // add friend badge
    if (PLAYERCONTROLLER->_numberOfCompensation > 0) {
        if (_presentBage != nullptr) {
            _presentBage->setVisible(true);
        }
        _presentBage = addBadgeNode(btPresent, "itemSpr.png", PLAYERCONTROLLER->_numberOfCompensation);
        _presentBage->setPosition(Vec2(btPresent->getContentSize().width - 5, btPresent->getContentSize().height - 5));
    }

    ////CCLOG("メニュー:%f, %f", _menu->getPosition().x, _menu->getPosition().y);
    // スクロールビュー用レイヤーを生成
    auto scrollViewLayer = Layer::create();
    scrollViewLayer->setContentSize(Size(_menu->getContentSize().width, _menu->getContentSize().height + 20));
    scrollViewLayer->addChild(_menu);
    _menu->setPositionY(_menu->getPositionY() - 20);

    // 画面サイズでスクロールビューを作る
    pScrollView = ScrollView::create();
    pScrollView->setViewSize(Size(winSize.width, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65));
    addChild(pScrollView);
    pScrollView->setContainer(scrollViewLayer);
    pScrollView->setDirection(ScrollView::Direction::VERTICAL);
    pScrollView->setContentSize(_menu->getContentSize());
    pScrollView->setPosition(Point(0, 0));
    pScrollView->setBounceable(false);
    pScrollView->setContentOffset(Point(0.0f, pScrollView->getViewSize().height - pScrollView->getContentSize().height));
    
    pScrollView->setDelegate(this);
    _scrollBar = ScrollBar::initScrollBar(winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 180 - 30, pScrollView->getViewSize().height, pScrollView->getContainer()->getContentSize().height, pScrollView->getContentOffset().y);
    addChild(_scrollBar);
    _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 15 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    if (pScrollView->getViewSize().height <= pScrollView->getContainer()->getContentSize().height) {
        pScrollView->setPosition(Point(-20, 0));
    }
    
    Director::getInstance()->getEventDispatcher()->removeEventListenersForTarget(_menu);
    auto listener = EventListenerTouchOneByOne::create();

    listener->onTouchBegan = [this](Touch* touch, Event* event) {
                                 if (!pScrollView->onTouchBegan(touch, event))
                                     return false;
                                 return _menu->onTouchBegan(touch, event);
                             };

    listener->onTouchMoved = [this](Touch* touch, Event* event) {
                                 pScrollView->onTouchMoved(touch, event);
                                 _menu->onTouchMoved(touch, event);
                             };

    listener->onTouchEnded = [this](Touch* touch, Event* event) {
                                 if (pScrollView->isTouchMoved()) {
                                     pScrollView->onTouchEnded(touch, event);
                                     _menu->onTouchCancelled(touch, event);
                                     return;
                                 }
                                 pScrollView->onTouchEnded(touch, event);
                                 _menu->onTouchEnded(touch, event);
                             };

    listener->onTouchCancelled = [this](Touch* touch, Event* event) {
                                     pScrollView->onTouchCancelled(touch, event);
                                     _menu->onTouchCancelled(touch, event);
                                 };

    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, _menu);
}

void OthersMainLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    int tag = ((Node*)pSender)->getTag();
    _othersLayer->showLayer((OthersIndex) tag);
}

void OthersMainLayer::scrollViewDidScroll(ScrollView* view)
{
    _scrollBar->updateScrollPoint(pScrollView->getContentOffset().y);
}
void OthersMainLayer::scrollViewDidZoom(ScrollView* view)
{}

void OthersMainLayer::updateBage()
{
    if (PLAYERCONTROLLER->_numberOfNotice > 0) {
        if (_noticeBage != nullptr) {
            _noticeBage->setVisible(true);
            ((Label*)_noticeBage->getChildByTag(1))->setVisible(true);
            ((Label*)_noticeBage->getChildByTag(1))->setString(StringUtils::format("%d", PLAYERCONTROLLER->_numberOfNotice));
        }
    } else {
        if (_noticeBage != nullptr) {
            ((Label*)_noticeBage->getChildByTag(1))->setVisible(false);
            _noticeBage->setVisible(false);
        }
    }

    if (PLAYERCONTROLLER->_numberOfCompensation > 0) {
        if (_presentBage != nullptr) {
            _presentBage->setVisible(true);
            ((Label*)_presentBage->getChildByTag(1))->setVisible(true);
            ((Label*)_presentBage->getChildByTag(1))->setString(StringUtils::format("%d", PLAYERCONTROLLER->_numberOfCompensation));
        }
    } else {
        if (_presentBage != nullptr) {
            ((Label*)_presentBage->getChildByTag(1))->setVisible(false);
            _presentBage->setVisible(false);
        }
    }
}

Node* OthersMainLayer::addBadgeNode(Node* node, std::string imgBadge, int number)
{
    auto bage = makeSprite(imgBadge.c_str());
    node->addChild(bage);

    auto label = Label::createWithTTF(StringUtils::format("%d", number).c_str(), FONT_NAME_2, 22);
    bage->addChild(label);
    label->setPosition(Vec2(bage->getContentSize().width / 2, bage->getContentSize().height / 2 - 11));
    label->setTag(1);

    return bage;
}
