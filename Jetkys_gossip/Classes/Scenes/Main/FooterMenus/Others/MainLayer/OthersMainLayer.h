#ifndef __syanago__OthersMainLayer__
#define __syanago__OthersMainLayer__

#include "cocos2d.h"
#include "ScrollBar.h"
USING_NS_CC;

#include "extensions/cocos-ext.h"
USING_NS_CC_EXT;

class OthersLayer;

class OthersMainLayer : public Layer, public ScrollViewDelegate{
public:
    bool init();
    CREATE_FUNC(OthersMainLayer);
    
    OthersMainLayer():
    _noticeBage(nullptr),
    _presentBage(nullptr)
    {};
    
    OthersLayer *_othersLayer;
    
    void btMenuItemCallback(Ref *pSender);
    
    void scrollViewDidScroll(ScrollView *view);
    void scrollViewDidZoom(ScrollView *view);
    void updateBage();
private:
    Node *addBadgeNode(Node *node, std::string imgBadge, int number);
    
    Point _beganTouhPoint;
    bool _moveSCrollFlag;
    Menu *_menu;
    
    ScrollView *pScrollView;
    ScrollBar *_scrollBar;
    Node *_noticeBage;
    Node *_presentBage;
    
    void showBackground();
    void showHeadLine();
    void showMenu();
};

#endif
