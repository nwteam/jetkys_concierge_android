#include "OthersContactLayer.h"

OthersContactLayer::OthersContactLayer():
    _callbackModel(nullptr)
{
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
}

OthersContactLayer::~OthersContactLayer()
{
    delete progress;
    delete request;
}

bool OthersContactLayer::init(const std::shared_ptr<OthersCallbackHtmlModel>& callbackModel)
{
    if (!Layer::init()) {
        return false;
    }
    _callbackModel = callbackModel;
    showBackground();
    showHeadLine(this, "お問い合わせ", CC_CALLBACK_1(OthersContactLayer::btMenuItemCallback, this));

    progress->onStart();
    request->getFAQ([this](std::string html) {
        _callbackModel->getShowWebView()(html);
    });
    return true;
}

void OthersContactLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getVisibleSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void OthersContactLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    _callbackModel->getRemoveWebView()();
    _callbackModel->getRemoveBlackLayer()();
    _callbackModel->getOnTapPageBack()();
}
