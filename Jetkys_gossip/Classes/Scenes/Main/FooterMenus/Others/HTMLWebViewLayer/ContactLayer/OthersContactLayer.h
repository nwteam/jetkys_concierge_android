#ifndef __syanago__OthersContactLayer__
#define __syanago__OthersContactLayer__

#include "cocos2d.h"
#include "OthersLayer.h"
#include "Loading.h"
#include "show_head_line.h"
#include "create_func.h"
#include "OthersCallbackHtmlModel.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
using namespace SyanagoAPI;

class OthersContactLayer : public Layer, public create_func<OthersContactLayer>,  public show_head_line
{
public:
    OthersContactLayer();
    ~OthersContactLayer();
    using create_func::create;
    bool init(const std::shared_ptr<OthersCallbackHtmlModel>& callbackModel);
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
    };
    std::shared_ptr<OthersCallbackHtmlModel> _callbackModel;
    
    void showBackground();
    void btMenuItemCallback(Ref* pSender);
    
    DefaultProgress* progress;
    RequestAPI* request;
};

#endif
