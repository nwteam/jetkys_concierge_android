#ifndef __syanago__OthersCallbackHtmlModel__
#define __syanago__OthersCallbackHtmlModel__

#include "cocos2d.h"

USING_NS_CC;

class OthersCallbackHtmlModel
{
public:
    typedef std::function<void()> OnTapBackButtonCallBack;
    typedef std::function<void(std::string htmlTag)> ShowWebViewCallback;
    typedef std::function<void()> RemoveWebViewCallback;
    typedef std::function<void()> RemoveBlackLayerCallback;
    
    OthersCallbackHtmlModel(const OnTapBackButtonCallBack& onTapBackButtonCallback,
                            const ShowWebViewCallback& showWebViewCallback,
                            const RemoveWebViewCallback& removeWebViewCallback,
                            const RemoveBlackLayerCallback& reomoveBlackLayerCallback);
    ~OthersCallbackHtmlModel(){};
    
    CC_SYNTHESIZE_READONLY(OnTapBackButtonCallBack, onTapPageBack, OnTapPageBack);
    CC_SYNTHESIZE_READONLY(ShowWebViewCallback, showWebView, ShowWebView);
    CC_SYNTHESIZE_READONLY(RemoveWebViewCallback, removewWebView, RemoveWebView);
    CC_SYNTHESIZE_READONLY(RemoveBlackLayerCallback, removeBlackLayer, RemoveBlackLayer);
};

#endif
