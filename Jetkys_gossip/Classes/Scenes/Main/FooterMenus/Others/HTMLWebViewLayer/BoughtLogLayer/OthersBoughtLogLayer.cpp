#include "OthersBoughtLogLayer.h"

OthersBoughtLogLayer::OthersBoughtLogLayer():
    _callbackModel(nullptr)
{
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
}

OthersBoughtLogLayer::~OthersBoughtLogLayer()
{
    delete progress;
    delete request;
}

bool OthersBoughtLogLayer::init(const std::shared_ptr<OthersCallbackHtmlModel>& callbackModel)
{
    if (!Layer::init()) {
        return false;
    }
    _callbackModel = callbackModel;
    showBackground();
    showHeadLine(this, "購入情報", CC_CALLBACK_1(OthersBoughtLogLayer::btMenuItemCallback, this));

    progress->onStart();
    request->getPurchaseInformation([this](std::string html) {
        _callbackModel->getShowWebView()(html);
    });
    return true;
}

void OthersBoughtLogLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getVisibleSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void OthersBoughtLogLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    _callbackModel->getRemoveWebView()();
    _callbackModel->getRemoveBlackLayer()();
    _callbackModel->getOnTapPageBack()();
}
