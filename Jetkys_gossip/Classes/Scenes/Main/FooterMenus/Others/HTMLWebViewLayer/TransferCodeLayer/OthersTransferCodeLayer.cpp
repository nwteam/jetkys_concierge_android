#include "OthersTransferCodeLayer.h"

OthersTransferCodeLayer::OthersTransferCodeLayer():
    _callbackModel(nullptr)
{
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
}

OthersTransferCodeLayer::~OthersTransferCodeLayer()
{
    delete progress;
    delete request;
}

bool OthersTransferCodeLayer::init(const std::shared_ptr<OthersCallbackHtmlModel>& callbackModel)
{
    if (!Layer::init()) {
        return false;
    }
    _callbackModel = callbackModel;
    showBackground();
    showHeadLine(this, "引き継ぎコード", CC_CALLBACK_1(OthersTransferCodeLayer::btMenuItemCallback, this));

    progress->onStart();
    request->getTakeoverPasswordForm([this](std::string html) {
        _callbackModel->getShowWebView()(html);
    });
    return true;
}

void OthersTransferCodeLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getVisibleSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void OthersTransferCodeLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    _callbackModel->getRemoveWebView()();
    _callbackModel->getRemoveBlackLayer()();
    _callbackModel->getOnTapPageBack()();
}
