#ifndef __syanago__OthersEventLayer__
#define __syanago__OthersEventLayer__

#include "cocos2d.h"
#include "OthersLayer.h"
#include "show_head_line.h"
#include "create_func.h"
#include "OthersCallbackWebURLModel.h"

USING_NS_CC;

class OthersEventLayer : public Layer, public create_func<OthersEventLayer>, public show_head_line {
public:
    OthersEventLayer();
    using create_func::create;
    bool init(const std::shared_ptr<OthersCallbackWebURLModel>& callbackModel);
    
    void btMenuItemCallback(Ref* pSender);
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        WEBVIEW_BASE,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_WEBVIEW_BASE = 12,
    };
    std::shared_ptr<OthersCallbackWebURLModel> _callbackModel;
    
    void showBackground();
    void showWebView();
};

#endif
