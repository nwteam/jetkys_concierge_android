#include "OthersHelpLayer.h"
#include "FontDefines.h"
#include "HeaderStatus.h"

OthersHelpLayer::OthersHelpLayer():
    _callbackModel(nullptr)
{}

bool OthersHelpLayer::init(const std::shared_ptr<OthersCallbackWebURLModel>& callbackModel)
{
    if (!Layer::create()) {
        return false;
    }
    _callbackModel = callbackModel;

    showBackground();
    showHeadLine(this, "ゲームの遊び方", CC_CALLBACK_1(OthersHelpLayer::btMenuItemCallback, this));
    showWebView();

    return true;
}

void OthersHelpLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getVisibleSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void OthersHelpLayer::showWebView()
{
    auto webViewBase = makeSprite("webview_bg_small.png");
    webViewBase->setTag(TAG_SPRITE::WEBVIEW_BASE);
    Size winSize = Director::getInstance()->getWinSize();
    webViewBase->setPosition(Point(winSize.width / 2, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - webViewBase->getContentSize().height / 2));
    addChild(webViewBase, Z_ORDER::Z_WEBVIEW_BASE);
    _callbackModel->getShowWebView()(OI_WEB_EVENT);
}

void OthersHelpLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    _callbackModel->getRemoveWebView()();
    _callbackModel->getRemoveBlackLayer()();
    _callbackModel->getOnTapPageBack()();
}

