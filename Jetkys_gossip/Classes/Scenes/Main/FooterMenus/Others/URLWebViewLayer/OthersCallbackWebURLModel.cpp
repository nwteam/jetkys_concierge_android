#include "OthersCallbackWebURLModel.h"

OthersCallbackWebURLModel::OthersCallbackWebURLModel(const OnTapBackButtonCallBack& onTapBackButtonCallback,
                                                     const ShowWebViewCallback& showWebViewCallback,
                                                     const RemoveWebViewCallback& removeWebViewCallback,
                                                     const RemoveBlackLayerCallback& reomoveBlackLayerCallback):
    onTapPageBack(nullptr), showWebView(nullptr), removewWebView(nullptr), removeBlackLayer(nullptr)
{
    onTapPageBack = onTapBackButtonCallback;
    showWebView = showWebViewCallback;
    removewWebView = removeWebViewCallback;
    removeBlackLayer = reomoveBlackLayerCallback;
}