#ifndef __syanago__OthersCallbackWebURLModel__
#define __syanago__OthersCallbackWebURLModel__

#include "cocos2d.h"
#include "OthersLayer.h"

class OthersCallbackWebURLModel
{
public:
    typedef std::function<void()> OnTapBackButtonCallBack;
    typedef std::function<void(OtyersUrlWebViewTag urlTag)> ShowWebViewCallback;
    typedef std::function<void()> RemoveWebViewCallback;
    typedef std::function<void()> RemoveBlackLayerCallback;
    
    OthersCallbackWebURLModel(const OnTapBackButtonCallBack& onTapBackButtonCallback,
                              const ShowWebViewCallback& showWebViewCallback,
                              const RemoveWebViewCallback& removeWebViewCallback,
                              const RemoveBlackLayerCallback& reomoveBlackLayerCallback);
    ~OthersCallbackWebURLModel(){};
    
    CC_SYNTHESIZE_READONLY(OnTapBackButtonCallBack, onTapPageBack, OnTapPageBack);
    CC_SYNTHESIZE_READONLY(ShowWebViewCallback, showWebView, ShowWebView);
    CC_SYNTHESIZE_READONLY(RemoveWebViewCallback, removewWebView, RemoveWebView);
    CC_SYNTHESIZE_READONLY(RemoveBlackLayerCallback, removeBlackLayer, RemoveBlackLayer);
};

#endif