#ifndef __syanago__NoticeDialog__
#define __syanago__NoticeDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "editor-support/spine/Json.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class NoticeDialog : public Layer, public create_func<NoticeDialog>
{
public:
    NoticeDialog();
    ~NoticeDialog();
    typedef std::function<void()> OnTapDeleteButton;
    typedef std::function<void()> OnTapCloseButton;
    typedef std::function<void(const std::string url)> OnTapLaunchURLButton;
    using create_func::create;
    bool init(Json* noticeJson,
              const OnTapDeleteButton& deleteCallback,
              const OnTapCloseButton& closeCallback,
              const OnTapLaunchURLButton& launchURLCallback);
    
private:
    enum TAG_SPRITE {
        BLACK_LAYER = 0,
        BACKGROUND,
        BACKGROUND_MESSAGE_BASE,
        TITLE,
        MESSAGE,
        DELETE_BUTTON,
        CLOSE_BUTTON,
        LAUNCH_BUTTON,
    };
    enum Z_ORDER {
        Z_BLACK_LAYER = 0,
        Z_BACKGROUND,
        Z_TITLE,
        Z_MESSAGE,
        Z_DELETE_BUTTON,
        Z_CLOSE_BUTTON,
        Z_LAUNCH_BUTTON,
    };
    OnTapDeleteButton _deleteCallback;
    OnTapCloseButton _closeCallback;
    OnTapLaunchURLButton _launchURLCallback;
    
    void showBlackLayer();
    void showBackground();
    void showTitle(const std::string title);
    void showMessage(const std::string message);
    void showNoticeDeleteButton();
    void showNoticeCloseButton();
    void showNoticelauncherURL(const std::string url);
};

#endif
