#include "NoticeDialog.h"
#include "HeaderStatus.h"
#include "TelopScrollView.h"
#include "FontDefines.h"
#include "SoundHelper.h"

NoticeDialog::NoticeDialog():
    _deleteCallback(nullptr), _closeCallback(nullptr), _launchURLCallback(nullptr)
{}

NoticeDialog::~NoticeDialog()
{}

bool NoticeDialog::init(Json* noticeJson,
                        const OnTapDeleteButton& deleteCallback,
                        const OnTapCloseButton& closeCallback,
                        const OnTapLaunchURLButton& launchURLCallback)
{
    if (!Layer::create()) {
        return false;
    }
    _deleteCallback = deleteCallback;
    _closeCallback = closeCallback;
    _launchURLCallback = launchURLCallback;

    showBlackLayer();
    showBackground();
    showTitle(Json_getString(noticeJson, "subject", ""));
    showMessage(Json_getString(noticeJson, "content", ""));

    std::string url = Json_getString(noticeJson, "url", "");
    if (url != "") {
        showNoticelauncherURL(url);
    }
    showNoticeDeleteButton();
    showNoticeCloseButton();

    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    return true;
}

void NoticeDialog::showBlackLayer()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto blackLayer = LayerColor::create(Color4B(Color3B::BLACK), winSize.width, winSize.height);
    blackLayer->setOpacity(128);
    blackLayer->setTag(TAG_SPRITE::BLACK_LAYER);
    addChild(blackLayer, Z_ORDER::Z_BLACK_LAYER);
}

void NoticeDialog::showBackground()
{
    auto background = Sprite::create("webview_bg_small.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    Size winSize = Director::getInstance()->getWinSize();
    background->setPosition(Vec2(winSize.width / 2, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - background->getContentSize().height / 2));
    auto messageBase = LayerColor::create(Color4B::WHITE,
                                          background->getContentSize().width - 80,
                                          background->getContentSize().height * 0.6);
    messageBase->setTag(TAG_SPRITE::BACKGROUND_MESSAGE_BASE);
    messageBase->setOpacity(180);
    background->addChild(messageBase);
    messageBase->setAnchorPoint(Vec2(0.5, 0.5));
    messageBase->setPosition(Vec2(background->getContentSize().width / 2 - messageBase->getContentSize().width / 2,
                                  background->getContentSize().height / 2 - messageBase->getContentSize().height / 2 + 40 - 20));
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void NoticeDialog::showTitle(const std::string title)
{
    TelopScrollView* telopView = TelopScrollView::initScrollTelop(title,
                                                                  34,
                                                                  getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width - 40,
                                                                  36,
                                                                  Color3B(255, 231, 117));
    telopView->setAnchorPoint(Vec2(0.5, 0.5));
    telopView->setPosition(Point(20, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height - 54));
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(telopView);
}

void NoticeDialog::showMessage(const std::string message)
{
    auto* pScrollView = ScrollView::create(Director::getInstance()->getWinSize() - Size(20, 20));
    pScrollView->setDirection(ScrollView::Direction::VERTICAL);
    auto messageBase = getChildByTag(TAG_SPRITE::BACKGROUND)->getChildByTag(TAG_SPRITE::BACKGROUND_MESSAGE_BASE);
    pScrollView->setViewSize(Size(messageBase->getContentSize().width - 10, messageBase->getContentSize().height - 10));
    pScrollView->ignoreAnchorPointForPosition(false);
    pScrollView->setAnchorPoint(Point(0.5, 0.5));
    pScrollView->setPosition(Point(messageBase->getContentSize().width * 0.5, messageBase->getContentSize().height * 0.5));

    auto contentLabel = Label::createWithTTF(message, FONT_NAME_2, 28);
    contentLabel->setAlignment(TextHAlignment::LEFT);
    contentLabel->setDimensions(pScrollView->getViewSize().width - 20, 0);
    contentLabel->setDimensions(contentLabel->getContentSize().width, contentLabel->getContentSize().height);
    contentLabel->setColor(Color3B::BLACK);

    pScrollView->setContainer(contentLabel);
    pScrollView->setContentOffset(Point(0, -(contentLabel->getContentSize().height - pScrollView->getViewSize().height)));
    messageBase->addChild(pScrollView);
}

void NoticeDialog::showNoticelauncherURL(const std::string url)
{
    auto launchButton = ui::Button::create("garage_popup_button.png");
    launchButton->setTag(TAG_SPRITE::LAUNCH_BUTTON);
    launchButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    launchButton->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() + Vec2(91, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + 30));
    launchButton->addTouchEventListener([this, url](Ref* pSender, ui::Widget::TouchEventType type) {
        if (type != ui::Widget::TouchEventType::ENDED) {
            return;
        }
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _launchURLCallback(url);
    });
    auto launchLabel = Label::createWithTTF("ブラウザ起動", FONT_NAME_2, 22);
    launchLabel->setPosition(Point(launchButton->getContentSize().width / 2, launchButton->getContentSize().height / 2 - 11));
    launchLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    launchButton->addChild(launchLabel);
    addChild(launchButton, Z_ORDER::Z_LAUNCH_BUTTON);
}

void NoticeDialog::showNoticeDeleteButton()
{
    auto deleteButton = ui::Button::create("gacha_dialog_button.png");
    deleteButton->setTag(TAG_SPRITE::DELETE_BUTTON);
    deleteButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    float deleteButtonPositionDeltaX = -20;
    if (getChildByTag(TAG_SPRITE::LAUNCH_BUTTON)) {
        deleteButtonPositionDeltaX = -91;
    }
    deleteButton->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() + Vec2(deleteButtonPositionDeltaX, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + 30));
    deleteButton->addTouchEventListener([this](Ref* pSender, ui::Widget::TouchEventType type) {
        if (type != ui::Widget::TouchEventType::ENDED) {
            return;
        }
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _deleteCallback();
        removeFromParent();
    });
    auto deleteLabel = Label::createWithTTF("削除", FONT_NAME_2, 32);
    deleteLabel->setPosition(Point(deleteButton->getContentSize().width / 2, deleteButton->getContentSize().height / 2 - 16));
    deleteLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    deleteButton->addChild(deleteLabel);
    addChild(deleteButton, Z_ORDER::Z_DELETE_BUTTON);
}

void NoticeDialog::showNoticeCloseButton()
{
    auto closeButton = ui::Button::create("gacha_dialog_button.png");
    closeButton->setTag(TAG_SPRITE::CLOSE_BUTTON);
    auto anchorPosition = Vec2::ANCHOR_BOTTOM_LEFT;
    float closeButtonPositionDeltaX = 20;
    if (getChildByTag(TAG_SPRITE::LAUNCH_BUTTON)) {
        closeButtonPositionDeltaX = 0;
        anchorPosition = Vec2::ANCHOR_MIDDLE_BOTTOM;
    }
    closeButton->setAnchorPoint(anchorPosition);
    closeButton->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() + Vec2(closeButtonPositionDeltaX, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + 30));
    closeButton->addTouchEventListener([this](Ref* pSender, ui::Widget::TouchEventType type) {
        if (type != ui::Widget::TouchEventType::ENDED) {
            return;
        }
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        _closeCallback();
        removeFromParent();
    });
    auto closeLabel = Label::createWithTTF("閉じる", FONT_NAME_2, 32);
    closeLabel->setPosition(Point(closeButton->getContentSize().width / 2, closeButton->getContentSize().height / 2 - 16));
    closeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    closeButton->addChild(closeLabel);
    addChild(closeButton, Z_ORDER::Z_CLOSE_BUTTON);
}
