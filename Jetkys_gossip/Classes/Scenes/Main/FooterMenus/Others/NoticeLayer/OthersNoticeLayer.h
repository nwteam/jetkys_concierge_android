#ifndef __syanago__OthersNoticeLayer__
#define __syanago__OthersNoticeLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Loading.h"
#include "DialogView.h"
#include "OthersLayer.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"
#include "create_func.h"
#include "show_head_line.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace SyanagoAPI;

class Cell;
class OthersLayer;

class OthersNoticeLayer : public Layer,
public cocos2d::extension::TableViewDataSource,
public cocos2d::extension::TableViewDelegate,
public DialogDelegate,
public create_func<OthersNoticeLayer>,
public show_head_line
{
public:
    OthersNoticeLayer();
    ~OthersNoticeLayer();
    typedef std::function<void()> OnTapBackButtonCallBack;
    typedef std::function<void()> NoticeBadgeUpdataCallBack;
    typedef std::function<void(std::string url)> LaunchBrowserCallBack;
    using create_func::create;
    bool init(const OnTapBackButtonCallBack& callback,
              const NoticeBadgeUpdataCallBack&  noticeBadgeUpdataCallback,
              const LaunchBrowserCallBack& launchBrowserCallback);
    
    void btMenuItemTableCallback(Ref *pSender);
    
    void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {}
    
    Cell * cellWithIdx(int idx);
    virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
    virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        NON_NOTICE_LIST_BASE,
        NON_NOTICE_LIST_LABEL,
        NOTICE_LIST,
        SCROLL_BAR,
        NOTICE_DIALOG,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_NON_NOTICE_LIST_BASE,
        Z_NON_NOTICE_LIST_LABEL,
        Z_NOTICE_LIST,
        Z_SCROLL_BAR,
        Z_NOTICE_DIALOG = 13,
    };
    enum SET_NOTICE_API_STATUS {
        NONE = 0,
        ALREADY_READ = 1,
        NOTICE_DELETE = 2,
    };
    DefaultProgress* _progress;
    RequestAPI* _request;
    OnTapBackButtonCallBack _callback;
    NoticeBadgeUpdataCallBack _noticeBadgeUpdataCallback;
    LaunchBrowserCallBack _launchBrowserCallback;
    SET_NOTICE_API_STATUS _setNoticeStatusFlag;
    std::vector<Json*> _notices;
    
    int _noticeIdx;
    int _numberOfCell;
    int _noticeNum;
    
    void showBackground();
    void showNoticeList();
    void responseGetNotice(Json* response);
    void showNoListLabel();
    void showTable();
    
    void getNotice(Ref* pSender);

    void responseSetNoticeStatus(Json* response);

    void btDialogCallback(ButtonIndex aIndex);
    void contentlayerClose();
};

#endif