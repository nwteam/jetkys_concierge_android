#include "OthersNoticeLayer.h"
#include "FontDefines.h"
#include "Cell.h"
#include "PlayerController.h"
#include "TelopScrollView.h"
#include "OthersMainLayer.h"
#include "HeaderStatus.h"
#include "TimeConverter.h"
#include "NoticeDialog.h"
#include "ScrollBar.h"

#define SIZE_PLAYER_CELL Size(581, 120)

OthersNoticeLayer::OthersNoticeLayer():
    _progress(nullptr), _request(nullptr), _callback(nullptr), _noticeBadgeUpdataCallback(nullptr), _launchBrowserCallback(nullptr)
{
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}
OthersNoticeLayer::~OthersNoticeLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

bool OthersNoticeLayer::init(const OnTapBackButtonCallBack& callback,
                             const NoticeBadgeUpdataCallBack& noticeBadgeUpdataCallback,
                             const LaunchBrowserCallBack& launchBrowserCallback)
{
    if (!Layer::create()) {
        return false;
    }
    _callback = callback;
    _noticeBadgeUpdataCallback = noticeBadgeUpdataCallback;
    _launchBrowserCallback = launchBrowserCallback;

    showBackground();
    showHeadLine(this, "運営からのお知らせ", [this](Ref* pSender) {
        _callback();
    });

    showNoticeList();
    return true;
}

void OthersNoticeLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getVisibleSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void OthersNoticeLayer::showNoticeList()
{
    _noticeNum = -1;
    _noticeIdx = -1;
    _numberOfCell = 0;
    _setNoticeStatusFlag = SET_NOTICE_API_STATUS::NONE;
    _notices = std::vector<Json*>();

    _progress->onStart();
    _request->getNotice(CC_CALLBACK_1(OthersNoticeLayer::responseGetNotice, this));
}

void OthersNoticeLayer::responseGetNotice(Json* response)
{
    std::string result = Json_getString(Json_getItem(response, "get_notice"), "result", "false");
    if (result == "true") {
        Json* data = Json_getItem(Json_getItem(response, "get_notice"), "data");
        Json* mDatas = Json_getItem(data, "notice");
        if (mDatas && mDatas->type == Json_Array) {
            for (Json* child = mDatas->child; child; child = child->next) {
                _notices.push_back(child);
            }
        }
        if (_notices.size() == 0) {
            showNoListLabel();
        } else {
            showTable();
        }
    }
}

void OthersNoticeLayer::showNoListLabel()
{
    auto background = makeSprite("dialog_base.png");
    background->setTag(TAG_SPRITE::NON_NOTICE_LIST_BASE);
    Size winSize = Director::getInstance()->getWinSize();
    background->setPosition(winSize / 2);
    addChild(background, Z_ORDER::Z_NON_NOTICE_LIST_BASE);
    auto resultLabel = Label::createWithTTF("お知らせはありません", FONT_NAME_2, 40);
    resultLabel->setTag(TAG_SPRITE::NON_NOTICE_LIST_LABEL);
    resultLabel->setPosition(Point(winSize.width / 2, (winSize.height / 2 - 20)));
    resultLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(resultLabel, Z_ORDER::Z_NON_NOTICE_LIST_LABEL);
}

void OthersNoticeLayer::showTable()
{
    auto layer = Layer::create();
    layer->setTag(TAG_SPRITE::NOTICE_LIST);
    _numberOfCell = (int)_notices.size();
    Size winSize = Director::getInstance()->getWinSize();
    TableView* tableView = TableView::create(this, Size(SIZE_PLAYER_CELL.width + 10, SIZE_PLAYER_CELL.height * 5));
    tableView->setDirection(ScrollView::Direction::VERTICAL);
    tableView->setContentSize(Size(SIZE_PLAYER_CELL.width, SIZE_PLAYER_CELL.height * (int)_notices.size()));
    if (tableView->getViewSize().height > SIZE_PLAYER_CELL.height * (int)_notices.size()) {
        tableView->setViewSize(tableView->getContentSize());
    }
    tableView->ignoreAnchorPointForPosition(false);
    tableView->setAnchorPoint(Point(0.5, 1));
    tableView->setPosition(Point(winSize.width / 2, winSize.height * 0.75));
    tableView->setBounceable(false);
    tableView->setDelegate(this);
    layer->addChild(tableView);
    auto scrollBar = ScrollBar::initScrollBar(winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 180 - 30 - 28, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 28, tableView->getContainer()->getContentSize().height, tableView->getContentOffset().y);
    scrollBar->setTag(TAG_SPRITE::SCROLL_BAR);
    scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 15 - 28 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    layer->addChild(scrollBar, Z_ORDER::Z_SCROLL_BAR);
    if (winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 28 <= tableView->getContainer()->getContentSize().height) {
        tableView->setPosition(tableView->getPosition() + Point(-20, 0));
    }
    tableView->reloadData();
    addChild(layer, Z_ORDER::Z_NOTICE_LIST);
}

Size OthersNoticeLayer::tableCellSizeForIndex(TableView* table, ssize_t idx)
{
    return SIZE_PLAYER_CELL;
}

TableViewCell* OthersNoticeLayer::tableCellAtIndex(TableView* table, ssize_t idx)
{
    Cell* cell = (Cell*)table->dequeueCell();
    if (cell) {
        cell->removeFromParent();
    }
    cell = cellWithIdx((int)idx);
    return cell;
}

ssize_t OthersNoticeLayer::numberOfCellsInTableView(TableView* table)
{
    return _numberOfCell + 2;
}

Cell* OthersNoticeLayer::cellWithIdx(int tempIdx)
{
    auto cell = new Cell();
    if (tempIdx > 1) {
        int idx = tempIdx - 2;
        _noticeIdx = (_numberOfCell + 2) - 1 - (idx + 2);
        Json* noticeJson = _notices.at(_noticeIdx);

        // セルを作成
        // 背景を表示
        auto bg = makeSprite("friend_notice_list_base.png");
        cell->addChild(bg);
        bg->setPosition(Vec2(SIZE_PLAYER_CELL.width / 2, SIZE_PLAYER_CELL.height / 2));
        // NEWバッチを表示
        if (atoi(Json_getString(noticeJson, "new", "0"))) {
            auto newBadge = Label::createWithTTF("NEW", FONT_NAME_2, 18);
            cell->addChild(newBadge);
            newBadge->setPosition(Point(bg->getPositionX() + bg->getContentSize().width / 2 - newBadge->getContentSize().width / 2 - 20,
                                        bg->getPositionY() + bg->getContentSize().height / 2 - newBadge->getContentSize().height / 2 - 20));
            newBadge->setColor(Color3B(255, 173, 194));
        }

        // 件名を表示
        TelopScrollView* telopView = TelopScrollView::initScrollTelop(Json_getString(noticeJson, "subject", ""),
                                                                      28,
                                                                      bg->getContentSize().width - 100,
                                                                      30,
                                                                      Color3B(255, 231, 117));
        telopView->setFontShadow(Color3B::BLACK, 2);
        cell->addChild(telopView);
        telopView->setAnchorPoint(Vec2(0, 0.5));
        telopView->setPosition(Point(25.0f, SIZE_PLAYER_CELL.height * 0.7 - 19));

        // お知らせ受信日時（表示開始日時）を表示
        auto lastLoginLabel = Label::createWithTTF(TimeConverter::convertLastLogInTimeString(Json_getString(noticeJson, "received_date", "")), FONT_NAME_2, 22);
        lastLoginLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
        cell->addChild(lastLoginLabel);
        lastLoginLabel->setAnchorPoint(Vec2(0, 0));
        lastLoginLabel->setPosition(Point(25.0f, SIZE_PLAYER_CELL.height * 0.2 - 22));


        auto getNoticeBtn = MenuItemImage::create("friend_getNotice_button.png", "friend_getNotice_button.png", CC_CALLBACK_1(OthersNoticeLayer::getNotice, this));
        getNoticeBtn->setTag(idx);
        // getNoticeBtn->setPosition(Point(25.0f,SIZE_PLAYER_CELL.height * 0.2 - 22));
        auto menu = MenuTouch::create(getNoticeBtn, NULL);
        cell->addChild(menu, 4);
        menu->setPosition(Point(bg->getPositionX() + bg->getContentSize().width / 2 - getNoticeBtn->getContentSize().width / 2 - 10,
                                bg->getPositionY() - bg->getContentSize().height / 2 + getNoticeBtn->getContentSize().height / 2 + 5));
        auto getLabel = Label::createWithTTF("受信", FONT_NAME_2, 23);
        getNoticeBtn->addChild(getLabel);
        getLabel->setPosition(Point(getNoticeBtn->getContentSize().width / 2, getNoticeBtn->getContentSize().height / 2 - 11.5));


        // 既読ならセルを暗くする
        if (atoi(Json_getString(noticeJson, "already_read", "0")) == 1) {
            auto darkFilter = Sprite::create("friend_notice_list_base.png");
            darkFilter->setColor(Color3B::BLACK);
            darkFilter->setOpacity(100);
            darkFilter->setPosition(bg->getPosition());
            cell->addChild(darkFilter, 3);
        }
        cell->setTag(_noticeIdx);
        cell->autorelease();
    }
    return cell;
}

void OthersNoticeLayer::getNotice(Ref* pSender)
{
    _noticeNum = _numberOfCell - 1 - ((MenuItemSprite*)pSender)->getTag();
    _setNoticeStatusFlag = SET_NOTICE_API_STATUS::ALREADY_READ;
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    _progress->onStart();
    _request->setNoticeStatus(atoi(Json_getString(_notices.at(_noticeNum), "notice_id", "-1")), 1, CC_CALLBACK_1(OthersNoticeLayer::responseSetNoticeStatus, this));
}

void OthersNoticeLayer::responseSetNoticeStatus(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "set_notice_status");
    std::string result = Json_getString(jsonData1, "result", "false");
    if (result == "true") {
        if (_setNoticeStatusFlag == SET_NOTICE_API_STATUS::ALREADY_READ) {
            auto noticeDialog = NoticeDialog::create(_notices.at(_noticeNum),
                                                     [this]() {
                auto dialog = DialogView::createWithShortMessage("本当に削除しますか？", 2);
                addChild(dialog, DIALOG_ZORDER);
                dialog->_delegate  = this;
            },
                                                     [this]() {
                contentlayerClose();
            },
                                                     [this](const std::string url) {
                _launchBrowserCallback(url);
            });
            noticeDialog->setTag(TAG_SPRITE::NOTICE_DIALOG);
            addChild(noticeDialog, Z_ORDER::Z_NOTICE_DIALOG);
        } else if (_setNoticeStatusFlag == SET_NOTICE_API_STATUS::NOTICE_DELETE) {
            contentlayerClose();
        }
        if (PLAYERCONTROLLER->_numberOfNotice > 0) {
            PLAYERCONTROLLER->_numberOfNotice--;
        }
        _noticeBadgeUpdataCallback();
    }
}

void OthersNoticeLayer::tableCellTouched(TableView* table, TableViewCell* cell)
{}

void OthersNoticeLayer::btDialogCallback(ButtonIndex aIndex)
{
    if (aIndex == BT_YES) {
        _setNoticeStatusFlag = SET_NOTICE_API_STATUS::NOTICE_DELETE;
        _progress->onStart();
        _request->setNoticeStatus(atoi(Json_getString(_notices.at(_noticeNum), "notice_id", "-1")), 2, CC_CALLBACK_1(OthersNoticeLayer::responseSetNoticeStatus, this));
    }
}

void OthersNoticeLayer::contentlayerClose()
{
    removeChildByTag(TAG_SPRITE::NOTICE_LIST);
    showNoticeList();
}

void OthersNoticeLayer::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
    if (!getChildByTag(TAG_SPRITE::NOTICE_LIST)) {
        return;
    }
    auto scrollBar = static_cast<ScrollBar*>(getChildByTag(TAG_SPRITE::NOTICE_LIST)->getChildByTag(TAG_SPRITE::SCROLL_BAR));
    if (scrollBar) {
        scrollBar->updateScrollPoint(view->getContentOffset().y);
    }
}