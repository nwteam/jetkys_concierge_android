#include "OthersLayer.h"
#include "jCommon.h"
#include "FontDefines.h"
#include "OthersMainLayer.h"
#include "OthersEventLayer.h"
#include "OthersColectionLayer.h"
#include "OthersTreasuresLayer.h"
#include "OthersOptionLayer.h"
#include "OthersHelpLayer.h"
#include "OthersBoughtLogLayer.h"
#include "OthersContactLayer.h"
#include "OthersTransferCodeLayer.h"
#include "OthersNoticeLayer.h"
#include "MainScene.h"
#include "BrowserLauncher.h"
#include "OthersPresentLayer.h"
#include "HeaderStatus.h"
#include "UserDefaultManager.h"
#include "TitleScene.h"
#include "SystemSettingModel.h"

#include "OthersCallbackWebURLModel.h"
#include "OthersCallbackHtmlModel.h"

#include "OthersColectionCallbackModel.h"

#define tag_dialog_rule 1
#define tag_dialog_settlement 2
#define tag_dialog_campaign 3
#define tag_dialog_notice 4

bool OthersLayer::init()
{
    if (!Layer::init()) {
        return false;
    }

    _othersIndex = OI_MAIN;

    Size winSize = Director::getInstance()->getWinSize();

    showLayer(OI_MAIN);
    return true;
}

void OthersLayer::resetPageTag()
{
    removeAllChildren();
    init();
}

void OthersLayer::showLayer(OthersIndex nextLayer)
{
    if (nextLayer == _othersIndex && getChildByTag(_othersIndex)) {
        return;
    }
    auto currentLayer = getChildByTag(_othersIndex);
    if (currentLayer && _othersIndex != OI_MAIN) {
        currentLayer->removeFromParent();
    }

    if (getChildByTag(OI_MAIN)) {
        getChildByTag(OI_MAIN)->setVisible(false);
    }
    if (_othersIndex == OI_TREASURES) {
        _mainScene->footerLayer->setVisible(true);
        _mainScene->headerLayer->setVisible(true);
    }

    _othersIndex = nextLayer;
    switch (_othersIndex) {
    case OI_MAIN: {
        if (!getChildByTag(OI_MAIN)) {
            auto layer = OthersMainLayer::create();
            addChild(layer);
            layer->setTag(OI_MAIN);
            layer->_othersLayer = this;
        } else {
            getChildByTag(OI_MAIN)->setVisible(true);
        }

        break;
    }
    case OI_INFOMATION: {
        getChildByTag(OI_MAIN)->setVisible(true);
        showHtmldialog();
        break;
    }
    case OI_EVENT: {
        std::shared_ptr<OthersCallbackWebURLModel>callbackModel(new OthersCallbackWebURLModel(CC_CALLBACK_0(OthersLayer::onTapPageBack,this),
                                                                                      CC_CALLBACK_1(OthersLayer::showUrlWebView, this),
                                                                                      CC_CALLBACK_0(OthersLayer::deleteWebView, this),
                                                                                      CC_CALLBACK_0(OthersLayer::removeBlackImage, this)));
        auto layer = OthersEventLayer::create(callbackModel);
        addChild(layer);
        layer->setTag(OI_EVENT);
        break;
    }
    case OI_COLECTION: {
        std::shared_ptr<OthersColectionCallbackModel>callbackModel(new OthersColectionCallbackModel(CC_CALLBACK_0(OthersLayer::onTapPageBack,this),
                                                                                                    CC_CALLBACK_0(OthersLayer::viewMenu, this),
                                                                                                    CC_CALLBACK_0(OthersLayer::hiddenMenu, this)));
        
        auto layer = OthersColectionLayer::create(callbackModel);
        addChild(layer);
        layer->setTag(OI_COLECTION);
        break;
    }
    case OI_TREASURES: {
        auto layer = OthersTreasuresLayer::create(CC_CALLBACK_0(OthersLayer::onTapPageBack,this));
        _mainScene->footerLayer->setVisible(false);
        _mainScene->headerLayer->setVisible(false);
        addChild(layer);
        layer->setTag(OI_TREASURES);
        break;
    }
    case OI_CAMPAIGN: {
        auto dialog = DialogView::createBrowserConfirm(tag_dialog_campaign);
        addChild(dialog, LOADING_ZORDER);
        dialog->_delegate = this;

        getChildByTag(OI_MAIN)->setVisible(true);
        break;
    }
    case OI_OPTION: {
        auto layer = OthersOptionLayer::create(CC_CALLBACK_0(OthersLayer::onTapPageBack,this));
        addChild(layer);
        layer->setTag(OI_OPTION);
        break;
    }
    case OI_HELP: {
        std::shared_ptr<OthersCallbackWebURLModel>callbackModel(new OthersCallbackWebURLModel(CC_CALLBACK_0(OthersLayer::onTapPageBack,this),
                                                                                              CC_CALLBACK_1(OthersLayer::showUrlWebView, this),
                                                                                              CC_CALLBACK_0(OthersLayer::deleteWebView, this),
                                                                                              CC_CALLBACK_0(OthersLayer::removeBlackImage, this)));
        auto layer = OthersHelpLayer::create(callbackModel);
        addChild(layer);
        layer->setTag(OI_HELP);
        break;
    }
    case OI_BUYLOG: {
        std::shared_ptr<OthersCallbackHtmlModel>callbackModel(new OthersCallbackHtmlModel(CC_CALLBACK_0(OthersLayer::onTapPageBack,this),
                                                                                          CC_CALLBACK_1(OthersLayer::showHtmlTagWebView, this),
                                                                                          CC_CALLBACK_0(OthersLayer::deleteWebView, this),
                                                                                          CC_CALLBACK_0(OthersLayer::removeBlackImage, this)));
        auto layer = OthersBoughtLogLayer::create(callbackModel);
        addChild(layer);
        layer->setTag(OI_BUYLOG);
        break;
    }
    case OI_CONTACT: {
        std::shared_ptr<OthersCallbackHtmlModel>callbackModel(new OthersCallbackHtmlModel(CC_CALLBACK_0(OthersLayer::onTapPageBack,this),
                                                                                          CC_CALLBACK_1(OthersLayer::showHtmlTagWebView, this),
                                                                                          CC_CALLBACK_0(OthersLayer::deleteWebView, this),
                                                                                          CC_CALLBACK_0(OthersLayer::removeBlackImage, this)));
        auto layer = OthersContactLayer::create(callbackModel);
        addChild(layer);
        layer->setTag(OI_CONTACT);
    
        break;
    }
    case OI_MODELCHANGE: {
        std::shared_ptr<OthersCallbackHtmlModel>callbackModel(new OthersCallbackHtmlModel(CC_CALLBACK_0(OthersLayer::onTapPageBack,this),
                                                                                          CC_CALLBACK_1(OthersLayer::showHtmlTagWebView, this),
                                                                                          CC_CALLBACK_0(OthersLayer::deleteWebView, this),
                                                                                          CC_CALLBACK_0(OthersLayer::removeBlackImage, this)));
        auto layer = OthersTransferCodeLayer::create(callbackModel);
        addChild(layer);
        layer->setTag(OI_MODELCHANGE);
        break;
    }
    case OI_RULE: {
        auto dialog = DialogView::createBrowserConfirm(tag_dialog_rule);
        addChild(dialog, LOADING_ZORDER);
        dialog->_delegate = this;
        getChildByTag(OI_MAIN)->setVisible(true);
        break;
    }
    case OI_SETTLEMENT: {
        auto dialog = DialogView::createBrowserConfirm(tag_dialog_settlement);
        addChild(dialog, LOADING_ZORDER);
        dialog->_delegate = this;

        getChildByTag(OI_MAIN)->setVisible(true);
        break;
    }
    case OI_NOTICE: {
        auto layer = OthersNoticeLayer::create(CC_CALLBACK_0(OthersLayer::onTapPageBack,this),
                                               CC_CALLBACK_0(OthersLayer::updateBage, this),
                                               CC_CALLBACK_1(OthersLayer::showNoticeLunchBrowser, this));
        addChild(layer);
        layer->setTag(OI_NOTICE);
        break;
    }
    case OI_PRESENT: {
        auto layer = OthersPresentLayer::create(CC_CALLBACK_0(OthersLayer::onTapPageBack,this));
        addChild(layer);
        layer->setTag(OI_PRESENT);
        layer->_othersLayer = this;
        break;
    }

    case OI_BACKTITLE: {
        transitScene(TitleScene::createScene());
        break;
    }
    case OI_TUTOLI: {
        UserDefault::getInstance()->setBoolForKey("NewPlayer", false);
        UserDefault::getInstance()->setBoolForKey("Tutorial", false);
        UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::OPEN_GACHA, false);

//            showLayer(OI_MAIN);
        getChildByTag(OI_MAIN)->setVisible(true);
        // ダイアログを表示し→[はい]でチュートリアル開始
        _mainScene->showHomeLayer();
        _mainScene->startTutorial();
        break;
    }
    default:
        showLayer(OI_MAIN);
        break;
    }
}

void OthersLayer::hiddenMenu()
{
    _mainScene->footerLayer->setVisible(false);
    _mainScene->headerLayer->setVisible(false);
}

void OthersLayer::viewMenu()
{
    _mainScene->footerLayer->setVisible(true);
    _mainScene->headerLayer->setVisible(true);
}

void OthersLayer::showHtmlTagWebView(std::string htmlTag)
{
    _mainScene->setBlackImage();
    _mainScene->showHtmlTagWebView(htmlTag);
}

void OthersLayer::updateInfoLayer()
{
    _mainScene->headerLayer->updateInfoLayer();
}

void OthersLayer::showUrlWebView(OtyersUrlWebViewTag tag)
{
    _mainScene->setBlackImage();
    switch (tag) {
    case OI_WEB_EVENT:
        _mainScene->showUrlWebView(WEB_EVENT);
        break;
    case OI_WEB_HELP:
        _mainScene->showUrlWebView(WEB_HELP);
        break;
    default:
        break;
    }
}

void OthersLayer::deleteWebView()
{
    _mainScene->deleteWebView();
}

void OthersLayer::showHtmldialog()
{
    _mainScene->showHtmldialog();
}

void OthersLayer::updateBage()
{
    _mainScene->footerLayer->setBage();
}

OthersIndex OthersLayer::getNowViewOthersLayer()
{
    return _othersIndex;
}

void OthersLayer::showNoticeLunchBrowser(std::string url)
{
    _url = url;
    auto dialog = DialogView::createBrowserConfirm(tag_dialog_notice);
    addChild(dialog, LOADING_ZORDER);
    dialog->_delegate = this;
}

void OthersLayer::btDialogCallback(ButtonIndex aIndex, int tag)
{
    if (aIndex == BT_YES) {
        std::string url;
        if (tag == tag_dialog_rule) {
            url = "http://syanago.com/other/contact.html";
            Cocos2dExt::BrowserLauncher::launchUrl(url.c_str());
        } else if (tag == tag_dialog_settlement) {
            std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
            url = systemSettingModel->getSettlementServerUrl();
            Cocos2dExt::BrowserLauncher::launchUrl(url.c_str());
        } else if (tag == tag_dialog_campaign) {
            url = UserDefault::getInstance()->getStringForKey("domein") + "/webview/campaign_form";
            Cocos2dExt::BrowserLauncher::launchUrl(url.c_str());
        } else if (tag == tag_dialog_notice) {
            Cocos2dExt::BrowserLauncher::launchUrl(_url.c_str());
        }
    }
}

void OthersLayer::removeBlackImage()
{
    _mainScene->removeChildByTag(90);
}

void OthersLayer::onTapPageBack()
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    showLayer(OI_MAIN);
}
