#include "OthersPresentLayer.h"
#include "FontDefines.h"
#include "Cell.h"
#include "PlayerController.h"
#include "TelopScrollView.h"
#include "OthersMainLayer.h"
#include "HeaderStatus.h"
#include "TimeConverter.h"
#include "PresentDialog.h"

#define SIZE_PLAYER_CELL Size(581, 120)

OthersPresentLayer::OthersPresentLayer():
    progress(nullptr), request(nullptr), _numberOfCell(0), _callback(nullptr)
{
    _notices.clear();
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
}

OthersPresentLayer::~OthersPresentLayer()
{
    delete progress;
    delete request;
    progress = nullptr;
    request = nullptr;
}

bool OthersPresentLayer::init(const OnTapBackButtonCallBack& callback)
{
    if (!Layer::create()) {
        return false;
    }
    _callback = callback;
    _winSize = Director::getInstance()->getWinSize();

    showBackground();
    showHeadLine(this, "プレゼントBOX", [this](Ref* pSender) {
        _callback();
    });

    initLayer();
    return true;
}

void OthersPresentLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getVisibleSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void OthersPresentLayer::initLayer()
{
    _numberOfCell = 0;
    _notices = std::vector<Json*>();
    progress->onStart();
    request->getPresentList(CC_CALLBACK_1(OthersPresentLayer::responseGetPresentList, this));
}

void OthersPresentLayer::responseGetPresentList(Json* response)
{
    std::string result = Json_getString(Json_getItem(response, "get_compensation"), "result", "false");
    if (result == "true") {
        Json* compensationJsons = Json_getItem(Json_getItem(Json_getItem(response, "get_compensation"), "data"), "compensation");
        if (compensationJsons && compensationJsons->type == Json_Array) {
            Json* child;
            for (child = compensationJsons->child; child; child = child->next) {
                _notices.push_back(child);
            }
            if (getChildByTag(TAG_SPRITE::HOLD_BASE)) {
                removeChildByTag(TAG_SPRITE::HOLD_BASE);
            }
            if (getChildByTag(TAG_SPRITE::HOLD_LABEL)) {
                removeChildByTag(TAG_SPRITE::HOLD_LABEL);
            }

            auto noteLabel = Label::createWithTTF("※50通を超えた分は自動的に削除されます。", FONT_NAME_2, 20);
            noteLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
            noteLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
            noteLabel->setPosition(Vec2(20, _winSize.height - 32.5 - HEADER_STATUS::SIZE::HEIGHT - 12 - 65 - 15));
            noteLabel->setColor(Color3B(0, 0, 0));
            addChild(noteLabel, Z_LABEL);

            auto bg = makeSprite("presentbox_bg1.png");
            bg->setTag(TAG_SPRITE::HOLD_BASE);
            bg->setPosition(Vec2(_winSize.width, _winSize.height * 0.75 + 15));
            bg->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
            addChild(bg, Z_ORDER::Z_HODL_BACKGROUND);

            std::string s = std::string("受信数") + StringUtils::format("%lu", _notices.size())  + std::string("/50");
            auto receivedCountLabel = Label::createWithTTF(s, FONT_NAME_2, 20);
            receivedCountLabel->setTag(TAG_SPRITE::HOLD_LABEL);
            receivedCountLabel->setColor(Color3B(255, 255, 255));
            receivedCountLabel->setPosition(bg->getPosition() + Vec2(-15, -15));
            receivedCountLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
            addChild(receivedCountLabel, Z_LABEL);

            showTable();
        }
        if (_notices.size() == 0) {
            showNoListLabel();
        }
    }
}
void OthersPresentLayer::showTable()
{
    _layer = Layer::create();
    addChild(_layer, Z_ORDER::Z_CONTENT);

    _numberOfCell = (int)_notices.size();

    TableView* tableView = TableView::create(this, Size(SIZE_PLAYER_CELL.width, _winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 28));
    tableView->setDirection(ScrollView::Direction::VERTICAL);
    tableView->setPosition(Point(_winSize.width / 2 - SIZE_PLAYER_CELL.width * 0.5,
                                 0));
    tableView->setDelegate(this);
    tableView->setBounceable(false);
    _layer->addChild(tableView);
    _scrollBar = ScrollBar::initScrollBar(_winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 180 - 30 - 28, _winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 28, tableView->getContainer()->getContentSize().height, tableView->getContentOffset().y);
    _layer->addChild(_scrollBar);
    _scrollBar->setPosition(Point(_winSize.width - ((_winSize.width / 2) - (567 / 2)) - 10, _winSize.height / 2 - 15 - 28 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    if (_winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 28 <= tableView->getContainer()->getContentSize().height) {
        tableView->setPosition(tableView->getPosition() + Point(-20, 0));
    }
    tableView->reloadData();
}

Size OthersPresentLayer::tableCellSizeForIndex(TableView* table, ssize_t idx)
{
    return SIZE_PLAYER_CELL;
}

ssize_t OthersPresentLayer::numberOfCellsInTableView(TableView* table)
{
    return _numberOfCell + 2;
}

TableViewCell* OthersPresentLayer::tableCellAtIndex(TableView* table, ssize_t idx)
{
    Cell* cell = (Cell*)table->dequeueCell();
    if (cell)
        cell->removeFromParent();
    cell = cellWithIdx((int)idx);
    return cell;
}

Cell* OthersPresentLayer::cellWithIdx(int tempIdx)
{
    auto cell = new Cell();
    if (tempIdx > 1) {
        int idx = tempIdx - 2;
        auto noticeIdx = (_numberOfCell + 2) - 1 - (idx + 2);
        Json* noticeJson = _notices.at(noticeIdx);
        auto bg = makeSprite("friend_notice_list_base.png");
        cell->addChild(bg);
        bg->setPosition(Vec2(SIZE_PLAYER_CELL.width / 2, SIZE_PLAYER_CELL.height / 2));

        // NEWバッチを表示
        if (atoi(Json_getString(noticeJson, "new", "0"))) {
            auto newBadge = Label::createWithTTF("NEW", FONT_NAME_2, 18);
            cell->addChild(newBadge);
            newBadge->setPosition(Point(bg->getPositionX() + bg->getContentSize().width / 2 - newBadge->getContentSize().width / 2 - 20,
                                        bg->getPositionY() + bg->getContentSize().height / 2 - newBadge->getContentSize().height / 2 - 20));
            newBadge->setColor(Color3B(255, 173, 194));
        }

        // 件名を表示
        auto subjectBg = makeSprite("presentbox_bg2.png");
        subjectBg->setPosition(Point(18.0f, SIZE_PLAYER_CELL.height * 0.7 - 30));
        subjectBg->setAnchorPoint(Vec2(0, 0));
        cell->addChild(subjectBg);

        //    //CCLOG("Title: %s", Json_getString(noticeJson, "title", ""));
        std::string subjectText = Json_getString(noticeJson, "title", "");
        TelopScrollView* telopView = TelopScrollView::initScrollTelop(subjectText,
                                                                      28,
                                                                      subjectBg->getContentSize().width,
                                                                      subjectBg->getContentSize().height,
                                                                      Color3B(147, 12, 8));
        subjectBg->addChild(telopView);
        telopView->setPosition(Point(0, 8));

        // 受け取り終了日時
        std::string endGetTime;
        int ret = strcmp(Json_getString(noticeJson, "received_date", ""), "null");
        if (ret == 0) {
            endGetTime = "";
        } else {
            endGetTime += getLastTime(noticeJson);
        }
        auto lastLoginLabel = Label::createWithTTF(endGetTime, FONT_NAME_2, 22);
        lastLoginLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
        cell->addChild(lastLoginLabel);
        lastLoginLabel->setAnchorPoint(Vec2(0, 0));
        lastLoginLabel->setPosition(Point(25.0f, SIZE_PLAYER_CELL.height * 0.2 - 22));


        auto getNoticeBtn = MenuItemImage::create("friend_getNotice_button.png", "friend_getNotice_button.png", CC_CALLBACK_1(OthersPresentLayer::getNotice, this));
        getNoticeBtn->setTag(idx);
        // getNoticeBtn->setPosition(Point(25.0f,SIZE_PLAYER_CELL.height * 0.2 - 22));
        auto menu = MenuTouch::create(getNoticeBtn, NULL);
        cell->addChild(menu);
        menu->setPosition(Point(bg->getPositionX() + bg->getContentSize().width / 2 - getNoticeBtn->getContentSize().width / 2 - 10,
                                bg->getPositionY() - bg->getContentSize().height / 2 + getNoticeBtn->getContentSize().height / 2 + 5));
        auto getLabel = Label::createWithTTF("受信", FONT_NAME_2, 23);
        getNoticeBtn->addChild(getLabel);
        getLabel->setPosition(Point(getNoticeBtn->getContentSize().width / 2, getNoticeBtn->getContentSize().height / 2 - 11.5));
        getLabel->enableShadow();

        cell->setTag(noticeIdx);

        cell->autorelease();
    }
    return cell;
}

void OthersPresentLayer::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
    _scrollBar->updateScrollPoint(view->getContentOffset().y);
}

void OthersPresentLayer::showNoListLabel()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto bgground = makeSprite("dialog_base.png");
    addChild(bgground);
    bgground->setPosition(winSize / 2);
    auto resultLabel = Label::createWithTTF("プレゼントはありません", FONT_NAME_2, 40);
    resultLabel->setPosition(Point(winSize.width / 2, (winSize.height / 2 - 20)));
    resultLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(resultLabel);
}

void OthersPresentLayer::getNotice(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    showPresentDialog((_numberOfCell - ((MenuItemSprite*)pSender)->getTag()) - 1);
}

void OthersPresentLayer::showPresentDialog(const int tag)
{
    auto dialog = PresentDialog::create(_notices.at(tag), CC_CALLBACK_1(OthersPresentLayer::PresentDialogCallback, this));
    addChild(dialog, Z_DIALOG);
}

void OthersPresentLayer::PresentDialogCallback(const int presentId)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    stopAllActions();
    progress->onStart();
    request->setPresentStatus(presentId, CC_CALLBACK_1(OthersPresentLayer::responseGetPresent, this));
}

void OthersPresentLayer::responseGetPresent(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "set_compensation_status");
    std::string result = Json_getString(jsonData1, "result", "false");
    if (result == "true") {
        Json* jsonData2 = Json_getItem(jsonData1, "data");

        Json* jsonPlayer = Json_getItem(jsonData2, "players");
        if (jsonPlayer != NULL) {
            Json* jsonToken = Json_getItem(jsonPlayer, "free_token");
            if (jsonToken != NULL) {
                PLAYERCONTROLLER->_player->setFreeToken(atoi(Json_getString(jsonPlayer, "free_token", "0")));
            }
            Json* jsonCoin = Json_getItem(jsonPlayer, "free_token");
            if (jsonCoin != NULL) {
                PLAYERCONTROLLER->_player->setFreeCoin(atoi(Json_getString(jsonPlayer, "free_coin", "0")));
            }
            Json* jsonFriendPoint = Json_getItem(jsonPlayer, "friend_point");
            if (jsonFriendPoint != NULL) {
                PLAYERCONTROLLER->_player->setFriendPoint(atoi(Json_getString(jsonPlayer, "friend_point", "0")));
            }
        }
        Json* jsonDataChara = Json_getItem(jsonData2, "player_characters");
        if (jsonDataChara != NULL) {
            for (Json* child = jsonDataChara->child; child; child = child->next) {
                showGetContentEddect(DROP_CHARACTER, atoi(Json_getString(child, "characters_id", "")));
                PLAYERCONTROLLER->addCharacter(child, false, true);
            }
        }

        Json* jsonDataParts = Json_getItem(jsonData2, "player_parts");
        if (jsonDataParts != NULL) {
            Json* child = nullptr;
            for (child = jsonDataParts->child; child; child = child->next) {
                showGetContentEddect(DROP_PART, atoi(Json_getString(child, "parts_id", "")));
                PLAYERCONTROLLER->addPart(atoi(Json_getString(child, "id", "")),
                                          atoi(Json_getString(child, "player_id", "")),
                                          atoi(Json_getString(child, "parts_id", "")),
                                          Json_getString(child, "created", ""),
                                          Json_getString(child, "modified", ""));
            }
        }
        Json* jsonItemUnits = Json_getItem(jsonData2, "player_item_units");
        if (jsonItemUnits != NULL) {
            Json* child = nullptr;
            for (child = jsonItemUnits->child; child;
                 child = child->next) {
                PLAYERCONTROLLER->addItemUnit(
                    atoiNull(Json_getString(child, "player_id", "")),
                    atoiNull(Json_getString(child, "item_unit_id", "")),
                    atoiNull(Json_getString(child, "item_unit_kind", "")),
                    Json_getString(child, "created", ""),
                    Json_getString(child, "modified", ""));
            }
        }
        contentlayerClose();
        _othersLayer->updateInfoLayer();
        if (PLAYERCONTROLLER->_numberOfCompensation > 0) {
            PLAYERCONTROLLER->_numberOfCompensation--;
            dynamic_cast<OthersMainLayer*>(_othersLayer->getChildByTag(OI_MAIN))->updateBage();
        }
        _othersLayer->updateBage();
    }
}

void OthersPresentLayer::showGetContentEddect(const GET_TYPE type, const int masterId)
{
    _othersLayer->hiddenMenu();
    auto effectDialog = DropEffectLayer::create(type, masterId, false, [this]() {
        _othersLayer->viewMenu();
    });
    addChild(effectDialog, Z_ORDER::Z_GET_EFFECT);
}

void OthersPresentLayer::contentlayerClose()
{
    _layer->removeFromParent();
    initLayer();
}

std::string OthersPresentLayer::getLastTime(Json* json)
{
    std::string stdLoginTime = Json_getString(json, "received_date", "");
    return TimeConverter::convertLastLogInTimeString(stdLoginTime);
}
