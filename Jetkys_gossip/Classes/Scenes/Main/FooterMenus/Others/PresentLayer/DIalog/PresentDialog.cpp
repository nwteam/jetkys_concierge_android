#include "PresentDialog.h"
#include "FontDefines.h"
#include "SoundHelper.h"
#include "TelopScrollView.h"


PresentDialog::PresentDialog() :
	_callback(nullptr),
	_presentId(0) {
}

PresentDialog::~PresentDialog() {
}

bool PresentDialog::init(Json *presentInfomationJson, const GetPresentCallback& callback) {
	if (!Layer::create()) {
		return false;
	}
	_callback = callback;
	_presentId = atoi(Json_getString(presentInfomationJson, "compensation_id", "-1"));

	EventListenerTouchOneByOne *_listener = EventListenerTouchOneByOne::create();
	_listener->setSwallowTouches(true);
	_listener->onTouchBegan = [&](Touch *touch, Event *unused_event) -> bool {
			return true;
		};
	_listener->onTouchMoved = [&](Touch *touch, Event *unused_event) {
		};
	_listener->onTouchCancelled = [&](Touch *touch, Event *unused_event) {
		};
	_listener->onTouchEnded = [&](Touch *touch, Event *unused_event) {
		};
	getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

	showBackground();
	showContent(presentInfomationJson);
	showButton();

	return true;
}

void PresentDialog::showBackground() {
	auto black = LayerColor::create(Color4B(Color3B::BLACK));
	black->setOpacity(180);
	black->setTag(TAG_SPRITE::BLACK_LAYER);
	addChild(black, Z_ORDER::Z_BLACK_LAYER);

	auto backGround = Sprite::create("webview_bg_small.png");
	Size winSize = Director::getInstance()->getWinSize();
	backGround->setPosition(Point(winSize.width / 2, winSize.height / 2));
	backGround->setTag(TAG_SPRITE::BACKGROUND);
	addChild(backGround, Z_BACKGROUND);
}

void PresentDialog::showContent(Json *presentInfomationJson) {
	auto contentlayer = LayerColor::create(Color4B::WHITE,
	                                       getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width - 80,
	                                       getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height * 0.6);
	contentlayer->setOpacity(180);
	getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(contentlayer);
	contentlayer->setAnchorPoint(Vec2(0.5, 0.5));
	contentlayer->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 - contentlayer->getContentSize().width / 2,
	                               getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - contentlayer->getContentSize().height / 2 + 40 - 20));

	//件名を表示
	auto title = Json_getString(presentInfomationJson, "title", "");
	TelopScrollView *telopView = TelopScrollView::initScrollTelop(title,
	                                                              34,
	                                                              getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width - 20,
	                                                              34,
	                                                              Color3B(255, 231, 117));
	telopView->setPosition(Point(10,
	                             getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height - 64));
	getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(telopView);

	auto layerSize = contentlayer->getContentSize();
	//スクロールビューを表示
	auto pScrollView = ScrollView::create(layerSize - Size(20, 20));
	contentlayer->addChild(pScrollView);
	pScrollView->setDirection(ScrollView::Direction::VERTICAL);
	pScrollView->setViewSize(Size(contentlayer->getContentSize().width - 10, contentlayer->getContentSize().height - 10));
	pScrollView->ignoreAnchorPointForPosition(false);
	pScrollView->setAnchorPoint(Point(0.5, 0.5));
	pScrollView->setPosition(Point(contentlayer->getContentSize().width * 0.5, contentlayer->getContentSize().height * 0.5));

	auto contentLabel = Label::createWithTTF(Json_getString(presentInfomationJson, "content", ""), FONT_NAME_2, 28);
	contentLabel->setAlignment(TextHAlignment::LEFT);
	contentLabel->setDimensions(pScrollView->getViewSize().width - 20, 0);
	contentLabel->setColor(Color3B::BLACK);

	pScrollView->setContainer(contentLabel);
	pScrollView->setContentOffset(Point(0, -(contentLabel->getContentSize().height - pScrollView->getViewSize().height)));
}

void PresentDialog::showButton() {
	auto getPresentButton = ui::Button::create("gacha_dialog_button.png");
	getPresentButton->addTouchEventListener(CC_CALLBACK_2(PresentDialog::onTouchGetPresentButton, this));
	getPresentButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
	getPresentButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2, 30));
	getPresentButton->setTag(TAG_SPRITE::BUTTON);
	auto getPresentLabel = Label::createWithTTF("受け取り", FONT_NAME_2, 32);
	getPresentLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
	getPresentLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	getPresentLabel->setPosition(Vec2(getPresentButton->getContentSize() / 2) - Vec2(0, 14));
	getPresentButton->addChild(getPresentLabel);
	getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(getPresentButton, Z_ORDER::Z_BUTTON);
}

void PresentDialog::onTouchGetPresentButton(Ref *sender, ui::Widget::TouchEventType type) {
	SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
	_callback(_presentId);
	removeFromParent();
}
