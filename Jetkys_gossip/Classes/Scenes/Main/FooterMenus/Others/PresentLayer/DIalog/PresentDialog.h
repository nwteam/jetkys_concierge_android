#ifndef __syanago__PresentDialog__
#define __syanago__PresentDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "extensions/cocos-ext.h"
#include "editor-support/spine/Json.h"


USING_NS_CC;
USING_NS_CC_EXT;

class PresentDialog : public Layer, public create_func<PresentDialog>
{
public:
    PresentDialog();
    ~PresentDialog();
    
    typedef std::function<void(const int presentId)> GetPresentCallback;
    using create_func::create;
    bool init(Json* presentInfomationJson,const GetPresentCallback& callback);
    
private:
    enum TAG_SPRITE{
        BLACK_LAYER = 0,
        BACKGROUND,
        BUTTON,
    };
    enum Z_ORDER{
        Z_BLACK_LAYER = 0,
        Z_BACKGROUND,
        Z_BUTTON,
    };
    GetPresentCallback _callback;
    int _presentId;
    
    void showBackground();
    void showContent(Json* presentInfomationJson);
    void showButton();
    void onTouchGetPresentButton(Ref* sender, ui::Widget::TouchEventType type);
    
};

#endif
