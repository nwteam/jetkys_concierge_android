#ifndef __syanago__OthersPresentLayer__
#define __syanago__OthersPresentLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Loading.h"
#include "DialogView.h"
#include "OthersLayer.h"
#include "ScrollBar.h"
#include "DropEffectLayer.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"
#include "create_func.h"
#include "show_head_line.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace SyanagoAPI;

class Cell;

class OthersPresentLayer : public Layer,
public cocos2d::extension::TableViewDataSource,
public cocos2d::extension::TableViewDelegate,
public DialogDelegate,
public create_func<OthersPresentLayer>,
public show_head_line
{
public:
    OthersPresentLayer();
    ~OthersPresentLayer();
    typedef std::function<void()> OnTapBackButtonCallBack;
    using create_func::create;
    bool init(const OnTapBackButtonCallBack& callback);
    
    void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
    virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {}
    virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell){};
    
    OthersLayer *_othersLayer;
private:
    enum TAG_SPRITE{
        BACKGROUND= 0,
        HOLD_BASE,
        HOLD_LABEL,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_HODL_BACKGROUND,
        Z_LABEL,
        Z_CONTENT,
        Z_DIALOG = 12,
        Z_GET_EFFECT,
    };
    DefaultProgress* progress;
    RequestAPI* request;
    int _numberOfCell;
    std::vector<Json*> _notices;
    OnTapBackButtonCallBack _callback;
    
    Size _winSize;
    ScrollBar * _scrollBar;
    Layer *_layer;
    
    void showBackground();
    void initLayer();
    void responseGetPresentList(Json* response);
    void showTable();
    Cell * cellWithIdx(int idx);
    void showNoListLabel();
    void getNotice(Ref* pSender);
    void showPresentDialog(const int tag);
    void PresentDialogCallback(const int presentId);
    void responseGetPresent(Json* response);
    void showGetContentEddect(const GET_TYPE type, const int masterId);
    void contentlayerClose();
    
    std::string getLastTime(Json *json);
};

#endif /* defined(__syanago__OthersPresentLayer__) */