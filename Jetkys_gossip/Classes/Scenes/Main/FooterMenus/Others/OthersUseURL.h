#ifndef syanago_OthersUseURL_h
#define syanago_OthersUseURL_h

#include "cocos2d.h"

#define RULE_URL "/rule/rule.html"
#define EVENT_URL "/event/event.html"
#define HELP_URL "/help/help.html"
#define SETTLEMENT_URL "/settlement/settlement.html"

#endif
