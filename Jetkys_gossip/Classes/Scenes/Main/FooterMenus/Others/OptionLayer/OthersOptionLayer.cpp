#include "OthersOptionLayer.h"
#include "FontDefines.h"
#include "UserDefaultManager.h"

OthersOptionLayer::OthersOptionLayer():
    _callback(nullptr)
{}

bool OthersOptionLayer::init(const OnTapBackButtonCallBack& callback)
{
    if (!Layer::create()) {
        return false;
    }
    _callback = callback;

    showBackground();
    showHeadLine(this, "オプション", [this](Ref* pSender) {
        _callback();
    });

    showOptionBase();
    showOption();

    return true;
}

void OthersOptionLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getVisibleSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void OthersOptionLayer::showOptionBase()
{
    auto base = makeSprite("other_option_background.png");
    auto winSize = Director::getInstance()->getWinSize();
    base->setPosition(Point(winSize.width / 2, winSize.height / 2));
    base->setTag(TAG_SPRITE::OPTION_BASE);
    addChild(base, Z_ORDER::Z_OPTION_BASE);

    auto title = Label::createWithTTF("サウンド設定", FONT_NAME_2, 32);
    title->setPosition(Point(base->getContentSize().width / 2, base->getContentSize().height - 30 - title->getContentSize().height / 2 - 16));
    title->setColor(COLOR_YELLOW);
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    base->addChild(title);
}

void OthersOptionLayer::showOption()
{
    auto winSize = Director::getInstance()->getWinSize();
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        showOptionButton(TAG_SPRITE::BGM_BUTTON, UserDefaultManager::KEY::BGM_OPTION_STATUS, "BGM", (winSize.height * 0.5) + 5 + 70);
        showOptionButton(TAG_SPRITE::SE_BUTTON, UserDefaultManager::KEY::SE_OPTION_STATUS, "SE", (winSize.height * 0.5) + 5);
        showOptionButton(TAG_SPRITE::VOICE_BUTTON, UserDefaultManager::KEY::VOICE_OPTION_STATUS, "ボイス", (winSize.height * 0.5) + 5 - 70);
    #else
        showApplyButton();
        showSwitchOptionButton(TAG_SPRITE::BGM_BUTTON, UserDefaultManager::KEY::BGM_OPTION_STATUS, "BGM", (winSize.height * 0.5) + 5 + 70);
        showSwitchOptionButton(TAG_SPRITE::SE_BUTTON, UserDefaultManager::KEY::SE_OPTION_STATUS, "SE", (winSize.height * 0.5) + 5);
        showSwitchOptionButton(TAG_SPRITE::VOICE_BUTTON, UserDefaultManager::KEY::VOICE_OPTION_STATUS, "ボイス", (winSize.height * 0.5) + 5 - 70);
    #endif
}

void OthersOptionLayer::showApplyButton()
{
    auto applyButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(OthersOptionLayer::applybtn, this));
    applyButton->setPosition(Vec2::ZERO);
    auto applyLabel = Label::createWithTTF("適用", FONT_NAME_2, 28);
    applyButton->addChild(applyLabel);
    applyLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    applyLabel->setPosition(Point(applyButton->getContentSize().width / 2, applyButton->getContentSize().height / 2 - 14));
    auto menu = MenuTouch::create(applyButton, NULL);
    menu->setPosition(Point(getChildByTag(TAG_SPRITE::OPTION_BASE)->getContentSize().width / 2, 30 + applyButton->getContentSize().height / 2));
    getChildByTag(TAG_SPRITE::OPTION_BASE)->addChild(menu, 1);
}

void OthersOptionLayer::applybtn(Ref* psender)
{
    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::BGM_OPTION_STATUS, static_cast<ControlSwitch*>(getChildByTag(TAG_SPRITE::BGM_BUTTON))->isOn());
    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::SE_OPTION_STATUS, static_cast<ControlSwitch*>(getChildByTag(TAG_SPRITE::SE_BUTTON))->isOn());
    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::VOICE_OPTION_STATUS, static_cast<ControlSwitch*>(getChildByTag(TAG_SPRITE::VOICE_BUTTON))->isOn());
    UserDefault::getInstance()->flush();
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    SOUND_HELPER->playingMainSceneBackgroundMusic();
}

void OthersOptionLayer::showSwitchOptionButton(int tag, const char* optionStatusKey, const std::string labelString, const float positionY)
{
    auto label1 = Label::createWithTTF("", FONT_NAME_2, 16);
    auto label2 = Label::createWithTTF("", FONT_NAME_2, 16);
    ControlSwitch* controlSwitch = ControlSwitch::create(Sprite::create("other_option_white.png"),
                                                         Sprite::create("other_option_on.png"),
                                                         Sprite::create("other_option_off.png"),
                                                         Sprite::create("other_option_point.png"),
                                                         label1,
                                                         label2);
    controlSwitch->setTag(tag);
    if (UserDefault::getInstance()->getBoolForKey(optionStatusKey) == true) {
        controlSwitch->setOn(true);
        controlSwitch->needsLayout();
    } else {
        controlSwitch->setOn(false);
        controlSwitch->needsLayout();
    }
    controlSwitch->setPosition(Point((Director::getInstance()->getWinSize().width * 0.5) + 80, positionY));
    addChild(controlSwitch, Z_ORDER::Z_SWITCH);

    auto label = Label::createWithTTF(labelString, FONT_NAME_2, 32);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setPosition(Point((Director::getInstance()->getWinSize().width * 0.5) - 80, positionY - 16));
    addChild(label, Z_ORDER::Z_SWITCH_LABEL);
}



void OthersOptionLayer::showOptionButton(int tag, const char* optionStatusKey, const std::string labelString, const float positionY)
{
    MenuItemImage* itemOn = MenuItemImage::create("other_option_toggle_on.png", "other_option_toggle_on.png");
    MenuItemImage* itemOff = MenuItemImage::create("other_option_toggle_off.png", "other_option_toggle_off.png");
    MenuItemToggle* toggle = MenuItemToggle::createWithTarget(this,
                                                              menu_selector(OthersOptionLayer::callBackshowOptionButton),
                                                              itemOn, itemOff, NULL);
    toggle->setTag(tag);
    toggle->setPosition(Point::ZERO);
    if (UserDefault::getInstance()->getBoolForKey(optionStatusKey) == true) {
        toggle->setSelectedIndex(0);
    } else {
        toggle->setSelectedIndex(1);
    }
    Menu* menu = Menu::create(toggle, NULL);
    menu->setPosition(Point((Director::getInstance()->getWinSize().width * 0.5) + 80, positionY));
    addChild(menu, 1);

    auto label = Label::createWithTTF(labelString, FONT_NAME_2, 32);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setPosition(Point((Director::getInstance()->getWinSize().width * 0.5) - 80, positionY - 16));
    addChild(label, 1);
}

void OthersOptionLayer::callBackshowOptionButton(Ref* pSender)
{
    const char* defaultKey;
    switch (((Node*)pSender)->getTag()) {
    case TAG_SPRITE::BGM_BUTTON: {
        defaultKey = UserDefaultManager::KEY::BGM_OPTION_STATUS;
        break;
    }
    case TAG_SPRITE::VOICE_BUTTON: {
        defaultKey = UserDefaultManager::KEY::VOICE_OPTION_STATUS;
        break;
    }
    case TAG_SPRITE::SE_BUTTON: {
        defaultKey = UserDefaultManager::KEY::SE_OPTION_STATUS;
        break;
    }
    default:
        break;
    }
    int index = dynamic_cast<MenuItemToggle*>(pSender)->getSelectedIndex();
    if (index == 1) {
        UserDefault::getInstance()->setBoolForKey(defaultKey, false);
    } else {
        UserDefault::getInstance()->setBoolForKey(defaultKey, true);
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    SOUND_HELPER->playingMainSceneBackgroundMusic();
}
