#ifndef __syanago__OthersOptionLayer__
#define __syanago__OthersOptionLayer__

#include "cocos2d.h"
#include "OthersLayer.h"
#include "extensions/cocos-ext.h"
#include "create_func.h"
#include "show_head_line.h"

USING_NS_CC;
USING_NS_CC_EXT;

class OthersOptionLayer : public Layer, public create_func<OthersOptionLayer>, public show_head_line{
public:
    OthersOptionLayer();
    typedef std::function<void()> OnTapBackButtonCallBack;
    using create_func::create;
    bool init(const OnTapBackButtonCallBack& callback);
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        OPTION_BASE,
        BGM_BUTTON,
        SE_BUTTON,
        VOICE_BUTTON,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_OPTION_BASE,
        Z_SWITCH,
        Z_SWITCH_LABEL,
    };
    
    OnTapBackButtonCallBack _callback;
    
    void showBackground();
    void showOptionBase();
    void showOption();
    
    void showApplyButton();
    void applybtn(Ref* pSender);
    void showSwitchOptionButton(int tag, const char* optionStatusKey, const std::string labelString, const float positionY);//androidバグが有り
    
    void showOptionButton(int tag, const char* optionStatusKey, const std::string labelString, const float positionY);
    void callBackshowOptionButton(Ref* pSender);
};

#endif /* defined(__syanago__OthersOptionLayer__) */
