#include "OthersColectionLayer.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include <memory>
#include <vector>
#include <algorithm>
#include <string>
#include "HeaderStatus.h"
#include "CharacterModel.h"
#include "SoundHelper.h"
#include "ColectionList.h"
#include "ColectionDetail.h"
#include "ColectionSortDialog.h"

struct sortIDForward {
    bool
    operator () (PlayerPictureBookModel* i, PlayerPictureBookModel* j) const
    {
        std::shared_ptr<CharacterModel>characterLeftModel(CharacterModel::find(i->getMstCharactersId()));
        std::shared_ptr<CharacterModel>characterRightModel(CharacterModel::find(j->getMstCharactersId()));
        int sortNo1 = characterLeftModel->getSortNo();
        int sortNo2 = characterRightModel->getSortNo();

        if (sortNo1 == sortNo2) {
            return i->getMstCharactersId() < j->getMstCharactersId();
        }

        return sortNo1 < sortNo2;
    }
} sortIDForward;

struct sortIDReverse {
    bool
    operator () (PlayerPictureBookModel* i, PlayerPictureBookModel* j) const
    {
        std::shared_ptr<CharacterModel>characterLeftModel(CharacterModel::find(i->getMstCharactersId()));
        std::shared_ptr<CharacterModel>characterRightModel(CharacterModel::find(j->getMstCharactersId()));
        int sortNo1 = characterLeftModel->getSortNo();
        int sortNo2 = characterRightModel->getSortNo();

        if (sortNo1 == sortNo2) {
            return i->getMstCharactersId() < j->getMstCharactersId();
        }

        return sortNo1 > sortNo2;
    }
} sortIDReverse;

struct rarityForward {
    bool
    operator () (PlayerPictureBookModel* i, PlayerPictureBookModel* j) const
    {
        std::shared_ptr<CharacterModel>characterLeftModel(CharacterModel::find(i->getMstCharactersId()));
        std::shared_ptr<CharacterModel>characterRightModel(CharacterModel::find(j->getMstCharactersId()));
        int rarity1 = characterLeftModel->getRarity();
        int rarity2 = characterRightModel->getRarity();

        if (rarity1 == rarity2) {
            return i->getMstCharactersId() < j->getMstCharactersId();
        }

        return rarity1 < rarity2;
    }
} rarityForward;

struct rarityReverse {
    bool
    operator () (PlayerPictureBookModel* i, PlayerPictureBookModel* j) const
    {
        std::shared_ptr<CharacterModel>characterLeftModel(CharacterModel::find(i->getMstCharactersId()));
        std::shared_ptr<CharacterModel>characterRightModel(CharacterModel::find(j->getMstCharactersId()));
        int rarity1 = characterLeftModel->getRarity();
        int rarity2 = characterRightModel->getRarity();

        if (rarity1 == rarity2) {
            return i->getMstCharactersId() < j->getMstCharactersId();
        }

        return rarity1 > rarity2;
    }
} rarityReverse;

OthersColectionLayer::OthersColectionLayer():
    _progress(nullptr), _request(nullptr), _sortMode(CSM_SORT_ID_FORWARD), _callbackModel(nullptr)
{
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}
OthersColectionLayer::~OthersColectionLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

bool OthersColectionLayer::init(const std::shared_ptr<OthersColectionCallbackModel>& callbackModel)
{
    if (!Layer::init()) {
        return false;
    }
    _callbackModel = callbackModel;

    showBackground();
    showHeadLine(this, "車なご図鑑", [this](Ref* pSender) {
        _callbackModel->getOnTapPageBack()();
    });

    _progress->onStart();
    _request->getPictureBook(CC_CALLBACK_1(OthersColectionLayer::onResponseGetPictureBook, this));
    return true;
}

void OthersColectionLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getVisibleSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void OthersColectionLayer::onResponseGetPictureBook(Json* response)
{
    std::string result = Json_getString(Json_getItem(response, "get_picture_book"), "result", "false");
    if (result == "true") {
        Json* jsonlist = Json_getItem(Json_getItem(Json_getItem(response, "get_picture_book"), "data"), "list");
        Json* child;
        PLAYERCONTROLLER->_playerPictureBook.clear();
        int metCharacterCount = 0;
        int totalCharacterCount = 0;
        for (child = jsonlist->child; child; child = child->next) {
            const int holdingsCount = atoi(Json_getString(child, "number_of_holdings", ""));
            const int metCount = atoi(Json_getString(child, "number_of_contacts", ""));
            PLAYERCONTROLLER->addPictureBook((int)PLAYERCONTROLLER->_player->getID(),
                                             atoi(Json_getString(child, "mst_characters_id", "")),
                                             holdingsCount,
                                             metCount);
            totalCharacterCount++;
            if (metCount > 0 || holdingsCount > 0) {
                metCharacterCount++;
            }
        }
        showMetCharacterView(metCharacterCount, totalCharacterCount);
        showSortButton();
        showCollection();
    }
}

void OthersColectionLayer::showMetCharacterView(const int metCharacterCount, const int totalCharacterCount)
{
    auto metCharacterViewBackground = Sprite::create("carry_number_bg.png");
    metCharacterViewBackground->setTag(TAG_SPRITE::MET_CHARACTER_VIEW);
    auto winSize = Director::getInstance()->getVisibleSize();
    metCharacterViewBackground->setPosition(Point(winSize.width - metCharacterViewBackground->getContentSize().width / 2,
                                                  winSize.height - 104.5 - HEADER_STATUS::SIZE::HEIGHT - metCharacterViewBackground->getContentSize().height / 2));
    auto metCharacterLabel = Label::createWithTTF("収集数", FONT_NAME_2, 18);
    metCharacterLabel->setAnchorPoint(Vec2(0, 0.5));
    metCharacterLabel->setPosition(Point(5, metCharacterViewBackground->getContentSize().height / 2 - 9));
    metCharacterViewBackground->addChild(metCharacterLabel);

    auto metCharacterValueLabel = Label::createWithTTF(StringUtils::format("%d/%d", metCharacterCount, totalCharacterCount), FONT_NAME_2, 18);
    metCharacterValueLabel->setAnchorPoint(Vec2(1, 0.5));
    metCharacterValueLabel->setPosition(Point(metCharacterViewBackground->getContentSize().width - 5, metCharacterLabel->getPosition().y));
    metCharacterViewBackground->addChild(metCharacterValueLabel);
    addChild(metCharacterViewBackground, Z_ORDER::Z_MET_CHARACTER_VIEW);
}

void OthersColectionLayer::showCollection()
{
    if (getChildByTag(TAG_SPRITE::COLECTION_LIST)) {
        removeChildByTag(TAG_SPRITE::COLECTION_LIST);
    }
    auto colectionList = ColectionList::create(CC_CALLBACK_1(OthersColectionLayer::showDetailCharacterLayer, this));
    colectionList->setTag(TAG_SPRITE::COLECTION_LIST);
    addChild(colectionList, Z_ORDER::Z_COLECTION_LIST);
}

void OthersColectionLayer::showDetailCharacterLayer(const int masterCharacterId)
{
    if (getChildByTag(TAG_SPRITE::DETAIL_LAYER)) {
        return;
    }
    _callbackModel->getRemoveHeaderAndFooter()();

    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(masterCharacterId));
    auto ColectionDetail = ColectionDetail::create(characterModel, CC_CALLBACK_0(OthersColectionLayer::detailCallback, this));
    ColectionDetail->setTag(TAG_SPRITE::DETAIL_LAYER);
    addChild(ColectionDetail, Z_ORDER::Z_DETAIL_LAYER);
}

void OthersColectionLayer::detailCallback()
{
    _callbackModel->getShowHeaderAndFooter()();
}

void OthersColectionLayer::showSortButton()
{
    if (getChildByTag(TAG_SPRITE::SORT_BUTTON)) {
        removeChildByTag(TAG_SPRITE::SORT_BUTTON);
    }
    std::string sortLabelString = "";
    switch (_sortMode) {
    case CSM_SORT_ID_FORWARD:
        sortLabelString = "No.昇";
        std::sort(PLAYERCONTROLLER->_playerPictureBook.begin(), PLAYERCONTROLLER->_playerPictureBook.end(), sortIDForward);
        break;
    case CSM_SORT_ID_REVERSE:
        sortLabelString = "No.降";
        std::sort(PLAYERCONTROLLER->_playerPictureBook.begin(), PLAYERCONTROLLER->_playerPictureBook.end(), sortIDReverse);
        break;
    case CSM_RARITY_FORWARD:
        sortLabelString = "レア昇";
        std::sort(PLAYERCONTROLLER->_playerPictureBook.begin(), PLAYERCONTROLLER->_playerPictureBook.end(), rarityForward);
        break;
    case CSM_RARITY_REVERSE:
        sortLabelString = "レア降";
        std::sort(PLAYERCONTROLLER->_playerPictureBook.begin(), PLAYERCONTROLLER->_playerPictureBook.end(), rarityReverse);
        break;
    default:
        break;
    }
    auto sortButton = ui::Button::create("garage_list_sort_bt.png");
    sortButton->addTouchEventListener(CC_CALLBACK_2(OthersColectionLayer::onTapSortButtonCallback, this));
    sortButton->setTag(TAG_SPRITE::SORT_BUTTON);
    sortButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    sortButton->setPosition(Vec2(Director::getInstance()->getVisibleSize().width - 14,
                                 Director::getInstance()->getVisibleSize().height - HEADER_STATUS::SIZE::HEIGHT - 44.5));
    auto sortlabel = Label::createWithTTF(sortLabelString, FONT_NAME_2, 22);
    sortlabel->setPosition(Point(sortButton->getContentSize().width / 2, sortButton->getContentSize().height / 2 - 11));
    sortlabel->enableShadow();
    sortButton->addChild(sortlabel);
    addChild(sortButton, Z_ORDER::Z_SORT_BUTTON);
}

void OthersColectionLayer::onTapSortButtonCallback(Ref* pSender, ui::Widget::TouchEventType type)
{
    if (type != ui::Widget::TouchEventType::ENDED) {
        return;
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    showSortDialog();
}

void OthersColectionLayer::showSortDialog()
{
    if (getChildByTag(TAG_SPRITE::SORT_DIALOG)) {
        return;
    }
    auto sortDialog = ColectionSortDialog::create(CC_CALLBACK_1(OthersColectionLayer::onTapSortDialogButtonCallback, this), _sortMode);
    sortDialog->setTag(TAG_SPRITE::SORT_DIALOG);
    addChild(sortDialog, Z_SORT_DIALOG);
}

void OthersColectionLayer::onTapSortDialogButtonCallback(COLECTION_SORT_MODE mode)
{
    _sortMode = mode;
    showSortButton();
    showCollection();
}
