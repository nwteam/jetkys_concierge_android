#ifndef __syanago__OthersColectionLayer__
#define __syanago__OthersColectionLayer__

#include "cocos2d.h"
#include "DialogView.h"
#include "CharacterModel.h"
#include "extensions/cocos-ext.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"
#include "ui/CocosGUI.h"
#include "show_head_line.h"
#include "create_func.h"
#include "OthersColectionCallbackModel.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace SyanagoAPI;

#define MAIN_TEXT_TAG 1000

enum COLECTION_SORT_MODE{
    CSM_SORT_ID_FORWARD = 0,
    CSM_SORT_ID_REVERSE,
    CSM_RARITY_FORWARD,
    CSM_RARITY_REVERSE,
};

class OthersColectionLayer : public Layer, public create_func<OthersColectionLayer>, public DialogDelegate, public show_head_line{
public:
    OthersColectionLayer();
    virtual ~OthersColectionLayer();
    using create_func::create;
    bool init(const std::shared_ptr<OthersColectionCallbackModel>& callbackModel);
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        COLECTION_LIST,
        MET_CHARACTER_VIEW,
        SORT_BUTTON,
        DETAIL_LAYER,
        SORT_DIALOG,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_COLECTION_LIST,
        Z_MET_CHARACTER_VIEW,
        Z_SORT_BUTTON,
        Z_DETAIL_LAYER = 12,
        Z_SORT_DIALOG,
    };
    COLECTION_SORT_MODE _sortMode;
    DefaultProgress* _progress;
    RequestAPI* _request;
    std::shared_ptr<OthersColectionCallbackModel> _callbackModel;
    
    
    void showBackground();
    void onResponseGetPictureBook(Json* response);
    void showMetCharacterView(const int metCharacterCount, const int totalCharacterCount);
    void showCollection();
    void showDetailCharacterLayer(const int masterCharacterId);
    void detailCallback();
    void showSortButton();
    void onTapSortButtonCallback(Ref *pSender, ui::Widget::TouchEventType type);
    void showSortDialog();
    void onTapSortDialogButtonCallback(COLECTION_SORT_MODE mode);
};


#endif /* defined(__syanago__OthersColectionLayer__) */
