#ifndef __syanago__OthersColectionCallbackModel__
#define __syanago__OthersColectionCallbackModel__

#include "cocos2d.h"

class OthersColectionCallbackModel
{
public:
    typedef std::function<void()> OnTapBackButtonCallback;
    typedef std::function<void()> ShowHeaderAndFooterMenu;
    typedef std::function<void()> RemoveHeaderAndFooterMenu;
    OthersColectionCallbackModel(const OnTapBackButtonCallback& onTapBackButtonCallback,
                                 const ShowHeaderAndFooterMenu& showHeaderAndFooterMenu,
                                 const RemoveHeaderAndFooterMenu& removeHeaderAndFooterMenu);
    ~OthersColectionCallbackModel(){};
    
    CC_SYNTHESIZE_READONLY(OnTapBackButtonCallback, onTapPageBack, OnTapPageBack);
    CC_SYNTHESIZE_READONLY(ShowHeaderAndFooterMenu, showHeaderAndFooter, ShowHeaderAndFooter);
    CC_SYNTHESIZE_READONLY(RemoveHeaderAndFooterMenu, removeHeaderAndFooter, RemoveHeaderAndFooter);
};

#endif