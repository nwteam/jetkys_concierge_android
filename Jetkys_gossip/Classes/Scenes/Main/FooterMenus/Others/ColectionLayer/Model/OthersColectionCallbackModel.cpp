#include "OthersColectionCallbackModel.h"

OthersColectionCallbackModel::OthersColectionCallbackModel(const OnTapBackButtonCallback& onTapBackButtonCallback,
                                                           const ShowHeaderAndFooterMenu& showHeaderAndFooterMenu,
                                                           const RemoveHeaderAndFooterMenu& removeHeaderAndFooterMenu):
    onTapPageBack(nullptr), showHeaderAndFooter(nullptr), removeHeaderAndFooter(nullptr)
{
    onTapPageBack = onTapBackButtonCallback;
    showHeaderAndFooter = showHeaderAndFooterMenu;
    removeHeaderAndFooter = removeHeaderAndFooterMenu;
}