#include "ColectionDetail.h"
#include "jCommon.h"
#include "SoundHelper.h"
#include "FontDefines.h"
#include "DetailCharacterStatusSprite.h"

ColectionDetail::ColectionDetail():
    _characterMaxScale(1.0f), _isVisibleObject(true), _callback(nullptr)
{}

ColectionDetail::~ColectionDetail()
{}

bool ColectionDetail::init(const std::shared_ptr<CharacterModel>& characterModel, const OnBackDetailCallback& callback)
{
    if (!Layer::create()) {
        return false;
    }
    _callback = callback;

    showBackground();
    showCharacter(characterModel->getID());
    showBackButton();
    showCharacterName(characterModel->getCarName());
    showCharacterStatus(characterModel);
    showChangeImageButton(characterModel->getID());
    showVoiceButton(characterModel->getID());

    setTouchEffect();

    return true;
}

void ColectionDetail::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setPosition(Director::getInstance()->getWinSize() / 2);
    background->setTag(TAG_SPRITE::BACKGROUND);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void ColectionDetail::showCharacter(const int masterCharacterId)
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_ca.plist",  masterCharacterId).c_str());
    auto characterImage = makeSprite(StringUtils::format("%d_t.png", masterCharacterId).c_str());
    characterImage->setTag(TAG_SPRITE::CHARACTER_IMAGE);
    characterImage->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    auto winSize = Director::getInstance()->getWinSize();
    characterImage->setPosition(Vec2(winSize.width / 2, winSize.height));
    _characterMaxScale = getCharacterMaxScale(characterImage);
    characterImage->setScale(_characterMaxScale);
    addChild(characterImage, Z_ORDER::Z_CHARACTER);
}

const float ColectionDetail::getCharacterMaxScale(const Sprite* characterImage)
{
    auto winSize = Director::getInstance()->getWinSize();
    float result = 1.0;
    result = winSize.width / characterImage->getContentSize().width;
    float heightScale = result * characterImage->getContentSize().height;
    float maxYScale = winSize.height / heightScale;
    if (maxYScale < 1) {
        result =  winSize.height / characterImage->getContentSize().height;
    }
    return result;
}

void ColectionDetail::showSDCharacter(const int masterCharacterId)
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_sdb.plist", masterCharacterId).c_str());
    auto characterSDImage = makeSprite(StringUtils::format("%d_sdb.png", masterCharacterId).c_str());
    characterSDImage->setTag(TAG_SPRITE::CHARACTER_SD_IMAGE);
    characterSDImage->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    characterSDImage->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2, 418));
    addChild(characterSDImage, Z_ORDER::Z_CHARACTER);
}

void ColectionDetail::showBackButton()
{
    auto backButton = ui::Button::create("back_button.png");
    backButton->setTag(TAG_SPRITE::BACK_BUTTON);
    backButton->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    backButton->setPosition(Vec2(14, Director::getInstance()->getWinSize().height - 14));
    backButton->addTouchEventListener(CC_CALLBACK_2(ColectionDetail::onTapBackButton, this));
    addChild(backButton, Z_ORDER::Z_BACK_BUTTON);
}

void ColectionDetail::onTapBackButton(Ref* pSender, ui::Widget::TouchEventType type)
{
    if (type != ui::Widget::TouchEventType::ENDED) {
        return;
    }
    _callback();
    SOUND_HELPER->stopVoiceEfect();
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    removeFromParent();
}

void ColectionDetail::showCharacterName(const std::string name)
{
    auto characterName = Label::createWithTTF(name, FONT_NAME_2, 28);
    characterName->setTag(TAG_SPRITE::CHARACTER_NAME);
    characterName->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    auto backButton = getChildByTag(TAG_SPRITE::BACK_BUTTON);
    characterName->setPosition(backButton->getPosition() + Vec2(backButton->getContentSize().width + 10,
                                                                -backButton->getContentSize().height / 2 - 14));
    characterName->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(characterName, Z_ORDER::Z_CHARACTER_NAME);
}

void ColectionDetail::showCharacterStatus(const std::shared_ptr<CharacterModel>& characterModel)
{
    auto statusView = DetailCharacterStatusSprite::create(characterModel);
    statusView->setTag(TAG_SPRITE::STATUS_VIEW);
    statusView->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2, statusView->getContentSize().height / 2 + 6));
    addChild(statusView, Z_ORDER::Z_STATUS_VIEW);
}

void ColectionDetail::showChangeImageButton(const int masterCharacterId)
{
    auto changeImageButton = ui::Button::create("other_colection_change_btn.png");
    changeImageButton->setTag(TAG_SPRITE::CHANGE_IMAGE_BUTTON);
    changeImageButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    changeImageButton->setPosition(Vec2(Director::getInstance()->getWinSize().width - 14, 426));
    changeImageButton->addTouchEventListener(CC_CALLBACK_2(ColectionDetail::onTapChangeImageButton, this, masterCharacterId));
    addChild(changeImageButton, Z_ORDER::Z_CHANGE_IMAGE_BUTTON);
}

void ColectionDetail::onTapChangeImageButton(Ref* pSender, ui::Widget::TouchEventType type, const int masterCharacterId)
{
    if (type != ui::Widget::TouchEventType::ENDED) {
        return;
    }
    if (getChildByTag(TAG_SPRITE::CHARACTER_IMAGE)) {
        removeChildByTag(TAG_SPRITE::CHARACTER_IMAGE);
        showSDCharacter(masterCharacterId);
    } else if (getChildByTag(TAG_SPRITE::CHARACTER_SD_IMAGE)) {
        removeChildByTag(TAG_SPRITE::CHARACTER_SD_IMAGE);
        showCharacter(masterCharacterId);
    }
}

void ColectionDetail::showVoiceButton(const int masterCharacterId)
{
    auto voiceButton = ui::Button::create("other_colection_voiceRestart.png");
    voiceButton->setTag(TAG_SPRITE::VOICE_BUTTON);
    voiceButton->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    voiceButton->setPosition(Vec2(Director::getInstance()->getWinSize().width - 14, 446 + getChildByTag(TAG_SPRITE::CHANGE_IMAGE_BUTTON)->getContentSize().height));
    voiceButton->addTouchEventListener(CC_CALLBACK_2(ColectionDetail::onTapVoiceButton, this, masterCharacterId));
    addChild(voiceButton, Z_ORDER::Z_VOICE_BUTTON);
}

void ColectionDetail::onTapVoiceButton(Ref* pSender, ui::Widget::TouchEventType type, const int masterCharacterId)
{
    if (type != ui::Widget::TouchEventType::ENDED) {
        return;
    }
    SOUND_HELPER->playCharacterVoiceEfect(masterCharacterId, 15);
}

void ColectionDetail::setTouchEffect()
{
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(this);
    listener->onTouchBegan = CC_CALLBACK_2(ColectionDetail::onTapColectionDetailLayer, this);
    auto dip = Director::getInstance()->getEventDispatcher();
    dip->addEventListenerWithSceneGraphPriority(listener, this);
}

bool ColectionDetail::onTapColectionDetailLayer(Touch* touch, Event* event)
{
    if (!getChildByTag(TAG_SPRITE::STATUS_VIEW)) {
        return true;
    }
    auto scrollRect = getChildByTag(TAG_SPRITE::STATUS_VIEW)->getBoundingBox();
    if (scrollRect.containsPoint(Director::getInstance()->convertToGL(touch->getLocationInView()))) {
        return true;
    }
    auto winSize = Director::getInstance()->getVisibleSize();
    if (_isVisibleObject == true) {
        if (getChildByTag(TAG_SPRITE::CHARACTER_IMAGE)) {
            getChildByTag(TAG_SPRITE::CHARACTER_IMAGE)->runAction(MoveTo::create(0.2, Vec2(winSize.width / 2,
                                                                                           winSize.height / 2 + (getChildByTag(TAG_SPRITE::CHARACTER_IMAGE)->getContentSize().height) * _characterMaxScale / 2)));
        } else if (getChildByTag(TAG_SPRITE::CHARACTER_SD_IMAGE)) {
            getChildByTag(TAG_SPRITE::CHARACTER_SD_IMAGE)->runAction(MoveTo::create(0.2, Vec2(winSize.width / 2,
                                                                                              winSize.height / 2 - (getChildByTag(TAG_SPRITE::CHARACTER_SD_IMAGE)->getContentSize().height) / 2)));
        }

        setVisibleObject(false);
    } else {
        if (getChildByTag(TAG_SPRITE::CHARACTER_IMAGE)) {
            getChildByTag(TAG_SPRITE::CHARACTER_IMAGE)->runAction(MoveTo::create(0.2, Vec2(winSize.width / 2, winSize.height)));
        } else if (getChildByTag(TAG_SPRITE::CHARACTER_SD_IMAGE)) {
            getChildByTag(TAG_SPRITE::CHARACTER_SD_IMAGE)->runAction(MoveTo::create(0.2, Vec2(winSize.width / 2, 418)));
        }
        setVisibleObject(true);
    }
    return true;
}

void ColectionDetail::setVisibleObject(const bool isVisible)
{
    getChildByTag(TAG_SPRITE::STATUS_VIEW)->setVisible(isVisible);
    getChildByTag(TAG_SPRITE::CHARACTER_NAME)->setVisible(isVisible);
    getChildByTag(TAG_SPRITE::BACK_BUTTON)->setVisible(isVisible);
    getChildByTag(TAG_SPRITE::CHANGE_IMAGE_BUTTON)->setVisible(isVisible);
    getChildByTag(TAG_SPRITE::VOICE_BUTTON)->setVisible(isVisible);
    _isVisibleObject = isVisible;
}
