#include "ColectionList.h"
#include "PlayerController.h"
#include "CharacterModel.h"
#include "jCommon.h"
#include "FontDefines.h"
#include "HeaderStatus.h"
#include "MenuTouch.h"

ColectionList::ColectionList():
    _callback(nullptr)
{}

ColectionList::~ColectionList()
{}

bool ColectionList::init(const OnTapShowDetailLayer& callback)
{
    if (!Layer::create()) {
        return false;
    }
    _callback = callback;

    showScrollView();
    return true;
}

void ColectionList::showScrollView()
{
    auto winSize = Director::getInstance()->getVisibleSize();
    auto layerSize = winSize; // レイヤーサイズを記録

    // 車なごの総数
    int charaCount = (int)PLAYERCONTROLLER->_playerPictureBook.size();
    int oldY =  (charaCount / 5 + 1) * (ICON_SEIZE_HEIGHT + ICON_SPACE);
    layerSize.height = oldY + ICON_SEIZE_HEIGHT / 2;

    // 表示する数だけループ 上から順番に配置
    Vector<MenuItem*>menuItems;
    MenuItem* menuItem;
    int oldX;
    for (int y = 0; y < (charaCount / 5) + 1; y++) {
        oldX = -30;
        for (int x = 0; x < 5; x++) {
            if (y * 5 + x > charaCount - 1) break;
            int ownTag = y * 5 + x;
            std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(PLAYERCONTROLLER->_playerPictureBook.at(ownTag)->getMstCharactersId()));

            int pTag;
            Sprite* pPicture;
            if (PLAYERCONTROLLER->_playerPictureBook.at(ownTag)->getNumberOfContacts() > 0 || PLAYERCONTROLLER->_playerPictureBook.at(ownTag)->getNumberOfHoldings() > 0) {
                pPicture = makeSprite(StringUtils::format("%d_i.png", PLAYERCONTROLLER->_playerPictureBook.at(ownTag)->getMstCharactersId()).c_str());
                pTag = PLAYERCONTROLLER->_playerPictureBook.at(ownTag)->getMstCharactersId();
            } else {
                pPicture = Sprite::create("other_colection_noimage.jpg");
                pTag = -1;
            }

            // アイテムを生成
            menuItem = MenuItemSprite::create(pPicture, pPicture, [this](Ref* sender) {});
            menuItem->setTag(pTag);
            auto size = menuItem->getContentSize();
            menuItem->setPosition(Point(oldX + size.width + ICON_SPACE, oldY - size.height - ICON_SPACE));
            oldX = menuItem->getPosition().x;

            // No番号の文字列を作成
            std::string charaNo;
            switch (StringUtils::format("%d", characterModel->getSortNo()).size()) {
            case 1:
                charaNo = "00" + StringUtils::format("%d", characterModel->getSortNo());
                break;
            case 2:
                charaNo = "0" + StringUtils::format("%d", characterModel->getSortNo());
                break;
            default:
                charaNo = StringUtils::format("%d", characterModel->getSortNo());
                break;
            }
            auto number = Label::createWithTTF("No " + charaNo, FONT_NAME_2, 19);
            number->enableOutline(Color4B::BLACK, 2);
            number->setPosition(Point(size.width * 0.5, -4.5));
            pPicture->addChild(number);

            menuItems.pushBack(menuItem);
        }
        oldY = menuItem->getPosition().y;
    }

    // 一覧を作成
    if (getChildByTag(TAG_SPRITE::CHARACTER_LIST)) {
        removeChildByTag(TAG_SPRITE::CHARACTER_LIST);
    }

    Menu* menuCollection = MenuTouch::createWithArray(menuItems);
    // addChild(menu);
    menuCollection->setPosition(Point::ZERO);
    menuCollection->setPositionY(menuCollection->getPositionY() + 260);
    menuCollection->setAnchorPoint(Point(0.5f, 0.5f));
    menuCollection->setEnabled(false);
    menuCollection->setTag(102);

    // スクロールビュー用レイヤーを作成
    auto layer = Layer::create();
    layer->addChild(menuCollection);
    layer->setContentSize(layerSize + Size(0, 150));
    layer->ignoreAnchorPointForPosition(false);
    layer->setAnchorPoint(Point(0.5, 0.5));
    layer->setPosition(Point(winSize.width * 0.5, winSize.height * 0.5));
    layer->setTag(101);

    // スクロールビューを作成
    auto scrollView = ScrollView::create();
    scrollView->setTag(TAG_SPRITE::CHARACTER_LIST);
    addChild(scrollView);
    scrollView->setPosition(Point(winSize.width * 0.5, 0));
    scrollView->ignoreAnchorPointForPosition(false);
    scrollView->setAnchorPoint(Point(0.5, 0));
    scrollView->setViewSize(Size(winSize.width,  winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 20 -
                                 28));
    scrollView->setContainer(layer);
    scrollView->setContentSize(Size(winSize.width, layer->getContentSize().height));
    scrollView->setDirection(ScrollView::Direction::VERTICAL);
    scrollView->setBounceable(false);
    scrollView->setContentOffset(Point(0, scrollView->getViewSize().height - scrollView->getContentSize().height));
    scrollView->setDelegate(this);

    // スクロールバー
    if (getChildByTag(TAG_SPRITE::SCROLL_BAR)) {
        removeChildByTag(TAG_SPRITE::SCROLL_BAR);
    }
    auto scrollBar = ScrollBar::initScrollBar(scrollView->getViewSize().height - 180, scrollView->getViewSize().height, scrollView->getContainer()->getContentSize().height, scrollView->getContentOffset().y);
    scrollBar->setTag(TAG_SPRITE::SCROLL_BAR);
    addChild(scrollBar);
    scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 8, winSize.height / 2 - 50 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    if (scrollView->getViewSize().height <= scrollView->getContainer()->getContentSize().height) {
        scrollView->setPosition(scrollView->getPosition() + Point(-20, 0));
    }

    // タッチ設定
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = [this](Touch* touch, Event* event) {
                                 _beganTouhPoint = touch->getLocation();
                                 return true;
                             };

    listener->onTouchMoved = [this](Touch* touch, Event* event) {
                                 auto endedTouchPoint = touch->getLocation();
                                 if ((sqrt(pow((_beganTouhPoint.x - endedTouchPoint.x), 2) + pow((_beganTouhPoint.y - endedTouchPoint.y), 2))) > THRESHOLD) {
                                     _moveSCrollFlag = true;
                                 }
                             };

    listener->onTouchEnded = [this](Touch* touch, Event* event) {
                                 auto target = static_cast<Sprite*>(event->getCurrentTarget());
                                 if (_moveSCrollFlag == false) {
                                     for (auto item : target->getChildByTag(101)->getChildByTag(102)->getChildren()) {
                                         auto localPoint = target->getChildByTag(101)->getChildByTag(102)->convertToNodeSpace(touch->getLocation());
                                         if (item->getBoundingBox().containsPoint(localPoint)) {
                                             if (item->getTag() != -1) {
                                                 _callback(item->getTag());
                                             }
                                         }
                                     }
                                 }
                                 _moveSCrollFlag = false;
                             };
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, scrollView);
}



void ColectionList::scrollViewDidScroll(ScrollView* view)
{
    auto scrollBar = static_cast<ScrollBar*>(getChildByTag(TAG_SPRITE::SCROLL_BAR));
    if (scrollBar) {
        scrollBar->updateScrollPoint(view->getContentOffset().y);
    }
}