#ifndef __syanago__DetailCharacterStatusSprite__
#define __syanago__DetailCharacterStatusSprite__

#include "cocos2d.h"
#include "CharacterModel.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class DetailCharacterStatusSprite : public Sprite
{
public:
    DetailCharacterStatusSprite();
    ~DetailCharacterStatusSprite();
    
    static DetailCharacterStatusSprite* create(const std::shared_ptr<CharacterModel>& characterModel);
    
    
private:
    enum TAG_SPRITE {
        DESCRIPTION = 0,
        BIRTH_PLACE_AND_MAKER_NAME,
        BODY_TYPE,
        NUMBER,
        TYPE_CERTIFICATE,
        RARITY,
    };
    enum Z_ORDER {
        Z_DESCRIPTION = 0,
        Z_BIRTH_PLACE_AND_MAKER_NAME,
        Z_BODY_TYPE,
        Z_NUMBER,
        Z_TYPE_CERTIFICATE,
        Z_RARITY
    };
    void showStatus(const std::shared_ptr<CharacterModel>& characterModel);
    
    void showDescription(const std::string description);
    void showBirthPlaceAndMaker(const std::string birthPlaceName, const std::string makerName);
    void showBodyType(const std::string bodyTypeName);
    void showCharacterNumber(const int characterNumber);
    void showCarTypeCertificate(const std::string typeCertificate);
    void showRarity(const int rarity);
};

#endif