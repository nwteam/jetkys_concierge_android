#ifndef __syanago__ColectionDetail__
#define __syanago__ColectionDetail__

#include "cocos2d.h"
#include "create_func.h"
#include "CharacterModel.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class ColectionDetail : public Layer, public create_func<ColectionDetail>
{
public:
    typedef std::function<void()> OnBackDetailCallback;
    
    ColectionDetail();
    ~ColectionDetail();
    using create_func::create;
    bool init(const std::shared_ptr<CharacterModel>& characterModel, const OnBackDetailCallback& callback);
    
private:
    enum TAG_SPRITE {
        BACKGROUND = 0,
        CHARACTER_IMAGE,
        CHARACTER_SD_IMAGE,
        BACK_BUTTON,
        CHARACTER_NAME,
        STATUS_VIEW,
        CHANGE_IMAGE_BUTTON,
        VOICE_BUTTON,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_CHARACTER,
        Z_BACK_BUTTON,
        Z_CHARACTER_NAME,
        Z_STATUS_VIEW,
        Z_CHANGE_IMAGE_BUTTON,
        Z_VOICE_BUTTON,
    };
    float _characterMaxScale;
    bool _isVisibleObject;
    OnBackDetailCallback _callback;
    
    void showBackground();
    void showCharacter(const int masterCharacterId);
    const float getCharacterMaxScale(const Sprite* characterImage);
    void showSDCharacter(const int masterCharacterId);
    void showBackButton();
    void onTapBackButton(Ref* pSender, ui::Widget::TouchEventType type);
    void showCharacterName(const std::string name);
    void showCharacterStatus(const std::shared_ptr<CharacterModel>& characterModel);
    void showChangeImageButton(const int masterCharacterId);
    void onTapChangeImageButton(Ref* pSender, ui::Widget::TouchEventType type, const int masterCharacterId);
    void showVoiceButton(const int masterCharacterId);
    void onTapVoiceButton(Ref* pSender, ui::Widget::TouchEventType type, const int masterCharacterId);
    
    void setTouchEffect();
    bool onTapColectionDetailLayer(Touch* touch, Event* event);
    void setVisibleObject(const bool isVisible);
};

#endif