#ifndef __syanago__ColectionList__
#define __syanago__ColectionList__

#include "cocos2d.h"
#include "create_func.h"
#include "cocos-ext.h"
#include "ScrollBar.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ColectionList : public Layer, public create_func<ColectionList>, public ScrollViewDelegate
{
public:
    ColectionList();
    ~ColectionList();
    using create_func::create;
    typedef std::function<void(const int masterCharacterId)> OnTapShowDetailLayer;
    
    bool init(const OnTapShowDetailLayer& callback);
    
    void scrollViewDidScroll(ScrollView *view);
    void scrollViewDidZoom(ScrollView *view){};
private:
    enum TAG_SPRITE{
        CHARACTER_LIST = 0,
        SCROLL_BAR,
    };
    enum Z_ORDER{
        Z_CHARACTER_LIST = 0,
        Z_SCROLL_BAR,
        
    };
    const int THRESHOLD = 20;
    const float ICON_SEIZE_HEIGHT =99.0f;
    const float ICON_SPACE = 20.f;
    OnTapShowDetailLayer _callback;
    
    Point _beganTouhPoint;
    bool _moveSCrollFlag;
    
    void showScrollView();
};

#endif