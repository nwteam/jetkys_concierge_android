#ifndef __syanago__ColectionSortDialog__
#define __syanago__ColectionSortDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"
#include "OthersColectionLayer.h"

USING_NS_CC;

class ColectionSortDialog : public Layer, public create_func<ColectionSortDialog>
{
public:
    ColectionSortDialog();
    ~ColectionSortDialog();
    
    typedef std::function<void(COLECTION_SORT_MODE mode)> OnSelectSortCallback;
    using create_func::create;
    bool init(const OnSelectSortCallback& callback, COLECTION_SORT_MODE mode);
    
private:
    enum TAG_SPRITE{
        BLACK_LAYER = 0,
        BACKGROUND,
        DESCRIPTION,
        SORT_BUTTON_NUNBER_FORWARD,
        SORT_BUTTON_NUNBER_REVERSE,
        SORT_BUTTON_RARITY_FORWARD,
        SORT_BUTTON_RARITY_REVERSE,
        CANCEL_BUTTON,
    };
    enum Z_ORDER{
        Z_BLACK_LAYER = 0,
        Z_BACKGROUND,
        Z_DESCRIPTION,
        Z_SORT_BUTTON,
        Z_CANCEL_BUTTON,
    };
    OnSelectSortCallback _callback;
    
    void showBlackLayer();
    void showBackground();
    void showDescriptionLabel();
    void showSortButtons(COLECTION_SORT_MODE mode);
    ui::Button* createSortButton(COLECTION_SORT_MODE mode, const std::string filePath, const std::string sortLabel);
    void onTapSortButton(Ref* pSender, ui::Widget::TouchEventType type, COLECTION_SORT_MODE mode);
    void showCancelButton();
    void onTapCancelButton(Ref* pSender, ui::Widget::TouchEventType type);
    
    void setTouchEvent();
};

#endif
