#include "ColectionSortDialog.h"
#include "SoundHelper.h"
#include "FontDefines.h"

ColectionSortDialog::ColectionSortDialog():
    _callback(nullptr)
{}
ColectionSortDialog::~ColectionSortDialog()
{}

bool ColectionSortDialog::init(const OnSelectSortCallback& callback, COLECTION_SORT_MODE mode)
{
    if (!Layer::create()) {
        return false;
    }
    _callback = callback;

    showBlackLayer();
    showBackground();
    showDescriptionLabel();
    showSortButtons(mode);
    showCancelButton();

    setTouchEvent();

    return true;
}

void ColectionSortDialog::showBlackLayer()
{
    auto blackLayer = LayerColor::create(Color4B(Color3B::BLACK));
    blackLayer->setTag(TAG_SPRITE::BLACK_LAYER);
    addChild(blackLayer, Z_ORDER::Z_BLACK_LAYER);
    blackLayer->setOpacity(180);
}

void ColectionSortDialog::showBackground()
{
    auto background = Sprite::create("sort_colection_base.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getWinSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void ColectionSortDialog::showDescriptionLabel()
{
    auto descriptionLabel = Label::createWithTTF("表示順を選んでください", FONT_NAME_2, 32);
    descriptionLabel->setTag(TAG_SPRITE::DESCRIPTION);
    descriptionLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    descriptionLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto background = getChildByTag(TAG_SPRITE::BACKGROUND);
    descriptionLabel->setPosition(background->getPosition() + Vec2(0, background->getContentSize().height / 2 - 40));
    addChild(descriptionLabel, Z_ORDER::Z_DESCRIPTION);
}

void ColectionSortDialog::showSortButtons(COLECTION_SORT_MODE mode)
{
    std::string forwardNumberSotrButtonImagePath = "sort_pink_button.png";
    std::string reverseNumberSortButtonImagePath = "sort_pink_button.png";
    std::string forwardRaritySotrButtonImagePath = "sort_pink_button.png";
    std::string reverseRaritySortButtonImagePath = "sort_pink_button.png";

    switch (mode) {
    case CSM_SORT_ID_FORWARD:
        forwardNumberSotrButtonImagePath = "sort_yellow_button.png";
        break;
    case CSM_SORT_ID_REVERSE:
        reverseNumberSortButtonImagePath = "sort_yellow_button.png";
        break;
    case CSM_RARITY_FORWARD:
        forwardRaritySotrButtonImagePath = "sort_yellow_button.png";
        break;
    case CSM_RARITY_REVERSE:
        reverseRaritySortButtonImagePath = "sort_yellow_button.png";
        break;
    default:
        break;
    }

    // ソートNO昇順
    auto forwardNumberSortButton = createSortButton(CSM_SORT_ID_FORWARD, forwardNumberSotrButtonImagePath, "No.昇順");
    forwardNumberSortButton->setTag(TAG_SPRITE::SORT_BUTTON_NUNBER_FORWARD);
    forwardNumberSortButton->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() + Point(-(20 + forwardNumberSortButton->getContentSize().width / 2), 10 + forwardNumberSortButton->getContentSize().height / 2));
    addChild(forwardNumberSortButton, Z_ORDER::Z_SORT_BUTTON);

    // ソートNO降順
    auto reverseNumberSortButton = createSortButton(CSM_SORT_ID_REVERSE, reverseNumberSortButtonImagePath, "No.降順");
    reverseNumberSortButton->setTag(TAG_SPRITE::SORT_BUTTON_NUNBER_REVERSE);
    reverseNumberSortButton->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() + Point(20 + reverseNumberSortButton->getContentSize().width / 2, 10 + reverseNumberSortButton->getContentSize().height / 2));
    addChild(reverseNumberSortButton, Z_ORDER::Z_SORT_BUTTON);

    // レアリティ昇順
    auto forwardRarirySortButton = createSortButton(CSM_RARITY_FORWARD, forwardRaritySotrButtonImagePath, "レア昇順");
    forwardRarirySortButton->setTag(TAG_SPRITE::SORT_BUTTON_RARITY_FORWARD);
    forwardRarirySortButton->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() + Point(-(20 + forwardRarirySortButton->getContentSize().width / 2), -(10 + forwardRarirySortButton->getContentSize().height / 2)));
    addChild(forwardRarirySortButton, Z_ORDER::Z_SORT_BUTTON);

    // レアリティ降順
    auto reverseRaritySortButton = createSortButton(CSM_RARITY_REVERSE, reverseRaritySortButtonImagePath, "レア降順");
    reverseRaritySortButton->setTag(TAG_SPRITE::SORT_BUTTON_RARITY_REVERSE);
    reverseRaritySortButton->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() + Point((20 + reverseRaritySortButton->getContentSize().width / 2), -(10 + reverseRaritySortButton->getContentSize().height / 2)));
    addChild(reverseRaritySortButton, Z_ORDER::Z_SORT_BUTTON);
}

ui::Button* ColectionSortDialog::createSortButton(COLECTION_SORT_MODE mode, const std::string filePath, const std::string sortLabel)
{
    auto result = ui::Button::create(filePath);
    result->addTouchEventListener(CC_CALLBACK_2(ColectionSortDialog::onTapSortButton, this, mode));
    auto resultLabel = Label::createWithTTF(sortLabel, FONT_NAME_2, 28);
    resultLabel->setPosition(Point(result->getContentSize().width / 2, result->getContentSize().height / 2 - 14));
    resultLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->addChild(resultLabel);
    return result;
}

void ColectionSortDialog::onTapSortButton(Ref* pSender, ui::Widget::TouchEventType type, COLECTION_SORT_MODE mode)
{
    if (type != ui::Widget::TouchEventType::ENDED) {
        return;
    }
    _callback(mode);
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    removeFromParent();
}

void ColectionSortDialog::showCancelButton()
{
    auto cancelButton = ui::Button::create("popupBack.png");
    cancelButton->setTag(TAG_SPRITE::CANCEL_BUTTON);
    cancelButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    cancelButton->setPosition(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition() + Vec2(0, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + 30));
    cancelButton->addTouchEventListener(CC_CALLBACK_2(ColectionSortDialog::onTapCancelButton, this));
    addChild(cancelButton, Z_ORDER::Z_CANCEL_BUTTON);
}

void ColectionSortDialog::onTapCancelButton(Ref* pSender, ui::Widget::TouchEventType type)
{
    if (type != ui::Widget::TouchEventType::ENDED) {
        return;
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    removeFromParent();
}

void ColectionSortDialog::setTouchEvent()
{
    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);
}
