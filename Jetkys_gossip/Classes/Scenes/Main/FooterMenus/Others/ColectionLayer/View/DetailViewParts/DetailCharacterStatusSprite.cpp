#include "DetailCharacterStatusSprite.h"
#include "FontDefines.h"
#include "BirthplaceModel.h"
#include "MakerModel.h"
#include "BodyTypeModel.h"

DetailCharacterStatusSprite::DetailCharacterStatusSprite()
{}

DetailCharacterStatusSprite::~DetailCharacterStatusSprite()
{}

DetailCharacterStatusSprite* DetailCharacterStatusSprite::create(const std::shared_ptr<CharacterModel> &characterModel)
{
    DetailCharacterStatusSprite* sprite = new DetailCharacterStatusSprite();
    if (sprite && sprite->initWithFile("other_colecion_base.png")) {
        sprite->showStatus(characterModel);
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

void DetailCharacterStatusSprite::showStatus(const std::shared_ptr<CharacterModel>& characterModel)
{
    showDescription(removeSpaces(characterModel->getFlavorText()));
    std::shared_ptr<BirthplaceModel>birthPlaceModel(BirthplaceModel::find(characterModel->getBirthplaceId()));
    std::shared_ptr<MakerModel>makerModel(MakerModel::find(characterModel->getMakerId()));
    showBirthPlaceAndMaker(birthPlaceModel->getName(), makerModel->getName());
    std::shared_ptr<BodyTypeModel>bodyTypeModel(BodyTypeModel::find(characterModel->getBodyType()));
    showBodyType(bodyTypeModel->getName());
    showCharacterNumber(characterModel->getSortNo());
    showCarTypeCertificate(characterModel->getName());
    showRarity(characterModel->getRarity());
}

void DetailCharacterStatusSprite::showDescription(const std::string description)
{
    auto descriptionScrollView = ScrollView::create();
    descriptionScrollView->setViewSize(Size(574, 185));
    descriptionScrollView->setDirection(ScrollView::Direction::VERTICAL);
    descriptionScrollView->setAnchorPoint(Point(0, 0));
    descriptionScrollView->setPosition(Point(38, 58));
    descriptionScrollView->setBounceable(false);
    descriptionScrollView->setTag(TAG_SPRITE::DESCRIPTION);

    auto descriptionLabel = Label::createWithTTF(description, FONT_NAME_4, 22);
    descriptionLabel->setDimensions(564, 0);
    descriptionLabel->setHorizontalAlignment(TextHAlignment::LEFT);
    descriptionLabel->setPosition(Point::ZERO);
    descriptionLabel->setAnchorPoint(Vec2::ZERO);
    descriptionLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto descriptionLayer = LayerColor::create(Color4B(0, 0, 0, 0), 574, descriptionLabel->getContentSize().height);
    descriptionLayer->addChild(descriptionLabel);
    descriptionScrollView->setContainer(descriptionLayer);
    descriptionScrollView->setContentOffset(Point(-(descriptionLayer->getContentSize().width - descriptionScrollView->getViewSize().width),
                                                  -(descriptionLayer->getContentSize().height - descriptionScrollView->getViewSize().height)));
    addChild(descriptionScrollView, Z_ORDER::Z_DESCRIPTION);
}

void DetailCharacterStatusSprite::showBirthPlaceAndMaker(const std::string birthPlaceName, const std::string makerName)
{
    auto birthPlaceAndMakerLabel = Label::createWithTTF(birthPlaceName + " / " + makerName, FONT_NAME_2, 22);
    birthPlaceAndMakerLabel->setTag(TAG_SPRITE::BIRTH_PLACE_AND_MAKER_NAME);
    birthPlaceAndMakerLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    auto descriptionScrollView = static_cast<ScrollView*>(getChildByTag(TAG_SPRITE::DESCRIPTION));
    birthPlaceAndMakerLabel->setPosition(descriptionScrollView->getPosition() + Vec2(100,
                                                                                     descriptionScrollView->getViewSize().height + 4));
    birthPlaceAndMakerLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(birthPlaceAndMakerLabel, Z_ORDER::Z_BIRTH_PLACE_AND_MAKER_NAME);
}

void DetailCharacterStatusSprite::showBodyType(const std::string bodyTypeName)
{
    auto bodyTypeLabel = Label::createWithTTF(bodyTypeName, FONT_NAME_2, 22);
    bodyTypeLabel->setTag(TAG_SPRITE::BODY_TYPE);
    bodyTypeLabel->setAnchorPoint(Point(0, 0));
    bodyTypeLabel->setPosition(Point(getChildByTag(TAG_SPRITE::DESCRIPTION)->getPositionX() + 170,
                                     getChildByTag(TAG_SPRITE::BIRTH_PLACE_AND_MAKER_NAME)->getPositionY() + 50));
    bodyTypeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(bodyTypeLabel, Z_ORDER::Z_BODY_TYPE);
}

void DetailCharacterStatusSprite::showCharacterNumber(const int characterNumber)
{
    std::string charaNo;
    switch (StringUtils::format("%d", characterNumber).size()) {
    case 1:
        charaNo = "00" + StringUtils::format("%d", characterNumber);
        break;
    case 2:
        charaNo = "0" + StringUtils::format("%d", characterNumber);
        break;
    default:
        charaNo = StringUtils::format("%d", characterNumber);
        break;
    }
    auto numberLabel = Label::createWithTTF("No." + charaNo, FONT_NAME_2, 26);
    numberLabel->setTag(TAG_SPRITE::NUMBER);
    numberLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    numberLabel->setPosition(Point(26,
                                   getChildByTag(TAG_SPRITE::BODY_TYPE)->getPositionY() + 25));
    numberLabel->setColor(COLOR_D_ORANGEE);
    addChild(numberLabel, Z_ORDER::Z_NUMBER);
}

void DetailCharacterStatusSprite::showCarTypeCertificate(const std::string typeCertificate)
{
    auto typeCertificateLabel = Label::createWithTTF("[型式　" + typeCertificate + "]", FONT_NAME_2, 19);
    typeCertificateLabel->setTag(TAG_SPRITE::TYPE_CERTIFICATE);
    auto numberLabel = getChildByTag(TAG_SPRITE::NUMBER);
    typeCertificateLabel->setPosition(numberLabel->getPosition() + Point(numberLabel->getContentSize().width + 25,
                                                                         9));
    typeCertificateLabel->setColor(COLOR_D_ORANGEE);
    typeCertificateLabel->setAnchorPoint(Point(0, 0));
    addChild(typeCertificateLabel, Z_ORDER::Z_TYPE_CERTIFICATE);
}

void DetailCharacterStatusSprite::showRarity(const int rarity)
{
    auto rarityBasePosition = Point(getContentSize().width - 33,
                                    getChildByTag(TAG_SPRITE::NUMBER)->getPosition().y + 41);
    for (int i = 1; i <= rarity; i++) {
        auto raritySprite = Sprite::create("other_rarity.png");
        raritySprite->setTag(TAG_SPRITE::RARITY + i - 1);
        raritySprite->setPosition(rarityBasePosition);
        rarityBasePosition -= Vec2(28, 0);
        addChild(raritySprite, Z_ORDER::Z_RARITY);
    }
}