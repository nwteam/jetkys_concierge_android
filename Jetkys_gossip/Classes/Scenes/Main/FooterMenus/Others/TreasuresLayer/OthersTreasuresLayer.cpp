#include "OthersTreasuresLayer.h"
#include "TreasureItemView.h"
#include "FontDefines.h"
#include "SoundHelper.h"

OthersTreasuresLayer::OthersTreasuresLayer():
    _progress(nullptr), _request(nullptr), _callback(nullptr)
{
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}
OthersTreasuresLayer::~OthersTreasuresLayer()
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("icon_treasure.plist");

    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

bool OthersTreasuresLayer::init(const OnTapBackButtonCallBack& callback)
{
    if (!Layer::create()) {
        return false;
    }
    _callback = callback;

    _viewTreasuresPage = 0;

    showBackground();
    showHeadLine();
    showTreasuresBase();

    showChangeTreasuresButton();

    _progress->onStart();
    _request->getTresureList(CC_CALLBACK_1(OthersTreasuresLayer::responseGetTreasures, this));
    
    return true;
}

void OthersTreasuresLayer::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Director::getInstance()->getVisibleSize() / 2);
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void OthersTreasuresLayer::showHeadLine()
{
    auto headLineBase = Sprite::create("head_line_no1.png");
    headLineBase->setTag(TAG_SPRITE::HEAD_LINE);
    headLineBase->setPosition(Vec2(headLineBase->getContentSize().width / 2, Director::getInstance()->getWinSize().height - headLineBase->getContentSize().height / 2 - 14));
    auto backButton = ui::Button::create("back_button.png");
    backButton->addTouchEventListener([this](Ref* pSender, ui::Widget::TouchEventType type) {
        _callback();
    });
    headLineBase->addChild(backButton);
    backButton->setPosition(Vec2(14 + backButton->getContentSize().width / 2, headLineBase->getContentSize().height / 2));
    auto titleLabel = Label::createWithTTF("宝物図鑑", FONT_NAME_2, 30);
    titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    titleLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
    titleLabel->setPosition(backButton->getPosition() + Point(backButton->getContentSize().width / 2 + 10, -15));
    titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    headLineBase->addChild(titleLabel);
    addChild(headLineBase, Z_ORDER::Z_HEAD_LINE);
}

void OthersTreasuresLayer::showTreasuresBase()
{
    auto treasuresBase = Sprite::create("other_tre_base.png");
    treasuresBase->setTag(TAG_SPRITE::TREASURES_BASE);
    treasuresBase->setPosition(Director::getInstance()->getWinSize() / 2);
    addChild(treasuresBase, Z_ORDER::Z_TREASURES_BASE);
}

void OthersTreasuresLayer::showChangeTreasuresButton()
{
    auto nextButton = ui::Button::create("other_tre_right_button.png");
    nextButton->setTag(TAG_SPRITE::NEXT_TREASURES_BUTTON);
    nextButton->addTouchEventListener(CC_CALLBACK_2(OthersTreasuresLayer::onTapNextTreasuresButton, this));
    nextButton->setVisible(false);
    nextButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    nextButton->setPosition(getChildByTag(TAG_SPRITE::TREASURES_BASE)->getPosition() + Point(140, -getChildByTag(TAG_SPRITE::TREASURES_BASE)->getContentSize().height / 2 + 117));
    addChild(nextButton, Z_ORDER::Z_NEXT_TREASURES_BUTTON);

    auto previousButton = ui::Button::create("other_tre_left_button.png");
    previousButton->setTag(TAG_SPRITE::PREVIOUS_TREASURES_BUTTON);
    previousButton->addTouchEventListener(CC_CALLBACK_2(OthersTreasuresLayer::onTapPreviousTreasuresButton, this));
    previousButton->setVisible(false);
    previousButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    previousButton->setPosition(getChildByTag(TAG_SPRITE::TREASURES_BASE)->getPosition() + Point(-140, -getChildByTag(TAG_SPRITE::TREASURES_BASE)->getContentSize().height / 2 + 117));
    addChild(previousButton, Z_ORDER::Z_PREVIOUS_TREASURES_BUTTON);
}

void OthersTreasuresLayer::onTapNextTreasuresButton(Ref* pSender, ui::Widget::TouchEventType type)
{
    if (type != ui::Widget::TouchEventType::ENDED) {
        return;
    }
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    _viewTreasuresPage++;
    
    removeChildByTag(TAG_SPRITE::TREASURES_VIEW);
    removeChildByTag(TAG_SPRITE::PAGE_LABEL);
    showTreasurePageLabel(_viewTreasuresPage);
    showTreasureView(_viewTreasuresPage);
}

void OthersTreasuresLayer::onTapPreviousTreasuresButton(Ref* pSender, ui::Widget::TouchEventType type)
{
    if (type != ui::Widget::TouchEventType::ENDED) {
        return;
    }
    if (_viewTreasuresPage >= 0) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _viewTreasuresPage--;
        
        removeChildByTag(TAG_SPRITE::TREASURES_VIEW);
        removeChildByTag(TAG_SPRITE::PAGE_LABEL);
        showTreasurePageLabel(_viewTreasuresPage);
        showTreasureView(_viewTreasuresPage);
    }
}

void OthersTreasuresLayer::responseGetTreasures(Json* response)
{
    std::string result = Json_getString(Json_getItem(response, "get_treasure_list"), "result", "false");
    if (result == "true") {
        std::shared_ptr<TreasuresItemModel>treasuresItemModel(new TreasuresItemModel(response));
        _treasuresItemModel = treasuresItemModel;

        _viewTreasuresPage = 0;
        
        showTreasurePageLabel(_viewTreasuresPage);
        showTreasureView(_viewTreasuresPage);
    }
}

void OthersTreasuresLayer::showTreasurePageLabel(const int viewPageNumber)
{
    auto pageLabel = Label::createWithTTF(StringUtils::format("%d / %lu", viewPageNumber + 1, _treasuresItemModel->getTreasureItems().size()), FONT_NAME_2, 22);
    pageLabel->setTag(TAG_SPRITE::PAGE_LABEL);
    pageLabel->setPosition(Vec2(Director::getInstance()->getVisibleSize() / 2) + Vec2(0,-getChildByTag(TAG_SPRITE::TREASURES_BASE)->getContentSize().height / 2 + 106));
    pageLabel->setColor(COLOR_D_ORANGEE);
    addChild(pageLabel, Z_ORDER::Z_PAGE_LABEL);
}

void OthersTreasuresLayer::showTreasureView(const int viewPageNumber)
{
    auto previousButton = static_cast<ui::Button*>(getChildByTag(TAG_SPRITE::PREVIOUS_TREASURES_BUTTON));
    if (previousButton) {
        if (viewPageNumber > 0) {
            previousButton->setVisible(true);
        } else {
            previousButton->setVisible(false);
        }
    }
    auto nextButton = static_cast<ui::Button*>(getChildByTag(TAG_SPRITE::NEXT_TREASURES_BUTTON));
    if (nextButton) {
        if (viewPageNumber < (_treasuresItemModel->getTreasureItems().size() - 1)) {
            nextButton->setVisible(true);
        } else {
            nextButton->setVisible(false);
        }
    }
    auto treasureView = TreasureItemView::create(_treasuresItemModel->getTreasureItems().at(viewPageNumber));
    treasureView->setTag(TAG_SPRITE::TREASURES_VIEW);
    addChild(treasureView, Z_ORDER::Z_TREASURES_VIEW);
}
