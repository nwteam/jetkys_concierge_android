#ifndef __syanago__TreasureItemView__
#define __syanago__TreasureItemView__

#include "cocos2d.h"
#include "create_func.h"
#include "TreasuresItemModel.h"

USING_NS_CC;

class TreasureItemView : public Layer, public create_func<TreasureItemView>
{
public:
    TreasureItemView();
    ~TreasureItemView();
    using create_func::create;
    bool init(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel);
    
private:
    enum TAG_SPRITE {
        ICON = 0,
        DESCRIPTION,
        EFFECT_PERCENT,
        STAR,
        ITEM_UNIT_VIEW = 12,
    };
    enum Z_ORDER{
        Z_ICON = 0,
        Z_DESCRIPTION,
        Z_EFFECT_PERCENT,
        Z_STAR,
        Z_ITEM_UNIT_VIEW,
    };
    
    void showIcon(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel);
    void showDescription(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel);
    void showTotalKindStar(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel);
    void showEffectPercent(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel);
    void showItemUnitView(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel);
};

#endif
