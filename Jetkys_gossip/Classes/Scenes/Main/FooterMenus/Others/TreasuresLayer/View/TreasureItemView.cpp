#include "TreasureItemView.h"
#include "TreasureItemUnitView.h"
#include "FontDefines.h"

#include "jcommon.h"
#include "CourseModel.h"

TreasureItemView::TreasureItemView()
{}

TreasureItemView::~TreasureItemView()
{}

bool TreasureItemView::init(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel)
{
    if (!Layer::create()) {
        return false;
    }
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon_treasure.plist");

    showIcon(treasureItemModel);
    showDescription(treasureItemModel);
    if (treasureItemModel->getIsOpenTreasureBox() == true) {
        showTotalKindStar(treasureItemModel);
    }
    showEffectPercent(treasureItemModel);
    showItemUnitView(treasureItemModel);

    return true;
}

void TreasureItemView::showIcon(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel)
{
    auto iconBase = makeSprite("other_tre_icon_big_base.png");
    iconBase->setTag(TAG_SPRITE::ICON);
    Sprite* iconSprite;
    if (treasureItemModel->getIsOpenTreasureBox() == true) {
        iconSprite = Sprite::create("others_tre_openicon.png");
    } else  {
        iconSprite = makeSprite("others_tre_closeicon.png");
    }
    iconSprite->setPosition(Point(iconBase->getContentSize() / 2));
    iconBase->addChild(iconSprite);
    iconBase->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(-292 + iconBase->getContentSize().width / 2,
                                                                                 306.5 - iconBase->getContentSize().width / 2));
    addChild(iconBase, Z_ORDER::Z_ICON);
}

void TreasureItemView::showDescription(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel)
{
    auto descriptionBase = Sprite::create("other_tre_big_discription_base.png");
    descriptionBase->setTag(TAG_SPRITE::DESCRIPTION);
    descriptionBase->setPosition(Point(Director::getInstance()->getWinSize().width / 2,
                                       getChildByTag(TAG_SPRITE::ICON)->getPosition().y - getChildByTag(TAG_SPRITE::ICON)->getContentSize().height / 2 - 10 - descriptionBase->getContentSize().height / 2));
    std::string description;
    if (treasureItemModel->getIsOpenTreasureBox() == true) {
        description = treasureItemModel->getItemModel()->getName();
    } else  {
        description = "????";
    }
    auto descriptionLabe = Label::createWithTTF(description, FONT_NAME_2, 19);
    descriptionLabe->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    descriptionLabe->setDimensions(descriptionBase->getContentSize().width - 10, descriptionBase->getContentSize().height - 10);
    descriptionLabe->setAnchorPoint(Vec2(0, 1));
    descriptionLabe->setPosition(Point(5, descriptionBase->getContentSize().height - 5));
    descriptionBase->addChild(descriptionLabe);
    addChild(descriptionBase, Z_ORDER::Z_DESCRIPTION);
}

void TreasureItemView::showTotalKindStar(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel)
{
    auto starPosition = Point(getChildByTag(TAG_SPRITE::ICON)->getPosition().x + getChildByTag(TAG_SPRITE::ICON)->getContentSize().width / 2 + 18,
                              getChildByTag(TAG_SPRITE::ICON)->getPosition().y + getChildByTag(TAG_SPRITE::ICON)->getContentSize().height / 2 - 12.5);
    for (int i = 1; i <= treasureItemModel->getTotalItemUnitKind(); i++) {
        auto starSprite = Sprite::create("other_rarity.png");
        starSprite->setTag(TAG_SPRITE::STAR + i - 1);
        starSprite->setPosition(starPosition);
        addChild(starSprite, Z_ORDER::Z_STAR);

        starPosition += Vec2(28, 0);
    }
}

void TreasureItemView::showEffectPercent(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel)
{
    int percent = 0;
    if (treasureItemModel->getIsOpenTreasureBox() == true) {
        if (treasureItemModel->getTotalItemUnitKind() == 9) {
            percent = 100;
        } else {
            percent = treasureItemModel->getTotalItemUnitKind() * 11;
        }
    }
    auto effectPercentBase = Sprite::create("other_tre_parsent_base.png");
    effectPercentBase->setTag(TAG_SPRITE::EFFECT_PERCENT);
    effectPercentBase->setPosition(Point(Director::getInstance()->getWinSize().width / 2 + getChildByTag(TAG_SPRITE::ICON)->getContentSize().width / 2 + 5,
                                     getChildByTag(TAG_SPRITE::ICON)->getPosition().y - getChildByTag(TAG_SPRITE::ICON)->getContentSize().height / 2 + effectPercentBase->getContentSize().height / 2));
    auto percentLabel = Label::createWithTTF(StringUtils::format("%d", percent), FONT_NAME_2, 28);
    percentLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    percentLabel->setAnchorPoint(Vec2(1, 0.5));
    percentLabel->setPosition(Point(effectPercentBase->getContentSize().width - 40, effectPercentBase->getContentSize().height / 2 - 14));
    effectPercentBase->addChild(percentLabel);
    addChild(effectPercentBase, Z_ORDER::Z_EFFECT_PERCENT);
}

void TreasureItemView::showItemUnitView(const std::shared_ptr<TreasuresItemModel::TreasureItem>& treasureItemModel)
{
    auto posi = Point(Director::getInstance()->getWinSize().width / 2, getChildByTag(TAG_SPRITE::DESCRIPTION)->getPosition().y - getChildByTag(TAG_SPRITE::DESCRIPTION)->getContentSize().height / 2 - 20);
    for (int i = 0; i < 3; i++) {
        auto treasureItemUnitModel = treasureItemModel->getTreasureItemUnits().at(i);
        if (treasureItemUnitModel != nullptr) {
            auto treasureItemUnitView = TreasureItemUnitView::create(treasureItemUnitModel);
            treasureItemUnitView->setTag(TAG_SPRITE::ITEM_UNIT_VIEW + i);
            treasureItemUnitView->setPosition(posi);
            addChild(treasureItemUnitView, Z_ORDER::Z_ITEM_UNIT_VIEW);
            posi -= Vec2(0, treasureItemUnitView->getContentSize().height + 10);
        }
    }
}