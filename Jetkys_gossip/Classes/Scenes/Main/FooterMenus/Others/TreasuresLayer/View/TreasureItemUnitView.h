#ifndef __syanago__TreasureItemUnitView__
#define __syanago__TreasureItemUnitView__

#include "cocos2d.h"
#include "TreasuresItemModel.h"

USING_NS_CC;

class TreasureItemUnitView : public Sprite
{
public:
    TreasureItemUnitView();
    ~TreasureItemUnitView();
    
    static TreasureItemUnitView* create(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model);
    void initialize(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model);
    
private:
    enum TAG_SPRITE{
        ICON,
        NAME,
        DROP_COURSE,
        NON_ITEM_UNIT_TEXT,
        STAR,
    };
    enum Z_ORDER{
        Z_ICON,
        Z_NAME,
        Z_DROP_COURSE,
        Z_NON_ITEM_UNIT_TEXT,
        Z_STAR,
    };
    const std::string STAGE = "出現ステージ：";
    const std::string FROME_TO = "〜";
    
    void showIcon(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model);
    void showName(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model);
    void showDropCourse(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model);
    void showStar(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model);
    void showNonItemUnitText();
};

#endif
