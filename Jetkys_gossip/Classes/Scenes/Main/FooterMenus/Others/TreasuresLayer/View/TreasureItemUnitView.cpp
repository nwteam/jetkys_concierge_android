#include "TreasureItemUnitView.h"
#include "CourseModel.h"
#include "FontDefines.h"
#include "jCommon.h"

TreasureItemUnitView::TreasureItemUnitView()
{}

TreasureItemUnitView::~TreasureItemUnitView()
{}

TreasureItemUnitView* TreasureItemUnitView::create(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model)
{
    TreasureItemUnitView* btn = new (std::nothrow)  TreasureItemUnitView();
    if (btn && btn->initWithFile("other_tre_small_discription_base.png")) {
        btn->initialize(model);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void TreasureItemUnitView::initialize(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model)
{
    setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);

    showIcon(model);
    showName(model);
    showDropCourse(model);
    if (model->getItemUnitKind() > 0) {
        showStar(model);
    } else {
        showNonItemUnitText();
    }
}

void TreasureItemUnitView::showIcon(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model)
{
    auto iconBase = Sprite::create("other_tre_icon_small_base.png");
    iconBase->setTag(TAG_SPRITE::ICON);
    iconBase->setPosition(Point(iconBase->getContentSize().width / 2 + 12, getContentSize().height / 2));
    Sprite* icon;
    if (model->getItemUnitKind() > 0) {
        icon = makeSprite(StringUtils::format("%d_treasure.png", model->getItemUnitModel()->getID()).c_str());
    } else {
        icon = Sprite::create("others_tre_noicon.png");
    }
    iconBase->addChild(icon);
    icon->setPosition(Point(iconBase->getContentSize() / 2));
    addChild(iconBase, Z_ORDER::Z_ICON);
}

void TreasureItemUnitView::showName(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model)
{
    std::string treasureItemName;
    if (model->getItemUnitKind() > 0) {
        treasureItemName = model->getItemUnitModel()->getName();
    } else {
        treasureItemName = "???";
    }
    auto treasureItemNameLabel = Label::createWithTTF(treasureItemName, FONT_NAME_2, 22);
    treasureItemNameLabel->setTag(TAG_SPRITE::NAME);
    treasureItemNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    treasureItemNameLabel->setColor(COLOR_PINK);
    treasureItemNameLabel->setPosition(Point(getChildByTag(TAG_SPRITE::ICON)->getPosition().x + getChildByTag(TAG_SPRITE::ICON)->getContentSize().width / 2 + 12 + treasureItemNameLabel->getContentSize().width / 2,
                                             getChildByTag(TAG_SPRITE::ICON)->getPosition().y + getChildByTag(TAG_SPRITE::ICON)->getContentSize().height / 2 - 22));
    addChild(treasureItemNameLabel, Z_ORDER::Z_NAME);
}

void TreasureItemUnitView::showDropCourse(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model)
{
    std::string dropCourse;
    std::shared_ptr<CourseModel>startCourseModel(CourseModel::find(model->getItemUnitModel()->getFromCourse()));
    std::shared_ptr<CourseModel>endCourseModel(CourseModel::find(model->getItemUnitModel()->getToCourse()));
    if (endCourseModel != nullptr && startCourseModel != nullptr) {
        dropCourse = STAGE + StringUtils::format("%d", startCourseModel->getNo()) + FROME_TO + StringUtils::format("%d", endCourseModel->getNo());
    } else {
        dropCourse = STAGE + "??" + FROME_TO + "??";
    }
    auto dropCourseLabel = Label::createWithTTF(dropCourse, FONT_NAME_2, 19);
    dropCourseLabel->setTag(TAG_SPRITE::DROP_COURSE);
    dropCourseLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    dropCourseLabel->setPosition(Point(getChildByTag(TAG_SPRITE::ICON)->getPosition().x + getChildByTag(TAG_SPRITE::ICON)->getContentSize().width / 2 + 12 + dropCourseLabel->getContentSize().width / 2,
                                       getChildByTag(TAG_SPRITE::NAME)->getPosition().y - 2 - getChildByTag(TAG_SPRITE::NAME)->getContentSize().height / 2));
    addChild(dropCourseLabel, Z_ORDER::Z_DROP_COURSE);
}

void TreasureItemUnitView::showStar(const std::shared_ptr<TreasuresItemModel::TreasureItem::TreasureItemUnit>& model)
{
    auto starPosition = Point(getContentSize().width - 12 - 13, getChildByTag(TAG_SPRITE::NAME)->getPosition().y + 11);
    for (int i = 1; i <= model->getItemUnitKind(); i++) {
        auto starSprite = Sprite::create("other_rarity.png");
        starSprite->setTag(TAG_SPRITE::STAR + i - 1);
        starSprite->setPosition(starPosition);
        starPosition -= Vec2(26 + 2, 0);
        addChild(starSprite, Z_ORDER::Z_STAR);
    }
}

void TreasureItemUnitView::showNonItemUnitText()
{
    auto textLabel = Label::createWithTTF("全て集めると新しい効果が発動できます!", FONT_NAME_2, 19);
    textLabel->setTag(TAG_SPRITE::NON_ITEM_UNIT_TEXT);
    textLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    textLabel->setPosition(Point(getChildByTag(TAG_SPRITE::ICON)->getPosition().x + getChildByTag(TAG_SPRITE::ICON)->getContentSize().width / 2 + 12 + textLabel->getContentSize().width / 2,
                                 getChildByTag(TAG_SPRITE::DROP_COURSE)->getPosition().y - 2 - getChildByTag(TAG_SPRITE::DROP_COURSE)->getContentSize().height / 2));

    addChild(textLabel, Z_ORDER::Z_NON_ITEM_UNIT_TEXT);
}