#ifndef __syanago__TreasuresItemModel__
#define __syanago__TreasuresItemModel__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"
#include "ItemModel.h"
#include "ItemUnitModel.h"

USING_NS_CC;

class TreasuresItemModel
{
public:
    TreasuresItemModel(Json* response);
    ~TreasuresItemModel();
    
    class TreasureItem
    {
    public:
        TreasureItem(Json* item);
        ~TreasureItem();
        enum ITEM_UNIT_NUMBER{
            LEFT = 0,
            CENTER,
            RIGHT,
        };
        
        CC_SYNTHESIZE_READONLY(std::shared_ptr<ItemModel>, _itemModel, ItemModel);
        CC_SYNTHESIZE_READONLY(bool, _isOpenTreasureBox, IsOpenTreasureBox);
        CC_SYNTHESIZE_READONLY(int, _totalItemUnitKind, TotalItemUnitKind);
        class TreasureItemUnit
        {
        public:
            TreasureItemUnit(Json* itemUnit);
            ~TreasureItemUnit();
            
            CC_SYNTHESIZE_READONLY(std::shared_ptr<ItemUnitModel>, _itemUnitModel, ItemUnitModel);
            CC_SYNTHESIZE_READONLY(int, _itemUnitKind, ItemUnitKind);
        private:
            const int convertTreasureKind(const int itemUnitkind);
        };
        CC_SYNTHESIZE_READONLY(std::vector<std::shared_ptr<TreasureItemUnit>>, _treasureItemUnits, TreasureItemUnits);
    private:
        const std::string getItemUnitColumnName(const int treasureItemUnitVectorNumber);
    };
    CC_SYNTHESIZE_READONLY(std::vector<std::shared_ptr<TreasureItem>>, _treasureItems, TreasureItems);
};

#endif
