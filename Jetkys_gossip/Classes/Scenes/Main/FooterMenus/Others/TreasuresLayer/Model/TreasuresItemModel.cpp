#include "TreasuresItemModel.h"

TreasuresItemModel::TreasuresItemModel(Json* response)
{
    _treasureItems.clear();
    Json* items = Json_getItem(Json_getItem(Json_getItem(response, "get_treasure_list"), "data"), "item");
    for (Json* child = items->child; child; child = child->next) {
        std::shared_ptr<TreasureItem>model(new TreasureItem(child));
        if (model->getItemModel() != nullptr) {
            _treasureItems.push_back(model);
        }
    }
}

TreasuresItemModel::~TreasuresItemModel()
{}


TreasuresItemModel::TreasureItem::TreasureItem(Json* item)
{
    std::shared_ptr<ItemModel>itemModel(ItemModel::find(atoi(Json_getString(item, "item_id", ""))));
    _itemModel = itemModel;
    if (_itemModel != nullptr) {
        _treasureItemUnits.clear();
        int totalItemUnitCount = 0;
        int holdItemUnitCount = 0;
        _totalItemUnitKind = 0;
        for (int i = 0; i < 3; i++) {
            Json* ItemUnit = Json_getItem(item, getItemUnitColumnName(i).c_str());
            if (ItemUnit) {
                std::shared_ptr<TreasureItemUnit>itemUnitModel(new TreasureItemUnit(ItemUnit));
                if (itemUnitModel->getItemUnitModel() != nullptr) {
                    totalItemUnitCount++;
                    if (itemUnitModel->getItemUnitKind() > 0) {
                        holdItemUnitCount++;
                        _totalItemUnitKind += itemUnitModel->getItemUnitKind();
                    }
                    _treasureItemUnits.push_back(itemUnitModel);
                } else {
                    _treasureItemUnits.push_back(nullptr);
                }
            } else {
                _treasureItemUnits.push_back(nullptr);
            }
        }

        if (totalItemUnitCount == holdItemUnitCount) {
            _isOpenTreasureBox = true;
        } else {
            _isOpenTreasureBox = false;
        }
    }
}

TreasuresItemModel::TreasureItem::~TreasureItem()
{}

const std::string TreasuresItemModel::TreasureItem::getItemUnitColumnName(const int treasureItemUnitVectorNumber)
{
    if (treasureItemUnitVectorNumber == 0) {
        return "left";
    } else if (treasureItemUnitVectorNumber == 1) {
        return "center";
    } else if (treasureItemUnitVectorNumber == 2) {
        return "right";
    }
    CC_ASSERT("error");
    return "";
}


TreasuresItemModel::TreasureItem::TreasureItemUnit::TreasureItemUnit(Json* itemUnit)
{
    std::shared_ptr<ItemUnitModel>itemUnitModel(ItemUnitModel::find(atoi(Json_getString(itemUnit, "item_unit_id", ""))));
    if (itemUnitModel != nullptr) {
        _itemUnitModel = itemUnitModel;
        _itemUnitKind = convertTreasureKind(atoi(Json_getString(itemUnit, "item_unit_kind", "")));
    } else {
        _itemUnitModel = nullptr;
        _itemUnitKind = 0;
    }
}

TreasuresItemModel::TreasureItem::TreasureItemUnit::~TreasureItemUnit()
{}


const int TreasuresItemModel::TreasureItem::TreasureItemUnit::convertTreasureKind(const int itemUnitkind)
{
    switch (itemUnitkind) {
    case 1: {
        return 3;
    }
    case 2: {
        return 2;
    }
    case 3: {
        return 1;
    }
    default:
        return 0;
    }
}