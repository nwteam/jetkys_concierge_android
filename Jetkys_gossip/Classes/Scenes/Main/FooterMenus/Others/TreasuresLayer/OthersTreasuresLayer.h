#ifndef __syanago__OthersTreasuresLayer__
#define __syanago__OthersTreasuresLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "create_func.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"
#include "TreasuresItemModel.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace SyanagoAPI;

class OthersTreasuresLayer : public Layer, public create_func<OthersTreasuresLayer>{
public:
    OthersTreasuresLayer();
    ~OthersTreasuresLayer();
    typedef std::function<void()> OnTapBackButtonCallBack;
    using create_func::create;
    bool init(const OnTapBackButtonCallBack& callback);
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        HEAD_LINE,
        TREASURES_BASE,
        TREASURES_VIEW,
        NEXT_TREASURES_BUTTON,
        PREVIOUS_TREASURES_BUTTON,
        PAGE_LABEL,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_HEAD_LINE,
        Z_TREASURES_BASE,
        Z_TREASURES_VIEW,
        Z_NEXT_TREASURES_BUTTON,
        Z_PREVIOUS_TREASURES_BUTTON,
        Z_PAGE_LABEL,
    };
    DefaultProgress* _progress;
    RequestAPI* _request;
    OnTapBackButtonCallBack _callback;
    std::shared_ptr<TreasuresItemModel> _treasuresItemModel;
    
    
    int _positionH = 20;
    int _viewTreasuresPage;
    
    
    
    void showBackground();
    void showHeadLine();
    void showTreasuresBase();
    void showChangeTreasuresButton();
    void onTapNextTreasuresButton(Ref* pSender, ui::Widget::TouchEventType type);
    void onTapPreviousTreasuresButton(Ref* pSender, ui::Widget::TouchEventType type);
    
    void responseGetTreasures(Json* response);
    void showTreasurePageLabel(const int viewPageNumber);
    void showTreasureView(const int viewPageNumber);
    
};

#endif /* defined(__syanago__OthersTreasuresLayer__) */
