#ifndef __syanago__OthersLayer__
#define __syanago__OthersLayer__

#include "cocos2d.h"
#include "OthersUseURL.h"
#include "SoundHelper.h"
#include "MenuTouch.h"
#include "DialogView.h"

USING_NS_CC;

class MainScene;

enum OthersIndex{
    OI_MAIN, OI_INFOMATION, OI_EVENT, OI_COLECTION, OI_TUTOLI, OI_CAMPAIGN, OI_OPTION, OI_HELP, OI_BUYLOG, OI_CONTACT, OI_MODELCHANGE, OI_RULE, OI_SETTLEMENT, OI_NOTICE, OI_BACKTITLE, OI_TREASURES, OI_PRESENT
};

enum OtyersUrlWebViewTag {
    OI_WEB_EVENT,
    OI_WEB_HELP,
};

class OthersLayer : public Layer, public DialogDelegate {
public:
    bool init();
    CREATE_FUNC(OthersLayer);
    
    MainScene *_mainScene;
    
    CC_SYNTHESIZE(OthersIndex, _othersIndex, OIndex);
    
    void hiddenMenu();
    void viewMenu();
    
    void showLayer(OthersIndex nextLayer);
    
    void showHtmlTagWebView(std::string htmlTag);
    void showUrlWebView(OtyersUrlWebViewTag tag);
    void deleteWebView();
    void resetPageTag();
    void showHtmldialog();    
    
    void updateInfoLayer();
    void updateBage();
    void removeBlackImage();
    
    OthersIndex getNowViewOthersLayer();
    
    void btDialogCallback(ButtonIndex aIndex, int tag);
    
    void showNoticeLunchBrowser(std::string url);
    
private:
    std::string _url;
    
    void onTapPageBack();
    
};


#endif /* defined(__syanago__OthersLayer__) */
