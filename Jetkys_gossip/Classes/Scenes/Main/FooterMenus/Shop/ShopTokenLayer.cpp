#include "ShopTokenLayer.h"
#include "ShopLayer.h"
#include "FontDefines.h"
#include "Cell.h"
#include "DialogView.h"
#include "PlayerController.h"
#include "GameDefines.h"
#include "ShopTokenAgePopupLayer.h"
#include "InAppPurchaseModel.h"
#include "PurchaseSuccessDialog.h"
#include "HeaderStatus.h"

#define TAG_AGE_CONFIRM_POPUP 12345

#define SIZE_TOKEN_CELL Sprite::create("shop_base.png")->getContentSize()

ShopTokenLayer::ShopTokenLayer():
    _buyID(-1), _totalPurchases(0), _numberOfCell(0), progress(nullptr), request(nullptr), _scrollBar(nullptr)
{
    _title = "トークンショップ";
}

void ShopTokenLayer::onExit()
{
    ShopLayerBase::onExit();
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("icon_tokens.plist");
}

bool ShopTokenLayer::init()
{
    if (!ShopLayerBase::init()) {
        return false;
    }

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon_tokens.plist");
    return true;
}

void ShopTokenLayer::onEnter()
{
    ShopLayerBase::onEnter();
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
    progress->onStart();
    request->getPayTokenItem(CC_CALLBACK_1(ShopTokenLayer::onResponseGetTokenList, this));
}

void ShopTokenLayer::onResponseGetTokenList(Json* response)
{
    delete progress;
    delete request;
    progress = nullptr;
    request = nullptr;
    std::string result = Json_getString(Json_getItem(response, "get_pay_item"), "result", "false");
    if (result == "true") {
        Json* mDatas = Json_getItem(Json_getItem(Json_getItem(response, "get_pay_item"), "data"), "pay_items");
        if (mDatas && mDatas->type == Json_Array) {
            Json* child;
            for (child = mDatas->child; child; child = child->next) {
                std::shared_ptr<Token>model(new Token(child));
                tokens.push_back(model);
            }
            auto popup = ShopTokenAgePopupLayer::create();
            addChild(popup, ZORDER_POPUP);
            popup->_delegate = this;

            showTable();
        }
    }
}

void ShopTokenLayer::btCallback(int btIndex, void* object)
{
    if (btIndex == STA_BT_BACK) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _shopLayer->showLayer(SI_MAIN);         // back to shop main
    }
    if (btIndex == STA_BT_20) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        int maxMoney = btIndex == STA_BT_0_16 ? AGE_0_16_MONEY_MAX : AGE_16_19_MONEY_MAX;
        if (maxMoney <= _totalPurchases) {
            auto dialog = ShopDialog::createWithError("警告", "一ヶ月の課金量をオーバーし、\n 購入できません。");
            addChild(dialog, DIALOG_ZORDER);
            dialog->_delegate = this;
        }
    }
}

void ShopTokenLayer::btDialogCallback(ButtonIndex aIndex, int tag)
{
    if (tag == TAG_AGE_CONFIRM_POPUP) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _shopLayer->showLayer(SI_MAIN);
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }
}

void ShopTokenLayer::showTable()
{
    Size winSize = Director::getInstance()->getWinSize();
    _numberOfCell = (int)tokens.size();

    if ((_numberOfCell < 5) || (_numberOfCell == 5 && Director::getInstance()->getWinSize().height == 1136)) {
        _dummyCellNum = 0;
    } else {
        _dummyCellNum = 2;
    }
    _numberOfCell = (int)tokens.size() + _dummyCellNum;

    auto tableView = TableView::create(this, Size(SIZE_TOKEN_CELL.width, winSize.height - HEADER_STATUS::SIZE::HEIGHT - 67));
    tableView->ignoreAnchorPointForPosition(false);
    tableView->setAnchorPoint(Point(0, 1));
    tableView->setPositionX(winSize.width / 2 - SIZE_TOKEN_CELL.width * 0.5);
    tableView->setDirection(ScrollView::Direction::VERTICAL);
    tableView->setDelegate(this);
    tableView->setBounceable(false);

    // セル数によって表示位置を変更する フッター:163, タイトルバー:77
    int cellNum = _numberOfCell - _dummyCellNum;
    if (cellNum >= 6) {
        _tableCellSize = Size(SIZE_TOKEN_CELL.width, SIZE_TOKEN_CELL.height + 14);
        tableView->setPositionY(winSize.height - HEADER_STATUS::SIZE::HEIGHT - 94);
    } else if (cellNum  <= 3) {
        _tableCellSize = Size(SIZE_TOKEN_CELL.width, SIZE_TOKEN_CELL.height + 30);
        tableView->setPositionY(winSize.height - 462 - HEADER_STATUS::SIZE::HEIGHT  - (winSize.height - _tableCellSize.height * cellNum) / 2);
    } else {
        // セルの隙間を算出
        int cellSpace = (winSize.height - 240 - HEADER_STATUS::SIZE::HEIGHT - SIZE_TOKEN_CELL.height * cellNum) / (cellNum + 1);
        _tableCellSize = Size(SIZE_TOKEN_CELL.width, SIZE_TOKEN_CELL.height + cellSpace);
        tableView->setPositionY(winSize.height - 77 - HEADER_STATUS::SIZE::HEIGHT - cellSpace / 2);
    }
    addChild(tableView);

    if (_scrollBar != nullptr) {
        delete _scrollBar;
        _scrollBar = nullptr;
    }
    _scrollBar = ScrollBar::initScrollBar(winSize.height - 304 - HEADER_STATUS::SIZE::HEIGHT, winSize.height - HEADER_STATUS::SIZE::HEIGHT - 94,
                                          tableView->getContainer()->getContentSize().height, tableView->getContentOffset().y);
    addChild(_scrollBar);
    _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 15 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    if (HEADER_STATUS::SIZE::HEIGHT - 94 <= tableView->getContainer()->getContentSize().height) {
        tableView->setPosition(tableView->getPosition() + Point(-20, -HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    }
    tableView->reloadData();
}

Size ShopTokenLayer::tableCellSizeForIndex(TableView* table, ssize_t idx)
{
    return _tableCellSize;
}

TableViewCell* ShopTokenLayer::tableCellAtIndex(TableView* table, ssize_t idx)
{
    Cell* cell = (Cell*)table->dequeueCell();
    if (cell) {
        cell->removeFromParent();
    }
    cell = cellWithIdx((int)idx);
    return cell;
}

ssize_t ShopTokenLayer::numberOfCellsInTableView(TableView* table)
{
    return _numberOfCell;
}

Cell* ShopTokenLayer::cellWithIdx(int idx)
{
    if (idx < 2 && _dummyCellNum != 0) {
        auto cell = new Cell();
        cell->autorelease();
        return cell;
    }

    int tokenIdx = _numberOfCell - 1 - idx;
    auto token = tokens.at(tokenIdx);

    auto cell = new Cell();

    // セルの背景
    auto bg = makeSprite("shop_base.png");
    bg->setPosition(Point(_tableCellSize.width / 2, _tableCellSize.height / 2));
    cell->addChild(bg);

    // アイコン
    std::string iconStr = StringUtils::format("%d_tokens.png", token->getImageId());
    auto icon = makeSprite(iconStr.c_str());
    icon->setPosition(Point(60, bg->getContentSize().height / 2));
    bg->addChild(icon);

    // 商品名
    auto title = Label::createWithTTF(token->getName(), FONT_NAME_2, 26);
    title->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    title->setAnchorPoint(Point(0, 1));
    title->setPosition(Point(115 + 14, bg->getContentSize().height - 12 - 11));
    bg->addChild(title);

    // トークン表示部分
    auto costBase = makeSprite("shop_yen_base.png");
    costBase->setAnchorPoint(Point(0, 1));
    costBase->setPosition(Point(title->getPositionX(), 14 + costBase->getContentSize().height));
    bg->addChild(costBase);

    // トークン数
    auto costNumLabel = Label::createWithTTF(FormatWithCommas(token->getPriceYen()), FONT_NAME_2, 19);
    costNumLabel->setColor(Color3B::BLACK);
    costNumLabel->setAnchorPoint(Point(1, 0.5));
    costNumLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    costNumLabel->setPosition(Point(costBase->getContentSize().width - 14, costBase->getContentSize().height / 2 - 9.5));
    costBase->addChild(costNumLabel);

    // ボタン
    auto btBuy = makeMenuItem("shop_buy_button.png", CC_CALLBACK_1(ShopTokenLayer::btMenuItemTableCallback, this));
    btBuy->setAnchorPoint(Point(1, 0.5));
    btBuy->setTag(tokenIdx);

    auto menu = MenuTouch::create(btBuy, NULL);
    menu->setAnchorPoint(Point(1, 0.5));
    menu->setPosition(Point(bg->getContentSize().width - 14, bg->getContentSize().height / 2));
    bg->addChild(menu);

    cell->autorelease();

    return cell;
}

void ShopTokenLayer::btMenuItemTableCallback(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    int tokenIdx = ((Node*)pSender)->getTag();
    _buyID = tokenIdx;
    auto token = tokens.at(tokenIdx);
    _dialog = ShopDialog::createWithToken(token->getPriceYen(), token->getQuantity());
    addChild(_dialog);
    _dialog->_delegate  = this;
}

void ShopTokenLayer::btDialogCallback(ButtonIndex aIndex)
{
    if (aIndex == BT_YES) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        InAppPurchaseModel* model = new InAppPurchaseModel;
        model->buyProduct(tokens.at(_buyID)->getProductId(), 20, tokens, CC_CALLBACK_0(ShopTokenLayer::onPurchaseResponse, this), CC_CALLBACK_0(ShopTokenLayer::onPurchaseErrorResponse, this));
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }
}

void ShopTokenLayer::onPurchaseResponse()
{
    _shopLayer->updateInfoLayer();
    auto dialog = PurchaseSuccessDialog::create();
    addChild(dialog, DIALOG_ZORDER);
    UserDefault::getInstance()->setStringForKey(KEY_TOKEN_REQUEST_VERIFICATION, "");
}

void ShopTokenLayer::onPurchaseErrorResponse()
{}

void ShopTokenLayer::btSuccessCallback(ButtonIndex aIndex)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
}

void ShopTokenLayer::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
    _scrollBar->updateScrollPoint(view->getContentOffset().y);
}
