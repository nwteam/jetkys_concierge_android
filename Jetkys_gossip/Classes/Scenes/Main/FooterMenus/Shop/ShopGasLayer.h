#ifndef __syanago__ShopGasLayer__
#define __syanago__ShopGasLayer__

#include "cocos2d.h"
#include "ShopLayer.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
using namespace SyanagoAPI;

class ShopGasLayer: public ShopLayerBase {
private:
    Layer* _baseLayer;
    void initBaseLayer();

public:
    ShopGasLayer();
    ~ShopGasLayer();

    bool init();
    CREATE_FUNC(ShopGasLayer);


    void btDialogCallback(ButtonIndex aIndex);
    void btnextSceneCallBack(ButtonIndex aIndex);
    void btSuccessCallback(ButtonIndex aIndex);
    void btExtendCallback(cocos2d::Ref* pSender);
private:
    DefaultProgress* _progress;
    RequestAPI* _request;
    
    void responseGetFuel(Json* response);
};

#endif /* defined(__syanago__ShopGasLayer__) */
