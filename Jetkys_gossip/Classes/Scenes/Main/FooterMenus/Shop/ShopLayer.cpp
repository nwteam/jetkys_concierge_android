#include "ShopLayer.h"
#include "jCommon.h"
#include "FontDefines.h"
#include "MainScene.h"

#include "ShopTokenLayer.h"
#include "ShopMainLayer.h"
#include "ShopCoinLayer.h"
#include "ShopPartLayer.h"
#include "ShopGasLayer.h"
#include "ShopGarageLayer.h"
#include "HeaderStatus.h"


bool ShopLayer::init()
{
    if (!Layer::init()) {
        return false;
    }
    _shopIndex = SI_MAIN;

    Size winSize = Director::getInstance()->getWinSize();

    // background
    showLayer(SI_MAIN);

    // 動作する背景を追加
    auto BgSp = Sprite::create("home_bg.png");
    addChild(BgSp, -1);
    BgSp->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    /*
       MoveTiledSprite* tileBG = MoveTiledSprite::createWithSprite("roopBackground.png" , 1 , 10);
       addChild(tileBG, -1);
     */
    return true;
}

void ShopLayer::resetPageTag()
{
    removeAllChildren();
    init();
}

void ShopLayer::showLayer(ShopIndex nextLayer)
{
    ////CCLOG("_layerIndex %d -> %d", _shopIndex, nextLayer);
    if (nextLayer == _shopIndex && getChildByTag(_shopIndex)) {
        return;
    }



    auto currentLayer = getChildByTag(_shopIndex);

    if (getChildByTag(SI_MAIN)) {
        getChildByTag(SI_MAIN)->setVisible(false);
    }

    _shopIndex = nextLayer;
    switch (_shopIndex) {
    case SI_MAIN: {
        if (!getChildByTag(SI_MAIN)) {
            auto layer = ShopMainLayer::create();
            addChild(layer);
            layer->setTag(SI_MAIN);
            layer->_shopLayer = this;
        } else {
//                getChildByTag(SI_MAIN)->setVisible(true);
            removeChildByTag(SI_MAIN);            // remove old shopMainLayer
            auto layer = ShopMainLayer::create();
            addChild(layer);
            layer->setTag(SI_MAIN);
            layer->_shopLayer = this;
        }

        break;
    }

    case SI_TOKEN: {
        auto layer = ShopTokenLayer::create();
        addChild(layer);
        layer->setTag(SI_TOKEN);
        layer->_shopLayer = this;
        break;
    }

    case SI_COIN: {
        auto layer = ShopCoinLayer::create();
        addChild(layer);
        layer->setTag(SI_COIN);
        layer->_shopLayer = this;
        break;
    }

    case SI_PART: {
        auto layer = ShopPartLayer::create();
        addChild(layer);
        layer->setTag(SI_PART);
        layer->_shopLayer = this;
        break;
    }
    case SI_GAS: {
        auto layer = ShopGasLayer::create();
        addChild(layer);
        layer->setTag(SI_GAS);
        layer->_shopLayer = this;
        break;
    }
    case SI_GARAGE: {
        auto layer = ShopGarageLayer::create();
        addChild(layer);
        layer->setTag(SI_GARAGE);
        layer->_shopLayer = this;
        break;
    }
    default:
        break;
    }

    if (currentLayer && getChildByTag(SI_MAIN) != currentLayer) {
        currentLayer->removeFromParent();
    }
    ////CCLOG("load layer end");
}

void ShopLayer::updateInfoLayer()
{
    _mainScene->headerLayer->updateInfoLayer();
}

// ShopLayerBase
ShopLayerBase::ShopLayerBase()
{
    _title = "ShopLayerBase";
}

bool ShopLayerBase::init()
{
    if (!Layer::init()) {
        return false;
    }

    Size winSize = Director::getInstance()->getWinSize();

    auto shopTitleBg = makeSprite("head_line_no1.png");
    addChild(shopTitleBg);
    int widthBg = shopTitleBg->getContentSize().width / 2;
    shopTitleBg->setPosition(Vec2(widthBg,
                                  winSize.height - shopTitleBg->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 12));

    auto btBack = makeMenuItem("back_button.png", CC_CALLBACK_1(ShopLayerBase::btMenuItemCallback, this));
    auto menuBack = MenuTouch::create(btBack, NULL);
    addChild(menuBack);
    menuBack->setPosition(Vec2(shopTitleBg->getPosition().x - shopTitleBg->getContentSize().width / 2 + 14 + btBack->getNormalImage()->getContentSize().width / 2,
                               shopTitleBg->getPosition().y));

    auto title = Label::createWithTTF(_title, FONT_NAME_2, 34);
    title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    addChild(title);
    title->setAnchorPoint(Vec2(0.0f, 0.5f));
    title->setPosition(menuBack->getPosition() + Point(btBack->getContentSize().width / 2 + 10,
                                                       -17));
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));


    return true;
}

void ShopLayerBase::btMenuItemCallback(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    _shopLayer->showLayer(SI_MAIN);
}


