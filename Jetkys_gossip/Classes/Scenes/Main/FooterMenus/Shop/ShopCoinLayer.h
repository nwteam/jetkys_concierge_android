#ifndef __syanago__ShopCoinLayer__
#define __syanago__ShopCoinLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Loading.h"
#include "ShopLayer.h"
#include "ScrollBar.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace SyanagoAPI;

class Cell;

struct CoinInfo {
    
    CoinInfo(int ID, std::string name, int coin, int token, std::string remarks){
        _ID = ID;
        _name = name;
        _coin = coin;
        _token = token;
        _remarks = remarks;
    }
    
    int _ID;
    std::string _name;
    int _coin;
    int _token;
    std::string _remarks;
};

class ShopCoinLayer : public ShopLayerBase, public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate{
public:
    ShopCoinLayer();
    ~ShopCoinLayer();
    bool init();
    CREATE_FUNC(ShopCoinLayer);
    
    void showTable();
    
    void btMenuItemTableCallback(Ref *pSender);
    
    //create cell with idx of table view
    Cell * cellWithIdx(int idx);
    void onEnterTransitionDidFinish();
    
    //Table view
    void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {}
    virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
    virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);
    
    
    void btnextSceneCallBack(ButtonIndex aIndex);
    void btSuccessCallback(ButtonIndex aIndex);
    void btDialogCallback(ButtonIndex aIndex);
    
    
private:
    int _numberOfCell;
    Size _tableCellSize;
    int _dummyCellNum;
    std::vector<CoinInfo> _coins;
    
    int _buyID;
    int _selectId;
    ScrollBar* _scrollBar;
    
    DefaultProgress* _progress;
    RequestAPI* _request;
    
    void responseGetCoinExchangeList(Json* response);
    void responseExchangeIntoCoin(Json* response);
};

#endif /* defined(__syanago__ShopCoinLayer__) */
