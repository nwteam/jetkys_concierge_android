#ifndef __Syanago__Cell__
#define __Syanago__Cell__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"

class Cell : public cocos2d::extension::TableViewCell
{
public:
	virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
};

#endif /* defined(__Syanago__Cell__) */
