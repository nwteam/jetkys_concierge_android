#ifndef __syanago__ShopLayer__
#define __syanago__ShopLayer__

#include "cocos2d.h"
#include "ShopDialog.h"
#include "Loading.h"

#include "SoundHelper.h"

USING_NS_CC;

class MainScene;

enum ShopIndex {
    SI_MAIN, SI_TOKEN, SI_COIN, SI_PART, SI_GARAGE, SI_GAS
};

class ShopLayer : public Layer {
public:
    bool init();
    CREATE_FUNC(ShopLayer);
    
    MainScene *_mainScene;
    
    CC_SYNTHESIZE(ShopIndex, _shopIndex, SIndex);
    
    void showLayer(ShopIndex nextLayer);
    
    void updateInfoLayer();
    void resetPageTag();
    
};

class ShopLayer;
class ShopLayerBase : public Layer, public ShopDialogDelegate{
public:
    ShopLayerBase();
    
    bool init();
    CREATE_FUNC(ShopLayerBase);
    
    ShopLayer *_shopLayer;
    
    void btMenuItemCallback(Ref *pSender);
    
    std::string _title;
    
    ShopDialog *_dialog;
};

#endif /* defined(__syanago__ShopLayer__) */
