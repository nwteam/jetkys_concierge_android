#ifndef __syanago__ShopGarageLayer__
#define __syanago__ShopGarageLayer__

#include "cocos2d.h"
#include "ShopLayer.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
using namespace SyanagoAPI;

class ShopGarageLayer : public ShopLayerBase
{
public:
    ShopGarageLayer();
    ~ShopGarageLayer();
    
    bool init();
    CREATE_FUNC(ShopGarageLayer);
    
    void btDialogCallback(ButtonIndex aIndex);
    void btnextSceneCallBack(ButtonIndex aIndex);
    void btSuccessCallback(ButtonIndex aIndex);
    void btExtendCallback(cocos2d::Ref *pSender);
private:
    Layer *_baseLayer;
    DefaultProgress* _progress;
    RequestAPI* _request;
    
    void initBaseLayer();
    
    void responseExtendGarage(Json* response);
};

#endif /* defined(__syanago__ShopGarageLayer__) */
