#include "ShopPartLayer.h"
#include "ShopLayer.h"
#include "FontDefines.h"
#include "Cell.h"
#include "DialogView.h"
#include "PlayerController.h"
#include "HeaderStatus.h"
#include "PartModel.h"

#define SIZE_PART_CELL Size(563.0f, 125.0f)


ShopPartLayer::ShopPartLayer():
    _progress(nullptr), _request(nullptr)
{
    _title = "装備ショップ";
    _buyID = -1;

    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}
ShopPartLayer::~ShopPartLayer()
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("icon_pas.plist");
    
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

bool ShopPartLayer::init()
{
    if (!ShopLayerBase::init()) {
        return false;
    }
    // initialize
    _numberOfCell = 0;
    _parts = std::vector<int>();

    Size winSize = Director::getInstance()->getWinSize();
    
    _progress->onStart();
    _request->getPartsList(CC_CALLBACK_1(ShopPartLayer::responseGetPartsList, this));
    return true;
}

void ShopPartLayer::responseGetPartsList(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "get_parts_list");
    std::string result = Json_getString(jsonData1, "result", "false");
    
    if (result == "true") {
        Json* jsonDataData = Json_getItem(jsonData1, "data");
        
        Json* mDatas = Json_getItem(jsonDataData, "parts_ids");
        if (mDatas && mDatas->type == Json_Array) {
            Json* child;
            for (child = mDatas->child; child; child = child->next) {
                _parts.push_back(atoi(child->valueString));
            }
            
            // show table view
            showTable();
        }
    }

}

void ShopPartLayer::tableCellTouched(TableView* table, TableViewCell* cell)
{
    ////CCLOG("cell touched at index: %ld", cell->getIdx());
}

Size ShopPartLayer::tableCellSizeForIndex(TableView* table, ssize_t idx)
{
    return SIZE_PART_CELL;
}

TableViewCell* ShopPartLayer::tableCellAtIndex(TableView* table, ssize_t idx)
{
    //    auto string = String::createWithFormat("%ld", idx);
    Cell* cell = (Cell*)table->dequeueCell();
    if (cell)
        cell->removeFromParent();

    // Cell UI
    cell = cellWithIdx((int)idx);

    ////CCLOG("tableCellAtIndex: %ld, %f, %f", idx, cell->getPosition().x, cell->getPosition().y);

    return cell;
}

ssize_t ShopPartLayer::numberOfCellsInTableView(TableView* table)
{
    return _numberOfCell + 2;
}

Cell* ShopPartLayer::cellWithIdx(int idx)
{
    auto cell = new Cell();

    if (idx > 1) {
        int partIdx = _numberOfCell - 1 - idx + 2;
        int part = _parts.at(partIdx);

        // get Part info from part table
        std::shared_ptr<PartModel>partsModel(PartModel::find(part));
        std::string name = partsModel->getName();
        int exercise = partsModel->getExercise();
        int reaction = partsModel->getReaction();
        int dicision = partsModel->getDecision();
        int price = partsModel->getBuyPrice();
        ////CCLOG("運: %d,　技: %d, 忍: %d",exercise, reaction, dicision);


        auto bg = makeSprite("shop_base.png");
        cell->addChild(bg);
        bg->setPosition(Vec2(SIZE_PART_CELL.width / 2, SIZE_PART_CELL.height / 2));

        // ショップに並ぶアイコン
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon_pas.plist");
        auto icon = makeSprite(StringUtils::format("%d_pas.png", part).c_str());
        cell->addChild(icon);
        icon->setPosition(Vec2(icon->getContentSize().width / 2, SIZE_PART_CELL.height / 2));

        auto rarityPosi = Point(0, 0);
        auto rarityLayer = Layer::create();
        for (int i = 0; i < partsModel->getRarity(); i++) {
            auto tmpRarity = Sprite::create("rarity.png");
            rarityLayer->addChild(tmpRarity);
            tmpRarity->setAnchorPoint(Vec2(0, 1));
            tmpRarity->setPosition(rarityPosi);
            rarityPosi += Vec2(tmpRarity->getContentSize().width, 0);
        }
        icon->addChild(rarityLayer);
        rarityLayer->setPosition(Point(icon->getContentSize().width / 2 - rarityPosi.x / 2, 15));

        auto title = Label::createWithTTF(name, FONT_NAME_2, 22, Size(500, 35));
        title->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
        title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        cell->addChild(title);
        title->setAnchorPoint(Vec2(0, 0.5));
        title->setPosition(Vec2(/*34.0f + 10.0f +*/ 20 + icon->getContentSize().width,
                                                    SIZE_PART_CELL.height / 2 + 35 + 11));


        auto statusPosi = Point(/*32.0f + 10.0f*/ 20 + icon->getContentSize().width - 8,
                                                  SIZE_PART_CELL.height / 2 - 1);

        if (exercise > 0) {
            auto exerciseLabel = Label::createWithTTF("「馬」+" + FormatWithCommas(exercise), FONT_NAME_2, 19);
            exerciseLabel->setColor(COLOR_YELLOW);
            cell->addChild(exerciseLabel);
            exerciseLabel->setAnchorPoint(Vec2(0, 0.5));
            exerciseLabel->setPosition(statusPosi);
            exerciseLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
            statusPosi = statusPosi + Vec2(exerciseLabel->getContentSize().width, 0);
        }

        if (reaction > 0) {
            auto reactionLabel = Label::createWithTTF("「技」+" + FormatWithCommas(reaction), FONT_NAME_2, 19);
            reactionLabel->setColor(COLOR_YELLOW);
            cell->addChild(reactionLabel);
            reactionLabel->setAnchorPoint(Vec2(0, 0.5));
            reactionLabel->setPosition(statusPosi);
            reactionLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
            statusPosi = statusPosi + Vec2(reactionLabel->getContentSize().width, 0);
        }

        if (dicision > 0) {
            auto dicisionLabel = Label::createWithTTF("「忍」+" + FormatWithCommas(dicision), FONT_NAME_2, 19);
            dicisionLabel->setColor(COLOR_YELLOW);
            cell->addChild(dicisionLabel);
            dicisionLabel->setAnchorPoint(Vec2(0, 0.5));
            dicisionLabel->setPosition(statusPosi);
            dicisionLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
            statusPosi = statusPosi + Vec2(dicisionLabel->getContentSize().width, 0);
        }

        auto bgCost = makeSprite("shop_coin_base.png");
        cell->addChild(bgCost);
        bgCost->setPosition(Vec2(/*44.0f + */ 20 + icon->getContentSize().width + bgCost->getContentSize().width / 2,
                                              SIZE_PART_CELL.height / 2 - 7 - bgCost->getContentSize().height / 2));

        auto costLabel = Label::createWithTTF(FormatWithCommas(price), FONT_NAME_2, 19, bgCost->getContentSize());
        costLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
        costLabel->setColor(Color3B::BLACK);
        cell->addChild(costLabel);
        costLabel->setAnchorPoint(Vec2(1, 0.5));
        costLabel->setPosition(bgCost->getPosition() + Vec2(bgCost->getContentSize().width / 2 - 10, -8.5));

        auto btBuy = makeMenuItem("shop_buy_button.png", CC_CALLBACK_1(ShopPartLayer::btMenuItemTableCallback, this));
        btBuy->setTag(partIdx);

        auto menu = MenuTouch::create(btBuy, NULL);
        cell->addChild(menu);
        menu->setPosition(Vec2(SIZE_PART_CELL.width - 20 - btBuy->getContentSize().width / 2, SIZE_PART_CELL.height * 0.5f));
    }
    cell->autorelease();
    return cell;
}

void ShopPartLayer::btMenuItemTableCallback(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    int partIdx = ((Node*)pSender)->getTag();
    int part = _parts.at(partIdx);
    ////CCLOG("Buy token: Idx(_parts) = %d, ID = %d", partIdx, part);

    // std::string name = MASTER_CONTROLLER->_part->getName();
    // int exercise = MASTER_CONTROLLER->_part->getExercise();
    // int price = MASTER_CONTROLLER->_part->getBuyPrice();

    // todo 確認 Popup
    _buyID = part;

    _dialog = ShopDialog::createWithPart(_buyID);
    addChild(_dialog);
    _dialog->_delegate  = this;
}

void ShopPartLayer::showTable()
{
    // table view

    _numberOfCell = (int)_parts.size();

    Size winSize = Director::getInstance()->getWinSize();

    auto tableView = TableView::create(this, Size(SIZE_PART_CELL.width, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65));
    tableView->setDirection(ScrollView::Direction::VERTICAL);
    tableView->setPosition(Point(winSize.width / 2 - SIZE_PART_CELL.width * 0.5,
                                 0));
    tableView->setDelegate(this);
    tableView->setBounceable(false);
    addChild(tableView);
    _scrollBar = ScrollBar::initScrollBar(winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 180 - 30, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65, tableView->getContainer()->getContentSize().height, tableView->getContentOffset().y);
    addChild(_scrollBar);
    _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 15 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    if (winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 <= tableView->getContainer()->getContentSize().height) {
        tableView->setPosition(tableView->getPosition() + Point(-20, 0));
    }
    tableView->reloadData();
}

void ShopPartLayer::btDialogCallback(ButtonIndex aIndex)
{
    ////CCLOG("ShopPartLayer::btDialogCallback");
    if (aIndex == BT_YES) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        int coin = PLAYERCONTROLLER->_player->getFreeCoin() + PLAYERCONTROLLER->_player->getPayCoin();
        std::shared_ptr<PartModel>partsModel(PartModel::find(_buyID));
        int payCoin = partsModel->getBuyPrice();
        if (coin >= payCoin) {
            _progress->onStart();
            _request->buyParts(_buyID, CC_CALLBACK_1(ShopPartLayer::responseBuyParts, this));
        } else {
            _dialog = ShopDialog::createWithGotoScene(GOS_COIN);
            addChild(_dialog, DIALOG_ZORDER);
            _dialog->_delegate = this;
        }
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }
    _buyID = -1;
}

void ShopPartLayer::responseBuyParts(Json *response)
{
    Json* jsonData1 = Json_getItem(response, "buy_parts");
    std::string result = Json_getString(jsonData1, "result", "false");
    
    if (result == "true") {
        Json* jsonDataData = Json_getItem(jsonData1, "data");
        
        Json* mDatas = Json_getItem(jsonDataData, "players");
        // Save player
        PLAYERCONTROLLER->_player->setFreeCoin(atoi(Json_getString(mDatas, "free_coin", "-1")));
        PLAYERCONTROLLER->_player->setPayCoin(atoi(Json_getString(mDatas, "pay_coin", "-1")));
        PLAYERCONTROLLER->_player->update();
        
        // save player parts
        mDatas = Json_getItem(jsonDataData, "player_parts");
        if (mDatas && mDatas->type == Json_Array) {
            Json* child;
            for (child = mDatas->child; child; child = child->next) {
                int ID = atoi(Json_getString(child, "id", "-1"));
                int player_id = atoi(Json_getString(child, "player_id", "-1"));
                int part_id = atoi(Json_getString(child, "parts_id", "-1"));
                std::string created = Json_getString(child, "created", "");
                std::string modified = Json_getString(child, "modified", "");
                // save db
                PLAYERCONTROLLER->savePart(ID, player_id, part_id, created, modified);
                // add to controller
                PLAYERCONTROLLER->addPart(ID, player_id, part_id, created, modified);
            }
        }
        
        // Show
        _shopLayer->updateInfoLayer();
        SOUND_HELPER->playeMainSceneEffect(SOUND_BUY, false);
        _dialog = ShopDialog::createWithSuccess("パーツを購入しました");
        addChild(_dialog, DIALOG_ZORDER);
        _dialog->_delegate = this;
    }
}

void ShopPartLayer::btnextSceneCallBack(ButtonIndex aIndex)
{
    if (aIndex == BT_YES) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _shopLayer->showLayer(SI_COIN);
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }
}

void ShopPartLayer::btSuccessCallback(ButtonIndex aIndex)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
}

void ShopPartLayer::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
    _scrollBar->updateScrollPoint(view->getContentOffset().y);
}
