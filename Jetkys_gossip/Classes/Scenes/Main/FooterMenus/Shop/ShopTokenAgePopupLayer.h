#ifndef __syanago__ShopTokenAgePopupLayer__
#define __syanago__ShopTokenAgePopupLayer__

#include "LayerPriority.h"
#include "PopUpDelegate.h"

#define kPriorityShopTokenAgePopupLayer -650
#define kPriorityShopTokenAgePopupMenu kPriorityShopTokenAgePopupLayer - 2

enum ShopTokenAgePopup {
    STA_BT_0_16 = 111,
    STA_BT_16_19 = 222,
    STA_BT_20 = 333,
    STA_BT_BACK =999
};

#define AGE_0_16_MONEY_MAX 5000
#define AGE_16_19_MONEY_MAX 20000

class ShopTokenAgePopupLayer : public LayerPriority {
public:
    
    PopUpDelegate *_delegate;
    
    bool init();
    CREATE_FUNC(ShopTokenAgePopupLayer);
    
    void callBackbtn(Ref *psender);
};

#endif /* defined(__syanago__ShopTokenAgePopupLayer__) */
