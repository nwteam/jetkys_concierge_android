#include "ShopTokenAgePopupLayer.h"
#include "jCommon.h"
#include "FontDefines.h"


bool ShopTokenAgePopupLayer::init()
{
    if (!LayerPriority::initWithPriority(kPriorityShopTokenAgePopupLayer)) {
        return false;
    }
    _delegate = nullptr;

    Size winSize = Director::getInstance()->getWinSize();

    // 半透明レイヤー
    auto layer = LayerColor::create(Color4B(0, 0, 0, 128));
    addChild(layer);

    // ダイアログ背景
    auto bg = Sprite::create("shop_age_apply_base.png");
    bg->setPosition(Point(winSize.width / 2, winSize.height / 2));
    layer->addChild(bg);

    auto bgSize = bg->getContentSize();

    // タイトル
    auto titleLabel = Label::createWithTTF("年齢を選択してください", FONT_NAME_2, 34);
    titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    titleLabel->setAnchorPoint(Point(0.5, 1));
    titleLabel->setPosition(Point(bgSize.width / 2, bgSize.height - 30));
    titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    bg->addChild(titleLabel);

    // 本文
    auto mainTextLabel = Label::createWithTTF("あなたの年齢(ねんれい)によって", FONT_NAME_2, 26);
    mainTextLabel->setColor(Color3B(255, 231, 117));
    mainTextLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    mainTextLabel->setAnchorPoint(Point(0.5, 1));
    mainTextLabel->setPosition(Point(bgSize.width / 2, titleLabel->getPositionY() - titleLabel->getContentSize().height - 30 - 13 + 50));
    mainTextLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    bg->addChild(mainTextLabel);

    auto mainTextLabel2 = Label::createWithTTF("トークンを買える金額(きんがく)が", FONT_NAME_2, 26);
    mainTextLabel2->setColor(Color3B(255, 231, 117));
    mainTextLabel2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    mainTextLabel2->setAnchorPoint(Point(0.5, 1));
    mainTextLabel2->setPosition(Point(bgSize.width / 2, mainTextLabel->getPositionY() - mainTextLabel->getContentSize().height + 20));
    mainTextLabel2->setHorizontalAlignment(TextHAlignment::CENTER);
    bg->addChild(mainTextLabel2);

    auto mainTextLabel3 = Label::createWithTTF("決まっています。", FONT_NAME_2, 26);
    mainTextLabel3->setColor(Color3B(255, 231, 117));
    mainTextLabel3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    mainTextLabel3->setAnchorPoint(Point(0.5, 1));
    mainTextLabel3->setPosition(Point(bgSize.width / 2, mainTextLabel2->getPositionY() - mainTextLabel2->getContentSize().height + 20));
    mainTextLabel3->setHorizontalAlignment(TextHAlignment::CENTER);
    bg->addChild(mainTextLabel3);

    auto mainTextLabel4 = Label::createWithTTF("必ず正しい年齢を選んでください。", FONT_NAME_2, 26);
    mainTextLabel4->setColor(Color3B(255, 231, 117));
    mainTextLabel4->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    mainTextLabel4->setAnchorPoint(Point(0.5, 1));
    mainTextLabel4->setPosition(Point(bgSize.width / 2, mainTextLabel3->getPositionY() - mainTextLabel3->getContentSize().height  + 20));
    mainTextLabel4->setHorizontalAlignment(TextHAlignment::CENTER);
    bg->addChild(mainTextLabel4);

    // メニュー
    auto menu = MenuPriority::createWithPriority(kPriorityShopTokenAgePopupMenu);
    bg->addChild(menu);
    menu->setPosition(Vec2(0, 20));
    menu->setAnchorPoint(Vec2::ZERO);

    // 16歳未満
    auto button_1 = makeMenuItem("shop_age_apply.png", CC_CALLBACK_1(ShopTokenAgePopupLayer::callBackbtn, this));
    button_1->setTag(STA_BT_0_16);
    button_1->setAnchorPoint(Point(0, 1));
    button_1->setPositionY(mainTextLabel4->getPositionY() - mainTextLabel4->getContentSize().height - 30);
    menu->addChild(button_1);

    auto buttonLabel_1 = Label::createWithTTF("16歳未満", FONT_NAME_2, 28);
    buttonLabel_1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    buttonLabel_1->setAnchorPoint(Point(0.5, 0.5));
    buttonLabel_1->setPosition(button_1->getContentSize().width / 2, button_1->getContentSize().height / 2 - 14);
    button_1->addChild(buttonLabel_1);

    auto label_1 = Label::createWithTTF("月5,000円まで ", FONT_NAME_2, 26);
    label_1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label_1->setAnchorPoint(Point(0, 0));
    label_1->setPosition(Point(0, button_1->getPositionY() - button_1->getContentSize().height / 2 - 13));
    bg->addChild(label_1);

    // ボタンとテキストが中央寄りになるように配置
    button_1->setPositionX((bgSize.width - (button_1->getContentSize().width + label_1->getContentSize().width + 14)) / 2);
    label_1->setPosition(button_1->getPositionX() + button_1->getContentSize().width + 14, button_1->getPositionY() - button_1->getContentSize().height / 2 - 35 / 2);

    // 16〜19歳
    auto button_2 = makeMenuItem("shop_age_apply.png", CC_CALLBACK_1(ShopTokenAgePopupLayer::callBackbtn, this));
    button_2->setTag(STA_BT_16_19);
    button_2->setAnchorPoint(Point(0, 1));
    button_2->setPosition(Point(button_1->getPositionX(), button_1->getPositionY() - button_1->getContentSize().height - 30));
    menu->addChild(button_2);

    auto buttonLabel_2 = Label::createWithTTF("16〜19歳", FONT_NAME_2, 28);
    buttonLabel_2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    buttonLabel_2->setAnchorPoint(Point(0.5, 0.5));
    buttonLabel_2->setPosition(button_2->getContentSize().width / 2, button_2->getContentSize().height / 2 - 14);
    buttonLabel_2->setHorizontalAlignment(TextHAlignment::CENTER);
    button_2->addChild(buttonLabel_2);

    auto label_2 = Label::createWithTTF("月20,000円まで", FONT_NAME_2, 26);
    label_2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label_2->setAnchorPoint(Point(0, 0.5));
    label_2->setPosition(Point(label_1->getPositionX(), button_2->getPositionY() - button_2->getContentSize().height / 2 - 13 + 35 / 2));
    label_2->setHorizontalAlignment(TextHAlignment::LEFT);
    bg->addChild(label_2);

    // 20歳以上
    auto button_3 = makeMenuItem("shop_age_apply.png", CC_CALLBACK_1(ShopTokenAgePopupLayer::callBackbtn, this));
    button_3->setTag(STA_BT_20);
    button_3->setAnchorPoint(Point(0, 1));
    button_3->setPosition(Point(button_2->getPositionX(), button_2->getPositionY() - button_2->getContentSize().height - 30));
    menu->addChild(button_3);

    auto buttonLabel_3 = Label::createWithTTF("20歳以上", FONT_NAME_2, 28);
    buttonLabel_3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    buttonLabel_3->setAnchorPoint(Point(0.5, 0.5));
    buttonLabel_3->setPosition(button_3->getContentSize().width / 2, button_3->getContentSize().height / 2 - 14);
    buttonLabel_3->setHorizontalAlignment(TextHAlignment::CENTER);
    button_3->addChild(buttonLabel_3);

    auto label_3 = Label::createWithTTF("制限なし", FONT_NAME_2, 26);
    label_3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label_3->setAnchorPoint(Point(0, 0.5));
    label_3->setPosition(Point(label_2->getPositionX(),
                               button_3->getPositionY() - button_3->getContentSize().height / 2 - 13 + 35 / 2));
    label_3->setHorizontalAlignment(TextHAlignment::LEFT);
    bg->addChild(label_3);
    /*
       auto btBack = makeMenuItem("back_button.png", CC_CALLBACK_1(ShopTokenAgePopupLayer::callBackbtn, this));
       btBack->setPosition(label_3->getContentSize().width + 340 ,label_3->getContentSize().height - 5);
       btBack->setTag(STA_BT_BACK);
       menu->addChild(btBack);
     */
    return true;
}

void ShopTokenAgePopupLayer::callBackbtn(Ref* psender)
{
    // Destroy dialog
    int aIndex = ((Node*)psender)->getTag();
    PopUpDelegate* delegate = _delegate;
    removeFromParent();

    // call back
    if (delegate != nullptr) {
        delegate->btCallback(aIndex, nullptr);
    }
}

