#ifndef __syanago__ShopMainLayer__
#define __syanago__ShopMainLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ScrollBar.h"
USING_NS_CC_EXT;


USING_NS_CC;

class ShopLayer;

class ShopMainLayer : public Layer, public ScrollViewDelegate {
public:
    bool init();
    CREATE_FUNC(ShopMainLayer);
    
    ShopLayer *_shopLayer;
    
    void btMenuItemCallback(Ref *pSender);
    Menu* _menu;
    
    ScrollView *_pScroll;
    ScrollBar *_scrollBar;
    
    void scrollViewDidScroll(ScrollView *view);
    void scrollViewDidZoom(ScrollView *view);
};

#endif /* defined(__syanago__ShopMainLayer__) */
