#ifndef __syanago__Token__
#define __syanago__Token__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"

using namespace cocos2d;

class Token
{
public:
    Token(Json* json);
    
    CC_SYNTHESIZE_READONLY(int, itemId, ItemId);
    CC_SYNTHESIZE_READONLY(std::string, name, Name);
    CC_SYNTHESIZE_READONLY(int, quantity, Quantity);
    CC_SYNTHESIZE_READONLY(int, quantityFree, QuantityFree);
    CC_SYNTHESIZE_READONLY(int, priceYen, PriceYen);
    CC_SYNTHESIZE_READONLY(std::string, remarks, Remarks);
    CC_SYNTHESIZE_READONLY(int, imageId, ImageId);
    CC_SYNTHESIZE_READONLY(std::string, productId, ProductId);
    CC_SYNTHESIZE_READONLY(std::string, startDateTime, StartDateTime);
    CC_SYNTHESIZE_READONLY(std::string, endDateTime, EndDateTime);
    
};

#endif