#ifndef __syanago__ShopTokenLayer__
#define __syanago__ShopTokenLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Loading.h"
#include "ShopLayer.h"
#include "PopUpDelegate.h"
#include "ScrollBar.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"
#include "Token.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace SyanagoAPI;

class Cell;

struct TokenInfo {
    TokenInfo(Json* tokenData){
        itemId = atoi(Json_getString(tokenData, "id", "-1"));
        name = Json_getString(tokenData, "name", "");
        quantity = atoi(Json_getString(tokenData, "quantity", "-1"));
        quantityFree = atoi(Json_getString(tokenData, "quantity_free", "-1"));
        priceYen = atoi(Json_getString(tokenData, "price_yen", "-1"));
        remarks = Json_getString(tokenData, "remarks", "");
        imageId = atoi(Json_getString(tokenData, "image_id", ""));
        productId = Json_getString(tokenData, "product_id", "");
        startDateTime = Json_getString(tokenData, "start_datetime", "");
        endDateTime = Json_getString(tokenData, "end_datetime", "");
    }
    CC_SYNTHESIZE_READONLY(int, itemId, ItemId);
    CC_SYNTHESIZE_READONLY(std::string, name, Name);
    CC_SYNTHESIZE_READONLY(int, quantity, Quantity);
    CC_SYNTHESIZE_READONLY(int, quantityFree, QuantityFree);
    CC_SYNTHESIZE_READONLY(int, priceYen, PriceYen);
    CC_SYNTHESIZE_READONLY(std::string, remarks, Remarks);
    CC_SYNTHESIZE_READONLY(int, imageId, ImageId);
    CC_SYNTHESIZE_READONLY(std::string, productId, ProductId);
    CC_SYNTHESIZE_READONLY(std::string, startDateTime, StartDateTime);
    CC_SYNTHESIZE_READONLY(std::string, endDateTime, EndDateTime);
};

class ShopTokenLayer : public ShopLayerBase, public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate, public PopUpDelegate{
public:
    ShopTokenLayer();

    bool init();
    CREATE_FUNC(ShopTokenLayer);
    
    
    void btMenuItemTableCallback(Ref *pSender);
    
    //create cell with idx of table view
    Cell * cellWithIdx(int idx);
    
    //Table view
    void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {}
    virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell){};
    virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);
    
    
    void btDialogCallback(ButtonIndex aIndex);
    void btDialogCallback(ButtonIndex aIndex, int tag);
    void btCallback(int btIndex, void *object);
    void btSuccessCallback(ButtonIndex aIndex);
    
    
    void onEnter();
    void onExit();
    
private:
    DefaultProgress* progress;
    RequestAPI* request;
    ScrollBar* _scrollBar;
    
    Loading *_loadingStatus;
    
    
    int _numberOfCell;
    int _dummyCellNum;
    Size _tableCellSize;
    std::vector<std::shared_ptr<const Token>> tokens;
    int _buyID;
    int _totalPurchases;
    
    

    void onResponseGetTokenList(Json* response);
    void showTable();
    void onPurchaseResponse();
    void onPurchaseErrorResponse();
    
    void sendBoughtMeasurementInformation(const int boughtToken);
};

#endif /* defined(__syanago__ShopTokenLayer__) */
