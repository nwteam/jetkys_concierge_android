#ifndef __syanago__ShopPartLayer__
#define __syanago__ShopPartLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Loading.h"
#include "ShopLayer.h"
#include "ScrollBar.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace SyanagoAPI;

class Cell;

class ShopPartLayer : public ShopLayerBase, public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate{
public:
    ShopPartLayer();
    ~ShopPartLayer();
    
    bool init();
    CREATE_FUNC(ShopPartLayer);
    
    void showTable();
    
    void btMenuItemTableCallback(Ref *pSender);
    
    //create cell with idx of table view
    Cell * cellWithIdx(int idx);
    
    //Table view
    void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {}
    virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
    virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);
    
    //API Response Callback
    
    void btnextSceneCallBack(ButtonIndex aIndex);
    void btDialogCallback(ButtonIndex aIndex);
    void btSuccessCallback(ButtonIndex aIndex);
    
private:
    int _numberOfCell;
    std::vector<int> _parts;
    int _buyID;
    ScrollBar* _scrollBar;
    
    DefaultProgress* _progress;
    RequestAPI* _request;
    
    void responseGetPartsList(Json* response);
    void responseBuyParts(Json* response);
};

#endif /* defined(__syanago__ShopPartLayer__) */
