#include "ShopMainLayer.h"
#include "ShopLayer.h"
#include "FontDefines.h"
#include "HeaderStatus.h"


bool ShopMainLayer::init()
{
    if (!Layer::init()) {
        return false;
    }



    Size winSize = Director::getInstance()->getWinSize();

    auto shopTitleBg = makeSprite("head_line_no1.png");
    addChild(shopTitleBg);
    shopTitleBg->setPosition(Vec2(shopTitleBg->getContentSize().width / 2,
                                  winSize.height - shopTitleBg->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 12));


    auto title = Label::createWithTTF("ショップ", FONT_NAME_2, 34);
    title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    addChild(title);
    title->setAnchorPoint(Vec2(0.0f, 0.5f));
    title->setPosition(Point(14,
                             shopTitleBg->getPosition().y - 17));
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));


    auto shopMenuLayer = Layer::create();

    auto btPart = makeMenuItem("button_no1.png", CC_CALLBACK_1(ShopMainLayer::btMenuItemCallback, this));
    btPart->setTag(SI_PART);
    auto partLabel = Label::createWithTTF("装備ショップ", FONT_NAME_2, 34);
    btPart->addChild(partLabel);
    partLabel->setPosition(Point(btPart->getContentSize().width / 2,
                                 btPart->getContentSize().height / 2 - 17));
    partLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));


    auto btGas = makeMenuItem("button_no1.png", CC_CALLBACK_1(ShopMainLayer::btMenuItemCallback, this));
    btGas->setTag(SI_GAS);
    auto gasLabel = Label::createWithTTF("ガソリンスタンド", FONT_NAME_2, 34);
    btGas->addChild(gasLabel);
    gasLabel->setPosition(Point(btGas->getContentSize().width / 2,
                                btGas->getContentSize().height / 2 - 17));
    gasLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));


    auto btToken = makeMenuItem("button_no1.png", CC_CALLBACK_1(ShopMainLayer::btMenuItemCallback, this));
    btToken->setTag(SI_TOKEN);
    auto tokenLabel = Label::createWithTTF("トークンショップ", FONT_NAME_2, 34);
    btToken->addChild(tokenLabel);
    tokenLabel->setPosition(Point(btToken->getContentSize().width / 2,
                                  btToken->getContentSize().height / 2 - 17));
    tokenLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));


    auto btCoin = makeMenuItem("button_no1.png", CC_CALLBACK_1(ShopMainLayer::btMenuItemCallback, this));
    btCoin->setTag(SI_COIN);
    auto coinLabel = Label::createWithTTF("コイン購入所", FONT_NAME_2, 34);
    btCoin->addChild(coinLabel);
    coinLabel->setPosition(Point(btCoin->getContentSize().width / 2,
                                 btCoin->getContentSize().height / 2 - 17));
    coinLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));


    auto btGarage = makeMenuItem("button_no1.png", CC_CALLBACK_1(ShopMainLayer::btMenuItemCallback, this));
    btGarage->setTag(SI_GARAGE);
    auto gareLabel = Label::createWithTTF("ガレージ拡張", FONT_NAME_2, 34);
    btGarage->addChild(gareLabel);
    gareLabel->setPosition(Point(btGarage->getContentSize().width / 2,
                                 btGarage->getContentSize().height / 2 - 17));
    gareLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));



    if (winSize.height == 1136) {
        _menu = MenuTouch::create(btPart, btGas, btToken, btCoin, btGarage, NULL);
        _menu->alignItemsVerticallyWithPadding(14);
        _menu->setPosition(Point(winSize.width / 2, winSize.height / 2));
        shopMenuLayer->addChild(_menu);
        addChild(shopMenuLayer);

        _scrollBar = ScrollBar::initScrollBar(winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 180, 100, 100, 0);
        addChild(_scrollBar);
        _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)), winSize.height / 2 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    } else {
        _menu = MenuTouch::create(btPart, btGas, btToken, btCoin, btGarage, NULL);
        _menu->alignItemsVerticallyWithPadding(14);
        _menu->setPosition(Point(winSize.width / 2, winSize.height / 2 + 29 + HEADER_STATUS::SIZE::HEIGHT + 65 - btPart->getContentSize().height / 2));
        shopMenuLayer->addChild(_menu);

        _pScroll = ScrollView::create();
        _pScroll->setViewSize(Size(winSize.width, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65));
        _pScroll->setDirection(ScrollView::Direction::VERTICAL);
        _pScroll->setContentOffset(Vec2(0, -(shopMenuLayer->getContentSize().height / 2 - 29 - HEADER_STATUS::SIZE::HEIGHT - 65)));
        _pScroll->addChild(shopMenuLayer);
        _pScroll->setBounceable(false);
        addChild(_pScroll);

        // スクロールバー
        _pScroll->setDelegate(this);
        _scrollBar = ScrollBar::initScrollBar(winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 180 - 30, _pScroll->getViewSize().height, _pScroll->getContainer()->getContentSize().height, _pScroll->getContentOffset().y);
        addChild(_scrollBar);
        _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 15 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
        if (_pScroll->getViewSize().height <= _pScroll->getContainer()->getContentSize().height) {
            _pScroll->setPosition(_pScroll->getPosition() + Point(-20, 0));
        }


        // QuynhNM
        // Scrollview + Menu
        Director::getInstance()->getEventDispatcher()->removeEventListenersForTarget(_menu);
        auto listener = EventListenerTouchOneByOne::create();

        listener->onTouchBegan = [this](Touch* touch, Event* event) {
                                     if (!_pScroll->onTouchBegan(touch, event))
                                         return false;
                                     return _menu->onTouchBegan(touch, event);
                                 };

        listener->onTouchMoved = [this](Touch* touch, Event* event) {
                                     _pScroll->onTouchMoved(touch, event);
                                     _menu->onTouchMoved(touch, event);
                                 };

        listener->onTouchEnded = [this](Touch* touch, Event* event) {
                                     if (_pScroll->isTouchMoved()) {
                                         _pScroll->onTouchEnded(touch, event);
                                         _menu->onTouchCancelled(touch, event);
                                         return;
                                     }
                                     _pScroll->onTouchEnded(touch, event);
                                     _menu->onTouchEnded(touch, event);
                                 };

        listener->onTouchCancelled = [this](Touch* touch, Event* event) {
                                         _pScroll->onTouchCancelled(touch, event);
                                         _menu->onTouchCancelled(touch, event);
                                     };

        Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, _menu);
    }

    return true;
}

void ShopMainLayer::btMenuItemCallback(cocos2d::Ref* pSender)
{
    int tag = ((Node*)pSender)->getTag();
    ////CCLOG("ShopMainLayer::btMenuItemCallback tag = %d", tag);
    ////CCLOG("playereffect");
    // todo Android error
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    _shopLayer->showLayer((ShopIndex) tag);
    ////CCLOG("ShopMainLayer::btMenuItemCallback end");
}

void ShopMainLayer::scrollViewDidScroll(ScrollView* view)
{
    _scrollBar->updateScrollPoint(view->getContentOffset().y);
}
void ShopMainLayer::scrollViewDidZoom(ScrollView* view)
{}

