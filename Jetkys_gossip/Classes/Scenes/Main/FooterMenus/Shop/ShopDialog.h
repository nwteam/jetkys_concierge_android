#ifndef __syanago__ShopDialog__
#define __syanago__ShopDialog__

#include "jCommon.h"
#include "DialogDefine.h"
#include "LayerPriority.h"
#include "FontDefines.h"

USING_NS_CC;
USING_NS_CC_EXT;

enum ShopDialogType{
    SDT_PART,
    SDT_GAS,
    SDT_TOKEN,
    SDT_COIN,
    SDT_GARAGE,
};

enum NextScene{
    GOS_COIN,
    GOS_TOKEN,
};


class ShopDialogDelegate {
public:
	~ShopDialogDelegate() {};
    virtual void btDialogCallback(ButtonIndex aIndex) { };
    virtual void btnextSceneCallBack(ButtonIndex aIndex){ };
    virtual void btSuccessCallback(ButtonIndex aIndex){ };
};

class ShopDialog : public LayerPriority {
private:
    void btnCallback(Ref* pSender);
    void btNextSceneCallback(Ref*pSender);
    void btSuccesscall(Ref* pSender);
public:
    ShopDialog();
    ~ShopDialog();
    
    ShopDialogDelegate *_delegate;
    
    bool init(int id, int coin, int token, int yen, int num, int type);
    bool initError(std::string errCode, std::string message);
    bool initSuccess(std::string message);
    bool initGotoScene(NextScene type);

    static ShopDialog* createWithPart(int id);
    static ShopDialog* createWithGas(int token);
    static ShopDialog* createWithToken(int yen, int token);
    static ShopDialog* createWithCoin(int token, int coin);
    static ShopDialog* createWithGarage(int token, int num);
    static ShopDialog* createWithError(std::string errCode, std::string message);
    static ShopDialog* createWithSuccess(std::string message);
    static ShopDialog* createWithGotoScene(NextScene type);
    
};

#endif /* defined(__syanago__ShopDialog__) */
