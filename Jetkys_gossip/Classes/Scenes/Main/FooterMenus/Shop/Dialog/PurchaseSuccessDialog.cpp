#include "PurchaseSuccessDialog.h"
#include "SoundHelper.h"
#include "FontDefines.h"

bool PurchaseSuccessDialog::init()
{
    if (!Layer::init()) {
        return false;
    }
    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch *touch, Event *unused_event) -> bool {
        return true;
    };
    _listener->onTouchMoved = [&](Touch *touch, Event *unused_event){};
    _listener->onTouchCancelled = [&](Touch *touch, Event *unused_event){};
    _listener->onTouchEnded = [&](Touch *touch, Event *unused_event){};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);
    
    showBackground();
    showButton();
    
    return true;
}

void PurchaseSuccessDialog::showBackground()
{
    auto black = LayerColor::create(Color4B(Color3B::BLACK));
    black->setOpacity(180);
    addChild(black);
    
    auto backGround = Sprite::create("dialog_purchase_success_base.png");
    Size winSize = Director::getInstance()->getWinSize();
    backGround->setPosition(Point(winSize.width/2, winSize.height/2));
    backGround->setTag(TAG_SPRITE::BACKGROUND);
    addChild(backGround);
}

void PurchaseSuccessDialog::showButton()
{
    auto button = ui::Button::create("shop_dialog_button_yes.png");
    button->addTouchEventListener(CC_CALLBACK_2(PurchaseSuccessDialog::onTouchButton, this));
    button->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    button->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2, 60));
    button->setTag(TAG_SPRITE::BUTTON);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(button, Z_ORDER::Z_BUTTON);
}

void PurchaseSuccessDialog::onTouchButton(cocos2d::Ref *sender, ui::Widget::TouchEventType type)
{
    if(type == ui::Widget::TouchEventType::ENDED)
    {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        getEventDispatcher()->removeEventListener(_listener);
        removeFromParent();
    }
}