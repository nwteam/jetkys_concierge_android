#ifndef __syanago__PurchaseSuccessDialog__
#define __syanago__PurchaseSuccessDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class PurchaseSuccessDialog : public Layer, public create_func<PurchaseSuccessDialog>
{
public:
    using create_func::create;
    bool init();
    
    PurchaseSuccessDialog(){};
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        BUTTON,
    };
    enum Z_ORDER{
        Z_BUTTON = 0,
    };
    
    EventListenerTouchOneByOne* _listener;
    
    void showBackground();
    void showButton();
    
    void onTouchButton(Ref* sender, ui::Widget::TouchEventType type);
};

#endif
