#include "Token.h"

Token::Token(Json* json)
{
    itemId = atoi(Json_getString(json, "id", "-1"));
    name = Json_getString(json, "name", "");
    quantity = atoi(Json_getString(json, "quantity", "-1"));
    quantityFree = atoi(Json_getString(json, "quantity_free", "-1"));
    priceYen = atoi(Json_getString(json, "price_yen", "-1"));
    remarks = Json_getString(json, "remarks", "");
    imageId = atoi(Json_getString(json, "image_id", ""));
    productId = Json_getString(json, "product_id", "");
    startDateTime = Json_getString(json, "start_datetime", "");
    endDateTime = Json_getString(json, "end_datetime", "");
}