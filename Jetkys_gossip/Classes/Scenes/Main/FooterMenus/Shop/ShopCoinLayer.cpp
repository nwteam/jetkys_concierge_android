#include "ShopCoinLayer.h"
#include "FontDefines.h"
#include "Cell.h"
#include "DialogView.h"
#include "PlayerController.h"
#include "MenuTouch.h"
#include "MeasurementInformation.h"
#include "HeaderStatus.h"
#include "Native.h"
#include "ArtLtvMeasurement.h"
#include "MeasurementInformation.h"

#define SIZE_COIN_CELL Sprite::create("shop_base.png")->getContentSize()

ShopCoinLayer::ShopCoinLayer():
    _progress(nullptr), _request(nullptr)
{
    _title = "コイン購入所";
    _buyID = -1;

    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}

ShopCoinLayer::~ShopCoinLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

bool ShopCoinLayer::init()
{
    if (!ShopLayerBase::init()) {
        return false;
    }
    // initialize
    _numberOfCell = 0;
    _coins = std::vector<CoinInfo>();

    Size winSize = Director::getInstance()->getWinSize();

    onEnterTransitionDidFinish();

    return true;
}

void ShopCoinLayer::onEnterTransitionDidFinish()
{
    Layer::onEnterTransitionDidFinish();

    _progress->onStart();
    _request->getCoinExchangeList(CC_CALLBACK_1(ShopCoinLayer::responseGetCoinExchangeList, this));
}

void ShopCoinLayer::responseGetCoinExchangeList(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "get_coin_exchange_list");
    std::string result = Json_getString(jsonData1, "result", "false");

    if (result == "true") {
        Json* jsonDataData = Json_getItem(jsonData1, "data");

        Json* mDatas = Json_getItem(jsonDataData, "exchange");
        if (mDatas && mDatas->type == Json_Array) {
            Json* child;
            for (child = mDatas->child; child; child = child->next) {
                CoinInfo token = CoinInfo(
                    atoi(Json_getString(child, "id", "-1")),
                    Json_getString(child, "name", ""),
                    atoi(Json_getString(child, "coin", "-1")),
                    atoi(Json_getString(child, "token", "-1")),
                    Json_getString(child, "remarks", "")
                    );
                _coins.push_back(token);
            }

            // show table view
            showTable();
        }
    }
}

void ShopCoinLayer::tableCellTouched(TableView* table, TableViewCell* cell)
{
    ////CCLOG("cell touched at index: %ld", cell->getIdx());
}

Size ShopCoinLayer::tableCellSizeForIndex(TableView* table, ssize_t idx)
{
    return _tableCellSize;
}

TableViewCell* ShopCoinLayer::tableCellAtIndex(TableView* table, ssize_t idx)
{
    //    auto string = String::createWithFormat("%ld", idx);
    Cell* cell = (Cell*)table->dequeueCell();
    if (cell)
        cell->removeFromParent();

    // Cell UI
    cell = cellWithIdx((int)idx);

    ////CCLOG("tableCellAtIndex: %ld, %f, %f", idx, cell->getPosition().x, cell->getPosition().y);

    return cell;
}

ssize_t ShopCoinLayer::numberOfCellsInTableView(TableView* table)
{
    return _numberOfCell;
}

Cell* ShopCoinLayer::cellWithIdx(int idx)
{
    if (idx < 2 && _dummyCellNum != 0) {
        auto cell = new Cell();
        cell->autorelease();
        return cell;
    }


    int coinIdx = _numberOfCell - 1 - idx;
    CoinInfo coin = _coins.at(coinIdx);

    auto cell = new Cell();

    // セルの背景
    auto bg = makeSprite("shop_base.png");
    bg->setPosition(Point(SIZE_COIN_CELL.width / 2, SIZE_COIN_CELL.height / 2));
    cell->addChild(bg);

    auto icon = makeSprite("coin_big.png");
    icon->setPosition(Point(60, bg->getContentSize().height / 2));
    bg->addChild(icon);

    // 商品名
    auto title = Label::createWithTTF(coin._name, FONT_NAME_2, 26);
    title->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    title->setAnchorPoint(Point(0, 1));
    title->setPosition(Point(115 + 14, bg->getContentSize().height - 12 - 11));
    bg->addChild(title);

    // トークン表示部分
    auto costBase = makeSprite("shop_token_base.png");
    costBase->setAnchorPoint(Point(0, 1));
    costBase->setPosition(Point(title->getPositionX(), 14 + costBase->getContentSize().height));
    bg->addChild(costBase);

    // トークン数
    auto costNumLabel = Label::createWithTTF(FormatWithCommas(coin._token), FONT_NAME_2, 19);
    costNumLabel->setColor(Color3B::BLACK);
    costNumLabel->setAnchorPoint(Point(1, 0.5));
    costNumLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    costNumLabel->setPosition(Point(costBase->getContentSize().width - 14, costBase->getContentSize().height / 2 - 9.5));
    costBase->addChild(costNumLabel);

    // ボタン
    auto btBuy = makeMenuItem("shop_exchange_button.png", CC_CALLBACK_1(ShopCoinLayer::btMenuItemTableCallback, this));
    btBuy->setAnchorPoint(Point(1, 0.5));
    btBuy->setTag(coinIdx);

    auto menu = MenuTouch::create(btBuy, NULL);
    menu->setAnchorPoint(Point(1, 0.5));
    menu->setPosition(Point(bg->getContentSize().width - 14, bg->getContentSize().height / 2));
    bg->addChild(menu);


    cell->autorelease();

    return cell;
}

void ShopCoinLayer::btMenuItemTableCallback(Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    int coinIdx = ((Node*)pSender)->getTag();
    CoinInfo coin = _coins.at(coinIdx);
    _selectId = coinIdx;
    ////CCLOG("Buy coin: Idx(_tokens) = %d, ID = %d", coinIdx, coin._ID);
    // todo 確認 Popup
    _buyID = coin._ID;
    _dialog = ShopDialog::createWithCoin(coin._token, coin._coin);
    addChild(_dialog);
    _dialog->_delegate  = this;
}

void ShopCoinLayer::showTable()
{
    // table view
    Size winSize = Director::getInstance()->getWinSize();
    _numberOfCell = (int)_coins.size();

    if ((_numberOfCell < 5) || (_numberOfCell == 5 && Director::getInstance()->getWinSize().height == 1136)) {
        _dummyCellNum = 0;
    } else {
        _dummyCellNum = 2;
    }

    _numberOfCell = (int)_coins.size() + _dummyCellNum;

    auto tableView = TableView::create(this, Size(SIZE_COIN_CELL.width, winSize.height - 212));
    tableView->ignoreAnchorPointForPosition(false);
    tableView->setAnchorPoint(Point(0, 1));
    tableView->setPositionX(winSize.width / 2 - SIZE_COIN_CELL.width * 0.5);
    tableView->setDirection(ScrollView::Direction::VERTICAL);
    tableView->setDelegate(this);
    tableView->setBounceable(false);

    // セル数によって表示位置を変更する フッター:163, タイトルバー:77
    int cellNum = _numberOfCell - _dummyCellNum;
    if (cellNum >= 6) {
        _tableCellSize = Size(SIZE_COIN_CELL.width, SIZE_COIN_CELL.height + 14);
        tableView->setPositionY(winSize.height - 222 - 17);
    } else if (cellNum  <= 3) {
        _tableCellSize = Size(SIZE_COIN_CELL.width, SIZE_COIN_CELL.height + 30);
        tableView->setPositionY(winSize.height - 222 - (winSize.height - 385 - _tableCellSize.height * cellNum) / 2);
    } else {
        // セルの隙間を算出
        int cellSpace = (winSize.height - 385 - SIZE_COIN_CELL.height * cellNum) / (cellNum + 1);
        _tableCellSize = Size(SIZE_COIN_CELL.width, SIZE_COIN_CELL.height + cellSpace);
        tableView->setPositionY(winSize.height - 222 - cellSpace / 2);
    }
    addChild(tableView);
    _scrollBar = ScrollBar::initScrollBar(winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 - 180 - 30, winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65, tableView->getContainer()->getContentSize().height, tableView->getContentOffset().y);
    addChild(_scrollBar);
    _scrollBar->setPosition(Point(winSize.width - ((winSize.width / 2) - (567 / 2)) - 10, winSize.height / 2 - 15 - HEADER_STATUS::DIFFERENCE::HEIGHT_DIFF / 2));
    if (winSize.height - 29 - HEADER_STATUS::SIZE::HEIGHT - 65 <= tableView->getContainer()->getContentSize().height) {
        tableView->setPosition(tableView->getPosition() + Point(-20, 0));
    }
    tableView->reloadData();
}

void ShopCoinLayer::btDialogCallback(ButtonIndex aIndex)
{
    if (aIndex == BT_YES) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        int token = PLAYERCONTROLLER->_player->getFreeToken() + PLAYERCONTROLLER->_player->getPayToken();
        CoinInfo coin = _coins.at(_selectId);
        int payToken = coin._token;
        if (token >= payToken) {
            _progress->onStart();
            _request->exchangeIntoCoin(_buyID, CC_CALLBACK_1(ShopCoinLayer::responseExchangeIntoCoin, this));
        } else {
            _dialog = ShopDialog::createWithGotoScene(GOS_TOKEN);
            addChild(_dialog, DIALOG_ZORDER);
            _dialog->_delegate = this;
        }
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }
    _buyID = -1;
}

void ShopCoinLayer::responseExchangeIntoCoin(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "exchange_into_coin");
    std::string result = Json_getString(jsonData1, "result", "false");

    if (result == "true") {
        Json* jsonDataData = Json_getItem(jsonData1, "data");

        Json* mDatas = Json_getItem(jsonDataData, "players");

        // Save
        int freeCoin = atoi(Json_getString(mDatas, "free_coin", "-1"));
        int payCoin = atoi(Json_getString(mDatas, "pay_coin", "-1"));
        int buyCoin = freeCoin + payCoin - PLAYERCONTROLLER->_player->getFreeCoin() - PLAYERCONTROLLER->_player->getPayCoin();
        PLAYERCONTROLLER->_player->setFreeCoin(freeCoin);
        PLAYERCONTROLLER->_player->setPayCoin(payCoin);
        PLAYERCONTROLLER->_player->setFreeToken(atoi(Json_getString(mDatas, "free_token", "-1")));
        PLAYERCONTROLLER->_player->setPayToken(atoi(Json_getString(mDatas, "pay_token", "-1")));
        PLAYERCONTROLLER->_player->update();

        // //CCLOG("buy coin :%d",buyCoin);
        switch (buyCoin) {
        case 30000:                 // 1 token
            ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_COIN_30000, (int)PLAYERCONTROLLER->_player->getID());
            break;
        case 165000:                // 5 token
            ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_COIN_165000, (int)PLAYERCONTROLLER->_player->getID());
            break;
        case 360000:                // 10 token
            ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_COIN_360000, (int)PLAYERCONTROLLER->_player->getID());
            break;
        case 840000:                // 20 token
            ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_COIN_840000, (int)PLAYERCONTROLLER->_player->getID());
            break;
        default:
            break;
        }

        // Show
        _shopLayer->updateInfoLayer();
        SOUND_HELPER->playeMainSceneEffect(SOUND_BUY, false);

        _dialog = ShopDialog::createWithSuccess("コインに交換しました");
        addChild(_dialog, DIALOG_ZORDER);
        _dialog->_delegate = this;
    }
}

void ShopCoinLayer::btnextSceneCallBack(ButtonIndex aIndex)
{
    if (aIndex == BT_YES) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _shopLayer->showLayer(SI_TOKEN);
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }
}

void ShopCoinLayer::btSuccessCallback(ButtonIndex aIndex)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
}

void ShopCoinLayer::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
    _scrollBar->updateScrollPoint(view->getContentOffset().y);
}
