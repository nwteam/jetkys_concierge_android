#include "ShopDialog.h"
#include "FontDefines.h"
#include "PartModel.h"

USING_NS_CC;

ShopDialog::ShopDialog() {}

ShopDialog::~ShopDialog()
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("icon_pas.plist");
}

ShopDialog* ShopDialog::createWithPart(int id)
{
    auto dialog = new ShopDialog();
    dialog->init(id, 0, 0, 0, 0, SDT_PART);
    dialog->autorelease();
    return dialog;
}

ShopDialog* ShopDialog::createWithGas(int token)
{
    auto dialog = new ShopDialog();
    dialog->init(0, 0, token, 0, 0, SDT_GAS);
    dialog->autorelease();
    return dialog;
}

ShopDialog* ShopDialog::createWithToken(int yen, int token)
{
    auto dialog = new ShopDialog();
    dialog->init(0, 0, token, yen, 0, SDT_TOKEN);
    dialog->autorelease();
    return dialog;
}
ShopDialog* ShopDialog::createWithCoin(int token, int coin)
{
    auto dialog = new ShopDialog();
    dialog->init(0, coin, token, 0, 0, SDT_COIN);
    dialog->autorelease();
    return dialog;
}

ShopDialog* ShopDialog::createWithGarage(int token, int num)
{
    auto dialog = new ShopDialog();
    dialog->init(0, 0, token, 0, num, SDT_GARAGE);
    dialog->autorelease();
    return dialog;
}

ShopDialog* ShopDialog::createWithError(std::string errCode, std::string message)
{
    auto dialog = new ShopDialog();
    dialog->initError(errCode, message);
    dialog->autorelease();
    return dialog;
}

ShopDialog* ShopDialog::createWithSuccess(std::string message)
{
    auto dialog = new ShopDialog();
    dialog->initSuccess(message);
    dialog->autorelease();
    return dialog;
}

ShopDialog* ShopDialog::createWithGotoScene(NextScene type)
{
    auto dialog = new ShopDialog();
    dialog->initGotoScene(type);
    dialog->autorelease();
    return dialog;
}

bool ShopDialog::init(int id, int coin, int token, int yen, int num, int type)
{
    if (!LayerPriority::init()) {
        return false;
    }

    auto winSize = Director::getInstance()->getWinSize();

    // レイヤーの背景を指定
    Sprite* bg;
    if (type == SDT_PART) {
        bg = makeSprite("shop_buy_dialog_base.png");
    } else {
        bg = makeSprite("dialog_base.png");
    }
    bg->setAnchorPoint(Point::ZERO);
    bg->setPosition(Point::ZERO);
    addChild(bg);

    auto bgSize = bg->getContentSize();

    // 半透明レイヤーを設定
    auto layer = LayerColor::create(Color4B(0, 0, 0, 128));
    layer->setPosition(Point(-(winSize.width -  bgSize.width) / 2, -(winSize.height -  bgSize.height) / 2));
    addChild(layer, bg->getLocalZOrder() - 1);

    // レイヤーのサイズと位置を指定
    setContentSize(bg->getContentSize());
    ignoreAnchorPointForPosition(false);
    setAnchorPoint(Point(0.5, 0.5));
    setPosition(Point(winSize.width / 2, winSize.height / 2));

    // ボタン
    auto btnYes = makeMenuItem("shop_dialog_button_yes.png", CC_CALLBACK_1(ShopDialog::btnCallback, this));
    ;
    btnYes->setAnchorPoint(Point(1, 0));
    btnYes->setPosition(Point(bgSize.width / 2 - 20, 40));
    btnYes->setTag(BT_YES);

    auto btnNo = makeMenuItem("shop_dialog_button_no.png", CC_CALLBACK_1(ShopDialog::btnCallback, this));
    btnNo->setAnchorPoint(Point(0, 0));
    btnNo->setPosition(Point(bgSize.width / 2 + 20, 40));
    btnNo->setTag(BT_NO);

    auto menu = MenuPriority::create();
    menu->addChild(btnYes);
    menu->addChild(btnNo);
    menu->setPosition(Point::ZERO);

    bg->addChild(menu);

    // タイトル
    Label* titleLabel;

    // 本文
    Label* bodyLabel;
    Label* label2;
    Label* label3;

    // アイコン
    auto iconBase = makeSprite("garage_part_icon_bg.png");
    Sprite* icon;

    // レアリティ
    std::shared_ptr<PartModel>partModel(PartModel::find(id));
    std::vector<Sprite*>raritys;
    Sprite* tmpRarity;
    int rarityPoint;
    if (type == SDT_PART) {
        for (int i = 0; i < partModel->getRarity(); i++) {
            tmpRarity = makeSprite("rarity.png");
            raritys.push_back(tmpRarity);
        }
        rarityPoint = iconBase->getContentSize().width / 2 - raritys.at(0)->getContentSize().width * partModel->getRarity() / 2;
    }
    std::string strValu1;
    std::string strValu2;

    switch (type) {
    // パーツショップ
    case SDT_PART: {
        titleLabel = Label::createWithTTF("パーツショップ", FONT_NAME_2, 34);
        titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        titleLabel->setAnchorPoint(Point(0.5, 1));
        titleLabel->setPosition(Point(bgSize.width / 2, bgSize.height - 30 - 15));

        iconBase->setPosition(Point(bgSize.width / 2, titleLabel->getPositionY() - titleLabel->getContentSize().height + 10));
        iconBase->setAnchorPoint(Point(0.5, 1));
        bg->addChild(iconBase);

        std::shared_ptr<PartModel>partModel(PartModel::find(id));
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon_pas.plist");
        icon = makeSprite(StringUtils::format("%d_pas.png", id).c_str());
        icon->setPosition(Point(iconBase->getContentSize().width / 2, iconBase->getContentSize().height / 2));
        iconBase->addChild(icon);


        for (int i = 0; i < partModel->getRarity(); i++) {
            raritys.at(i)->setAnchorPoint(Point(0, 0));
            raritys.at(i)->setPosition(Point(rarityPoint, -15));
            rarityPoint += raritys.at(i)->getContentSize().width;
            iconBase->addChild(raritys.at(i), 2);
        }
        auto coinSprite1 = Sprite::create("coin_small.png");
        coinSprite1->setAnchorPoint(Point(0.5, 1));
        bg->addChild(coinSprite1);

        strValu1 = FormatWithCommas(partModel->getBuyPrice());
        bodyLabel = Label::createWithTTF(strValu1 + " を使って",
                                         FONT_NAME_2,
                                         26);
        bodyLabel->setColor(COLOR_YELLOW);
        bodyLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        bodyLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        bodyLabel->setAnchorPoint(Point(0.5, 1));
        bodyLabel->setPosition(Point(bgSize.width / 2 + coinSprite1->getContentSize().width / 2 + 10,
                                     iconBase->getPositionY() - iconBase->getContentSize().height - 30 - 15 + 30 - 10));
        coinSprite1->setPosition(bodyLabel->getPosition() - Point(bodyLabel->getContentSize().width / 2 + 10 + coinSprite1->getContentSize().width / 2, 0));

        label2 = Label::createWithTTF(StringUtils::format("「%s」",
                                                          partModel->getName().c_str()),
                                      FONT_NAME_2,
                                      26);
        label2->setColor(COLOR_YELLOW);
        label2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        label2->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        label2->setAnchorPoint(Point(0.5, 1));
        label2->setPosition(Point(bgSize.width / 2,
                                  bodyLabel->getPositionY() - bodyLabel->getContentSize().height / 2 - 6));


        label3 = Label::createWithTTF("を取得しますか？",
                                      FONT_NAME_2,
                                      26);
        label3->setColor(COLOR_YELLOW);
        label3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        label3->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        label3->setAnchorPoint(Point(0.5, 1));
        label3->setPosition(Point(bgSize.width / 2,
                                  label2->getPositionY() - label2->getContentSize().height / 2 - 6));
        break;
    }
    // ガソリンスタンド
    case SDT_GAS: {
        titleLabel = Label::createWithTTF("ガソリンスタンド", FONT_NAME_2, 34);
        titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        titleLabel->setAnchorPoint(Point(0.5, 1));
        titleLabel->setPosition(Point(bgSize.width / 2, bgSize.height - 30));

        auto tokenSprite1 = Sprite::create("token_small.png");
        tokenSprite1->setAnchorPoint(Point(0.5, 1));
        bg->addChild(tokenSprite1);

        strValu1 = FormatWithCommas(token);
        bodyLabel = Label::createWithTTF(strValu1 + " を使って", FONT_NAME_2, 26);
        bodyLabel->setColor(COLOR_YELLOW);
        bodyLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        bodyLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        bodyLabel->setAnchorPoint(Point(0.5, 1));
        bodyLabel->setPosition(Point(bgSize.width / 2 + tokenSprite1->getContentSize().width / 2 + 10, titleLabel->getPositionY() - titleLabel->getContentSize().height / 2 - 50));
        tokenSprite1->setPosition(bodyLabel->getPosition() - Point(bodyLabel->getContentSize().width / 2 + 10 + tokenSprite1->getContentSize().width / 2, 0));

        label2 = Label::createWithTTF("燃料を満タンにしますか？", FONT_NAME_2, 26);
        label2->setColor(COLOR_YELLOW);
        label2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        label2->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        label2->setAnchorPoint(Point(0.5, 1));
        label2->setPosition(Point(bgSize.width / 2, bodyLabel->getPositionY() - bodyLabel->getContentSize().height / 2 - 8));

        break;
    }
    // トークンショップ
    case SDT_TOKEN: {
        titleLabel = Label::createWithTTF("トークンショップ", FONT_NAME_2, 34);
        titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        titleLabel->setAnchorPoint(Point(0.5, 1));
        titleLabel->setPosition(Point(bgSize.width / 2, bgSize.height - 30 - 15));

        strValu1 = FormatWithCommas(yen);
        bodyLabel = Label::createWithTTF(strValu1 + "円 を使って", FONT_NAME_2, 26);
        bodyLabel->setColor(COLOR_YELLOW);
        bodyLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        bodyLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        bodyLabel->setAnchorPoint(Point(0.5, 1));
        bodyLabel->setPosition(Point(bgSize.width / 2, titleLabel->getPositionY() - titleLabel->getContentSize().height / 2 - 50));

        auto tokenSprite1 = Sprite::create("token_small.png");
        tokenSprite1->setAnchorPoint(Point(0.5, 1));
        bg->addChild(tokenSprite1);

        strValu2 = FormatWithCommas(token);
        label2 = Label::createWithTTF(strValu2 + " を取得しますか？", FONT_NAME_2, 26);
        label2->setColor(COLOR_YELLOW);
        label2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        label2->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        label2->setAnchorPoint(Point(0.5, 1));
        label2->setPosition(Point(bgSize.width / 2 + tokenSprite1->getContentSize().width / 2 + 10, bodyLabel->getPositionY() - bodyLabel->getContentSize().height / 2 - 8));
        tokenSprite1->setPosition(label2->getPosition() - Point(label2->getContentSize().width / 2 + 10 + tokenSprite1->getContentSize().width / 2, 0));
        break;
    }
    // コイン購入所
    case SDT_COIN: {
        titleLabel = Label::createWithTTF("コイン購入所", FONT_NAME_2, 34);
        titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        titleLabel->setAnchorPoint(Point(0.5, 1));
        titleLabel->setPosition(Point(bgSize.width / 2, bgSize.height - 30 - 15));

        auto tokenSprite1 = Sprite::create("token_small.png");
        tokenSprite1->setAnchorPoint(Point(0.5, 1));
        bg->addChild(tokenSprite1);

        strValu1 = FormatWithCommas(token);
        bodyLabel = Label::createWithTTF(strValu1 + " を使って", FONT_NAME_2, 26);
        bodyLabel->setColor(COLOR_YELLOW);
        bodyLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        bodyLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        bodyLabel->setAnchorPoint(Point(0.5, 1));
        bodyLabel->setPosition(Point(bgSize.width / 2 + tokenSprite1->getContentSize().width / 2 + 10, titleLabel->getPositionY() - titleLabel->getContentSize().height / 2 - 50));
        tokenSprite1->setPosition(bodyLabel->getPosition() - Point(bodyLabel->getContentSize().width / 2 + 10 + tokenSprite1->getContentSize().width / 2, 0));

        auto coinSprite1 = Sprite::create("coin_small.png");
        coinSprite1->setAnchorPoint(Point(0.5, 1));
        bg->addChild(coinSprite1);

        strValu2 = FormatWithCommas(coin);
        label2 = Label::createWithTTF(strValu2 + " を取得しますか？", FONT_NAME_2, 26);
        label2->setColor(COLOR_YELLOW);
        label2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        label2->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        label2->setAnchorPoint(Point(0.5, 1));
        label2->setPosition(Point(bgSize.width / 2 + coinSprite1->getContentSize().width / 2 + 10, bodyLabel->getPositionY() - bodyLabel->getContentSize().height / 2 - 8));
        coinSprite1->setPosition(label2->getPosition() - Point(label2->getContentSize().width / 2 + 10 + coinSprite1->getContentSize().width / 2, 0));

        break;
    }
    // ガレージ拡張
    case SDT_GARAGE: {
        titleLabel = Label::createWithTTF("ガレージ拡張", FONT_NAME_2, 34);
        titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        titleLabel->setAnchorPoint(Point(0.5, 1));
        titleLabel->setPosition(Point(bgSize.width / 2, bgSize.height - 30));


        auto tokenSprite1 = Sprite::create("token_small.png");
        tokenSprite1->setAnchorPoint(Point(0.5, 1));
        bg->addChild(tokenSprite1);

        strValu1 = FormatWithCommas(token);
        bodyLabel = Label::createWithTTF(strValu1 + " を使って", FONT_NAME_2, 26);
        bodyLabel->setColor(COLOR_YELLOW);
        bodyLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        bodyLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        bodyLabel->setAnchorPoint(Point(0.5, 1));
        bodyLabel->setPosition(Point(bgSize.width / 2 + tokenSprite1->getContentSize().width / 2 + 10, titleLabel->getPositionY() - titleLabel->getContentSize().height / 2 - 50 + 21));
        tokenSprite1->setPosition(bodyLabel->getPosition() - Point(bodyLabel->getContentSize().width / 2 + 10 + tokenSprite1->getContentSize().width / 2, 0));

        label2 = Label::createWithTTF(StringUtils::format("ガレージを%d体分拡張しますか？", num), FONT_NAME_2, 26);
        label2->setColor(COLOR_YELLOW);
        label2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        label2->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        label2->setAnchorPoint(Point(0.5, 1));
        label2->setPosition(Point(bgSize.width / 2, bodyLabel->getPositionY() - bodyLabel->getContentSize().height / 2 - 8));

        label3 = Label::createWithTTF(StringUtils::format("※装備枠も%d追加!", (num * 2)), FONT_NAME_2, 26);
        label3->setColor(COLOR_YELLOW);
        label3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        label3->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        label3->setAnchorPoint(Point(0.5, 1));
        label3->setPosition(Point(bgSize.width / 2,
                                  label2->getPositionY() - label2->getContentSize().height / 2 - 8));
        break;
    }
    default:
        break;
    }

    if (type == SDT_PART || type == SDT_GARAGE) {
        bg->addChild(label3);
    }

    bg->addChild(label2);
    bg->addChild(titleLabel);
    bg->addChild(bodyLabel);



    return true;
}

bool ShopDialog::initError(std::string errCode, std::string message)
{
    if (!LayerPriority::init()) {
        return false;
    }

    // タッチ設定
//    auto listener = EventListenerTouchOneByOne::create();
//    listener->setSwallowTouches(true);
//    listener->onTouchBegan = [](Touch *touch,Event*event)->bool{
//        return true;
//    };
//    auto dip = Director::getInstance()->getEventDispatcher();
//    dip->addEventListenerWithSceneGraphPriority(listener, this);
//    dip->setPriority(listener, -1);


    auto winSize = Director::getInstance()->getWinSize();

    // レイヤーの背景を指定
    auto bg = makeSprite("shop_buy_dialog_base.png");
    bg->setAnchorPoint(Point::ZERO);
    bg->setPosition(Point::ZERO);
    addChild(bg);

    auto bgSize = bg->getContentSize();

    // 半透明レイヤーを設定
    auto layer = LayerColor::create(Color4B(0, 0, 0, 128));
    layer->setPosition(Point(-(winSize.width -  bgSize.width) / 2, -(winSize.height -  bgSize.height) / 2));
    addChild(layer, bg->getLocalZOrder() - 1);

    // レイヤーのサイズと位置を指定
    setContentSize(bg->getContentSize());
    ignoreAnchorPointForPosition(false);
    setAnchorPoint(Point(0.5, 0.5));
    setPosition(Point(winSize.width / 2, winSize.height / 2));

    // タイトル
    auto titleLabel = Label::createWithTTF(errCode, FONT_NAME_2, 30);
    titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    titleLabel->setAnchorPoint(Point(0.5, 1));
    titleLabel->setPosition(Point(bgSize.width / 2, bgSize.height - 30 - 15));
    bg->addChild(titleLabel);

    // 本文
    auto bodyLabel = Label::createWithTTF(message, FONT_NAME_2, 30);
    bodyLabel->setColor(COLOR_YELLOW);
    bodyLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    bodyLabel->setAnchorPoint(Point(0.5, 1));
    bodyLabel->setPosition(Point(bgSize.width / 2, titleLabel->getPositionY() - titleLabel->getContentSize().height - 30));
    bg->addChild(bodyLabel);

    // ボタン
    auto btnYes = makeMenuItem("shop_dialog_button_yes.png", CC_CALLBACK_1(ShopDialog::btnCallback, this));
    ;
    btnYes->setAnchorPoint(Point(0.5, 0));
    btnYes->setPosition(Point(bgSize.width / 2, 30));
    btnYes->setTag(BT_YES_2);

    auto menu = MenuPriority::create();
    menu->addChild(btnYes);
    menu->setPosition(Point::ZERO);

    bg->addChild(menu);

    return true;
}

bool ShopDialog::initSuccess(std::string message)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;
    // タッチ設定
//    auto listener = EventListenerTouchOneByOne::create();
//    listener->setSwallowTouches(true);
//    listener->onTouchBegan = [](Touch *touch,Event*event)->bool{
//        return true;
//    };
//    auto dip = Director::getInstance()->getEventDispatcher();
//    dip->addEventListenerWithSceneGraphPriority(listener, this);
//    dip->setPriority(listener, -1);


    auto winSize = Director::getInstance()->getWinSize();

    // レイヤーの背景を指定
    auto bg = makeSprite("dialog_base.png");
    bg->setAnchorPoint(Point::ZERO);
    bg->setPosition(Point::ZERO);
    addChild(bg);

    auto bgSize = bg->getContentSize();


    // 半透明レイヤーを設定
    auto layer = LayerColor::create(Color4B(0, 0, 0, 128));
    layer->setPosition(Point(-(winSize.width -  bgSize.width) / 2, -(winSize.height -  bgSize.height) / 2));
    addChild(layer, bg->getLocalZOrder() - 1);

    // レイヤーのサイズと位置を指定
    setContentSize(bg->getContentSize());
    ignoreAnchorPointForPosition(false);
    setAnchorPoint(Point(0.5, 0.5));
    setPosition(Point(winSize.width / 2, winSize.height / 2));

    // ボタン
    auto btnYes = makeMenuItem("shop_dialog_button_yes.png", CC_CALLBACK_1(ShopDialog::btSuccesscall, this));
    ;
    btnYes->setAnchorPoint(Point(0.5, 0));
    btnYes->setPosition(Point(bgSize.width / 2, 40));
    btnYes->setTag(BT_YES_2);

    // 本文
    auto bodyLabel = Label::createWithTTF(message, FONT_NAME_2, 33);
    bodyLabel->setColor(COLOR_YELLOW);
    bodyLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    bodyLabel->setAnchorPoint(Point(0.5, 0.5));
    bodyLabel->setPosition(Point(bgSize.width / 2, (bgSize.height -  (btnYes->getContentSize().height + 40)) / 2  - 18 + btnYes->getPosition().y + btnYes->getContentSize().height));
    bg->addChild(bodyLabel);


    auto menu = MenuPriority::create();
    menu->addChild(btnYes);
    menu->setPosition(Point::ZERO);

    bg->addChild(menu);

    return true;
}

bool ShopDialog::initGotoScene(NextScene type)
{
    if (!LayerPriority::init()) {
        return false;
    }
    // タッチ設定
//    auto listener = EventListenerTouchOneByOne::create();
//    listener->setSwallowTouches(true);
//    listener->onTouchBegan = [](Touch *touch,Event*event)->bool{
//        return true;
//    };
//    auto dip = Director::getInstance()->getEventDispatcher();
//    dip->addEventListenerWithSceneGraphPriority(listener, this);
//    dip->setPriority(listener, -1);


    auto winSize = Director::getInstance()->getWinSize();

    // レイヤーの背景を指定
    Sprite* bg;
    bg = makeSprite("dialog_base.png");
    bg->setAnchorPoint(Point::ZERO);
    bg->setPosition(Point::ZERO);
    addChild(bg);

    auto bgSize = bg->getContentSize();

    // 半透明レイヤーを設定
    auto layer = LayerColor::create(Color4B(0, 0, 0, 128));
    layer->setPosition(Point(-(winSize.width -  bgSize.width) / 2, -(winSize.height -  bgSize.height) / 2));
    addChild(layer, bg->getLocalZOrder() - 1);

    // レイヤーのサイズと位置を指定
    setContentSize(bg->getContentSize());
    ignoreAnchorPointForPosition(false);
    setAnchorPoint(Point(0.5, 0.5));
    setPosition(Point(winSize.width / 2, winSize.height / 2));

    // ボタン
    auto btnYes = makeMenuItem("shop_dialog_button_yes.png", CC_CALLBACK_1(ShopDialog::btNextSceneCallback, this));
    ;
    btnYes->setAnchorPoint(Point(1, 0));
    btnYes->setPosition(Point(bgSize.width / 2 - 20, 40));
    btnYes->setTag(BT_YES);

    auto btnNo = makeMenuItem("shop_dialog_button_no.png", CC_CALLBACK_1(ShopDialog::btNextSceneCallback, this));
    btnNo->setAnchorPoint(Point(0, 0));
    btnNo->setPosition(Point(bgSize.width / 2 + 20, 40));
    btnNo->setTag(BT_NO);

    auto menu = MenuPriority::create();
    menu->addChild(btnYes);
    menu->addChild(btnNo);
    menu->setPosition(Point::ZERO);

    bg->addChild(menu);

    // タイトル
    Label* titleLabel;

    // 本文
    Label* bodyLabel;

    switch (type) {
    case GOS_COIN:
        titleLabel = Label::createWithTTF("コインが足りません", FONT_NAME_2, 34);
        titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        titleLabel->setAnchorPoint(Point(0.5, 1));
        titleLabel->setPosition(Point(bgSize.width / 2, bgSize.height - 30));

        bodyLabel = Label::createWithTTF("コインを購入しますか？", FONT_NAME_2, 26);
        bodyLabel->setColor(COLOR_YELLOW);
        bodyLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        bodyLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        bodyLabel->setAnchorPoint(Point(0.5, 1));
        bodyLabel->setPosition(Point(bgSize.width / 2, titleLabel->getPositionY() - titleLabel->getContentSize().height / 2 - 50 - 8 - 26));

        break;
    case GOS_TOKEN:
        titleLabel = Label::createWithTTF("トークンが足りません", FONT_NAME_2, 34);
        titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        titleLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        titleLabel->setAnchorPoint(Point(0.5, 1));
        titleLabel->setPosition(Point(bgSize.width / 2, bgSize.height - 30));

        bodyLabel = Label::createWithTTF("トークンを購入しますか？", FONT_NAME_2, 26);
        bodyLabel->setColor(COLOR_YELLOW);
        bodyLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        bodyLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        bodyLabel->setAnchorPoint(Point(0.5, 1));
        bodyLabel->setPosition(Point(bgSize.width / 2, titleLabel->getPositionY() - titleLabel->getContentSize().height / 2 - 50 - 8 - 26));

        break;
    default:
        break;
    }

    bg->addChild(titleLabel);
    bg->addChild(bodyLabel);

    return true;
}

void ShopDialog::btnCallback(Ref* pSender)
{
    ////CCLOG("ShopDialog::btnCallback");
    ButtonIndex tag = (ButtonIndex)((Node*)pSender)->getTag();
    ////CCLOG("btIndex = %d", (int)tag);


    if (_delegate != nullptr) {
        if (tag == BT_YES || tag == BT_NO) {
            ////CCLOG("delegate call");
            _delegate->btDialogCallback(tag);
        }
    }
    removeFromParent();
}

void ShopDialog::btNextSceneCallback(Ref* pSender)
{
    ButtonIndex tag = (ButtonIndex)((Node*)pSender)->getTag();
    ShopDialogDelegate* delegate = _delegate;
    removeFromParent();
    if (delegate != nullptr) {
        if (tag == BT_YES || tag == BT_NO)
            delegate->btnextSceneCallBack(tag);
    }
}

void ShopDialog::btSuccesscall(Ref* pSender)
{
    ButtonIndex tag = (ButtonIndex)((Node*)pSender)->getTag();
    ShopDialogDelegate* delegate = _delegate;
    removeFromParent();
    if (delegate != nullptr) {
        delegate->btSuccessCallback(tag);
    }
}
