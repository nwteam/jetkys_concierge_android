#include "ShopGarageLayer.h"
#include "jCommon.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include "DialogView.h"
#include "MeasurementInformation.h"
#include "SystemSettingModel.h"
#include "Native.h"
#include "ArtLtvMeasurement.h"
#include "MeasurementInformation.h"


#define FONT_SIZE_1 26
#define FONT_SIZE_2 30
#define SPACE_1     30
#define SPACE_2     10


ShopGarageLayer::ShopGarageLayer():
    _progress(nullptr), _request(nullptr)
{
    _title = "ガレージ拡張";
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}

ShopGarageLayer::~ShopGarageLayer()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

bool ShopGarageLayer::init()
{
    if (!ShopLayerBase::init()) {
        return false;
    }

    initBaseLayer();

    return true;
}

void ShopGarageLayer::initBaseLayer()
{
    _baseLayer = NULL;
    _baseLayer = Layer::create();
    addChild(_baseLayer);

    Size winSize = Director::getInstance()->getWinSize();

    std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());

    // ベース
    auto mainBase = makeSprite("shop_garage_fuel_base.png");
    mainBase->setAnchorPoint(Point(0.5, 1));
    mainBase->setPosition(Vec2(winSize.width * 0.5, winSize.height * 0.5 + mainBase->getContentSize().height * 0.5));
    _baseLayer->addChild(mainBase);

    // 保持数表示ベース
    auto ownHoldBase = makeSprite("shop_garage_fuel_hold_base.png");
    ownHoldBase->setAnchorPoint(Point(0.5, 1));
    ownHoldBase->setPosition(Point(mainBase->getContentSize().width * 0.5, mainBase->getContentSize().height - SPACE_1));
    mainBase->addChild(ownHoldBase);

    // 保持数文字ラベル
    auto ownHoldLabel = Label::createWithTTF("　車なご保持数", FONT_NAME_2, FONT_SIZE_1);
    ownHoldLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    ownHoldLabel->setColor(Color3B::BLACK);
    ownHoldLabel->setAnchorPoint(Point(0, 0.5));
    ownHoldLabel->cocos2d::Node::setPosition(Point(0,
                                                   ownHoldBase->getContentSize().height * 0.5 - FONT_SIZE_1 / 2));
    ownHoldBase->addChild(ownHoldLabel);

    // ガレージ数ラベル
    auto ownGarageLabel = Label::createWithTTF(StringUtils::format("／%d　", PLAYERCONTROLLER->_player->getGarageSize()), FONT_NAME_2, FONT_SIZE_1);
    ownGarageLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    ownGarageLabel->setColor(Color3B::BLACK);
    ownGarageLabel->setAnchorPoint(Point(1, 0.5));
    ownGarageLabel->cocos2d::Node::setPosition(Point(ownHoldBase->getContentSize().width,
                                                     ownHoldLabel->getPositionY()));
    ownHoldBase->addChild(ownGarageLabel);

    // 車なご保持数ラベル
    auto ownHoldNumLabel = Label::createWithTTF(StringUtils::format("%lu", PLAYERCONTROLLER->_playerCharacterModels.size()), FONT_NAME_2, FONT_SIZE_1);
    ownHoldNumLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    ownHoldNumLabel->setColor(Color3B::RED);
    ownHoldNumLabel->setAnchorPoint(Point(1, 0.5));
    ownHoldNumLabel->cocos2d::Node::setPosition(Point(ownGarageLabel->getPositionX() - ownGarageLabel->getContentSize().width,
                                                      ownGarageLabel->getPositionY()));
    ownHoldBase->addChild(ownHoldNumLabel);

    // 本文ラベル
    auto tokenSprite1 = Sprite::create("token_small.png");
    tokenSprite1->setAnchorPoint(Point(0.5, 1));
    mainBase->addChild(tokenSprite1);
    auto mainLabel = Label::createWithTTF(StringUtils::format("%d を消費して",
                                                              systemSettingModel->getNumOfTokenForExtension()),
                                          FONT_NAME_2,
                                          FONT_SIZE_1);
    mainLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    mainLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    mainLabel->setAnchorPoint(Point(0.5, 1));
    mainLabel->setPosition(Point(mainBase->getContentSize().width * 0.5 + tokenSprite1->getContentSize().width * 0.5 + SPACE_2
                                 , ownHoldBase->getPositionY() - ownHoldBase->getContentSize().height - SPACE_1 - 20));
    mainBase->addChild(mainLabel);
    tokenSprite1->setPosition(mainLabel->getPosition() - Point(tokenSprite1->getContentSize().width / 2 + SPACE_2 + mainLabel->getContentSize().width / 2, 0));

    auto mainLabel2 = Label::createWithTTF(StringUtils::format("ガレージを%d体分拡張できます。",
                                                               systemSettingModel->getExpandedNumOfGarage()),
                                           FONT_NAME_2,
                                           FONT_SIZE_1);
    mainLabel2->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    mainLabel2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    mainLabel2->setAnchorPoint(Point(0.5, 1));
    mainLabel2->setPosition(Point(mainBase->getContentSize().width * 0.5, mainLabel->getPositionY() - 33));
    mainBase->addChild(mainLabel2);


    // 所持ラベル
    auto tokenHoldLabel = Label::createWithTTF("所持", FONT_NAME_2, FONT_SIZE_1);
    tokenHoldLabel->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    tokenHoldLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    tokenHoldLabel->setAnchorPoint(Point(0, 1));
    mainBase->addChild(tokenHoldLabel);

    // トークン画像2
    auto tokenSprite2 = Sprite::create("token_small.png");
    tokenSprite2->setAnchorPoint(Point(0, 1));
    mainBase->addChild(tokenSprite2);

    // 所持トークン数
    auto tokenHoldNum = Label::createWithTTF(StringUtils::format("%d", PLAYERCONTROLLER->_player->getPayToken() + PLAYERCONTROLLER->_player->getFreeToken()), FONT_NAME_2, FONT_SIZE_1);
    tokenHoldNum->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    tokenHoldNum->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    tokenHoldNum->setAnchorPoint(Point(0, 1));
    mainBase->addChild(tokenHoldNum);



    // トークン系を中央に表示するための処理
    // トークン表示の横幅
    int tokenWidth = tokenHoldLabel->getContentSize().width + SPACE_2 + tokenSprite2->getContentSize().width + SPACE_2 + tokenHoldNum->getContentSize().width;
    int tokenLeftPoint = (mainBase->getContentSize().width - tokenWidth) / 2;

    // 所持トークン系の座標を設定
    tokenHoldLabel->setPosition(Point(tokenLeftPoint, mainLabel2->getPositionY() - 50));
    tokenSprite2->setPosition(Point(tokenHoldLabel->getPositionX() + tokenHoldLabel->getContentSize().width + SPACE_2, tokenHoldLabel->getPositionY()));

    tokenHoldNum->setPosition(Point(tokenSprite2->getPositionX() + tokenSprite2->getContentSize().width + SPACE_2, tokenSprite2->getPositionY()));

    // ボタン
    auto btn = makeMenuItem("shop_garage_fuel_button.png", CC_CALLBACK_1(ShopGarageLayer::btExtendCallback, this));
    btn->setAnchorPoint(Point(0.5, 1));

    // ボタン文字
    auto btnLabel = Label::createWithTTF("ガレージを拡張する", FONT_NAME_2, FONT_SIZE_2);
    btnLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    btnLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    btnLabel->setPosition(Point(btn->getContentSize().width / 2, btn->getContentSize().height / 2 - FONT_SIZE_2 / 2));
    btn->addChild(btnLabel);

    auto menu = MenuTouch::create(btn, NULL);

    menu->setPosition(Point(mainBase->getContentSize().width * 0.5, SPACE_1 + btn->getContentSize().height));
    mainBase->addChild(menu, 2);
}

void ShopGarageLayer::btExtendCallback(cocos2d::Ref* pSender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
    _dialog = ShopDialog::createWithGarage(systemSettingModel->getNumOfTokenForExtension(),
                                           systemSettingModel->getExpandedNumOfGarage());
    addChild(_dialog);
    _dialog->_delegate  = this;
}

void ShopGarageLayer::btDialogCallback(ButtonIndex aIndex)
{
    if (aIndex == BT_YES) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        int token = PLAYERCONTROLLER->_player->getFreeToken() + PLAYERCONTROLLER->_player->getPayToken();
        std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
        int payToken = systemSettingModel->getNumOfTokenForExtension();
        if (token >= payToken) {
            _progress->onStart();
            _request->extendGarage(CC_CALLBACK_1(ShopGarageLayer::responseExtendGarage, this));
        } else {
            _dialog = ShopDialog::createWithGotoScene(GOS_TOKEN);
            addChild(_dialog, DIALOG_ZORDER);
            _dialog->_delegate  = this;
        }
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }
}

void ShopGarageLayer::responseExtendGarage(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "extend_garage");
    std::string result = Json_getString(jsonData1, "result", "false");

    if (result == "true") {
        Json* jsonDataData = Json_getItem(jsonData1, "data");

        Json* mDatas = Json_getItem(jsonDataData, "players");
        int garage_size = atoi(Json_getString(mDatas, "garage_size", "-1"));
        int free_token = atoi(Json_getString(mDatas, "free_token", "-1"));
        int pay_token = atoi(Json_getString(mDatas, "pay_token", "-1"));

        PLAYERCONTROLLER->_player->setGarageSize(garage_size);
        PLAYERCONTROLLER->_player->setFreeToken(free_token);
        PLAYERCONTROLLER->_player->setPayToken(pay_token);
        PLAYERCONTROLLER->_player->update();

        _shopLayer->updateInfoLayer();
        _baseLayer->removeFromParent();
        initBaseLayer();
        SOUND_HELPER->playeMainSceneEffect(SOUND_BUY, false);

        // ART Expland garage
        ArtLtvMeasurement::send(MEASURMENT_INFO::ART::EXPANDED_GARAGE, (int)PLAYERCONTROLLER->_player->getID());       // ART ガレージ拡張

        _dialog = ShopDialog::createWithSuccess("ガレージを拡張しました");
        addChild(_dialog, DIALOG_ZORDER);
        _dialog->_delegate = this;
    }
}

void ShopGarageLayer::btnextSceneCallBack(ButtonIndex aIndex)
{
    if (aIndex == BT_YES) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _shopLayer->showLayer(SI_TOKEN);
    } else {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    }
}

void ShopGarageLayer::btSuccessCallback(ButtonIndex aIndex)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
}
