#include "FooterLayer.h"
#include "FontDefines.h"
#include "MainScene.h"
#include "HomeButton.h"
#include "ItemBoxButton.h"
#include "SystemSettingModel.h"

FooterLayer::FooterLayer():
    _startTab(0) {}

bool FooterLayer::init()
{
    if (!Layer::init()) {
        return false;
    }
    showBackGround();

    showStartButton();
    showGachaButton();
    showFriendButton();
    showShopButton();
    showOtherButton();

    showHomeButton();
    showItemBoxButton();

    addBadges();

    return true;
}

void FooterLayer::showBackGround()
{
    auto background = makeSprite("menu_background.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2, background->getContentSize().height / 2));
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void FooterLayer::showStartButton()
{
    ui::Button* startButton = ui::Button::create("menu_start.png");
    startButton->setTag(TAG_SPRITE::START);
    startButton->addTouchEventListener(CC_CALLBACK_2(FooterLayer::onTapStartButton, this));
    startButton->setPosition(Point(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition().x,
                                   startButton->getContentSize().height / 2));
    addChild(startButton, Z_ORDER::Z_START);
}

void FooterLayer::onTapStartButton(Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
    checkSingleTap(sender, type, [this]() {
        _startTab++;
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        if (_mainScene->getChildByTag(MainScene::TAG_SPRITE::ITEM_BOX_LAYER)) {
            _mainScene->removeChildByTag(MainScene::TAG_SPRITE::ITEM_BOX_LAYER);
        }
        if (_startTab <= 1) {
            _mainScene->removeBlackImage();
            _mainScene->showStartLayer();
        }
    });
}

void FooterLayer::showGachaButton()
{
    ui::Button* gachaButton = ui::Button::create("menu_gacha.png");
    gachaButton->setTag(TAG_SPRITE::GACHA);
    gachaButton->addTouchEventListener(CC_CALLBACK_2(FooterLayer::onTapGachaButton, this));
    gachaButton->setPosition(Point(getChildByTag(TAG_SPRITE::START)->getPosition().x - (gachaButton->getContentSize().width / 2 + getChildByTag(TAG_SPRITE::START)->getContentSize().width / 2),
                                   gachaButton->getContentSize().height / 2));
    addChild(gachaButton, Z_ORDER::Z_GACHA);
}

void FooterLayer::onTapGachaButton(Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
    checkSingleTap(sender, type, [this]() {
        _startTab = 0;
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _mainScene->removeBlackImage();
        _mainScene->showLayer(MainIndex::MI_GACHA);
    });
}

void FooterLayer::showFriendButton()
{
    ui::Button* friendButton = ui::Button::create("menu_friend.png");
    friendButton->setTag(TAG_SPRITE::FRIEND);
    friendButton->addTouchEventListener(CC_CALLBACK_2(FooterLayer::onTapFriendButton, this));
    friendButton->setPosition(Point(getChildByTag(TAG_SPRITE::START)->getPosition().x + (friendButton->getContentSize().width / 2 + getChildByTag(TAG_SPRITE::START)->getContentSize().width / 2),
                                    friendButton->getContentSize().height / 2));
    addChild(friendButton, Z_ORDER::Z_FRIEND);
}

void FooterLayer::onTapFriendButton(Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
    checkSingleTap(sender, type, [this]() {
        _startTab = 0;
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _mainScene->removeBlackImage();
        _mainScene->showLayer(MainIndex::MI_FRIEND);
    });
}

void FooterLayer::showShopButton()
{
    ui::Button* shopButton = ui::Button::create("menu_shop.png");
    shopButton->setTag(TAG_SPRITE::SHOP);
    shopButton->addTouchEventListener(CC_CALLBACK_2(FooterLayer::onTapShopButton, this));
    shopButton->setPosition(Point(getChildByTag(TAG_SPRITE::GACHA)->getPosition().x - (shopButton->getContentSize().width / 2 + getChildByTag(TAG_SPRITE::GACHA)->getContentSize().width / 2),
                                  shopButton->getContentSize().height / 2));
    addChild(shopButton, Z_ORDER::Z_SHOP);
}

void FooterLayer::onTapShopButton(Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
    checkSingleTap(sender, type, [this]() {
        _startTab = 0;
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _mainScene->removeBlackImage();
        _mainScene->showLayer(MainIndex::MI_SHOP);
    });
}

void FooterLayer::showOtherButton()
{
    ui::Button* otherButton = ui::Button::create("menu_other.png");
    otherButton->setTag(TAG_SPRITE::OTHER);
    otherButton->addTouchEventListener(CC_CALLBACK_2(FooterLayer::onTapOtherButton, this));
    otherButton->setPosition(Point(getChildByTag(TAG_SPRITE::FRIEND)->getPosition().x + (otherButton->getContentSize().width / 2 + getChildByTag(TAG_SPRITE::FRIEND)->getContentSize().width / 2),
                                   otherButton->getContentSize().height / 2));
    addChild(otherButton, Z_ORDER::Z_OTHER);
}

void FooterLayer::onTapOtherButton(Ref* sender, cocos2d::ui::Widget::TouchEventType type)
{
    checkSingleTap(sender, type, [this]() {
        _startTab = 0;
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _mainScene->removeBlackImage();
        _mainScene->showLayer(MainIndex::MI_OTHERS);
    });
}

void FooterLayer::showHomeButton()
{
    HomeButton* homeButton = HomeButton::create();
    homeButton->setTag(TAG_SPRITE::HOME);
    homeButton->setPosition(Point(getChildByTag(TAG_SPRITE::GACHA)->getPosition().x,
                                  getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height));
    homeButton->addTouchEventListener(CC_CALLBACK_2(FooterLayer::onTapHomeButton, this));
    addChild(homeButton, Z_ORDER::Z_HOME);
}

void FooterLayer::onTapHomeButton(Ref* sender, ui::Widget::TouchEventType type)
{
    checkSingleTap(sender, type, [this, sender]() {
        _startTab = 0;
        HomeButton* button = static_cast<HomeButton*>(sender);
        if (!button->getChildByTag(HomeButton::TAG_SPRITE::HOME_BODY) && !button->getChildByTag(HomeButton::TAG_SPRITE::HOME_COMMENT)) {
            SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
            button->startHomeButtonAnimation();
            _mainScene->removeBlackImage();
            _mainScene->showHomeLayer();
        }
    });
}

void FooterLayer::showItemBoxButton()
{
    auto ItemBoxButton = ItemBoxButton::create();
    ItemBoxButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    ItemBoxButton->addTouchEventListener(CC_CALLBACK_2(FooterLayer::onTapItemBox, this));
    Size winSize = Director::getInstance()->getWinSize();
    ItemBoxButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getPosition().x + 150, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height));
    ItemBoxButton->setTag(TAG_SPRITE::ITEM_BOX);
    addChild(ItemBoxButton, Z_ORDER::Z_ITEM_BOX);
}

void FooterLayer::onTapItemBox(Ref* sender, ui::Widget::TouchEventType type)
{
    checkSingleTap(sender, type, [this, sender]() {
        ItemBoxButton* button = static_cast<ItemBoxButton*>(sender);
        if (!button->getChildByTag(ItemBoxButton::TAG_SPRITE::ITEM_BOX_BODY) && !button->getChildByTag(ItemBoxButton::TAG_SPRITE::ITEM_BOX_COMMENT)) {
            SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
            button->startItemBoxButtonAnimation();
            _mainScene->removeBlackImage();
            _mainScene->showItemBox();
        }
    });
}

void FooterLayer::addBadges()
{
    _gachaBage = createBadge("itemSpr.png", 0);
    _gachaBage->setPosition(Vec2(getChildByTag(TAG_SPRITE::GACHA)->getContentSize().width - 20, getChildByTag(TAG_SPRITE::GACHA)->getContentSize().height - 20));
    _gachaBage->setVisible(false);
    getChildByTag(TAG_SPRITE::GACHA)->addChild(_gachaBage);

    _friendBadge = createBadge("itemSpr.png", PLAYERCONTROLLER->_numberOfPendingFriend);
    _friendBadge->setPosition(Vec2(getChildByTag(TAG_SPRITE::FRIEND)->getContentSize().width - 20, getChildByTag(TAG_SPRITE::FRIEND)->getContentSize().height - 20));
    _friendBadge->setVisible(PLAYERCONTROLLER->_numberOfPendingFriend > 0 ? true : false);
    getChildByTag(TAG_SPRITE::FRIEND)->addChild(_friendBadge);

    const int numberOther = PLAYERCONTROLLER->_numberOfNotice + PLAYERCONTROLLER->_numberOfCompensation;
    _otherBadge = createBadge("itemSpr.png", numberOther);
    _otherBadge->setPosition(Vec2(getChildByTag(TAG_SPRITE::OTHER)->getContentSize().width - _otherBadge->getContentSize().width * 0.5, getChildByTag(TAG_SPRITE::OTHER)->getContentSize().height - 20));
    _otherBadge->setVisible(numberOther > 0 ? true : false);
    getChildByTag(TAG_SPRITE::OTHER)->addChild(_otherBadge);
}

Node* FooterLayer::createBadge(std::string fileName, int number)
{
    auto bage = makeSprite(fileName.c_str());
    auto label = Label::createWithTTF(StringUtils::format("%d", number).c_str(), FONT_NAME_2, 22);
    label->setPosition(Vec2(bage->getContentSize().width / 2, bage->getContentSize().height / 2 - 11));
    label->setTag(TAG_BAGE::LABEL);
    bage->addChild(label);
    return bage;
}

void FooterLayer::setBage()
{
    // update gacha badge
    std::shared_ptr<SystemSettingModel>systemSettingModel(SystemSettingModel::getModel());
    int fpGacha = systemSettingModel->getFpForFreeGacha();
    int playerFp  = PLAYERCONTROLLER->_player->getFriendPoint();
    if (fpGacha < playerFp) {
        int gachaCount = floor(playerFp / fpGacha);
        if (gachaCount > 99) {
            gachaCount = 99;
        }
        ((Label*)_gachaBage->getChildByTag(TAG_BAGE::LABEL))->setString(StringUtils::format("%d", gachaCount));
        _gachaBage->setVisible(true);
    } else {
        ((Label*)_gachaBage->getChildByTag(TAG_BAGE::LABEL))->setString(StringUtils::format("%d", 0));
        _gachaBage->setVisible(false);
    }

    // update friend badge
    ((Label*)_friendBadge->getChildByTag(TAG_BAGE::LABEL))->setString(StringUtils::format("%d", PLAYERCONTROLLER->_numberOfPendingFriend));
    _friendBadge->setVisible(PLAYERCONTROLLER->_numberOfPendingFriend > 0 ? true : false);

    // update other badge
    int numberOther = PLAYERCONTROLLER->_numberOfNotice + PLAYERCONTROLLER->_numberOfCompensation;
    ((Label*)_otherBadge->getChildByTag(TAG_BAGE::LABEL))->setString(StringUtils::format("%d", numberOther));
    _otherBadge->setVisible(numberOther > 0 ? true : false);
}