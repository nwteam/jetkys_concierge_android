#ifndef __Syanago__FooterLayer__
#define __Syanago__FooterLayer__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "check_single_tap.h"

USING_NS_CC;

class MainScene;

class FooterLayer : public Layer, public check_single_tap {
public:
    FooterLayer();
    bool init();
    CREATE_FUNC(FooterLayer);
    
    void setBage();
    
    MainScene *_mainScene;
    
    int _startTab;// おさらく昔のスタート連打で重くなるバグの回避用の変数
    
private:
    enum TAG_SPRITE {
        HOME = 0,
        SHOP,
        GACHA,
        START,
        FRIEND,
        OTHER,
        BACKGROUND,
        ITEM_BOX,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_SHOP,
        Z_GACHA,
        Z_START,
        Z_FRIEND,
        Z_OTHER,
        Z_ITEM_BOX,
        Z_HOME,
    };
    enum TAG_BAGE{
        LABEL = 1,
    };
    
    void showBackGround();
    
    void showStartButton();
    void onTapStartButton(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    
    void showGachaButton();
    void onTapGachaButton(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    
    void showShopButton();
    void onTapShopButton(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    
    void showFriendButton();
    void onTapFriendButton(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    
    void showOtherButton();
    void onTapOtherButton(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    
    void showHomeButton();
    void onTapHomeButton(Ref *sender, ui::Widget::TouchEventType type);
    
    void showItemBoxButton();
    void onTapItemBox(Ref* sender, ui::Widget::TouchEventType type);
    
    void addBadges();
    Node* createBadge(std::string fileName,int number);
    
    Node *_gachaBage;
    Node *_friendBadge;
    Node *_otherBadge;
    
};

#endif /* defined(__Syanago__FooterLayer__) */
