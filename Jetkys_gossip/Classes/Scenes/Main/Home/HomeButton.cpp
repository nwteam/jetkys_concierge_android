#include "HomeButton.h"
#include "SoundHelper.h"

HomeButton::HomeButton() {}

HomeButton* HomeButton::create()
{
    HomeButton* btn = new (std::nothrow) HomeButton();
    if (btn && btn->init("footer_home_button_base.png")) {
        btn->initialize();
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void HomeButton::initialize()
{
    showButtonImage();
}

void HomeButton::showButtonImage()
{
    auto buttonImage = Sprite::create("menu_home.png");
    buttonImage->setTag(TAG_SPRITE::BUTTON);
    buttonImage->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    addChild(buttonImage, Z_ORDER::Z_BUTTON);
}


void HomeButton::startHomeButtonAnimation()
{
    getChildByTag(TAG_SPRITE::BUTTON)->setVisible(false);

    // CCAnimationの初期化
    auto sdcharacterAnimation = Animation::create();
    auto sdcharacterCommentAnimation = Animation::create();

    // 画像を配列に代入
    for (int i = 1; i < 4; i++) {
        sdcharacterAnimation->addSpriteFrameWithFile(StringUtils::format("home_sd_%d.png", i));
        sdcharacterCommentAnimation->addSpriteFrameWithFile(StringUtils::format("home_sd_coment_%d.png", i));
    }
    sdcharacterAnimation->setDelayPerUnit(0.1f); // アニメの動く時間を設定
    sdcharacterCommentAnimation->setDelayPerUnit(0.1f);
    sdcharacterAnimation->setLoops(5);

    auto sdSprite = Sprite::create("menu_home.png");
    sdSprite->setTag(TAG_SPRITE::HOME_BODY);
    sdSprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    sdSprite->runAction(Sequence::create(Animate::create(sdcharacterAnimation),
                                         CallFunc::create(CC_CALLBACK_0(HomeButton::finishSdAnimation, this)),
                                         NULL));
    addChild(sdSprite, Z_HOME_BODY);

    auto sdCommentSprite = Sprite::create();
    sdCommentSprite->setTag(TAG_SPRITE::HOME_COMMENT);
    sdCommentSprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    sdCommentSprite->runAction(Sequence::create(CCDelayTime::create(1),
                                                Animate::create(sdcharacterCommentAnimation),
                                                NULL));
    addChild(sdCommentSprite, Z_HOME_COMMENT);
}

void HomeButton::finishSdAnimation()
{
    removeChildByTag(TAG_SPRITE::HOME_BODY);
    removeChildByTag(TAG_SPRITE::HOME_COMMENT);
    getChildByTag(TAG_SPRITE::BUTTON)->setVisible(true);
}