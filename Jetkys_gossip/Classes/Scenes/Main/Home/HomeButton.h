#ifndef __syanago__HomeButton__
#define __syanago__HomeButton__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class HomeButton : public ui::Button
{
public:
    HomeButton();
    static HomeButton* create();
    void initialize();
    void startHomeButtonAnimation();
    enum TAG_SPRITE{
        BUTTON = 0,
        HOME_BODY,
        HOME_COMMENT
    };
private:
    
    enum Z_ORDER{
        Z_BUTTON = 0,
        Z_HOME_BODY,
        Z_HOME_COMMENT,
    };
    void showButtonImage();
    void finishSdAnimation();
    
};

#endif
