#ifndef __syanago__HomeLayer__
#define __syanago__HomeLayer__

#include "cocos2d.h"
#include "DialogView.h"
#include "Loading.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class MainScene;

class HomeLayer : public Layer, public DialogDelegate {
public:
    HomeLayer();
    virtual ~HomeLayer();
    CREATE_FUNC(HomeLayer);
    bool init();
    void show();
    
    void updateCharacterImg();
    
    MainScene *_mainScene;
    
    void showFairy();
    
private:
    static const std::string DEFAULT_VOICE_TEXT;
    enum TAG_SPRITE {
        DISABLE_EVENT = 0,
        VOICE_BALLOON,
        MISSION
    };
    
    void onEndBlackOut();
    
    void createTouchEvent();
    bool onTouchBegan(cocos2d::Touch* touch,cocos2d::Event* event);
    
    void showBackground();
    void showCharacter();
    void showLeaderCharacter();
    void showLeaderCharacterVoiceBalloon();
    void showLeaderCharacterVoiceBalloonScroll();
    void showLeaderCharacterVoiceBalloonLabel();
    void showVoiceMessage();
    void showMissionButton();
    void fairyMoveFinished();
    std::string getWelcomeVoiceText();
    std::string getTapVoiceText();
    
    int _leaderCharacrterId;
    
    Sprite * characterSprite;
    Sprite* _fairy;
    int _oldRank;
    int _flyPattern;
    bool _isFairyMoving;
    bool _touchFlag;
    bool _initFailyFlag;
    bool _charaTouchEnable;
    Label* _mainTextLabel;
    cocos2d::extension::ScrollView* _mainTextScrollView;
};

#endif /* defined(__syanago__HomeLayer__) */
