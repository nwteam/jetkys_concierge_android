#include "HomeLayer.h"
#include "PlayerController.h"
#include "jCommon.h"
#include "MainScene.h"
#include "GameDefines.h"
#include "SelectFirstCarScene.h"
#include "UserDefaultManager.h"
#include "MeasurementInformation.h"
#include "MissionButton.h"
#include "HeaderStatus.h"
#include "WordsModel.h"

const std::string HomeLayer::DEFAULT_VOICE_TEXT = "親愛度のボイスデータがここに表示されるよ!";

HomeLayer::HomeLayer():
    _charaTouchEnable(false)
    , _leaderCharacrterId(0)
    , _isFairyMoving(false)
    , _flyPattern(0)
    , _initFailyFlag(false)
    , _fairy(nullptr) {}

HomeLayer::~HomeLayer()
{
    if (_leaderCharacrterId > 0) {
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile(StringUtils::format("%d_ca.plist", _leaderCharacrterId).c_str());
    }
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("home_background.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("home_avatar.plist");
}

bool HomeLayer::init()
{
    if (!Layer::init()) {
        return false;
    }
    setTag(MI_HOME);
    _leaderCharacrterId = UserDefault::getInstance()->getIntegerForKey("LeaderCharaMst");
    return true;
}

void HomeLayer::show()
{
    setVisible(true);

    showBackground();
    showCharacter();
    showFairy();
    showMissionButton();

    createTouchEvent();
}

void HomeLayer::showBackground()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("home_background.plist");
    Sprite* background = makeSprite("home_background.png");
    background->setAnchorPoint(Vec2(0.5f, 1.0f));
    background->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2, Director::getInstance()->getWinSize().height - HEADER_STATUS::SIZE::HEIGHT));
    addChild(background, -1);
}

void HomeLayer::showCharacter()
{
    showLeaderCharacter();
    showLeaderCharacterVoiceBalloon();
    showLeaderCharacterVoiceBalloonScroll();
    showLeaderCharacterVoiceBalloonLabel();
    runAction(Sequence::create(DelayTime::create(1.0), CCCallFunc::create(CC_CALLBACK_0(HomeLayer::showVoiceMessage, this)), NULL));
}

void HomeLayer::showLeaderCharacter()
{
    Size winSize = Director::getInstance()->getWinSize();
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_ca.plist", _leaderCharacrterId).c_str());
    characterSprite = makeSprite(StringUtils::format("%d_t.png", _leaderCharacrterId).c_str());
    characterSprite->setAnchorPoint(Vec2(0.5f, 1.0f));
    characterSprite->setPosition(Vec2(winSize.width / 2, winSize.height - 135.0f));
    characterSprite->setScale(1.2f);
    characterSprite->runAction(RepeatForever::create(Sequence::create(
                                                         MoveTo::create(10, Vec2(winSize.width / 2, winSize.height - 155.0f)),
                                                         MoveTo::create(10, Vec2(winSize.width / 2, winSize.height - 135.0f)),
                                                         NULL)));
    addChild(characterSprite, 1);
}

void HomeLayer::showLeaderCharacterVoiceBalloon()
{
    Sprite* balloon = makeSprite("gacha_comment_base.png");
    balloon->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2, 320));
    balloon->setOpacity(0);
    balloon->setTag(TAG_SPRITE::VOICE_BALLOON);
    addChild(balloon, 2);
}

void HomeLayer::showLeaderCharacterVoiceBalloonScroll()
{
    _mainTextScrollView = ScrollView::create();
    _mainTextScrollView->setDirection(ScrollView::Direction::VERTICAL);
    _mainTextScrollView->setAnchorPoint(Point(0, 0));
    _mainTextScrollView->setPosition(Point(0, 8));
    _mainTextScrollView->setBounceable(false);
    Node* balloon = getChildByTag(TAG_SPRITE::VOICE_BALLOON);
    _mainTextScrollView->setViewSize(Size(balloon->getContentSize().width - 26, 130));
    balloon->addChild(_mainTextScrollView, 1);
}

void HomeLayer::showLeaderCharacterVoiceBalloonLabel()
{
    _mainTextLabel = Label::createWithTTF(HomeLayer::DEFAULT_VOICE_TEXT, FONT_NAME_4, 24);
    _mainTextLabel->setDimensions(_mainTextScrollView->getViewSize().width - 26, 0);
    _mainTextLabel->setHorizontalAlignment(TextHAlignment::LEFT);
    _mainTextLabel->setOpacity(0);
    _mainTextLabel->setString(removeSpaces(getWelcomeVoiceText()));
    _mainTextScrollView->setContainer(_mainTextLabel);
    _mainTextScrollView->setContentOffset(Point(0 - (_mainTextLabel->getContentSize().width - _mainTextScrollView->getViewSize().width),
                                                0 - (_mainTextLabel->getContentSize().height - _mainTextScrollView->getViewSize().height)));
}

void HomeLayer::updateCharacterImg()
{
    if (_leaderCharacrterId > 0) {
        SpriteFrameCache::getInstance()->removeSpriteFramesFromFile(StringUtils::format("%d_ca.plist", _leaderCharacrterId).c_str());

        int leaderCharacterID = (PLAYERCONTROLLER->_playerTeams)->getLearderCharacterId();
        _leaderCharacrterId = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[leaderCharacterID]->getCharactersId();
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_ca.plist", _leaderCharacrterId).c_str());

        characterSprite->setSpriteFrame(StringUtils::format("%d_t.png", _leaderCharacrterId).c_str());
    }
}



bool HomeLayer::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (_mainScene->getMainIndex() == MI_HOME) {
        if (_charaTouchEnable == true) {
            _mainTextLabel->setString(removeSpaces(getTapVoiceText()));
            _mainTextScrollView->setContentOffset(Point(0 - (_mainTextLabel->getContentSize().width - _mainTextScrollView->getViewSize().width),
                                                        0 - (_mainTextLabel->getContentSize().height - _mainTextScrollView->getViewSize().height)));
            _charaTouchEnable = false;
            Size winSize = Director::getInstance()->getWinSize();
            characterSprite->stopAllActions();
            auto moveUp = Sequence::create(MoveBy::create(0.2f, Vec2(0, -5.0f)),
                                           MoveBy::create(0.1f, Vec2(0, 10.0f)),
                                           MoveBy::create(0.2f, Vec2(0, 10.0f)),
                                           MoveBy::create(0.3f, Vec2(0, 15.0f)),
                                           NULL);
            auto moveDown = Sequence::create(MoveBy::create(0.3f, Vec2(0, -15.0f)),
                                             MoveBy::create(0.2f, Vec2(0, -10.0f)),
                                             MoveBy::create(0.1f, Vec2(0, -10.0f)),
                                             MoveBy::create(0.1f, Vec2(0, 5.0f)),
                                             NULL);

            characterSprite->runAction(Sequence::create(moveUp,
                                                        DelayTime::create(0.1f),
                                                        moveDown,
                                                        RepeatForever::create(Sequence::create(MoveTo::create(10, Vec2(winSize.width / 2, winSize.height - 155.0f)),
                                                                                               MoveTo::create(10, Vec2(winSize.width / 2, winSize.height - 135.0f)),
                                                                                               NULL)),
                                                        NULL));
            HomeLayer::showVoiceMessage();
        }
    }

    return false;
}

void HomeLayer::showVoiceMessage()
{
    if (_mainScene->getMainIndex() == MI_HOME) {
        _mainTextLabel->runAction(Sequence::create(FadeIn::create(0.2f),
                                                   DelayTime::create(4.0f),
                                                   FadeOut::create(0.2f),
                                                   NULL));
        getChildByTag(TAG_SPRITE::VOICE_BALLOON)->runAction(Sequence::create(
                                                                FadeIn::create(0.2f),
                                                                DelayTime::create(4.0f),
                                                                FadeOut::create(0.2f),
                                                                CallFunc::create([&]() {_charaTouchEnable = true; }),
                                                                NULL));
    } else {
        _charaTouchEnable = true;
    }
}

std::string HomeLayer::getWelcomeVoiceText()
{
    float dearValue = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]->getDearValue();
    std::vector<std::string>words;
    std::vector<int>wordId;
    words.clear();
    wordId.clear();
    for (int i = 1; i < 5; i++) {
        std::shared_ptr<WordsModel>wordsModel(WordsModel::find(_leaderCharacrterId, 1, 1, i));
        if (dearValue >= wordsModel->getDearValue()) {
            if (wordsModel->getDearValue() != 0) {
                words.push_back(wordsModel->getWords());
                wordId.push_back(wordsModel->getWordId());
            }
        }
    }
    int count = (int)words.size();
    srand((unsigned int)time(NULL));
    int randNum = (rand() % count);
    CallFunc* callfunc1 = CallFunc::create([&, wordId, randNum]() {
        SoundHelper::shareHelper()->playCharacterVoiceEfect(_leaderCharacrterId, wordId.at(randNum));
    });
    runAction(Sequence::create(DelayTime::create(1.0),
                               callfunc1,
                               NULL));
    return words.at(randNum);
}

std::string HomeLayer::getTapVoiceText()
{
    int leaderCharacterID = (PLAYERCONTROLLER->_playerTeams)->getLearderCharacterId();
    float dearValue = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[leaderCharacterID]->getDearValue();
    int mstCharacterId = PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[leaderCharacterID]->getCharactersId();
    std::vector<std::string>words;
    std::vector<int>wordId;
    words.clear();
    wordId.clear();
    for (int i = 5; i < 11; i++) {
        std::shared_ptr<WordsModel>wordsModel(WordsModel::find(_leaderCharacrterId, 1, 2, i));
        if (dearValue >= wordsModel->getDearValue()) {
            if (wordsModel->getDearValue() != 0) {
                words.push_back(wordsModel->getWords());
                wordId.push_back(wordsModel->getWordId());
                ////CCLOG("キャラid:%d台詞:%s",_leaderCharacrterId,MASTER_CONTROLLER->_words->getWords().c_str());
            }
        }
    }
    int count = (int)words.size();
    srand((unsigned int)time(NULL));
    int randNum = (rand() % count);

    SoundHelper::shareHelper()->playCharacterVoiceEfect(mstCharacterId, wordId.at(randNum));
    return words.at(randNum);
}

void HomeLayer::createTouchEvent()
{
    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(HomeLayer::onTouchBegan, this);

    auto eventDispatcher = Director::getInstance()->getEventDispatcher();
    eventDispatcher->removeEventListenersForTarget(this);
    eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void HomeLayer::showMissionButton()
{
    if (PLAYERCONTROLLER->getActiveMissionCount() > 0) {
        auto button = MissionButton::create(CC_CALLBACK_0(MainScene::showMissionDialog, _mainScene));
        button->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
        Size winSize = Director::getInstance()->getWinSize();
        button->setPosition(Vec2(10, winSize.height - HEADER_STATUS::SIZE::HEIGHT - 10.0f));
        button->setTag(TAG_SPRITE::MISSION);
        addChild(button, 2);
    }
}


void HomeLayer::showFairy()
{
    Size winSize = Director::getInstance()->getWinSize();
    // add fairy
    if (_fairy == nullptr) {
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("home_avatar.plist");
        _fairy = makeSprite("31_avatar.png");
        _fairy->setPosition(winSize.width - _fairy->getContentSize().width / 2,
                            winSize.height - HEADER_STATUS::SIZE::HEIGHT - _fairy->getContentSize().height / 2);
        addChild(_fairy, 2);
    } else {
        _fairy->stopAllActions();
    }

    if (!PLAYERCONTROLLER->hasFairy()) {
        _fairy->setVisible(false);
        return;
    }

    const Vec2 position = Vec2(winSize.width - _fairy->getContentSize().width / 2,
                               winSize.height - HEADER_STATUS::SIZE::HEIGHT - _fairy->getContentSize().height / 2 - 30);
    _fairy->setVisible(true);
    _fairy->runAction(RepeatForever::create(Sequence::create(
                                                EaseBackInOut::create(MoveTo::create(3, position)),
                                                EaseBackInOut::create(MoveTo::create(3, position - Vec2(0, 90))),                                                                      NULL)));
}














