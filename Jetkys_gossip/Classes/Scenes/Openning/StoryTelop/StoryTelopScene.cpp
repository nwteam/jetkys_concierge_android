#include "StoryTelopScene.h"
#include "TitleScene.h"
#include "SoundHelper.h"

StoryTelopScene::StoryTelopScene():
    _movingToTitleScene(false) {}

Scene* StoryTelopScene::createScene()
{
    auto scene = Scene::create();
    scene->addChild(StoryTelopScene::create());
    return scene;
}

bool StoryTelopScene::init()
{
    if (!Layer::init()) {
        return false;
    }

    showBackgroundImage();
    _telopLayer = StoryTelopLayer::create();
    addChild(_telopLayer);

    scheduleUpdate();

    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(StoryTelopScene::cancel, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}

void StoryTelopScene::showBackgroundImage()
{
    Size winSize = Director::getInstance()->getWinSize();
    Sprite* openingBackgroundImage = Sprite::create("garege_background.png");
    openingBackgroundImage->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(openingBackgroundImage, -1);

    LayerColor* filter = LayerColor::create(Color4B(Color3B::BLACK));
    filter->setOpacity(180);
    addChild(filter);
}

void StoryTelopScene::update(float delta)
{
    if (_telopLayer->scrollUpBy(delta) == false) {
        goToTitleScene();
    }
}

bool StoryTelopScene::cancel(cocos2d::Touch* touch, cocos2d::Event* event)
{
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    goToTitleScene();
    return true;
}

void StoryTelopScene::goToTitleScene()
{
    if (_movingToTitleScene == false) {
        _movingToTitleScene = true;
        Director::getInstance()->getTextureCache()->removeAllTextures();
        Director::getInstance()->replaceScene(TransitionFade::create(1.0f, TitleScene::createScene(), Color3B::BLACK));
    }
}








