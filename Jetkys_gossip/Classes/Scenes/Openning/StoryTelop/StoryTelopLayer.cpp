#include "StoryTelopLayer.h"
#include "StoryTelopScene.h"
#include "FontDefines.h"

bool StoryTelopLayer::init()
{
    if (!Layer::init()) {
        return false;
    }

    Size winSize = Director::getInstance()->getWinSize();
    setContentSize(winSize);
    setPositionY(-winSize.height);

    Label* telop;

    telop = createTelop("20XX年、", winSize.height, false);
    addChild(telop);
    telop = createTelop("全国各地で【車なご】と呼ばれる少女たちが", telop->getPosition().y, false);
    addChild(telop);
    telop = createTelop("駆(はし)る風景が日常になりつつある世界。", telop->getPosition().y, false);
    addChild(telop);

    telop = createTelop("「あらゆる願いが叶う」と噂される", telop->getPosition().y, true);
    addChild(telop);
    telop = createTelop("【日本縦断ラリー】の話題が世間を賑わせていた。", telop->getPosition().y, false);
    addChild(telop);

    telop = createTelop("そんな中、貴方はふとしたキッカケで出会った", telop->getPosition().y, true);
    addChild(telop);
    telop = createTelop("【車なご】の願いをかなえるべく、", telop->getPosition().y, false);
    addChild(telop);
    telop = createTelop("日本縦断ラリーへの参加を決意し、", telop->getPosition().y, false);
    addChild(telop);
    telop = createTelop("トップチームを目指すのだった。", telop->getPosition().y, false);
    addChild(telop);

    return true;
}


Label* StoryTelopLayer::createTelop(std::string contents, int lastPosition, bool isParagraph)
{
    const int TELOP_FONT_SIZE = 26;
    const int TELOP_LINE_HEIGHT = 46;
    const int TELOP_LINE_HEIGHT_PARAGRAPH = 146;

    int lineHeight = (isParagraph) ? TELOP_LINE_HEIGHT_PARAGRAPH : TELOP_LINE_HEIGHT;

    Label* result = Label::createWithTTF(contents, FONT_NAME_2, TELOP_FONT_SIZE);
    result->setPosition(Point(Director::getInstance()->getWinSize().width / 2, lastPosition - lineHeight));
    return result;
}

bool StoryTelopLayer::scrollUpBy(float delta)
{
    if (Director::getInstance()->getWinSize().height - 500 < getPositionY()) {
        return false;
    }
    Vec2 position = Vec2(getPositionX(), getPositionY() + 50 * delta);
    setPosition(position);
    return true;
}





















