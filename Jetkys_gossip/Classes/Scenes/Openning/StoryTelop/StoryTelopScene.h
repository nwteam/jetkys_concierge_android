#ifndef __Syanago__StoryTelopScene__
#define __Syanago__StoryTelopScene__

#include "cocos2d.h"
#include "StoryTelopLayer.h"

USING_NS_CC;

class StoryTelopScene : public cocos2d::Layer
{
public:
    StoryTelopScene();
    static cocos2d::Scene* createScene();
    CREATE_FUNC(StoryTelopScene);
    virtual bool init();
    
private:
    void showBackgroundImage();    
    
    
    void update(float delta);
    void goToTitleScene();
    bool cancel(cocos2d::Touch* touch,cocos2d::Event* event);
    
    StoryTelopLayer* _telopLayer;
    bool _movingToTitleScene;
    
    
};
#endif /* defined(__Syanago__StoryTelopScene__) */
