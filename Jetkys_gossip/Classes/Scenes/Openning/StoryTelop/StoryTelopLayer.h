#ifndef __syanago__StoryTelopLayer__
#define __syanago__StoryTelopLayer__

#include "cocos2d.h"

USING_NS_CC;
using namespace std;

class StoryTelopLayer: public Layer
{
public:
    CREATE_FUNC(StoryTelopLayer);
    virtual bool init();

    bool scrollUpBy(float delta);

private:
    Label* createTelop(std::string contents, int lastPosition, bool isParagraph);
};

#endif /* defined(__syanago__StoryTelopLayer__) */
