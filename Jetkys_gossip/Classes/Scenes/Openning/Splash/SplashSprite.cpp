#include "SplashSprite.h"
#include "SplashScene.h"
const std::string SplashSprite::SPRITE_FILE_NAME = "Ac1_Logo.png";

SplashSprite* SplashSprite::create()
{
    SplashSprite* sprite = new SplashSprite();
    if (sprite && sprite->initWithFile(SPRITE_FILE_NAME)) {
        Size winSize = Director::getInstance()->getWinSize();
        sprite->setPosition(Point(winSize.width / 2, winSize.height / 2));
        sprite->setScale(0.8);
        sprite->setTag(SplashScene::TAG_SPRITE::LOGO);
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

SplashSprite::~SplashSprite()
{
    Director::getInstance()->getTextureCache()->removeTextureForKey(SPRITE_FILE_NAME);
}
