#ifndef __syanago__SplashScene__
#define __syanago__SplashScene__

#include "cocos2d.h"

USING_NS_CC;

class SplashScene : public cocos2d::LayerColor
{
public:
    enum TAG_SPRITE {
        LOGO = 0,
        ATTENTION
    };
    
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(SplashScene);
    
private:
    void playTitleVoiceCall(float milliSecound);
    void showSplash();
    void showAttention();
    
    bool cancelSplash(cocos2d::Touch* touch,cocos2d::Event* event);
    bool cancelAttension(cocos2d::Touch* touch,cocos2d::Event* event);
    
    void goToStoryTelopScene(float milliSecound);
};

#endif /* defined(__syanago__SplashScene__) */
