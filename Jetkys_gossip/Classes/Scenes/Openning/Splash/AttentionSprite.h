#ifndef __syanago__AttentionSprite__
#define __syanago__AttentionSprite__

#include "cocos2d.h"

USING_NS_CC;

class AttentionSprite : public Sprite {
public:
    const static std::string SPRITE_FILE_NAME;
    ~AttentionSprite();
    static AttentionSprite* create();
};

#endif /* defined(__syanago__AttentionSprite__) */
