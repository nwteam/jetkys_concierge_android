#include "AttentionSprite.h"
#include "SplashScene.h"

const std::string AttentionSprite::SPRITE_FILE_NAME = "attension.png";

AttentionSprite* AttentionSprite::create()
{
    AttentionSprite* sprite = new AttentionSprite();
    if (sprite && sprite->initWithFile(SPRITE_FILE_NAME)) {
        Size winSize = Director::getInstance()->getWinSize();
        sprite->setPosition(Point(winSize.width / 2, winSize.height / 2));
        sprite->setOpacity(0);
        sprite->setScale(1);
        sprite->setTag(SplashScene::TAG_SPRITE::ATTENTION);
        sprite->runAction(Sequence::create(FadeIn::create(0.5),
                                           DelayTime::create(1.5),
                                           FadeOut::create(0.5),
                                           NULL));
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

AttentionSprite::~AttentionSprite()
{
    Director::getInstance()->getTextureCache()->removeTextureForKey(SPRITE_FILE_NAME);
}
