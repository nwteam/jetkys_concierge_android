#include "SplashScene.h"
#include "StoryTelopScene.h"
#include "UserDefaultManager.h"
#include "SplashSprite.h"
#include "AttentionSprite.h"
#include "SoundHelper.h"

Scene* SplashScene::createScene()
{
    auto scene = Scene::create();
    scene->addChild(SplashScene::create());
    return scene;
}

bool SplashScene::init()
{
    if (!LayerColor::initWithColor(Color4B(255, 255, 255, 255))) {
        return false;
    }
    UserDefaultManager::initSplashScene();
    scheduleOnce(schedule_selector(SplashScene::playTitleVoiceCall), 0.5f);
    showSplash();
    return true;
}

void SplashScene::playTitleVoiceCall(float milliSecound)
{
    SOUND_HELPER->playSplashSceneVoice();
}

void SplashScene::showSplash()
{
    SplashSprite* splashSprite = SplashSprite::create();
    splashSprite->runAction(Sequence::create(DelayTime::create(1.5),
                                             FadeOut::create(0.5),
                                             CallFunc::create([this]() {
        _eventDispatcher->removeAllEventListeners();
        showAttention();
    }), NULL));
    addChild(splashSprite, 1);

    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(SplashScene::cancelSplash, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

bool SplashScene::cancelSplash(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (getChildByTag(TAG_SPRITE::LOGO) ==  NULL) {
        return false;
    }
    unschedule(schedule_selector(SplashScene::playTitleVoiceCall));
    getChildByTag(TAG_SPRITE::LOGO)->stopAllActions();
    showAttention();
    return true;
}

void SplashScene::showAttention()
{
    getChildByTag(TAG_SPRITE::LOGO)->removeFromParent();
    addChild(AttentionSprite::create(), 1);
    scheduleOnce(schedule_selector(SplashScene::goToStoryTelopScene), 2.0f);

    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(SplashScene::cancelAttension, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

bool SplashScene::cancelAttension(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (getChildByTag(TAG_SPRITE::ATTENTION) ==  NULL) {
        return false;
    }
    unschedule(schedule_selector(SplashScene::goToStoryTelopScene));
    getChildByTag(TAG_SPRITE::ATTENTION)->removeFromParent();
    goToStoryTelopScene(0);
    return true;
}

void SplashScene::goToStoryTelopScene(float milliSecound)
{
    Director::getInstance()->replaceScene(TransitionFade::create(0.5f,
                                                                 StoryTelopScene::createScene(),
                                                                 Color3B::WHITE));
}
