#ifndef __syanago__SplashSprite__
#define __syanago__SplashSprite__

#include "cocos2d.h"

USING_NS_CC;

class SplashSprite : public Sprite {
public:
    const static std::string SPRITE_FILE_NAME;
    ~SplashSprite();
    static SplashSprite* create();
};

#endif /* defined(__syanago__SplashSprite__) */
