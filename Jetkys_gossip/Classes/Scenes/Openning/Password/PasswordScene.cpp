#include "PasswordScene.h"
#include "FontDefines.h"
#include "jCommon.h"
#include "PlayerController.h"
#include "TitleScene.h"
#include "SoundHelper.h"
#include "DataPlayer.h"
#include "LoadingScene.h"
#include "ResouseDeleteModel.h"
#include "UserDefaultManager.h"
#include "ResourceDeleteButton.h"
#include "SimpleDialog.h"

USING_NS_CC;
USING_NS_CC_EXT;

Scene* PasswordScene::createScene()
{
    auto scene = Scene::create();
    scene->addChild(PasswordScene::create());
    return scene;
}

bool PasswordScene::init()
{
    if (!Layer::init()) {
        return false;
    }

    progress = new DefaultProgress();
    request = new RequestAPI(progress);

    showBackground();
    showHeadLine(this, "引き継ぎコード入力", CC_CALLBACK_0(PasswordScene::backToTileScene, this));
    showInputPasswordDialog();
    showDidLabel();
    showResourceDeleteButton();
    addChild(MenuTouch::create(createOkButton(),  NULL));
    ;
    return true;
}

void PasswordScene::showBackground()
{
    auto backgroundImage = Sprite::create("home_bg.png");
    backgroundImage->setPosition(Director::getInstance()->getWinSize() / 2);
    addChild(backgroundImage, -1);
}

void PasswordScene::showInputPasswordDialog()
{
    const Size winSize = Director::getInstance()->getWinSize();
    auto inputBaseImage = makeSprite("friend_idserch_base.png");
    inputBaseImage->setTag(TAG::INPUT_BASE);
    inputBaseImage->setPosition(Point(winSize.width / 2, winSize.height / 2));
    addChild(inputBaseImage);

    auto inputLabel = Label::createWithTTF("引き継ぎコードを入力してください", FONT_NAME_2, 28);
    inputLabel->setPosition(Vec2(winSize.width / 2,
                                 winSize.height / 2 + inputBaseImage->getContentSize().height / 2 - inputLabel->getContentSize().height / 2 - 40));
    inputLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(inputLabel);

    auto editBox = cocos2d::ui::EditBox::create(Size(405, 69), Scale9Sprite::create("friend_input_base.png"));
    editBox->setTag(TAG::EDIT_BOX);
    editBox->setPosition(Vec2(winSize.width / 2, winSize.height / 2 + 20));
    editBox->setPlaceHolder("");
    editBox->setMaxLength(10);
    editBox->setInputFlag(EditBox::InputFlag::SENSITIVE);
    editBox->setInputMode(EditBox::InputMode::SINGLE_LINE);
    editBox->setFontColor(Color3B::BLACK);
    editBox->setReturnType(EditBox::KeyboardReturnType::DONE);
    addChild(editBox);
}

void PasswordScene::showDidLabel()
{
    const std::string didData = (!DataPlayer::getInstance()->isFirstInstall()) ? removeSpaces(DataPlayer::getInstance()->getDID()) : "NoData";
    auto didLabel = LabelTTF::create("識別ID:" + didData, FONT_NAME_4, 19);
    const int LABEL_OFFSET = 10;
    const int POSITION_X = Director::getInstance()->getWinSize().width / 2;
    const int POSITION_Y = (Director::getInstance()->getWinSize().height
                            - getChildByTag(TAG::INPUT_BASE)->getContentSize().height
                            - didLabel->getContentSize().height) / 2 - LABEL_OFFSET;
    didLabel->setPosition(Point(POSITION_X, POSITION_Y));
    didLabel->setAnchorPoint(Vec2(0.5, 0.5));
    didLabel->setColor(Color3B::BLACK);
    addChild(didLabel, 1);
}

void PasswordScene::showResourceDeleteButton()
{
    auto button = ResourceDeleteButton::create();
    button->setPosition(Point(30 + button->getContentSize().width / 2,
                              30 + button->getContentSize().height / 2));
    button->setTag(BUTTON_TAG::RESOURCES_DELETE);
    addChild(button);
}

MenuItemSprite* PasswordScene::createOkButton()
{
    auto result = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_0(PasswordScene::onInputPassword, this));
    result->setPosition(Point(0, -113.5));
    result->setTag(BUTTON_TAG::OK_BUTTON);

    addDisableEditBoxEvent(result);

    auto oklabel = Label::createWithTTF("OK", FONT_NAME_2, 28);
    oklabel->setPosition(Point(result->getContentSize().width / 2, result->getContentSize().height / 2 - 14));
    oklabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->addChild(oklabel);
    return result;
}


void PasswordScene::addDisableEditBoxEvent(MenuItemSprite* button)
{
    auto lister = EventListenerTouchOneByOne::create();
    lister->onTouchBegan = [&](Touch* touch, Event* event) {
                               auto target = static_cast<Sprite*>(event->getCurrentTarget());
                               Rect rect = Rect(0, 0, target->getContentSize().width, target->getContentSize().height);
                               if (rect.containsPoint(target->convertToNodeSpace(touch->getLocation()))) {
                                   ((cocos2d::ui::EditBox*) getChildByTag(TAG::EDIT_BOX))->setEnabled(false);
                                   return true;
                               }
                               return false;
                           };
    lister->onTouchEnded = [&](Touch* touch, Event* event) {
                               ((cocos2d::ui::EditBox*)getChildByTag(TAG::EDIT_BOX))->setEnabled(true);
                           };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(lister, button);
}

void PasswordScene::backToTileScene()
{
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    transitScene(TitleScene::createScene());
}

void PasswordScene::onInputPassword()
{
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    std::string transferCode = ((cocos2d::ui::EditBox*)getChildByTag(TAG::EDIT_BOX))->getText();
    if (transferCode.size() < 1) {
        addChild(SimpleDialog::create("コードを入力してください", "コードが記入されてません"), DIALOG_ZORDER);
        return;
    }
    progress->onStart();
    request->transferPlayer(transferCode, CC_CALLBACK_1(PasswordScene::onResponseTransferPlayer, this));
}

void PasswordScene::onResponseTransferPlayer(Json* response)
{
    UserDefault::getInstance()->setBoolForKey(KEY_FIRST_CREATE_PLAYER, true);
    PLAYERCONTROLLER->saveNewPlayer(Json_getItem(response, "create_player"));
    Director::getInstance()->replaceScene(LoadingScene::createScene(LoadingScene::SEQUENCE::SYNC_PLAYER_DATA));
}




