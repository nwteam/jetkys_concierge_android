#ifndef __Syanago__PasswordScene__
#define __Syanago__PasswordScene__

#include "cocos2d.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"
#include "show_head_line.h"

USING_NS_CC;
using namespace SyanagoAPI;

class PasswordScene: public Layer, public show_head_line {
public:
    static Scene* createScene();
    bool init();
    CREATE_FUNC(PasswordScene);

private:
    RequestAPI* request;
    DefaultProgress* progress;

    enum TAG {
        EDIT_BOX = 100,
        INPUT_BASE
    };
    enum BUTTON_TAG {
        OK_BUTTON = 0,
        BACK_BUTTON,
        RESOURCES_DELETE,
    };

    void showBackground();
    void showInputPasswordDialog();
    void showDidLabel();
    void showResourceDeleteButton();
    MenuItemSprite* createOkButton();


    void goToMainScene();
    void backToTileScene();

    void addDisableEditBoxEvent(MenuItemSprite* button);
    void onInputPassword();

    void onResponseTransferPlayer(Json* response);
};
#endif /* defined(__Syanago__PasswordScene__) */
