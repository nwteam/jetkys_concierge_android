#ifndef __syanago__ResourceDeleteButton__
#define __syanago__ResourceDeleteButton__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "DialogView.h"

USING_NS_CC;

class ResourceDeleteButton : public ui::Button, public DialogDelegate
{
public:
    typedef std::function<void()> OnResponceCallback;
    ResourceDeleteButton();
    static ResourceDeleteButton* create();
    void initialize();
    void onTap(Ref *sender, Widget::TouchEventType type);
    void btDialogCallback(ButtonIndex aIndex, int tag);
private:
    bool _touched;
};



#endif /* defined(__syanago__ResourceDeleteButton__) */
