#include "ResourceDeleteButton.h"
#include "UserDefaultManager.h"
#include "ResouseDeleteModel.h"

ResourceDeleteButton::ResourceDeleteButton():
    _touched(false) {}

ResourceDeleteButton* ResourceDeleteButton::create()
{
    ResourceDeleteButton* btn = new (std::nothrow)ResourceDeleteButton();
    if (btn && btn->init("cash_clear_button.png")) {
        btn->initialize();
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void ResourceDeleteButton::initialize()
{
    addTouchEventListener(CC_CALLBACK_2(ResourceDeleteButton::onTap, this));
}

void ResourceDeleteButton::onTap(Ref* sender, Widget::TouchEventType type)
{
    if (type == TouchEventType::ENDED && !_touched) {
        _touched = false;
        auto dialog = DialogView::createWithShortMessage("キャッシュを消去しますか？", 2, 0);
        dialog->_delegate = this;
        Director::getInstance()->getRunningScene()->addChild(dialog, DIALOG_ZORDER);
    }
}

void ResourceDeleteButton::btDialogCallback(ButtonIndex aIndex, int tag)
{
    if (tag == 0) {
        if (aIndex == BT_YES) {
            auto deleteMethod = new ResouseDeleteModel();
            deleteMethod->StartDeleteResouses();
            #if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
                if (true) {
                    deleteMethod->StartDeleteOldResouses();
                    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::OLD_RESOURCES_DELETE, true);
                }
            #endif
        }
        return;
    }
}
