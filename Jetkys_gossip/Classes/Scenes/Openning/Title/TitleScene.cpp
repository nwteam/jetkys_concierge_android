#include "TitleScene.h"
#include "VisibleRect.h"
#include "RegisterScene.h"
#include "DataPlayer.h"
#include "SoundHelper.h"
#include "UserDefaultManager.h"
#include "LoadingScene.h"
#include "GoToPasswordSceneButton.h"
#include "Native.h"

Scene* TitleScene::createScene()
{
    auto scene = Scene::create();
    scene->addChild(TitleScene::create());
    return scene;
}

bool TitleScene::init()
{
    if (!Layer::init()) {
        return false;
    }
    _touchFlag = false;

    UserDefaultManager::initTitleScene();

    showTitleBackgroundImage();
    showTapToStartLabel();
    showApplicationVersion();
    addChild(GoToPasswordSceneButton::create());

    titleCall();

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(TitleScene::onTouchBegan, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    return true;
}

void TitleScene::showTitleBackgroundImage()
{
    auto sprite = Sprite::create("title_image.png");
    sprite->setPosition(Director::getInstance()->getWinSize() / 2);
    addChild(sprite, 0);
}

void TitleScene::showTapToStartLabel()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto sprite = Sprite::create("tap_to_start.png");
    sprite->setPosition(Point(winSize.width / 2, winSize.height / 2 - 310));
    addChild(sprite);
}

void TitleScene::showApplicationVersion()
{
    auto label = Label::createWithTTF("アプリバージョン:" + Native::getApplicationVersion(), FONT_NAME_2, 18);
    label->setPosition(Point(Director::getInstance()->getWinSize().width,
                             Point(VisibleRect::leftBottom() + Point(0, 46)).y));
    label->enableOutline(Color4B(Color3B::BLACK), 2);
    label->setAnchorPoint(Vec2(1.0, 0.5));
    addChild(label, 1);
}

void TitleScene::titleCall()
{
    runAction(Sequence::create(DelayTime::create(1),
                               CallFunc::create([]() {
        SoundHelper::shareHelper()->playTitleSceneVoice();
    }), NULL));
}

bool TitleScene::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (_touchFlag == false) {
        _touchFlag = true;
        SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        if (DataPlayer::getInstance()->isFirstInstall()) {
            transitScene(RegisterScene::createScene());
        } else {
            Director::getInstance()->replaceScene(LoadingScene::createScene(LoadingScene::SEQUENCE::INITALIZE));
        }
    }
    return true;
}


