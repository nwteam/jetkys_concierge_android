#ifndef __syanago__TitleScene__
#define __syanago__TitleScene__

#include "cocos2d.h"

USING_NS_CC;

class TitleScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    CREATE_FUNC(TitleScene);
    virtual bool init();
    
private:
    
    bool onTouchBegan(cocos2d::Touch* touch,cocos2d::Event* event);
    
    void showTitleBackgroundImage();
    void showApplicationVersion();
    void showTapToStartLabel();
    
    void titleCall();
    
    bool _touchFlag;
    
};

#endif /* defined(__syanago__TitleScene__) */
