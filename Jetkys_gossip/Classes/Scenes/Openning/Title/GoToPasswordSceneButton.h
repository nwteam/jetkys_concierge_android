#ifndef __syanago__GoToPasswordSceneButton__
#define __syanago__GoToPasswordSceneButton__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class GoToPasswordSceneButton : public ui::Button
{
public:
    typedef std::function<void()> OnResponceCallback;
    GoToPasswordSceneButton();
    static GoToPasswordSceneButton* create();
    void initialize();
    void onTap(Ref *sender, Widget::TouchEventType type);
private:
    bool _touched;
};


#endif /* defined(__syanago__GoToPasswordSceneButton__) */
