#include "GoToPasswordSceneButton.h"
#include "VisibleRect.h"
#include "SoundHelper.h"
#include "PasswordScene.h"
#include "jCommon.h"

GoToPasswordSceneButton::GoToPasswordSceneButton():
    _touched(false) {}

GoToPasswordSceneButton* GoToPasswordSceneButton::create()
{
    GoToPasswordSceneButton* btn = new (std::nothrow) GoToPasswordSceneButton();
    if (btn && btn->init("transfer_code_button.png")) {
        btn->initialize();
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void GoToPasswordSceneButton::initialize()
{
    setPosition(VisibleRect::leftBottom() + Point(getContentSize().width / 2 + 30,
                                                  getContentSize().height / 2 + 30));
    addTouchEventListener(CC_CALLBACK_2(GoToPasswordSceneButton::onTap, this));

}

void GoToPasswordSceneButton::onTap(Ref* sender, Widget::TouchEventType type)
{
    if (type == TouchEventType::ENDED && !_touched) {
        _touched = false;
        SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        transitScene(PasswordScene::createScene());
    }
}