#ifndef __Syanago__DownloadLayer__
#define __Syanago__DownloadLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "DialogView.h"
#include "create_func.h"
#include "RequestAPI.h"
#include "LoadingSceneProgress.h"


USING_NS_CC;
USING_NS_CC_EXT;
using namespace SyanagoAPI;

class DownloadDelegate {
public:
    DownloadDelegate() {};
    virtual ~DownloadDelegate(){};
    virtual void endDownload(){};
};

class DownloadLayer : public Layer, public create_func<DownloadLayer>, public AssetsManagerDelegateProtocol, public DialogDelegate{
public:
    DownloadLayer();
    virtual ~DownloadLayer();
    
    
    
    bool init(LoadingSceneProgress* progress, RequestAPI* request);
    using create_func::create;
    
    void initDownloadDir();
    
    void startDownload();
    
    void nextUpgrade();
    void upgrade(Ref *pSender) ;
    
    virtual void onError(cocos2d::extension::AssetsManager::ErrorCode errorCode);
    virtual void onProgress(int percent);
    virtual void onSuccess();
    virtual void btDialogCallback(ButtonIndex aIndex, int tag);
    
    void updateStatusInfo(int percent);
    
    void endDownloadCallback();
    
    EventListenerTouchOneByOne *_touchListener;
    DownloadDelegate *_delegate;
    std::vector<std::string> getResourcesOfNeedUpdate(Json* response);
    
private:
    cocos2d::extension::AssetsManager *getAssetManager();
    static std::string getResourceVersionKeyWithHash(const std::string& packageUrl);
    
    LoadingSceneProgress* progress;
    RequestAPI* request;
    
    std::string pathToSave;
    
    std::vector<std::string> downloadQueue;
    int _percentage;
    int _resouceCount;
};

#endif /* defined(__Syanago__DownloadLayer__) */
