#include "LoadingStatusViewer.h"
#include "LoadingScene.h"
#include "ui/CocosGUI.h"

const int LoadingStatusViewer::FONT_SIZE = 24;
const int LoadingStatusViewer::FONT_MARGIN = FONT_SIZE * 1.1;

LoadingStatusViewer::LoadingStatusViewer(Layer* layer):
    parent(layer)
{
    showStatusLabel();
    showLoadingBar();
}

void LoadingStatusViewer::showStatusLabel()
{
    auto container = parent->getChildByTag(LoadingScene::TAG_SPRITE::TIPS_CONTAINER);
    auto label = Label::createWithTTF("データの同期中...", FONT_NAME_2, FONT_SIZE);
    label->setTag(LoadingScene::TAG_SPRITE::STATUS_LABEL);
    label->setPosition(Vec2(container->getContentSize().width / 2, 48 - FONT_MARGIN));
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    label->setColor(Color3B::WHITE);
    container->addChild(label);
}

void LoadingStatusViewer::showLoadingBar()
{
    auto container = parent->getChildByTag(LoadingScene::TAG_SPRITE::TIPS_CONTAINER);
    const Vec2 POSITION_LOADING_BAR = Vec2(48, 96);
    Sprite* backGround = Sprite::create("loadbar_bg.png");
    backGround->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    backGround->setPosition(POSITION_LOADING_BAR);
    container->addChild(backGround);

    ui::LoadingBar* loadingBar = ui::LoadingBar::create("loadbar_on.png");
    loadingBar->setTag(LoadingScene::TAG_SPRITE::LOADING_BAR);
    loadingBar->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    loadingBar->setPosition(POSITION_LOADING_BAR);
    loadingBar->setPercent(0);
    container->addChild(loadingBar);

    CCLOG("loadingBar :%d", (int)loadingBar->getContentSize().width);

    auto percent = Label::createWithTTF("0%", FONT_NAME_2, FONT_SIZE);
    percent->setTag(LoadingScene::TAG_SPRITE::PROGRESS_LABEL);
    percent->setPosition(Vec2(238, 133 - FONT_MARGIN));
    percent->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    percent->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    percent->setColor(Color3B::WHITE);
    container->addChild(percent);
}

void LoadingStatusViewer::setLabel(std::string label)
{
    auto container = parent->getChildByTag(LoadingScene::TAG_SPRITE::TIPS_CONTAINER);
    static_cast<Label*>(container->getChildByTag(LoadingScene::TAG_SPRITE::STATUS_LABEL))->setString(label);
}


void LoadingStatusViewer::update(int progress)
{
    auto container = parent->getChildByTag(LoadingScene::TAG_SPRITE::TIPS_CONTAINER);

    Label* percent = static_cast<Label*>(container->getChildByTag(LoadingScene::TAG_SPRITE::PROGRESS_LABEL));
    percent->setString(StringUtils::format("%d%%", progress));

    ui::LoadingBar* loadingBar = static_cast<ui::LoadingBar*>(container->getChildByTag(LoadingScene::TAG_SPRITE::LOADING_BAR));
    loadingBar->setPercent(progress);
}

void LoadingStatusViewer::showFileCounter(int _totalFile)
{
    totalFile = _totalFile;
    const Size WINDOW_SIZE = Director::getInstance()->getWinSize();
    auto container = parent->getChildByTag(LoadingScene::TAG_SPRITE::TIPS_CONTAINER);
    const Size CONTAINER_SIZE = container->getContentSize();

    auto fileCounter = Label::createWithTTF(StringUtils::format("0/%d", totalFile), FONT_NAME_2, FONT_SIZE);
    fileCounter->setTag(LoadingScene::TAG_SPRITE::FILE_COUNTER);
    fileCounter->setPosition(Vec2(CONTAINER_SIZE.width - 50, 100 - FONT_SIZE * 1.1));
    fileCounter->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    fileCounter->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    fileCounter->setColor(Color3B::WHITE);
    container->addChild(fileCounter);
}

void LoadingStatusViewer::updateFileCounter(int current)
{
    auto container = parent->getChildByTag(LoadingScene::TAG_SPRITE::TIPS_CONTAINER);
    Label* counter = static_cast<Label*>(container->getChildByTag(LoadingScene::TAG_SPRITE::FILE_COUNTER));
    counter->setString(StringUtils::format("%d/%d", current, totalFile));
}






