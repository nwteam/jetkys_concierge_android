#include "DownloadLayer.h"
#include <dirent.h>
#include <sys/stat.h>

#include "FontDefines.h"
#include "SystemSettingModel.h"

static AssetsManager* assetManager = nullptr;

DownloadLayer::DownloadLayer():
    progress(0), pathToSave(""), _percentage(0) {}

DownloadLayer::~DownloadLayer()
{
    Director::getInstance()->getTextureCache()->removeAllTextures();
    SpriteFrameCache::getInstance()->removeSpriteFrames();
    delete assetManager;
    assetManager = 0;
}

bool DownloadLayer::init(LoadingSceneProgress* _progress, RequestAPI* _request)
{
    if (!Layer::init()) {
        return false;
    }
    progress = _progress;
    request = _request;
    initDownloadDir();
    return true;
}

void DownloadLayer::initDownloadDir()
{
    pathToSave = FileUtils::getInstance()->getWritablePath() + "Resources";
    if (!opendir(pathToSave.c_str())) {
        mkdir(pathToSave.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
    }
}

void DownloadLayer::startDownload()
{
    progress->setLabel("リソースのバージョンを確認中");
    progress->onStart();
    request->getResourceVersions([&](Json* response) {
        downloadQueue = getResourcesOfNeedUpdate(response);
        _resouceCount = downloadQueue.size();
        if (_resouceCount == 0) {
            endDownloadCallback();
        } else {
            progress->update(0);
            progress->showFileCounter(_resouceCount);
            progress->setLabel("リソースをダウンロード中");
            upgrade(nullptr);
        }
    });
}

std::vector<std::string>DownloadLayer::getResourcesOfNeedUpdate(Json* response)
{
    Json* data = Json_getItem(Json_getItem(response, "get_resource_versions"), "data");
    std::vector<std::string>results;
    if (data != NULL) {
        data = data->child;
        while (data != NULL) {
            std::string appVersion = UserDefault::getInstance()->getStringForKey(DownloadLayer::getResourceVersionKeyWithHash(data->name).c_str());
            // CCLOG("[DownloadLayer::getResourcesOfNeedUpdate] file : %s, server : %s, app : %s, ", data->name, data->valueString, appVersion.c_str());
            if (!Version::isNotNeedUpdate(appVersion, data->valueString)) {
                results.push_back(std::string(data->name));
            }
            ;
            data = data->next;
        }
    }
    return results;
}

std::string DownloadLayer::getResourceVersionKeyWithHash(const std::string& packageUrl)
{
    return StringUtils::format("%s%zd", "current-version-code", std::hash<std::string>()(packageUrl + ".zip"));
}


void DownloadLayer::upgrade(Ref* pSender)
{
    progress->updateFileCounter(_resouceCount - downloadQueue.size());
    if (downloadQueue.size() == 0) {
        return;
    }
    std::string path = SystemSettingModel::getModel()->getDlServerUrl() + downloadQueue.back();
    std::string key = downloadQueue.back();
    getAssetManager()->setPackageUrl((path + ".zip").c_str(), (key + ".zip").c_str());
    getAssetManager()->setVersionFileUrl((path + ".txt").c_str());
    getAssetManager()->update();
}

void DownloadLayer::onError(AssetsManager::ErrorCode errorCode)
{
    if (errorCode == AssetsManager::ErrorCode::NO_NEW_VERSION) {
        nextUpgrade();
    } else if (errorCode == AssetsManager::ErrorCode::NETWORK) {
        auto dialog = DialogView::createWithNetworkError();
        addChild(dialog, DIALOG_ZORDER);
        dialog->_delegate = this;
    } else if (errorCode == AssetsManager::ErrorCode::UNCOMPRESS) {
        nextUpgrade();
    }
}

void DownloadLayer::onProgress(int percent)
{
    if (percent > 100) {
        percent = 100;
    }
    progress->update(percent);
    if (percent < 100) {
        updateStatusInfo(percent);
    }
}

void DownloadLayer::onSuccess()
{
    nextUpgrade();
}

void DownloadLayer::btDialogCallback(ButtonIndex aIndex, int tag)
{
    if (aIndex == BT_YES) {
        updateStatusInfo(0);
        upgrade(nullptr);
    }
}

AssetsManager* DownloadLayer::getAssetManager()
{
    if (!assetManager) {
        assetManager = new AssetsManager("", "", pathToSave.c_str());
        assetManager->setDelegate(this);
        assetManager->setConnectionTimeout(3);
    }
    return assetManager;
}

void DownloadLayer::nextUpgrade()
{
    downloadQueue.pop_back();
    updateStatusInfo(100);
    upgrade(nullptr);
}

void DownloadLayer::updateStatusInfo(int percent)
{
    if (percent == 100) {
        _percentage += percent;
    }
    if (_percentage >= _resouceCount * 100) {
        runAction(Sequence::create(DelayTime::create(0.3f),
                                   CallFunc::create(CC_CALLBACK_0(DownloadLayer::endDownloadCallback, this)), NULL));
    }
}


void DownloadLayer::endDownloadCallback()
{
    removeFromParent();
    _delegate->endDownload();
}