#include "LoadingSceneProgress.h"

#include "NetworkErrorDialog.h"
#include "LoginFailDialog.h"
#include "API.h"

using namespace SyanagoAPI;

LoadingSceneProgress::LoadingSceneProgress(Layer* layer)
{
    _loadingStatusViewer = new LoadingStatusViewer(layer);
}

LoadingSceneProgress::~LoadingSceneProgress()
{
    delete _loadingStatusViewer;
}

void LoadingSceneProgress::setLabel(std::string label)
{
    _loadingStatusViewer->setLabel(label);
}

void LoadingSceneProgress::showFileCounter(int max)
{
    _loadingStatusViewer->showFileCounter(max);
}

void LoadingSceneProgress::update(int percent)
{
    _loadingStatusViewer->update(percent);
}

void LoadingSceneProgress::updateFileCounter(int current)
{
    _loadingStatusViewer->updateFileCounter(current);
}

void LoadingSceneProgress::onStart()
{}
void LoadingSceneProgress::onEnd()
{
    _loadingStatusViewer->setLabel("完了");
}

void LoadingSceneProgress::onError(int errorCode, std::string errorMessage, const RetryRequest& retryRequest)
{
    using namespace SyanagoAPI::API;
    std::string title = errorCode == ERROR::CODE::NETWORK ? "ネットワークエラー" : errorMessage;
    std::string message = errorCode == ERROR::CODE::NETWORK ? "ネットワーク環境の良いところで\n再接続を行ってください" : StringUtils::format("%d", errorCode);
    _loadingStatusViewer->setLabel(title);
    int tag = errorCode == ERROR::CODE::NETWORK ? ERROR::CODE::NETWORK : -1;

    std::string _apiTag = "";
    if (_apiTag == TAG::LOG_GOOGLEPLAY_PURCHASE || _apiTag == TAG::CHECK_STORE_RECEIPT) {
        tag = 250;
    }
    if (errorCode == ERROR::CODE::SERVER_MAINTENANCE) {
        tag = ERROR::CODE::SERVER_MAINTENANCE;
    } else if (errorCode == ERROR::CODE::NOT_FOUND_PLAYER || errorCode == ERROR::CODE::VALIDATION_FAIL) {
        message = (errorCode == ERROR::CODE::NOT_FOUND_PLAYER) ? ERROR::getMessage(ERROR::CODE::NOT_FOUND_PLAYER) : ERROR::getMessage(ERROR::CODE::VALIDATION_FAIL);
        LoginFailDialog::show(message);
        return;
    }
    NetworkErrorDialog::show(title, message, tag, [this, retryRequest]() {
        _loadingStatusViewer->setLabel("再接続中...");
        retryRequest();
    });
}
