#include "Tips.h"
#include "LoadingScene.h"

const int Tips::FONT_SIZE = 22;
const int Tips::FONT_MARGIN = FONT_SIZE * 1.1;

Tips::Tips():
    container(nullptr), tapCount(0) {}

bool Tips::init(Sprite* _container)
{
    container = _container;

    showTips();

    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(Tips::onTouchBegan, this);
    listener->onTouchMoved = [](Touch* touch, Event* unused_event) {};
    listener->onTouchEnded = [](Touch* touch, Event* unused_event) {};
    listener->onTouchCancelled = [](Touch* touch, Event* unused_event) {};
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    return true;
}

void Tips::showTapCounter()
{
    const Vec2 BASE_POINT = Vec2(container->getContentSize().width - 63,
                                 193 - FONT_MARGIN);
    tapCounter = Label::createWithTTF("0回", FONT_NAME_2, FONT_SIZE);
    tapCounter->setTag(LoadingScene::TAG_SPRITE::TAP_COUNTER);
    tapCounter->setPosition(Vec2(BASE_POINT.x, BASE_POINT.y));
    tapCounter->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    tapCounter->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    tapCounter->setColor(Color3B(254, 115, 172));
    container->addChild(tapCounter);
}

void Tips::showTips()
{
    const Vec2 BASE_POINT = Vec2(68, container->getContentSize().height - 110);
    Label* label = Label::createWithTTF(getTipString(), FONT_NAME_2, FONT_SIZE);
    label->setTag(LoadingScene::TAG_SPRITE::TIPS_STRING);
    label->setDimensions(420, 0);
    label->setPosition(Vec2(BASE_POINT.x, BASE_POINT.y));
    label->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    label->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    label->setColor(Color3B::WHITE);
    container->addChild(label);
}


bool Tips::onTouchBegan(Touch* touch, Event* unused_event)
{
    showNextTips();
    if (tapCount == 0) {
        showTapCounter();
    }
    tapCount++;
    tapCounter->setString(StringUtils::format("%ld回", tapCount));

    if (tapCount % 50 == 0) {
        ParticleSystemQuad* particle = ParticleSystemQuad::create("Fire.plist");
        particle->setTag(touch->getID());
        particle->setPosition(tapCounter->getPosition());
        particle->setDuration(0.2f);
        particle->setAutoRemoveOnFinish(true);
        container->addChild(particle);
    }
    return true;
}

void Tips::showNextTips()
{
    static_cast<Label*>(container->getChildByTag(LoadingScene::TAG_SPRITE::TIPS_STRING))->setString(getTipString());
}

std::string Tips::getTipString()
{
    if (tips.empty()) {
        const std::string FILE_NAME = "tips.json";
        auto fileUtils = FileUtils::getInstance();
        Json* json = Json_create(fileUtils->getStringFromFile(FILE_NAME).c_str())->child;
        while (json != NULL) {
            tips.push_back(json->valueString);
            json = json->next;
        };
        Json_dispose(json);
        std::random_shuffle(tips.begin(), tips.end());
    }
    std::string result = tips.back();
    tips.pop_back();
    return result;
}


