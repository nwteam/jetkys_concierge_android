#ifndef __syanago__ResouseDeleteModel__
#define __syanago__ResouseDeleteModel__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace SyanagoAPI;

class ResouseDeleteModel :public AssetsManagerDelegateProtocol
{
public:
    ResouseDeleteModel();
    ~ResouseDeleteModel();
    
    void StartDeleteResouses();
    void StartDeleteOldResouses();
    
private:
    RequestAPI* request;
    DefaultProgress* progress;
    
    AssetsManager* getAssetManager();
    
    void deleteResouses(const std::string resousesPath);
    
    std::vector<std::string> getResources(Json* response);
    std::vector<std::string> getOldResourcePaths();
};

#endif
