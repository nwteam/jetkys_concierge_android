#include "LoadingScene.h"
#include "Version.h"
#include "editor-support/spine/Json.h"
#include "SplashScene.h"
#include "StoryTelopScene.h"
#include "BrowserLauncher.h"
#include "PlayerController.h"
#include "UserDefaultManager.h"
#include "MainScene.h"
#include "DialogWithCallbackButton.h"
#include "DialogWithDoubleCallbackButton.h"
#include "SelectFirstCarScene.h"
#include "Tips.h"
#include "UpdataMasterDataModel.h"
#include "CharacterResourceDebugger.h"

LoadingScene::LoadingScene():
    progress(nullptr), request(nullptr)
{}

LoadingScene::~LoadingScene()
{
    delete progress;
    delete request;
}

Scene* LoadingScene::createScene(SEQUENCE sequence)
{
    auto scene = Scene::create();
    scene->addChild(LoadingScene::create(sequence));
    return scene;
}


bool LoadingScene::init(SEQUENCE _sequence)
{
    if (!Layer::init()) {
        return false;
    }
    sequence = _sequence;

    showBackgrounds();
    showTips();

    progress = new LoadingSceneProgress(dynamic_cast<Layer*>(this));
    request = new RequestAPI(progress);
    return true;
}

void LoadingScene::showTips()
{
    addChild(Tips::create(static_cast<Sprite*>(getChildByTag(TAG_SPRITE::TIPS_CONTAINER))));
}

void LoadingScene::showBackgrounds()
{
    auto background = Sprite::create("tips_bg.png");
    background->setPosition(Director::getInstance()->getWinSize() / 2);
    addChild(background, Z_ORDER::BACKGROUND);

    Sprite* container = Sprite::create("tips_frame.png");
    container->setTag(TAG_SPRITE::TIPS_CONTAINER);
    container->setPosition(Director::getInstance()->getWinSize() / 2);
    container->setOpacity(204);

    auto title = Label::createWithTTF("▼ TIPS ▼", FONT_NAME_2, 30);
    title->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    title->setPosition(Vec2(container->getContentSize().width / 2, container->getContentSize().height - 38));
    title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    title->setColor(Color3B(254, 115, 172));
    container->addChild(title);
    addChild(container, Z_ORDER::TIPS);
}

void LoadingScene::onEnterTransitionDidFinish()
{
    progress->update(0);
    switch (sequence) {
    case SEQUENCE::FROM_DEBUG_RESOURCES:
    case SEQUENCE::INITALIZE:
        progress->setLabel("バージョンの確認中...");
        progress->onStart();
        request->getVersion(CC_CALLBACK_1(LoadingScene::onResponseGetVersion, this));
        break;
    case SEQUENCE::SYNC_PLAYER_DATA:
        progress->setLabel("ローカルDBを更新中");
        progress->onStart();
        request->sync(CC_CALLBACK_1(LoadingScene::onResponseSync, this));
        break;
    case SEQUENCE::NOTING_TO_DO:
        progress->showFileCounter(300);
        break;
    default:
        break;
    }
}

const std::string LoadingScene::FORCE_UPDATE_YES = "1";

void LoadingScene::onResponseGetVersion(Json* response)
{
    progress->update(10);
    Version version = Version(response);
    if (version.checkVersion()) {
        progress->setLabel("認証中...");
        progress->onStart();
        if (PLAYERCONTROLLER->isExistPlayer()) {
            request->auth(CC_CALLBACK_1(LoadingScene::onResponseAuth, this));
        } else {
            request->createPlayer("PLAYER", CC_CALLBACK_1(LoadingScene::onResponseCreatePlayer, this));
        }
    } else {
        progress->update(100);
        std::string updateMessage = "アップデートを行ってください";
        if (version.getForceUpdate() != FORCE_UPDATE_YES) {
            addChild(DialogWithDoubleCallbackButton::create("アップデートしますか？",
                                                            "最新のバージョンがあります",
                                                            CC_CALLBACK_0(LoadingScene::forceUpdate, this),
                                                            []() {
                transitScene(SplashScene::createScene());
            }), DIALOG_ZORDER);
        } else {
            addChild(DialogWithCallbackButton::create("アップデートを行ってください", "最新のバージョンがあります", CC_CALLBACK_0(LoadingScene::forceUpdate, this)));
        }
        storeUrl = version.getStoreUrl();
    }
}

void LoadingScene::forceUpdate()
{
    Cocos2dExt::BrowserLauncher::launchUrl(storeUrl.c_str());
    transitScene(SplashScene::createScene());
}

void LoadingScene::onResponseCreatePlayer(Json* response)
{
    progress->update(20);
    progress->onStart();
    UserDefault::getInstance()->setBoolForKey(KEY_FIRST_CREATE_PLAYER, true);
    PLAYERCONTROLLER->saveNewPlayer(Json_getItem(response, "create_player"));
    request->auth(CC_CALLBACK_1(LoadingScene::onResponseAuth, this));
}

void LoadingScene::onResponseAuth(Json* response)
{
    progress->update(30);
    progress->setLabel("マスターDBを取得中");
    progress->onStart();
    progress->update(40);
    request->sync(CC_CALLBACK_1(LoadingScene::onResponseSync, this));
}

void LoadingScene::onResponseSync(Json* response)
{
    progress->update(50);
    progress->setLabel("ローカルDBを更新中");
    progress->update(60);
    Json* data = Json_getItem(response, "sync");
    UpdataMasterDataModel::setMasterData(data);
    progress->setLabel("マスターDBを取得中");
    progress->update(70);
    request->syncWord(CC_CALLBACK_1(LoadingScene::onResponseSyncWord, this));
}

void LoadingScene::onResponseSyncWord(Json* response)
{
    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::IS_RESET_MASTER_TABLE, false);
    progress->setLabel("ローカルDBを更新中");
    progress->update(80);
    Json* data = Json_getItem(response, "sync");
    UpdataMasterDataModel::setMasterData(data);
    progress->update(100);
    progress->onEnd();
    auto downloadLayer = DownloadLayer::create(progress, request);
    downloadLayer->_delegate = this;
    addChild(downloadLayer, 100);
    downloadLayer->startDownload();
}

void LoadingScene::endDownload()
{
    switch (sequence) {
    case SEQUENCE::INITALIZE: {
        progress->setLabel("");
        HTMLDialog* htmlDialog = HTMLDialog::create();
        htmlDialog->setDialogViewFlag(true);
        htmlDialog->_delegate = this;
        addChild(htmlDialog, 1000);
        break;
    }
    case SEQUENCE::SYNC_PLAYER_DATA: {
        progress->setLabel("お知らせを取得中...");
        progress->onStart();
        request->getInformations(CC_CALLBACK_1(LoadingScene::onResponseGetDialog, this));
        break;
    }
    case SEQUENCE::FROM_DEBUG_RESOURCES:
        Director::getInstance()->replaceScene(CharacterResourceDebugger::createScene());
        break;
    case SEQUENCE::NOTING_TO_DO:
        break;
    }
}


void LoadingScene::onResponseGetDialog(Json* json)
{
    progress->onStart();
    Json* mainData = Json_getItem(json, "get_dialog");
    Json* data = Json_getItem(mainData, "data");
    for (int i = 0; i < PLAYERCONTROLLER->_messages.size(); i++) {
        std::string key = StringUtils::format("scroll_message_%d", i + 1);
        PLAYERCONTROLLER->_messages.at(i) = Json_getString(data, key.c_str(), "");
    }
    progress->onEnd();
    transitScene(SelectFirstCarScene::createScene());
}


void LoadingScene::endHtmlDialog()
{
    Director::getInstance()->replaceScene(MainScene::createScene());
}






