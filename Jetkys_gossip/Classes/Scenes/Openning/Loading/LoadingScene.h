#ifndef __syanago__LoadingScene__
#define __syanago__LoadingScene__

#include "cocos2d.h"
#include "create_func.h"
#include "DownloadLayer.h"
#include "HTMLDialog.h"
#include "RequestAPI.h"
#include "LoadingSceneProgress.h"
#include "LoadingStatusViewer.h"

USING_NS_CC;
using namespace SyanagoAPI;

class LoadingScene : public Layer, public create_func<LoadingScene>, public DownloadDelegate, public HtMLDialogDelegate {
public:
    enum SEQUENCE {
        INITALIZE = 0,
        SYNC_PLAYER_DATA,
        NOTING_TO_DO,
        FROM_DEBUG_RESOURCES,
    };
    enum TAG_SPRITE {
        TIPS_CONTAINER = 0,
        TIPS_STRING,
        STATUS_LABEL,
        FILE_COUNTER,
        PROGRESS_LABEL,
        LOADING_BAR,
        TAP_COUNTER
    };
    
    LoadingScene();
    ~LoadingScene();
    static Scene *createScene(SEQUENCE _sequence);
    using create_func::create;
    bool init(SEQUENCE sequence);
    void onEnterTransitionDidFinish();

private:
    enum Z_ORDER {
        BACKGROUND = 0,
        TIPS,
        PARTS
    };
    
    void showBackgrounds();
    void showTips();
    
    void onResponseGetVersion(Json* response);
    
    void forceUpdate();
    
    void onResponseCreatePlayer(Json *response);
    void onResponseAuth(Json *response);
    void onResponseSync(Json *response);
    void onResponseSyncWord(Json *response);
    void onResponseGetDialog(Json *response);
    
    //DownloadDelegate
    void endDownload();
    
    //HtMLDialogDelegate
    void endHtmlDialog();
    
    static const std::string FORCE_UPDATE_YES;
    
    RequestAPI* request;
    LoadingSceneProgress* progress;
    SEQUENCE sequence;
    std::string storeUrl;
};



#endif /* defined(__syanago__LoadingScene__) */
