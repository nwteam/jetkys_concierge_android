#include "ResouseDeleteModel.h"
#include <dirent.h>
#include <sys/stat.h>
#include "SystemSettingModel.h"
#include "UserDefaultManager.h"
#include "CharacterModel.h"
#include "AreaModel.h"
#include "MapModel.h"
#include "CourseModel.h"

static AssetsManager* assetManager = nullptr;

ResouseDeleteModel::ResouseDeleteModel():
    progress(nullptr), request(nullptr)
{
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
}

ResouseDeleteModel::~ResouseDeleteModel()
{
    delete progress;
    delete request;
    progress = nullptr;
    request = nullptr;
}

AssetsManager* ResouseDeleteModel::getAssetManager()
{
    if (assetManager == nullptr) {
        auto pathToSave = FileUtils::getInstance()->getWritablePath();
        pathToSave += "Resources";

        DIR* pDir = opendir(pathToSave.c_str());
        if (!pDir) {
            mkdir(pathToSave.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
        }
        assetManager = new AssetsManager("", "", pathToSave.c_str());
        assetManager->setDelegate(this);
        assetManager->setConnectionTimeout(3);
    }
    return assetManager;
}

void ResouseDeleteModel::StartDeleteResouses()
{
    std::string pathToSave = FileUtils::getInstance()->getWritablePath() + "Resources/";
    deleteResouses(pathToSave);
    progress->onStart();
    request->getResourceVersions([&](Json* response) {
        auto resourcesPaths = getResources(response);
        std::shared_ptr<SystemSettingModel>systemModel = SystemSettingModel::getModel();
        for (auto resStr : resourcesPaths) {
            std::string path = systemModel->getDlServerUrl() + resStr;
            getAssetManager()->setPackageUrl((path + ".zip").c_str(), (resStr + ".zip").c_str());
            getAssetManager()->setVersionFileUrl((path + ".txt").c_str());
            getAssetManager()->deleteVersion();
        }
        delete assetManager;
        assetManager = 0;
    });
    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::IS_RESET_MASTER_TABLE, true);
}

void ResouseDeleteModel::StartDeleteOldResouses()
{
    std::string oldFilePass = FileUtils::getInstance()->getOldWritablePath() + "Resources/";
    deleteResouses(oldFilePass);
    std::shared_ptr<SystemSettingModel>systemModel = SystemSettingModel::getModel();
    for (auto resStr : getOldResourcePaths()) {
        std::string path = systemModel->getDlServerUrl() + resStr;
        getAssetManager()->setPackageUrl((path + ".zip").c_str());
        getAssetManager()->setVersionFileUrl((path + ".txt").c_str());
        getAssetManager()->deleteVersion();
    }
}

void ResouseDeleteModel::deleteResouses(const std::string resousesPath)
{
    //TODO リソースの消去処理を追加
    CCLOG("[ResouseDeleteModel::deleteResouses]リソース消去のパス:%s", resousesPath.c_str());
    // Remove downloaded files
    #if (CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)
        std::string command = "rm -r ";
        // Path may include space.
        command += "\"" + resousesPath + "\"";
        system(command.c_str());
        CCLOG("[ResouseDeleteModel::deleteResouses]リソース消去のコマンド:%s", command.c_str());
    #else
        string command = "rd /s /q ";
        // Path may include space.
        command += "\"" + resousesPath + "\"";
        system(command.c_str());
        CCLOG("[ResouseDeleteModel::deleteResouses]リソース消去のコマンド:%s", command.c_str());
    #endif
}

std::vector<std::string>ResouseDeleteModel::getResources(Json* response)
{
    Json* data = Json_getItem(Json_getItem(response, "get_resource_versions"), "data");
    std::vector<std::string>results;
    if (data != NULL) {
        data = data->child;
        while (data != NULL) {
            results.push_back(std::string(data->name));
            data = data->next;
        }
    }
    return results;
}

std::vector<std::string>ResouseDeleteModel::getOldResourcePaths()
{
    //character
    std::vector<std::string>results = CharacterModel::getResources();

    //character icon
    results.push_back("/character_icons/icon");
    results.push_back("/character_icons/icon_r");
    results.push_back("/character_icons/icon_s");
    results.push_back("/character_icons/icon_hk");
    results.push_back("/character_icons/icon_co");
    results.push_back("/character_icons/icon_cus");
    results.push_back("/character_icons/ca_sdb");
    results.push_back("/character_icons/tu_ca");

    //SE mp3Data
    results.push_back("/sound/soundSE/No04");
    results.push_back("/sound/soundSE/No05");
    results.push_back("/sound/soundSE/No06");
    results.push_back("/sound/soundSE/No07");
    results.push_back("/sound/soundSE/No08");
    results.push_back("/sound/soundSE/No09");
    results.push_back("/sound/soundSE/No10");
    results.push_back("/sound/soundSE/No11");
    results.push_back("/sound/soundSE/No12");
    results.push_back("/sound/soundSE/No13");
    results.push_back("/sound/soundSE/No14");
    results.push_back("/sound/soundSE/No15");
    results.push_back("/sound/soundSE/No16");
    results.push_back("/sound/soundSE/No17");
    results.push_back("/sound/soundSE/No19");
    results.push_back("/sound/soundSE/No22");
    results.push_back("/sound/soundSE/No23");
    results.push_back("/sound/soundSE/No24");
    results.push_back("/sound/soundSE/No25");
    results.push_back("/sound/soundSE/No26");
    results.push_back("/sound/soundSE/No27");
    results.push_back("/sound/soundSE/No28");
    results.push_back("/sound/soundSE/No29");
    results.push_back("/sound/soundSE/No32");
    results.push_back("/sound/soundSE/No33");
    results.push_back("/sound/soundSE/No34");
    results.push_back("/sound/soundSE/No35");
    results.push_back("/sound/soundSE/No36");

    //Parts
    results.push_back("/parts/icon_pab"); //big
    results.push_back("/parts/icon_pag"); //gacha
    results.push_back("/parts/icon_pas"); //store

    //tutorial
    results.push_back("/tutorial/tuto_tap1");
    results.push_back("/tutorial/tuto_tap2");
    results.push_back("/tutorial/tuto_tap3");
    results.push_back("/tutorial/tuto_tap4");
    results.push_back("/tutorial/tuto_tap5");

    //Tresures
    results.push_back("/treasure/icon_treasure");

    //Shop
    results.push_back("/shop/icon_tokens");

    //home
    results.push_back("/home/home_background");
    results.push_back("/home/home_avatar");

    //dice
    results.push_back("/race/item/dice_set");
    results.push_back("/race/item/item_set");

    //start and stop
    results.push_back("/race/item/doorway");

    //worldMap(parts)
    results.push_back("/worldmaps/dialog_map");
    results.push_back("/worldmaps/map_slime_image");

    //BGM mp3Data
    results.push_back("/sound/soundBGM/No01");
    auto bgmIdCount = CourseModel::getCourseBgmIdCount();
    for (int i = 1; i <= bgmIdCount; i++) {
        results.push_back(StringUtils::format("/sound/soundBGM/bgm_%d", i));
    }

    //Race
    int imageIdCount = CourseModel::getCourseImageIdCount();
    for (int i = 1; i <= imageIdCount; i++) {
        results.push_back(StringUtils::format("/race/cells/cells_%d", i));
        results.push_back(StringUtils::format("/race/backgrounds/%d_background", i));
    }

    //worldMap
    auto tempMapVec = MapModel::getResources();
    for (int i = 0; i < tempMapVec.size(); i++) {
        results.push_back(tempMapVec.at(i));
    }
    int gropeId = MapModel::getMapGroupCount();
    for (int i = 1; i <= gropeId; i++) {
        results.push_back(StringUtils::format("/worldmaps/map/nextMapGropeClear/%d_next_map_grope_clear", i));
        results.push_back(StringUtils::format("/worldmaps/map/nextMapGropeNonClear/%d_next_map_grope_non_clear", i));
        results.push_back(StringUtils::format("/worldmaps/map/selectAreaSea/%d_select_area_sea", i));
    }
    auto tempAreaVec = AreaModel::getResources();//"/worldmaps/area/%d_area"
    for (int i = 0; i < tempAreaVec.size(); i++) {
        results.push_back(tempAreaVec.at(i));
    }

    return results;
}

