#ifndef __syanago__UpdataMasterDataModel__
#define __syanago__UpdataMasterDataModel__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"

USING_NS_CC;

class UpdataMasterDataModel
{
public:
    static void setMasterData(Json* json);
};

#endif
