#include "UpdataMasterDataModel.h"
#include "MapModel.h"
#include "AreaModel.h"
#include "CourseModel.h"
#include "GridTypeModel.h"
#include "GridOperandsModel.h"
#include "CharacterModel.h"
#include "LevelModel.h"
#include "RarityModel.h"
#include "BirthplaceModel.h"
#include "BodyTypeModel.h"
#include "MakerModel.h"
#include "SkillModel.h"
#include "SkillNameModel.h"
#include "LeaderSkillModel.h"
#include "LeaderSkillNameModel.h"
#include "WordsModel.h"
#include "PartModel.h"
#include "RankModel.h"
#include "ItemModel.h"
#include "ItemUnitModel.h"
#include "SystemSettingModel.h"

void UpdataMasterDataModel::setMasterData(Json* json)
{
    Json* data = Json_getItem(json, "data");
    if (!data) {
        return;
    }
    Json* mDatas = Json_getItem(data, "areas");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<AreaModel>model(new AreaModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "birthplaces");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<BirthplaceModel>model(new BirthplaceModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "body_types");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<BodyTypeModel>model(new BodyTypeModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "characters");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<CharacterModel>model(new CharacterModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "courses");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<CourseModel>model(new CourseModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "grid_types");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<GridTypeModel>model(new GridTypeModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "item_units");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<ItemUnitModel>model(new ItemUnitModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "items");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<ItemModel>model(new ItemModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "leader_skills");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<LeaderSkillModel>model(new LeaderSkillModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "skills");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<SkillModel>model(new SkillModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "skill_names");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<SkillNameModel>model(new SkillNameModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "leader_skill_names");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<LeaderSkillNameModel>model(new LeaderSkillNameModel);
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "levels");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<LevelModel>model(new LevelModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "makers");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<MakerModel>model(new MakerModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "maps");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<MapModel>model(new MapModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "parts");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<PartModel>model(new PartModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "ranks");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<RankModel>model(new RankModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "rarities");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<RarityModel>model(new RarityModel());
        model->hulkInsert(mDatas);
    }
    // words
    mDatas = Json_getItem(data, "words");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<WordsModel>model(new WordsModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "system_settings");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<SystemSettingModel>model(new SystemSettingModel());
        model->hulkInsert(mDatas);
    }
    mDatas = Json_getItem(data, "grid_operands");
    if (mDatas && mDatas->type == Json_Array) {
        std::shared_ptr<GridOperandsModel>model(new GridOperandsModel());
        model->hulkInsert(mDatas);
    }
}