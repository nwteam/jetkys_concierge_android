#ifndef __syanago__LoadingSceneProgress__
#define __syanago__LoadingSceneProgress__

#include "cocos2d.h"
#include "ProgressAbstract.h"
#include "Loading.h"
#include "RequestAPI.h"
#include "LoadingStatusViewer.h"

USING_NS_CC;

namespace SyanagoAPI {

class LoadingSceneProgress: public ProgressAbstract
{
    public:
    LoadingSceneProgress(Layer * layer);
    ~LoadingSceneProgress();

    void setLabel(std::string label);
    void showFileCounter(int max);
    void update(int percent);
    void updateFileCounter(int current);

    void onStart() override;
    void onEnd() override;
    void onError(int errorCode, std::string message, const RetryRequest& retryRequest) override;
    private:
    LoadingStatusViewer* _loadingStatusViewer;
};

};
#endif /* defined(__syanago__LoadingSceneProgress__) */
