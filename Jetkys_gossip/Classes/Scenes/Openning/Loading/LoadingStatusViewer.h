#ifndef __syanago__LoadingStatusViewer__
#define __syanago__LoadingStatusViewer__

#include "cocos2d.h"

USING_NS_CC;

class LoadingStatusViewer
{
public:
    LoadingStatusViewer(Layer * layer);
    void showLoadingBar();
    void showStatusLabel();
    void setLabel(std::string label);
    void update(int progress);
    void showFileCounter(int totalFile);
    void updateFileCounter(int current);

private:
    static const int FONT_SIZE;
    static const int FONT_MARGIN;
    Layer* parent;
    int totalFile;
};

#endif /* defined(__syanago__LoadingStatusViewer__) */
