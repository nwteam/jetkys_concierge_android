#ifndef __syanago__Tips__
#define __syanago__Tips__

#include "cocos2d.h"
#include "create_func.h"

USING_NS_CC;

class Tips : public Layer, public create_func<Tips>
{
public:
    Tips();
    using create_func::create;
    bool init(Sprite* container);
private:
    void showTapCounter();
    void showTips();
    void showNextTips();
    
    bool onTouchBegan(Touch *touch, Event *unused_event);
    
    std::string getTipString();
    
    static const int FONT_SIZE;
    static const int FONT_MARGIN;
    Sprite* container;
    Label* tapCounter;
    unsigned long tapCount;
    std::vector<std::string> tips;
};




#endif /* defined(__syanago__Tips__) */
