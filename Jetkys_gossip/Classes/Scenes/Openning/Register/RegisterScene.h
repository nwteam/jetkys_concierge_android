#ifndef __Syanago__RegisterScene__
#define __Syanago__RegisterScene__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "RequestAPI.h"
#include "API.h"
#include "DefaultProgress.h"

using namespace cocos2d::ui;
USING_NS_CC;
using namespace SyanagoAPI;

class RegisterScene : public cocos2d::Layer{
public:
    ~RegisterScene(){
        delete request;
        delete progress;
    }
    static cocos2d::Scene * createScene();
    bool init();
    CREATE_FUNC(RegisterScene);
    
private:
    RequestAPI* request;
    DefaultProgress* progress;
    
    enum TAG_VIEW_PARTS {
        INPUT_BASE = 0,
        EDIT_BOX
    };
    
    void showBackground();
    void showHeadLine();
    void showInputBase();
    void showInputLabel();
    void showEditBox();
    void showOkButton();
    void showBackButton();
    
    bool preventEditBoxEvent(Touch* touch, Event* event);
    void notPreventEditBoxEvent(Touch* touch, Event* event);
    
    void onTapOkButton(Ref* sender, ui::Widget::TouchEventType type);
    
    void onResponseCreatePlayer(Json* response);
    
};

#endif /* defined(__Syanago__RegisterScene__) */
