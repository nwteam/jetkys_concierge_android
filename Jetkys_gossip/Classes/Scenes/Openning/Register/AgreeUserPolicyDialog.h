#ifndef __syanago__AgreeUserPolicyDialog__
#define __syanago__AgreeUserPolicyDialog__

#include "DialogAbstract.h"

class AgreeUserPolicyDialog : public DialogAbstract
{
public:
    CREATE_FUNC(AgreeUserPolicyDialog);
    bool init();
private:
    void showTitle();
    void showDescription();
    void showUserPolicyButton();
    void showYesButton();
    void showNoButton();
    
    Vec2 titlePosition;
};



#endif /* defined(__syanago__AgreeUserPolicyDialog__) */
