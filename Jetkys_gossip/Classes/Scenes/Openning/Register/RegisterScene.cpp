#include "RegisterScene.h"
#include "FontDefines.h"
#include "TitleScene.h"
#include "jCommon.h"
#include "PlayerController.h"
#include "Loading.h"
#include "SoundHelper.h"
#include "LoadingScene.h"
#include "AgreeUserPolicyDialog.h"
#include "SimpleDialog.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace SyanagoAPI;

Scene* RegisterScene::createScene()
{
    auto scene = Scene::create();
    scene->addChild(RegisterScene::create());
    return scene;
}

bool RegisterScene::init()
{
    if (!Layer::init()) {
        return false;
    }

    progress = new DefaultProgress();
    request = new RequestAPI(progress);

    addChild(AgreeUserPolicyDialog::create(), DIALOG_ZORDER);

    showBackground();
    showHeadLine();
    showInputBase();
    showInputLabel();
    showEditBox();
    showOkButton();
    showBackButton();

    return true;
}

void RegisterScene::showBackground()
{
    Sprite* backgroundImage = Sprite::create("home_bg.png");
    backgroundImage->setPosition(Director::getInstance()->getWinSize() / 2);
    addChild(backgroundImage, -1);
}

void RegisterScene::showHeadLine()
{
    Sprite* titleBackground = makeSprite("head_line_no1.png");
    titleBackground->setPosition(Vec2(titleBackground->getContentSize().width / 2, Director::getInstance()->getWinSize().height - titleBackground->getContentSize().height / 2 - 14));
    addChild(titleBackground);

    cocos2d::Label* title = Label::createWithTTF("ユーザー登録", FONT_NAME_2, 34);
    title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    title->setAnchorPoint(Vec2(0.0f, 0.5f));
    title->setPosition(Point(14, titleBackground->getPosition().y - 17));
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(title);
}

void RegisterScene::showInputBase()
{
    Sprite* base = makeSprite("friend_idserch_base.png");
    base->setTag(TAG_VIEW_PARTS::INPUT_BASE);
    base->setPosition(Point(Director::getInstance()->getWinSize().width / 2, Director::getInstance()->getWinSize().height / 2));
    addChild(base);
}

void RegisterScene::showInputLabel()
{
    cocos2d::Label* lead = Label::createWithTTF("あなたの名前を入力してください", FONT_NAME_2, 28);
    lead->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    lead->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2, Director::getInstance()->getWinSize().height / 2 + 130 - 14));
    addChild(lead);
}

void RegisterScene::showEditBox()
{
    cocos2d::ui::EditBox* editBox = cocos2d::ui::EditBox::create(Size(270 * 1.5, 46 * 1.5), Scale9Sprite::create("friend_input_base.png"));
    editBox->setFontSize(38);
    editBox->setFontName(FONT_NAME_4);
    editBox->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2, Director::getInstance()->getWinSize().height / 2 + 20));
    editBox->setMaxLength(10);
    editBox->setFontColor(Color3B::BLACK);
    editBox->setInputMode(EditBox::InputMode::SINGLE_LINE);
    editBox->setReturnType(EditBox::KeyboardReturnType::DONE);
    editBox->setTag(TAG_VIEW_PARTS::EDIT_BOX);
    addChild(editBox);
}

void RegisterScene::showOkButton()
{
    Size winSize = Director::getInstance()->getWinSize();
    ui::Button* button = ui::Button::create("gacha_dialog_button.png");
    button->addTouchEventListener(CC_CALLBACK_2(RegisterScene::onTapOkButton, this));
    button->setPosition(Vec2(winSize.width / 2, winSize.height / 2) +
                        Vec2(0, -getChildByTag(TAG_VIEW_PARTS::INPUT_BASE)->getContentSize().height / 2 + 40 + button->getContentSize().height / 2));

    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(RegisterScene::preventEditBoxEvent, this);
    listener->onTouchEnded = CC_CALLBACK_2(RegisterScene::notPreventEditBoxEvent, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, button);

    cocos2d::Label* label = Label::createWithTTF("OK", FONT_NAME_2, 28);
    label->setPosition(Point(button->getContentSize().width / 2, button->getContentSize().height / 2 - 14));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    button->addChild(label);
    addChild(button);
}

void RegisterScene::showBackButton()
{
    ui::Button* button = ui::Button::create("back_button.png");
    Node* base = getChildByTag(TAG_VIEW_PARTS::INPUT_BASE);
    button->setPosition(Vec2(base->getPosition().x - base->getContentSize().width / 2 + button->getContentSize().width / 2 - Director::getInstance()->getWinSize().width / 2,
                             base->getPosition().x - base->getContentSize().width / 2 + button->getContentSize().height / 2 - Director::getInstance()->getWinSize().height / 2));
    button->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_FALSE, false);
            Director::getInstance()->replaceScene(TitleScene::createScene());
        }
    });
    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(RegisterScene::preventEditBoxEvent, this);
    listener->onTouchEnded = CC_CALLBACK_2(RegisterScene::notPreventEditBoxEvent, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, button);

    addChild(button);
}

bool RegisterScene::preventEditBoxEvent(Touch* touch, Event* event)
{
    auto target = static_cast<Sprite*>(event->getCurrentTarget());

    Point locationInNode = target->convertToNodeSpace(touch->getLocation());
    Size s = target->getContentSize();
    Rect rect = Rect(0, 0, s.width, s.height);
    if (rect.containsPoint(locationInNode)) {
        static_cast<cocos2d::ui::EditBox*>(getChildByTag(TAG_VIEW_PARTS::EDIT_BOX))->setEnabled(false);
        return true;
    }
    return false;
}

void RegisterScene::notPreventEditBoxEvent(Touch* touch, Event* event)
{
    static_cast<cocos2d::ui::EditBox*>(getChildByTag(TAG_VIEW_PARTS::EDIT_BOX))->setEnabled(true);
}

void RegisterScene::onTapOkButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        std::string name = static_cast<cocos2d::ui::EditBox*>(getChildByTag(TAG_VIEW_PARTS::EDIT_BOX))->getText();

        // TODO:全角文字のカウント
        if (name.size() < 2) {
            addChild(SimpleDialog::create("二文字以上入力してください", "文字数が不足しています"), DIALOG_ZORDER);
            return;
        }
        progress->onStart();
        request->createPlayer(name, CC_CALLBACK_1(RegisterScene::onResponseCreatePlayer, this));
    }
}

void RegisterScene::onResponseCreatePlayer(Json* response)
{
    UserDefault::getInstance()->setBoolForKey(KEY_FIRST_CREATE_PLAYER, true);
    PLAYERCONTROLLER->saveNewPlayer(Json_getItem(response, "create_player"));
    Director::getInstance()->replaceScene(LoadingScene::createScene(LoadingScene::SEQUENCE::SYNC_PLAYER_DATA));
    return;
}
