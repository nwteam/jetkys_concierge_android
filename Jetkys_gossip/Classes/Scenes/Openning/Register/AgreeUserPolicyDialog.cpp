#include "AgreeUserPolicyDialog.h"
#include "FontDefines.h"
#include "BrowserLauncher.h"
#include "SplashScene.h"

bool AgreeUserPolicyDialog::init()
{
    if (!DialogAbstract::init("shop_age_apply_base.png")) {
        return false;
    }
    showTitle();
    showDescription();
    showUserPolicyButton();
    showYesButton();
    showNoButton();
    return true;
}

void AgreeUserPolicyDialog::showTitle()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto titleLabel = Label::createWithTTF("車なごコレクション利用規約", FONT_NAME_2, 34);
    titleLabel->setDimensions(500, 0);
    titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    titleLabel->setPosition(Point(winSize.width / 2,
                                  winSize.height / 2 + background->getContentSize().height / 2 - 45 - titleLabel->getContentSize().height / 2));
    titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    titleLabel->setColor(COLOR_YELLOW);
    titlePosition = titleLabel->getPosition();
    addChild(titleLabel, 1);
}

void AgreeUserPolicyDialog::showDescription()
{
    auto titleLabel2 = Label::createWithTTF("このアプリケーションの利用には、", FONT_NAME_2, 28);
    titleLabel2->setDimensions(500, 0);
    titleLabel2->setHorizontalAlignment(TextHAlignment::CENTER);
    titleLabel2->setPosition(titlePosition + Vec2(0, -45 - titleLabel2->getContentSize().height / 2));
    titleLabel2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(titleLabel2, 1);

    auto textLabel = Label::createWithTTF("利用規約への同意が必要です。", FONT_NAME_2, 28);
    textLabel->setPosition(titleLabel2->getPosition() + Vec2(0, -10 - textLabel->getContentSize().height / 2));
    textLabel->setVerticalAlignment(TextVAlignment::TOP);
    textLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    textLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    textLabel->setDimensions(500, 0);
    addChild(textLabel, 1);

    auto textLabel2 = Label::createWithTTF("利用規約を確認し、同意しますか？", FONT_NAME_2, 28);
    textLabel2->setDimensions(500, 0);
    textLabel2->setPosition(textLabel->getPosition() + Vec2(0, -45 - textLabel2->getContentSize().height / 2));
    textLabel2->setVerticalAlignment(TextVAlignment::TOP);
    textLabel2->setHorizontalAlignment(TextHAlignment::CENTER);
    textLabel2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(textLabel2, 1);
}

void AgreeUserPolicyDialog::showUserPolicyButton()
{
    Size winSize = Director::getInstance()->getWinSize();
    ui::Button* button = ui::Button::create("composition_empty_button.png");
    button->setPosition(Vec2(winSize.width / 2, winSize.height / 2) + Vec2(0, -28 * 3 + button->getContentSize().height / 2));
    button->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            Cocos2dExt::BrowserLauncher::launchUrl("http://syanago.com/other/contact.html");
        }
    });

    auto label = Label::createWithTTF("利用規約", FONT_NAME_2, 28);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setPosition(Point(button->getContentSize().width / 2, button->getContentSize().height / 2 - 14));

    button->addChild(label);
    addChild(button);
}

void AgreeUserPolicyDialog::showYesButton()
{
    auto button = ui::Button::create("gacha_dialog_button.png");
    button->addTouchEventListener(CC_CALLBACK_2(AgreeUserPolicyDialog::onTapCloseButton, this));
    button->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    button->setPosition(Vec2(background->getContentSize().width / 2 - 20, 69 / 2 + 40));

    auto label = Label::createWithTTF("同意する", FONT_NAME_2, 28);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    label->setPosition(Vec2(button->getContentSize() / 2) - Vec2(0, 28 / 2));
    button->addChild(label);
    background->addChild(button);
}

void AgreeUserPolicyDialog::showNoButton()
{
    auto button = ui::Button::create("gacha_dialog_button.png");
    button->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            Director::getInstance()->replaceScene(SplashScene::createScene());
        }
    });
    button->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    button->setPosition(Vec2(background->getContentSize().width / 2 + 20, 69 / 2 + 40));

    auto label = Label::createWithTTF("同意しない", FONT_NAME_2, 28);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    label->setPosition(Vec2(button->getContentSize() / 2) - Vec2(0, 28 / 2));
    button->addChild(label);
    background->addChild(button);
}



