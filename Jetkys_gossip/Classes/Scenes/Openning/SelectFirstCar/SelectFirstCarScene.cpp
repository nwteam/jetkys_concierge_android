#include "SelectFirstCarScene.h"
#include "jCommon.h"
#include "MacroDefines.h"
#include "MainScene.h"
#include "PlayerController.h"
#include "FontDefines.h"
#include "BodyTypeModel.h"
#include "ConfirmSelectedCarDialog.h"

SelectFirstCarScene::~SelectFirstCarScene()
{
    int count = (int)_characters.size();
    for (int i = 0; i < count; i++) {
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_ca.plist", _characters.at(i)->getID()).c_str());
    }
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

SelectFirstCarScene::SelectFirstCarScene():
    _progress(nullptr)
    , _request(nullptr)
    , _selectMasterCharacterId(0)
{
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}

Scene* SelectFirstCarScene::createScene()
{
    auto scene = Scene::create();
    scene->addChild(SelectFirstCarScene::create());
    return scene;
}


bool SelectFirstCarScene::init()
{
    if (!Layer::init()) {
        return false;
    }
    _characters = CharacterModel::getFirstChoiceCharacters();

    showBackGroundView();
    showFirstCharacterTableViewList();
    showTelopView();
    return true;
}

void SelectFirstCarScene::showBackGroundView()
{
    Size winSize = Director::getInstance()->getWinSize();

    auto background = Sprite::create("home_bg.png");
    background->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(background, -1);

    auto titleBg = makeSprite("head_line_no1.png");
    titleBg->setPosition(Vec2(titleBg->getContentSize().width / 2,
                              winSize.height - titleBg->getContentSize().height / 2 - 14));
    addChild(titleBg);

    auto title = Label::createWithTTF("車なご選択", FONT_NAME_2, 34);
    title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    title->setAnchorPoint(Vec2(0.0f, 0.5f));
    title->setPosition(Point(14, titleBg->getPosition().y - 17));
    title->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(title);
}

void SelectFirstCarScene::showFirstCharacterTableViewList()
{
    TableView* tableView = TableView::create(this, Size(SIZE_SELECT_CELL.width, SIZE_SELECT_CELL.height * 5));
    tableView->setDirection(ScrollView::Direction::VERTICAL);
    int cellSize = (int)_characters.size();
    tableView->setContentSize(Size(SIZE_SELECT_CELL.width, SIZE_SELECT_CELL.height * cellSize));
    if (tableView->getViewSize().height > SIZE_SELECT_CELL.height * cellSize) {
        tableView->setViewSize(tableView->getContentSize());
    }
    tableView->setAnchorPoint(Point(0.5, 0.5));
    tableView->setPosition(Point(Director::getInstance()->getWinSize() / 2) - Point(SIZE_SELECT_CELL.width / 2, tableView->getContentSize().height / 2));
    tableView->setBounceable(false);
    tableView->setDelegate(this);
    addChild(tableView);
    tableView->reloadData();
}

void SelectFirstCarScene::showTelopView()
{
    auto winSize = Director::getInstance()->getWinSize();

    auto background = makeSprite("telop_bg.png");
    background->setPosition(Point(winSize.width / 2, background->getContentSize().height / 2));
    addChild(background);

    auto reportMess = Label::createWithTTF(PLAYERCONTROLLER->_messages.at(0), FONT_NAME_2, 22);
    reportMess->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    reportMess->setColor(Color3B::WHITE);
    reportMess->setAnchorPoint(Vec2(0, 0.75f));
    reportMess->setPosition(Vec2(winSize.width, background->getContentSize().height / 2));
    reportMess->runAction(RepeatForever::create(Sequence::create(MoveBy::create(TEROP_SPEED, Vec2(-1500, 0)), CallFuncN::create([](Ref* sender) {
        Node* node = static_cast<Node*>(sender);
        node->setPositionX(node->getPositionX() + 1500);
    }), NULL)));

    addChild(reportMess);
}

TableViewCell* SelectFirstCarScene::tableCellAtIndex(TableView* table, ssize_t idx)
{
    TableViewCell* cell = table->dequeueCell();
    cell = new TableViewCell();
    cell->autorelease();
    cell->addChild(createCharacterImage(_characters.at((int)_characters.size() - 1 - idx)->getID()));
    cell->addChild(createStatusBaseImage(cell));
    std::shared_ptr<CharacterModel>characterModel(CharacterModel::find(_characters.at((int)_characters.size() - 1 - idx)->getID()));
    cell->getChildByTag(TAG::STATUS_BASE)->addChild(createCarNameLabel(cell, characterModel->getCarName()));
    cell->getChildByTag(TAG::STATUS_BASE)->addChild(createCharacterOfBodyTypeLabel(cell, BodyTypeModel::find(characterModel->getBodyType())->getName()));
    cell->getChildByTag(TAG::STATUS_BASE)->addChild(createBodyTypeLabel(cell));
    cell->setTag(characterModel->getID());
    return cell;
}

ssize_t SelectFirstCarScene::numberOfCellsInTableView(TableView* table)
{
    return static_cast<int>(_characters.size());
}

Size SelectFirstCarScene::tableCellSizeForIndex(TableView* table, ssize_t idx)
{
    return SIZE_SELECT_CELL;
}

Sprite* SelectFirstCarScene::createCharacterImage(const int masterCharacterId)
{
    auto characterShadow = makeSprite("other_colection_sd_base.png");
    characterShadow->setTag(TAG::CHARACTER_SHADOW);
    characterShadow->setAnchorPoint(Vec2(0.5f, 0.0f));
    characterShadow->setScale(0.5);

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_ca.plist", masterCharacterId).c_str());
    auto characterImage = makeSprite(StringUtils::format("%d_sd.png", masterCharacterId).c_str());
    characterImage->setTag(TAG::CHARACTER_IMAGE);
    characterImage->setAnchorPoint(Vec2(0.5f, 0.0f));
    characterImage->setPosition(Point(characterShadow->getContentSize().width / 2, 10));
    characterImage->setScale(2);
    characterShadow->addChild(characterImage);

    characterShadow->setPosition(Point(SIZE_SELECT_CELL.width / 2 + 10 + characterImage->getContentSize().width / 2 - 10 - characterImage->getContentSize().width / 2 - 140.5,
                                       SIZE_SELECT_CELL.height / 2 - characterImage->getContentSize().height / 2));

    return characterShadow;
}

Sprite* SelectFirstCarScene::createStatusBaseImage(TableViewCell* cell)
{
    auto cellBackground = Sprite::create("maincarselect_status_base.png");
    cellBackground->setTag(TAG::STATUS_BASE);
    cellBackground->setPosition(Point(SIZE_SELECT_CELL.width / 2 + 10 + (cell->getChildByTag(TAG::CHARACTER_SHADOW)->getChildByTag(TAG::CHARACTER_IMAGE))->getContentSize().width / 2,
                                      SIZE_SELECT_CELL.height / 2));
    return cellBackground;
}

Label* SelectFirstCarScene::createCarNameLabel(TableViewCell* cell, std::string carName)
{
    auto carNameLabel = Label::createWithTTF(carName, FONT_NAME_2, 28);
    carNameLabel->setPosition(Point(carNameLabel->getContentSize().width / 2 + 10,
                                    cell->getChildByTag(TAG::STATUS_BASE)->getContentSize().height - carNameLabel->getContentSize().height / 2 - 14));
    carNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    return carNameLabel;
}

Label* SelectFirstCarScene::createCharacterOfBodyTypeLabel(TableViewCell* cell, std::string bodyTypeName)
{
    auto bodyTypeLabel = Label::createWithTTF(bodyTypeName, FONT_NAME_2, 28);
    bodyTypeLabel->setTag(TAG::CHARACTER_BODY_TYPE);
    bodyTypeLabel->setPosition(Point(cell->getChildByTag(TAG::STATUS_BASE)->getContentSize().width - bodyTypeLabel->getContentSize().width / 2 - 10,
                                     bodyTypeLabel->getContentSize().height / 2 - 14));
    bodyTypeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    return bodyTypeLabel;
}

Label* SelectFirstCarScene::createBodyTypeLabel(TableViewCell* cell)
{
    auto bodylabel = Label::createWithTTF("ボディタイプ", FONT_NAME_2, 22);
    bodylabel->setPosition(Point(cell->getChildByTag(TAG::STATUS_BASE)->getContentSize().width - bodylabel->getContentSize().width / 2 - 10, cell->getChildByTag(TAG::STATUS_BASE)->getChildByTag(TAG::CHARACTER_BODY_TYPE)->getPositionY() + 11 + 25));
    bodylabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    return bodylabel;
}

void SelectFirstCarScene::tableCellTouched(TableView* table, TableViewCell* cell)
{
    SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    _selectMasterCharacterId = static_cast<int>(cell->getTag());
    addChild(ConfirmSelectedCarDialog::create(CharacterModel::find(_selectMasterCharacterId), [this]() {
        SoundHelper::shareHelper()->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        _progress->onStart();
        _request->addCharacter(_selectMasterCharacterId, CC_CALLBACK_1(SelectFirstCarScene::onResponseAddCharacter, this));
    }), 10);
}


void SelectFirstCarScene::onResponseAddCharacter(Json* response)
{
    Json* jsonData = Json_getItem(response, "add_character");
    std::string result = Json_getString(jsonData, "result", "false");
    if (result == "true") {
        transitScene(MainScene::createScene());
    }
}
