#include "ConfirmSelectedCarDialog.h"
#include "FontDefines.h"
#include "jCommon.h"

bool ConfirmSelectedCarDialog::init(const std::shared_ptr<CharacterModel>characterModel, const DialogCallback& callback)
{
    if (!DialogAbstract::init("main_car_select_popup_base.png")) {
        return false;
    }
    showTitle();
    showCharacterName(characterModel->getCarName());
    showCharacterStatus(characterModel);
    showLeftButton(callback);
    showRightButton();
    return true;
}

void ConfirmSelectedCarDialog::showTitle()
{
    Size winSize = Director::getInstance()->getWinSize();

    auto label = Label::createWithTTF("この車なごにしますか？", FONT_NAME_2, 32);
    label->setPosition(Point(winSize.width / 2,
                             winSize.height / 2 + background->getContentSize().height / 2 - 30 - 25));
    label->setVerticalAlignment(TextVAlignment::TOP);
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    title = label;
    addChild(label, 1);
}

void ConfirmSelectedCarDialog::showCharacterName(std::string characterName)
{
    Size winSize = Director::getInstance()->getWinSize();

    auto label = Label::createWithTTF(characterName, FONT_NAME_2, 28);
    label->setDimensions(500, 40);
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    label->setPosition(Point(winSize.width / 2,
                             title->getPosition().y  - 5 - title->getContentSize().height / 2));
    label->setColor(COLOR_YELLOW);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    name = label;
    addChild(label, 1);
}

void ConfirmSelectedCarDialog::showCharacterStatus(const std::shared_ptr<CharacterModel>characterModel)
{
    auto exerciseLabel = Label::createWithTTF("馬力 " + FormatWithCommas(characterModel->getBaseExercise()), FONT_NAME_2, 22);
    exerciseLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    exerciseLabel->setAnchorPoint(Point(0, 1));
    exerciseLabel->setPosition(Point(name->getPositionX(), name->getPositionY() - 22 - 14));
    exerciseLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(exerciseLabel);

    auto reactionLabel = Label::createWithTTF("技量 " + FormatWithCommas(characterModel->getBaseReaction()), FONT_NAME_2, 22);
    reactionLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    reactionLabel->setAnchorPoint(Point(0, 1));
    reactionLabel->setPosition(Point(exerciseLabel->getPositionX(), exerciseLabel->getPositionY() - 22));
    reactionLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(reactionLabel);

    auto decisionLabel = Label::createWithTTF("忍耐 " + FormatWithCommas(characterModel->getBaseDecision()), FONT_NAME_2, 22);
    decisionLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    decisionLabel->setAnchorPoint(Point(0, 1));
    decisionLabel->setPosition(Point(reactionLabel->getPositionX(), reactionLabel->getPositionY() - 22));
    decisionLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(decisionLabel);

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon.plist");
    auto icon = makeSprite(StringUtils::format("%d_i.png", characterModel->getID()).c_str());
    icon->setAnchorPoint(Point(1.0f, 0.5f));
    icon->setPosition(Point(title->getPositionX() - 10, reactionLabel->getPosition().y - reactionLabel->getContentSize().height / 2 + 11));
    addChild(icon);
}

void ConfirmSelectedCarDialog::showLeftButton(const DialogCallback& callback)
{
    Size winSize = Director::getInstance()->getWinSize();
    auto button = ui::Button::create("gacha_dialog_button.png");
    button->setPosition(Vec2(winSize.width / 2, winSize.height / 2) + Vec2(-button->getContentSize().width / 2 - 15,
                                                                           -background->getContentSize().height / 2 + 30 + button->getContentSize().height / 2));
    button->addTouchEventListener([callback](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            callback();
        }
    });

    auto label = Label::createWithTTF("はい", FONT_NAME_2, 36);
    label->setPosition(Point(button->getContentSize().width / 2, button->getContentSize().height / 2 - 18));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    button->addChild(label);
    addChild(button);
}

void ConfirmSelectedCarDialog::showRightButton()
{
    Size winSize = Director::getInstance()->getWinSize();
    auto button = ui::Button::create("gacha_dialog_button.png");
    button->setPosition(Vec2(winSize.width / 2, winSize.height / 2) + Vec2(button->getContentSize().width / 2 + 15,
                                                                           -background->getContentSize().height / 2 + 30 + button->getContentSize().height / 2));
    button->addTouchEventListener(CC_CALLBACK_2(ConfirmSelectedCarDialog::onTapCloseButton, this));

    auto label = Label::createWithTTF("いいえ", FONT_NAME_2, 36);
    label->setPosition(Point(button->getContentSize().width / 2, button->getContentSize().height / 2 - 18));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    button->addChild(label);
    addChild(button);
}































