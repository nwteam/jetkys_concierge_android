#ifndef __syanago__SelectFirstCarScene__
#define __syanago__SelectFirstCarScene__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Loading.h"
#include "CharacterModel.h"
#include "SoundHelper.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace SyanagoAPI;

class SelectFirstCarScene : public Layer, public TableViewDataSource, public TableViewDelegate
{
public:
    SelectFirstCarScene();
    virtual ~SelectFirstCarScene();
    
    virtual bool init();
    CREATE_FUNC(SelectFirstCarScene);
    static Scene* createScene();
    
    
    virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view) {};
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {}
    virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
    virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);
    
    
private:
    enum TAG{
        STATUS_BASE = 0,
        CHARACTER_SHADOW,
        CHARACTER_IMAGE,
        CHARACTER_BODY_TYPE
    };
    const cocos2d::Size SIZE_SELECT_CELL = cocos2d::Size(440, 257);
    
    int _syanagoId;
    std::vector<std::shared_ptr<CharacterModel>> _characters;
    DefaultProgress* _progress;
    RequestAPI* _request;
    int _selectMasterCharacterId;
    
    void showBackGroundView();
    void showFirstCharacterTableViewList();
    void showTelopView();
    void onResponseAddCharacter(Json* response);
    
    Sprite* createCharacterImage(const int masterCharacterId);
    Sprite* createStatusBaseImage(TableViewCell* cell);
    Label* createCarNameLabel(TableViewCell* cell, std::string carName);
    Label* createCharacterOfBodyTypeLabel(TableViewCell* cell, std::string bodyTypeName);
    Label* createBodyTypeLabel(TableViewCell* cell);
    
    

};
#endif /* defined(__syanago__SelectFirstCarScene__) */
