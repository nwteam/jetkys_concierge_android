#ifndef __syanago__ConfirmSelectedCarDialog__
#define __syanago__ConfirmSelectedCarDialog__

#include "DialogAbstract.h"
#include "create_func.h"
#include "CharacterModel.h"

class ConfirmSelectedCarDialog : public DialogAbstract, public create_func<ConfirmSelectedCarDialog>
{
public:
    using create_func::create;
    bool init(const std::shared_ptr<CharacterModel> characterModel, const DialogCallback& callback);
private:
    void showTitle();
    void showCharacterName(std::string characterName);
    void showCharacterStatus(const std::shared_ptr<CharacterModel> characterModel);
    void showLeftButton(const DialogCallback& callback);
    void showRightButton();
    Label* title;
    Label* name;
};

#endif /* defined(__syanago__ConfirmSelectedCarDialog__) */
