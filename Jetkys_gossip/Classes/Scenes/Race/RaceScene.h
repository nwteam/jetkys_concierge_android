#ifndef __MainRaceScene_H__
#define __MainRaceScene_H__
#include "create_func.h"
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "SaikoroLayer1.h"
#include "RaceUIControll.h"
#include "SoundHelper.h"
#include "SkillViewController.h"
#include "RaceTutorial.h"
#include "DialogView.h"
#include "HelpPlayer.h"
#include "TeamStatus.h"
#include "MiniMapSprite.h"
#include "StatusManager.h"
#include "ShowRaceMenuButton.h"
#include "ItemButton.h"
#include "ScoreAndTurnLayer.h"
#include "ItemDescription.h"
#include "RaceResultModel.h"

USING_NS_CC;
using namespace SyanagoAPI;

class RaceScene : public cocos2d::Layer, create_func<RaceScene>, public DialogDelegate
{
public:
    ~RaceScene();
    
    using create_func::create;
    
    bool init(const std::shared_ptr<CourseModel>& courseModel,
              Json* courseGrid,
              const std::vector<std::shared_ptr<const CharacterModel>>& playerCharacters,
              TeamStatus* teamStatus,
              HelpPlayer* helpPlayer,
              const std::vector<std::shared_ptr<const CharacterModel>>& enemyCharacters);
    
    //dice
    void tapDiceButton(Ref* pSender);
    void onEndShootDice();
    void turnEnd();
    void turnStart();
    void refreshDiceColor();
    
    //item
    void activateItem(ITEM_TYPE itemId);
    void showItemDescription();
    void disableItemDescription();
    void executeItemEffect();
    
    //skill
    void refreshGameTurn();
    void setActiveSkill();
    void showCriticalDiceEffect();
    void hideCriticalDiceEffect();
    void setCutinName(std::string name, int idImage);
    void selectTargetToStop(int skillID, int turn);
    void stopTarget(int index);
    
    void activateSkillBar();
    
    void showDiceResultUp();
    void showDiceResultDown();
    void hideDiceResultUpDown();
    
    std::string getCutInFailePass();
    void checkRemainningTargetTurn();
    
     //finished
    void endGameWithLose();
    void setGoal(int rank, int totalScore);
    
    MiniMapSprite* _miniMapSprite;
    TurnPurchaseDialog* _turnPurchaseDialog;
    
    RaceStatus::StatusManager* statusManager;
    RaceUIControll *_raceControll;
    ScoreAndTurnLayer* _scoreAndTurnLayer;
private:
    enum TAG_SPRITE {
        START = 0,
        SCORE_AND_TURN
    };
    void initPlist(int characterIds[4], int imageId);
    void showMiniMap(int enemyCharacterCount, float scale);
    void showSaikoroButton();
    void showCurrentScoreAndTurn();
    void showStartSprite();
    
    void startGame(Touch* touch, Event* unused_event);
    
    virtual void endGame(Touch *touch, Event *unused_event);
    
    void showNextTutorial(Touch* touch, Event* unused_event);
    
    void showSkillBar();
    
    void showItemSprites();
    void onTapUseItem() ;
    void onTapNotUseItem();
    
    void createCriticalDiceEffect();
    void showSelectTargetBackground();
    void showGoalSprite();
    void showRaceMenuButton();
    void createItemDescription();
    void createTurnPurchaseDialog();
    
    void checkCriticalDice();
    bool activateCriticalDice();
    void activateActionByDiceValue();
    void activateChangeDiceValue();
    void activatePreCellEffect();
    
    void setTrafficControlEffectToEnemy(bool flag ,int itemID);
    
    std::string getRankImage(int rankId);
    
    int getPlayerCharacterAwayFromGoal();
    std::shared_ptr<RaceResultModel> getRaceResultModel(const int rankOfPlayer);
    
    CC_SYNTHESIZE_READONLY(TeamStatus*, _teamStatus, TeamStatus);
    
    CC_SYNTHESIZE_READONLY(int, _cutinID, CutInId);
    std::string _cutinName;

    HelpPlayer* _helpPlayer;
    std::shared_ptr<const CourseModel> _courseModel;
    
    SkillViewController* _skillBar;
    
    EventListenerTouchOneByOne* _touchEventListener;
    std::vector<Touch*> _touches;
    int _goalIndex;
    
    Size frame;
    
    Sprite* _upResult;
    Sprite* _downResult;
    
    Sprite* _criticalDiceEffect;
    Sprite* _selectAiteSpr;
    Sprite* _goalSprite;
    SaikoroLayer1* _saikoro;
    Sprite* _saikoroBgr;
    
    MenuItemSprite * _saikoroBtn;
    
    //item
    ItemButton* _itemButton;
    ItemDescription* _itemDescription;
    
    Menu* _menu1;

    Label* _saikoroX2Lb;
    
    //skill
    int _skillUse;
    
    
    RaceTutorail* _raceTutorial;
    int _tutorialStage;
    
    
};

#endif /* defined(__MainRaceScene_H__) */
