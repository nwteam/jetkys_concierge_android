#include "RaceScene.h"
#include "CharacterView.h"
#include "jCommon.h"
#include "Define.h"
#include "WinScene.h"
#include "MainScene.h"
#include "LoseScene.h"
#include "FontDefines.h"
#include "CellType.h"
#include "MenuTouch.h"
#include "ItemType.h"
#include "UserDefaultManager.h"
#include "GridConverter.h"
#include "RaceCellRogicModel.h"
#include "PostRaceTutorialScene.h"

#include "ResultCourse.h"
#include "ResultPlayer.h"
#include "ResultClearThings.h"
#include "ResultScore.h"
#include "ResultHoldContent.h"

using namespace cocos2d;
using namespace std;
using namespace RaceStatus;

RaceScene::~RaceScene()
{
    for (RaceStatus::CharacterStatus* enemyStatus : statusManager->enemyStatus) {
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_ca.plist",
                                                                                     enemyStatus->characterModel->getID()));
    }
    for (int i = 0; i < 5; i++) {
        if (statusManager->teamCharacters[i] != nullptr && statusManager->teamCharacters[i]->characterModel != nullptr) {
            SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_ca.plist",
                                                                                         statusManager->teamCharacters[i]->characterModel->getID()));
        }
    }
    SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_ca.plist", statusManager->playerStatus->characterModel->getID()));
    if ((UserDefault::getInstance())->getBoolForKey("Tutorial")) {
        SpriteFrameCache::getInstance()->removeSpriteFrameByName("tu_ca.plist");
    }
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("item_set.plist");
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("icon.plist");
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("doorway.plist");
    SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("cells_%d.plist", _courseModel->getImageId()));

    delete _teamStatus;
    delete _helpPlayer;
    delete statusManager;
    _courseModel = nullptr;

    SpriteFrameCache::getInstance()->removeSpriteFrames();
    Director::getInstance()->getTextureCache()->removeAllTextures();
}

bool RaceScene::init(const shared_ptr<CourseModel>&                              courseModel,
                     Json* courseGrid,
                     const std::vector<std::shared_ptr<const CharacterModel> >& playerCharacters,
                     TeamStatus* teamStatus,
                     HelpPlayer* helpPlayer,
                     const std::vector<std::shared_ptr<const CharacterModel> >& enemyCharacters)
{
    if (!Layer::init()) {
        return false;
    }

    SpriteFrameCache::getInstance()->removeSpriteFrames();
    Director::getInstance()->getTextureCache()->removeAllTextures();
    Director::getInstance()->purgeCachedData();

    int characterIds[4] = { playerCharacters.at(0)->getID(),
                            enemyCharacters.at(0)->getID(),
                            enemyCharacters.at(1)->getID(),
                            enemyCharacters.at(2)->getID() };

    initPlist(characterIds, courseModel->getImageId());

    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::NON_RACE_SCENE_FLAG, true);

    frame = Director::getInstance()->getWinSize();

    _courseModel = courseModel;
    _teamStatus = teamStatus;
    _helpPlayer = helpPlayer;
    _goalIndex = courseGrid->size + START_CELL + 1;

    statusManager = new StatusManager(playerCharacters, helpPlayer, enemyCharacters, teamStatus, courseModel);

    // initialize valiables
    // TODO: move to constructor
    _itemDescription = nullptr;
    _saikoro = NULL;
    _skillBar = nullptr;

    _raceTutorial = NULL;

    showMiniMap(static_cast<int>(enemyCharacters.size()), MAX_DIS_MINIMAP / (58.0 * courseGrid->size));
    showRaceMenuButton();
    showSaikoroButton();
    showCurrentScoreAndTurn();
    showStartSprite();

    // _raceControll = RaceUIControll::create(this, courseModel, GridConverter::convertPreferenceCellToForward(courseGrid));
    _raceControll = RaceUIControll::create(this, courseModel, courseGrid);
    addChild(_raceControll, 1);

    _touchEventListener = EventListenerTouchOneByOne::create();
    _touchEventListener->onTouchBegan = [&](Touch*, Event*) -> bool {
                                            return true;
                                        };
    _touchEventListener->onTouchMoved = [&](Touch*, Event*) {};
    _touchEventListener->onTouchCancelled = [&](Touch*, Event*) {};
    _touchEventListener->onTouchEnded = CC_CALLBACK_2(RaceScene::startGame, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchEventListener, this);

    return true;
}

void RaceScene::initPlist(int characterIds[4], int imageId)
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("doorway.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("item_set.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("item_set.plist");

    if (UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tu_ca.plist");
    }
    for (int i = 0; i < 4; i++) {
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_ca.plist", characterIds[i]));
    }
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("cells_%d.plist", imageId));
}

void RaceScene::showMiniMap(int enemyCharacterCount, float scale)
{
    _miniMapSprite = MiniMapSprite::create(enemyCharacterCount, scale);
    _miniMapSprite->setPosition(Point(frame.width / 4 - 40.0, frame.height * 7 / 8 - 0.0));
    addChild(_miniMapSprite, 5);
}

void RaceScene::showRaceMenuButton()
{
    ShowRaceMenuButton* showRaceMenuButton = ShowRaceMenuButton::create(this, _courseModel);
    showRaceMenuButton->setPosition(Point(frame.width * 3 / 4 + 90.0, frame.height * 7 / 8 + 100.0));
    addChild(showRaceMenuButton, 3);
}

void RaceScene::showSaikoroButton()
{
    _saikoroBtn = makeMenuItem("saikoro.png", CC_CALLBACK_1(RaceScene::tapDiceButton, this));
    _saikoroBtn->setPosition(Point(frame.width / 2 - _saikoroBtn->getContentSize().width / 2 - 10, -frame.height / 2 + 204.0));

    _saikoroBgr = makeSprite("itemBgr.png");
    _saikoroBgr->setVisible(false);
    _saikoroBgr->setOpacity(0);

    _saikoroX2Lb = Label::createWithTTF("X2", FONT_NAME_2, 25);
    _saikoroX2Lb->setVisible(false);
    _saikoroX2Lb->setColor(Color3B::RED);
    _saikoroX2Lb->setPosition(Point(_saikoroBtn->getContentSize().width * 3 / 4 + _saikoroX2Lb->getContentSize().width / 2, _saikoroBtn->getContentSize().height * 3 / 4 + _saikoroX2Lb->getContentSize().height / 2 - 12.5));

    _saikoroBtn->addChild(_saikoroX2Lb);

    if (UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        _raceTutorial = RaceTutorail::create();
        _raceTutorial->ShowRace(1);
        _raceTutorial->setPosition(Point(-frame.width * 0.5 - _saikoroBgr->getPositionX() - _raceTutorial->getContentSize().width / 4 - 40.0, 100.0));
        _saikoroBgr->addChild(_raceTutorial, 1110);
    }
    addChild(_saikoroBgr, 2);

    _menu1 = MenuTouch::create(_saikoroBtn, NULL);
    _menu1->setVisible(false);
    addChild(_menu1, 3);

    _saikoroBgr->setPosition(_saikoroBtn->getPosition() + _menu1->getPosition());

    _upResult = makeSprite("aka_yazirusi.png");
    _upResult->setScale(0.25);
    _upResult->setPosition(Point(_saikoroBgr->getPositionX() - _saikoroBgr->getContentSize().width / 2, _saikoroBgr->getPositionY() + _saikoroBgr->getContentSize().height / 2 - 10.0));
    _upResult->setVisible(false);
    addChild(_upResult, 4);

    _downResult = makeSprite("ao_yazirusi.png");
    _downResult->setScale(0.25);
    _downResult->setPosition(Point(_saikoroBgr->getPositionX() - _saikoroBgr->getContentSize().width / 2, _saikoroBgr->getPositionY() + _saikoroBgr->getContentSize().height / 2 - 10.0));
    _downResult->setVisible(false);
    addChild(_downResult, 4);

    _saikoro = SaikoroLayer1::create(statusManager->teamCharacters[0]->characterModel->getID());
    _saikoro->setPosition(Point(0.0, 0.0));
    _saikoro->_raceScene = this;
    addChild(_saikoro, 5);
}

void RaceScene::showCurrentScoreAndTurn()
{
    GOAL_TARGET_TYPE targetTpe = TARGET_NONE;
    if (_courseModel->isScoreTarget()) {
        statusManager->score->targetScore = _courseModel->getTargetScore();
        targetTpe = TARGET_SCORE;
    } else if (_courseModel->isTurnTarget()) {
        statusManager->sequence->tagerTurn = _courseModel->getMaxNumberOfTurns();
        targetTpe = TARGET_TURN;
    }
    _scoreAndTurnLayer = ScoreAndTurnLayer::create(statusManager, targetTpe);
    _scoreAndTurnLayer->setTag(TAG_SPRITE::SCORE_AND_TURN);
    addChild(_scoreAndTurnLayer, 2);
}

void RaceScene::showStartSprite()
{
    Sprite* sprite = makeSprite("start.png");
    sprite->setTag(TAG_SPRITE::START);
    sprite->setPosition(Point(frame.width / 2, frame.height / 2));
    addChild(sprite, 5);
}

void RaceScene::startGame(Touch* touch, Event* unused_event)
{
    if (_touchEventListener == nullptr) {
        return;
    }
    _eventDispatcher->removeEventListener(_touchEventListener);
    _touchEventListener = nullptr;
    if (!UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        statusManager->view->diceAvailable = true;
        refreshDiceColor();
        statusManager->view->cameraPositionFixed = false;
    } else {
        statusManager->view->diceAvailable = false;
        refreshDiceColor();
        statusManager->view->cameraPositionFixed = true;
        _tutorialStage = 1;
    }
    getChildByTag(TAG_SPRITE::START)->removeFromParent();

    _menu1->setVisible(true);
    _saikoroBgr->setVisible(true);

    showSkillBar();
    showItemSprites();
    createItemDescription();
    createCriticalDiceEffect();
    showSelectTargetBackground();
    showGoalSprite();
    createTurnPurchaseDialog();

    SOUND_HELPER->playEffect(RACE_START, false);
    auto seq = Sequence::create(DelayTime::create(1.5),
                                CallFunc::create([this]() {
        SOUND_HELPER->playRaceSceneBackgroundMusic(CourseModel::find(_courseModel->getID())->getBgmId());
    }), NULL);
    runAction(seq);
    _touchEventListener = EventListenerTouchOneByOne::create();
    _touchEventListener->onTouchBegan = [&](Touch*, Event*) -> bool {
                                            return true;
                                        };
    _touchEventListener->onTouchMoved = [&](Touch*, Event*) {};
    _touchEventListener->onTouchCancelled = [&](Touch*, Event*) {};
    if (UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        _touchEventListener->onTouchEnded = CC_CALLBACK_2(RaceScene::showNextTutorial, this);
    } else {
        _touchEventListener->onTouchEnded = CC_CALLBACK_2(RaceScene::endGame, this);
    }
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchEventListener, this);
}

void RaceScene::createTurnPurchaseDialog()
{
    if (_courseModel->isTurnTarget()) {
        _turnPurchaseDialog = TurnPurchaseDialog::create([this](int additionalTurn) {
            statusManager->sequence->tagerTurn += additionalTurn;
            _scoreAndTurnLayer->refresh();
            _turnPurchaseDialog->setVisible(false);
        }, CC_CALLBACK_0(RaceScene::endGameWithLose, this));
        _turnPurchaseDialog->setVisible(false);
        addChild(_turnPurchaseDialog, 1001);
    }
}

void RaceScene::checkRemainningTargetTurn()
{
    if (_courseModel->isTurnTarget() &&  statusManager->sequence->tagerTurn <= statusManager->sequence->elaplsedTurn) {
        _turnPurchaseDialog->show();
    }
}

void RaceScene::showSkillBar()
{
    _skillBar = SkillViewController::create(this);
    addChild(_skillBar, 10);
}

void RaceScene::createCriticalDiceEffect()
{
    _criticalDiceEffect = makeSprite("bikkuri.png");
    _criticalDiceEffect->setPosition(Point(frame.width / 2, frame.height * 3 / 4));
    _criticalDiceEffect->setScale(1.5);
    _criticalDiceEffect->setVisible(false);
    addChild(_criticalDiceEffect, 15);
}

void RaceScene::showSelectTargetBackground()
{
    _selectAiteSpr = makeSprite("dialog_base.png");
    _selectAiteSpr->setVisible(false);
    addChild(_selectAiteSpr, 2001);
}

void RaceScene::showItemSprites()
{
    _itemButton = ItemButton::create(CC_CALLBACK_0(RaceScene::onTapUseItem, this), CC_CALLBACK_0(RaceScene::onTapNotUseItem, this),
                                     CC_CALLBACK_0(RaceScene::showItemDescription, this), CC_CALLBACK_0(RaceScene::disableItemDescription, this));
    _itemButton->setVisible(false);
    _itemButton->setPosition(Point(_saikoroBgr->getPositionX(), _saikoroBgr->getPositionY() + 125.0));
    addChild(_itemButton, 2);
}

void RaceScene::createItemDescription()
{
    _itemDescription = ItemDescription::create(statusManager->item);
    _itemDescription->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    _itemDescription->setOpacity(0);
    _itemDescription->setPosition(Point(_itemButton->getPosition() + Vec2(-_itemButton->getContentSize().width / 2 - 10, 0)));
    _itemDescription->setCascadeOpacityEnabled(true);
    addChild(_itemDescription, 10101);
}

void RaceScene::onTapUseItem()
{
    if (_skillBar->getDiaLogShow() || statusManager->view->diceRolling == true) {
        return;
    }

    if (!statusManager->view->iconItemTapped) {
        statusManager->view->iconItemTapped = true;
        statusManager->item->itemAvailable = false;
        statusManager->playerStatus->characterView->removeEffectOnTop();
        switch (statusManager->item->currentItem) {
        case 0:
            SOUND_HELPER->playEffect(ITEM_NITORO, false);
            statusManager->playerStatus->characterView->showEffectOnTop("Item1.png");
            statusManager->item->nitoroUsed = true;
            break;

        case 1:
            SOUND_HELPER->playEffect(ITEM_PATOLAMP, false);
            statusManager->item->patolampUsed = true;
            statusManager->playerStatus->characterView->showEffectOnTop("Item2.png");
            break;

        case 3:
            SOUND_HELPER->playEffect(ITEM_TRAFFIC_CONTROL, false);
            statusManager->item->trafficControlUsed = true;
            setTrafficControlEffectToEnemy(true, statusManager->item->currentItem);
            break;

        default:
            break;
        }
    }
}

void RaceScene::onTapNotUseItem()
{
    if (_skillBar->getDiaLogShow() || statusManager->view->diceRolling == true) {
        return;
    }

    if (statusManager->view->iconItemTapped) {
        statusManager->view->iconItemTapped = false;
        statusManager->item->itemAvailable = false;
        switch (statusManager->item->currentItem) {
        case 0:
            statusManager->playerStatus->characterView->removeEffectOnTop();
            statusManager->item->nitoroUsed = false;
            break;

        case 1:
            statusManager->item->patolampUsed = false;
            statusManager->playerStatus->characterView->removeEffectOnTop();
            break;

        case 3:
            statusManager->item->trafficControlUsed = false;
            setTrafficControlEffectToEnemy(false, statusManager->item->currentItem);
            break;

        default:
            break;
        }
    }
}

void RaceScene::setTrafficControlEffectToEnemy(bool flag, int itemID)
{
    for (RaceStatus::CharacterStatus* enemyStatus : statusManager->enemyStatus) {
        if (enemyStatus->characterView->getCharacterColumn() - statusManager->playerStatus->characterView->getCharacterColumn() > 0 &&
            enemyStatus->characterView->getCharacterColumn() - statusManager->playerStatus->characterView->getCharacterColumn() <= 5) {
            enemyStatus->characterView->removeEffectOnTop();
            if (flag) {
                enemyStatus->characterView->showEffectOnTop("Item4.png");
            }
        }
    }
}

// アイテムの説明文の表示と非表示
// TODO: remove comment and move method
void RaceScene::showItemDescription()
{
    if (statusManager->view->itemDescriptionActive == true) {
        return;
    }
    statusManager->view->itemDescriptionActive = true;
    _itemDescription->setVisible(true);
    _itemDescription->refresh();
    _itemDescription->runAction(FadeIn::create(0.2f));
}

void RaceScene::disableItemDescription()
{
    if (statusManager->view->itemDescriptionActive == false) {
        return;
    }
    statusManager->view->itemDescriptionActive = false;
    _itemDescription->runAction(Sequence::create(FadeOut::create(0.2), CallFunc::create([this]() {
        _itemDescription->setVisible(false);
    }), NULL));
}

void RaceScene::showGoalSprite()
{
    _goalSprite = makeSprite("goal.png");
    _goalSprite->setPosition(Point(frame.width / 2, frame.height / 2));
    _goalSprite->setVisible(false);
    addChild(_goalSprite, 5);
}

void RaceScene::endGame(Touch* touch, Event* unused_event)
{
    if (statusManager->sequence->finish == false) {
        return;
    }
    if (_courseModel->getCourseType() == 4) {
        transitScene(MainScene::createScene());
    } else {
        Scene* result = Scene::create();
        result->addChild(WinScene::create(getRaceResultModel(_raceControll->getCharacterRank())));
        transitScene(result);
    }
}

int RaceScene::getPlayerCharacterAwayFromGoal()
{
    const int result = statusManager->playerStatus->characterView->getCharacterColumn() - _goalIndex;
    if (result < 1) {
        return 0;
    }
    return result;
}

using namespace ResultContent;
using namespace ResultPlayerContent;

std::shared_ptr<RaceResultModel>RaceScene::getRaceResultModel(const int rankOfPlayer)
{
    std::shared_ptr<ResultClearThings>resultClearThings(new ResultClearThings(rankOfPlayer,
                                                                              statusManager->sequence->elaplsedTurn,
                                                                              getPlayerCharacterAwayFromGoal(),
                                                                              statusManager->score->consecutiveStopOnMeritCell,
                                                                              statusManager->playerStatus->usedSkillCount,
                                                                              statusManager->playerStatus->stoppedGridIds));

    std::shared_ptr<ResultHoldContent>resultHoldContent(new ResultHoldContent(statusManager->item->itemAvailable ? 1 : 0,
                                                                              _skillBar->getCountOfChargedSkill()));

    std::shared_ptr<ResultScore>resultScore(new ResultScore(statusManager->score->bonusScore));

    std::shared_ptr<ResultPlayer>resultPlayer(new ResultPlayer(resultClearThings,
                                                               resultScore,
                                                               resultHoldContent));

    std::shared_ptr<ResultCourse>resultCourse(new ResultCourse(CourseModel::find(_courseModel->getID()),
                                                               UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::IS_EVENT_RACE, false),
                                                               _raceControll->getMasterCharacterIdsOfRank()));

    std::shared_ptr<HelpPlayer>helpPlayer(new HelpPlayer(_helpPlayer->getId(),
                                                         _helpPlayer->getCharacterId(),
                                                         _helpPlayer->getIsFriend(),
                                                         _helpPlayer->getName(),
                                                         _helpPlayer->getRank(),
                                                         _helpPlayer->getLevel()));

    std::shared_ptr<RaceResultModel>model(new RaceResultModel(resultCourse, resultPlayer, helpPlayer));
    return model;
}

void RaceScene::tapDiceButton(Ref* pSender)
{
    if (statusManager->view->diceAvailable == false ||
        statusManager->view->diceRolling == true ||
        statusManager->view->itemDescriptionActive == true ||
        _skillBar->getDiaLogShow() ||
        _skillBar->isDesShowing()
        ) {
        return;
    }
    if (_turnPurchaseDialog) {
        if (_turnPurchaseDialog->isVisible() == true) {
            return;
        }
    }

    if (_tutorialStage == 24) {
        _raceTutorial->removeFromParent();
        _tutorialStage = 0;
    }

    if (statusManager->skill->myPaceFlag == false) {
        statusManager->skill->myPaceSkipCellFlag = false;
    }

    if (statusManager->view->iconItemTapped == true) {
        _itemButton->useItem();
        _itemButton->setVisible(false);
        executeItemEffect();
        statusManager->view->iconItemTapped = false;
    }

    statusManager->playerStatus->characterView->removeEffectOnTop();
    _saikoro->shootDice();
}

void RaceScene::onEndShootDice()
{
    if (activateCriticalDice()) {
        return;
    }

    if (statusManager->playerStatus->slowDownValue != 0 ||
        statusManager->dice->buff != 0 ||
        statusManager->skill->plusDiceValue != 0) {
        _raceControll->setIsNomalRunSound(false);
    } else {
        _raceControll->setIsNomalRunSound(true);
    }

    if (statusManager->sequence->shootDiceAgain == false) {
        statusManager->sequence->elaplsedTurn++;
        refreshGameTurn();
    }

    statusManager->resetChainCounts();

    activateChangeDiceValue();
    activatePreCellEffect();
    statusManager->dice->diceValue = 0;
    statusManager->skill->deleteDemeritCellActivated = false;

    _raceControll->checkCamera(CC_CALLBACK_0(RaceUIControll::moveEachCharacters, _raceControll));
}

void RaceScene::activateChangeDiceValue()
{
    // item
    statusManager->playerStatus->moveDistance += statusManager->dice->buff;
    statusManager->dice->buff = 0;

    // skill
    if (statusManager->skill->remainningTurn > 0) {
        // プラスダイス
        if (statusManager->skill->plusDiceValue != 0) {
            statusManager->skill->remainningTurn--;
            statusManager->playerStatus->moveDistance += statusManager->skill->plusDiceValue;
            if (statusManager->skill->remainningTurn < 0) {
                statusManager->skill->remainningTurn = 0;
            }
            if (statusManager->skill->remainningTurn == 0) {
                statusManager->skill->plusDiceValue = 0;
            }
            _skillBar->activeSkillBar();
            _skillBar->refreshSkillButtons();
        }
    }

    // leader skill
    // LEDER_SKILL_KIND_DICE_RESULT_UP
    if (statusManager->leaderSkill->diceResultUpAtProbavilityFlag) {
        int hight = arc4random() % 100;
        if (hight < statusManager->leaderSkill->diceResultUpProbability) {
            statusManager->playerStatus->moveDistance *= statusManager->leaderSkill->diceResultUpMagnification;
            _raceControll->setViewDiceDoubleValueResultEfect(statusManager->leaderSkill->diceResultUpMagnification);
        }
    }

    if (statusManager->dice->buff == 0 &&
        statusManager->skill->plusDiceValue == 0 &&
        statusManager->playerStatus->slowDownValue == 0) {
        _upResult->setVisible(false);
        _downResult->setVisible(false);
    }
}

void RaceScene::activatePreCellEffect()
{
    if (statusManager->playerStatus->remainningStopTurn > 0) {
        // 止まれ
        statusManager->playerStatus->moveDistance = 0;
        statusManager->playerStatus->remainningStopTurn--;
    } else if (statusManager->playerStatus->slowDownValue != 0) {
        // カーブ
        statusManager->playerStatus->moveDistance += statusManager->playerStatus->slowDownValue;
        if (statusManager->playerStatus->moveDistance < 1) {
            statusManager->playerStatus->moveDistance = 1;
        }
        statusManager->playerStatus->slowDownValue = 0;
    } else if (statusManager->playerStatus->mustLimitSpeed) {
        // 徐行
        if (statusManager->dice->diceValue == 5 || statusManager->dice->diceValue == 6) {
            if (statusManager->skill->deleteDemeritCell) {
                _raceControll->setViewDeleteDemeritCellEfect(1);
                statusManager->playerStatus->mustLimitSpeed = false;
            } else {
                SOUND_HELPER->playCharacterVoiceEfect(statusManager->teamCharacters[0]->characterModel->getID(), 24);
                statusManager->playerStatus->moveDistance = 0;
            }
        } else {
            statusManager->playerStatus->mustLimitSpeed = false;
        }
    } else if (statusManager->playerStatus->mustHighSpeed) {
        // 高速
        if (statusManager->playerStatus->moveDistance == 1 ||
            statusManager->playerStatus->moveDistance == 2) {
            SOUND_HELPER->playCharacterVoiceEfect(statusManager->teamCharacters[0]->characterModel->getID(), 24);
            statusManager->playerStatus->moveDistance = 0;
        } else {
            statusManager->playerStatus->mustHighSpeed = false;
        }
    } else if (statusManager->playerStatus->downwardSlopeType != 0) {
        //下り坂
        if (statusManager->skill->deleteDemeritCellActivated == false) {
            statusManager->showCellEffect(statusManager->playerStatus->downwardSlopeType);
            statusManager->playerStatus->moveDistance = RaceCellRogicModel::cellRogicDownSlope(statusManager->playerStatus->moveDistance, statusManager->teamStatus->getBrake(), statusManager->playerStatus->downwardSlopeType, _courseModel->getOperandId());
        }
        statusManager->playerStatus->downwardSlopeType = NONE;
    } else if (statusManager->playerStatus->upwardSlopeType != 0) {
        //上り坂
        if (statusManager->skill->deleteDemeritCellActivated == false) {
            statusManager->showCellEffect(statusManager->playerStatus->upwardSlopeType);
            statusManager->playerStatus->moveDistance = RaceCellRogicModel::cellRogicUpSlope(statusManager->playerStatus->moveDistance, statusManager->teamStatus->getPower(), statusManager->playerStatus->upwardSlopeType, _courseModel->getOperandId());
        }
        statusManager->playerStatus->upwardSlopeType = NONE;
    }
}

void RaceScene::showDiceResultUp()
{
    _upResult->setVisible(true);
    _downResult->setVisible(false);
}

void RaceScene::showDiceResultDown()
{
    _upResult->setVisible(false);
    _downResult->setVisible(true);
}

void RaceScene::hideDiceResultUpDown()
{
    _upResult->setVisible(false);
    _downResult->setVisible(false);
}

bool RaceScene::activateCriticalDice()
{
    if (statusManager->skill->criticalDiceFlag &&
        statusManager->playerStatus->moveDistance != 0) {
        statusManager->skill->remainningTurn = 0;
        _skillBar->refreshSkillButtons();
        if (statusManager->playerStatus->moveDistance == statusManager->skill->criticalDiceTargetValue) {
            statusManager->skill->criticalDiceResult += statusManager->playerStatus->moveDistance;
            statusManager->view->diceRolling = true;

            SOUND_HELPER->playEffect(DICE_REPLAY_EFFECT, false);

            showCriticalDiceEffect();

            _saikoro->setSaikoroRateSpeed();
            _saikoro->shootDice();
            return true;
        }
        _saikoro->resetSaikoroRateSpeed();
        statusManager->skill->criticalDiceTargetValue = 0;
        statusManager->skill->criticalDiceFlag = false;
    }
    if (statusManager->skill->criticalDiceResult > 0) {
        statusManager->playerStatus->moveDistance += statusManager->skill->criticalDiceResult;
        statusManager->skill->criticalDiceResult = 0;
    }
    return false;
}

void RaceScene::showCriticalDiceEffect()
{
    if (!_criticalDiceEffect->isVisible()) {
        _criticalDiceEffect->setVisible(true);
        _criticalDiceEffect->runAction(RepeatForever::create(Sequence::create(
                                                                 ScaleTo::create(0.3, 2.25),
                                                                 ScaleTo::create(0.3, 1.5),
                                                                 NULL)));
    }
}

void RaceScene::hideCriticalDiceEffect()
{
    if (_criticalDiceEffect->isVisible()) {
        _criticalDiceEffect->stopAllActions();
        _criticalDiceEffect->setVisible(false);
        _criticalDiceEffect->setScale(1.5);
    }
}

void RaceScene::selectTargetToStop(int skillID, int turn)
{
    _skillUse = skillID;
    statusManager->view->pause = true;
    statusManager->view->diceAvailable = false;
    refreshDiceColor();

    _selectAiteSpr->setVisible(true);
    _selectAiteSpr->setPosition(Point(frame.width / 2, frame.height / 2));

    auto title = Label::createWithTTF("どの車なごを休みにしますか？", FONT_NAME_2, 30);
    title->setPosition(Point(_selectAiteSpr->getContentSize().width / 2, _selectAiteSpr->getContentSize().height - title->getContentSize().height * 1.5 + 20));
    title->enableShadow();
    _selectAiteSpr->addChild(title, 2);

    MenuItem* targetSDMenus[3] = { nullptr, nullptr, nullptr };
    Sprite* targetSDSprites[3] = { nullptr, nullptr, nullptr };
    for (int i = 0; i < 3; i++) {
        if (statusManager->enemyStatus[i]->characterModel->getID() != 0) {
            SpriteFrameCache::getInstance()->addSpriteFramesWithFile(
                StringUtils::format("%d_ca.plist", statusManager->enemyStatus[i]->characterModel->getID()));
            Vec2 position = Vec2(Point(90, 30));
            Vec2 anchorPoint = Vec2::ANCHOR_MIDDLE_BOTTOM;
            switch (i) {
            case 0:
                position.x = 10;
                anchorPoint = Vec2::ANCHOR_BOTTOM_LEFT;
                break;

            case 1:
                position.x = _selectAiteSpr->getContentSize().width / 2;
                anchorPoint = Vec2::ANCHOR_MIDDLE_BOTTOM;
                break;

            case 2:
                position.x = _selectAiteSpr->getContentSize().width - 10;
                anchorPoint = Vec2::ANCHOR_BOTTOM_RIGHT;
                break;
            }
            targetSDSprites[i] = makeSprite(StringUtils::format("%d_sd.png", statusManager->enemyStatus[i]->characterModel->getID()).c_str());
            targetSDSprites[i]->setAnchorPoint(anchorPoint);
            targetSDSprites[i]->setColor(Color3B(10, 10, 10));
            targetSDSprites[i]->setPosition(position);

            targetSDMenus[i] = makeMenuItem(StringUtils::format("%d_sd.png", statusManager->enemyStatus[i]->characterModel->getID()).c_str(),
                                            CC_CALLBACK_0(RaceScene::stopTarget, this, i));
            targetSDMenus[i]->setScale(1);
            targetSDMenus[i]->setAnchorPoint(anchorPoint);
            targetSDMenus[i]->setPosition(position);
        }
    }

    // fix image position
    if (targetSDSprites[0]->getContentSize().width != targetSDSprites[2]->getContentSize().width ||
        targetSDMenus[0]->getContentSize().width != targetSDMenus[2]->getContentSize().width) {
        const float deltaWidth = abs((targetSDSprites[0]->getContentSize().width - targetSDSprites[2]->getContentSize().width) / 2);
        if (targetSDSprites[0]->getContentSize().width > targetSDSprites[2]->getContentSize().width) {
            targetSDSprites[2]->setPositionX(targetSDSprites[2]->getPositionX() - deltaWidth);
            targetSDMenus[2]->setPositionX(targetSDMenus[2]->getPositionX() - deltaWidth);
        } else {
            targetSDSprites[0]->setPositionX(targetSDSprites[0]->getPositionX() + deltaWidth);
            targetSDMenus[0]->setPositionX(targetSDMenus[0]->getPositionX() + deltaWidth);
        }
    }

    std::queue<MenuItem*>notGoalEnemies;
    for (int i = 0; i < 3; i++) {
        if (statusManager->enemyStatus[i]->characterView->getCharacterColumn() <= _goalIndex) {
            notGoalEnemies.push(targetSDMenus[i]);
        } else {
            _selectAiteSpr->addChild(targetSDSprites[i]);
        }
    }
    MenuTouch* menu = MenuTouch::create(notGoalEnemies.front(), NULL);
    menu->setPosition(Point(0.0, 0.0));
    notGoalEnemies.pop();
    while (!notGoalEnemies.empty()) {
        menu->addChild(notGoalEnemies.front());
        notGoalEnemies.pop();
    }
    _selectAiteSpr->addChild(menu);
}

void RaceScene::stopTarget(int index)
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_ca.plist", statusManager->enemyStatus[index]->characterModel->getID()));
    int remainningStopTurn = SkillModel::find(_skillUse)->getValue1();
    statusManager->enemyStatus[index]->remainningBlockTurn = remainningStopTurn;
    statusManager->skill->forceStop = true;

    statusManager->view->pause = false;
    statusManager->view->diceAvailable = true;
    refreshDiceColor();

    statusManager->enemyStatus[index]->characterView->removeEffectOnTop();
    statusManager->enemyStatus[index]->characterView->showEffectOnTop("Item4.png");

    _selectAiteSpr->setVisible(false);

    statusManager->skill->remainningTurn = 0;
    _skillBar->refreshSkillButtons();
}

// Create Item
void RaceScene::activateItem(ITEM_TYPE itemId)
{
    if (statusManager->item->itemAvailable) {
        return;
    }
    statusManager->item->itemAvailable = true;
    statusManager->item->currentItem = itemId;
    if (UserDefault::getInstance()->getBoolForKey("Tutorial", false)) {
        statusManager->item->currentItem = ITEM_NO_NITORO;
    }
    if (_menu1->isVisible() && statusManager->item->currentItem < 5) {
        _itemButton->setVisible(true);
        _itemButton->setItem(statusManager->item->currentItem);
    } else {
        _itemButton->setVisible(false);
    }
}

// used by SkillViewController
void RaceScene::setCutinName(std::string name, int idImage)
{
    _cutinName = name;
    _cutinID = idImage;
}

// used by SkillViewController
std::string RaceScene::getCutInFailePass()
{
    return _cutinName;
}

void RaceScene::executeItemEffect()
{
    switch (statusManager->item->currentItem) {
    case ITEM_NO_NITORO:
        statusManager->dice->buff = 6;
        break;

    case ITEM_NO_PATORANP:
        statusManager->dice->buff = 1;
        statusManager->item->patolampActivated = true;
        break;

    case ITEM_NO_TRAFFIC_CONTROL:
        statusManager->dice->buff = 0;
        statusManager->item->trafficControlActivated = true;
        break;

    default:
        break;
    }
    statusManager->item->currentItem = ITEM_NO_NON_ITEM;
}

void RaceScene::refreshDiceColor()
{
    _saikoroBtn->setColor((statusManager->view->diceAvailable) ? Color3B::WHITE : Color3B::GRAY);
}

void RaceScene::turnStart()
{
    _skillBar->setEnableToButtons(false);
    _raceControll->setVisibleToCharacterNameLabels(false);
    statusManager->view->diceAvailable = false;
    refreshDiceColor();
}

void RaceScene::turnEnd()
{
    // check race ended
    if (statusManager->sequence->finish) {
        return;
    }
    if (_raceControll->isPlayerGoal()) {
        statusManager->view->cameraPositionFixed = false;
        _raceControll->finishRace();
        return;
    }
    checkRemainningTargetTurn();

    statusManager->view->pause = false;
    statusManager->view->cameraPositionFixed = false;
    // activate buttons
    _skillBar->setEnableToButtons(true);
    _raceControll->setVisibleToCharacterNameLabels(true);
    statusManager->view->diceAvailable = true;
    refreshDiceColor();

    // TODO: remove this flag check
    if (statusManager->skill->deleteDemeritCell) {
        statusManager->skill->remainningTurn--;
        if (statusManager->skill->remainningTurn == 0) {
            statusManager->skill->deleteDemeritCell = false;
        }
    }
    statusManager->skill->myPaceSkipCellFlag = false;
    statusManager->skill->movePlayerCharacterDistance = 0;
    _skillBar->refreshSkillButtons();
}

void RaceScene::setGoal(int rank, int totalScore)
{
    if (_courseModel->isScoreTarget() && totalScore < statusManager->score->targetScore) {
        endGameWithLose();
    } else {
        if (rank != 4) {
            SOUND_HELPER->playEffect(GOAL_FANFARE, false);
        } else {
            SOUND_HELPER->playEffect(LOSE_EFFECT, false);
        }
        _goalSprite->setVisible(true);

        statusManager->view->cameraPositionFixed = true;
        statusManager->view->diceAvailable = false;
        _saikoroBgr->setVisible(false);
        _upResult->setVisible(false);
        _downResult->setVisible(false);
        _saikoroBtn->setVisible(false);
        _skillBar->setVisible(false);
        // transitScene on next tap
    }
}

void RaceScene::refreshGameTurn()
{
    _skillBar->changeDeltaDelay(1);
    _skillBar->activeSkillBar();
    _scoreAndTurnLayer->refresh();
}

// lose as player not reached target turn or score
void RaceScene::endGameWithLose()
{
    if (_courseModel->getCourseType() == 4) {
        transitScene(MainScene::createScene());
        return;
    }

    Scene* result = Scene::create();
    result->addChild(LoseScene::create(getRaceResultModel(4)));
    transitScene(result);
}

std::string RaceScene::getRankImage(int rankId)
{
    std::string name;
    if (rankId == statusManager->teamCharacters[0]->characterModel->getID()) {
        name = "Character0.png";
    } else if (rankId == statusManager->enemyStatus[0]->characterModel->getID()) {
        name = "clone0.png";
    } else if (rankId == statusManager->enemyStatus[1]->characterModel->getID()) {
        name = "clone10.png";
    } else if (rankId == statusManager->enemyStatus[2]->characterModel->getID()) {
        name = "clone0.png";
    }
    return name;
}

void RaceScene::activateSkillBar()
{
    _skillBar->activeSkillBar();
}

void RaceScene::showNextTutorial(Touch* touch, Event* unused_event)
{
    if (_tutorialStage == 1) {
        _tutorialStage = 2;
        _raceTutorial->removeFromParent();
        _raceTutorial = RaceTutorail::create();
        _raceTutorial->ShowSkill(1);
        _raceTutorial->setPosition(Point(-105.0, frame.height / 2 - 60));
        _skillBar->addChild(_raceTutorial, 10);
    } else if (_tutorialStage == 2) {
        _tutorialStage = 3;
        _raceTutorial->ShowSkill(2);
    } else if (_tutorialStage == 3) {
        _tutorialStage = 4;
        _raceTutorial->ShowSkill(3);
    } else if (_tutorialStage == 4) {
        _tutorialStage = 5;
        _raceTutorial->removeFromParent();
        _raceTutorial = RaceTutorail::create();
        _raceTutorial->ShowMap(1);
        _raceTutorial->setPosition(Point(-105.0, -500.0));
        _miniMapSprite->addChild(_raceTutorial, 10);
    } else if (_tutorialStage == 5) {
        _tutorialStage = 6;
        _raceTutorial->ShowMap(2);
    } else if (_tutorialStage == 6) {
        _tutorialStage = 7;
        _raceTutorial->removeFromParent();
        _raceTutorial = RaceTutorail::create();
        _raceTutorial->ShowMenu(1);
        _raceTutorial->setPosition(Point(frame.width * 0.25 - 50, frame.height * 0.5));
        addChild(_raceTutorial, 100);
    } else if (_tutorialStage == 7) {
        _tutorialStage = 8;
        _raceTutorial->ShowMenu(2);
    } else if (_tutorialStage == 8) {
        _tutorialStage = 9;
        _raceTutorial->removeFromParent();
        _raceTutorial = RaceTutorail::create();
        _raceTutorial->ShowMasu(1);
        _raceTutorial->setPosition(Point(0.0, 240.0));
        addChild(_raceTutorial, 100);
    } else if (_tutorialStage == 9) {
        _tutorialStage = 10;
        _raceTutorial->ShowMasu(2);
    } else if (_tutorialStage == 10) {
        _tutorialStage = 11;
        _raceTutorial->removeFromParent();
        _raceTutorial = RaceTutorail::create();
        _raceTutorial->ShowMasuEffect(1);
        _raceTutorial->setPosition(Point(-10.0, -400.0));
        _miniMapSprite->addChild(_raceTutorial, 10);
    } else if (_tutorialStage == 11) {
        _tutorialStage = 12;
        _raceTutorial->ShowMasuEffect(2);
    } else if (_tutorialStage == 12) {
        _tutorialStage = 13;
        _raceTutorial->ShowMasuEffect(3);
    } else if (_tutorialStage == 13) {
        _tutorialStage = 14;
        _raceTutorial->ShowMasuEffect(4);
    } else if (_tutorialStage == 14) {
        _tutorialStage = 15;
        _raceTutorial->ShowMasuEffect(5);
    } else if (_tutorialStage == 15) {
        _tutorialStage = 16;
        _raceTutorial->ShowMasuEffect(6);
    } else if (_tutorialStage == 16) {
        _tutorialStage = 17;
        _raceTutorial->ShowMasuEffect(7);
    } else if (_tutorialStage == 17) {
        _tutorialStage = 18;
        _raceTutorial->ShowMasuEffect(8);
    } else if (_tutorialStage == 18) {
        _tutorialStage = 19;
        _raceTutorial->ShowMasuEffect(9);
    } else if (_tutorialStage == 19) {
        _tutorialStage = 20;
        _raceTutorial->removeFromParent();
        _raceTutorial = RaceTutorail::create();
        _raceTutorial->ShowSaikoro(1);
        _raceTutorial->setPosition(Point(-430.0, -50.0));
        _raceTutorial->setVisible(true);
        _saikoroBgr->addChild(_raceTutorial, 10);
    } else if (_tutorialStage == 20) {
        _tutorialStage = 21;
        _raceTutorial->ShowSaikoro(2);
    } else if (_tutorialStage == 21) {
        _tutorialStage = 22;
        _raceTutorial->removeFromParent();
        _raceTutorial = RaceTutorail::create();
        _raceTutorial->Showitem(1);
        _raceTutorial->setPosition(Point(-430.0, 70.0));
        _raceTutorial->setVisible(true);
        _saikoroBgr->addChild(_raceTutorial, 10);
    } else if (_tutorialStage == 22) {
        _tutorialStage = 23;
        _raceTutorial->removeFromParent();
        _raceTutorial = RaceTutorail::create();
        _raceTutorial->ShowSaikoro(3);
        _raceTutorial->setPosition(Point(-430.0, -50.0));
        _raceTutorial->setVisible(true);
        _saikoroBgr->addChild(_raceTutorial, 10);
    } else if (_tutorialStage == 23) {
        _tutorialStage = 24;
        _raceTutorial->removeFromParent();
        statusManager->view->diceAvailable = true;
        refreshDiceColor();
        statusManager->view->cameraPositionFixed = false;
        _raceTutorial = RaceTutorail::create();
        _raceTutorial->ShowTouchSaikoro();
        _raceTutorial->setPosition(Point(-430.0, -50.0));
        _raceTutorial->setVisible(true);
        _saikoroBgr->addChild(_raceTutorial, 10);
    }

    if (statusManager->sequence->finish == true) {
        transitScene(PostRaceTutorialScene::createScene());
    }
}
