#include "RaceUIControll.h"
#include "RaceScene.h"
#include "jCommon.h"
#include "Define.h"
#include "PlayerController.h"
#include "FontDefines.h"
#include "CellType.h"
#include "EnemyDiceValueLogicModel.h"
#include "RaceCellRogicModel.h"
#include "ItemType.h"
#include "CharacterEfectView.h"
#include "RunningPlayerCharacter.h"
#include "RunningEnemyCharacter.h"
#include <functional>

USING_NS_CC;
using namespace std;

RaceUIControll::RaceUIControll():
    _moveSoundId(-1)
    , _isNomalRunSound(true)
    , _previousPlayerColumnIndex(0)
    , _winSize(Director::getInstance()->getWinSize())
{
    _enemyAnimationKeys = {
        ANIMATION_AITE_2,
        ANIMATION_AITE_3,
        ANIMATION_AITE_4
    };
    _masterCharacterIdsOfRank = {
        -1,
        -1,
        -1,
        -1
    };
}

RaceUIControll::~RaceUIControll() {}

bool RaceUIControll::init(RaceScene* raceScene, const std::shared_ptr<CourseModel>& courseModel, Json* courseGrid)
{
    if (!Layer::init()) {
        return false;
    }

    _raceScene = raceScene;
    _courseModel = courseModel;
    playerCharacterId = raceScene->statusManager->playerStatus->characterModel->getID();
    GOAL_INDEX = courseGrid->size + START_CELL + 1;

    showCourseBackground();
    showCourseCell(courseGrid);
    showPlayerCharacter();
    showEnemyCharacters();
    showCutinBackGround();

    createRunAnimation();

    setTapEvent();
    setScale(1);
    return true;
}

void RaceUIControll::showCourseBackground()
{
    _raceBackgroundManager = RaceBackgroundManager::create(_courseModel->getImageId());
    addChild(_raceBackgroundManager, 0);
}

void RaceUIControll::showCourseCell(Json* courseGrid)
{
    _cellView = CellViewController::create(courseGrid);
    _raceBackgroundManager->addChild(_cellView, 2);

    const int offset = _cellView->getPositionX() + _raceBackgroundManager->getPositionX() + getPositionX() / getScale();
    _minDrag = _cellView->_cell[3][START_CELL]->getPositionX() + offset;
    _maxDrag = _cellView->_cell[3][GOAL_INDEX]->getPositionX() + offset;
}

void RaceUIControll::createRunAnimation()
{
    auto animation = Animation::create();
    for (int i = 1; i <= 4; i++) {
        animation->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(StringUtils::format("%d_sd%d.png", playerCharacterId, i)));
    }
    animation->setDelayPerUnit(0.1f);
    animation->setLoops(3);
    animation->setRestoreOriginalFrame(true);
    AnimationCache::getInstance()->addAnimation(animation, RunningPlayerCharacter::KEY);
}

void RaceUIControll::showPlayerCharacter()
{
    _raceScene->statusManager->playerStatus->characterView->setPosition(_cellView->_cell[3][START_CELL]->getPosition() + _raceBackgroundManager->getPosition() + _cellView->getPosition());
    addChild(_raceScene->statusManager->playerStatus->characterView, SPRITE_Z_ODER::PLAYER_CHARACTER);
}

void RaceUIControll::showCutinBackGround()
{
    _cutinBgr = makeSprite("black_image.png");
    _cutinBgr->setVisible(false);
    _cutinBgr->setScale(3.0);
    _cutinBgr->setOpacity(180);
    _cutinBgr->setPosition(Point(_winSize.width / 2, _winSize.height / 2));
    addChild(_cutinBgr, 1010);
}

void RaceUIControll::showEnemyCharacters()
{
    for (int i = 0; i < 3; i++) {
        _raceScene->statusManager->enemyStatus[i]->characterView->setPosition(_cellView->_cell[i][START_CELL]->getPosition() + _raceBackgroundManager->getPosition() + _cellView->getPosition());
        AnimationCache::getInstance()->addAnimation(createEnemyAnimation(_raceScene->statusManager->enemyStatus[i]->characterModel->getID()), _enemyAnimationKeys.at(i));
        addChild(_raceScene->statusManager->enemyStatus[i]->characterView, SPRITE_Z_ODER::ENEMY_CHARACTER_BASEVALUE + i);
    }
}

Animation* RaceUIControll::createEnemyAnimation(int enemyCharacterId)
{
    auto result = Animation::create();
    for (int i = 1; i <= 4; i++) {
        std::string spriteName;
        if (UserDefault::getInstance()->getBoolForKey("Tutorial")) {
            spriteName = StringUtils::format("tu_sd%d.png", i);
        } else {
            spriteName = StringUtils::format("%d_sd%d.png", enemyCharacterId, i);
        }
        result->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(spriteName));
    }
    result->setDelayPerUnit(0.1f);
    result->setLoops(3);
    result->setRestoreOriginalFrame(true);

    return result;
}

void RaceUIControll::setTapEvent()
{
    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(RaceUIControll::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(RaceUIControll::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(RaceUIControll::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(RaceUIControll::onTouchCancelled, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

bool RaceUIControll::onTouchBegan(Touch* touch, Event* unused_event)
{
    if (_raceScene->statusManager->view->cameraPositionFixed || UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        return false;
    }

    _touchLen = 0.0;
    _touchLenDelta = 0.0;

    if (_touches.size() > 2) {
        return true;
    }

    if (std::find(_touches.begin(), _touches.end(), touch) == _touches.end()) {
        _touches.push_back(touch);
    }

    if (_touches.size() == 1) {
        _movePointOld = convertTouchToNodeSpace(touch).x;
    } else if (_touches.size() == 2) {
        _positionTemp = getPosition() / getScale();
        _touchLen = convertTouchToNodeSpace(_touches[0]).getDistance(convertTouchToNodeSpace(_touches[1]));
        _touchLenDelta = _touchLen;
    }
    return true;
}

void RaceUIControll::onTouchMoved(Touch* touch, Event* unused_event)
{
    if (_raceScene->statusManager->view->pause == true ||
        _raceScene->statusManager->view->cameraPositionFixed ||
        UserDefault::getInstance()->getBoolForKey("Tutorial") ||
        std::find(_touches.begin(), _touches.end(), touch) == _touches.end()) {
        return;
    }
    if (_touches.size() == 1) {
        const float deltaMove = convertTouchToNodeSpace(touch).x - _movePointOld;
        if (_minDrag + deltaMove * 0.5 <=
            _winSize.width / 2 && _maxDrag + deltaMove * 0.5
            >= _winSize.width / 2) {
            setPosition(Point((getPositionX() + deltaMove * 0.5), (getPositionY() + ((deltaMove * 33.53) / 58) * 0.5)));
            _positionTemp = getPosition();
            _raceScene->_miniMapSprite->moveFrame((deltaMove * 0.5) / getScale(), false);
            if (deltaMove < 0) {
                _raceBackgroundManager->checkPositionBgr(true, getPositionX(), getScale());
            } else {
                _raceBackgroundManager->checkPositionBgr(false, getPositionX(), getScale());
            }
        }
        _minDrag = _cellView->_cell[3][START_CELL]->getPositionX() + _cellView->getPositionX() + _raceBackgroundManager->getPositionX() + (getPositionX() / getScale());
        _maxDrag = _cellView->_cell[3][GOAL_INDEX]->getPositionX() + _cellView->getPositionX() + _raceBackgroundManager->getPositionX() + (getPositionX() / getScale());
        _movePointOld = convertTouchToNodeSpace(touch).x;
    } else if (_touches.size() == 2) {
        const float len = convertTouchToNodeSpace(_touches[0]).getDistance(convertTouchToNodeSpace(_touches[1]));
        if (_touchLen > 0.0) {
            scaleScreen((len - _touchLenDelta) / _touchLenDelta);
        }
        _touchLenDelta = len;
    }
}

void RaceUIControll::onTouchEnded(Touch* touch, Event* unused_event)
{
    if (_raceScene->statusManager->view->cameraPositionFixed) {
        return;
    }
    auto touchIter = std::find(_touches.begin(), _touches.end(), touch);
    if (touchIter != _touches.end()) {
        _touches.erase(touchIter);
    }
    _touches.clear();
}

void RaceUIControll::onTouchCancelled(Touch* touch, Event* unused_event)
{
    if (UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        return;
    }

    auto touchIter = std::find(_touches.begin(), _touches.end(), touch);
    _touches.erase(touchIter);
}

void RaceUIControll::checkCamera(std::function<void()>callback)
{
    _raceScene->statusManager->view->cameraPositionFixed = true;

    _targetPlayerCharacterPosition = Point(_raceScene->statusManager->playerStatus->characterView->getPosition() -
                                           Vec2(_winSize.width / 2 + 7.5, _winSize.height / 2 + 7.5));

    if (isDefaultCameraPosition()) {
        callback();
        return;
    }
    _raceScene->_miniMapSprite->moveFrame((getPositionX() - _targetPlayerCharacterPosition.x) / getScale(), true);
    schedule(schedule_selector(RaceUIControll::prepareBackgroundImage), 0.02);
    runAction(Sequence::create(Spawn::create(ScaleTo::create(0.4f, 1.0),
                                             MoveTo::create(0.4f, _targetPlayerCharacterPosition),
                                             NULL),
                               CallFunc::create([&, callback]() {
        unscheduleAllCallbacks();
        callback();
    }),
                               NULL));
}

bool RaceUIControll::isDefaultCameraPosition()
{
    return getScale() >= 1.0 && getPosition().x == _targetPlayerCharacterPosition.x* getScale();
}

void RaceUIControll::prepareBackgroundImage(float dt)
{
    if (!isDefaultCameraPosition()) {
        // prepare backgroundImage;
        const bool isForward = getPosition().x > _targetPlayerCharacterPosition.x * (getScale());
        _raceBackgroundManager->checkPositionBgr(isForward, getPositionX(), getScale());
    }
}

void RaceUIControll::moveEachCharacters()
{
    int diceResult = _raceScene->statusManager->playerStatus->moveDistance;
    // reset flags
    if (_raceScene->statusManager->playerStatus->mustSlowDown) {
        _raceScene->statusManager->playerStatus->mustSlowDown = false;
        _raceScene->statusManager->playerStatus->characterView->gensokuEffect(false);
    }

    // create enemy move distances
    std::vector<int>enemyMoveDistances = { 0, 0, 0 };
    if (_raceScene->statusManager->sequence->movingByChain) {
        for (int i = 0; i < 3; i++) {
            if (_raceScene->statusManager->enemyStatus[i]->moveDistance != 0) {
                enemyMoveDistances.at(i) = _raceScene->statusManager->enemyStatus[i]->moveDistance;
                _raceScene->statusManager->enemyStatus[i]->moveDistance = 0;
            }
        }
    } else if (!_raceScene->statusManager->sequence->shootDiceAgain &&
               !UserDefault::getInstance()->getBoolForKey("Tutorial", false) &&
               _raceScene->statusManager->sequence->playerChainResultFoward == 0 &&
               _raceScene->statusManager->sequence->playerChainResultBack == 0) {
        enemyMoveDistances = _raceScene->statusManager->createEnemyDiceValue(_cellView);
    }
    _raceScene->statusManager->playerStatus->moveDistance = 0;
    _raceScene->statusManager->playerStatus->mustStop = false;

    _raceScene->_miniMapSprite->moveCharacters(diceResult, enemyMoveDistances.at(2), enemyMoveDistances.at(1), enemyMoveDistances.at(0));
    moveEnemyCharacters(enemyMoveDistances, diceResult);
    moveCharacter(diceResult);
    playRunSound(diceResult, enemyMoveDistances);

    runAction(Sequence::create(DelayTime::create(1.2),
                               CallFunc::create(CC_CALLBACK_0(RaceUIControll::onEndAnimateCharacter, this)),
                               CallFunc::create(CC_CALLBACK_0(RaceUIControll::onEndMoveCharacter, this)),
                               NULL));

    // reset player chain
    // _raceScene->statusManager->sequence->playerChainResultFoward = 0;
    // _raceScene->statusManager->sequence->playerChainResultBack = 0;
}

void RaceUIControll::onEndCheckCameraForMovePlayerSkill()
{
    moveOnlyPlayerCharacter(_raceScene->statusManager->skill->movePlayerCharacterDistance);
    runAction(Sequence::create(DelayTime::create(1.2),
                               CallFunc::create(CC_CALLBACK_0(RaceUIControll::onEndAnimateCharacter, this)),
                               CallFunc::create(CC_CALLBACK_0(RaceUIControll::onEndMoveCharacter, this)),
                               NULL));
}

void RaceUIControll::moveOnlyPlayerCharacter(int distance)
{
    _raceScene->statusManager->resetChainCounts();
    _raceScene->statusManager->skill->deleteDemeritCellActivated = false;
    _raceScene->statusManager->playerStatus->characterView->demeritEffect(false);
    _raceScene->statusManager->view->pause = true;

    stopRunSoundEffect();
    _raceScene->_miniMapSprite->moveCharacters(distance, 0, 0, 0);
    moveCharacter(distance);
    std::vector<int>emptyMove = { 0, 0, 0 };
    moveEnemyCharacters(emptyMove, distance);
    if (distance > 0) {
        SOUND_HELPER->playEffect(FAST_RUN, false);
    } else if (distance < 0) {
        SOUND_HELPER->playEffect(BACK, false);
    }
}

void RaceUIControll::myPaceRun(int distance)
{
    _raceScene->statusManager->skill->deleteDemeritCellActivated = false;
    moveOnlyPlayerCharacter(distance);
    runAction(Sequence::create(DelayTime::create(1.2),
                               CallFunc::create(CC_CALLBACK_0(RaceUIControll::onEndAnimateCharacter, this)),
                               CallFunc::create([&]() {
        _raceScene->turnEnd();
    }),
                               NULL));
}

void RaceUIControll::moveCharacter(int moveTo)
{
    if (moveTo > 0) {
        forwardCharacter(moveTo);
    } else if (moveTo < 0) {
        backCharacter(moveTo);
    }
    _raceScene->statusManager->playerStatus->characterView->setCharacterColumn(_raceScene->statusManager->playerStatus->characterView->getCharacterColumn() + moveTo);
}

void RaceUIControll::forwardCharacter(int moveTo)
{
    _raceScene->statusManager->playerStatus->characterView->setEnableCharacter(false);
    auto runSprite = RunningPlayerCharacter::create(_raceScene->statusManager->playerStatus->characterView->getPosition());
    addChild(runSprite, _raceScene->statusManager->playerStatus->characterView->getLocalZOrder());
    runSprite->play();
    _raceBackgroundManager->runAction(Sequence::create(
                                          RaceBackgroundManager::getMoveBy(1.2, moveTo),
                                          NULL));
}

void RaceUIControll::backCharacter(int moveTo)
{
    _raceScene->statusManager->playerStatus->characterView->jump();
    _raceBackgroundManager->runAction(Sequence::create(RaceBackgroundManager::getMoveBy(1.0, moveTo), NULL));
}

void RaceUIControll::moveEnemyCharacters(std::vector<int>moveTo, int playerMove)
{
    for (int i = 0; i < 3; i++) {
        if (moveTo.at(i) >= 0) {
            if (moveTo.at(i) != 0) {
                _raceScene->statusManager->enemyStatus[i]->characterView->setEnableCharacter(false);
                auto runSprite = RunningEnemyCharacter::create(
                    _raceScene->statusManager->enemyStatus[i]->characterView->getPosition(),
                    _enemyAnimationKeys.at(i),
                    MoveBy::create(1.2, VELOCITY_CHARACTER * (-playerMove + moveTo.at(i))));
                addChild(runSprite, _raceScene->statusManager->enemyStatus[i]->characterView->getLocalZOrder());
                runSprite->play();
            }
            _raceScene->statusManager->enemyStatus[i]->characterView->runAction(RaceBackgroundManager::getMoveBy((playerMove >= 0) ? 1.2 : 1.0,
                                                                                                                 playerMove - moveTo.at(i)));
        } else if (moveTo.at(i) < 0) {
            _raceScene->statusManager->enemyStatus[i]->characterView->jump(VELOCITY_CHARACTER  * (-playerMove + moveTo.at(i)));
        }
        _raceScene->statusManager->enemyStatus[i]->characterView->setCharacterColumn(_raceScene->statusManager->enemyStatus[i]->characterView->getCharacterColumn() + moveTo.at(i));
    }
}

void RaceUIControll::onEndAnimateCharacter()
{
    stopRunSoundEffect();
    runAction(Sequence::create(
                  DelayTime::create(0.6),
                  CallFunc::create(CC_CALLBACK_0(CharacterView::removeEffectOnTop, _raceScene->statusManager->playerStatus->characterView)), NULL));

    _raceScene->statusManager->playerStatus->characterView->setLocalZOrder(SPRITE_Z_ODER::PLAYER_CHARACTER);
    _raceScene->statusManager->playerStatus->characterView->setEnableCharacter(true);
    for (RaceStatus::CharacterStatus* enemyStatus : _raceScene->statusManager->enemyStatus) {
        enemyStatus->characterView->setEnableCharacter(true);
    }
}

void RaceUIControll::playRunSound(const int playerDiceValue, std::vector<int>enemyDiceValues)
{
    playMoveSoundEffect(getPlayMoveSoundEffectMove(playerDiceValue, enemyDiceValues));

    int runtime1 = 1.0;
    if (playerDiceValue > 0) {
        runtime1 = 1.2;
    }
    bool isPlayEnemyGoalEffect = false;
    for (int i = 0; i < 3; i++) {
        if (_raceScene->statusManager->enemyStatus[i]->characterView->getCharacterColumn() >= GOAL_INDEX && _raceScene->statusManager->enemyStatus[i]->finished == false) {
            const float callTime = 1.2 * (GOAL_INDEX - _raceScene->statusManager->enemyStatus[i]->characterView->getCharacterColumn() + enemyDiceValues.at(i)) / enemyDiceValues.at(i);
            if (!isPlayerGoal() && enemyDiceValues.at(i) > 0 && isPlayEnemyGoalEffect == false) {
                isPlayEnemyGoalEffect = true;
                runAction(Sequence::create(DelayTime::create(callTime),
                                           CallFunc::create([&]() {
                    SOUND_HELPER->playEffect(ENEMY_GOAL_EFFECT, false);
                }),
                                           NULL));
            }
        }
    }
}

const int RaceUIControll::getPlayMoveSoundEffectMove(const int playerDiceValue, std::vector<int>enemyDiceValues)
{
    if (playerDiceValue != 0) {
        return playerDiceValue;
    } else {
        int result = 0;
        for (int i = 0; i < enemyDiceValues.size(); i++) {
            const int diceValue = enemyDiceValues.at(i);
            if (diceValue != 0) {
                result = diceValue;
            }
        }
        return result;
    }
}

void RaceUIControll::playMoveSoundEffect(const int moveDistance)
{
    if (moveDistance < 0) {
        _moveSoundId = SOUND_HELPER->playEffect(BACK, true);
    } else if (moveDistance > 0) {
        if (_isNomalRunSound == true && _raceScene->statusManager->sequence->shootDiceAgain == false) {
            _moveSoundId = SOUND_HELPER->playEffect(RUN, true);
        } else {
            _moveSoundId = SOUND_HELPER->playEffect(FAST_RUN, true);
        }
    }
    _isNomalRunSound = false;
}

void RaceUIControll::stopRunSoundEffect()
{
    if (_moveSoundId > -1) {
        SOUND_HELPER->stopEffect(_moveSoundId);
        _moveSoundId = -1;
    }
}

void RaceUIControll::onEndMoveCharacter()
{
    _raceBackgroundManager->checkPositionBgr(true, getPositionX(), getScale());

    // check player is reached goal
    if (isPlayerGoal()) {
        finishRace();
        return;
    }

    // check enemy is reached goal
    for (int i = 0; i < 3; i++) {
        if (_raceScene->statusManager->enemyStatus[i]->characterView->getCharacterColumn() >= GOAL_INDEX &&
            !_raceScene->statusManager->enemyStatus[i]->finished) {
            _raceScene->statusManager->sequence->finishedEnemyIndex.push(i);
            _raceScene->statusManager->enemyStatus[i]->finished = true;
            setEnemyRaceRank(_raceScene->statusManager->enemyStatus[i]->characterModel->getID());
        }
    }

    // check player is lose
    if (_raceScene->statusManager->isEnemiesReachedTargetPosition()) {
        _characterRank = 4;
        fixConsecutiveStopOnMeritCell();
        _masterCharacterIdsOfRank.at(3) = playerCharacterId;
        _raceScene->endGameWithLose();
        return;
    }


    // disable skill force stop
    for (RaceStatus::CharacterStatus* enemyStatus : _raceScene->statusManager->enemyStatus) {
        if (enemyStatus->remainningBlockTurn <= 0 && _raceScene->statusManager->skill->forceStop) {
            enemyStatus->characterView->removeEffectOnTop();
        }
    }
    if (_raceScene->statusManager->enemyStatus[0]->remainningBlockTurn == 0 &&
        _raceScene->statusManager->enemyStatus[1]->remainningBlockTurn == 0 &&
        _raceScene->statusManager->enemyStatus[2]->remainningBlockTurn == 0) {
        _raceScene->statusManager->skill->forceStop  = false;
    }

    CELL_TYPE playerColumn = _cellView->_cell[3][_raceScene->statusManager->playerStatus->characterView->getCharacterColumn()]->getCellType();

    // check delete demerit cell
    if ((!_raceScene->statusManager->sequence->movingByChain || _raceScene->statusManager->sequence->playerChainResultFoward != 0) &&
        !_raceScene->statusManager->sequence->shootDiceAgain) {
        _raceScene->statusManager->checkDeleteDemeritCellSkills(playerColumn);
        if (_raceScene->statusManager->skill->deleteDemeritCellActivated == true) {
            setViewDeleteDemeritCellEfect(1);
        }
    }
    if (_raceScene->statusManager->skill->deleteDemeritCellActivated == true ||
        (_raceScene->statusManager->item->patolampActivated && CellType::isBack(playerColumn))) {
        playerColumn = NON_EVENT_CELL_1;
    }
    // disable used item
    _raceScene->statusManager->item->patolampActivated = false;
    _raceScene->statusManager->disableUsedItems();

    // player cell
    if (_previousPlayerColumnIndex != _raceScene->statusManager->playerStatus->characterView->getCharacterColumn() &&
        _raceScene->statusManager->skill->myPaceSkipCellFlag == false) {
        _raceScene->statusManager->countStopOnMeritIfPlayerIsOnMeritCell(playerColumn);
        activatePostPlayerCellEffect(playerColumn);
    }
    // check leader skill score up
    int playerColumnIndex = _raceScene->statusManager->playerStatus->characterView->getCharacterColumn();
    if (CellType::isScoreCell(_cellView->_cell[3][playerColumnIndex]->getCellType()) &&
        _raceScene->statusManager->leaderSkill->scoreMagnification > 0 &&
        _previousPlayerColumnIndex != playerColumnIndex) {
        _raceScene->statusManager->score->bonusScore += _raceScene->statusManager->leaderSkill->scoreUp(_cellView->_cell[3][playerColumnIndex]->getScoreCell(), _raceScene->statusManager->playerStatus);
        // add score
    } else if (_previousPlayerColumnIndex != playerColumnIndex) {
        _raceScene->statusManager->score->bonusScore += _cellView->_cell[3][playerColumnIndex]->getScoreCell();
    }
    _raceScene->_scoreAndTurnLayer->refresh();
    _previousPlayerColumnIndex = playerColumnIndex;

    // enemy cell
    if (_raceScene->statusManager->skill->movePlayerCharacterDistance == 0) {
        for (int i = 0; i < 3; i++) {
            _raceScene->statusManager->activatePostEnemyCellEffect(i, _cellView);
        }
    }

    // check turn ended
    if (!shootDiceActivated() &&
        !moveCharactersIfPlayerShouldMove() &&
        !moveCharactersIfEnemiesShouldMove() &&
        !myPaceRunActivated()) {
        // call turnEnd with delay
        runAction(Sequence::create(DelayTime::create(0.01),
                                   CallFunc::create(CC_CALLBACK_0(RaceScene::turnEnd, _raceScene)),
                                   NULL));
    }
}

bool RaceUIControll::shootDiceActivated()
{
    if (_raceScene->statusManager->sequence->shootDiceAgain) {
        _raceScene->statusManager->view->diceAvailable = true;
        _raceScene->statusManager->view->diceRolling = false;
        Ref* x;
        _raceScene->tapDiceButton(x);
        return true;
    }
    return false;
}

// return moveEachCharacters called
bool RaceUIControll::moveCharactersIfPlayerShouldMove()
{
    // activate player chains
    if (_raceScene->statusManager->sequence->playerChainResultFoward != 0 || _raceScene->statusManager->sequence->playerChainResultBack != 0) {
        if (_raceScene->statusManager->sequence->playerChainResultFoward > 0) {
            _raceScene->statusManager->playerStatus->chainForwardCount++;
        }
        if (_raceScene->statusManager->sequence->playerChainResultBack < 0) {
            _raceScene->statusManager->playerStatus->chainBackCount++;
        }
        if (_raceScene->statusManager->playerStatus->chainForwardCount <= _raceScene->statusManager->systemModel->getMaxNumberOfGridChain() ||
            _raceScene->statusManager->playerStatus->chainBackCount <= _raceScene->statusManager->systemModel->getMaxNumberOfUturnGridChai()) {
            _raceScene->turnStart();
            if (_raceScene->statusManager->playerStatus->chainForwardCount <= _raceScene->statusManager->systemModel->getMaxNumberOfGridChain() && _raceScene->statusManager->sequence->playerChainResultFoward > 0) {
                _raceScene->statusManager->playerStatus->moveDistance = _raceScene->statusManager->sequence->playerChainResultFoward;
            } else if (_raceScene->statusManager->playerStatus->chainBackCount <= _raceScene->statusManager->systemModel->getMaxNumberOfUturnGridChai() && _raceScene->statusManager->sequence->playerChainResultBack < 0) {
                _raceScene->statusManager->playerStatus->moveDistance = _raceScene->statusManager->sequence->playerChainResultBack;
            }
            _raceScene->statusManager->sequence->movingByChain = true;
            // call moveEachCharacters with delay
            runAction(Sequence::create(DelayTime::create(0.5),
                                       CallFunc::create(CC_CALLBACK_0(RaceUIControll::moveEachCharacters, this)),
                                       NULL));
            return true;
        }
    }
    return false;
}

// return moveEachCharacters called
bool RaceUIControll::moveCharactersIfEnemiesShouldMove()
{
    Sequence* callOnEndCheckCameraWithDelay = Sequence::create(DelayTime::create(0.5),
                                                               CallFunc::create(CC_CALLBACK_0(RaceUIControll::moveEachCharacters, this)),
                                                               NULL);
    if (_raceScene->statusManager->enemyStatus[0]->moveDistance != 0 ||
        _raceScene->statusManager->enemyStatus[1]->moveDistance != 0 ||
        _raceScene->statusManager->enemyStatus[2]->moveDistance != 0) {
        _raceScene->statusManager->sequence->movingByChain = true;
        runAction(callOnEndCheckCameraWithDelay);
        return true;
    } else {
        _raceScene->statusManager->sequence->movingByChain = false;
        return false;
    }
}

void RaceUIControll::setEnemyRaceRank(int characterId)
{
    for (int i = 0; i < 4; i++) {
        if (_masterCharacterIdsOfRank.at(i) != -1) {
            continue;
        }
        _masterCharacterIdsOfRank.at(i) = characterId;
        break;
    }
}

void RaceUIControll::finishRace()
{
    setRanking();

    for (int i = 0; i < 4; i++) {
        _masterCharacterIdsOfRank.at(i) = _raceScene->statusManager->sequence->characterStatusInRanking[i]->characterModel->getID();
    }

    fixConsecutiveStopOnMeritCell();
    _raceScene->setGoal(_characterRank, getCalculateScore());

    // set end game flag
    _raceScene->statusManager->sequence->finish = true;
}

void RaceUIControll::setRanking()
{
    struct columnComparator {
        bool
        operator ()(const RaceStatus::CharacterStatus* a, const RaceStatus::CharacterStatus* b) const
        {
            return a->characterView->getCharacterColumn() < b->characterView->getCharacterColumn();
        }
    };
    std::priority_queue<RaceStatus::CharacterStatus*, std::vector<RaceStatus::CharacterStatus*>, columnComparator>unfinishedCharacterStatus;

    int ranking = 0;
    // if no one finished a race, player is finished and position is 1st
    if (_raceScene->statusManager->sequence->finishedEnemyIndex.empty()) {
        _raceScene->statusManager->sequence->characterStatusInRanking[ranking] = _raceScene->statusManager->playerStatus;
        _characterRank = 1;
        ranking++;
    }

    // push unfinished enemies to priority queue
    for (RaceStatus::CharacterStatus* characterStatus : _raceScene->statusManager->enemyStatus) {
        if (characterStatus->finished == false) {
            unfinishedCharacterStatus.push(characterStatus);
        } else if (characterStatus->finished == true) {
            _raceScene->statusManager->sequence->characterStatusInRanking[ranking] = _raceScene->statusManager->enemyStatus[_raceScene->statusManager->sequence->finishedEnemyIndex.front()];
            _raceScene->statusManager->sequence->finishedEnemyIndex.pop();
            ranking++;
        }
    }

    if (_characterRank != 1) {
        _raceScene->statusManager->sequence->characterStatusInRanking[ranking] = _raceScene->statusManager->playerStatus;
        _characterRank = ranking + 1;
        ranking++;
    }

    // set unfinished charadcters to characterStatusInRanking
    while (!unfinishedCharacterStatus.empty()) {
        _raceScene->statusManager->sequence->characterStatusInRanking[ranking] = unfinishedCharacterStatus.top();
        unfinishedCharacterStatus.top()->finished = true;
        unfinishedCharacterStatus.pop();
        ranking++;
    }
}

// 連続でmeritマスに止まった値の修正
// TODO 正常にmeritマスに止まった値が取れていれば不要
void RaceUIControll::fixConsecutiveStopOnMeritCell()
{
    if (_raceScene->statusManager->score->consecutiveStopOnMeritCell >= 2) {
        _raceScene->statusManager->score->consecutiveStopOnMeritCell--;
    } else {
        _raceScene->statusManager->score->consecutiveStopOnMeritCell = 0;
    }
}

// スコアの計算
int RaceUIControll::getCalculateScore()
{
    int raceScore =  _courseModel->getBaseScore() + _raceScene->statusManager->score->bonusScore + (_raceScene->statusManager->score->consecutiveStopOnMeritCell * 1000);
    if (_courseModel->isGoalTarget()) {
        std::shared_ptr<SystemSettingModel>systemSettingModel =  SystemSettingModel::getModel();
        if (_characterRank == 1) {
            raceScore *= systemSettingModel->getRankBonusRate1();
        } else if (_characterRank == 2) {
            raceScore *= systemSettingModel->getRankBonusRate2();
        } else if (_characterRank == 3) {
            raceScore *= systemSettingModel->getRankBonusRate3();
        } else {
            raceScore *= systemSettingModel->getRankBonusRate4();
        }
    }
    return raceScore;
}

void RaceUIControll::scaleScreen(float scale)
{
    float deltaScale = getScale() + scale / 2;
    if (deltaScale <= 1 && deltaScale >= 0.5) {
        setScale(deltaScale);
        setPosition(_positionTemp * getScale());
    } else if (deltaScale > 1) {
        setScale(1);
    } else if (deltaScale < 0.5) {
        setScale(0.5);
    }
}

void RaceUIControll::activatePostPlayerCellEffect(CELL_TYPE type)
{
    if (_raceScene->statusManager->playerStatus->remainningStopTurn < 1) {
        _raceScene->statusManager->playerStatus->stoppedGridIds.push_back(type);
    }

    _raceScene->statusManager->sequence->playerChainResultFoward = 0;
    _raceScene->statusManager->sequence->playerChainResultBack = 0;
    _raceScene->statusManager->sequence->shootDiceAgain = false;

    if (!(CellType::isStop(type) && (!_raceScene->statusManager->playerStatus->mustStop &&
                                     _raceScene->statusManager->playerStatus->remainningStopTurn == 0))) {
        _raceScene->statusManager->playerStatus->remainningStopTurn = 0;
    }

    if (!CellType::isSpeedDown(type)) {
        _raceScene->statusManager->playerStatus->slowDownValue = 0;
    }

    if (!(CellType::isForward(type) && _raceScene->statusManager->playerStatus->chainForwardCount < _raceScene->statusManager->systemModel->getMaxNumberOfGridChain()) &&
        !(CellType::isBack(type) && _raceScene->statusManager->playerStatus->chainBackCount < _raceScene->statusManager->systemModel->getMaxNumberOfUturnGridChai())) {
        _raceScene->statusManager->playerStatus->chainBonus = 0;
    }

    if (CellType::isForward(type) && _raceScene->statusManager->playerStatus->chainForwardCount < _raceScene->statusManager->systemModel->getMaxNumberOfGridChain()) {
        _raceScene->statusManager->sequence->playerChainResultFoward = _raceScene->statusManager->playerStatus->chainBonus + lederSkillCellEfectUpForward(RaceCellRogicModel::cellRogicFoword(_raceScene->statusManager->teamStatus->getPower(), type,  _courseModel->getOperandId()));
        _raceScene->statusManager->showCellEffect(type);
        _raceScene->statusManager->playerStatus->chainBonus++;
    } else if (CellType::isBack(type) && _raceScene->statusManager->playerStatus->chainBackCount < _raceScene->statusManager->systemModel->getMaxNumberOfUturnGridChai()) {
        _raceScene->statusManager->sequence->playerChainResultBack = -RaceCellRogicModel::cellRogicBack(_raceScene->statusManager->teamStatus->getTechnique(), type, _courseModel->getOperandId());
        _raceScene->statusManager->showCellEffect(type);
    } else if (CellType::isSpeedDown(type)) {
        _raceScene->statusManager->playerStatus->mustSlowDown = true;
        _raceScene->statusManager->playerStatus->slowDownValue = -RaceCellRogicModel::cellRogicSpeedDown(_raceScene->statusManager->teamStatus->getTechnique(), type, _courseModel->getOperandId());
        _raceScene->statusManager->showCellEffect(type);
    } else if (CellType::isStop(type)) {
        if (!_raceScene->statusManager->playerStatus->mustStop &&
            _raceScene->statusManager->playerStatus->remainningStopTurn == 0) {
            _raceScene->statusManager->playerStatus->remainningStopTurn = RaceCellRogicModel::cellRogicLose(_raceScene->statusManager->teamStatus->getBrake(), type, _courseModel->getOperandId());
            _raceScene->statusManager->showCellEffect(type);
            _raceScene->statusManager->playerStatus->mustStop = true;
        } else if (_raceScene->statusManager->playerStatus->mustStop &&
                   _raceScene->statusManager->playerStatus->remainningStopTurn == 0) {
            _raceScene->statusManager->playerStatus->mustStop = false;
        }
    } else if (PREFERENCE_CELL_1 == type) {
        _raceScene->statusManager->playerStatus->remainningStopTurn = -1;
        _raceScene->statusManager->sequence->shootDiceAgain = true;
        _raceScene->statusManager->showCellEffect(type);
    } else if (SLOWLY_CELL_1 == type) {
        _raceScene->statusManager->playerStatus->mustLimitSpeed = true;
    } else if (MINIMUM_SPEED_CELL_1 == type) {
        _raceScene->statusManager->playerStatus->mustHighSpeed = true;
    } else if (CellType::isDownwardSlope(type)) {
        _raceScene->statusManager->showCellEffect(type);
        _raceScene->statusManager->playerStatus->downwardSlopeType = type;
        _raceScene->showDiceResultUp();
    } else if (CellType::isUpwardSlope(type)) {
        _raceScene->statusManager->showCellEffect(type);
        _raceScene->statusManager->playerStatus->upwardSlopeType = type;
        _raceScene->showDiceResultDown();
    } else if (GET_ITEM_CELL_1 == type && _raceScene->statusManager->item->currentItem == 5) {
        _raceScene->statusManager->showCellEffect(type);
        _raceScene->activateItem(_raceScene->statusManager->item->getRandomItem());
    } else if (CellType::isScoreCell(type)) {
        _raceScene->statusManager->showCellEffect(type);
    } else if (CHARGE_CELL_1 == type) {
        _raceScene->statusManager->showCellEffect(type);
        _raceScene->_skillBar->chargeSkillWithEffect(1);
    }

    if (CellType::isDemerit(type) &&
        (!(CellType::isBack(type) && _raceScene->statusManager->playerStatus->chainBackCount < _raceScene->statusManager->systemModel->getMaxNumberOfUturnGridChai()))) {
        _raceScene->statusManager->playerStatus->characterView->demeritEffect(true);
        SOUND_HELPER->playEffect(STATUS_DOWN_EFFECT, false);
    } else {
        _raceScene->statusManager->playerStatus->characterView->removeDemerit();
    }
}

// TODO: move this method to RaceScene after remove _block
bool RaceUIControll::myPaceRunActivated()
{
    if (_raceScene->statusManager->skill->myPaceFlag == true &&
        _raceScene->statusManager->skill->remainningTurn > 0) {
        _raceScene->_skillBar->refreshSkillButtons();
        _raceScene->statusManager->skill->myPaceSkipCellFlag = true;
        // activate
        _raceScene->statusManager->playerStatus->resetPreCellEffect();
        _raceScene->statusManager->playerStatus->characterView->gensokuEffect(false);
        _raceScene->hideDiceResultUpDown();
        myPaceRun(_raceScene->statusManager->skill->myPaceRunDistance);

        _raceScene->statusManager->skill->remainningTurn--;
        if (_raceScene->statusManager->skill->remainningTurn == 0) {
            _raceScene->statusManager->skill->myPaceFlag = false;
            _raceScene->statusManager->skill->myPaceRunDistance = 0;
        }
        return true;
    }
    return false;
}

void RaceUIControll::setVisibleToCharacterNameLabels(bool npcEnableFlag)
{
    _raceScene->statusManager->playerStatus->characterView->setNameLabelVisible(npcEnableFlag);
    for (RaceStatus::CharacterStatus* enemyStatus : _raceScene->statusManager->enemyStatus) {
        enemyStatus->characterView->setNameLabelVisible(npcEnableFlag);
    }
}

bool RaceUIControll::isPlayerGoal()
{
    return _raceScene->statusManager->playerStatus->characterView->getCharacterColumn() >= GOAL_INDEX;
}

int RaceUIControll::lederSkillCellEfectUpForward(int forwardCellCount)
{
    if (_raceScene->statusManager->leaderSkill->forwardMagnification < 1) {
        return forwardCellCount;
    }
    int plusFowardCellCount = forwardCellCount * (_raceScene->statusManager->leaderSkill->forwardMagnification * 0.01);
    if (plusFowardCellCount < 1) {
        plusFowardCellCount = 1;
    }
    int result = forwardCellCount + plusFowardCellCount;
    auto characterEffect = CharacterEfectView::create()->createViewCellEfectUpEfect(plusFowardCellCount, 1);
    characterEffect->setPosition(Point(_raceScene->statusManager->playerStatus->characterView->getContentSize().width / 2, _raceScene->statusManager->playerStatus->characterView->getContentSize().height / 2));
    _raceScene->statusManager->playerStatus->characterView->addChild(characterEffect, SPRITE_Z_ODER_OF_PLAYER_CHARACTER::CELL_EFFECT);
    return result;
}

CellViewController* RaceUIControll::getCellView()
{
    return _cellView;
}

void RaceUIControll::setViewDeleteDemeritCellEfect(int type)
{
    auto characterEffect = CharacterEfectView::create()->createViewDemeritCellTypeDeleteEfect(type);
    characterEffect->setPosition(Point(_raceScene->statusManager->playerStatus->characterView->getContentSize().width / 2, _raceScene->statusManager->playerStatus->characterView->getContentSize().height / 2));
    _raceScene->statusManager->playerStatus->characterView->addChild(characterEffect, SPRITE_Z_ODER_OF_PLAYER_CHARACTER::CELL_EFFECT);
}

void RaceUIControll::setViewDiceDoubleValueResultEfect(int doubleValue)
{
    auto characterEffect = CharacterEfectView::create()->createViewDiceDoubleValueResultEfect(doubleValue);
    characterEffect->setPosition(Point(_raceScene->statusManager->playerStatus->characterView->getContentSize().width / 2, _raceScene->statusManager->playerStatus->characterView->getContentSize().height / 2));
    _raceScene->statusManager->playerStatus->characterView->addChild(characterEffect, SPRITE_Z_ODER_OF_PLAYER_CHARACTER::CELL_EFFECT);
}
