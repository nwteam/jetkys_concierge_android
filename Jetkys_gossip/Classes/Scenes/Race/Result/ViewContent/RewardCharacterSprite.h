#ifndef __syanago__RewardCharacterSprite__
#define __syanago__RewardCharacterSprite__

#include "cocos2d.h"
#include "RaceResultResponseModel.h"
#include "jCommon.h"

USING_NS_CC;

class RewardCharacterSprite : public Layer
{
public:
    RewardCharacterSprite():
    viewLevel(0)
    {}
    
    static RewardCharacterSprite* createCharacterSprite(const std::shared_ptr<RaceResultResponseModel::Character>& model);
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        ICON,
        LEVEL,
        NAME,
        EXP_BAR,
        EXP,
    };
    enum Z_ORDER{
        Z_ICON = 0,
        Z_LEVEL,
        Z_NAME,
        Z_EXP_BAR,
        Z_EXP,
    };
    int viewLevel;
    
    void showCharacterSprite(const std::shared_ptr<RaceResultResponseModel::Character>& model);
    void showIcon(const int masterCharacterId, Sprite* base);
    void showLevel(const int level, Sprite* base);
    void showName(const std::string characterName, Sprite* base);
    void showExpLabel(const int getExp, Sprite* base);
    void showExpBar(const std::shared_ptr<RaceResultResponseModel::Character>& model, Sprite* base);
    
    void playSoundExp();
    void PlaySoundLevel();
    void setLevelString();
};

#endif