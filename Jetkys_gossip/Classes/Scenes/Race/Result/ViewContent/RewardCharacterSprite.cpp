#include "RewardCharacterSprite.h"
#include "FontDefines.h"
#include "LevelModel.h"
#include "SoundHelper.h"

RewardCharacterSprite* RewardCharacterSprite::createCharacterSprite(const std::shared_ptr<RaceResultResponseModel::Character>& model)
{
    auto view = new RewardCharacterSprite();
    view->showCharacterSprite(model);
    view->autorelease();
    return view;
}

void RewardCharacterSprite::showCharacterSprite(const std::shared_ptr<RaceResultResponseModel::Character>& model)
{
    Sprite* result = Sprite::create("get_exp_base.png");
    result->setTag(TAG_SPRITE::BACKGROUND);
    showIcon(model->getCharacterId(), result);
    showLevel(model->getLevel(), result);
    showName(model->getName(), result);
    showExpLabel(model->getExpDifference(), result);
    showExpBar(model, result);
    addChild(result);
}

void RewardCharacterSprite::showIcon(const int masterCharacterId, Sprite* base)
{
    auto icon = makeSprite(StringUtils::format("%d_i.png", masterCharacterId).c_str());
    icon->setTag(TAG_SPRITE::ICON);
    icon->setPosition(Vec2(icon->getContentSize().width / 2 + 100, base->getContentSize().height / 2));
    base->addChild(icon, Z_ORDER::Z_ICON);
}

void RewardCharacterSprite::showLevel(const int level, cocos2d::Sprite* base)
{
    viewLevel = level;
    auto levellabel = Label::createWithTTF(StringUtils::format("Lv %d", level), FONT_NAME_2, 18);
    levellabel->setTag(TAG_SPRITE::LEVEL);
    levellabel->setPosition(Vec2(base->getChildByTag(TAG_SPRITE::ICON)->getPosition()) + Vec2(0, -base->getChildByTag(TAG_SPRITE::ICON)->getContentSize().height / 2 - 9));
    levellabel->enableOutline(Color4B(Color3B::BLACK), 2);
    base->addChild(levellabel, Z_ORDER::Z_LEVEL);
}

void RewardCharacterSprite::showName(const std::string characterName, Sprite* base)
{
    Label* name = Label::createWithTTF(characterName, FONT_NAME_2, 25);
    name->setTag(TAG_SPRITE::NAME);
    name->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    name->setPosition(Vec2(base->getContentSize().width / 2 - 10, base->getChildByTag(TAG_SPRITE::ICON)->getPosition().y + base->getChildByTag(TAG_SPRITE::ICON)->getContentSize().height / 2));
    name->setColor(Color3B::BLACK);
    base->addChild(name, Z_ORDER::Z_NAME);
}

void RewardCharacterSprite::showExpLabel(const int getExp, Sprite* base)
{
    Label* expLabel = Label::createWithTTF(StringUtils::format("経験値:   + %d", getExp), FONT_NAME_2, 25);
    expLabel->setTag(TAG_SPRITE::EXP);
    expLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    expLabel->setPosition(Vec2(base->getContentSize().width / 2 - 10, base->getChildByTag(TAG_SPRITE::ICON)->getPosition().y - base->getChildByTag(TAG_SPRITE::ICON)->getContentSize().height / 2 - 25));
    expLabel->setColor(Color3B::BLACK);
    base->addChild(expLabel, Z_ORDER::Z_EXP);
}

void RewardCharacterSprite::showExpBar(const std::shared_ptr<RaceResultResponseModel::Character>& model, Sprite* base)
{
    // 未改修
    int lv = model->getLevel();
    int oldLevel = lv - model->getLevelDifference();
    int exp = model->getExp();
    int oldExp = exp - model->getExpDifference();

    int nlvUp = model->getLevelDifference();

    auto expBarBase = makeSprite("linewhite.png");
    expBarBase->setPosition(Vec2(base->getChildByTag(TAG_SPRITE::NAME)->getPosition().x + 115,
                                 base->getChildByTag(TAG_SPRITE::ICON)->getPosition().y));
    expBarBase->setColor(Color3B::GRAY);
    expBarBase->setScale(0.5, 2.0);
    base->addChild(expBarBase);

    auto expBar = makeSprite("linered.png");
    expBar->setAnchorPoint(Point(0.0, 0.0));
    expBar->setPosition(Point(0.0, 0.0));
    expBarBase->addChild(expBar);

    float scale = 0;
    float scaleOld = 0;
    if (model->isLevelUp() == false) {
        if (lv < 99) {
            std::shared_ptr<LevelModel>model(LevelModel::find(lv + 1));
            int maxExp = (int)model->getRequiredExp();
            scale = (float)exp / (float)maxExp;
            scaleOld = (float)oldExp / (float)maxExp;
            expBar->setScale(scaleOld, 1.0);
            expBar->runAction(Sequence::create(DelayTime::create(1.0), ScaleTo::create(1.0, scale, 1.0), NULL));

            runAction(Sequence::create(DelayTime::create(1.0), CallFunc::create(CC_CALLBACK_0(RewardCharacterSprite::playSoundExp, this)), NULL));
        } else {
            scale = 1;
            scaleOld = 1;
            expBar->setScale(scaleOld, 1.0);
        }
    } else {
        if (oldLevel < 99) {
            std::shared_ptr<LevelModel>model(LevelModel::find(lv + 1));
            int maxExp1 = (int)model->getRequiredExp();
            scale = (float)exp / (float)maxExp1;

            std::shared_ptr<LevelModel>oldModel(LevelModel::find(oldLevel + 1));
            int maxExp = (int)oldModel->getRequiredExp();
            scaleOld = (float)oldExp / (float)maxExp;
            expBar->setScale(scaleOld, 1.0);
            expBar->runAction(Sequence::create(DelayTime::create(1.0), Repeat::create(Sequence::create(ScaleTo::create(1.0, 1.0, 1.0), CallFunc::create(CC_CALLBACK_0(RewardCharacterSprite::PlaySoundLevel, this)), DelayTime::create(0.5), ScaleTo::create(0.01, 0.0, 1.0), NULL), nlvUp), ScaleTo::create(1.0, scale, 1.0), NULL));
            runAction(Sequence::create(DelayTime::create(1.0), CallFunc::create(CC_CALLBACK_0(RewardCharacterSprite::playSoundExp, this)), NULL));
            runAction(Repeat::create(Sequence::create(DelayTime::create(1.0), CallFunc::create(CC_CALLBACK_0(RewardCharacterSprite::setLevelString, this)), NULL), nlvUp));
        } else {
            scale = 1;
            scaleOld = 1;
            expBar->setScale(scaleOld, 1.0);
        }
    }
}

void RewardCharacterSprite::playSoundExp()
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_GAGE_UP, false);
}

void RewardCharacterSprite::PlaySoundLevel()
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_LEVEL_UP, false);
}

void RewardCharacterSprite::setLevelString()
{
    viewLevel++;
    static_cast<Label*>(getChildByTag(TAG_SPRITE::BACKGROUND)->getChildByTag(TAG_SPRITE::LEVEL))->setString(StringUtils::format("Lv %d", viewLevel));
}