#include "RaceResultResponseModel.h"
#include "PlayerController.h"
#include "CharacterModel.h"

RaceResultResponseModel::RaceResultResponseModel(Json* response, const std::shared_ptr<HelpPlayer>& _helpPlayer)
{
    Json* data = Json_getItem(Json_getItem(response, "set_race_results"), "data");

    if (strcmp(Json_getString(data, "possible_id_friend_request", ""), "null") != 0) {
        int requestFriendId = atoi(Json_getString(data, "possible_id_friend_request", ""));
        helpPlayer = _helpPlayer;
        if (requestFriendId != helpPlayer->getId()) {
            CC_ASSERT("[RaceResultResponseModel::RaceResultResponseModel]エラー");
            return;
        }
        helpPlayer->setLastLogInTime(Json_getString(data, "login_datetime_of_possible_friend_request", ""));
    } else {
        helpPlayer = nullptr;
    }


    if (strcmp(Json_getString(data, "event_message", ""), "null") != 0) {
        eventMessage = Json_getString(data, "event_message", "");
    } else {
        eventMessage = "";
    }

    if (strcmp(Json_getString(data, "mission_clear_message", ""), "null") != 0) {
        missionClearMessage = Json_getString(data, "mission_clear_message", "");
    } else {
        missionClearMessage = "";
    }

    Json* player = Json_getItem(data, "players");
    if (player) {
        parsePlayerJsonData(player);
    }

    Json* playerCharacters = Json_getItem(data, "player_characters");
    if (playerCharacters) {
        parsePlayerCharactersJsonData(playerCharacters);
    }

    Json* playerParts = Json_getItem(data, "player_parts");
    if (playerParts) {
        parsePlayerPartsJsonData(playerParts);
    }

    Json* playerItemUnits = Json_getItem(data, "player_item_units");
    if (playerItemUnits) {
        parsePlayerItemUnitsJsonData(playerItemUnits);
    }

    Json* firstGetCharacterIds = Json_getItem(data, "first_get_character_ids");
    if (firstGetCharacterIds) {
        parseFirstGetCharacterIdsJsonData(firstGetCharacterIds);
    }
}

RaceResultResponseModel::Player::Player(Json* child)
{
    playerId = atoi(Json_getString(child, "id", ""));
    fuel = atoi(Json_getString(child, "fuel", ""));
    totalExp = atoi(Json_getString(child, "total_exp", ""));
    freeCoin = atoi(Json_getString(child, "free_coin", ""));
    friendPoint = atoi(Json_getString(child, "friend_point", ""));
    currentCourseId = atoi(Json_getString(child, "current_course_id", ""));
    rank = atoi(Json_getString(child, "rank", ""));
}

int RaceResultResponseModel::Player::getCoinDifference()
{
    if (PLAYERCONTROLLER->_player == NULL) {
        return freeCoin;
    }
    return std::max(0, freeCoin - PLAYERCONTROLLER->_player->getFreeCoin());
}

bool RaceResultResponseModel::Player::isPlayerRankUp()
{
    if (PLAYERCONTROLLER->_player == NULL) {
        return false;
    }
    if (rank > PLAYERCONTROLLER->_player->getRank()) {
        return true;
    }
    return false;
}

void RaceResultResponseModel::parsePlayerJsonData(Json* playerData)
{
    std::shared_ptr<Player>model(new Player(playerData));
    player = model;
}

RaceResultResponseModel::Character::Character(Json* child)
{
    playerCharacterId = atoi(Json_getString(child, "id", ""));
    playerId = atoi(Json_getString(child, "player_id", ""));
    characterId = atoi(Json_getString(child, "characters_id", ""));
    exp = atoi(Json_getString(child, "exp", ""));
    level = atoi(Json_getString(child, "level", ""));
    power = atoi(Json_getString(child, "exercise", ""));
    technique = atoi(Json_getString(child, "reaction", ""));
    brake = atoi(Json_getString(child, "decision", ""));
    dearValue = atoi(Json_getString(child, "dear_value", ""));

    std::map<int, PlayerCharactersModel*>::iterator it;
    it = PLAYERCONTROLLER->_playerCharacterModels.find(playerCharacterId);
    if (it != PLAYERCONTROLLER->_playerCharacterModels.end()) {
        isGetCharacter = false;
    } else {
        isGetCharacter = true;
    }
    std::shared_ptr<CharacterModel>model(CharacterModel::find(characterId));
    if (model != NULL) {
        name = model->getCarName();
    } else {
        name = "";
    }
}

int RaceResultResponseModel::Character::getExpDifference()
{
    if (isGetCharacter == false) {
        return exp - PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getExp();
    }
    return 0;
}

int RaceResultResponseModel::Character::getLevelDifference()
{
    if (isGetCharacter == false) {
        return level - PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getLevel();
    }
    return 0;
}

bool RaceResultResponseModel::Character::isLevelUp()
{
    if (PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId] == NULL) {
        return false;
    }
    if (level > PLAYERCONTROLLER->PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId]->getLevel()) {
        return true;
    }
    return false;
}

std::shared_ptr<RaceResultResponseModel::Character>RaceResultResponseModel::findCharacterDataOfCharacterId(const int characterId)
{
    for (int i = 0; i < characters.size(); i++) {
        if (characters.at(i)->getPlayerCharacterId() == characterId) {
            return characters.at(i);
        }
    }
    return nullptr;
}

std::shared_ptr<RaceResultResponseModel::Character>RaceResultResponseModel::getRewardCharacterData()
{
    for (int i = 0; i < characters.size(); i++) {
        if (characters.at(i)->getIsGetCharacter() == true) {
            return characters.at(i);
        }
    }
    return nullptr;
}

void RaceResultResponseModel::parsePlayerCharactersJsonData(Json* playerCharacterData)
{
    characters.clear();
    isResponseCharacterData = false;
    for (Json* child = playerCharacterData->child; child; child = child->next) {
        std::shared_ptr<Character>model(new Character(child));
        characters.push_back(model);
        isResponseCharacterData = true;
    }
}

RaceResultResponseModel::Parts::Parts(Json* child)
{
    playerPartsId = atoi(Json_getString(child, "id", ""));
    playerId = atoi(Json_getString(child, "player_id", ""));
    partsId = atoi(Json_getString(child, "parts_id", ""));
}

void RaceResultResponseModel::parsePlayerPartsJsonData(Json* playerPartsData)
{
    parts.clear();
    for (Json* child = playerPartsData->child; child; child = child->next) {
        std::shared_ptr<Parts>model(new Parts(child));
        parts.push_back(model);
    }
}

RaceResultResponseModel::ItemUnit::ItemUnit(Json* child)
{
    playerId = atoi(Json_getString(child, "player_id", ""));
    itemUnitId = atoi(Json_getString(child, "item_unit_id", ""));
    itemUnitKind = atoi(Json_getString(child, "item_unit_kind", ""));
}

void RaceResultResponseModel::parsePlayerItemUnitsJsonData(Json* playerItemUnitsData)
{
    itemUnits.clear();
    for (Json* child = playerItemUnitsData->child; child; child = child->next) {
        std::shared_ptr<ItemUnit>model(new ItemUnit(child));
        itemUnits.push_back(model);
    }
}

void RaceResultResponseModel::parseFirstGetCharacterIdsJsonData(Json* firstGetCharacterIdsData)
{
    firstGetCharacterIds.clear();
    for (Json* child = firstGetCharacterIdsData->child; child; child = child->next) {
        firstGetCharacterIds.push_back(child->valueInt);
    }
}