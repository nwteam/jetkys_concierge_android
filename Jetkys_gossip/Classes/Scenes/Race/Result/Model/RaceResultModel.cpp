#include "RaceResultModel.h"

const std::string RaceResultModel::getRollOfFilePath(const int rank)
{
    if (rank == _resultPlayer->getResultClearThings()->getRankOfPlayer()) {
        return "race_player_point.png";
    }

    if ((rank > _resultCourse->getMasterCharacterIdsOfRank().size() || _resultCourse->getMasterCharacterIdsOfRank().size() < 1) && _resultCourse->getCourseModel()->getCourseType() == 2) {
        return "";
    }
    if (_resultCourse->getMasterCharacterIdsOfRank().at(rank - 1) < 1) {
        return "";
    }

    if (_resultCourse->getCourseModel()->getBossCharacterId() == _resultCourse->getMasterCharacterIdsOfRank().at(rank - 1) && _resultCourse->getCourseModel()->getCourseType() == 2) {
        return "race_boss_point.png";
    }
    return "";
}



