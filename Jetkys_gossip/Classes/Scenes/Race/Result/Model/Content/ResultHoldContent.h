#ifndef __syanago__ResultHoldContent__
#define __syanago__ResultHoldContent__

#include "cocos2d.h"

USING_NS_CC;

namespace ResultPlayerContent{
    class ResultHoldContent
    {
    public:
        ResultHoldContent(const int holdItemCount, const int holdSkillChargeCharacterCount)
        {
            _holdItemCount = holdItemCount;
            _holdSkillChargeCharacterCount = holdSkillChargeCharacterCount;
        };
        
        CC_SYNTHESIZE_READONLY(int, _holdItemCount, HoldItemCount);
        CC_SYNTHESIZE_READONLY(int, _holdSkillChargeCharacterCount, HoldSkillChargeCharacterCount);
    };
}

#endif
