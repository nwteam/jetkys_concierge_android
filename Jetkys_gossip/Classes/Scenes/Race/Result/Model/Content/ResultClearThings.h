#ifndef __syanago__ResultClearThings__
#define __syanago__ResultClearThings__

#include "cocos2d.h"

USING_NS_CC;

namespace ResultPlayerContent{
    class ResultClearThings
    {
    public:
        ResultClearThings(const int rankOfPlayer, const int goalTurnCount, const  int numOfGridToGoal, const int stopMeritCount, const int skillUsedCount, const std::vector<int> stoppedGridIds)
        {
            _rankOfPlayer = rankOfPlayer;
            _goalTurnCount = goalTurnCount;
            _numOfGridToGoal = numOfGridToGoal;
            _stopMeritCount = stopMeritCount;
            _skillUsedCount = skillUsedCount;
            _stoppedGridIds = stoppedGridIds;
        };
        
        const std::string getStopedGridIdsArrayString();

        
        CC_SYNTHESIZE_READONLY(int, _rankOfPlayer, RankOfPlayer);
        CC_SYNTHESIZE_READONLY(int, _goalTurnCount, GoalTurnCount);
        CC_SYNTHESIZE_READONLY(int, _numOfGridToGoal, NumOfgridToGoal);
        CC_SYNTHESIZE_READONLY(int, _stopMeritCount, StopMeritCount);
        CC_SYNTHESIZE_READONLY(int, _skillUsedCount, SkillUsedCount);
        CC_SYNTHESIZE_READONLY(std::vector<int>, _stoppedGridIds, StoppedGridIds);
    };
}

#endif