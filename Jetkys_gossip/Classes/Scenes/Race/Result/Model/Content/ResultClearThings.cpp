#include "ResultClearThings.h"

using namespace ResultPlayerContent;

const std::string ResultClearThings::getStopedGridIdsArrayString()
{
    std::string result = "[";
    for (int i = 0; i < _stoppedGridIds.size(); i++) {
        if (i != 0) {
            result += ",";
        }
        result += StringUtils::format("%d", _stoppedGridIds.at(i));
    }
    result += "]";
    return result;
}