#ifndef __syanago__ResultScore__
#define __syanago__ResultScore__

#include "cocos2d.h"
#include "CourseModel.h"

USING_NS_CC;

namespace ResultPlayerContent{
    class ResultScore
    {
    public:
        ResultScore(const int viewScore)
        {
            _viewScore = viewScore;
        };
        
        void setOtherScoreData(const std::shared_ptr<CourseModel>& corseModel, const int rankOfPlayer, const int stopMeritCount);
        
        CC_SYNTHESIZE_READONLY(int, _viewScore, ViewScore);
        CC_SYNTHESIZE_READONLY(int, _baseScore, BaseScore);
        CC_SYNTHESIZE_READONLY(int, _raceScore, RaceScore);
        CC_SYNTHESIZE_READONLY(int, _totalScore, TotalScore);
        CC_SYNTHESIZE_READONLY(float, _scoreRate, ScoreRate);
    private:
        static const float getScoreRate(const int raceType,const int rankOfPlayer);
        static const int getTotalScore(const int baseScore, const int raceScore, const float scoreRate);
    };
}
#endif
