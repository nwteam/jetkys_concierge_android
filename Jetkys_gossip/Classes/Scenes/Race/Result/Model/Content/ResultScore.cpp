#include "ResultScore.h"
#include "SystemSettingModel.h"

using namespace ResultPlayerContent;

void ResultScore::setOtherScoreData(const std::shared_ptr<CourseModel>& courseModel, const int rankOfPlayer, const int stopMeritCount)
{
    _baseScore = courseModel->getBaseScore();
    _raceScore =  _viewScore + (stopMeritCount * 1000);
    _scoreRate = getScoreRate(courseModel->getRaceType(), rankOfPlayer);
    _totalScore = ResultScore::getTotalScore(_baseScore, _raceScore, _scoreRate);
}

const float ResultScore::getScoreRate(const int raceType, const int rankOfPlayer)
{
    std::shared_ptr<const SystemSettingModel>model(SystemSettingModel::getModel());
    if (rankOfPlayer == 1) {
        return model->getRankBonusRate1();
    } else if (rankOfPlayer == 2) {
        return model->getRankBonusRate2();
    } else if (rankOfPlayer == 3) {
        return model->getRankBonusRate3();
    } else {
        return model->getRankBonusRate4();
    }
}

const int ResultScore::getTotalScore(const int baseScore, const int raceScore, const float scoreRate)
{
    // hack score to get large exp
    // return 800000000;
    return (baseScore + raceScore) * scoreRate;
}