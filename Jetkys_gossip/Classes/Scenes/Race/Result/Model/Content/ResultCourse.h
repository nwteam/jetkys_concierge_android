#ifndef __syanago__ResultCourse__
#define __syanago__ResultCourse__

#include "cocos2d.h"
#include "CourseModel.h"

USING_NS_CC;

namespace ResultContent{
    class ResultCourse
    {
    public:
        ResultCourse(const std::shared_ptr<CourseModel>& courseModel, const bool isRankingEvent, std::vector<int> masterCharacterIdsOfRank)
        {
            _courseModel = courseModel;
            _isRankingEvent = isRankingEvent;
            _masterCharacterIdsOfRank = masterCharacterIdsOfRank;
        };
        
        const std::string getCharacterSDFilePath(const int exportPlaceNumber);
        
        CC_SYNTHESIZE_READONLY(std::shared_ptr<CourseModel>, _courseModel, CourseModel);
        CC_SYNTHESIZE_READONLY(bool, _isRankingEvent, IsRankingEvent);
        CC_SYNTHESIZE_READONLY(std::vector<int>, _masterCharacterIdsOfRank, MasterCharacterIdsOfRank);
    };
}

#endif
