#include "ResultCourse.h"

using namespace ResultContent;

const std::string ResultCourse::getCharacterSDFilePath(const int exportPlaceNumber)
{
    if (exportPlaceNumber > _masterCharacterIdsOfRank.size() || _masterCharacterIdsOfRank.size() < 1) {
        return "1_sdb";
    }
    if (_masterCharacterIdsOfRank.at(exportPlaceNumber - 1) < 1) {
        return "";
    }
    return StringUtils::format("%d_sdb", _masterCharacterIdsOfRank.at(exportPlaceNumber - 1));
}
