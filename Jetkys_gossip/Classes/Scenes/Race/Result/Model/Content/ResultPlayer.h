#ifndef __syanago__ResultPlayer__
#define __syanago__ResultPlayer__

#include "cocos2d.h"
#include "ResultClearThings.h"
#include "ResultScore.h"
#include "ResultHoldContent.h"

USING_NS_CC;
using namespace ResultPlayerContent;

namespace ResultContent{
    class ResultPlayer
    {
    public:
        ResultPlayer(const std::shared_ptr<ResultClearThings>& resultClearThings,
                     const std::shared_ptr<ResultScore>& resultScore,
                     const std::shared_ptr<ResultHoldContent>& resultHoldContent)
        {
            _resultClearThings = resultClearThings;
            _resultScore = resultScore;
            _resultHoldContent = resultHoldContent;
        };
        
        CC_SYNTHESIZE_READONLY(std::shared_ptr<ResultClearThings>, _resultClearThings, ResultClearThings);
        CC_SYNTHESIZE_READONLY(std::shared_ptr<ResultScore>, _resultScore, ResultScore);
        CC_SYNTHESIZE_READONLY(std::shared_ptr<ResultHoldContent>, _resultHoldContent, ResultHoldContent);
    };
}
#endif
