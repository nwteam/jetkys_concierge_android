#ifndef __syanago__RaceResultModel__
#define __syanago__RaceResultModel__

#include "cocos2d.h"
#include "HelpPlayer.h"
#include "CharacterModel.h"
#include "StatusManager.h"
#include "ResultCourse.h"
#include "ResultPlayer.h"

USING_NS_CC;

class RaceResultModel
{
public:
    RaceResultModel(const std::shared_ptr<ResultContent::ResultCourse>& resultCourse,
                    const std::shared_ptr<ResultContent::ResultPlayer>& resultPlayer,
                    const std::shared_ptr<HelpPlayer>& helpPlayer)
    {
        _resultCourse = resultCourse;
        resultPlayer->getResultScore()->setOtherScoreData(resultCourse->getCourseModel(),
                                                          resultPlayer->getResultClearThings()->getRankOfPlayer(),
                                                          resultPlayer->getResultClearThings()->getStopMeritCount());
        _resultPlayer = resultPlayer;
        _helpPlayer = helpPlayer;
    }
    
    CC_SYNTHESIZE_READONLY(std::shared_ptr<ResultContent::ResultCourse>, _resultCourse, ResultCourse);
    CC_SYNTHESIZE_READONLY(std::shared_ptr<ResultContent::ResultPlayer>, _resultPlayer, ResultPlayer);
    CC_SYNTHESIZE_READONLY(std::shared_ptr<HelpPlayer>, _helpPlayer, HelpPlayer);

    
    const std::string getRollOfFilePath(const int exportPlaceNumber);
};

#endif
