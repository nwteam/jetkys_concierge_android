#ifndef __syanago__RaceResultResponseModel__
#define __syanago__RaceResultResponseModel__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"
#include "HelpPlayer.h"
USING_NS_CC;

class RaceResultResponseModel
{
public:
    RaceResultResponseModel(Json* response, const std::shared_ptr<HelpPlayer>& helpPlayer);
    
    CC_SYNTHESIZE_READONLY(std::shared_ptr<HelpPlayer>, helpPlayer, HelpPlayer);
    CC_SYNTHESIZE_READONLY(std::string, eventMessage, EventMessage);
    CC_SYNTHESIZE_READONLY(std::string, missionClearMessage, MissionClearMessage);
    CC_SYNTHESIZE_READONLY(bool, isResponseCharacterData, IsResponseCharacterData);
    
    class Player
    {
    public:
        Player(Json* child);
        CC_SYNTHESIZE_READONLY(int, playerId, PlayerId);
        CC_SYNTHESIZE_READONLY(int, rank, Rank);
        CC_SYNTHESIZE_READONLY(int, fuel, Fuel);
        CC_SYNTHESIZE_READONLY(int, totalExp, TotalExp);
        CC_SYNTHESIZE_READONLY(int, freeCoin, FreeCoin);
        CC_SYNTHESIZE_READONLY(int, friendPoint, FriendPoint);
        CC_SYNTHESIZE_READONLY(int, currentCourseId, CurrentCourseId);
        
        int getCoinDifference();
        bool isPlayerRankUp();
    };
    CC_SYNTHESIZE_READONLY(std::shared_ptr<Player>, player, Player);
    
    class Character
    {
    public:
        Character(Json* child);
        CC_SYNTHESIZE_READONLY(int, playerCharacterId, PlayerCharacterId);
        CC_SYNTHESIZE_READONLY(int, playerId, PlayerId);
        CC_SYNTHESIZE_READONLY(int, characterId, CharacterId);
        CC_SYNTHESIZE_READONLY(std::string, name, Name);
        CC_SYNTHESIZE_READONLY(int, exp, Exp);
        CC_SYNTHESIZE_READONLY(int, level, Level);
        CC_SYNTHESIZE_READONLY(int, power, Power);
        CC_SYNTHESIZE_READONLY(int, technique, Technique);
        CC_SYNTHESIZE_READONLY(int, brake, Brake);
        CC_SYNTHESIZE_READONLY(int, dearValue, DearValue);
        
        CC_SYNTHESIZE_READONLY(bool, isGetCharacter, IsGetCharacter);
        
        int getExpDifference();
        int getLevelDifference();
        bool isLevelUp();
    };
    CC_SYNTHESIZE_READONLY(std::vector<std::shared_ptr<Character>>, characters, Characters);
    std::shared_ptr<Character> findCharacterDataOfCharacterId(const int characterId);
    std::shared_ptr<Character> getRewardCharacterData();
    
    class ItemUnit
    {
    public:
        ItemUnit(Json* child);
        CC_SYNTHESIZE_READONLY(int, itemUnitId, ItemUnitId);
        CC_SYNTHESIZE_READONLY(int, playerId, PlayerId);
        CC_SYNTHESIZE_READONLY(int, itemUnitKind, ItemUnitKind);
    };
    CC_SYNTHESIZE_READONLY(std::vector<std::shared_ptr<ItemUnit>>, itemUnits, ItemUnits);
    
    class Parts
    {
    public:
        Parts(Json* child);
        CC_SYNTHESIZE_READONLY(int, playerPartsId, PlayerPartsId);
        CC_SYNTHESIZE_READONLY(int, playerId, PlayerId);
        CC_SYNTHESIZE_READONLY(int, partsId, PartsId);
    };
    CC_SYNTHESIZE_READONLY(std::vector<std::shared_ptr<Parts>>, parts, Parts);
    
    CC_SYNTHESIZE_READONLY(std::vector<int>, firstGetCharacterIds, FirstGetCharacterIds);
private:
    void parsePlayerJsonData(Json* playerData);
    void parsePlayerCharactersJsonData(Json* playerCharacterData);
    void parsePlayerPartsJsonData(Json* playerPartsData);
    void parsePlayerItemUnitsJsonData(Json* playerItemUnitsData);
    void parseFirstGetCharacterIdsJsonData(Json* firstGetCharacterIdsData);
};

#endif