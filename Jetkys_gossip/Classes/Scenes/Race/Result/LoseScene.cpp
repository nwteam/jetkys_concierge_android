#include "LoseScene.h"
#include "FontDefines.h"
#include "jCommon.h"
#include "ScoreScene.h"
#include "SoundHelper.h"
#include "PlayerController.h"

LoseScene::LoseScene():
    _raceResultModel(nullptr)
{}

LoseScene::~LoseScene()
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName(_raceResultModel->getResultCourse()->getCharacterSDFilePath(4) + ".plist");
    Director::getInstance()->getTextureCache()->removeAllTextures();
    _raceResultModel = nullptr;
}

bool LoseScene::init(const std::shared_ptr<RaceResultModel>& model)
{
    if (!Layer::create()) {
        return false;
    }
    showBackground();
    showGoalFailureSprite();
    showTapSprite();
    showLoseSprite();
    showFailureStand();

    callLoseVoice();

    _raceResultModel = model;

    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(LoseScene::onTouchNextScene, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}

void LoseScene::showBackground()
{
    auto backgoround = Sprite::create("RankBGR.png");
    backgoround->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    backgoround->setTag(TAG_SPRITE::BACKGROUND);
    addChild(backgoround, Z_ORDER::Z_BACKGROUND);
}

void LoseScene::showGoalFailureSprite()
{
    auto goalSprite = Sprite::create("GOAL_UP.png");
    goalSprite->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 50.0 - goalSprite->getContentSize().height / 2));
    goalSprite->setTag(TAG_SPRITE::GOAL_FAILURE);
    addChild(goalSprite, Z_ORDER::Z_GOAL_FAILURE);
    auto goalFailureSprite = makeSprite("lose_rank_over_image.png");
    goalFailureSprite->setPosition(Vec2(goalSprite->getContentSize().width / 2, goalSprite->getContentSize().height / 2 + 20));
    goalSprite->addChild(goalFailureSprite);
}

void LoseScene::showTapSprite()
{
    auto tapSprite = Sprite::create("rank_boder.png");
    tapSprite->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + 50.0 + tapSprite->getContentSize().height / 2));
    tapSprite->setTag(TAG_SPRITE::TAP);
    addChild(tapSprite, Z_ORDER::Z_TAP);
    Label* label = Label::createWithTTF("画面をタップしてください", FONT_NAME_2, 25);
    label->setPosition(Point(tapSprite->getContentSize().width / 2, tapSprite->getContentSize().height / 2 - 12.5));
    label->setColor(Color3B::WHITE);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    tapSprite->addChild(label);
}

void LoseScene::showLoseSprite()
{
    Sprite* loseSprite = makeSprite("lose_rank4_new.png");
    loseSprite->setScale(0.8);
    loseSprite->setTag(TAG_SPRITE::LOSE);
    loseSprite->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 250));
    addChild(loseSprite, Z_ORDER::Z_LOSE);
}

void LoseScene::showFailureStand()
{
    auto failureStand = Sprite::create("lose_base.png");
    failureStand->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + failureStand->getContentSize().height / 2));
    failureStand->setTag(TAG_SPRITE::VICTORYSTAND);
    addChild(failureStand, Z_ORDER::Z_VICTORYSTAND);
    std::string sdName = StringUtils::format("%d_sdb", PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]->getCharactersId());
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(sdName + ".plist");
    auto fourthPlaceSD = createSDCharacter(sdName + ".png");
    fourthPlaceSD->setPosition(Vec2(failureStand->getContentSize().width / 2, 240));
    failureStand->addChild(fourthPlaceSD);
}

Sprite* LoseScene::createSDCharacter(std::string filePath)
{
    Sprite* result = makeSprite(filePath.c_str());
    result->setScale(0.7);
    result->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    return result;
}

void LoseScene::callLoseVoice()
{
    const int masterCharacterId = PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]->getCharactersId();
    runAction(Sequence::create(DelayTime::create(0.6), CallFunc::create([masterCharacterId]() {
        SoundHelper::shareHelper()->playCharacterVoiceEfect(masterCharacterId, 20);
    }), NULL));
}

bool LoseScene::onTouchNextScene(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (_raceResultModel == nullptr) {
        CC_ASSERT("_raceResultModel is null");
        return false;
    }
    Scene* scene = Scene::create();
    scene->addChild(ScoreScene::create(_raceResultModel));
    transitScene(scene);
    return true;
}