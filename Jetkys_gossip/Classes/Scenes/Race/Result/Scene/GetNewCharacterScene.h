#ifndef __syanago__GetNewCharacterScene__
#define __syanago__GetNewCharacterScene__

#include "cocos2d.h"
#include "create_func.h"
#include "HelpPlayer.h"

USING_NS_CC;

class GetNewCharacterScene : public Layer, public create_func<GetNewCharacterScene>
{
public:
    GetNewCharacterScene();
    ~GetNewCharacterScene();
    
    using create_func::create;
    bool init(const int masterCharacterId, const std::shared_ptr<HelpPlayer>& helpPlayer);
private:
    enum TAG_SPRITE{
        DROP_EFFECT = 0,
    };
    enum Z_ORDER{
        Z_DROP_EFFECT = 0,
    };
    std::shared_ptr<HelpPlayer> _helpPlayer;
    
    void showDropEffect(const int masterCharacterId);
    void endDropEffectCallBack();
};

#endif