#include "GetNewCharacterScene.h"
#include "DropEffectLayer.h"
#include "MainScene.h"
#include "RequestFriendScene.h"

GetNewCharacterScene::GetNewCharacterScene():
    _helpPlayer(nullptr)
{}

GetNewCharacterScene::~GetNewCharacterScene()
{
    _helpPlayer = nullptr;
    Director::getInstance()->getTextureCache()->removeAllTextures();
}

bool GetNewCharacterScene::init(const int masterCharacterId, const std::shared_ptr<HelpPlayer>& helpPlayer)
{
    if (!Layer::create()) {
        return false;
    }
    _helpPlayer = helpPlayer;

    showDropEffect(masterCharacterId);

    return true;
}
void GetNewCharacterScene::showDropEffect(const int masterCharacterId)
{
    auto dropEffectLayer = DropEffectLayer::create(DROP_CHARACTER, masterCharacterId, true, CC_CALLBACK_0(GetNewCharacterScene::endDropEffectCallBack, this));
    dropEffectLayer->setTag(TAG_SPRITE::DROP_EFFECT);
    addChild(dropEffectLayer, Z_ORDER::Z_DROP_EFFECT);
}

void GetNewCharacterScene::endDropEffectCallBack()
{
    if (_helpPlayer == nullptr) {
        auto race = MainScene::createScene();
        transitScene(race);
    } else   {
        Scene* scene = Scene::create();
        scene->addChild(RequestFriendScene::create(_helpPlayer));
        transitScene(scene);
    }
}