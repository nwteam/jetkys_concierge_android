#include "RequestFriendScene.h"
#include "FontDefines.h"
#include "jCommon.h"
#include "TimeConverter.h"
#include "SoundHelper.h"
#include "MainScene.h"

#include "MenuTouch.h"

RequestFriendScene::RequestFriendScene():
    progress(nullptr), request(nullptr), _helpPlayer(nullptr)
{}

RequestFriendScene::~RequestFriendScene()
{
    delete progress;
    delete request;
    progress = nullptr;
    request = nullptr;
    _helpPlayer = nullptr;
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("icon.plist");
    Director::getInstance()->getTextureCache()->removeAllTextures();

}

bool RequestFriendScene::init(const std::shared_ptr<HelpPlayer>& helpPlayer)
{
    if (!Layer::init()) {
        return false;
    }
    showBackground();
    showRequestFriendView(helpPlayer);

    _helpPlayer = helpPlayer;

    return true;
}

void RequestFriendScene::showBackground()
{
    auto background = Sprite::create("home_bg.png");
    background->setTag(TAG_SPRITE::BACKGROUND);
    background->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    addChild(background, Z_ORDER::Z_BACKGROUND);
}

void RequestFriendScene::showRequestFriendView(const std::shared_ptr<HelpPlayer>& helpPlayer)
{
    auto viewBase = createRequestFriendView(helpPlayer);
    viewBase->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    addChild(viewBase);
}

Sprite* RequestFriendScene::createRequestFriendView(const std::shared_ptr<HelpPlayer>& helpPlayer)
{
    Sprite* result = Sprite::create("addFrBgr.png");
    //**
    //未改修
    auto title1 = Label::createWithTTF("助っ人してくれたプレイヤー", FONT_NAME_2, 36);
    title1->setPosition(Point(result->getContentSize().width / 2, result->getContentSize().height - title1->getContentSize().height / 2 - 30.0));
    title1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->addChild(title1);

    auto title2 = Label::createWithTTF("フレンド申請を行いますか？", FONT_NAME_2, 28);
    title2->setPosition(Point(result->getContentSize().width / 2, result->getContentSize().height / 2 - title2->getContentSize().height / 2 - 50.0));
    result->addChild(title2);
    title2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));



    auto helpPlayerBase = Sprite::create("itemFrBgr.png");
    helpPlayerBase->setPosition(Point(result->getContentSize().width / 2, result->getContentSize().height / 2 + helpPlayerBase->getContentSize().height / 2 - 30.0));
    result->addChild(helpPlayerBase);

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon.plist");
    auto iconHelp = makeSprite(StringUtils::format("%d_i.png", helpPlayer->getCharacterId()).c_str());
    iconHelp->setPosition(Point(iconHelp->getContentSize().width / 2 + 10.0, helpPlayerBase->getContentSize().height / 2));
    iconHelp->setScale(1.2, 1.2);
    helpPlayerBase->addChild(iconHelp);

    auto playerNameLabel = Label::createWithTTF(helpPlayer->getName().c_str(), FONT_NAME_2, 32);
    playerNameLabel->setAnchorPoint(Point(0.0, 0.5));
    playerNameLabel->setPosition(Point(iconHelp->getContentSize().width + 35.0, helpPlayerBase->getContentSize().height - playerNameLabel->getContentSize().height / 2 - 15.0));
    playerNameLabel->setColor(COLOR_YELLOW);
    helpPlayerBase->addChild(playerNameLabel);

    auto playerRankLabel = Label::createWithTTF(StringUtils::format("ランク %zd", helpPlayer->getRank()).c_str(), FONT_NAME_2, 22);
    playerRankLabel->setAnchorPoint(Point(1.0, 0.5));
    playerRankLabel->setPosition(Point(helpPlayerBase->getContentSize().width - 15.0, playerNameLabel->getPosition().y + 5));
    helpPlayerBase->addChild(playerRankLabel);

    auto playerLoginLabel = Label::createWithTTF(TimeConverter::convertLastLogInTimeString(helpPlayer->getLastLoginTime()).c_str(), FONT_NAME_2, 22);
    playerLoginLabel->setAnchorPoint(Point(0.0, 0.5));
    playerLoginLabel->setPosition(Point(iconHelp->getContentSize().width + 35.0, 25.0));
    helpPlayerBase->addChild(playerLoginLabel);

    std::string playerLevel = StringUtils::format("Lv %d", helpPlayer->getLevel());
    auto playerLevelLabel = Label::createWithTTF(playerLevel.c_str(), FONT_NAME_2, 18);
    playerLevelLabel->setAnchorPoint(Point(0.5, 0.0));
    playerLevelLabel->setPosition(Point(iconHelp->getContentSize().width / 2, -27));
    iconHelp->addChild(playerLevelLabel);
    playerLevelLabel->enableOutline(Color4B(0, 0, 0, 255), 1);



    auto addBtn = makeMenuItem("addbtn.png", CC_CALLBACK_0(RequestFriendScene::onDecisionCallBack, this));
    addBtn->setPosition(Point(-addBtn->getContentSize().width / 2 - 15, -result->getContentSize().height / 2 + 25 + addBtn->getContentSize().height / 2));
    auto textBt1 = Label::createWithTTF("はい", FONT_NAME_2, 32);
    textBt1->setPosition(Point(addBtn->getContentSize().width / 2, addBtn->getContentSize().height / 2 - 16));
    textBt1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addBtn->addChild(textBt1);

    auto cancelBtn = makeMenuItem("addbtn.png", CC_CALLBACK_0(RequestFriendScene::onCancelCallBack, this));
    cancelBtn->setPosition(Point(cancelBtn->getContentSize().width / 2 + 15, -result->getContentSize().height / 2 + 25 + cancelBtn->getContentSize().height / 2));
    auto textBt2 = Label::createWithTTF("いいえ", FONT_NAME_2, 32);
    textBt2->setPosition(Point(cancelBtn->getContentSize().width / 2, cancelBtn->getContentSize().height / 2 - 16));
    textBt2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    cancelBtn->addChild(textBt2);

    auto menu = MenuTouch::create(addBtn, cancelBtn, NULL);
    addChild(menu, 1);
    //**
    return result;
}

void RequestFriendScene::onDecisionCallBack()
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
    progress->onStart();
    request->setFriendRequest(_helpPlayer->getId(), CC_CALLBACK_1(RequestFriendScene::onResponseSetFriendRequest, this));
}

void RequestFriendScene::onResponseSetFriendRequest(Json* response)
{
    std::string result = Json_getString(Json_getItem(response, "set_friend_request"), "result", "false");
    if (result != "true") {
        return;
    }
    goToMainScene();
}

void RequestFriendScene::onCancelCallBack()
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
    goToMainScene();
}

void RequestFriendScene::goToMainScene()
{
    transitScene(MainScene::createScene());
}