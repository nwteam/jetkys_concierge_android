#include "RewardScene.h"
#include "FontDefines.h"
#include "jCommon.h"
#include "SoundHelper.h"
#include "MainScene.h"
#include "RewardItemUnitDialog.h"
#include "RewardPartsDialog.h"
#include "MessageDialog.h"
#include "UserDefaultManager.h"
#include "RewardCharacterSprite.h"
#include "RequestFriendScene.h"
#include "GetNewCharacterScene.h"
#include "ItemUnitModel.h"

RewardScene::RewardScene():
    _raceResultResponseModel(nullptr)
{}

RewardScene::~RewardScene()
{
    _raceResultResponseModel = nullptr;
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("icon.plist");
    Director::getInstance()->getTextureCache()->removeAllTextures();
}

bool RewardScene::init(const std::shared_ptr<RaceResultResponseModel>& model)
{
    if (!Layer::create()) {
        return false;
    }

    showBackground();
    showRewardSprite();
    showTapSprite();

    showReward(model);
    showGetCoin(model->getPlayer()->getCoinDifference());
    UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::IS_RANK_UP, model->getPlayer()->isPlayerRankUp());
    Vec2 ribonPosition = Vec2(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + getChildByTag(TAG_SPRITE::TAP)->getContentSize().height + 120.0 + getChildByTag(TAG_SPRITE::COIN)->getContentSize().height));
    if (model->getRewardCharacterData() != nullptr) {
        ribonPosition = showGetNewCharacterRibon(ribonPosition);
    }
    if (model->getItemUnits().size() > 0) {
        for (int i = 0; i < model->getItemUnits().size(); i++) {
            ribonPosition = showGetItemUnitRibon(model->getItemUnits().at(i)->getItemUnitId(), ribonPosition);
        }
    }
    _raceResultResponseModel = model;

    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(RewardScene::onTapScene, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}

void RewardScene::showBackground()
{
    auto backgoround = Sprite::create("RankBGR.png");
    backgoround->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    backgoround->setTag(TAG_SPRITE::BACKGROUND);
    addChild(backgoround, Z_ORDER::Z_BACKGROUND);
}

void RewardScene::showRewardSprite()
{
    auto goalSprite = Sprite::create("expLogo.png");
    goalSprite->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 50.0 - goalSprite->getContentSize().height / 2));
    goalSprite->setTag(TAG_SPRITE::REWARD_LOGO);
    addChild(goalSprite, Z_ORDER::Z_REWARD_LOGO);
}

void RewardScene::showTapSprite()
{
    auto tapSprite = Sprite::create("rank_boder.png");
    tapSprite->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + 50.0 + tapSprite->getContentSize().height / 2));
    tapSprite->setTag(TAG_SPRITE::TAP);
    addChild(tapSprite, Z_ORDER::Z_TAP);
    Label* label = Label::createWithTTF("画面をタップしてください", FONT_NAME_2, 25);
    label->setPosition(Point(tapSprite->getContentSize().width / 2, tapSprite->getContentSize().height / 2 - 12.5));
    label->setColor(Color3B::WHITE);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    tapSprite->addChild(label);
}

void RewardScene::showReward(const std::shared_ptr<RaceResultResponseModel>& model)
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon.plist");
    Vec2 position = Vec2(getChildByTag(TAG_SPRITE::REWARD_LOGO)->getPosition()) - Vec2(0, getChildByTag(TAG_SPRITE::REWARD_LOGO)->getContentSize().height / 2 + 60);
    position = showCharacterStatus(position, model->findCharacterDataOfCharacterId(PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()));
    position = showCharacterStatus(position, model->findCharacterDataOfCharacterId(PLAYERCONTROLLER->_playerTeams->getSupport1CharacterId()));
    position = showCharacterStatus(position, model->findCharacterDataOfCharacterId(PLAYERCONTROLLER->_playerTeams->getSupport2CharacterId()));
    position = showCharacterStatus(position, model->findCharacterDataOfCharacterId(PLAYERCONTROLLER->_playerTeams->getSupport3CharacterId()));
}

Point RewardScene::showCharacterStatus(Point position, const std::shared_ptr<RaceResultResponseModel::Character>& model)
{
    if (model != nullptr) {
        auto characterDataSprite = RewardCharacterSprite::createCharacterSprite(model);
        characterDataSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
        characterDataSprite->setTag(TAG_SPRITE::REWARD);
        characterDataSprite->setPosition(position);
        addChild(characterDataSprite, Z_ORDER::Z_REWARD);
        position = Vec2(position - Vec2(0, 113));
    }
    return position;
}

void RewardScene::showGetCoin(const int getCoin)
{
    auto coinBase = Sprite::create("race_result_coment_base.png");
    coinBase->setTag(TAG_SPRITE::COIN);
    coinBase->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + getChildByTag(TAG_SPRITE::TAP)->getContentSize().height + 100.0 + coinBase->getContentSize().height / 2));
    addChild(coinBase, Z_ORDER::Z_COIN);

    Label* coinLabel = Label::createWithTTF(StringUtils::format("獲得コイン %d 枚", getCoin), FONT_NAME_2, 25);
    coinLabel->setPosition(Point(coinBase->getContentSize().width / 2, coinBase->getContentSize().height - coinLabel->getContentSize().height / 2 - 14.5));
    coinLabel->setColor(Color3B::WHITE);
    coinLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    coinBase->addChild(coinLabel);
}

Vec2 RewardScene::showGetNewCharacterRibon(Vec2 position)
{
    auto getCharacterBase = Sprite::create("gacha_text_base.png");
    getCharacterBase->setTag(TAG_SPRITE::RIBON);
    getCharacterBase->setPosition(position + Vec2(0, getCharacterBase->getContentSize().height / 2));
    addChild(getCharacterBase, Z_ORDER::Z_RIBON);
    Label* getCharacterLabel = Label::createWithTTF("新しい車なごに出会いました！", FONT_NAME_2, 30);
    getCharacterLabel->setPosition(getCharacterBase->getContentSize().width / 2, getCharacterBase->getContentSize().height / 2 - 15);
    getCharacterLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    getCharacterBase->addChild(getCharacterLabel);
    return position + Vec2(0, 10 + getCharacterBase->getContentSize().height);
}

Vec2 RewardScene::showGetItemUnitRibon(const int ItemUnitId, Vec2 position)
{
    auto getItemUnitBase = Sprite::create("gacha_text_base.png");
    getItemUnitBase->setTag(TAG_SPRITE::RIBON);
    getItemUnitBase->setPosition(position + Vec2(0, getItemUnitBase->getContentSize().height / 2));
    addChild(getItemUnitBase, Z_ORDER::Z_RIBON);
    Label* getItemUnitLabel = Label::createWithTTF("宝物「" + ItemUnitModel::find(ItemUnitId)->getName() + "」を手に入れました!", FONT_NAME_2, 30);
    getItemUnitLabel->setPosition(getItemUnitBase->getContentSize().width / 2, getItemUnitBase->getContentSize().height / 2 - 15);
    getItemUnitLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    getItemUnitBase->addChild(getItemUnitLabel);
    return position + Vec2(0, 10 + getItemUnitBase->getContentSize().height);
}

bool RewardScene::onTapScene(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (_raceResultResponseModel == nullptr) {
        CC_ASSERT("_raceResultResponseModel is null");
        return false;
    }
    showGetItemUnitDialog();
    return true;
}

void RewardScene::showGetItemUnitDialog()
{
    if (_raceResultResponseModel->getItemUnits().size() > 0) {
        auto dialog = RewardItemUnitDialog::create(_raceResultResponseModel->getItemUnits().at(0)->getItemUnitId(), _raceResultResponseModel->getItemUnits().at(0)->getItemUnitKind(), CC_CALLBACK_0(RewardScene::showGetPartsDialog, this));
        dialog->setTag(TAG_SPRITE::DIALOG);
        addChild(dialog, Z_ORDER::Z_DIALOG);
        return;
    }
    showGetPartsDialog();
}

void RewardScene::showGetPartsDialog()
{
    if (_raceResultResponseModel->getParts().size() > 0) {
        auto dialog = RewardPartsDialog::create(_raceResultResponseModel->getParts().at(0)->getPartsId(), CC_CALLBACK_0(RewardScene::showMissionMessageDialog, this));
        dialog->setTag(TAG_SPRITE::DIALOG);
        addChild(dialog, Z_ORDER::Z_DIALOG);
        return;
    }
    showMissionMessageDialog();
}

void RewardScene::showMissionMessageDialog()
{
    if (_raceResultResponseModel->getMissionClearMessage() != "") {
        auto dialog = MessageDialog::create("ミッション", _raceResultResponseModel->getMissionClearMessage(), CC_CALLBACK_0(RewardScene::showEventMessageDialog, this));
        dialog->setTag(TAG_SPRITE::DIALOG);
        addChild(dialog, Z_ORDER::Z_DIALOG);
        return;
    }
    showEventMessageDialog();
}

void RewardScene::showEventMessageDialog()
{
    if (_raceResultResponseModel->getEventMessage() != "") {
        auto dialog = MessageDialog::create("イベント", _raceResultResponseModel->getEventMessage(), CC_CALLBACK_0(RewardScene::goToNextScene, this));
        dialog->setTag(TAG_SPRITE::DIALOG);
        addChild(dialog, Z_ORDER::Z_DIALOG);
        return;
    }
    goToNextScene();
}

void RewardScene::goToNextScene()
{
    if (_raceResultResponseModel->getRewardCharacterData() != nullptr) {
        Scene* scene = Scene::create();
        scene->addChild(GetNewCharacterScene::create(_raceResultResponseModel->getRewardCharacterData()->getCharacterId(), _raceResultResponseModel->getHelpPlayer()));
        Director::getInstance()->replaceScene(scene);
        return;
    }

    if (_raceResultResponseModel->getHelpPlayer() != nullptr) {
        Scene* scene = Scene::create();
        scene->addChild(RequestFriendScene::create(_raceResultResponseModel->getHelpPlayer()));
        transitScene(scene);
        return;
    }
    transitScene(MainScene::createScene());
}