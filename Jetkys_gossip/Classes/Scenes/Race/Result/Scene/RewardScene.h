#ifndef __syanago__RewardScene__
#define __syanago__RewardScene__

#include "cocos2d.h"
#include "create_func.h"
#include "RaceResultResponseModel.h"

USING_NS_CC;

class RewardScene : public Layer, public create_func<RewardScene>
{
public:
    bool init(const std::shared_ptr<RaceResultResponseModel>& model);
    using create_func::create;
    
    RewardScene();
    ~RewardScene();
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        REWARD,
        REWARD_LOGO,
        COIN,
        RIBON,
        TAP,
        DIALOG,
    };
    enum Z_ORDER {
        Z_BACKGROUND = 0,
        Z_REWARD,
        Z_REWARD_LOGO,
        Z_COIN,
        Z_RIBON,
        Z_TAP,
        Z_DIALOG,
    };
    std::shared_ptr<RaceResultResponseModel> _raceResultResponseModel;
    
    void showBackground();
    void showRewardSprite();
    void showTapSprite();
    
    
    void showReward(const std::shared_ptr<RaceResultResponseModel>& model);
    Point showCharacterStatus(Point position, const std::shared_ptr<RaceResultResponseModel::Character>& model);
    void showGetCoin(const int getCoin);
    Vec2 showGetNewCharacterRibon(Vec2 position);
    Vec2 showGetItemUnitRibon(const int ItemUnitId, Vec2 position);
    
    bool onTapScene(cocos2d::Touch* touch,cocos2d::Event* event);
    void showGetItemUnitDialog();
    void showGetPartsDialog();
    void showMissionMessageDialog();
    void showEventMessageDialog();
    void goToNextScene();
};

#endif
