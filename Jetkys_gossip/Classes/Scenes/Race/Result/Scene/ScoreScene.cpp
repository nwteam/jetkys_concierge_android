#include "ScoreScene.h"
#include "FontDefines.h"
#include "TweetButton.h"
#include "TweetMessage.h"
#include "RewardScene.h"
#include "RequestFriendScene.h"
#include "MainScene.h"
#include "jCommon.h"
#include "RaceResultResponseModel.h"

ScoreScene::ScoreScene():
    _raceResultModel(nullptr)
    , request(nullptr)
    , progress(nullptr)
{
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
}

ScoreScene::~ScoreScene()
{
    delete progress;
    delete request;
    progress = nullptr;
    request = nullptr;
    _raceResultModel = nullptr;
}

bool ScoreScene::init(const std::shared_ptr<RaceResultModel>& model)
{
    if (!Layer::create()) {
        return false;
    }
    _raceResultModel = model;

    showBackground();
    showScoreLogo();
    showTapSprite();
    showScoreView(model);
    showTweetButton(model);


    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(ScoreScene::onTouchNextScene, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}

void ScoreScene::showBackground()
{
    auto backgoround = Sprite::create("RankBGR.png");
    backgoround->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    backgoround->setTag(TAG_SPRITE::BACKGROUND);
    addChild(backgoround, Z_ORDER::Z_BACKGROUND);
}

void ScoreScene::showScoreLogo()
{
    auto scoreLogo = Sprite::create("ScoreLogo.png");
    scoreLogo->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 50.0 - scoreLogo->getContentSize().height / 2));
    scoreLogo->setTag(TAG_SPRITE::SCORE_LOGO);
    addChild(scoreLogo, Z_ORDER::Z_SCORE_LOGO);
}

void ScoreScene::showTapSprite()
{
    auto tapSprite = Sprite::create("rank_boder.png");
    tapSprite->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + 50.0 + tapSprite->getContentSize().height / 2));
    tapSprite->setTag(TAG_SPRITE::TAP);
    addChild(tapSprite, Z_ORDER::Z_TAP);
    Label* label = Label::createWithTTF("画面をタップしてください", FONT_NAME_2, 25);
    label->setPosition(Point(tapSprite->getContentSize().width / 2, tapSprite->getContentSize().height / 2 - 12.5));
    label->setColor(Color3B::WHITE);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    tapSprite->addChild(label);
}

void ScoreScene::showScoreView(const std::shared_ptr<RaceResultModel>& model)
{
    auto scoreViewBase = Sprite::create("TextDetail.png");
    scoreViewBase->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    scoreViewBase->setTag(TAG_SPRITE::SCORE);
    addChild(scoreViewBase, Z_ORDER::Z_SCORE);

    auto base = Sprite::create("get_score_base.png");
    scoreViewBase->addChild(base, -1);
    base->setPosition(Point(scoreViewBase->getContentSize() / 2) - Vec2(0, 25));

    auto scoreModel = model->getResultPlayer()->getResultScore();

    Label* stage = Label::createWithTTF(StringUtils::format("%d", scoreModel->getBaseScore()), FONT_NAME_2, 40);
    stage->setColor(Color3B::BLACK);
    stage->setAnchorPoint(Vec2(1.0, 0.5));
    stage->setPosition(Point(scoreViewBase->getContentSize().width - 10, scoreViewBase->getContentSize().height - stage->getContentSize().height / 2 + 5));
    scoreViewBase->addChild(stage, 1);

    Label* bonus = Label::createWithTTF(StringUtils::format("%d", scoreModel->getRaceScore()), FONT_NAME_2, 35);
    bonus->setColor(Color3B::BLACK);
    bonus->setAnchorPoint(Vec2(1.0, 0.5));
    bonus->setPosition(Point(scoreViewBase->getContentSize().width - 10, stage->getPosition().y - 10 - bonus->getContentSize().height / 2 - 5));
    scoreViewBase->addChild(bonus, 1);

    Label* scaleLb = Label::createWithTTF(StringUtils::format("× %.01f", scoreModel->getScoreRate()), FONT_NAME_2, 35);
    scaleLb->setColor(Color3B::BLACK);
    scaleLb->setAnchorPoint(Vec2(1.0, 0.5));
    scaleLb->setPosition(Point(scoreViewBase->getContentSize().width - 10, bonus->getPosition().y - 15 - scaleLb->getContentSize().height / 2));
    scoreViewBase->addChild(scaleLb, 1);

    Label* total = Label::createWithTTF(StringUtils::format("%d", scoreModel->getTotalScore()), FONT_NAME_2, 40);
    total->setColor(Color3B::BLACK);
    total->setAnchorPoint(Vec2(1.0, 0.5));
    total->setPosition(Point(scoreViewBase->getContentSize().width - 10, scaleLb->getPosition().y - 55 - total->getContentSize().height / 2));
    scoreViewBase->addChild(total, 1);

    Label* TurnLb = Label::createWithTTF(StringUtils::format("ターン数 :    %d", model->getResultPlayer()->getResultClearThings()->getGoalTurnCount()), FONT_NAME_2, 40);
    TurnLb->setColor(Color3B::BLACK);
    TurnLb->setPosition(Point(Director::getInstance()->getWinSize().width / 2, total->getPosition().y - 20 - TurnLb->getContentSize().height / 2));
    scoreViewBase->addChild(TurnLb, 1);
}

void ScoreScene::showTweetButton(const std::shared_ptr<RaceResultModel>& model)
{
    auto helPlayer = model->getHelpPlayer();
    CCLOG("[ScoreScene::showTweetButton]model->getHelpPlayer()->getCharacterId():%d", helPlayer->getCharacterId());

    Twitter::TweetButton* tweetButton = Twitter::TweetButton::create(TweetMessage::getScoreTweetMessage(model->getHelpPlayer()->getCharacterId(),
                                                                                                        model->getResultCourse()->getCourseModel()->getID(),
                                                                                                        model->getResultPlayer()->getResultClearThings()->getGoalTurnCount()), "tweet_button.png");
    tweetButton->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, -(getChildByTag(TAG_SPRITE::SCORE)->getContentSize().height / 2 + 130 + tweetButton->getContentSize().height / 2)));
    tweetButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    tweetButton->setTag(TAG_SPRITE::TWEET);
    addChild(tweetButton, Z_ORDER::Z_TWEET);
}

bool ScoreScene::onTouchNextScene(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (_raceResultModel == nullptr) {
        CC_ASSERT("_raceResultModel is null");
        return false;
    }
    progress->onStart();
    request->setRaceResult(_raceResultModel->getResultCourse()->getCourseModel()->getID(),
                           _raceResultModel->getResultPlayer()->getResultClearThings()->getRankOfPlayer(),
                           _raceResultModel->getResultPlayer()->getResultClearThings()->getGoalTurnCount(),
                           _raceResultModel->getResultPlayer()->getResultScore()->getTotalScore(),
                           _raceResultModel->getResultPlayer()->getResultClearThings()->getStopMeritCount(),
                           _raceResultModel->getResultPlayer()->getResultHoldContent()->getHoldItemCount(),
                           _raceResultModel->getResultPlayer()->getResultHoldContent()->getHoldSkillChargeCharacterCount(),
                           _raceResultModel->getResultPlayer()->getResultClearThings()->getNumOfgridToGoal(),
                           _raceResultModel->getResultPlayer()->getResultClearThings()->getSkillUsedCount(),
                           (_raceResultModel->getHelpPlayer())->getId(),
                           _raceResultModel->getResultCourse()->getMasterCharacterIdsOfRank().at(0),
                           _raceResultModel->getResultCourse()->getMasterCharacterIdsOfRank().at(1),
                           _raceResultModel->getResultCourse()->getMasterCharacterIdsOfRank().at(2),
                           _raceResultModel->getResultCourse()->getMasterCharacterIdsOfRank().at(3),
                           _raceResultModel->getResultPlayer()->getResultClearThings()->getStopedGridIdsArrayString(),
                           CC_CALLBACK_1(ScoreScene::onResponseRaceResult, this));
    return true;
}

void ScoreScene::onResponseRaceResult(Json* response)
{
    std::shared_ptr<RaceResultResponseModel>model(new RaceResultResponseModel(response, _raceResultModel->getHelpPlayer()));
    if (model->getIsResponseCharacterData() == false) {
        if (model->getHelpPlayer() != nullptr) {
            Scene* scene = Scene::create();
            scene->addChild(RequestFriendScene::create(model->getHelpPlayer()));
            transitScene(scene);
        } else {
            transitScene(MainScene::createScene());
        }
    } else {
        Scene* scene = Scene::create();
        scene->addChild(RewardScene::create(model));
        transitScene(scene);
    }
}