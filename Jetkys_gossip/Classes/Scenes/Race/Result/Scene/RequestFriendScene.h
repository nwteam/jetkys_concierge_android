#ifndef __syanago__RequestFriendScene__
#define __syanago__RequestFriendScene__

#include "create_func.h"
#include "cocos2d.h"
#include "HelpPlayer.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"

USING_NS_CC;
using namespace SyanagoAPI;

class RequestFriendScene : public create_func<RequestFriendScene>, public Layer
{
public:
    RequestFriendScene();
    ~RequestFriendScene();
    bool init(const std::shared_ptr<HelpPlayer>& helpPlayer);
    using create_func::create;
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        DIALOG,
    };
    enum Z_ORDER{
        Z_BACKGROUND = 0,
        Z_DIALOG,
    };
    enum TAG_SPRITE_VIEW{
        BASE = 0,
    };
    enum Z_ORDER_VIEW{
        Z_BASE = 0,
    };
    
    RequestAPI* request;
    DefaultProgress* progress;
    std::shared_ptr<HelpPlayer> _helpPlayer;
    
    void showBackground();
    void showRequestFriendView(const std::shared_ptr<HelpPlayer>& helpPlayer);
    
    Sprite* createRequestFriendView(const std::shared_ptr<HelpPlayer>& helpPlayer);
    
    void onDecisionCallBack();
    void onResponseSetFriendRequest(Json* response);
    void onCancelCallBack();
    
    void goToMainScene();
};

#endif
