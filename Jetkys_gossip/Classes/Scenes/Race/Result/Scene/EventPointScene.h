#ifndef __syanago__EventPointScene__
#define __syanago__EventPointScene__

#include "create_func.h"
#include "cocos2d.h"
#include "RaceResultModel.h"

USING_NS_CC;

class EventPointScene : public create_func<EventPointScene>, public Layer
{
public:
    EventPointScene();
    ~EventPointScene();
    bool init(const std::shared_ptr<RaceResultModel>& model);
    using create_func::create;
    
private:
    void showBackground();
    void showLogo();
    void showPointText();
    void showPleaseTap();
    void showRankPoint(int rankPoint);
    void showScorePoint(int scorePoint);
    void showTurnPoint(int turnPoint);
    void showPlayPoint(int playPoint);
    void showTotalPoint(int totalPoint);
    void showCourseName(int courseId);
    int calculateRankPoint(int userRank);
    int calculateScorePoint(int totalScore);
    int calculateTurnPoint(int goalTotalTurn);
    int calculatePlayPoint(int numOfItem, int maxChainMeritCount, int numOfcharge);
    
    bool onTouchNextScene(cocos2d::Touch* touch,cocos2d::Event* event);
    
    enum TAG_SPRITE {
        BACKGROUND = 1
    };
    std::shared_ptr<RaceResultModel> _raceResultModel;
    Size win;
};

#endif
