#include "EventPointScene.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include "TelopScrollView.h"
#include "ScoreScene.h"
#include "MapModel.h"

EventPointScene::EventPointScene():
    _raceResultModel(nullptr)
{}

EventPointScene::~EventPointScene()
{
    _raceResultModel = nullptr;
}

bool EventPointScene::init(const std::shared_ptr<RaceResultModel>& model)
{
    if (!Layer::init()) {
        return false;
    }

    win = Director::getInstance()->getWinSize();
    _raceResultModel = model;
    showBackground();
    showLogo();
    showPointText();
    showPleaseTap();
    showCourseName(model->getResultCourse()->getCourseModel()->getID());

    int rankPoint = calculateRankPoint(model->getResultPlayer()->getResultClearThings()->getRankOfPlayer());
    showRankPoint(rankPoint);

    int scorePoint = calculateScorePoint(model->getResultPlayer()->getResultScore()->getTotalScore());
    showScorePoint(scorePoint);

    int turnPoint = calculateTurnPoint(model->getResultPlayer()->getResultClearThings()->getGoalTurnCount());
    showTurnPoint(turnPoint);

    int playPoint = calculatePlayPoint(model->getResultPlayer()->getResultHoldContent()->getHoldItemCount(), model->getResultPlayer()->getResultClearThings()->getStopMeritCount(), model->getResultPlayer()->getResultHoldContent()->getHoldSkillChargeCharacterCount());
    showPlayPoint(playPoint);

    showTotalPoint(rankPoint + scorePoint + turnPoint + playPoint);

    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(EventPointScene::onTouchNextScene, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}

void EventPointScene::showBackground()
{
    Sprite* bg = Sprite::create("RankBGR.png");
    bg->setPosition(Vec2(win.width / 2, win.height / 2));
    bg->setTag(BACKGROUND);
    addChild(bg);
}

void EventPointScene::showLogo()
{
    Sprite* bg = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BACKGROUND));
    Sprite* logo = Sprite::create("event_logo.png");
    logo->setPosition(Point(win.width / 2, bg->getContentSize().height - 50.0 - logo->getContentSize().height / 2));
    bg->addChild(logo);
}

void EventPointScene::showPointText()
{
    Sprite* bg = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BACKGROUND));
    Sprite* pointText = Sprite::create("event_point_text.png");
    pointText->setPosition(Point(win.width / 2, bg->getContentSize().height / 2));
    bg->addChild(pointText);
}

void EventPointScene::showPleaseTap()
{
    Sprite* bg = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BACKGROUND));
    Sprite* tapBg = Sprite::create("rank_boder.png");
    tapBg->setPosition(Point(win.width / 2, 50.0 + tapBg->getContentSize().height / 2));
    bg->addChild(tapBg);

    Label* tap = Label::createWithTTF("画面をタップしてください", FONT_NAME_2, 25);
    tap->setPosition(Point(tapBg->getContentSize().width / 2, tapBg->getContentSize().height / 2 - 12.5));
    tap->setColor(Color3B::WHITE);
    tap->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    tapBg->addChild(tap);
}

void EventPointScene::showRankPoint(int rankPoint)
{
    Sprite* bg = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BACKGROUND));
    const std::string pointStr = StringUtils::format("%d", rankPoint);
    Label* pointLabel = Label::createWithTTF(pointStr, FONT_NAME_2, 40);
    pointLabel->setColor(Color3B::BLACK);
    pointLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    pointLabel->setPosition(Point(bg->getContentSize().width - 80, 495));
    bg->addChild(pointLabel);
}

void EventPointScene::showScorePoint(int scorePoint)
{
    Sprite* bg = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BACKGROUND));
    const std::string pointStr = StringUtils::format("%d", scorePoint);
    Label* pointLabel = Label::createWithTTF(pointStr, FONT_NAME_2, 40);
    pointLabel->setColor(Color3B::BLACK);
    pointLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    pointLabel->setPosition(Point(bg->getContentSize().width - 80, 445));
    bg->addChild(pointLabel);
}

void EventPointScene::showTurnPoint(int turnPoint)
{
    Sprite* bg = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BACKGROUND));
    const std::string pointStr = StringUtils::format("%d", turnPoint);
    Label* pointLabel = Label::createWithTTF(pointStr, FONT_NAME_2, 40);
    pointLabel->setColor(Color3B::BLACK);
    pointLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    pointLabel->setPosition(Point(bg->getContentSize().width - 80, 395));
    bg->addChild(pointLabel);
}

void EventPointScene::showPlayPoint(int playPoint)
{
    Sprite* bg = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BACKGROUND));
    const std::string pointStr = StringUtils::format("%d", playPoint);
    Label* pointLabel = Label::createWithTTF(pointStr, FONT_NAME_2, 40);
    pointLabel->setColor(Color3B::BLACK);
    pointLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    pointLabel->setPosition(Point(bg->getContentSize().width - 80, 345));
    bg->addChild(pointLabel);
}

void EventPointScene::showTotalPoint(int totalPoint)
{
    Sprite* bg = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BACKGROUND));
    const std::string pointStr = StringUtils::format("%d", totalPoint);
    Label* pointLabel = Label::createWithTTF(pointStr, FONT_NAME_2, 40);
    pointLabel->setColor(Color3B::BLACK);
    pointLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
    pointLabel->setPosition(Point(bg->getContentSize().width - 80, 260));
    bg->addChild(pointLabel);
}

void EventPointScene::showCourseName(int courseId)
{
    Sprite* bg = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::BACKGROUND));
    std::shared_ptr<CourseModel>courseModel = CourseModel::find(courseId);

    auto courseNameLabel = Label::createWithTTF(MapModel::find(courseModel->getMapID())->getName() + " " + courseModel->getName(), FONT_NAME_2, 30);
    courseNameLabel->setColor(Color3B::BLACK);
    courseNameLabel->setPosition(Point(bg->getContentSize().width * 0.5,
                                       bg->getContentSize().height - courseNameLabel->getContentSize().height / 2 - 299));
    courseNameLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    bg->addChild(courseNameLabel);

    int width1 = courseNameLabel->getContentSize().width / 2;
    int width2;
    int designHeightPosi;
    auto coseInfoLabel = Label::createWithTTF(courseModel->getDescription(), FONT_NAME_2, 26);
    if (coseInfoLabel->getContentSize().width < Director::getInstance()->getWinSize().width - 100) {
        coseInfoLabel->setPosition(Point(bg->getContentSize().width * 0.5, courseNameLabel->getPosition().y - 15 - 5 - 10));
        coseInfoLabel->setHorizontalAlignment(TextHAlignment::CENTER);
        coseInfoLabel->setColor(Color3B::BLACK);
        bg->addChild(coseInfoLabel);
        width2 = coseInfoLabel->getContentSize().width / 2;
        designHeightPosi = courseNameLabel->getPosition().y - ((courseNameLabel->getPosition().y - coseInfoLabel->getPosition().y) / 2);
    } else  {
        TelopScrollView* telopView = TelopScrollView::initScrollTelop(courseModel->getDescription(),
                                                                      26,
                                                                      bg->getContentSize().width - 100,
                                                                      30,
                                                                      Color3B::BLACK);
        telopView->setPosition(Point(50,
                                     courseNameLabel->getPosition().y - 30));
        bg->addChild(telopView);
        width2 = (bg->getContentSize().width - 100) / 2;
        designHeightPosi = courseNameLabel->getPosition().y -  15;
    }

    int designWidth = 0;
    if (width1 > width2) {
        designWidth = width1;
    } else  {
        designWidth = width2;
    }

    auto designLabel1 = Label::createWithTTF("—", FONT_NAME_2, 30);

    designLabel1->setPosition(Point(bg->getContentSize().width / 2 + (designWidth + designLabel1->getContentSize().width / 2 + 10), designHeightPosi));
    designLabel1->setColor(Color3B::BLACK);
    bg->addChild(designLabel1);

    auto designLabel2 = Label::createWithTTF("—", FONT_NAME_2, 30);
    designLabel2->setPosition(Point(bg->getContentSize().width / 2 - (designWidth + designLabel2->getContentSize().width / 2 + 10), designHeightPosi));
    designLabel2->setColor(Color3B::BLACK);
    bg->addChild(designLabel2);

}

int EventPointScene::calculateRankPoint(int userRank)
{
    int point = 0;
    switch (userRank) {
    case 1:
        point = _raceResultModel->getResultCourse()->getCourseModel()->getRankingRank1Constant();
        break;
    case 2:
        point = _raceResultModel->getResultCourse()->getCourseModel()->getRankingRank2Constant();
        break;
    case 3:
        point = _raceResultModel->getResultCourse()->getCourseModel()->getRankingRank3Constant();
        break;
    }

    return point;
}

int EventPointScene::calculateScorePoint(int totalScore)
{
    int point;

    const int baseScore = _raceResultModel->getResultCourse()->getCourseModel()->getRankingBaseScore();
    const int scoreConstant = _raceResultModel->getResultCourse()->getCourseModel()->getRankingScoreConstant();
    point = (totalScore - baseScore) / scoreConstant;

    return point > 0 ? point : 0;
}

int EventPointScene::calculateTurnPoint(int goalTotalTurn)
{
    int point;

    const int baseTurn = _raceResultModel->getResultCourse()->getCourseModel()->getRankingBaseTurn();
    const int turnConstant = _raceResultModel->getResultCourse()->getCourseModel()->getRankingTurnConstant();
    point = (baseTurn - goalTotalTurn) * turnConstant;

    return point > 0 ? point : 0;
}

int EventPointScene::calculatePlayPoint(int numOfItem, int maxChainMeritCount, int numOfcharge)
{
    int point = 0;

    if (numOfItem > 0) {
        point += _raceResultModel->getResultCourse()->getCourseModel()->getRankingItemPoint();
    }

    point += maxChainMeritCount * _raceResultModel->getResultCourse()->getCourseModel()->getRankingMeritConstant();

    point += numOfcharge * _raceResultModel->getResultCourse()->getCourseModel()->getRankingSkillChargeConstant();

    return point;
}

bool EventPointScene::onTouchNextScene(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (_raceResultModel == nullptr) {
        CC_ASSERT("_raceResultModel is null");
        return false;
    }
    Scene* scene = Scene::create();
    scene->addChild(ScoreScene::create(_raceResultModel));
    transitScene(scene);
    return true;
}