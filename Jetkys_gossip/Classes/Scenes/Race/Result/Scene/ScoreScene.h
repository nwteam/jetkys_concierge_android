#ifndef __syanago__ScoreScene__
#define __syanago__ScoreScene__

#include "cocos2d.h"
#include "create_func.h"
#include "RaceResultModel.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

USING_NS_CC;
using namespace SyanagoAPI;

class ScoreScene : public Layer, public create_func<ScoreScene>
{
public:
    bool init(const std::shared_ptr<RaceResultModel>& model);
    using create_func::create;
    
    ScoreScene();
    ~ScoreScene();
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        SCORE,
        TWEET,
        SCORE_LOGO,
        TAP
    };
    enum Z_ORDER {
        Z_BACKGROUND = 0,
        Z_SCORE,
        Z_TWEET,
        Z_SCORE_LOGO,
        Z_TAP
    };
    std::shared_ptr<RaceResultModel> _raceResultModel;
    RequestAPI* request;
    DefaultProgress* progress;
    
    void showBackground();
    void showScoreLogo();
    void showTapSprite();
    void showScoreView(const std::shared_ptr<RaceResultModel>& model);
    void showTweetButton(const std::shared_ptr<RaceResultModel>& model);
    
    bool onTouchNextScene(cocos2d::Touch* touch,cocos2d::Event* event);
    
    void onResponseRaceResult(Json* response);
};

#endif
