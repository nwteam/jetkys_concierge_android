#ifndef __syanago__LoseScene__
#define __syanago__LoseScene__

#include "cocos2d.h"
#include "create_func.h"
#include "RaceResultModel.h"

USING_NS_CC;

class LoseScene : public Layer, public create_func<LoseScene>
{
public:
    bool init(const std::shared_ptr<RaceResultModel>& model);
    using create_func::create;
    
    LoseScene();
    ~LoseScene();
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        VICTORYSTAND,
        GOAL_FAILURE,
        LOSE,
        TAP,
    };
    enum Z_ORDER {
        Z_BACKGROUND = 0,
        Z_VICTORYSTAND,
        Z_GOAL_FAILURE,
        Z_LOSE,
        Z_TAP,
    };
    std::shared_ptr<RaceResultModel> _raceResultModel;
    std::vector<int> viewMasterCharacterId;
    
    void showBackground();
    void showGoalFailureSprite();
    void showTapSprite();
    void showLoseSprite();
    void showFailureStand();
    Sprite* createSDCharacter(std::string filePath);
    
    void callLoseVoice();
    
    bool onTouchNextScene(cocos2d::Touch* touch,cocos2d::Event* event);
};

#endif
