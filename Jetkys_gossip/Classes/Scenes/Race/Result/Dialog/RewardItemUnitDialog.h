#ifndef __syanago__RewardItemUnitDialog__
#define __syanago__RewardItemUnitDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class RewardItemUnitDialog : public Layer, public create_func<RewardItemUnitDialog>
{
public:
    typedef std::function<void()> OnCloseCallback;
    using create_func::create;
    bool init(const int masterItemUnitId, const int itemUnitKind, const OnCloseCallback& callBack);
    
    RewardItemUnitDialog():
    onCloseCallBack(nullptr)
    {}
    ~RewardItemUnitDialog();
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        BUTTON,
        TITLE,
    };
    enum Z_ORDER{
        Z_BUTTON = 0,
        Z_TITLE
    };
    EventListenerTouchOneByOne* _listener;
    OnCloseCallback onCloseCallBack;
    
    void showBackground();
    void showTitle();
    void showGetItemUnit(const int masterItemUnitId, const int itemUnitKind);
    Node* setStar(int numberOfStar,Point nodePossition);
    void showText();
    void showCloseButton();
    void onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type);
};

#endif