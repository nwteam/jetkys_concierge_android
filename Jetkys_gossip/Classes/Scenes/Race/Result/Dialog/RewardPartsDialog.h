#ifndef __syanago__RewardPartsDialog__
#define __syanago__RewardPartsDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class RewardPartsDialog : public Layer, public create_func<RewardPartsDialog>
{
public:
    typedef std::function<void()> OnCloseCallback;
    using create_func::create;
    bool init(const int masterPartsId, const OnCloseCallback& callBack);
    
    RewardPartsDialog():
    onCloseCallBack(nullptr)
    {}
    ~RewardPartsDialog();
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        BUTTON,
        TITLE,
    };
    enum Z_ORDER{
        Z_BUTTON = 0,
        Z_TITLE,
    };
    EventListenerTouchOneByOne* _listener;
    OnCloseCallback onCloseCallBack;
    
    void showBackground();
    void showTitle();
    void showParts(const int masterPartsId);
    Node* setStar(int numberOfStar,Point nodePossition);
    void showCloseButton();
    void onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type);
};

#endif
