#include "RewardItemUnitDialog.h"
#include "SoundHelper.h"
#include "FontDefines.h"
#include "ItemUnitModel.h"
#include "jCommon.h"

RewardItemUnitDialog::~RewardItemUnitDialog()
{
    onCloseCallBack = nullptr;
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("icon_treasure.plist");
}

bool RewardItemUnitDialog::init(const int masterItemUnitId, const int itemUnitKind, const OnCloseCallback& callBack)
{
    if (!Layer::init()) {
        return false;
    }
    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    showBackground();
    showCloseButton();
    showTitle();
    showGetItemUnit(masterItemUnitId, itemUnitKind);
    showText();

    onCloseCallBack = callBack;
    return true;
}

void RewardItemUnitDialog::showBackground()
{
    auto black = LayerColor::create(Color4B(Color3B::BLACK));
    black->setOpacity(180);
    addChild(black);
    auto backGround = Sprite::create("dialog_base.png");
    Size winSize = Director::getInstance()->getWinSize();
    backGround->setPosition(Point(winSize.width / 2, winSize.height / 2));
    backGround->setTag(TAG_SPRITE::BACKGROUND);
    addChild(backGround);
}

void RewardItemUnitDialog::showTitle()
{
    auto label = Label::createWithTTF("宝物を獲得しました！", FONT_NAME_2, 32);
    label->setTag(TAG_SPRITE::TITLE);
    label->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 25));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setColor(Color3B::YELLOW);
    label->setAnchorPoint(Vec2(0.5, 1.0f));
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(label, Z_ORDER::Z_TITLE);
}

void RewardItemUnitDialog::showGetItemUnit(const int masterItemUnitId, const int itemUnitKind)
{
    //未リファクタリング
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon_treasure.plist");
    auto tresurUnit1Icon = Sprite::create("other_tre_icon_small_base.png");
    auto treasureIcon = makeSprite(StringUtils::format("%d_treasure.png", masterItemUnitId).c_str());
    tresurUnit1Icon->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 120));
    treasureIcon->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 120));

    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(tresurUnit1Icon);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(treasureIcon);

    cocos2d::Point starMiddle = Point(treasureIcon->getPosition().x, treasureIcon->getPosition().y - tresurUnit1Icon->getContentSize().height / 2 + 2);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(setStar(4 - itemUnitKind, starMiddle));

    std::shared_ptr<ItemUnitModel>model(ItemUnitModel::find(masterItemUnitId));
    std::string name = model->getName();
    Label* atarashi = Label::createWithTTF(name, FONT_NAME_2, 30);
    atarashi->setColor(Color3B::ORANGE);
    atarashi->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 200));
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(atarashi, 1);
    atarashi->enableShadow();
}

Node* RewardItemUnitDialog::setStar(int numberOfStar, Point nodePossition)
{
    auto starNode = Node::create();
    for (int i = 0; i < numberOfStar; i++) {
        auto star = makeSprite("rarity.png");
        starNode->addChild(star);
        if (numberOfStar % 2 == 1) {
            star->setPosition(Vec2(star->getContentSize().width * (i - numberOfStar / 2), 0));
        } else {
            star->setPosition(Vec2(star->getContentSize().width * (0.5f + (i - numberOfStar / 2)), 0));
        }
    }
    starNode->setPosition(nodePossition);
    return starNode;
}

void RewardItemUnitDialog::showText()
{
    auto label = Label::createWithTTF("詳しくは「宝物図鑑」で確認できます。", FONT_NAME_2, 28);
    label->setDimensions(500, 0);
    label->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 210));
    label->setVerticalAlignment(TextVAlignment::TOP);
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setColor(Color3B::WHITE);
    label->setAnchorPoint(Vec2(0.5, 1.0f));
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(label, 1);
}

void RewardItemUnitDialog::showCloseButton()
{
    auto closeButton = ui::Button::create("gacha_dialog_button.png");
    closeButton->addTouchEventListener(CC_CALLBACK_2(RewardItemUnitDialog::onTouchCloseButton, this));
    closeButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    closeButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2, 25));
    closeButton->setTag(TAG_SPRITE::BUTTON);
    auto closeLabel = Label::createWithTTF("閉じる", FONT_NAME_2, 28);
    closeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    closeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    closeLabel->setPosition(Vec2(closeButton->getContentSize() / 2) - Vec2(0, 14));
    closeButton->addChild(closeLabel);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(closeButton, Z_ORDER::Z_BUTTON);
}

void RewardItemUnitDialog::onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        getEventDispatcher()->removeEventListener(_listener);
        onCloseCallBack();
        removeFromParent();
    }
}
