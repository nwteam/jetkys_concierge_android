#ifndef __syanago__WinScene__
#define __syanago__WinScene__

#include "cocos2d.h"
#include "create_func.h"
#include "RaceResultModel.h"

USING_NS_CC;

class WinScene : public Layer, public create_func<WinScene>
{
public:
    bool init(const std::shared_ptr<RaceResultModel>& model);
    using create_func::create;
    
    WinScene();
    ~WinScene();
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        PLACE_OF_PLAYER,
        VICTORYSTAND,
        GOAL,
        TAP,
    };
    enum Z_ORDER {
        Z_BACKGROUND = 0,
        Z_PLACE_OF_PLAYER,
        Z_VICTORYSTAND,
        Z_GOAL,
        Z_TAP,
    };
    std::shared_ptr<RaceResultModel> _raceResultModel;
    
    void showBackground();
    void showGoalSprite();
    void showPlaceOfPlaye(const int placeOfPlaye);
    void showTapSprite();
    void showVictoryStand(const std::shared_ptr<RaceResultModel>& model);
    Sprite* createSDCharacter(const std::string filePath, const std::string rollFilePath);
    Sprite* createCharacterRollSprite(const std::string rollFilePath);
    
    void callVectoryVoice(const std::shared_ptr<RaceResultModel>& model);
    
    bool onTouchNextScene(cocos2d::Touch* touch,cocos2d::Event* event);
};

#endif
