#include "WinScene.h"
#include "FontDefines.h"
#include "jCommon.h"
#include "SoundHelper.h"
#include "ScoreScene.h"
#include "EventPointScene.h"
#include "PlayerController.h"

WinScene::WinScene():
    _raceResultModel(nullptr)
{}

WinScene::~WinScene()
{
    for (int i = 1; i < 4; i++) {
        if (_raceResultModel->getResultCourse()->getCharacterSDFilePath(i) == "") {
            continue;
        }
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(_raceResultModel->getResultCourse()->getCharacterSDFilePath(i) + ".plist");
    }
    Director::getInstance()->getTextureCache()->removeAllTextures();
    _raceResultModel = nullptr;
}

bool WinScene::init(const std::shared_ptr<RaceResultModel>& model)
{
    if (!Layer::create()) {
        return false;
    }
    showBackground();
    showGoalSprite();
    showPlaceOfPlaye(model->getResultPlayer()->getResultClearThings()->getRankOfPlayer());
    showTapSprite();
    showVictoryStand(model);
    callVectoryVoice(model);
    _raceResultModel = model;

    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(WinScene::onTouchNextScene, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}

void WinScene::showBackground()
{
    auto backgoround = Sprite::create("RankBGR.png");
    backgoround->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    backgoround->setTag(TAG_SPRITE::BACKGROUND);
    addChild(backgoround, Z_ORDER::Z_BACKGROUND);
}

void WinScene::showGoalSprite()
{
    auto goalSprite = Sprite::create("GOAL_UP.png");
    goalSprite->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 50.0 - goalSprite->getContentSize().height / 2));
    goalSprite->setTag(TAG_SPRITE::GOAL);
    addChild(goalSprite, Z_ORDER::Z_GOAL);
}

void WinScene::showPlaceOfPlaye(const int placeOfPlaye)
{
    auto placeOfPlayeSprite = Sprite::create(StringUtils::format("rank%d.png", placeOfPlaye));
    placeOfPlayeSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    placeOfPlayeSprite->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 - 60 - getChildByTag(TAG_SPRITE::GOAL)->getContentSize().height));
    placeOfPlayeSprite->setTag(TAG_SPRITE::PLACE_OF_PLAYER);
    addChild(placeOfPlayeSprite, Z_ORDER::Z_PLACE_OF_PLAYER);
}

void WinScene::showTapSprite()
{
    auto tapSprite = Sprite::create("rank_boder.png");
    tapSprite->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + 50.0 + tapSprite->getContentSize().height / 2));
    tapSprite->setTag(TAG_SPRITE::TAP);
    addChild(tapSprite, Z_ORDER::Z_TAP);
    Label* label = Label::createWithTTF("画面をタップしてください", FONT_NAME_2, 25);
    label->setPosition(Point(tapSprite->getContentSize().width / 2, tapSprite->getContentSize().height / 2 - 12.5));
    label->setColor(Color3B::WHITE);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    tapSprite->addChild(label);
}

void WinScene::showVictoryStand(const std::shared_ptr<RaceResultModel>& model)
{
    auto victoryStand = Sprite::create("rank_down.png");
    victoryStand->setPosition(Vec2(Director::getInstance()->getWinSize() / 2) + Vec2(0, -getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height / 2 + victoryStand->getContentSize().height / 2));
    victoryStand->setTag(TAG_SPRITE::VICTORYSTAND);
    addChild(victoryStand, Z_ORDER::Z_VICTORYSTAND);

    auto firstPlaceSD = createSDCharacter(model->getResultCourse()->getCharacterSDFilePath(1), model->getRollOfFilePath(1));
    firstPlaceSD->setPosition(Vec2(victoryStand->getContentSize().width / 2, 350));
    victoryStand->addChild(firstPlaceSD);

    auto secondPlaceSD = createSDCharacter(model->getResultCourse()->getCharacterSDFilePath(2), model->getRollOfFilePath(2));
    secondPlaceSD->setPosition(Vec2(victoryStand->getContentSize().width / 2 - 180, 283));
    victoryStand->addChild(secondPlaceSD);

    auto thirdPlaceSD = createSDCharacter(model->getResultCourse()->getCharacterSDFilePath(3), model->getRollOfFilePath(3));
    thirdPlaceSD->setPosition(Vec2(victoryStand->getContentSize().width / 2 + 180, 259));
    victoryStand->addChild(thirdPlaceSD);
}

Sprite* WinScene::createSDCharacter(const std::string filePath, const std::string rollFilePath)
{
    std::string plistFilePath = filePath + ".plist";
    std::string sdFilePath = filePath  + ".png";
    Sprite* result;
    if (filePath == "") {
        result = Sprite::create();
    } else {
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(plistFilePath);
        result = makeSprite(sdFilePath.c_str());
    }
    result->setScale(0.7);
    result->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);

    auto rollSprite = createCharacterRollSprite(rollFilePath);
    rollSprite->setPosition(Vec2(result->getContentSize().width / 2, result->getContentSize().height + 10));
    result->addChild(rollSprite);
    return result;
}

Sprite* WinScene::createCharacterRollSprite(const std::string rollFilePath)
{
    if (rollFilePath == "") {
        return Sprite::create();
    }
    auto result = Sprite::create(rollFilePath);
    result->setScale(1.3);
    result->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    return result;
}

void WinScene::callVectoryVoice(const std::shared_ptr<RaceResultModel>& model)
{
    if (model->getResultPlayer()->getResultClearThings()->getRankOfPlayer() == 1) {
        const int leaderMasterCharacterId = PLAYERCONTROLLER->_playerCharacterModels[PLAYERCONTROLLER->_playerTeams->getLearderCharacterId()]->getCharactersId();
        runAction(Sequence::create(DelayTime::create(0.6), CallFunc::create([leaderMasterCharacterId]() {
            SoundHelper::shareHelper()->playCharacterVoiceEfect(leaderMasterCharacterId, 19);
        }), NULL));
    }
}

bool WinScene::onTouchNextScene(cocos2d::Touch* touch, cocos2d::Event* event)
{
    if (_raceResultModel == nullptr) {
        CC_ASSERT("_raceResultModel is null");
        return false;
    }

    if (_raceResultModel->getResultCourse()->getIsRankingEvent() == true) {
        Scene* scene = Scene::create();
        scene->addChild(EventPointScene::create(_raceResultModel));
        transitScene(scene);
        return true;
    } else {
        Scene* scene = Scene::create();
        scene->addChild(ScoreScene::create(_raceResultModel));
        transitScene(scene);
        return true;
    }
}