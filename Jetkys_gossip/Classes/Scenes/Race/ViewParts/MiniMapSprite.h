#ifndef __syanago__MiniMapSprite__
#define __syanago__MiniMapSprite__

#include "cocos2d.h"

USING_NS_CC;

class MiniMapSprite : public Sprite {
public:
    static MiniMapSprite* create(int enemyCharacterCount, float scale);
    MiniMapSprite(int size, float scale);
    void initialize();
    void moveCharacters(int playerCharacterPosition, int enemyCharacterPosition0, int enemyCharacterPosition1, int enemyCharacterPosition2);
    void moveFrame(float distance, bool autoMove);
    
private:
    enum TAG_SPRITE {
        FRAME = 0
    };
    
    int _enemyCharacterCount;
    float _scale;
    Sprite* _playerOnMinimap;
    std::vector<Sprite*> _rivalsOnMinimap;
};



#endif /* defined(__syanago__MiniMapSprite__) */
