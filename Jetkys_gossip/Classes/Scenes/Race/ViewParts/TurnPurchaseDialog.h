#ifndef __syanago__TurnPurchaseDialog__
#define __syanago__TurnPurchaseDialog__

#include <functional>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "create_func.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"

USING_NS_CC;
using namespace SyanagoAPI;

class TurnPurchaseDialog : public Layer, public create_func<TurnPurchaseDialog>
{
public:
    TurnPurchaseDialog();
    ~TurnPurchaseDialog();
    using create_func::create;
    typedef std::function<void(int)>OnSuccessCallback;
    typedef std::function<void()>TransitLoseSceneCallback;
    bool init(const OnSuccessCallback& onSuccessCallback, const TransitLoseSceneCallback& transitLoseSceneCallback);
    
    void show();
    
private:
    enum TAG_SPRITE {
        BACKGROUND,
        BUTTON_CLOSE,
        BUTTON_PURCHASE
    };
    
    bool hasToken();
    void showText(Sprite* bg);
    void showPurchaseButton(Sprite* bg);
    void showCloseButton(Sprite* bg);
    void onTouchPurchaseButton(Ref* sender, ui::Widget::TouchEventType type);
    void onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type);
    void onResponseBuyTurns(Json* json);
    
    void disableButtons();
    
    RequestAPI* api;
    DefaultProgress* progress;
    EventListenerTouchOneByOne* _listener;
    OnSuccessCallback _onSuccessCallback;
    TransitLoseSceneCallback _transitLoseSceneCallback;
    
    bool _buttonPushed;
};

#endif /* defined(__syanago__TurnPurchaseDialog__) */
