#include "GridConverter.h"
#include <sstream>
Json* GridConverter::convertPreferenceCellToForward(Json* grid)
{
    std::string result = R"({"grid":[)";
    for (Json* child = grid->child; child; child = child->next) {
        if (0 == strcmp("18", Json_getString(child, "id", "37"))) {
            result += createRow("3", "1", "500");
        } else {
            result += createRow(Json_getString(child, "id", "37"),
                                Json_getString(child, "kind", "1"),
                                Json_getString(child, "score", "0"));
        }
    }
    result.pop_back();
    result += R"(]})";
    grid = nullptr;
    return Json_getItem(Json_create(result.c_str()), "grid");
}

std::string GridConverter::createRow(std::string id_, std::string kind, std::string score)
{
    //{"id":"37","kind":"1","score":"0"},
    std::stringstream result;
    result << "{";
    result << "\"id\":\""    << id_   << "\",";
    result << "\"kind\":\""  << kind  << "\",";
    result << "\"score\":\"" << score << "\"";
    result << "},";
    return result.str();
}












