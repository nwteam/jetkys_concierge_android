#include "CellViewController.h"
#include "Define.h"
#include "CellType.h"

CellViewController::CellViewController():
    _totalCell(1)
    , _signConectFlag(cellSignType::CELL_SIGN_NON_EVENT)
{}

bool CellViewController::init(Json* json)
{
    if (!Layer::init()) {
        return false;
    }
    setPosition(Director::getInstance()->getWinSize() / 2);

    GOAL_SELL = json->size + START_CELL + 1;

    showBothEnds();
    showCourse(json);
    showCourseSide(json);

    return true;
}

void CellViewController::showBothEnds()
{
    for (int t = 0; t < GOAL_SELL + 50; t++) {
        for (int i = 0; i < 4; i++) {
            if (t <= START_CELL || GOAL_SELL <= t) {
                _cell[i][t] = CellRace::create((t == START_CELL || t == GOAL_SELL) ? CellRace::TYPE::START : CellRace::TYPE::END, CELL_TYPE::BACK_GROUND);
                _cell[i][t]->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
                _cell[i][t]->setPosition(getCellPosition(i, t));
                _cell[i][t]->setScoreCell(0);
                addChild(_cell[i][t], 1);

                if (t == START_CELL || t == GOAL_SELL) {
                    _road[t] = createRoad(NONE, t);
                    addChild(_road[t], 1);
                }
            }
        }
    }
}

void CellViewController::showCourse(Json* json)
{
    int zoder = json->size;

    for (Json* child = json->child; child; child = child->next) {
        zoder--;

        CELL_TYPE id = static_cast<CELL_TYPE>(atoi(Json_getString(child, "id", "")));
        int score = atoi(Json_getString(child, "score", ""));

        for (int i = 0; i < 4; i++) {
            //TODO::refactoring
            _cell[i][_totalCell + START_CELL] = CellRace::create(CellRace::TYPE::COURSE, id);
            _cell[i][_totalCell + START_CELL]->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
            _cell[i][_totalCell + START_CELL]->setScoreCell(score);
            _cell[i][_totalCell + START_CELL]->setPosition(getCellPosition(i, (_totalCell + START_CELL)));
            addChild(_cell[i][_totalCell + START_CELL], zoder);
        }
        if (0 < id  && id < 39) {
            _road[_totalCell + START_CELL] = createRoad(id, _totalCell + START_CELL);

            if (_road[_totalCell + START_CELL] != NULL) {
                bool flag = false;
                for (int j = 1; j < 4; j++) {
                    if (_cell[1][_totalCell + START_CELL]->getCellGroupType() == _cell[1][_totalCell + START_CELL - j]->getCellGroupType()) {
                        flag = true;
                    }
                }

                if (!flag) {
                    addChild(_road[_totalCell + START_CELL], 1);
                    _road[_totalCell + START_CELL]->setAnchorPoint(Vec2(0.5f, 0.0f));
                }
            }
        }
        _totalCell++;
    }
}



void CellViewController::showCourseSide(Json* json)
{
    const int end = GOAL_SELL + 50;
    for (int i = 0; i < end; i++) {
        _cell[4][i] = CellRace::create(CellRace::TYPE::SIDE, CELL_TYPE::BACK_GROUND);
        _cell[4][i]->setAnchorPoint(Point(0.5, 0.5));
        _cell[4][i]->setPosition(getCellPosition(0, i) + Vec2(0, 40));
        addChild(_cell[4][i], json->size + 1);

        _cell[5][i] = CellRace::create(CellRace::TYPE::SIDE, CELL_TYPE::BACK_GROUND);
        _cell[5][i]->setAnchorPoint(Point(0.5, 0.5));
        _cell[5][i]->setPosition(getCellPosition(3, i) - Vec2(0, 40));
        addChild(_cell[5][i], json->size + 1);
    }
}


Sprite* CellViewController::createRoad(CELL_TYPE id_col, int col)
{
    //標識
    Sprite* spr = NULL;
    switch (id_col) {
    case FORWARD_CELL_1:
    case FORWARD_CELL_2:
    case FORWARD_CELL_3:
    case FORWARD_CELL_4:
    case FORWARD_CELL_5:
    case FORWARD_CELL_6:
    case FORWARD_CELL_7:
        spr = makeSprite("nonCell.png");
        if (_signConectFlag != cellSignType::CELL_SIGN_FORWARD) {
            _signConectFlag = cellSignType::CELL_SIGN_FORWARD;
        }
        break;
    case BACK_CELL_1:
    case BACK_CELL_2:
    case BACK_CELL_3:
    case BACK_CELL_4:
        spr = makeSprite("nonCell.png");
        if (_signConectFlag != cellSignType::CELL_SIGN_BACK) {
            _signConectFlag = cellSignType::CELL_SIGN_BACK;
        }
        break;
    case SPEED_DOWN_CELL_1:
    case SPEED_DOWN_CELL_2:
    case SPEED_DOWN_CELL_3:
    case SPEED_DOWN_CELL_4:
        spr = makeSprite("nonCell.png");
        if (_signConectFlag != cellSignType::CELL_SIGN_SPEED_DOWN) {
            _signConectFlag = cellSignType::CELL_SIGN_SPEED_DOWN;
        }
        break;
    case LOSE_CELL_1:
    case LOSE_CELL_2:
        spr = makeSprite("lose_sign.png");
        if (_signConectFlag != cellSignType::CELL_SIGN_LOSE) {
            _signConectFlag = cellSignType::CELL_SIGN_LOSE;
        }
        break;
    case PREFERENCE_CELL_1:
        spr = makeSprite("nonCell.png");
        if (_signConectFlag != cellSignType::CELL_SIGN_PREFERENCE) {
            _signConectFlag = cellSignType::CELL_SIGN_PREFERENCE;
        }
        break;
    case SLOWLY_CELL_1:
        if (_signConectFlag == cellSignType::CELL_SIGN_SLOWLY) {
            spr = makeSprite("nonCell.png");
        } else {
            spr = makeSprite("slowly_sign.png");
            _signConectFlag = cellSignType::CELL_SIGN_SLOWLY;
        }
        break;
    case MINIMUM_SPEED_CELL_1:
        if (_signConectFlag == cellSignType::CELL_SIGN_MINIMUM_SPEED) {
            spr = makeSprite("nonCell.png");
        } else {
            spr = makeSprite("minimum_speed_sign.png");
            _signConectFlag = cellSignType::CELL_SIGN_MINIMUM_SPEED;
        }
        break;
    case DOWNWARD_SLOPE_CELL_1:
    case DOWNWARD_SLOPE_CELL_2:
    case DOWNWARD_SLOPE_CELL_3:
    case DOWNWARD_SLOPE_CELL_4:
    case DOWNWARD_SLOPE_CELL_5:
        if (_signConectFlag == cellSignType::CELL_SIGN_DOWNWARD_SLOPE) {
            spr = makeSprite("nonCell.png");
        } else {
            spr = makeSprite("down_slope_sign.png");
            _signConectFlag = cellSignType::CELL_SIGN_DOWNWARD_SLOPE;
        }
        break;
    case UP_SLOPE_CELL_1:
    case UP_SLOPE_CELL_2:
    case UP_SLOPE_CELL_3:
    case UP_SLOPE_CELL_4:
    case UP_SLOPE_CELL_5:
        if (_signConectFlag == cellSignType::CELL_SIGN_UP_SLOPE) {
            spr = makeSprite("nonCell.png");
        } else {
            spr = makeSprite("up_slope_sign.png");
            _signConectFlag = cellSignType::CELL_SIGN_UP_SLOPE;
        }
        break;
    case GET_ITEM_CELL_1:
        spr = makeSprite("nonCell.png");
        if (_signConectFlag != cellSignType::CELL_SIGN_GET_ITEM) {
            _signConectFlag = cellSignType::CELL_SIGN_GET_ITEM;
        }
        break;
    case GET_SCORE_CELL_1:
    case GET_SCORE_CELL_2:
    case GET_SCORE_CELL_3:
    case GET_SCORE_CELL_4:
        spr = makeSprite("nonCell.png");
        if (_signConectFlag != cellSignType::CELL_SIGN_SCORE_CELL) {
            _signConectFlag = cellSignType::CELL_SIGN_SCORE_CELL;
        }
        break;
    case CHARGE_CELL_1:
        if (_signConectFlag == cellSignType::CELL_SIGN_CHARGE) {
            spr = makeSprite("nonCell.png");
        } else {
            spr = makeSprite("charge_sign.png");
            _signConectFlag = cellSignType::CELL_SIGN_CHARGE;
        }
        break;
    case NON_EVENT_CELL_1:
        if (_signConectFlag != cellSignType::CELL_SIGN_NON_EVENT) {
            _signConectFlag = cellSignType::CELL_SIGN_NON_EVENT;
        }
        break;
    case NONE:
        spr = makeSprite("nonCell.png");
        if (_signConectFlag != cellSignType::CELL_SIGN_NON_EVENT) {
            _signConectFlag = cellSignType::CELL_SIGN_NON_EVENT;
        }
        break;
    default:
        spr = NULL;
        break;
    }
    if (spr != NULL) {
        spr->setPosition(getCellPosition(-0.65, col) + Point(-32, spr->getContentSize().height / 2 - 20));
    }

    return spr;
}

const Vec2 CellViewController::getCellPosition(int grid, int index)
{
    return Vec2(-64 + 64 * grid + 58 * (index - 15),
                74 - 37  * grid + 33.53 * (index - 15));

}

