#ifndef __syanago__GridConverter__
#define __syanago__GridConverter__

#include "editor-support/spine/Json.h"
#include <string>

class GridConverter {
public:
    static Json* convertPreferenceCellToForward(Json* json);
    
private:
    static std::string createRow(std::string id_, std::string kind, std::string score);
};












#endif /* defined(__syanago__GridConverter__) */
