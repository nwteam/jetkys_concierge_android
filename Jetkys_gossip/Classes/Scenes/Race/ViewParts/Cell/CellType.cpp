#include <CellType.h>

bool CellType::isMerit(CELL_TYPE type)
{
    if (isForward(type) ||
        PREFERENCE_CELL_1 == type ||
        isDownwardSlope(type) ||
        isUpwardSlope(type) ||
        GET_ITEM_CELL_1 == type ||
        isScoreCell(type) ||
        CHARGE_CELL_1 == type) {
        return true;
    } else {
        return false;
    }
}

bool CellType::isDemerit(CELL_TYPE type)
{
    if (isSpeedDown(type) ||
        isBack(type) ||
        isStop(type) ||
        isUpwardSlope(type) ||
        SLOWLY_CELL_1 == type ||
        MINIMUM_SPEED_CELL_1 == type
        ) {
        return true;
    }
    return false;
}


bool CellType::isForward(CELL_TYPE type)
{
    switch (type) {
    case FORWARD_CELL_1:
    case FORWARD_CELL_2:
    case FORWARD_CELL_3:
    case FORWARD_CELL_4:
    case FORWARD_CELL_5:
    case FORWARD_CELL_6:
    case FORWARD_CELL_7:
        return true;
    default:
        return false;
    }
}

bool CellType::isBack(CELL_TYPE type)
{
    switch (type) {
    case BACK_CELL_1:
    case BACK_CELL_2:
    case BACK_CELL_3:
    case BACK_CELL_4:
        return true;
    default:
        return false;
    }
}

bool CellType::isSpeedDown(CELL_TYPE type)
{
    switch (type) {
    case SPEED_DOWN_CELL_1:
    case SPEED_DOWN_CELL_2:
    case SPEED_DOWN_CELL_3:
    case SPEED_DOWN_CELL_4:
        return true;
    default:
        return false;
    }
}

bool CellType::isStop(CELL_TYPE type)
{
    switch (type) {
    case LOSE_CELL_1:
    case LOSE_CELL_2:
        return true;
    default:
        return false;
    }
}

bool CellType::isDownwardSlope(CELL_TYPE type)
{
    switch (type) {
    case DOWNWARD_SLOPE_CELL_1:
    case DOWNWARD_SLOPE_CELL_2:
    case DOWNWARD_SLOPE_CELL_3:
    case DOWNWARD_SLOPE_CELL_4:
    case DOWNWARD_SLOPE_CELL_5:
        return true;
    default:
        return false;
    }
}

bool CellType::isUpwardSlope(CELL_TYPE type)
{
    switch (type) {
    case UP_SLOPE_CELL_1:
    case UP_SLOPE_CELL_2:
    case UP_SLOPE_CELL_3:
    case UP_SLOPE_CELL_4:
    case UP_SLOPE_CELL_5:
        return true;
    default:
        return false;
    }
}

bool CellType::isScoreCell(CELL_TYPE type)
{
    switch (type) {
    case GET_SCORE_CELL_1:
    case GET_SCORE_CELL_2:
    case GET_SCORE_CELL_3:
    case GET_SCORE_CELL_4:
        return true;
    default:
        return false;
    }
}







