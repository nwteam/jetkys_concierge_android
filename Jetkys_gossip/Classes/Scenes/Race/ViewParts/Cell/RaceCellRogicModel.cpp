#include "RaceCellRogicModel.h"
#include "CellType.h"
#include <string>
#include "GridOperandsModel.h"

USING_NS_CC;

const int RaceCellRogicModel::cellRogicFoword(const int power, const int kind, const int operand_id)
{
    std::shared_ptr<GridOperandsModel>model(GridOperandsModel::find(operand_id, CELL_EFFECT_KIND::FOWORD, kind));
    if (model == NULL) {
        return 1;
    }
    const int result = (power / model->getOperand2()) * model->getOperand1();
    return std::max(1, result);
}

const int RaceCellRogicModel::cellRogicBack(const int technique, const int kind, const int operand_id)
{
    std::shared_ptr<GridOperandsModel>model(GridOperandsModel::find(operand_id, CELL_EFFECT_KIND::BACK, kind));
    if (model == NULL) {
        return 1;
    }
    const int result = (model->getOperand2() / technique) * model->getOperand1();
    return std::max(1, result);
}

const int RaceCellRogicModel::cellRogicSpeedDown(const int technique, const int kind, const int operand_id)
{
    std::shared_ptr<GridOperandsModel>model(GridOperandsModel::find(operand_id, CELL_EFFECT_KIND::SPEED_DOWN, kind));
    if (model == NULL) {
        return 1;
    }
    const int result = (model->getOperand2() / technique) * model->getOperand1();
    return std::max(1, result);
}

const int RaceCellRogicModel::cellRogicLose(const int brake, const int kind, const int operand_id)
{
    std::shared_ptr<GridOperandsModel>model(GridOperandsModel::find(operand_id, CELL_EFFECT_KIND::LOSE, kind));
    if (model == NULL) {
        return 1;
    }
    const int result = (model->getOperand2() / brake) * model->getOperand1();
    return std::max(1, result);
}

const int RaceCellRogicModel::cellRogicDownSlope(const int diceEyes, const int brake, const int kind, const int operand_id)
{
    if (diceEyes == 0) {
        return 0;
    }
    std::shared_ptr<GridOperandsModel>model(GridOperandsModel::find(operand_id, CELL_EFFECT_KIND::DOWN_SLOPE, kind));
    if (model == NULL) {
        return diceEyes;
    }
    const int result = (brake / model->getOperand2()) * model->getOperand1();
    return std::max(1, (std::max(1, result) + diceEyes));
}

const int RaceCellRogicModel::cellRogicUpSlope(const int diceEyes, const int power, const int kind, const int operand_id)
{
    if (diceEyes == 0) {
        return 0;
    }
    std::shared_ptr<GridOperandsModel>model(GridOperandsModel::find(operand_id, CELL_EFFECT_KIND::UP_SLOPE, kind));
    if (model == NULL) {
        return diceEyes;
    }
    const int result =  ((model->getOperand2() / power) * model->getOperand1());
    return std::max(1, (diceEyes - std::max(1, result)));
}