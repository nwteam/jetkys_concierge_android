#ifndef __syanago__RaceCellRogicModel__
#define __syanago__RaceCellRogicModel__

class RaceCellRogicModel
{
public:
    RaceCellRogicModel(){};
    ~RaceCellRogicModel(){};
    
    static const int cellRogicFoword(const int power, const  int kind, const int operand_id);
    static const int cellRogicBack(const int technique, const int kind, const int operand_id);
    static const int cellRogicSpeedDown(const int technique, const int kind, const int operand_id);
    static const int cellRogicLose(const int brake, const int kind, const int operand_id);
    static const int cellRogicDownSlope(const int diceEyes, const int brake, const int kind, const int operand_id);
    static const int cellRogicUpSlope(const int diceEyes, const int power, const int kind, const int operand_id);
private:
    enum CELL_EFFECT_KIND{
        FOWORD = 10,
        BACK = 20,
        SPEED_DOWN = 30,
        LOSE = 40,
        DOWN_SLOPE = 80,
        UP_SLOPE = 90,
    };
};

#endif /* defined(__syanago__RaceCellRogicModel__) */
