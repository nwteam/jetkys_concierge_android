#ifndef __syanago__CellRace__
#define __syanago__CellRace__

#include "cocos2d.h"
#include "jCommon.h"
#include "create_func.h"
#include "CellType.h"

USING_NS_CC;

class CellRace : public Sprite
{
public:
    enum TYPE {
        SIDE = 0,
        START,
        END,
        COURSE
    };
    CellRace(CELL_TYPE cellType);
    
    static CellRace* create(CellRace::TYPE fileType, CELL_TYPE cellType);
    void setCellGroup();
    
    CELL_TYPE getCellType();
    void setScoreCell (int type);
    int getScoreCell();
    int getCellGroupType();
    
    void show();
    
private:
    static std::string getCellFileName(CELL_TYPE kind);
    
    CELL_TYPE _cellType;
    int _score;
    int _cellGroupType;
    Sprite* _cellSprite;
};

#endif /* defined(__syanago__CellRace__) */
