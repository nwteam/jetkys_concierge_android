#ifndef syanago_CellType_h
#define syanago_CellType_h

enum CELL_TYPE{
    NONE = 0,
    FORWARD_CELL_1 = 1,
    FORWARD_CELL_2 = 2,
    FORWARD_CELL_3 = 3,
    FORWARD_CELL_4 = 4,
    FORWARD_CELL_5 = 5,
    FORWARD_CELL_6 = 6,
    FORWARD_CELL_7 = 7,
    BACK_CELL_1 = 8,
    BACK_CELL_2 = 9,
    BACK_CELL_3 = 10,
    BACK_CELL_4 = 11,
    SPEED_DOWN_CELL_1 = 12,
    SPEED_DOWN_CELL_2 = 13,
    SPEED_DOWN_CELL_3 = 14,
    SPEED_DOWN_CELL_4 = 15,
    LOSE_CELL_1 = 16,
    LOSE_CELL_2 = 17,
    PREFERENCE_CELL_1 = 18,
    SLOWLY_CELL_1 = 19,
    MINIMUM_SPEED_CELL_1 = 20,
    DOWNWARD_SLOPE_CELL_1 = 21,
    DOWNWARD_SLOPE_CELL_2 = 22,
    DOWNWARD_SLOPE_CELL_3 = 23,
    DOWNWARD_SLOPE_CELL_4 = 24,
    DOWNWARD_SLOPE_CELL_5 = 25,
    UP_SLOPE_CELL_1 = 26,
    UP_SLOPE_CELL_2 = 27,
    UP_SLOPE_CELL_3 = 28,
    UP_SLOPE_CELL_4 = 29,
    UP_SLOPE_CELL_5 = 30,
    GET_ITEM_CELL_1 = 31,
    GET_SCORE_CELL_1 = 32,
    GET_SCORE_CELL_2 = 33,
    GET_SCORE_CELL_3 = 34,
    GET_SCORE_CELL_4 = 35,
    CHARGE_CELL_1 = 36,
    NON_EVENT_CELL_1 = 37,
    BACK_GROUND = 100
};


class CellType {
public:
    static bool isMerit(CELL_TYPE type);
    static bool isDemerit(CELL_TYPE type);
    static bool isForward(CELL_TYPE type);
    static bool isBack(CELL_TYPE type);
    static bool isSpeedDown(CELL_TYPE type);
    static bool isStop(CELL_TYPE type);
    static bool isDownwardSlope(CELL_TYPE type);
    static bool isUpwardSlope(CELL_TYPE type);
    static bool isScoreCell(CELL_TYPE type);
};


















#endif
