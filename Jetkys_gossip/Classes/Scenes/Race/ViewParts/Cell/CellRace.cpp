#include "CellRace.h"

CellRace::CellRace(CELL_TYPE cellType):
    _cellType(cellType)
    , _cellGroupType(0) {}


CellRace* CellRace::create(CellRace::TYPE fileType, CELL_TYPE cellType)
{
    std::string filePath;
    if (cellType == BACK_GROUND) {
        switch (fileType) {
        case CellRace::SIDE:
            filePath = "cell_side.png";
            break;
        case CellRace::START:
            filePath = "start_cell.png";
            break;
        case CellRace::END:
            filePath = "cell_template.png";
            break;
        case CellRace::COURSE:
            filePath = "";
            break;
        }
    } else {
        filePath = getCellFileName(cellType);
    }

    CellRace* sprite = new CellRace(cellType);
    if (sprite && sprite->initWithSpriteFrameName(filePath)) {
        sprite->setCellGroup();
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

void CellRace::setCellGroup()
{
    switch (_cellType) {
    case FORWARD_CELL_1:
    case FORWARD_CELL_2:
    case FORWARD_CELL_3:
        _cellGroupType = 1;
        break;
    case FORWARD_CELL_4:
    case FORWARD_CELL_5:
    case FORWARD_CELL_6:
        _cellGroupType = 2;
        break;
    case FORWARD_CELL_7:
        _cellGroupType = 3;
        break;
    case BACK_CELL_1:
    case BACK_CELL_2:
        _cellGroupType = 4;
        break;
    case BACK_CELL_3:
    case BACK_CELL_4:
        _cellGroupType = 5;
        break;
    case SPEED_DOWN_CELL_1:
    case SPEED_DOWN_CELL_2:
        _cellGroupType = 6;
        break;
    case SPEED_DOWN_CELL_3:
    case SPEED_DOWN_CELL_4:
        _cellGroupType = 7;
        break;
    case LOSE_CELL_1:
    case LOSE_CELL_2:
        _cellGroupType = 8;
        break;
    case PREFERENCE_CELL_1:
        _cellGroupType = 9;
        break;
    case SLOWLY_CELL_1:
        _cellGroupType = 10;
        break;
    case MINIMUM_SPEED_CELL_1:
        _cellGroupType = 10;
        break;
    case DOWNWARD_SLOPE_CELL_1:
    case DOWNWARD_SLOPE_CELL_2:
        _cellGroupType = 11;
        break;
    case DOWNWARD_SLOPE_CELL_3:
    case DOWNWARD_SLOPE_CELL_4:
        _cellGroupType = 12;
        break;
    case DOWNWARD_SLOPE_CELL_5:
        _cellGroupType = 13;
        break;
    case UP_SLOPE_CELL_1:
    case UP_SLOPE_CELL_2:
        _cellGroupType = 14;
        break;
    case UP_SLOPE_CELL_3:
    case UP_SLOPE_CELL_4:
        _cellGroupType = 15;
        break;
    case UP_SLOPE_CELL_5:
        _cellGroupType = 16;
        break;
    case GET_ITEM_CELL_1:
        _cellGroupType = 17;
        break;
    case GET_SCORE_CELL_1:
    case GET_SCORE_CELL_2:
    case GET_SCORE_CELL_3:
    case GET_SCORE_CELL_4:
        _cellGroupType = 18;
        break;
    case CHARGE_CELL_1:
        _cellGroupType = 19;
        break;
    case NON_EVENT_CELL_1:
        _cellGroupType = 20;
        break;
    default:
        break;
    }
}


std::string CellRace::getCellFileName(CELL_TYPE kind)
{
    std::string result;
    switch (kind) {
    case FORWARD_CELL_1:
    case FORWARD_CELL_2:
    case FORWARD_CELL_3:
        result = "forward_cell_1.png";
        break;
    case FORWARD_CELL_4:
    case FORWARD_CELL_5:
    case FORWARD_CELL_6:
        result = "forward_cell_2.png";
        break;
    case FORWARD_CELL_7:
        result = "forward_cell_3.png";
        break;
    case BACK_CELL_1:
    case BACK_CELL_2:
        result = "back_cell_1.png";
        break;
    case BACK_CELL_3:
    case BACK_CELL_4:
        result = "back_cell_2.png";
        break;
    case SPEED_DOWN_CELL_1:
    case SPEED_DOWN_CELL_2:
        result = "speed_down_cell_1.png";
        break;
    case SPEED_DOWN_CELL_3:
    case SPEED_DOWN_CELL_4:
        result = "speed_down_cell_1.png";
        break;
    case LOSE_CELL_1:
    case LOSE_CELL_2:
        result = "lose_cell.png";
        break;
    case PREFERENCE_CELL_1:
        result = "preference_cell.png";
        break;
    case SLOWLY_CELL_1:
        result = "slowly_cell.png";
        break;
    case MINIMUM_SPEED_CELL_1:
        result = "minimum_speed_cell.png";
        break;
    case DOWNWARD_SLOPE_CELL_1:
    case DOWNWARD_SLOPE_CELL_2:
        result = "down_slope_cell_1.png";
        break;
    case DOWNWARD_SLOPE_CELL_3:
    case DOWNWARD_SLOPE_CELL_4:
        result = "down_slope_cell_2.png";
        break;
    case DOWNWARD_SLOPE_CELL_5:
        result = "down_slope_cell_3.png";
        break;
    case UP_SLOPE_CELL_1:
    case UP_SLOPE_CELL_2:
        result = "up_slope_cell_1.png";
        break;
    case UP_SLOPE_CELL_3:
    case UP_SLOPE_CELL_4:
        result = "up_slope_cell_2.png";
        break;
    case UP_SLOPE_CELL_5:
        result = "up_slope_cell_3.png";
        break;
    case GET_ITEM_CELL_1:
        result = "get_item_cell.png";
        break;
    case GET_SCORE_CELL_1:
    case GET_SCORE_CELL_2:
    case GET_SCORE_CELL_3:
    case GET_SCORE_CELL_4:
        result = "get_score_cell.png";
        break;
    case CHARGE_CELL_1:
        result = "charge_cell.png";
        break;
    case NON_EVENT_CELL_1:
        result = "non_cell.png";
        break;
    default:
        result = "non_cell.png";
        break;
    }
    return result;
}


CELL_TYPE CellRace::getCellType()
{
    return _cellType;
}

void CellRace::setScoreCell(int score)
{
    _score = score;
}

int CellRace::getScoreCell()
{
    return _score;
}

int CellRace::getCellGroupType()
{
    return _cellGroupType;
}


