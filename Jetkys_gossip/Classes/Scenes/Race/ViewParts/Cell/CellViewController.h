#ifndef __syanago__CellViewController__
#define __syanago__CellViewController__

#include "cocos2d.h"
#include "CellRace.h"
#include "editor-support/spine/Json.h"
#include "create_func.h"

USING_NS_CC;

enum class cellSignType: int{
    CELL_SIGN_FORWARD,
    CELL_SIGN_BACK,
    CELL_SIGN_SPEED_DOWN,
    CELL_SIGN_LOSE,
    CELL_SIGN_PREFERENCE,
    CELL_SIGN_SLOWLY,
    CELL_SIGN_MINIMUM_SPEED,
    CELL_SIGN_DOWNWARD_SLOPE,
    CELL_SIGN_UP_SLOPE,
    CELL_SIGN_GET_ITEM,
    CELL_SIGN_SCORE_CELL,
    CELL_SIGN_CHARGE,
    CELL_SIGN_NON_EVENT,
};

class CellViewController : public cocos2d::Layer, create_func<CellViewController>
{
public:
    CellViewController();
    using create_func::create;
    bool init(Json* json);
    
    CellRace* _cell[6][300];
    int GOAL_SELL;
    
private:
    void showBothEnds();
    void showCourse(Json* json);
    void showCourseSide(Json* json);
    Sprite* createRoad(CELL_TYPE id_col, int col);
    
    const Vec2 getCellPosition(int grid, int index);
    
    
    Sprite* _road[300];
    int _totalCell;
    cellSignType _signConectFlag;
};

#endif /* defined(__syanago__CellViewController__) */
