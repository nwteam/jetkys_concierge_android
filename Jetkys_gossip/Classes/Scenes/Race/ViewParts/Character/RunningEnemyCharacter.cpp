#include "RunningEnemyCharacter.h"

RunningEnemyCharacter* RunningEnemyCharacter::create(Vec2 position, std::string animationKey, MoveBy* moveBy)
{
    RunningEnemyCharacter* sprite = new RunningEnemyCharacter(animationKey, moveBy);
    if (sprite && sprite->init()) {
        sprite->setAnchorPoint(Point(0.7, 0.35));
        sprite->setPosition(position);
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

void RunningEnemyCharacter::play()
{
    runAction(_moveBy);
    runAction(Animate::create(AnimationCache::getInstance()->getAnimation(_animationKey)));
}
