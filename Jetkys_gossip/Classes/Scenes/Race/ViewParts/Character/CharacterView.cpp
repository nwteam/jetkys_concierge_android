#include "CharacterView.h"
#include "Define.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include "jCommon.h"

using namespace std;


CharacterView* CharacterView::create(TeamStatus* teamStatus, std::string CharacterFilePath, std::string targetFilePath, std::string label, int row, int column)
{
    CharacterView* sprite = new CharacterView(teamStatus, column);
    SpriteFrame* frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(targetFilePath.c_str());
    if (sprite && sprite->initWithSpriteFrame(frame)) {
        if (UserDefault::getInstance()->getBoolForKey("Tutorial") && row != 3) {
            sprite->setVisible(false);
        }
        sprite->init(CharacterFilePath, label);
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

CharacterView::CharacterView(TeamStatus* teamStatus, int column):
    _power(teamStatus->getPower()), _technique(teamStatus->getTechnique()), _brake(teamStatus->getBrake()), _activeUE(false), _demerit(false), _gensokuActive(false), _vissibleNameLabelFlag(false), _characterLabelSpriteSetFlag(false), _characterColumn(column)
{
    //delete teamStatus;
}

bool CharacterView::init(std::string filePath, std::string label)
{
    setAnchorPoint(Point(0.5, 0.5));

    _characterNameString = label;

    showCharacter(filePath.c_str());
    showCharacterLabel(label);

    prepareAnimations();

    return true;
}

void CharacterView::showCharacter(const char* fileName)
{
    _characterSprite = makeSprite(fileName);
    _characterSprite->setAnchorPoint(Point(0.5, 0.0));
    _characterSprite->setPosition(Point(getContentSize().width / 2, 10));

    addChild(_characterSprite);
}

void CharacterView::showCharacterLabel(std::string label)
{
    if (label == "") {
        return;
    }
    _characterLabelSpriteSetFlag = true;
    if (label == "1P") {
        _characterLabelSprite = makeSprite("race_player_point.png");
    } else if (label == "BOSS") {
        _characterLabelSprite = makeSprite("race_boss_point.png");
    }
    _characterLabelSprite->setPosition(Point(_characterSprite->getContentSize().width / 2 + 8, _characterSprite->getContentSize().height + 40.0));
    _characterPoint = makeSprite("race_point.png");
    _characterPoint->setPosition(_characterLabelSprite->getPosition() - Point(0, 40));
    _characterSprite->addChild(_characterLabelSprite, 4);
    _characterSprite->addChild(_characterPoint, 4);
}

void CharacterView::prepareAnimations()
{
    auto animationGensoku = Animation::create();
    for (int i = 1; i <= 2; i++) {
        animationGensoku->addSpriteFrameWithFile(StringUtils::format("ase%d.png", i));
    }
    animationGensoku->setDelayPerUnit(0.4f);
    animationGensoku->setLoops(50);
    animationGensoku->setRestoreOriginalFrame(true);
    AnimationCache::getInstance()->addAnimation(animationGensoku, ANIMATION_GENSOKU);
}


void CharacterView::showEffectOnTop(std::string imageName)
{
    if (!_activeUE && imageName != "") {
        _activeUE = true;
        _atamaUe = makeSprite(imageName.c_str());
        _atamaUe->setPosition(Point(_characterSprite->getContentSize().width / 2 + 8, _characterSprite->getContentSize().height + 10.0));
        _characterSprite->addChild(_atamaUe, 4);
        if (imageName == "Item1.png") {
            _atamaUe->setPosition(Point(_characterSprite->getContentSize().width / 2, _characterSprite->getContentSize().height + 20.0));
        }

        if (imageName == "bikkuri.png") {
            _atamaUe->setScale(0.4);
            runAction(Sequence::create(DelayTime::create(0.6), CallFunc::create(CC_CALLBACK_0(CharacterView::removeEffectOnTop, this)), NULL));
        }
        if (imageName == "hikarukyuu.png") {
            _atamaUe->setScale(0.3);
            runAction(Sequence::create(DelayTime::create(0.6), CallFunc::create(CC_CALLBACK_0(CharacterView::removeEffectOnTop, this)), NULL));
        }
    }
}

void CharacterView::setEffectOnHeadByCellType(CELL_TYPE type)
{
    if (CellType::isStop(type)) {
        showEffectOnTop("Item4.png");
    } else if (type == SLOWLY_CELL_1) {
        showEffectOnTop("Item7.png");
    } else if (type == MINIMUM_SPEED_CELL_1) {
        showEffectOnTop("Item6.png");
    } else {
        showEffectOnTop("");
    }
}


void CharacterView::gensokuEffect(bool flag)
{
    if (flag && !_gensokuActive) {
        _gensokuActive = true;
        Sprite* sweatSprite = makeSprite("ase1.png");
        sweatSprite->setTag(TAG_SPRITE::SWEAT);
        sweatSprite->setPosition(Point(_characterSprite->getContentSize().width / 2 + 8 - 50.0, _characterSprite->getContentSize().height - 40.0));
        sweatSprite->setScale(0.25);
        sweatSprite->runAction(RepeatForever::create(Animate::create(AnimationCache::getInstance()->getAnimation(ANIMATION_GENSOKU))));
        addChild(sweatSprite, 10);
    } else if (_gensokuActive) {
        _gensokuActive = false;
        Sprite* sweatSprite = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::SWEAT));
        if (sweatSprite) {
            sweatSprite->stopAllActions();
            sweatSprite->removeFromParent();
        }
    }
}

void CharacterView::demeritEffect(bool flag)
{
    if (flag && !_demerit) {
        _demerit = true;
        _demeritEffect = makeSprite("odoroki.png");
        _demeritEffect->setPosition(Point(_characterSprite->getContentSize().width / 2 + 8 - _characterSprite->getContentSize().width * 3 / 16 - 12.0, _characterSprite->getContentSize().height * 7 / 8 - 20.0));
        _demeritEffect->setScale(0.3);
        auto run = Sequence::create(CallFunc::create(CC_CALLBACK_0(CharacterView::showDemerit, this)),
                                    DelayTime::create(0.2),
                                    CallFunc::create(CC_CALLBACK_0(CharacterView::disableDemerit, this)),
                                    DelayTime::create(0.2),
                                    CallFunc::create(CC_CALLBACK_0(CharacterView::showDemerit, this)),
                                    DelayTime::create(0.4),
                                    CallFunc::create(CC_CALLBACK_0(CharacterView::removeDemerit, this)), NULL);
        addChild(_demeritEffect, 10);
        _demeritEffect->runAction(run);
    } else if (_demerit && !flag)   {
        _demerit = false;
        _demeritEffect->stopAllActions();
        _demeritEffect->removeFromParent();
    }
}


void CharacterView::setEnableCharacter(bool isActive)
{
    _characterSprite->setVisible(isActive);
}

void CharacterView::setCharacterColumn(int column)
{
    _characterColumn = column;
}

int CharacterView::getCharacterColumn()
{
    return _characterColumn;
}

void CharacterView::removeEffectOnTop()
{
    if (_activeUE) {
        _activeUE = false;
        _atamaUe->removeFromParent();
    }
}

void CharacterView::showDemerit()
{
    _demeritEffect->setVisible(true);
}

void CharacterView::disableDemerit()
{
    _demeritEffect->setVisible(false);
}

void CharacterView::removeDemerit()
{
    if (_demerit) {
        _demerit = false;
        _demeritEffect->stopAllActions();
        _demeritEffect->removeFromParent();
    }

}

void CharacterView::setNameLabelVisible(bool flag)
{
    if (_characterLabelSpriteSetFlag == false) {
        return;
    }
    if (_vissibleNameLabelFlag == flag) {
        return;
    }
    _vissibleNameLabelFlag = flag;
    _characterLabelSprite->setVisible(flag);
    _characterPoint->setVisible(flag);
}

std::string CharacterView::getNameLabelString()
{
    return _characterNameString;
}

cocos2d::Point CharacterView::getNameLabelPosition()
{
    return _characterLabelSprite->getPosition();
}

void CharacterView::jump()
{
    runAction(Sequence::create(
                  JumpBy::create(1.0, Vec2(0.0, 0.0), RACE_JUMP, 1),
                  JumpBy::create(0.1, Vec2(0.0, 0.0), RACE_JUMP / 8, 1),
                  JumpBy::create(0.1, Vec2(0.0, 0.0), RACE_JUMP / 12, 1),
                  NULL));
}

void CharacterView::jump(Vec2 position)
{
    runAction(Sequence::create(
                  JumpBy::create(1.0, position, RACE_JUMP, 1),
                  JumpBy::create(0.1, Vec2(0.0, 0.0), RACE_JUMP / 8, 1),
                  JumpBy::create(0.1, Vec2(0.0, 0.0), RACE_JUMP / 12, 1),
                  NULL));
}



















