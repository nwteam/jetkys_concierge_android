#include "RunningPlayerCharacter.h"

USING_NS_CC;

const std::string RunningPlayerCharacter::KEY = "Animation_RunCharacter";

RunningPlayerCharacter* RunningPlayerCharacter::create(Vec2 position)
{
    RunningPlayerCharacter* sprite = new RunningPlayerCharacter();
    if (sprite && sprite->init()) {
        sprite->setAnchorPoint(Point(0.7, 0.35));
        sprite->setPosition(position);
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

void RunningPlayerCharacter::play()
{
    runAction(Animate::create(AnimationCache::getInstance()->getAnimation(KEY)));
}





