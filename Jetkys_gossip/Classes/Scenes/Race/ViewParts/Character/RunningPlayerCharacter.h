#ifndef RUNNINGPLAYERCHARACTER_H_
#define RUNNINGPLAYERCHARACTER_H_

#include "cocos2d.h"

USING_NS_CC;

class RunningPlayerCharacter : public Sprite
{
public:
	static RunningPlayerCharacter* create(Vec2 position);

	void play();

	static const std::string KEY;

private:
	RunningPlayerCharacter(){};

};

#endif /* RUNNINGPLAYERCHARACTER_H_ */
