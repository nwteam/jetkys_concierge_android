#ifndef RUNNINGENEMYCHARACTER_H_
#define RUNNINGENEMYCHARACTER_H_

#include "cocos2d.h"

USING_NS_CC;

class RunningEnemyCharacter : public Sprite
{
public:
	static RunningEnemyCharacter* create(Vec2 position, std::string animationKey, MoveBy* moveBy);

	void play();

private:
	RunningEnemyCharacter(std::string animationKey, MoveBy* moveBy):
		_animationKey(animationKey),
		_moveBy(moveBy){};

	std::string _animationKey;
	MoveBy* _moveBy;

};

#endif /* RUNNINGENEMYCHARACTER_H_ */
