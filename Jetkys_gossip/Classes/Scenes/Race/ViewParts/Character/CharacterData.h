#ifndef __CharacterData_H__
#define __CharacterData_H__

#include "cocos2d.h"

class CharacterData
{
private:
    std::string CharacterName;//車なごの名前
    
    std::string CharacterFilePass;
    std::string targetFilePass;
    /* ステータス */
    int _movingParameter;//運動
    int _reactionParameter;//反応
    int _judgeParameter;//判断
    /* 表示するマスの位置 */
    int _characterPositionNum;//現在のキャラの居場所
    cocos2d::Vec2 _characterPosition;
    int _targetPositionNum;//ターゲット表示の位置
    cocos2d::Vec2 targetPosition;
    /* その他のステータス */
    int _characterScore;//スコア
    int _holdingItemId;//取得しているアイテムId
    
    int _moveDef;//移動量
    
    
public:
    
    bool initializeCharacterData();
    CharacterData();
    virtual ~CharacterData();
    
    std::string getCharacterName();
    bool setCharacterName(std::string name);
    
    std::string getCharacterFilePass();
    bool setCharacterFilePass(std::string filePass);
    std::string getTargetFilePass();
    bool setTargetFilePass(std::string filePass);
    
    int getMovingParameter();
    bool setMovingParameter(int parameter);
    int getReactionParameter();
    bool setReactionParameter(int parameter);
    int getJudgeParameter();
    bool setJudgeParameter(int parameter);
    
    int getCharacterPositionNum();
    bool setCharacterPositionNum(int positionNum);
    cocos2d::Vec2 getCharacterPosition();
    bool setCharacterPosition(cocos2d::Vec2 position);
    int getTargetPositionNum();
    bool setTargetPositionNum(int positionNum);
    cocos2d::Vec2 getTargetPosition();
    bool setTargetPosition(cocos2d::Vec2 position);
    
    int getScore();
    bool setScore(int score);
    
    int getHoldingItemId();
    bool setHoldingItemId(int itemId);
    
    int getMoveDef();
    bool setMoveDef(int def);
    
};


#endif /* defined(__CharacterData_H__) */
