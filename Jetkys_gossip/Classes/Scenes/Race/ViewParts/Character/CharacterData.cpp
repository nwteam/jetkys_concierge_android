#include "CharacterData.h"
#include "cocos2d.h"

using namespace cocos2d;
using namespace std;

CharacterData::CharacterData() {}
CharacterData::~CharacterData() {}


bool CharacterData::initializeCharacterData()
{
    _movingParameter = 0;
    _reactionParameter = 0;
    _judgeParameter = 0;
    _characterPositionNum = 0;
    _targetPositionNum = 0;
    _characterScore = 0;
    _holdingItemId = 0;
    _moveDef = 0;
    return true;
}

std::string CharacterData::getCharacterName()
{
    return CharacterName;
}

bool CharacterData::setCharacterName(std::string name)
{
    CharacterName = name;
    return true;
}

std::string CharacterData::getCharacterFilePass()
{
    return CharacterFilePass;
}

bool CharacterData::setCharacterFilePass(std::string filePass)
{
    CharacterFilePass = filePass;
    return true;
}

std::string CharacterData::getTargetFilePass()
{
    return targetFilePass;
}

bool CharacterData::setTargetFilePass(std::string filePass)
{
    targetFilePass = filePass;
    return true;
}

int CharacterData::getMovingParameter()
{
    return _movingParameter;
}

bool CharacterData::setMovingParameter(int parameter)
{
    _movingParameter = parameter;
    return true;
}

int CharacterData::getReactionParameter()
{
    return _reactionParameter;
}

bool CharacterData::setReactionParameter(int parameter)
{
    _reactionParameter = parameter;
    return true;
}

int CharacterData::getJudgeParameter()
{
    return _judgeParameter;
}

bool CharacterData::setJudgeParameter(int parameter)
{
    _judgeParameter = parameter;
    return true;
}

int CharacterData::getCharacterPositionNum()
{
    return _characterPositionNum;
}

bool CharacterData::setCharacterPositionNum(int positionNum)
{
    _characterPositionNum = positionNum;
    return true;
}

cocos2d::Vec2 CharacterData::getCharacterPosition()
{
    return _characterPosition;
}

bool CharacterData::setCharacterPosition(cocos2d::Vec2 position)
{
    _characterPosition = position;
    return true;
}

int CharacterData::getTargetPositionNum()
{
    return _targetPositionNum;
}

bool CharacterData::setTargetPositionNum(int positionNum)
{
    _targetPositionNum = positionNum;
    return true;
}

cocos2d::Vec2 CharacterData::getTargetPosition()
{
    return targetPosition;
}

bool CharacterData::setTargetPosition(cocos2d::Vec2 position)
{
    targetPosition = position;
    return true;
}

int CharacterData::getScore()
{
    return _characterScore;
}

bool CharacterData::setScore(int score)
{
    _characterScore = score;
    return true;
}

int CharacterData::getHoldingItemId()
{
    return _holdingItemId;
}

bool CharacterData::setHoldingItemId(int itemId)
{
    _holdingItemId = itemId;
    return true;
}

int CharacterData::getMoveDef()
{
    return _moveDef;
}

bool CharacterData::setMoveDef(int def)
{
    _moveDef = def;
    return true;
}