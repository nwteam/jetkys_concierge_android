#include "CharacterEfectView.h"
#include "CellType.h"
#include "MacroDefines.h"
#include "FontDefines.h"

CharacterEfectView::CharacterEfectView() {}
CharacterEfectView::~CharacterEfectView() {}

bool CharacterEfectView::init()
{
    if (!Layer::init()) {
        return false;
    }

    // 初期化
    _valuLabel = NULL;
    _upFlag = false;
    _value = 0;
    _UpDown = "\0";
    _statusType = "\0";

    return true;
}

CharacterEfectView* CharacterEfectView::createCellActionEfect(int type, int bugingEyes, int result, int extension, int reaction, int decisition)
{
    auto efect = new CharacterEfectView;
    efect->cellActionEfect(type, bugingEyes, result, extension, reaction, decisition);
    efect->autorelease();

    return efect;
}

void CharacterEfectView::cellActionEfect(int type, int bugingEyes, int result, int extension, int reaction, int decisition)
{
    if (NON_EVENT_CELL_1 == type) {
        return;
    }

    _upFlag = false;
    auto endComent = "マス!";
    int efectType = 0;
    Color3B fontColor = Color3B::BLUE;
    switch (type) {
    case FORWARD_CELL_1:
    case FORWARD_CELL_2:
    case FORWARD_CELL_3:
    case FORWARD_CELL_4:
    case FORWARD_CELL_5:
    case FORWARD_CELL_6:
    case FORWARD_CELL_7:
        _value = extension;
        _statusType = "馬力 ";
        _UpDown = "+";
        _upFlag = true;
        fontColor = Color3B::RED;
        break;
    case BACK_CELL_1:
    case BACK_CELL_2:
    case BACK_CELL_3:
    case BACK_CELL_4:
        _value = reaction;
        _statusType = "技量 ";
        _UpDown = "-";
        _upFlag = false;
        break;
    case SPEED_DOWN_CELL_1:
    case SPEED_DOWN_CELL_2:
    case SPEED_DOWN_CELL_3:
    case SPEED_DOWN_CELL_4:
        _value = reaction;
        _statusType = "技量 ";
        _UpDown = "出目から-";
        endComent = "";
        _upFlag = false;
        break;
    case LOSE_CELL_1:
    case LOSE_CELL_2:
        _value = decisition;
        _statusType = "忍耐 ";
        _UpDown = "";
        endComent = "休み";
        _upFlag = false;
        break;
    case PREFERENCE_CELL_1:
        break;
    case SLOWLY_CELL_1:
        break;
    case MINIMUM_SPEED_CELL_1:
        break;
    case DOWNWARD_SLOPE_CELL_1:
    case DOWNWARD_SLOPE_CELL_2:
    case DOWNWARD_SLOPE_CELL_3:
    case DOWNWARD_SLOPE_CELL_4:
    case DOWNWARD_SLOPE_CELL_5:
        _value = decisition;
        _statusType = "忍耐 ";
        _UpDown = "";
        _upFlag = true;
        fontColor = Color3B::RED;
        efectType = 1;
        break;
    case UP_SLOPE_CELL_1:
    case UP_SLOPE_CELL_2:
    case UP_SLOPE_CELL_3:
    case UP_SLOPE_CELL_4:
    case UP_SLOPE_CELL_5:
        _value = extension;
        _statusType = "馬力 ";
        _UpDown = "";
        _upFlag = false;
        efectType = 1;
        break;
    case GET_ITEM_CELL_1:
        break;
    case GET_SCORE_CELL_1:
    case GET_SCORE_CELL_2:
    case GET_SCORE_CELL_3:
    case GET_SCORE_CELL_4:
        break;
    case CHARGE_CELL_1:
        break;
    default:
        break;
    }

    // 演出
    if (efectType == 0) {
        _valuLabel = Label::createWithTTF(_statusType + FormatWithCommas(_value), FONT_NAME_2, 28);
        _valuLabel->enableOutline(Color4B(Color3B::BLACK), 2);
        _valuLabel->setOpacity(0);

        auto resultLabel = Label::createWithTTF(_UpDown + FormatWithCommas(result) + endComent, FONT_NAME_2, 32);
        resultLabel->enableOutline(Color4B(fontColor), 2);
        resultLabel->setOpacity(0);

        float heightPosi1 = 150;
        float heightPosi2 = 128;
        _valuLabel->setPosition(Vec2(0, heightPosi1));
        resultLabel->setPosition(Vec2(0, heightPosi2));
        addChild(_valuLabel, 100);
        addChild(resultLabel, 101);

        auto fadeIn = FadeIn::create(0.1);
        auto fadeOut = FadeOut::create(0.3);
        auto DownMove1 = MoveBy::create(0.1, Vec2(0, -30));
        auto DownMove2 = MoveBy::create(0.1, Vec2(0, -20));
        auto DownMove3 = MoveBy::create(0.1, Vec2(0, -10));

        auto UpMove1 = MoveBy::create(0.1, Vec2(0, 10));
        auto UpMove2 = MoveBy::create(0.1, Vec2(0, 20));
        auto UpMove3 = MoveBy::create(0.1, Vec2(0, 30));
        auto UpMove4 = MoveBy::create(0.1, Vec2(0, 30));
        auto UpMove5 = MoveBy::create(0.1, Vec2(0, 30));
        auto UpMove6 = MoveBy::create(0.1, Vec2(0, 30));
        CallFunc* countUpStartFunc = CallFunc::create(CC_CALLBACK_0(CharacterEfectView::countUpStart, this));
        CallFunc* countUpEndFunc = CallFunc::create(CC_CALLBACK_0(CharacterEfectView::countUpEnd, this));

        _valuLabel->runAction(Sequence::create(DownMove1, DownMove2, DownMove3, UpMove1, UpMove2, UpMove3, UpMove4, UpMove5, UpMove6, NULL));
        _valuLabel->runAction(Sequence::create(fadeIn, DelayTime::create(0.5), fadeOut, NULL));
        _valuLabel->runAction(Sequence::create(countUpStartFunc, DelayTime::create(1.0), countUpEndFunc, NULL));


        auto UpMoveRsulut = MoveBy::create(1.0, Vec2(0, 128));
        auto fadeInRsulut = FadeIn::create(0.2);
        auto fadeOutRsulut = FadeOut::create(0.2);
        resultLabel->runAction(Sequence::create(DelayTime::create(0.5), fadeInRsulut, DelayTime::create(0.8), fadeOutRsulut, NULL));
        resultLabel->runAction(Sequence::create(DelayTime::create(0.5), UpMoveRsulut, NULL));
    } else if (efectType == 1) {
        _valuLabel = Label::createWithTTF(_statusType + FormatWithCommas(_value), FONT_NAME_2, 28);
        _valuLabel->enableOutline(Color4B(Color3B::BLACK), 2);
        _valuLabel->setOpacity(0);

        auto bugingEyesLabel = Label::createWithTTF("出目:" + FormatWithCommas(bugingEyes), FONT_NAME_2, 28);
        bugingEyesLabel->enableOutline(Color4B(Color3B::BLACK), 2);
        bugingEyesLabel->setOpacity(0);

        auto resultLabel = Label::createWithTTF(_UpDown + FormatWithCommas(result) + endComent, FONT_NAME_2, 32);
        resultLabel->enableOutline(Color4B(fontColor), 2);
        resultLabel->setOpacity(0);

        float heightPosi1 = 150;
        float heightPosi2 = 128;

        _valuLabel->setPosition(Vec2(120, heightPosi1));
        bugingEyesLabel->setPosition(Vec2(-120, heightPosi1));
        resultLabel->setPosition(Vec2(0, heightPosi2));
        addChild(_valuLabel, 100);
        addChild(bugingEyesLabel, 100);
        addChild(resultLabel, 101);

        auto fadeIn1 = FadeIn::create(0.1);
        auto fadeOut1 = FadeOut::create(0.3);
        auto fadeIn2 = FadeIn::create(0.1);
        auto fadeOut2 = FadeOut::create(0.3);

        auto leftMove1 = MoveBy::create(0.3, Vec2(60, 0));
        auto leftMove2 = MoveBy::create(0.3, Vec2(40, 0));
        auto leftMove3 = MoveBy::create(0.4, Vec2(20, 0));

        auto rightMove1 = MoveBy::create(0.3, Vec2(-60, 0));
        auto rightMove2 = MoveBy::create(0.3, Vec2(-40, 0));
        auto rightMove3 = MoveBy::create(0.4, Vec2(-20, 0));

        CallFunc* countUpStartFunc = CallFunc::create(CC_CALLBACK_0(CharacterEfectView::countUpStart, this));
        CallFunc* countUpEndFunc = CallFunc::create(CC_CALLBACK_0(CharacterEfectView::countUpEnd, this));

        _valuLabel->runAction(Sequence::create(rightMove1, rightMove2, rightMove3, NULL));
        _valuLabel->runAction(Sequence::create(fadeIn1, DelayTime::create(0.5), fadeOut1, NULL));
        _valuLabel->runAction(Sequence::create(countUpStartFunc, DelayTime::create(1.0), countUpEndFunc, NULL));

        bugingEyesLabel->runAction(Sequence::create(leftMove1, leftMove2, leftMove3, NULL));
        bugingEyesLabel->runAction(Sequence::create(fadeIn2, DelayTime::create(0.5), fadeOut2, NULL));

        auto UpMoveRsulut = MoveBy::create(1.0, Vec2(0, 128));
        auto fadeInRsulut = FadeIn::create(0.2);
        auto fadeOutRsulut = FadeOut::create(0.2);
        resultLabel->runAction(Sequence::create(DelayTime::create(0.5), fadeInRsulut, DelayTime::create(0.8), fadeOutRsulut, NULL));
        resultLabel->runAction(Sequence::create(DelayTime::create(0.5), UpMoveRsulut, NULL));
    } else {}
}

CharacterEfectView* CharacterEfectView::createViewCellEfectUpEfect(int viewValue, int type)
{
    auto efect = new CharacterEfectView;
    efect->viewCellEfectUpEfect(viewValue, type);
    efect->autorelease();

    return efect;
}

void CharacterEfectView::viewCellEfectUpEfect(int viewValue, int type)
{
    std::string mess;
    int moveDef1;
    int moveDef2;
    int moveDef3;
    mess = "リーダスキル効果";
    moveDef1 = -30;
    moveDef2 = -20;
    moveDef3 = -10;
    auto skillLabel = Label::createWithTTF(mess, FONT_NAME_2, 28);
    addChild(skillLabel, 200);
    skillLabel->setPosition(Vec2(-(moveDef1 + moveDef2 + moveDef3), 200 + abs(moveDef1) + abs(moveDef2) + abs(moveDef3)));
    skillLabel->enableOutline(Color4B(Color3B::RED), 2);
    skillLabel->setOpacity(0);
    std::string efectName;

    switch (type) {
    case 1: {
        efectName = StringUtils::format("+%dマス", viewValue);
        break;
    }
    case 2: {
        efectName = StringUtils::format("+%dスコア", viewValue);
        break;
    }
    default:
        break;
    }
    auto skillLabel2 = Label::createWithTTF(efectName, FONT_NAME_2, 32);
    addChild(skillLabel2, 200);
    skillLabel2->setPosition(skillLabel->getPosition() + Point(0, -skillLabel2->getContentSize().height / 2 - 10));
    skillLabel2->enableOutline(Color4B(Color3B::RED), 2);
    skillLabel2->setOpacity(0);

    auto fadeIn1 = FadeIn::create(0.1);
    auto fadeOut1 = FadeOut::create(0.3);
    skillLabel->runAction(Sequence::create(fadeIn1, DelayTime::create(0.6), fadeOut1, NULL));

    auto fadeIn2 = FadeIn::create(0.1);
    auto fadeOut2 = FadeOut::create(0.3);
    skillLabel2->runAction(Sequence::create(fadeIn2, DelayTime::create(0.6), fadeOut2, NULL));

    auto topMove1 = MoveBy::create(0.1, Vec2(moveDef1, -abs(moveDef1)));
    auto topMove2 = MoveBy::create(0.1, Vec2(moveDef2, -abs(moveDef2)));
    auto topMove3 = MoveBy::create(0.1, Vec2(moveDef3, -abs(moveDef3)));
    auto topMove4 = MoveBy::create(0.1, Vec2(moveDef1, -abs(moveDef1)));
    auto topMove5 = MoveBy::create(0.1, Vec2(moveDef2, -abs(moveDef2)));
    auto topMove6 = MoveBy::create(0.1, Vec2(moveDef3, -abs(moveDef3)));
    skillLabel->runAction(Sequence::create(topMove1, topMove2, topMove3, DelayTime::create(0.4), topMove6, topMove5, topMove4, NULL));

    auto underMove1 = MoveBy::create(0.1, Vec2(moveDef1, -abs(moveDef1)));
    auto underMove2 = MoveBy::create(0.1, Vec2(moveDef2, -abs(moveDef2)));
    auto underMove3 = MoveBy::create(0.1, Vec2(moveDef3, -abs(moveDef3)));
    auto underMove4 = MoveBy::create(0.1, Vec2(moveDef1, -abs(moveDef1)));
    auto underMove5 = MoveBy::create(0.1, Vec2(moveDef2, -abs(moveDef2)));
    auto underMove6 = MoveBy::create(0.1, Vec2(moveDef3, -abs(moveDef3)));
    skillLabel2->runAction(Sequence::create(underMove1, underMove2, underMove3, DelayTime::create(0.4), underMove6, underMove5, underMove4, NULL));
}

CharacterEfectView* CharacterEfectView::createViewDemeritCellTypeDeleteEfect(int type)
{
    auto efect = new CharacterEfectView;
    efect->viewDemeritCellTypeDeleteEfect(type);
    efect->autorelease();
    return efect;
}

void CharacterEfectView::viewDemeritCellTypeDeleteEfect(int type)
{
    std::string mess;
    int moveDef1;
    int moveDef2;
    int moveDef3;
    switch (type) {
    case 1: {
        mess = "スキル効果";
        moveDef1 = 30;
        moveDef2 = 20;
        moveDef3 = 10;
        break;
    }
    case 2: {
        mess = "リーダスキル効果";
        moveDef1 = -30;
        moveDef2 = -20;
        moveDef3 = -10;
        break;
    }
    default:
        break;
    }
    auto skillLabel = Label::createWithTTF(mess, FONT_NAME_2, 28);
    addChild(skillLabel, 200);
    skillLabel->setPosition(Vec2(-(moveDef1 + moveDef2 + moveDef3), 200 + abs(moveDef1) + abs(moveDef2) + abs(moveDef3)));
    skillLabel->enableOutline(Color4B(Color3B::RED), 2);
    skillLabel->setOpacity(0);
    auto skillLabel2 = Label::createWithTTF("デメリットマス無効!!", FONT_NAME_2, 32);
    addChild(skillLabel2, 200);
    skillLabel2->setPosition(skillLabel->getPosition() + Point(0, -skillLabel2->getContentSize().height / 2 - 10));
    skillLabel2->enableOutline(Color4B(Color3B::RED), 2);
    skillLabel2->setOpacity(0);

    auto fadeIn1 = FadeIn::create(0.1);
    auto fadeOut1 = FadeOut::create(0.3);
    skillLabel->runAction(Sequence::create(fadeIn1, DelayTime::create(0.6), fadeOut1, NULL));

    auto fadeIn2 = FadeIn::create(0.1);
    auto fadeOut2 = FadeOut::create(0.3);
    skillLabel2->runAction(Sequence::create(fadeIn2, DelayTime::create(0.6), fadeOut2, NULL));

    auto topMove1 = MoveBy::create(0.1, Vec2(moveDef1, -abs(moveDef1)));
    auto topMove2 = MoveBy::create(0.1, Vec2(moveDef2, -abs(moveDef2)));
    auto topMove3 = MoveBy::create(0.1, Vec2(moveDef3, -abs(moveDef3)));
    auto topMove4 = MoveBy::create(0.1, Vec2(moveDef1, -abs(moveDef1)));
    auto topMove5 = MoveBy::create(0.1, Vec2(moveDef2, -abs(moveDef2)));
    auto topMove6 = MoveBy::create(0.1, Vec2(moveDef3, -abs(moveDef3)));
    skillLabel->runAction(Sequence::create(topMove1, topMove2, topMove3, DelayTime::create(0.4), topMove6, topMove5, topMove4, NULL));

    auto underMove1 = MoveBy::create(0.1, Vec2(moveDef1, -abs(moveDef1)));
    auto underMove2 = MoveBy::create(0.1, Vec2(moveDef2, -abs(moveDef2)));
    auto underMove3 = MoveBy::create(0.1, Vec2(moveDef3, -abs(moveDef3)));
    auto underMove4 = MoveBy::create(0.1, Vec2(moveDef1, -abs(moveDef1)));
    auto underMove5 = MoveBy::create(0.1, Vec2(moveDef2, -abs(moveDef2)));
    auto underMove6 = MoveBy::create(0.1, Vec2(moveDef3, -abs(moveDef3)));
    skillLabel2->runAction(Sequence::create(underMove1, underMove2, underMove3, DelayTime::create(0.4), underMove6, underMove5, underMove4, NULL));
}

CharacterEfectView* CharacterEfectView::createViewDiceDoubleValueResultEfect(int doubleValue)
{
    auto efect = new CharacterEfectView;
    efect->viewDiceDoubleValueResultEfect(doubleValue);
    efect->autorelease();
    return efect;
}

void CharacterEfectView::viewDiceDoubleValueResultEfect(int doubleValue)
{
    std::string mess;
    int moveDef1;
    int moveDef2;
    int moveDef3;
    mess = "リーダスキル効果";
    moveDef1 = -30;
    moveDef2 = -20;
    moveDef3 = -10;
    auto skillLabel = Label::createWithTTF(mess, FONT_NAME_2, 28);
    addChild(skillLabel, 200);
    skillLabel->setPosition(Vec2(0, 200 + moveDef1 + moveDef2 + moveDef3));
    skillLabel->enableOutline(Color4B(Color3B::RED), 2);
    skillLabel->setOpacity(0);
    std::string efectName  = StringUtils::format("出目を%d倍", doubleValue);
    auto skillLabel2 = Label::createWithTTF(efectName, FONT_NAME_2, 32);
    addChild(skillLabel2, 200);
    skillLabel2->setPosition(skillLabel->getPosition() + Point(0, -skillLabel2->getContentSize().height / 2 - 10));
    skillLabel2->enableOutline(Color4B(Color3B::RED), 2);
    skillLabel2->setOpacity(0);

    auto fadeIn1 = FadeIn::create(0.1);
    auto fadeOut1 = FadeOut::create(0.3);
    skillLabel->runAction(Sequence::create(fadeIn1, DelayTime::create(0.6), fadeOut1, NULL));

    auto fadeIn2 = FadeIn::create(0.1);
    auto fadeOut2 = FadeOut::create(0.3);
    skillLabel2->runAction(Sequence::create(fadeIn2, DelayTime::create(0.6), fadeOut2, NULL));

    auto topMove1 = MoveBy::create(0.1, Vec2(0, abs(moveDef1)));
    auto topMove2 = MoveBy::create(0.1, Vec2(0, abs(moveDef2)));
    auto topMove3 = MoveBy::create(0.1, Vec2(0, abs(moveDef3)));
    auto topMove4 = MoveBy::create(0.1, Vec2(0, abs(moveDef1)));
    auto topMove5 = MoveBy::create(0.1, Vec2(0, abs(moveDef2)));
    auto topMove6 = MoveBy::create(0.1, Vec2(0, abs(moveDef3)));
    skillLabel->runAction(Sequence::create(topMove1, topMove2, topMove3, DelayTime::create(0.4), topMove6, topMove5, topMove4, NULL));

    auto underMove1 = MoveBy::create(0.1, Vec2(0, abs(moveDef1)));
    auto underMove2 = MoveBy::create(0.1, Vec2(0, abs(moveDef2)));
    auto underMove3 = MoveBy::create(0.1, Vec2(0, abs(moveDef3)));
    auto underMove4 = MoveBy::create(0.1, Vec2(0, abs(moveDef1)));
    auto underMove5 = MoveBy::create(0.1, Vec2(0, abs(moveDef2)));
    auto underMove6 = MoveBy::create(0.1, Vec2(0, abs(moveDef3)));
    skillLabel2->runAction(Sequence::create(underMove1, underMove2, underMove3, DelayTime::create(0.4), underMove6, underMove5, underMove4, NULL));
}


void CharacterEfectView::countUpStart()
{
    schedule(schedule_selector(CharacterEfectView::countUp));
}
void CharacterEfectView::countUpEnd()
{
    unschedule(schedule_selector(CharacterEfectView::countUp));
}

void CharacterEfectView::countUp(float delta)
{
    if (_upFlag == true) {
        _value += 1;
    } else {
        _value -= 1;
    }
    _valuLabel->setString(_statusType + FormatWithCommas(_value));
}
