#ifndef __CharacterView_H__
#define __CharacterView_H__

#include "cocos2d.h"
#include "create_func.h"
#include "TeamStatus.h"
#include "CellType.h"

USING_NS_CC;

class CharacterView : public Sprite {
public:
    static CharacterView* create(TeamStatus* teamStatus, std::string CharacterFilePath, std::string targetFilePath, std::string label, int row, int column);
    bool init(std::string CharacterFilePath, std::string label);
    
    CharacterView(TeamStatus* teamStatus, int column);
    
    CC_SYNTHESIZE(int, _power, Power);
    CC_SYNTHESIZE(int, _technique, Technique);
    CC_SYNTHESIZE(int, _brake, Brake);
    
    void showEffectOnTop(std::string imageName);
    void setEffectOnHeadByCellType(CELL_TYPE type);
    
    void removeEffectOnTop();
    void gensokuEffect(bool flag);
    void demeritEffect(bool flag);
    
    void removeDemerit();
    
    void setCharacterColumn(int column);
    
    int getCharacterColumn();
    
    void setEnableCharacter(bool isActive);
    
    void setNameLabelVisible(bool flag);
    std::string getNameLabelString();
    cocos2d::Point getNameLabelPosition();
    
    void jump();
    void jump(Vec2 position);

private:
    enum TAG_SPRITE {
        SWEAT,
    };

    const float RACE_JUMP = 100.0;

    void showCharacter(const char* fileName);
    void showCharacterLabel(std::string label);
    
    void prepareAnimations();
    
    void showDemerit();
    void disableDemerit();
    
    Sprite* _characterSprite;
    Sprite* _target;
    Sprite* _atamaUe;
    Sprite* _demeritEffect;
    Sprite* _characterFairy;
    
    Sprite* _characterLabelSprite;
    Sprite* _characterPoint;
    bool _characterLabelSpriteSetFlag;
    
    int _characterRow;
    int _characterColumn;
    
    bool _activeUE;
    bool _demerit;
    bool _gensokuActive;
    bool _vissibleNameLabelFlag;
    bool _isCharacterFairyMoving;
    std::string _characterNameString;
};


#endif /* defined(__CharacterView_H__) */
