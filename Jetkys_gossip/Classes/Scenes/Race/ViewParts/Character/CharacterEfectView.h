#ifndef __syanago__CharacterEfectView__
#define __syanago__CharacterEfectView__

#include "cocos2d.h"
using namespace cocos2d;

class CharacterEfectView: public cocos2d::Layer
{
public:
    virtual bool init();
    CREATE_FUNC(CharacterEfectView);
    CharacterEfectView();
    ~CharacterEfectView();
    
    /**
     *
     マスの演出の表示を行う(CharacterViewにaddChildして利用ください)
     type       :マスの種類
     bugingEyes :サイコロの出目
     result     :マスの計算により出た目
     extension  :馬力
     reaction   :技量
     decisition :忍耐
     *
     **/
    static CharacterEfectView* createCellActionEfect(int type, int bugingEyes, int result, int extension, int reaction, int decisition);
    
    /**
     *
     リーダースキル[加速マス、スコアマスの効果アップ]演出の表示を行う(CharacterViewにaddChildして利用ください)
     viewValue  :増加した量
     type       :[1]加速マス [2]スコアマス
     *
     **/
    static CharacterEfectView* createViewCellEfectUpEfect(int viewValue,int type);
    
    /**
     *
     リーダースキル[デメリットマスを無効]演出の表示を行う(CharacterViewにaddChildして利用ください)
     type   :[1]スキル [2]リーダースキル
     *
     **/
    static CharacterEfectView* createViewDemeritCellTypeDeleteEfect(int type);
    
    /**
     *
     *
     リーダースキル[出目を%d倍する]演出の表示を行う(CharacterViewにaddChildして利用ください)
     doubleValue    :倍になる数字
     *
     **/
    static CharacterEfectView* createViewDiceDoubleValueResultEfect(int doubleValue);
    
private:
    /**
     *
     マスの演出のメソッド
     *
     **/
    void cellActionEfect(int type, int bugingEyes, int result, int extension, int reaction, int decisition);
    
    /**
     *
     リーダースキル[加速マス、スコアマスの効果アップ]演出のメソッド
     *
     **/
    void viewCellEfectUpEfect(int viewValue,int type);
    
    /**
     *
     リーダースキル,スキル[デメリットマスを無効]演出のメソっド
     *
     **/
    void viewDemeritCellTypeDeleteEfect(int type);
    
    /**
     *
     リーダースキル[出目を%d倍する]演出のメソッド
     *
     **/
    void viewDiceDoubleValueResultEfect(int doubleValue);
    
    
    /**
     *
     文字のカウントアップ用のメソッド
     *
     **/
    void countUpStart();
    void countUpEnd();
    void countUp(float delta);
    
   
    Label* _valuLabel;
    bool _upFlag;
    int _value;
    std::string _UpDown = {0};
    std::string _statusType = {0};
    
};

#endif /* defined(__syanago__CharacterEfectView__) */
