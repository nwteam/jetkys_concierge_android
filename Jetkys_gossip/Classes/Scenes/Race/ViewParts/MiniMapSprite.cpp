#include "MiniMapSprite.h"
#include "Define.h"

MiniMapSprite* MiniMapSprite::create(int enemyCharacterCount, float scale)
{
    MiniMapSprite* sprite = new MiniMapSprite(enemyCharacterCount, scale);
    if (sprite && sprite->initWithFile("MapMini.png")) {
        sprite->initialize();
        Size winSize = Director::getInstance()->getWinSize();
        sprite->setPosition(Point(winSize.width / 2, winSize.height / 2));
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return NULL;
}

MiniMapSprite::MiniMapSprite(int enemyCharacterCount, float scale):
    _enemyCharacterCount(enemyCharacterCount)
    , _scale(scale)
{}

void MiniMapSprite::initialize()
{
    Sprite* frame = Sprite::create("BoderMini.png");
    frame->setPosition(Point(frame->getContentSize().width / 2 + 10.0, getContentSize().height / 2));
    frame->setTag(TAG_SPRITE::FRAME);
    addChild(frame, 2);

    _playerOnMinimap = Sprite::create("UserMini.png");
    _playerOnMinimap->setPosition(Point(frame->getPositionX() + 5.0, 30.0));
    addChild(_playerOnMinimap, 1);

    const float y = 2.5 * 3.0 * 58.0 / 33.5;
    for (int i = 0; i < _enemyCharacterCount; i++) {
        Sprite* miniRival = Sprite::create("AiTeMini.png");
        miniRival->setPosition(_playerOnMinimap->getPosition() + Vec2(0, (i + 1) * y));
        addChild(miniRival, 1);
        _rivalsOnMinimap.push_back(miniRival);
    }
}

void MiniMapSprite::moveCharacters(int playerCharacterPosition, int enemyCharacterPosition0, int enemyCharacterPosition1, int enemyCharacterPosition2)
{
    Vec2 vfMoveDis = Vec2(58.0f, 0.0) * (_scale * playerCharacterPosition);

    Sprite* frame = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::FRAME));
    float maxMoveDis = getContentSize().width - 10.0f - frame->getContentSize().width / 2;

    if (frame->getPosition().x + vfMoveDis.x > maxMoveDis) {
        vfMoveDis = Vec2(maxMoveDis - frame->getPosition().x, 0);
    }
    frame->runAction(MoveBy::create(1.2, vfMoveDis));

    _playerOnMinimap->runAction(MoveBy::create(1.2, vfMoveDis));

    for (int i = 0; i < _rivalsOnMinimap.size(); i++) {
        int distance;
        switch (i) {
        case 0:
            distance = enemyCharacterPosition0;
            break;
        case 1:
            distance = enemyCharacterPosition1;
            break;
        case 2:
            distance = enemyCharacterPosition2;
            break;
        }
        _rivalsOnMinimap.at(i)->runAction(MoveBy::create(1.2, Vec2(VELOCITY_CHARACTER.x, 0.0) * (_scale * distance)));
    }
}

void MiniMapSprite::moveFrame(float distance, bool autoMove)
{
    Sprite* frame = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::FRAME));
    if (autoMove) {
        frame->runAction(MoveBy::create(0.4, Vec2(distance * _scale, 0.0)));
    } else {
        frame->setPosition(Point(frame->getPositionX() - distance * _scale, frame->getPositionY()));
    }
}












