#include "ScoreAndTurnLayer.h"
#include "FontDefines.h"

ScoreAndTurnLayer::ScoreAndTurnLayer():
    _status(nullptr), _targetType(TARGET_NONE)
{}

ScoreAndTurnLayer::~ScoreAndTurnLayer()
{}

bool ScoreAndTurnLayer::init(StatusManager* status, const GOAL_TARGET_TYPE targetType)
{
    if (!Layer::create()) {
        return false;
    }
    _status = status;
    _targetType = targetType;

    showScore();
    showTurn();
    showGoalTarget(targetType);
    return true;
}

void ScoreAndTurnLayer::showScore()
{
    Sprite* scoreImage = Sprite::create("scoreImg.png");
    scoreImage->setTag(TAG_SPRITE::SCORE_IMAGE);
    scoreImage->setPosition(Point(scoreImage->getContentSize().width / 2 + 5.0, Director::getInstance()->getWinSize().height * 7 / 8 + 100.0));
    addChild(scoreImage, Z_ORDER::Z_SCORE_IMAGE);

    auto scoreLabel = Label::createWithTTF("0", FONT_NAME_2, 22);
    scoreLabel->setTag(TAG_SPRITE::SCORE_LABEL);
    scoreLabel->setPosition(Point(scoreImage->getPositionX() + scoreImage->getContentSize().width + 5.0,
                                  scoreImage->getPositionY() - 11));
    scoreLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(scoreLabel, Z_ORDER::Z_SCORE_LABEL);
}

void ScoreAndTurnLayer::showTurn()
{
    auto scorelabel = getChildByTag(TAG_SPRITE::SCORE_LABEL);
    Sprite* turnImage = Sprite::create("tanImg.png");
    turnImage->setTag(TAG_SPRITE::TURN_IMAGE);
    turnImage->setPosition(Point(scorelabel->getContentSize().width + scorelabel->getPositionX() + 60.0, getChildByTag(TAG_SPRITE::SCORE_IMAGE)->getPositionY()));
    addChild(turnImage, Z_ORDER::Z_TURN_IMAGE);

    auto turnLabel = Label::createWithTTF("000", FONT_NAME_2, 22);
    turnLabel->setTag(TAG_SPRITE::TURN_LABEL);
    turnLabel->setPosition(Point(turnImage->getContentSize().width + turnImage->getPositionX() - 5.0,
                                 turnImage->getPositionY() - 11));
    turnLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(turnLabel, Z_ORDER::Z_TURN_LABEL);

}

void ScoreAndTurnLayer::showGoalTarget(const GOAL_TARGET_TYPE targetType)
{
    if (targetType == TARGET_SCORE) {
        auto scoreTargetLb = Label::createWithTTF(StringUtils::format("目標スコア：%d", _status->score->targetScore), FONT_NAME_2, 25);
        scoreTargetLb->setTag(TAG_SPRITE::TARGET_LABEL);
        scoreTargetLb->setAnchorPoint(Point(0.0, 0.5));
        scoreTargetLb->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
        scoreTargetLb->setPosition(Point(5.0, Director::getInstance()->getWinSize().height * 7 / 8 + 51 + scoreTargetLb->getContentSize().height / 2 + 10.0 - 25));
        scoreTargetLb->setColor(Color3B::RED);
        addChild(scoreTargetLb, Z_ORDER::Z_TARGET_LABEL);
    } else if (targetType == TARGET_TURN) {
        auto turnTargetLb = Label::createWithTTF(StringUtils::format("制限：%dターン", _status->sequence->tagerTurn), FONT_NAME_2, 25);
        turnTargetLb->setTag(TAG_SPRITE::TARGET_LABEL);
        turnTargetLb->setAnchorPoint(Point(0.0, 0.5));
        turnTargetLb->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
        turnTargetLb->setPosition(Point(5.0, Director::getInstance()->getWinSize().height * 7 / 8 + 51 + turnTargetLb->getContentSize().height / 2 + 10.0 - 25));
        turnTargetLb->setColor(Color3B::RED);
        addChild(turnTargetLb, Z_ORDER::Z_TARGET_LABEL);
    }
}

void ScoreAndTurnLayer::refresh()
{
    if (!_status) {
        return;
    }
    setBonusScore(_status->score->bonusScore);
    setTurn(_status->sequence->elaplsedTurn);

    if (_targetType == TARGET_SCORE) {
        setTagetLabel(StringUtils::format("目標スコア：%d", _status->score->targetScore));
    } else if (_targetType == TARGET_TURN) {
        setTagetLabel(StringUtils::format("制限：%dターン", _status->sequence->tagerTurn - _status->sequence->elaplsedTurn));
    }
}

void ScoreAndTurnLayer::setBonusScore(const int score)
{
    auto scorelabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::SCORE_LABEL));
    if (!scorelabel) {
        CC_ASSERT("[ScoreAndTurnLayer::setBonusScore] not fond ScoreLabel");
        return;
    }
    scorelabel->setString(StringUtils::format("%d", score));
}

void ScoreAndTurnLayer::setTurn(const int turn)
{
    auto turnlabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::TURN_LABEL));
    if (!turnlabel) {
        CC_ASSERT("[ScoreAndTurnLayer::setBonusScore] not fond ScoreLabel");
        return;
    }
    turnlabel->setString(StringUtils::format("%d", turn));
}

void ScoreAndTurnLayer::setTagetLabel(const std::string targetContent)
{
    auto targetLabel = static_cast<Label*>(getChildByTag(TAG_SPRITE::TARGET_LABEL));
    if (!targetLabel) {
        CC_ASSERT("[ScoreAndTurnLayer::setBonusScore] not fond TargetLabel");
        return;
    }
    targetLabel->setString(targetContent);
}

