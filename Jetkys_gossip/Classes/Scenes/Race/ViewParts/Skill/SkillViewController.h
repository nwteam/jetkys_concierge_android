#ifndef __syanago__SkillViewController__
#define __syanago__SkillViewController__

#include "cocos2d.h"
#include "RaceUIControll.h"
#include "DialogView.h"
#include "SoundHelper.h"
#include "create_func.h"
#include "CharacterModel.h"
#include "SkillButton.h"
#include "SkillDescription.h"

USING_NS_CC;

class RaceScene;
class SkillButton;

class SkillViewController : public Layer,  public create_func<SkillViewController>, public DialogDelegate
{
public:
    SkillViewController();
    using create_func::create;
    bool init(RaceScene* raceScene);
    
    void refreshSkillButtons();
    bool getSkillSound();
    void changeDeltaDelay(int change);
    void chargeSkillWithEffect(int change);
    
    bool getDiaLogShow();
    
    void showDesFromMenu(int type);
    void hidenDesFromMenu();
    void hidenDesAni();
    void showDesAni(int type);
    
    bool isDesShowing();
    
    void activeSkillBar();
    
    void btDialogCallback(ButtonIndex aIndex);
    
    void setEnableToButtons(bool hidden);
    
    int getCountOfChargedSkill();
    
    void showConfirmSkillDialog(int index, std::shared_ptr<const SkillModel>& skillModel);
    
    RaceUIControll* _raceControl;
    bool _touchActive;
    
private:
    void showIcons();
    void showRemainingTurnLabels();
    void activateSkill(const std::shared_ptr<const SkillModel>& skillModel);
    
    void showDescription(const int viewSkillNumber);
    void createDescription();
    
    void setListIcon();
    void setListSkill();
    
    void downTimeDelayBySkill(int skillID);
    
    void deActiveSkillBar();
    
    void showSkillCutIn(int playerNo);
    
    void checkSkillAvailable(float delta);
    void callSkillfunc(float delta);
    
    const float SKILL_DISCRIPTION_HEIGH = 145.0f;
    
    RaceScene *_raceScene;
    
    Size _winSize;
    std::vector<std::shared_ptr<const SkillModel>> _skillModels;
    
    std::vector<SkillButton* > _skillButtons;
    
    
    SkillDescription* _skillDescription;
    
    int _skillUse;
    int _bufSkillUser;
    
    bool _isSkillStop;
    bool _soundSkill;
    
    bool _dialogShow;
    
    int _turnTarget;
    

};


#endif /* defined(__syanago__SkillViewController__) */
