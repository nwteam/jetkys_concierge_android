#include "SkillViewController.h"
#include "jCommon.h"
#include "RaceScene.h"
#include "SkillDescriptionLayer.h"
#include "FontDefines.h"
#include "SkillAndLederSkillKind.h"

SkillViewController::SkillViewController():
    _skillUse(0)
    , _soundSkill(false)
    , _isSkillStop(false)
    , _dialogShow(false)
    , _turnTarget(0)
{
    _winSize = Director::getInstance()->getWinSize();
}

bool SkillViewController::init(RaceScene* raceScene)
{
    if (!Layer::init()) {
        return false;
    }
    _raceScene = raceScene;

    _skillModels = {nullptr, nullptr, nullptr, nullptr, nullptr};
    for (int i = 0; i < 5; i++) {

        const int skillId = (raceScene->statusManager->teamCharacters[i] != nullptr && raceScene->statusManager->teamCharacters[i]->characterModel != nullptr) ? raceScene->statusManager->teamCharacters[i]->characterModel->getSkill() : 0;
        _skillModels.at(i) = SkillModel::find(skillId);
    }
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("icon.plist");

    setPosition(Point(0, -_winSize.height / 2 + 50.0));


    _skillButtons = {nullptr, nullptr, nullptr, nullptr, nullptr};
    for (int i = 0; i < 5; i++) {
        if (raceScene->statusManager->teamCharacters[i] != nullptr && raceScene->statusManager->teamCharacters[i]->characterModel != nullptr) {
            SkillButton* skillButton = SkillButton::create(raceScene->statusManager->teamCharacters[i]->characterModel, i, this);
            _skillButtons.at(i) = skillButton;
        }
    }

    createDescription();
    SkillDescriptionLayer* skillDescriptionLayer = SkillDescriptionLayer::create(_skillButtons);
    skillDescriptionLayer->_skillViewController = this;
    addChild(skillDescriptionLayer, 2);

    return true;
}


void SkillViewController::createDescription()
{
    _skillDescription = SkillDescription::create(_skillModels);
    _skillDescription->setPosition(Point(_skillDescription->getContentSize().width / 2 + 50.0,
                                         Director::getInstance()->getWinSize().height / 2 + SKILL_DISCRIPTION_HEIGH));
    _skillDescription->setOpacity(0);
    _skillDescription->setVisible(false);
    addChild(_skillDescription, 10);
}

void SkillViewController::showDesAni(int type)
{
    const int tag = type;
    if (_skillModels.at(tag) != nullptr) {
        _skillDescription->viewDiscription(tag);
        _skillDescription->setVisible(true);
        _skillDescription->setPosition(Point(_skillDescription->getContentSize().width / 2 + 20.0 + (20.0 * tag),
                                             Director::getInstance()->getWinSize().height / 2 + SKILL_DISCRIPTION_HEIGH));
        _skillDescription->runAction(FadeIn::create(0.2));
    }
}

void SkillViewController::showDescription(const int viewSkillNumber)
{
    _skillDescription->stopAllActions();
    _skillDescription->viewDiscription(viewSkillNumber);
    _skillDescription->setOpacity(255);
    _skillDescription->setVisible(true);
}

void SkillViewController::showDesFromMenu(int type)
{
    const int tag = type;
    if (_skillModels.at(tag) != nullptr) {
        _skillDescription->setPosition(Vec2(_skillDescription->getContentSize().width / 2 + 20.0 + (20.0 * tag),
                                            Director::getInstance()->getWinSize().height / 2 + SKILL_DISCRIPTION_HEIGH));
        showDescription(tag);
    }
}

void SkillViewController::hidenDesAni()
{
    _skillDescription->runAction(Sequence::create(FadeOut::create(0.2),
                                                  CallFunc::create(CC_CALLBACK_0(SkillViewController::hidenDesFromMenu, this)),
                                                  NULL));
}

void SkillViewController::hidenDesFromMenu()
{
    _skillDescription->stopAllActions();
    _skillDescription->setOpacity(0);
    _skillDescription->setVisible(false);
}

void SkillViewController::showConfirmSkillDialog(int index, std::shared_ptr<const SkillModel>& skillModel)
{
    if (_raceScene->statusManager->skill->remainningTurn == 0 &&
        _raceScene->statusManager->view->diceAvailable == true) {
        _dialogShow = true;
        _bufSkillUser = index + 1;

        auto dialog = DialogView::createSkillSelectDialog(skillModel->getID());
        dialog->setPosition(0, _winSize.height / 2);
        dialog->_delegate = this;
        addChild(dialog, 10);

        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    }
}

void SkillViewController::btDialogCallback(ButtonIndex aIndex)
{
    _dialogShow = false;
    if (aIndex == BT_YES) {
        _raceScene->statusManager->playerStatus->usedSkillCount++;
        _raceScene->turnStart();
        _raceScene->setCutinName(StringUtils::format("%d_t.png", _raceScene->statusManager->teamCharacters[_bufSkillUser - 1]->characterModel->getID()), _raceScene->statusManager->teamCharacters[_bufSkillUser - 1]->characterModel->getID());
        showSkillCutIn(_bufSkillUser);
        scheduleOnce(schedule_selector(SkillViewController::checkSkillAvailable), 1.0);
    } else {
        _bufSkillUser = 0;
    }
}

void SkillViewController::checkSkillAvailable(float delta)
{
    _skillUse = _bufSkillUser;
    if (_skillButtons.at(_bufSkillUser - 1)->isSkillAvailable()) {
        _soundSkill = true;
        activateSkill(_skillModels.at(_bufSkillUser - 1));
        activeSkillBar();
    }
}

bool SkillViewController::getDiaLogShow()
{
    return _dialogShow;
}

void SkillViewController::activateSkill(const std::shared_ptr<const SkillModel>& skillModel)
{
    // activate raceScene
    _raceScene->statusManager->view->pause = false;
    setEnableToButtons(true);
    _raceScene->_raceControll->setVisibleToCharacterNameLabels(true);
    _raceScene->statusManager->view->diceAvailable = true;
    _raceScene->refreshDiceColor();


    switch (skillModel->getSkillKind()) {
    case SKILL_KIND_CRITICAL_DICE: {
        if (_raceScene->statusManager->skill->remainningTurn == 0) {
            _skillButtons.at(_skillUse - 1)->setRemainningTurn(1);
            _raceScene->statusManager->skill->remainningTurn = 1;
            _raceScene->statusManager->skill->criticalDiceFlag = true;
            _raceScene->statusManager->skill->criticalDiceTargetValue = skillModel->getValue1();
        }
        break;
    }
    case SKILL_KIND_PLUS_DICE: {
        if (_raceScene->statusManager->skill->remainningTurn == 0) {
            _raceScene->statusManager->skill->remainningTurn = skillModel->getValue1();
            _skillButtons.at(_skillUse - 1)->setRemainningTurn(_raceScene->statusManager->skill->remainningTurn);
            _raceScene->statusManager->skill->plusDiceValue = skillModel->getValue2();
            refreshSkillButtons();
            _raceScene->showDiceResultUp();
        }
        break;
    }
    case SKILL_KIND_FORCE_STOP: {
        if (_raceScene->statusManager->skill->remainningTurn == 0) {
            _raceScene->statusManager->skill->remainningTurn = skillModel->getValue1();
            _skillButtons.at(_skillUse - 1)->setRemainningTurn(_raceScene->statusManager->skill->remainningTurn);
            _raceScene->selectTargetToStop(skillModel->getID(), skillModel->getValue1());
            SOUND_HELPER->playEffect(SKILL_CHARGE_EFFECT, false);
        }
        break;
    }
    case SKILL_KIND_DEMERIT_CANCEL: {
        if (_raceScene->statusManager->skill->remainningTurn == 0) {
            _raceScene->statusManager->skill->remainningTurn = skillModel->getValue1();
            refreshSkillButtons();
            _raceScene->statusManager->skill->deleteDemeritCell = true;
            _raceScene->statusManager->skill->deleteDemeritCellType = skillModel->getValue2();
        }
        break;
    }
    case SKILL_KIND_SKILL_CHARGE: {
        _isSkillStop = true;
        SOUND_HELPER->playEffect(SKILL_CHARGE_EFFECT, false);
        for (int i = 0; i < 5; i++) {
            if (_skillUse - 1 != i &&
                _skillButtons.at(i) != nullptr &&
                !_skillButtons.at(i)->isSkillAvailable()) {
                _skillButtons.at(i)->changeTriggetTurn(skillModel->getValue1());
                _skillButtons.at(i)->decreaseTriggerTurnEffect();
            }
        }
        _skillButtons.at(_skillUse - 1)->resetTriggerTurn();
        for (SkillButton* skillButton : _skillButtons) {
            if (skillButton != nullptr) {
                skillButton->refresh();
            }
        }
        _skillUse = 0;
        break;
    }
    case SKILL_KIND_MOVE_PLAYER: {
        _isSkillStop = true;
        _raceScene->statusManager->skill->remainningTurn = 1;
        _skillButtons.at(_bufSkillUser - 1)->setRemainningTurn(_raceScene->statusManager->skill->remainningTurn);
        _raceScene->statusManager->view->diceAvailable = false;
        _raceScene->statusManager->skill->movePlayerCharacterDistance = skillModel->getValue1();
        _raceScene->turnStart();
        _raceScene->statusManager->skill->remainningTurn = 0;
        _raceScene->statusManager->playerStatus->resetPreCellEffect();
        _raceScene->statusManager->playerStatus->characterView->gensokuEffect(false);
        _raceScene->hideDiceResultUpDown();
        _raceScene->_raceControll->checkCamera(CC_CALLBACK_0(RaceUIControll::onEndCheckCameraForMovePlayerSkill, _raceScene->_raceControll));
        _skillUse = 0;
        break;
    }
    case SKILL_KIND_MYPACE_RUN: {
        if (_raceScene->statusManager->skill->remainningTurn == 0) {
            _isSkillStop = true;
            _raceScene->statusManager->skill->remainningTurn = skillModel->getValue1();
            _skillButtons.at(_bufSkillUser - 1)->setRemainningTurn(_raceScene->statusManager->skill->remainningTurn);
            _raceScene->statusManager->skill->myPaceFlag = true;
            _raceScene->statusManager->skill->myPaceRunDistance = skillModel->getValue2();
        }
        break;
    }
    }
}

void SkillViewController::refreshSkillButtons()
{
    if (_raceScene->statusManager->skill->remainningTurn == 0) {
        _skillUse = 0;
    }
    deActiveSkillBar();
    for (int i = 0; i < 5; i++) {
        if ((_skillUse - 1 == i || _raceScene->statusManager->skill->remainningTurn == 0) && _skillButtons.at(i) != nullptr) {
            _skillButtons.at(i)->setRemainningTurn(_raceScene->statusManager->skill->remainningTurn);
        }
    }
}

void SkillViewController::activeSkillBar()
{
    deActiveSkillBar();
    for (SkillButton* skillButton : _skillButtons) {
        if (skillButton != nullptr) {
            skillButton->refresh();
        }
    }
    if (_isSkillStop) {
        for (SkillButton* skillButton : _skillButtons) {
            if (skillButton != nullptr) {
                skillButton->runAction(Sequence::create(
                                           ScaleTo::create(0.3, 1.35),
                                           ScaleTo::create(0.3, 1.25),
                                           CallFunc::create(CC_CALLBACK_0(SkillViewController::deActiveSkillBar, this)),
                                           NULL));
            }
        }

    }
    if (_skillUse == 0) {
        _soundSkill = false;
    } else {
        if (_skillButtons.at(_skillUse - 1) != nullptr) {
            _skillButtons.at(_skillUse - 1)->runAction(RepeatForever::create(Sequence::create(
                                                                                 ScaleTo::create(0.3, 1.35),
                                                                                 ScaleTo::create(0.3, 1.25),
                                                                                 NULL)));
        }
    }

}

void SkillViewController::deActiveSkillBar()
{
    if (_isSkillStop) {
        _isSkillStop = false;
    }

    for (SkillButton* skillButton : _skillButtons) {
        if (skillButton != nullptr) {
            skillButton->stopAllActions();
        }
    }
}

void SkillViewController::changeDeltaDelay(int change)
{
    for (SkillButton* skillButton : _skillButtons) {
        if (skillButton != nullptr) {
            skillButton->changeTriggetTurn(1);
        }
    }
}

void SkillViewController::chargeSkillWithEffect(int change)
{
    SOUND_HELPER->playEffect(SKILL_CHARGE_EFFECT, false);
    for (SkillButton* skillButton : _skillButtons) {
        if (skillButton != nullptr &&
            !skillButton->isSkillAvailable() &&
            _skillUse == 0) {
            skillButton->changeTriggetTurn(change);
            skillButton->decreaseTriggerTurnEffect();
            skillButton->refresh();
        }
    }
}

void SkillViewController::downTimeDelayBySkill(int skillID)
{
    SOUND_HELPER->playEffect(SKILL_CHARGE_EFFECT, false);
    for (int i = 0; i < 5; i++) {
        if (skillID - 1 == i  &&
            _skillButtons.at(i) != nullptr &&
            !_skillButtons.at(i)->isSkillAvailable()) {
            _skillButtons.at(i)->decreaseTriggerTurnEffect();
            _skillButtons.at(i)->refresh();
        }
    }
}

bool SkillViewController::getSkillSound()
{
    return _soundSkill;
}

void SkillViewController::showSkillCutIn(int playerNo)
{
    const int mst_id = _raceScene->getCutInId();
    SOUND_HELPER->playEffect(CHARACTER_CUT_IN_EFFECT, false);
    SOUND_HELPER->playCharacterVoiceEfect(mst_id, 18);

    auto cutInBackground = LayerColor::create(Color4B(0, 0, 0, 0), _winSize.width, _winSize.height);
    cutInBackground->setPosition(Vec2(0, _winSize.height / 2 - 50));
    cutInBackground->runAction(Sequence::create(
                                   FadeTo::create(0.1, 130),
                                   DelayTime::create(0.7),
                                   FadeOut::create(0.1),
                                   NULL));

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_ca.plist", mst_id).c_str());
    auto cutIncharacter = makeSprite(_raceScene->getCutInFailePass().c_str());
    cutInBackground->addChild(cutIncharacter);
    cutIncharacter->setAnchorPoint(Vec2(0.5, 1.0));
    cutIncharacter->setPosition(Point((cutInBackground->getContentSize().width / 2) * 3, cutInBackground->getContentSize().height - 10));
    cutIncharacter->runAction(MoveBy::create(0.3, Vec2(-_winSize.width, 0)));
    cutIncharacter->runAction(Sequence::create(
                                  FadeIn::create(0.1),
                                  DelayTime::create(0.7),
                                  FadeOut::create(0.1),
                                  CallFunc::create([&]() {
        SpriteFrameCache::getInstance()->removeSpriteFrameByName(StringUtils::format("%d_ca.plist", mst_id).c_str());
    }), NULL));
    addChild(cutInBackground, 5);
}

bool SkillViewController::isDesShowing()
{
    if (_skillDescription) {
        return _skillDescription->isVisible();
    }
    return false;
}

void SkillViewController::setEnableToButtons(bool hidden)
{
    for (SkillButton* skillButton : _skillButtons) {
        if (skillButton != nullptr) {
            skillButton->setEnabled(hidden);
        }
    }
}

int SkillViewController::getCountOfChargedSkill()
{
    int result = 0;
    for (SkillButton* skillButton : _skillButtons) {
        if (skillButton != nullptr &&
            skillButton->isSkillAvailable()) {
            result++;
        }
    }
    return result;
}
