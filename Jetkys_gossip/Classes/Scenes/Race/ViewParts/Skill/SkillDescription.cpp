#include "SkillDescription.h"
#include "FontDefines.h"

SkillDescription::SkillDescription()
{
    _skillModels.clear();
}

SkillDescription::~SkillDescription()
{}

SkillDescription* SkillDescription::create(std::vector<std::shared_ptr<const SkillModel> >& skillModels)
{
    SkillDescription* btn = new (std::nothrow) SkillDescription();
    if (btn && btn->initWithFile("SkillDesBgr.png")) {
        btn->initialize(skillModels);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void SkillDescription::initialize(std::vector<std::shared_ptr<const SkillModel> >& skillModels)
{
    _skillModels = skillModels;
    showName();
    showDescription();
}

void SkillDescription::showName()
{
    auto nameLabel = Label::createWithTTF("", FONT_NAME_4, 25, Size(400, 20));
    nameLabel->setTag(TAG_LABEL::NAME);
    nameLabel->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    nameLabel->setPosition(Point(10.0f, 89.0f));
    addChild(nameLabel, Z_ORDER::Z_NAME);
}

void SkillDescription::showDescription()
{
    auto descriptionLabel = Label::createWithTTF("", FONT_NAME_4, 20, Size(400, 60));
    descriptionLabel->setTag(TAG_LABEL::DISCRIPTION);
    descriptionLabel->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    descriptionLabel->setPosition(Point(10.0f, 62.0f));
    addChild(descriptionLabel, Z_ORDER::Z_DISCRIPTION);
}

void SkillDescription::viewDiscription(const int viewskillNumber)
{
    if (_skillModels.size() < 1) {
        CC_ASSERT("[SkillDescription::viewDiscription]error:_skillModels.size() < 1");
        return;
    }
    auto nameLabel = static_cast<Label*>(getChildByTag(TAG_LABEL::NAME));
    nameLabel->setString(_skillModels.at(viewskillNumber)->getSkillName());

    auto descriptionLabel = static_cast<Label*>(getChildByTag(TAG_LABEL::DISCRIPTION));
    descriptionLabel->setString(_skillModels.at(viewskillNumber)->getSkillDescription());
}
