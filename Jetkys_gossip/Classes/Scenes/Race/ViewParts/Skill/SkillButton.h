#ifndef __syanago__SkillButton__
#define __syanago__SkillButton__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "CharacterModel.h"
#include "SkillModel.h"
#include "SkillViewController.h"

USING_NS_CC;

class SkillViewController;

class SkillButton : public Sprite
{
public:
    SkillButton(std::shared_ptr<const CharacterModel>& characterModel, int index, SkillViewController* skillViewController);
    
    static SkillButton* create(std::shared_ptr<const CharacterModel>& characterModel, int index, SkillViewController* skillViewController);
    void initialize();
    
    void onTap();
    
    void changeTriggetTurn(int delta);
    void resetTriggerTurn();
    void refresh();
    
    void decreaseTriggerTurnEffect();
    
    bool isSkillAvailable();
    
    void setRemainningTurn(int turn);

    Rect rect() const;
    
    CC_SYNTHESIZE(bool, _enabled, Enabled);
    
private:
    void showRemainningTurnLabel();
    void showTriggerTurnLabel();
    void showDecreaseTriggetTurnEffect();
    
    void onEndDecreaseTriggerTurnEffect();
    
    std::shared_ptr<const CharacterModel> _characterModel;
    std::shared_ptr<const SkillModel> _skillModel;
    
    int _index;
    SkillViewController* _skillViewController;
    
    Sprite* _remainnigTurnLabelBackground;
    Label* _remainnigTurnLabel;
    //必ず一人のキャラクターのみが_remainningTurn > 0 になる
    int _remainningTurn;

    Sprite* _triggerTurnBackground;
    Label* _triggerTurnLabel;
    int _triggerTurn;
    
    Sprite* _decreaseTriggerTurnEffect;
    
    bool _playSound;
    
    
};






#endif /* defined(__syanago__SkillButton__) */
