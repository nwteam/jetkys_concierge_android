#include "SkillButton.h"
#include "FontDefines.h"

SkillButton::SkillButton(std::shared_ptr<const CharacterModel>& characterModel, int index, SkillViewController* skillViewController):
    _characterModel(characterModel), _skillModel(SkillModel::find(characterModel->getSkill())), _index(index), _skillViewController(skillViewController), _triggerTurn(0), _remainningTurn(0), _enabled(true), _decreaseTriggerTurnEffect(nullptr) {};

SkillButton* SkillButton::create(std::shared_ptr<const CharacterModel>& characterModel, int index, SkillViewController* skillViewController)
{
    SkillButton* btn = new (std::nothrow) SkillButton(characterModel, index, skillViewController);
    const std::string fileName = StringUtils::format("%d_i.png", characterModel->getID());

    if (btn && btn->initWithSpriteFrameName(fileName)) {
        btn->initialize();
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void SkillButton::initialize()
{
    const Vec2 DEFAULT_POSITION = Vec2(98 / 2 * 1.25 + 3.33, Director::getInstance()->getWinSize().height / 2 + 25.0);
    if (_index == 0) {
        setPosition(Vec2(DEFAULT_POSITION));
    } else {
        setPosition(Vec2(DEFAULT_POSITION.x + (98 * 1.25 + 3.33) * _index,
                         DEFAULT_POSITION.y));
    }

    setScale(1.25);
    setLocalZOrder(6 - _index);
    setTag(_index);

    showTriggerTurnLabel();
    showRemainningTurnLabel();
}


void SkillButton::showRemainningTurnLabel()
{
    Sprite* backGround = Sprite::create("itemSpr.png");
    backGround->setPosition(Point(backGround->getContentSize().width / 2,
                                  backGround->getContentSize().height / 2));
    backGround->setScale(0.8);
    backGround->setVisible(false);

    auto label = Label::createWithTTF(StringUtils::format("%d", _remainningTurn), FONT_NAME_2, 25);
    label->setColor(Color3B::WHITE);
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->setPosition(Point(backGround->getContentSize().width / 2,
                             backGround->getContentSize().height / 4));
    backGround->addChild(label, 3);

    _remainnigTurnLabelBackground = backGround;
    _remainnigTurnLabel = label;
    addChild(backGround, 2);
}

void SkillButton::showTriggerTurnLabel()
{
    _triggerTurnLabel = Label::createWithTTF(StringUtils::format("%d", _skillModel->getTriggerConditions()), FONT_NAME_2, 15);
    _triggerTurnLabel->setColor(Color3B::WHITE);
    _triggerTurnLabel->setPosition(Vec2((98 * 1.125) - (22 * 0.8) - 5,
                                        getContentSize().height * 1.125 - _triggerTurnLabel->getContentSize().height));
    addChild(_triggerTurnLabel, 2);

    _triggerTurnBackground = makeSprite("itemSpr.png");
    _triggerTurnBackground->setScale(0.8);
    _triggerTurnBackground->setPosition(Point((98 * 1.125) - (22 * 0.8) - 5, _triggerTurnLabel->getPosition().y + 5));
    addChild(_triggerTurnBackground, 1);
}

void SkillButton::showDecreaseTriggetTurnEffect()
{
    if (_decreaseTriggerTurnEffect != nullptr) {
        onEndDecreaseTriggerTurnEffect();
    }
    _decreaseTriggerTurnEffect = makeSprite("aka_yazirusi.png");
    _decreaseTriggerTurnEffect->setScale(0.2);
    _decreaseTriggerTurnEffect->setPosition(Point(_triggerTurnLabel->getPositionX(),
                                                  _triggerTurnLabel->getPositionY() + 5));
    addChild(_decreaseTriggerTurnEffect, 4);
}

void SkillButton::onTap()
{
    if (isSkillAvailable()) {
        _skillViewController->showConfirmSkillDialog(_index, _skillModel);
    }
}

void SkillButton::changeTriggetTurn(int delta)
{
    _triggerTurn += delta;
}

void SkillButton::resetTriggerTurn()
{
    _triggerTurn = 0;
}

void SkillButton::refresh()
{
    int remainingTriggerTurn = _skillModel->getTriggerConditions() - _triggerTurn;

    if (remainingTriggerTurn <= 0) {
        remainingTriggerTurn = 0;
        _triggerTurnBackground->setTexture("itemSprOk.png");
        _triggerTurnLabel->setVisible(false);
    } else if (remainingTriggerTurn > 0) {
        _playSound = false;
        _triggerTurnBackground->setTexture("itemSpr.png");
        _triggerTurnLabel->setString(StringUtils::format("%d", remainingTriggerTurn));
        _triggerTurnLabel->setVisible(true);
    }
    if (remainingTriggerTurn == 0 && !_playSound) {
        _playSound = true;
        SOUND_HELPER->playEffect(SKILL_CHARGE_END_EFFECT, false);
    }

    if (_remainningTurn > 0) {
        _remainnigTurnLabel->setString(StringUtils::format("%d", _remainningTurn));
        _remainnigTurnLabelBackground->setVisible(true);
        _remainnigTurnLabel->setVisible(true);
    } else {
        _remainnigTurnLabel->setVisible(false);
        _remainnigTurnLabelBackground->setVisible(false);
        stopAllActions();
    }
}

bool SkillButton::isSkillAvailable()
{
    if (_remainningTurn < 0) {
        _remainningTurn = 0;
    }
    return (_triggerTurn >= _skillModel->getTriggerConditions() && _remainningTurn == 0);
}

Rect SkillButton::rect() const
{
    return Rect(_position.x - _contentSize.width * _anchorPoint.x,
                _position.y - _contentSize.height * _anchorPoint.y,
                _contentSize.width,
                _contentSize.height);
}


void SkillButton::decreaseTriggerTurnEffect()
{
    if (_decreaseTriggerTurnEffect != nullptr) {
        onEndDecreaseTriggerTurnEffect();
    }
    _decreaseTriggerTurnEffect = Sprite::create("aka_yazirusi.png");
    _decreaseTriggerTurnEffect->setScale(0.2);
    _decreaseTriggerTurnEffect->setPosition(Point(_triggerTurnLabel->getPositionX(),
                                                  _triggerTurnLabel->getPositionY() + 5));
    _decreaseTriggerTurnEffect->runAction(Sequence::create(
                                              MoveBy::create(0.3, Vec2(0.0, 10.0)),
                                              MoveBy::create(0.3, Vec2(0.0, -10.0)),
                                              MoveBy::create(0.3, Vec2(0.0, 10.0)),
                                              MoveBy::create(0.3, Vec2(0.0, -10.0)),
                                              CallFunc::create(CC_CALLBACK_0(SkillButton::onEndDecreaseTriggerTurnEffect, this)),
                                              NULL));
    addChild(_decreaseTriggerTurnEffect, 4);
}

void SkillButton::onEndDecreaseTriggerTurnEffect()
{
    _decreaseTriggerTurnEffect->removeFromParent();
    _decreaseTriggerTurnEffect = nullptr;
}

void SkillButton::setRemainningTurn(int turn)
{
    int previousValue = _remainningTurn;
    _remainningTurn = (0 < turn) ? turn : 0;
    // スキル使用後にremainningTurn が０になった時に_triggerTurnをリセットする
    if (turn == 0 && previousValue > 0) {
        _triggerTurn = 0;
    }
    refresh();
}





