#ifndef syanago_SkillAndLederSkillKind_h
#define syanago_SkillAndLederSkillKind_h

/**
 *
   スキルのkindのデータ
   ・SKILL_KIND_CRITICAL_DICE
   効果：Nの目を出すともう一度振る事が出来る。
   名前例：「クリティカルダイス」

   ・SKILL_KIND_PLUS_DICE
   効果：Nターンの間、出目に+Xされる
   名前例：「プラスダイス」「ブーストダイス」

   ・SKILL_KIND_FORCE_STOP
   効果：タップした相手をNターン休みにする
   名前例：「強制停車」

   ・SKILL_KIND_DEMERIT_CANCEL
   効果：Nターンの間【任意のデメリット系マス（複数可）、アイテム、スキル】を無効
   名前例：「悪路耐性」

   ・SKILL_KIND_SKILL_CHARGE
   効果：自身以外のチャージ数をN減少させる
   名前例：「スキルチャージ」

   ・SKILL_KIND_MOVE_PLAYER
   効果：Nマス【戻る】or【進む】
   名前例：「バック」「アクセル」

   ・SKILL_KIND_MYPACE_RUN  =12
   効果：Nターンの間自動でMマス進み、マスの効果を無効
   名前例：「マイペース走行」
 *
 */
enum {
    SKILL_KIND_CRITICAL_DICE = 1,
    SKILL_KIND_PLUS_DICE = 2,
    SKILL_KIND_FORCE_STOP = 3,
    SKILL_KIND_DEMERIT_CANCEL = 6,
    SKILL_KIND_SKILL_CHARGE = 7,
    SKILL_KIND_MOVE_PLAYER = 9,
    SKILL_KIND_CHANGE_CELL_TYPE = 10,
    SKILL_KIND_MYPACE_RUN  = 12
};

/**
 *
   リーダースキルのkindデータ
   ・PLUS_STATUS
   効果：編成している【ボディタイプ】の【ステータス】がN倍

   ・DELETE_DEMERIT_CELL
   効果：デメリットマスの効果をN％で無効

   ・CELL_EFECT_UP
   効果：[特定マス]の効果をN倍にする


   ・DICE_RESULT_UP
   効果：N％で出目がM倍

   ・CHARGE_AT_DICE_VALUE
   効果: 出目がNの場合、N％の確率で自身以外のスキルを1チャージ

 */
enum LEADER_SKILL_TYPE {
    PLUS_STATUS = 1,
    DELETE_DEMERIT_CELL = 2,
    CELL_EFECT_UP = 7,
    DICE_RESULT_UP = 9,
    CHARGE_AT_DICE_VALUE = 10
};

#endif
