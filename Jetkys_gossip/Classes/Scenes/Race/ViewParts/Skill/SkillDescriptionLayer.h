#ifndef __syanago__SkillDescriptionLayer__
#define __syanago__SkillDescriptionLayer__

#include "cocos2d.h"
#include "create_func.h"
#include "SkillButton.h"

USING_NS_CC;

class SkillViewController;

class SkillDescriptionLayer : public Layer, public create_func<SkillDescriptionLayer>
{
public:
    SkillDescriptionLayer();
    
    using create_func::create;
    bool init(const std::vector<SkillButton*>& arrayOfItems);
    
    virtual bool isEnabled() const { return _enabled; }
    virtual void setEnabled(bool value) { _enabled = value; };
    
    virtual void onExit() override;

    SkillViewController* _skillViewController;

private:
    enum class State {
        WAITING,
        TRACKING_TOUCH,
    };
    enum {
        kDefaultPadding =  5,
    };
    virtual bool onTouchBegan(Touch* touch, Event* event);
    virtual void onTouchEnded(Touch* touch, Event* event);
    virtual void onTouchCancelled(Touch* touch, Event* event);
    virtual void onTouchMoved(Touch* touch, Event* event);
    
    SkillButton* getItemForTouch(Touch * touch);

    bool _enabled;

    State _state;
    SkillButton *_selectedItem;
};


#endif /* defined(__syanago__SkillDescriptionLayer__) */
