#ifndef __syanago__SkillDescription__
#define __syanago__SkillDescription__

#include "cocos2d.h"
#include "SkillModel.h"

USING_NS_CC;

class SkillDescription : public Sprite
{
public:
    SkillDescription();
    ~SkillDescription();
    
    static SkillDescription* create(std::vector<std::shared_ptr<const SkillModel>>& skillModels);
    
    void viewDiscription(const int viewskillNumber);
    
private:
    enum TAG_LABEL{
        NAME = 0,
        DISCRIPTION,
    };
    enum Z_ORDER{
        Z_NAME = 0,
        Z_DISCRIPTION,
    };
    std::vector<std::shared_ptr<const SkillModel>> _skillModels;
    
    void initialize(std::vector<std::shared_ptr<const SkillModel>>& skillModels);
    
    void showName();
    void showDescription();
};

#endif