#include "SkillDescriptionLayer.h"
#include "SkillViewController.h"
#include "MenuTouch.h"

using namespace std;

SkillDescriptionLayer::SkillDescriptionLayer():
    _enabled(true)
    , _selectedItem(nullptr)
    , _state(SkillDescriptionLayer::State::WAITING) {};

bool SkillDescriptionLayer::init(const std::vector<SkillButton*>& arrayOfItems)
{
    if (!Layer::init()) {
        return false;
    }
    setAnchorPoint(Vec2(0.5f, 0.5f));
    setPosition(Point(0.0, 0.0));
    setContentSize(Director::getInstance()->getWinSize());
    setCascadeColorEnabled(true);
    setCascadeOpacityEnabled(true);

    for (int i = 0; i < 5; i++) {
        {
            if (arrayOfItems.at(i) != nullptr)
                addChild(arrayOfItems.at(i), 4 - i);
        }
    }

    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->setSwallowTouches(true);
    touchListener->onTouchBegan = CC_CALLBACK_2(SkillDescriptionLayer::onTouchBegan, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(SkillDescriptionLayer::onTouchMoved, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(SkillDescriptionLayer::onTouchEnded, this);
    touchListener->onTouchCancelled = CC_CALLBACK_2(SkillDescriptionLayer::onTouchCancelled, this);

    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

    return true;
}

bool SkillDescriptionLayer::onTouchBegan(Touch* touch, Event* event)
{
    if (_state != SkillDescriptionLayer::State::WAITING ||
        !_visible ||
        !_enabled) {
        return false;
    }

    for (Node* c = _parent; c != nullptr; c = c->getParent()) {
        if (c->isVisible() == false) {
            return false;
        }
    }

    _selectedItem = getItemForTouch(touch);
    if (_selectedItem) {
        _selectedItem->setColor(Color3B::GRAY);
        _state = SkillDescriptionLayer::State::TRACKING_TOUCH;
        _skillViewController->showDesAni(_selectedItem->getTag());
        return true;
    }
    return false;
}

void SkillDescriptionLayer::onTouchEnded(Touch* touch, Event* event)
{
    if (_selectedItem) {
        _skillViewController->hidenDesAni();
        _selectedItem->setColor(Color3B::WHITE);
        _selectedItem->onTap();
    }
    _state = SkillDescriptionLayer::State::WAITING;
}

void SkillDescriptionLayer::onTouchCancelled(Touch* touch, Event* event)
{
    if (_selectedItem) {
        _selectedItem->setColor(Color3B::WHITE);
        _skillViewController->hidenDesAni();
    }
    _state = SkillDescriptionLayer::State::WAITING;
}

void SkillDescriptionLayer::onTouchMoved(Touch* touch, Event* event)
{
    SkillButton* currentItem = getItemForTouch(touch);
    if (currentItem != _selectedItem) {
        if (_selectedItem) {
            _skillViewController->hidenDesFromMenu();
        }
        _selectedItem = currentItem;
        if (_selectedItem) {
            _skillViewController->showDesFromMenu(_selectedItem->getTag());
        }
    }
}

void SkillDescriptionLayer::onExit()
{
    if (_state == SkillDescriptionLayer::State::TRACKING_TOUCH) {
        if (_selectedItem) {
            _skillViewController->hidenDesFromMenu();
            _selectedItem = nullptr;
        }
        _state = SkillDescriptionLayer::State::WAITING;
    }
    Layer::onExit();
}

SkillButton* SkillDescriptionLayer::getItemForTouch(Touch* touch)
{
    Vec2 touchLocation = touch->getLocation();

    if (!_children.empty()) {
        for (auto iter = _children.crbegin(); iter != _children.crend(); ++iter) {
            SkillButton* child = dynamic_cast<SkillButton*>(*iter);
            if (child && child->isVisible() && child->getEnabled()) {
                Vec2 local = child->convertToNodeSpace(touchLocation);
                Rect r = child->rect();
                r.origin = Vec2::ZERO;

                if (r.containsPoint(local)) {
                    return child;
                }
            }
        }
    }
    return nullptr;
}
