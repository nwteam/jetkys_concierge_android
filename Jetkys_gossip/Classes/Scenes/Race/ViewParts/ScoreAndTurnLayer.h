#ifndef __syanago__ScoreAndTurnLayer__
#define __syanago__ScoreAndTurnLayer__

#include "cocos2d.h"
#include "create_func.h"
#include "StatusManager.h"

USING_NS_CC;
using namespace RaceStatus;

enum GOAL_TARGET_TYPE{
    TARGET_NONE = 0,
    TARGET_SCORE,
    TARGET_TURN,
};

class ScoreAndTurnLayer : public Layer, public create_func<ScoreAndTurnLayer>
{
public:
    using create_func::create;
    bool init(StatusManager* status, const GOAL_TARGET_TYPE targetType);
    ScoreAndTurnLayer();
    ~ScoreAndTurnLayer();
    
    void refresh();
private:
    enum TAG_SPRITE{
        SCORE_IMAGE = 0,
        SCORE_LABEL,
        TURN_IMAGE,
        TURN_LABEL,
        TARGET_LABEL,
    };
    enum Z_ORDER{
        Z_SCORE_IMAGE = 0,
        Z_SCORE_LABEL,
        Z_TURN_IMAGE,
        Z_TURN_LABEL,
        Z_TARGET_LABEL,
    };
    StatusManager* _status;
    GOAL_TARGET_TYPE _targetType;
    
    void showScore();
    void showTurn();
    void showGoalTarget(const GOAL_TARGET_TYPE targetType);
    
    void setBonusScore(const int score);
    void setTurn(const int turn);
    void setTagetLabel(const std::string targetContent);
    
};

#endif