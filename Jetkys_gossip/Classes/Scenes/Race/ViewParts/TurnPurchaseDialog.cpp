#include "TurnPurchaseDialog.h"
#include "FontDefines.h"
#include "SoundHelper.h"
#include "PlayerController.h"
#include "SystemSettingModel.h"
#include "MeasurementInformation.h"
#include "ArtLtvMeasurement.h"

TurnPurchaseDialog::TurnPurchaseDialog()
    : progress(nullptr)
    , api(nullptr)
    , _buttonPushed(false)
    , _listener(nullptr)
{
    progress = new DefaultProgress();
    api = new RequestAPI(progress);
}

TurnPurchaseDialog::~TurnPurchaseDialog()
{
    delete progress;
    delete api;
}

bool TurnPurchaseDialog::init(const OnSuccessCallback& onSuccessCallback, const TransitLoseSceneCallback& transitLoseSceneCallback)
{
    if (!Layer::init()) {
        return false;
    }

    _onSuccessCallback = onSuccessCallback;
    _transitLoseSceneCallback = transitLoseSceneCallback;

    Size winSize = Director::getInstance()->getWinSize();

    setVisible(true);

    LayerColor* black = LayerColor::create(Color4B(Color3B::BLACK));
    black->setOpacity(180);
    addChild(black);

    Sprite* bg = Sprite::create("dialog_base.png");
    bg->setTag(TAG_SPRITE::BACKGROUND);
    bg->setPosition(Point(winSize.width / 2, winSize.height / 2));
    addChild(bg);

    showText(bg);
    showPurchaseButton(bg);
    showCloseButton(bg);

    return true;
}

bool TurnPurchaseDialog::hasToken()
{
    return PLAYERCONTROLLER->_player->getFreeToken() + PLAYERCONTROLLER->_player->getPayToken() > 0;
}

void TurnPurchaseDialog::showText(Sprite* bg)
{
    Label* title = Label::createWithTTF("ターンオーバー", FONT_NAME_2, 34);
    title->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    title->setPosition(Vec2(bg->getContentSize().width / 2, bg->getContentSize().height - 90));
    bg->addChild(title);

    Label* lead = Label::createWithTTF(StringUtils::format("1枚でターン数を%d回復できます", SystemSettingModel::getModel()->getTurnPerToken()), FONT_NAME_2, 26);
    lead->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    lead->setPosition(Vec2(bg->getContentSize().width / 2, bg->getContentSize().height - 150));
    bg->addChild(lead);

    Label* tokenLabel = Label::createWithTTF(StringUtils::format("現在のトークン：%d", PLAYERCONTROLLER->_player->getFreeToken() + PLAYERCONTROLLER->_player->getPayToken()), FONT_NAME_2, 26);
    tokenLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    tokenLabel->setPosition(Vec2(bg->getContentSize().width / 2, bg->getContentSize().height - 190));
    bg->addChild(tokenLabel);

    Sprite* token = Sprite::create("token.png");
    token->cocos2d::Node::setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    token->setPosition(tokenLabel->getPosition() - Vec2(tokenLabel->getContentSize().width / 2 + 15, -10));
    bg->addChild(token);
}

void TurnPurchaseDialog::showPurchaseButton(Sprite* bg)
{
    ui::Button* purchaseButton = ui::Button::create("gacha_dialog_button.png");
    purchaseButton->setTouchEnabled(false);
    purchaseButton->addTouchEventListener(CC_CALLBACK_2(TurnPurchaseDialog::onTouchPurchaseButton, this));
    purchaseButton->setTag(TAG_SPRITE::BUTTON_PURCHASE);
    purchaseButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    purchaseButton->setPosition(Vec2(bg->getContentSize().width / 2 - 100, 28));

    Sprite* token = Sprite::create("token.png");
    token->cocos2d::Node::setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    token->setPosition(Vec2(10, 15));
    purchaseButton->addChild(token);

    Label* purchaseLabel = Label::createWithTTF("を使う", FONT_NAME_2, 26);
    purchaseLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    purchaseLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    purchaseLabel->setPosition(Vec2(purchaseButton->getContentSize() / 2) + Vec2(15, -14));
    purchaseButton->addChild(purchaseLabel);
    bg->addChild(purchaseButton);
}

void TurnPurchaseDialog::showCloseButton(Sprite* bg)
{
    ui::Button* closeButton = ui::Button::create("gacha_dialog_button.png");
    closeButton->setTouchEnabled(false);
    closeButton->addTouchEventListener(CC_CALLBACK_2(TurnPurchaseDialog::onTouchCloseButton, this));
    closeButton->setTag(TAG_SPRITE::BUTTON_CLOSE);
    closeButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    closeButton->setPosition(Vec2(bg->getContentSize().width / 2 + 100, 28));

    Label* closeLabel = Label::createWithTTF("諦める", FONT_NAME_2, 26);
    closeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    closeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    closeLabel->setPosition(Vec2(closeButton->getContentSize() / 2) - Vec2(0, 14));
    closeButton->addChild(closeLabel);
    bg->addChild(closeButton);
}

void TurnPurchaseDialog::onTouchPurchaseButton(cocos2d::Ref* sender, ui::Widget::TouchEventType type)
{
    if (!_buttonPushed && type == ui::Widget::TouchEventType::ENDED) {
        disableButtons();
        progress->onStart();
        api->buyTurns(CC_CALLBACK_1(TurnPurchaseDialog::onResponseBuyTurns, this));
    }
}

void TurnPurchaseDialog::onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (!_buttonPushed && type == ui::Widget::TouchEventType::ENDED) {
        disableButtons();
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        getEventDispatcher()->removeEventListener(_listener);
        _listener = nullptr;
        _visible = false;
        _transitLoseSceneCallback();
    }
}

void TurnPurchaseDialog::onResponseBuyTurns(Json* json)
{
    Json* data = Json_getItem(Json_getItem(Json_getItem(json, "buy_turns"), "data"), "players");
    PLAYERCONTROLLER->_player->setFreeToken(atoi(Json_getString(data, "free_token", "")));
    PLAYERCONTROLLER->_player->setPayToken(atoi(Json_getString(data, "pay_token", "")));
    getEventDispatcher()->removeEventListener(_listener);
    _listener = nullptr;
    _visible = false;
    ArtLtvMeasurement::send(MEASURMENT_INFO::ART::BOUGHT_RACE_TURN, (int)PLAYERCONTROLLER->_player->getID());
    _onSuccessCallback(atoi(Json_getString(data, "turns", "")));
}

void TurnPurchaseDialog::disableButtons()
{
    _buttonPushed = true;
    Node* background = getChildByTag(TAG_SPRITE::BACKGROUND);
    static_cast<ui::Button*>(background->getChildByTag(TAG_SPRITE::BUTTON_CLOSE))->setTouchEnabled(false);
    static_cast<ui::Button*>(background->getChildByTag(TAG_SPRITE::BUTTON_PURCHASE))->setTouchEnabled(false);
}

void TurnPurchaseDialog::show()
{
    if (TurnPurchaseDialog::hasToken() == false) {
        _transitLoseSceneCallback();
    }
    _buttonPushed = false;
    Node* background = getChildByTag(TAG_SPRITE::BACKGROUND);
    static_cast<ui::Button*>(background->getChildByTag(TAG_SPRITE::BUTTON_CLOSE))->setTouchEnabled(true);
    static_cast<ui::Button*>(background->getChildByTag(TAG_SPRITE::BUTTON_PURCHASE))->setTouchEnabled(true);
    _visible = true;
    _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);
}
