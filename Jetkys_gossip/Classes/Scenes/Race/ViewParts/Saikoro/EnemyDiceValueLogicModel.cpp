#include "EnemyDiceValueLogicModel.h"
#include "CellType.h"
#include "RaceCellRogicModel.h"

using namespace CHARACTER_DATA;

bool EnemyDiceValueLogicModel::init(const int playerCellPlace, std::shared_ptr<const CourseModel>courseModel, CellViewController* cellViewData)
{
    if (!Layer::init()) {
        return false;
    }

    _playerCellPlace = playerCellPlace;
    _courseModel = courseModel;
    _cellData = cellViewData;
    return true;
}

int EnemyDiceValueLogicModel::getEnemyDiceValue(std::shared_ptr<const CharacterModel>characterModel, CharacterView* characterViewData)
{
    _characterModel = characterModel;
    if (isSetUpEnemyDice(characterViewData)) {
        int diceValue = getDiceValuePreprocessingMode(characterViewData->getCharacterColumn());
        if (diceValue < 0) {
            diceValue = getRandomDiceValue();
        }
        return checkMinMax(diceValue);
    }
    return checkMinMax(arc4random() % 6);
}

bool EnemyDiceValueLogicModel::isSetUpEnemyDice(CharacterView* characterViewData)
{
    if (_courseModel == NULL || _characterModel == NULL) {
        return false;
    }

    int enemyDiceValue[9];
    enemyDiceValue[0] = checkMinMax(_characterModel->getDiceValue1() + _courseModel->getEnemyDiceCorrection());
    enemyDiceValue[1] = checkMinMax(_characterModel->getDiceValue2() + _courseModel->getEnemyDiceCorrection());
    enemyDiceValue[2] = checkMinMax(_characterModel->getDiceValue3() + _courseModel->getEnemyDiceCorrection());
    enemyDiceValue[3] = checkMinMax(_characterModel->getDiceValue4() + _courseModel->getEnemyDiceCorrection());
    enemyDiceValue[4] = checkMinMax(_characterModel->getDiceValue5() + _courseModel->getEnemyDiceCorrection());
    enemyDiceValue[5] = checkMinMax(_characterModel->getDiceValue6() + _courseModel->getEnemyDiceCorrection());

    enemyDiceValue[6] = _courseModel->getEnemyPlusDice1();
    enemyDiceValue[7] = _courseModel->getEnemyPlusDice2();
    enemyDiceValue[8] = _courseModel->getEnemyPlusDice3();

    std::sort(enemyDiceValue, enemyDiceValue + 9);
    _diceValue.clear();
    for (int i = 0; i < 9; i++) {
        if (enemyDiceValue[i] == 0) {
            continue;
        }
        _diceValue.push_back(enemyDiceValue[i]);
    }
    _characterData[LEVEL] = getEnemyLevel(_characterModel->getID());
    _characterData[POWOER] = characterViewData->getPower();
    _characterData[TECHNIQUE] = characterViewData->getTechnique();
    _characterData[BRAKE] = characterViewData->getBrake();

    return true;
}

int EnemyDiceValueLogicModel::getEnemyLevel(int msterCharacterId)
{
    if (_courseModel == NULL) {
        return 1;
    }
    if (_courseModel->getCourseType() == 2) {
        if (msterCharacterId == _courseModel->getBossCharacterId()) {
            return _courseModel->getBossCharacterLevel();
        } else if (msterCharacterId == _courseModel->getSub1CharacterId())   {
            return _courseModel->getSub1CharacterLevel();
        } else if (msterCharacterId == _courseModel->getSub2CharacterId())   {
            return _courseModel->getSub2CharacterLevel();
        }
    }
    return _courseModel->getEnemyCharacterLevel();
}

int EnemyDiceValueLogicModel::checkMinMax(int number)
{
    number = std::min(6, number);
    number = std::max(1, number);
    return std::max(1, number);
}

int EnemyDiceValueLogicModel::getDiceValuePreprocessingMode(const int enemyCellPlace)
{
    int result = -1;
    const int deltaCellPlace = enemyCellPlace - _playerCellPlace;
    if (deltaCellPlace > 0) {
        if (isPreprocessingEffectTriggered(5, _characterData[LEVEL])) {
            return getMaxForwardCellSelectLogic(enemyCellPlace);
        }
    } else if (-2 >= deltaCellPlace && deltaCellPlace <= -4)   {
        if (isPreprocessingEffectTriggered(12, _characterData[LEVEL])) {
            return getMaxForwardCellSelectLogic(enemyCellPlace);
        }
    } else if (deltaCellPlace <= -5)   {
        if (isPreprocessingEffectTriggered(35, _characterData[LEVEL])) {
            return getMaxForwardCellSelectLogic(enemyCellPlace);
        }
    }
    return result;
}

bool EnemyDiceValueLogicModel::isPreprocessingEffectTriggered(const int correctionValueOfTriggerd, const int level)
{
    if (_courseModel == NULL) {
        return false;
    }
    if ((arc4random() % 10000) <= ((correctionValueOfTriggerd + _courseModel->getEnemyDiceProbability()) * 100)) {
        return true;
    } else  {
        return false;
    }
}

int EnemyDiceValueLogicModel::getMaxForwardCellSelectLogic(const int cellPlace)
{
    int result = -1;
    int maxSpeedCellCount = _diceValue.back();
    const int operandId = _courseModel->getOperandId();
    for (int i = 0, diceValue = 0; i < _diceValue.size(); i++) {
        if (diceValue == _diceValue.at(i)) {
            continue;
        }
        diceValue = _diceValue.at(i);
        int cellType = _cellData->_cell[2][cellPlace + diceValue]->getCellType();
        switch (cellType) {
        case FORWARD_CELL_1:
        case FORWARD_CELL_2:
        case FORWARD_CELL_3:
        case FORWARD_CELL_4:
        case FORWARD_CELL_5:
        case FORWARD_CELL_6:
        case FORWARD_CELL_7: {
            int cellCount = RaceCellRogicModel::cellRogicFoword(_characterData[POWOER], cellType, operandId) + diceValue;
            if (cellCount >= maxSpeedCellCount) {
                result = diceValue;
                maxSpeedCellCount = cellCount;
            }
            break;
        }
        case PREFERENCE_CELL_1: {
            int cellCount = (maxSpeedCellCount * 2) + diceValue;
            if (cellCount >= maxSpeedCellCount) {
                result = diceValue;
                maxSpeedCellCount = cellCount;
            }
            break;
        }
        case DOWNWARD_SLOPE_CELL_1:
        case DOWNWARD_SLOPE_CELL_2:
        case DOWNWARD_SLOPE_CELL_3:
        case DOWNWARD_SLOPE_CELL_4:
        case DOWNWARD_SLOPE_CELL_5: {
            int cellCount = RaceCellRogicModel::cellRogicDownSlope(maxSpeedCellCount, _characterData[BRAKE], cellType, operandId) + diceValue;
            if (cellCount >= maxSpeedCellCount) {
                result = diceValue;
                maxSpeedCellCount = cellCount;
            }
            break;
        }
        default:
            break;
        }
    }
    return result;
}

int EnemyDiceValueLogicModel::getRandomDiceValue()
{
    return _diceValue.at(arc4random() % _diceValue.size());
}