#ifndef __syanago__EnemyDiceValueLogicModel__
#define __syanago__EnemyDiceValueLogicModel__

#include "cocos2d.h"
#include "CellViewController.h"
#include "CharacterView.h"
#include <map>
#include "create_func.h"
#include "CharacterModel.h"
#include "CourseModel.h"

USING_NS_CC;

namespace CHARACTER_DATA{
    static const std::string LEVEL = "level";
    static const std::string POWOER = "powoer";
    static const std::string TECHNIQUE = "technique";
    static const std::string BRAKE = "brake";
}

class EnemyDiceValueLogicModel:  public Layer, create_func<EnemyDiceValueLogicModel>
{
public:
    bool init(const int playerCellPlace, std::shared_ptr<const CourseModel> courseModel, CellViewController* cellViewData);
    using create_func::create;
    
    int getEnemyDiceValue(std::shared_ptr<const CharacterModel> characterModel,  CharacterView* characterViewData);
    
private:
    int checkMinMax(int diceValue);
    
    bool isSetUpEnemyDice(CharacterView* characterViewData);
    int getEnemyLevel(int msterCharacterId);
    
    int getDiceValuePreprocessingMode(const int enemyCellPlace);
    bool isPreprocessingEffectTriggered(const int correctionValueOfTriggerd, const int level);
    int getMaxForwardCellSelectLogic(const int cellPlace);

    int getRandomDiceValue();
    
    std::map<std::string, int> _characterData;
    std::vector<int> _diceValue;
    CellViewController* _cellData;
    int _playerCellPlace;
    std::shared_ptr<const CourseModel> _courseModel;
    std::shared_ptr<const CharacterModel> _characterModel;
};

#endif /* defined(__syanago__EnemyDiceValueLogicModel__) */
