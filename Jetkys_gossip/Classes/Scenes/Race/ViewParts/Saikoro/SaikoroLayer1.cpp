#include "RaceScene.h"
#include "PlayerController.h"
#include "SaikoroLayer1.h"
#include "jCommon.h"

#define kSAIKORO_NUMBER 6
#define kSAIKORO_CELL 164.0f
#define kSAIKORO_MOVE_STEP 41.0f
#define kSAIKORO_SCROLL 20.5f
#define kDELTA_TIME_MIN 0.05f
#define kDELTA_TIME_MAX 0.4f
#define kDELTA_TIME_INS 0.03f
#define kSAIKORO_MAXTIME 1.0f

SaikoroLayer1::~SaikoroLayer1()
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("dice_set");
    delete saikoro;
    delete saikoroValue;
}

SaikoroLayer1::SaikoroLayer1():
    _masterCharacterId(0), ss(ST_STOP), container1(nullptr), container2(nullptr), isTouch(false), _saikorocount(0), _saikoroArrayCount(0), _increaseRateSpeed(1), _stop(true)
{
    winSize = Director::getInstance()->getWinSize();
}

bool SaikoroLayer1::init(int masterCharacterId)
{
    if (!Layer::init()) {
        return false;
    }
    _masterCharacterId = masterCharacterId;

    std::vector<int>* saikoroList = new std::vector<int>(9);
    saikoroList->clear();
    PlayerCharactersModel* characterModel;
    if (UserDefault::getInstance()->getBoolForKey("Tutorial", false)) {
        characterModel = PLAYERCONTROLLER->_playerCharacterModels.begin()->second;
    } else {
        int lederId = PLAYERCONTROLLER->_playerTeams->getLearderCharacterId();
        characterModel = PLAYERCONTROLLER->_playerCharacterModels[lederId];
    }
    for (int i = 0; i < 9; i++) {
        if (characterModel->getDiceValue(i) > 0) {
            saikoroList->push_back(characterModel->getDiceValue(i));
            _saikoroArrayCount++;
        }
    }

    saikoro = new std::vector<int>(_saikoroArrayCount);
    saikoro->clear();
    for (int i = 0; i < _saikoroArrayCount; i++) {
        saikoro->push_back(saikoroList->at(i));
    }

    delete saikoroList;

    for (int i = 0; i < _saikoroArrayCount; i++) {
        int saikoroCount = static_cast<int>(saikoro->size());
        if (i < saikoroCount) {
            saikoroDice[i] = saikoro->at(i);
        }
        if (_saikoroArrayCount != saikoroCount) {
            _saikoroArrayCount = saikoroCount;
        }
    }

    showBackGround();
    showOnTapSaikoroButtonEffect();

    saikoroValue = new std::vector<int>(_saikoroArrayCount * 2);

    EventListenerTouchOneByOne* _touchListener = EventListenerTouchOneByOne::create();
    _touchListener->setSwallowTouches(true);
    _touchListener->onTouchBegan = CC_CALLBACK_2(SaikoroLayer1::onTouchBegan, this);
    _touchListener->onTouchMoved = [](Touch* touch, Event* unused_event) {};
    _touchListener->onTouchEnded = CC_CALLBACK_2(SaikoroLayer1::onTouchEnded, this);
    _touchListener->onTouchCancelled = [](Touch* touch, Event* unused_event) {};
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_touchListener, this);

    return true;
}

void SaikoroLayer1::showBackGround()
{
    _bgr = makeSprite("saikoro_bg.png");
    _bgr->setPosition(Point(winSize.width / 2, winSize.height / 2));
    _bgr->setColor(Color3B::BLUE);
}

void SaikoroLayer1::showOnTapSaikoroButtonEffect()
{
    _topSaikoroTarget = makeSprite("target_top.png");
    _topSaikoroTarget->setPosition(Point(winSize.width / 2,  winSize.height / 2 + 130));
    _topSaikoroTarget->setVisible(false);
    addChild(_topSaikoroTarget, 1000);

    _underSaikoroTarget = makeSprite("target_under.png");
    _underSaikoroTarget->setPosition(Point(winSize.width / 2,  winSize.height / 2 - 130));
    _underSaikoroTarget->setVisible(false);
    addChild(_underSaikoroTarget, 1000);

    _tapToStop = makeSprite("tap_to_stop.png");
    _tapToStop->setPosition(Point(winSize.width / 2,  winSize.height / 2 + 220));
    _tapToStop->setVisible(false);
    addChild(_tapToStop, 1000);
}

void SaikoroLayer1::shootDice()
{
    SOUND_HELPER->playEffect(DICE_TAP, false);

    _raceScene->turnStart();
    _raceScene->statusManager->view->diceRolling = true;
    _raceScene->statusManager->view->pause = true;
    _vec0 = 50.0 * _increaseRateSpeed;
    _indexRS = -1;
    timeRun = 0;
    _nodeResult = NULL;
    _spriteResult = NULL;
    _isTap = false;
    _stop = false;

    setVisible(true);
    setPosition(Vec2::ZERO);

    changeIndexSaikoro(saikoroDice, _saikoroArrayCount);
    showDiceValues();
    animateTapToStop();
    scheduleUpdate();
}

void SaikoroLayer1::changeIndexSaikoro(int* arr, int n)
{
    int bufArr[_saikoroArrayCount];
    int indexChange = arc4random() % _saikoroArrayCount;
    for (int i = 0; i < _saikoroArrayCount; i++) {
        if (i + indexChange < _saikoroArrayCount) {
            bufArr[ i] = arr[i + indexChange];
        } else {
            bufArr[i] = arr[i + indexChange - _saikoroArrayCount];
        }
    }

    for (int i = 0; i < _saikoroArrayCount; i++) {
        saikoroDiceChange[i] = bufArr[i];
    }

    if (UserDefault::getInstance()->getBoolForKey("Tutorial", false)) {
        for (int i = 0; i < 6; i++) {
            saikoroDiceChange[i] = i + 1;
        }
    }
}

void SaikoroLayer1::showDiceValues()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("dice_set.plist");
    for (int i = 0; i < 2; i++) {
        Node* node = Node::create();
        switch (i) {
        case 0:
            if (container1) {
                container1->removeFromParent();
            }
            node->setPosition(Vec2(-kSAIKORO_CELL, winSize.height / 2));
            container1 = node;
            addChild(container1, 3);
            break;
        case 1:
            if (container2) {
                container2->removeFromParent();
            }
            node->setPosition(Vec2(container1->getPositionX() + _saikoroArrayCount * kSAIKORO_CELL, container1->getPositionY()));
            container2 = node;
            addChild(container2, 2);
        }
        for (int j = 0; j < _saikoroArrayCount; j++) {
            int value = saikoroDiceChange[j];
            if (value > 0) {
                const int index = j + _saikoroArrayCount * i;
                auto sprite = makeSprite(StringUtils::format("dice_%d.png", value).c_str());
                sprite->setPosition(Vec2(kSAIKORO_CELL * (j + 1), 0));
                sprite->setTag(index);
                saikoroValue->at(index) = value;
                node->addChild(sprite, 2);
            }
        }
    }
}

void SaikoroLayer1::animateTapToStop()
{
    _topSaikoroTarget->setScale(1);
    _topSaikoroTarget->setVisible(true);
    _underSaikoroTarget->setScale(1);
    _underSaikoroTarget->setVisible(true);
    _tapToStop->setScale(1);
    _tapToStop->setVisible(true);
    auto topMoveUp1 = MoveBy::create(0.25, Point(0, 30));
    auto topMoveUp2 = MoveBy::create(0.25, Point(0, 30));
    auto topmMoveDown1 = MoveBy::create(0.5, Point(0, -60));
    _topSaikoroTarget->runAction(RepeatForever::create(Sequence::create(topMoveUp1, topmMoveDown1, topMoveUp2, NULL)));

    auto underMoveUp1 = MoveBy::create(0.25, Point(0, -30));
    auto underMoveUp2 = MoveBy::create(0.25, Point(0, -30));
    auto underMoveDown1 = MoveBy::create(0.5, Point(0, 60));
    _underSaikoroTarget->runAction(RepeatForever::create(Sequence::create(underMoveUp1, underMoveDown1, underMoveUp2, NULL)));
    _tapToStop->runAction(MoveTo::create(0.1, Vec2(winSize.width / 2, winSize.height / 2 + 220)));
}

void SaikoroLayer1::update(float dt)
{
    timeRun += dt;
    container1->setPosition(Point(container1->getPositionX() - _vec0, container1->getPositionY()));
    container2->setPosition(Point(container2->getPositionX() - _vec0, container1->getPositionY()));
    if (container1->getPositionX() < -(_saikoroArrayCount + 1) * kSAIKORO_CELL) {
        container1->setPosition(Point(container1->getPositionX() + (2 * _saikoroArrayCount * kSAIKORO_CELL), container1->getPositionY()));
    }
    if (container2->getPositionX() < -(_saikoroArrayCount + 1) * kSAIKORO_CELL) {
        container2->setPosition(Point(container2->getPositionX() + (2 * _saikoroArrayCount * kSAIKORO_CELL), container2->getPositionY()));
    }

    if (timeRun >= 2.0 && !_isTap) {
        _isTap = true;
        diceStoped();
    }

    if (_stop) {
        if (_spriteResult->getPositionX() + _nodeResult->getPositionX() + getPositionX() > winSize.width / 2 &&
            _spriteResult->getPositionX() + _nodeResult->getPositionX() + getPositionX() <= winSize.width / 2 + _vec0 + 2.1) {
            unscheduleUpdate();

            SOUND_HELPER->playEffect(DICE_STOP, false);
            SOUND_HELPER->playCharacterVoiceEfect(_masterCharacterId, 17);

            setPosition(Point(winSize.width / 2 - _nodeResult->getPositionX() - _spriteResult->getPositionX(), _spriteResult->getPositionY()));

            _topSaikoroTarget->stopAllActions();

            auto seq2 = Sequence::create(ScaleTo::create(0.3, 1.5), NULL);
            _topSaikoroTarget->runAction(seq2);
            _topSaikoroTarget->runAction(MoveTo::create(0.3, Point(winSize.width / 2, winSize.height / 2 + 150)));

            _underSaikoroTarget->stopAllActions();
            auto seq3 = Sequence::create(ScaleTo::create(0.3, 1.5), NULL);
            _underSaikoroTarget->runAction(seq3);
            _underSaikoroTarget->runAction(MoveTo::create(0.3, Point(winSize.width / 2, winSize.height / 2 - 150)));

            _tapToStop->stopAllActions();
            auto seq4 = Sequence::create(ScaleTo::create(0.3, 1.5), NULL);
            _tapToStop->runAction(seq4);
            _tapToStop->runAction(MoveTo::create(0.3, Point(winSize.width / 2, winSize.height / 2 + 220)));

            _spriteResult->runAction(Sequence::create(ScaleTo::create(0.3, 1.5), CallFunc::create(CC_CALLBACK_0(SaikoroLayer1::diceEnded, this)), NULL));
        }
    }
}



bool SaikoroLayer1::onTouchBegan(Touch* touch, Event* unused_event)
{
    Point touchPoint = touch->getLocation();

    if (_raceScene->statusManager->view->diceRolling == true && !_stop) {
        diceStoped();
    }
    return false;
}


void SaikoroLayer1::onTouchEnded(Touch* touch, Event* unused_event)
{
    if (!isVisible()) {
        return;
    }
}


void SaikoroLayer1::diceStoped()
{
    if (UserDefault::getInstance()->getBoolForKey("Tutorial", false)) {
        int j = 2;
        container1->setLocalZOrder(2);
        container2->setLocalZOrder(1);
        _nodeResult = container1;
        _spriteResult = (Sprite*) container1->getChildByTag(j);
        _spriteResult->setLocalZOrder(10);
        _isTap = true;
        _indexRS = j;
        _stop = true;
        _vec0 = 25.0;
    } else {
        for (int j = 0; j < _saikoroArrayCount * 2; j++) {
            auto node = j < _saikoroArrayCount ? container1 : container2;
            if (node->getChildByTag(j)->getPositionX() + node->getPositionX() + getPositionX() - kSAIKORO_CELL / 4 > winSize.width / 2 &&
                node->getChildByTag(j)->getPositionX() + node->getPositionX() + getPositionX() - node->getChildByTag(j)->getContentSize().width - kSAIKORO_CELL / 4 < winSize.width / 2) {
                if (j < _saikoroArrayCount) {
                    container1->setLocalZOrder(2);
                    container2->setLocalZOrder(1);
                    _nodeResult = container1;
                    _spriteResult = (Sprite*) container1->getChildByTag(j);
                } else {
                    container1->setLocalZOrder(1);
                    container2->setLocalZOrder(2);
                    _nodeResult = container2;
                    _spriteResult = (Sprite*) container2->getChildByTag(j);
                }
                _spriteResult->setLocalZOrder(10);
                _isTap = true;
                _indexRS = j;
                _stop = true;
                _vec0 = 25.0;
            }
        }
    }
}


void SaikoroLayer1::diceEnded()
{
    unscheduleAllCallbacks();
    _raceScene->hideCriticalDiceEffect();
    ss = ST_STOP;
    // activate leaderskill by diceValue
    if (_raceScene->statusManager->leaderSkill->chargeAtDiceValueTarget == saikoroValue->at(_indexRS)) {
        if (_raceScene->statusManager->leaderSkill->chargeAtEffectPercent >= arc4random() % 100) {
            _raceScene->_skillBar->chargeSkillWithEffect(1);
        }
    } else if (_raceScene->statusManager->leaderSkill->chargeAtDiceValueTargetFriend == saikoroValue->at(_indexRS)) {
        if (_raceScene->statusManager->leaderSkill->chargeAtEffectPercentFriend >= arc4random() % 100) {
            _raceScene->_skillBar->chargeSkillWithEffect(1);
        }
    }
    if (_raceScene->statusManager->dice->diceValue == 0) {
        _raceScene->statusManager->dice->diceValue = saikoroValue->at(_indexRS);
    }
    _raceScene->statusManager->playerStatus->moveDistance = saikoroValue->at(_indexRS);
    runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(SaikoroLayer1::sleep, this)), NULL));
}

void SaikoroLayer1::sleep()
{
    if (_raceScene->statusManager->view->diceRolling == true) {
        _raceScene->statusManager->view->diceRolling = false;
    }
    setVisible(false);
    _topSaikoroTarget->setVisible(false);
    _underSaikoroTarget->setVisible(false);
    _tapToStop->setVisible(false);
    _raceScene->onEndShootDice();
}



void SaikoroLayer1::setSaikoroRateSpeed()
{
    _increaseRateSpeed = _increaseRateSpeed * 1.04;
}

void SaikoroLayer1::resetSaikoroRateSpeed()
{
    _increaseRateSpeed = 1;
}

