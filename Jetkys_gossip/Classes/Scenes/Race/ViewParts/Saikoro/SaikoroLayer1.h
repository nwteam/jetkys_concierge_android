#ifndef __Syanago__SaikoroLayer1__
#define __Syanago__SaikoroLayer1__

#include "cocos2d.h"
#include "create_func.h"
USING_NS_CC;

class RaceScene;

enum SaikoroState{
    ST_START,
    ST_RUNNING,
    ST_STOP
};

class SaikoroLayer1 : public Layer, create_func<SaikoroLayer1>{
public:
    SaikoroLayer1();
    ~SaikoroLayer1();
    bool init(int masterCharacterId);
    using create_func::create;
    
    void shootDice();
    ;
    void setSaikoroRateSpeed();
    void resetSaikoroRateSpeed();
    
    RaceScene *_raceScene;
    
    
private:
    void showBackGround();
    void showOnTapSaikoroButtonEffect();
    void changeIndexSaikoro(int arr[], int n);
    void showDiceValues();
    
    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
    
    void animateTapToStop();
    
    void update(float dt);
    
    void diceStoped();
    void diceEnded();
    void sleep();
    
    
    cocos2d::Size winSize;
    int _masterCharacterId;
    
    Sprite* _topSaikoroTarget;
    Sprite* _underSaikoroTarget;
    Sprite* _tapToStop;
    
    float timeRun;
    int _saikorocount;
    int _saikoroArrayCount;
    Node *container1;
    Node *container2;
    Node *_nodeResult;
    Sprite * _bgr;
    Sprite * _spriteResult;
    
    std::vector<int> *saikoro;
    std::vector<int> *saikoroValue;
    int saikoroDice[9];
    int saikoroDiceChange[9];

    SaikoroState ss;
    bool isTouch;
    
    float _vec0;
    int _indexRS;
    bool _stop;
    bool _isTap;
    
    float _increaseRateSpeed;
    
};

#endif /* defined(__Syanago__SaikoroLayer1__) */
