#include "ItemButton.h"
#include "SoundHelper.h"
#include "jCommon.h"

ItemButton::ItemButton():
    _useCallBak(nullptr)
    , _notUseCallBak(nullptr)
    , _showDescriptionCallback(nullptr)
    , _notShowDescriptionCallback(nullptr)
    , _status(NOT_USE), _currentItemId(-1)
{}

ItemButton::~ItemButton()
{
    _useCallBak = nullptr;
    _notUseCallBak = nullptr;
    _showDescriptionCallback = nullptr;
    _notShowDescriptionCallback = nullptr;
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("item_set.plist");
}

ItemButton* ItemButton::create(const OnUseItemCallback& useCallBak, const OnNotUseItemCallback& notUseCallBak,
                               const OnShowItemDescriptionCallback& showDescriptionCallback, const OnNotShowItemDescriptionCallback& notShowDescriptionCallback)
{
    ItemButton* btn = new (std::nothrow) ItemButton();
    if (btn && btn->init("itemBgr.png")) {
        btn->initialize(useCallBak, notUseCallBak, showDescriptionCallback, notShowDescriptionCallback);
        btn->setZoomScale(0);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void ItemButton::initialize(const OnUseItemCallback& useCallBak, const OnNotUseItemCallback& notUseCallBak,
                            const OnShowItemDescriptionCallback& showDescriptionCallback, const OnNotShowItemDescriptionCallback& notShowDescriptionCallback)
{
    _useCallBak = useCallBak;
    _notUseCallBak = notUseCallBak;
    _showDescriptionCallback = showDescriptionCallback;
    _notShowDescriptionCallback = notShowDescriptionCallback;

    showItemSprite();

    addTouchEventListener(CC_CALLBACK_2(ItemButton::onTapItemButton, this));
}

void ItemButton::showItemSprite()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("item_set.plist");
    for (int i = 0; i < 5; i++) {
        _itemButtons[i] = makeSprite(StringUtils::format("Item%d.png", i + 1).c_str());
        _itemButtons[i]->setPosition(Point(51, 52));
        _itemButtons[i]->setVisible(false);
        _itemButtons[i]->setTag(TAG_SPRITE::ITEM_IMAGE);
        addChild(_itemButtons[i], Z_ITEM_IMAGE);
    }
}

void ItemButton::onTapItemButton(Ref* sender, Widget::TouchEventType type)
{
    if (type == TouchEventType::BEGAN) {
        _showDescriptionCallback();
    } else if (type == TouchEventType::MOVED) {} else if (type == TouchEventType::ENDED) {
        _notShowDescriptionCallback();
        if (_status == NOT_USE) {
            startUseItemAnimation();
            _useCallBak();
        } else {
            stopUseItemAnimation();
            _notUseCallBak();
        }
        return;
    } else if (type == TouchEventType::CANCELED) {
        _notShowDescriptionCallback();
        return;
    }
}

void ItemButton::startUseItemAnimation()
{
    auto animation = RepeatForever::create(Sequence::create(ScaleTo::create(0.3f, 1.3f), ScaleTo::create(0.3f, 1.0f), NULL));
    animation->setTag(TAG_ANIMATION::USE_ITEM);
    runAction(animation);
    _status = USE;
}

void ItemButton::stopUseItemAnimation()
{
    stopActionByTag(TAG_ANIMATION::USE_ITEM);
    setScale(1.0f);
    _status = NOT_USE;
}

void ItemButton::setItem(const ITEM_TYPE itemId)
{
    if (itemId < 0 || 4 < itemId) {
        CC_ASSERT("[ItemButton::getItem]error: get itemId not fond ");
        return;
    }
    _itemButtons[itemId]->setVisible(true);
    _currentItemId = itemId;
}

void ItemButton::useItem()
{
    if (_currentItemId == -1) {
        CC_ASSERT("[ItemButton::useItem]error:not set item");
        return;
    }
    stopUseItemAnimation();
    _itemButtons[_currentItemId]->setVisible(false);
    _currentItemId = -1;
}