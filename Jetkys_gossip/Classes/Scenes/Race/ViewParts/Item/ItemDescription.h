#ifndef __syanago__ItemDescription__
#define __syanago__ItemDescription__

#include "cocos2d.h"
#include "Item.h"

USING_NS_CC;
using namespace RaceStatus;

class ItemDescription : public Sprite
{
public:
    ItemDescription();
    ~ItemDescription();
    
    static ItemDescription* create(Item* item);
    
    void refresh();
private:
    enum TAG_LABEL{
        NAME =0,
        DESCRIPTION,
    };
    enum Z_ORDER{
        Z_NAME =0,
        Z_DESCRIPTION,
    };
    Item* _item;
    
    void initialize(Item* item);
    void showName();
    void showDescription();
};

#endif