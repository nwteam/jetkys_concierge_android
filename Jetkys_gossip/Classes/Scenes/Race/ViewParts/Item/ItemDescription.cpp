#include "ItemDescription.h"
#include "FontDefines.h"

ItemDescription::ItemDescription():
_item(nullptr)
{}

ItemDescription::~ItemDescription()
{}

ItemDescription* ItemDescription::create(Item* item)
{
    ItemDescription* btn = new (std::nothrow) ItemDescription();
    if (btn && btn->initWithFile("SkillDesBgr.png")) {
        btn->initialize(item);
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void ItemDescription::initialize(Item* item)
{
    _item = item;
    showName();
    showDescription();
}

void ItemDescription::showName()
{
    auto itemNameLabel = Label::createWithTTF("", FONT_NAME_4, 25, Size(400,20));
    itemNameLabel->setTag(TAG_LABEL::NAME);
    itemNameLabel->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    itemNameLabel->setPosition(Point( 10.0, 89.0));
    addChild(itemNameLabel, Z_ORDER::Z_NAME);
}

void ItemDescription::showDescription()
{
    auto itemDescriptionLabel = Label::createWithTTF("", FONT_NAME_4, 20, Size(400,60));
    itemDescriptionLabel->setTag(TAG_LABEL::DESCRIPTION);
    itemDescriptionLabel->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    itemDescriptionLabel->setPosition(Point( 10.0, 62.0));
    addChild(itemDescriptionLabel, Z_ORDER::Z_DESCRIPTION);
}

void ItemDescription::refresh()
{
    std::string itemName;
    std::string itemDescription;
    if (_item->currentItem == ITEM_NO_NITORO) {
        itemName = "ニトロ";
        itemDescription = "出目に+6にする";
    }else if (_item->currentItem == ITEM_NO_PATORANP){
        itemName = "警光灯";
        itemDescription = "出目に+1し、抜いた相手一回休みにし、Uターンマスを無効化する";
    }else if (_item->currentItem == ITEM_NO_TRAFFIC_CONTROL){
        itemName = "交通規制";
        itemDescription = "前5マス以内にいる相手は1回休み";
    }
    auto nameLabel = static_cast<Label*>(getChildByTag(TAG_LABEL::NAME));
    if(nameLabel)
    {
        nameLabel->setString(itemName);
    }
    auto descriptionLabel = static_cast<Label*>(getChildByTag(TAG_LABEL::DESCRIPTION));
    if(descriptionLabel)
    {
        descriptionLabel->setString(itemDescription);
    }
}