#ifndef __syanago__ItemButton__
#define __syanago__ItemButton__

#include "cocos2d.h"
#include "ui/cocosGUI.h"
#include "ItemType.h"

USING_NS_CC;

class ItemButton : public ui::Button
{
public:
    typedef std::function<void()> OnUseItemCallback;
    typedef std::function<void()> OnNotUseItemCallback;
    typedef std::function<void()> OnShowItemDescriptionCallback;
    typedef std::function<void()> OnNotShowItemDescriptionCallback;
    ItemButton();
    ~ItemButton();
    static ItemButton* create(const OnUseItemCallback& useCallBak,
                              const OnNotUseItemCallback& notUseCallBak,
                              const OnShowItemDescriptionCallback& showDescriptionCallback,
                              const OnNotShowItemDescriptionCallback& notItemDescriptionCallback);
    
    void setItem(const ITEM_TYPE itemId);
    void useItem();
private:
    enum TAG_SPRITE{
        ITEM_IMAGE = 0,
    };
    enum Z_ORDER{
        Z_ITEM_IMAGE = 0,
    };
    enum TAG_ANIMATION{
        USE_ITEM = 0,
    };
    enum ITEM_USE_STATUS{
        USE,
        NOT_USE
    };
    OnUseItemCallback _useCallBak;
    OnNotUseItemCallback _notUseCallBak;
    OnShowItemDescriptionCallback _showDescriptionCallback;
    OnNotShowItemDescriptionCallback _notShowDescriptionCallback;
    ITEM_USE_STATUS _status;
    Sprite* _itemButtons[5];
    int _currentItemId;
    
    void initialize(const OnUseItemCallback& useCallBak, const OnNotUseItemCallback& cancelUseCallBak, const OnShowItemDescriptionCallback& showDescriptionCallback, const OnNotShowItemDescriptionCallback& notShowDescriptionCallback);
    void showItemSprite();
    
    void onTapItemButton(Ref *sender, Widget::TouchEventType type);
    void startUseItemAnimation();
    void stopUseItemAnimation();
};

#endif
