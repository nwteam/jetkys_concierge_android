#ifndef syanago_ItemType_h
#define syanago_ItemType_h

/**
 *
 ・ITEM_NO_NITORO
 名前:ニトロ　効果:出目に+6にする
 
 ・ITEM_NO_PATORANP
名前:警光灯 効果:出目に+1し、抜いた相手一回休みにする
 
 
 ・ITEM_NO_TRAFFIC_CONTROL
 名前:交通規制 効果:前5マス以内にいる相手は1回休み
 
 *
 */
enum ITEM_TYPE{
    ITEM_NO_NITORO = 0,
    ITEM_NO_PATORANP = 1,
    ITEM_NO_TRAFFIC_CONTROL = 3,
    ITEM_NO_NON_ITEM = 5
};

#endif
