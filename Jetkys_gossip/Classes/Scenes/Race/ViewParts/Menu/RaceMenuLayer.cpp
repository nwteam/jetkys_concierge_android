#include "RaceMenuLayer.h"
#include "FontDefines.h"
#include "MainScene.h"
#include "ui/CocosGUI.h"
#include "RaceDebugger.h"
#include "SyanagoDebugger.h"

RaceMenuLayer::RaceMenuLayer():
    _listener(nullptr), _retired(false) {};

bool RaceMenuLayer::init(std::shared_ptr<const CourseModel>& courseModel)
{
    if (!Layer::init()) {
        return false;
    }
    setVisible(false);
    setPosition(Point(Director::getInstance()->getWinSize().width / 2, Director::getInstance()->getWinSize().height / 2));

    auto background = Sprite::create("black_image.png");
    background->setOpacity(190);

    auto headerBackground = Sprite::create("poupTitle.png");
    headerBackground->setPosition(Point(headerBackground->getContentSize().width / 2,
                                        background->getContentSize().height - headerBackground->getContentSize().height * 2.0 + 15.0));

    auto headerLabel = Label::createWithTTF("MENU", FONT_NAME_2, 32);
    headerLabel->setPosition(Point(headerLabel->getContentSize().width / 2 + 20.0, headerBackground->getContentSize().height / 2 - 16));
    headerLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    headerBackground->addChild(headerLabel);
    background->addChild(headerBackground);


    auto courseNumberLabel = Label::createWithTTF(StringUtils::format("【コース No.%d】", courseModel->getNo()), FONT_NAME_2, 32);
    courseNumberLabel->setPosition(Point(background->getContentSize().width / 2, background->getContentSize().height / 2 + 220.0));
    courseNumberLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    background->addChild(courseNumberLabel, 2);

    std::string courseName = courseModel->getName() + " " + StringUtils::format("%s", courseModel->getDescription().c_str());
    auto desLb = Label::createWithTTF(courseName,
                                      FONT_NAME_2,
                                      (courseName.size() <= 57) ? 34 : 28);
    desLb->setPosition(courseNumberLabel->getPosition() + Vec2(0, -16 - 10 - 17));
    background->addChild(desLb, 2);

    auto courseClearlabel = Label::createWithTTF("【クリア条件】", FONT_NAME_2, 32);
    courseClearlabel->setPosition(courseNumberLabel->getPosition() + Vec2(0, -16 - 10 - 17) + Vec2(0, -17 - 30 - 16));
    background->addChild(courseClearlabel, 2);

    std::string targetName = courseModel->getRaceTypeName();

    auto targetLabel = Label::createWithTTF(targetName, FONT_NAME_2, 34);
    targetLabel->setPosition(courseClearlabel->getPosition() + Vec2(0, -17 - 10 - 16));
    targetLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    background->addChild(targetLabel, 2);

    addChild(background);

    auto retireButton = ui::Button::create("popupRestart.png");
    retireButton->setPosition(Point(background->getPositionX(), background->getPositionY()));
    retireButton->addTouchEventListener([this](Ref* ref, ui::Widget::TouchEventType type) {
        if (!_retired && type == ui::Widget::TouchEventType::ENDED) {
            _retired = true;
            transitScene(MainScene::createScene());
        }
    });

    auto retireLabel = Label::createWithTTF("リタイア", FONT_NAME_2, 32);
    retireLabel->setPosition(Point(retireButton->getContentSize().width / 2, retireButton->getContentSize().height / 2 - 16));
    retireLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    retireButton->addChild(retireLabel, 2);
    addChild(retireButton);

    auto closeButton = ui::Button::create("popupBack.png");
    closeButton->setPosition(Point(background->getPositionX(), background->getPositionY() - 150));
    closeButton->addTouchEventListener([this](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            hide();
        }
    });
    addChild(closeButton);

    #if COCOS2D_DEBUG > 0
        // for debug
        ui::Button* returnDevelopSceneButton = ui::Button::create("shop_garage_fuel_button.png");
        returnDevelopSceneButton->setTitleFontSize(20);
        returnDevelopSceneButton->setTitleText("back to SyanagoDebugger");
        returnDevelopSceneButton->setPosition(Vec2(background->getPositionX(), background->getPositionY() - 300));
        returnDevelopSceneButton->addTouchEventListener([this](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            static_cast<ui::Button*>(ref)->setTouchEnabled(false);
            transitScene(SyanagoDebugger::createScene());
        }
    });
        addChild(returnDevelopSceneButton);
    #endif

    return true;
}

void RaceMenuLayer::show()
{
    if (!isVisible()) {
        setVisible(true);
        _listener = EventListenerTouchOneByOne::create();
        _listener->setSwallowTouches(true);
        _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                      return true;
                                  };
        _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
        _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
        _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
        getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);
    }
}

void RaceMenuLayer::hide()
{
    if (isVisible()) {
        setVisible(false);
        getEventDispatcher()->removeEventListener(_listener);
    }
}

