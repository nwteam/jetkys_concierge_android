#ifndef __syanago__RaceMenuLayer__
#define __syanago__RaceMenuLayer__

#include "cocos2d.h"
#include "create_func.h"
#include "CourseModel.h"
USING_NS_CC;

class RaceMenuLayer : public Layer, public create_func<RaceMenuLayer>
{
public:
    using create_func::create;
    bool init(std::shared_ptr<const CourseModel>& courseModel);
    RaceMenuLayer();
    
    void show();
    
private:
    void hide();
    
    EventListenerTouchOneByOne* _listener;
    bool _retired;
    
};



#endif /* defined(__syanago__RaceMenuLayer__) */
