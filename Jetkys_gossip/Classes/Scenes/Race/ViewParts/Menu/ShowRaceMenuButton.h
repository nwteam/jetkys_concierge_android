#ifndef __syanago__ShowRaceMenuButton__
#define __syanago__ShowRaceMenuButton__

#include "cocos2d.h"
#include "RaceScene.h"
#include "ui/CocosGUI.h"
#include "RaceMenuLayer.h"

USING_NS_CC;

class ShowRaceMenuButton : public ui::Button {
public:
    static ShowRaceMenuButton* create(RaceScene* raceScene, std::shared_ptr<const CourseModel>& courseModel);
private:
    ShowRaceMenuButton(RaceScene* raceScene,std::shared_ptr<const CourseModel>& courseModel):
    _raceScene(raceScene),
    _courseModel(courseModel){};
    
    void initialize();
    
    void onTapButton(Ref* sender, Widget::TouchEventType type);
    
    RaceScene* _raceScene;
    std::shared_ptr<const CourseModel> _courseModel;
    RaceMenuLayer* _raceMenuLayer;
};

#endif /* defined(__syanago__ShowRaceMenuButton__) */
