#include "ShowRaceMenuButton.h"


ShowRaceMenuButton* ShowRaceMenuButton::create(RaceScene* raceScene, std::shared_ptr<const CourseModel>& courseModel)
{
    ShowRaceMenuButton* btn = new (std::nothrow) ShowRaceMenuButton(raceScene, courseModel);
    if (btn && btn->init("MENUNew.png")) {
        btn->autorelease();
        btn->initialize();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

void ShowRaceMenuButton::initialize()
{
    _raceMenuLayer = RaceMenuLayer::create(_courseModel);
    _raceScene->addChild(_raceMenuLayer, 2000);
    addTouchEventListener(CC_CALLBACK_2(ShowRaceMenuButton::onTapButton, this));
}

void ShowRaceMenuButton::onTapButton(cocos2d::Ref* sender, Widget::TouchEventType type)
{
    if (type == TouchEventType::ENDED) {
        if (UserDefault::getInstance()->getBoolForKey("Tutorial")) {
            return;
        }
        _raceMenuLayer->show();
    }
}