#include "RaceBackground.h"
#include "jCommon.h"
#include "Define.h"

enum {
    SEA_SIDE = 1,
    MOUNTAIN = 2,
    CIRCUIT = 3,
    COUNTRY = 4,
    HEIGHT_WAY = 5,
    CITY = 6
};


bool RaceBackground::init(int imageId)
{
    if (!Layer::init()) {
        return false;
    }
    showCenter(imageId);
    showTop(imageId);
    showBottom(imageId);
    return true;
}

void RaceBackground::showCenter(int imageId)
{
    float delta;
    switch (imageId) {
    case MOUNTAIN:
    case COUNTRY: {
        delta = 35;
        break;
    }
    case CITY: {
        delta = 130;
        break;
    }
    case SEA_SIDE:
    case CIRCUIT:
    case HEIGHT_WAY:
    default: {
        delta = 0;
        break;
    }
    }

    Sprite* background1 = makeSprite(StringUtils::format("%d_center_background.png", imageId).c_str());
    Sprite* background2 = makeSprite(StringUtils::format("%d_center_background.png", imageId).c_str());
    background1->setTag(TAG_SPRITE::CENTER_0);
    background2->setTag(TAG_SPRITE::CENTER_1);
    background1->setPosition(Point(-WIDTH_VIEW_CHANGE, -HEIGHT_VIEW_CHANGE + delta));
    background2->setPosition(Point(WIDTH_VIEW_CHANGE, HEIGHT_VIEW_CHANGE + delta));
    addChild(background1, 2);
    addChild(background2, 2);
}

void RaceBackground::showTop(int imageId)
{
    Sprite* backGround0 = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::CENTER_0));
    Sprite* backGround1 = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::CENTER_1));

    Sprite* topSprite0 = makeSprite(StringUtils::format("%d_top_background.png", imageId).c_str());
    Sprite* topSprite1 = makeSprite(StringUtils::format("%d_top_background.png", imageId).c_str());
    topSprite0->setPosition(Point(backGround0->getPositionX(), backGround0->getPositionY() + 960.0 + 220));
    topSprite1->setPosition(Point(backGround1->getPositionX(), backGround1->getPositionY() + 960.0 + 220));
    addChild(topSprite0, 1);
    addChild(topSprite1, 1);
}

void RaceBackground::showBottom(int imageId)
{
    Sprite* backGround0 = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::CENTER_0));
    Sprite* backGround1 = static_cast<Sprite*>(getChildByTag(TAG_SPRITE::CENTER_1));

    Sprite* bottomSprite0 = makeSprite(StringUtils::format("%d_under_background.png", imageId).c_str());
    Sprite* bottomSprite1 = makeSprite(StringUtils::format("%d_under_background.png", imageId).c_str());
    bottomSprite0->setPosition(Point(backGround0->getPositionX(), backGround0->getPositionY() - 960.0 - 220));
    bottomSprite1->setPosition(Point(backGround1->getPositionX(), backGround1->getPositionY() - 960.0 - 220));
    addChild(bottomSprite0, 1);
    addChild(bottomSprite1, 1);
}











