#include "RaceBackgroundManager.h"
#include "Define.h"

RaceBackgroundManager* RaceBackgroundManager::create(int imageId)
{
    RaceBackgroundManager* ret = new (std::nothrow) RaceBackgroundManager();
    if (ret && ret->init(imageId)) {
        ret->autorelease();
    } else {
        CC_SAFE_DELETE(ret);
    }
    return ret;
}

bool RaceBackgroundManager::init(int imageId)
{
    _bgrArray[0] = RaceBackground::create(imageId);
    _bgrArray[1] = RaceBackground::create(imageId);
    _bgrArray[2] = RaceBackground::create(imageId);
    _bgrArray[3] = nullptr;

    _bgrArray[0]->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    _bgrArray[1]->setPosition(Vec2(2556.0 + _bgrArray[0]->getPositionX(),
                                   _bgrArray[0]->getPositionY() + 1472.243186));
    _bgrArray[2]->setPosition(Vec2(2556.0 + _bgrArray[1]->getPositionX(),
                                   _bgrArray[1]->getPositionY() + 1472.243186));

    addChild(_bgrArray[0], 1);
    addChild(_bgrArray[1], 1);
    addChild(_bgrArray[2], 1);
    return true;
}

void RaceBackgroundManager::checkPositionBgr(bool forward, float positionX, float scale)
{
    if (forward == true &&
        (_bgrArray[0]->getPositionX() + getPositionX()) * scale + positionX
        <= -2556.0 * scale) {

        _bgrArray[0]->setPosition(Point(2556.0 + _bgrArray[2]->getPositionX(),
                                        _bgrArray[2]->getPositionY() + 369 * 4));
        _bgrArray[3] = _bgrArray[0];
        _bgrArray[0] = _bgrArray[1];
        _bgrArray[1] = _bgrArray[2];
        _bgrArray[2] = _bgrArray[3];
    } else if (forward == false &&
               (_bgrArray[2]->getPositionX() + getPositionX()) * scale + positionX
               >= 2556.0 * scale) {
        _bgrArray[2]->setPosition(Point(-2556.0 + _bgrArray[0]->getPositionX(),
                                        _bgrArray[0]->getPositionY() - 369 * 4));
        _bgrArray[3] = _bgrArray[2];
        _bgrArray[2] = _bgrArray[1];
        _bgrArray[1] = _bgrArray[0];
        _bgrArray[0] = _bgrArray[3];
    }
}

MoveBy* RaceBackgroundManager::getMoveBy(float runtime, int distance)
{
    return MoveBy::create(runtime, VELOCITY_CHARACTER * (-distance));
}









