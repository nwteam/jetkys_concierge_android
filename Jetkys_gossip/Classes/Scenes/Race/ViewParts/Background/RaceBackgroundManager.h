#ifndef __syanago__RaceBackgroundManager__
#define __syanago__RaceBackgroundManager__

#include "cocos2d.h"
#include "RaceBackground.h"

USING_NS_CC;

class RaceBackgroundManager : public Node
{
public:
    static RaceBackgroundManager* create(int imageId);
    static MoveBy* getMoveBy(float runtime, int distance);
    bool init(int imageId);
    void checkPositionBgr(bool type, float positionX, float scale);
    void move(int distance);
    
private:
    RaceBackground* _bgrArray[4];
};


#endif /* defined(__syanago__RaceBackgroundManager__) */
