#ifndef __syanago__RaceBackground__
#define __syanago__RaceBackground__

#include "cocos2d.h"
#include "create_func.h"

USING_NS_CC;

class RaceBackground : public Layer, create_func<RaceBackground>
{
public:
    bool init(int backgroundImageId);
    using create_func::create;
private:
    enum TAG_SPRITE {
        CENTER_0 = 0,
        CENTER_1 = 1
    };
    void showTop(int imageId);
    void showCenter(int imageId);
    void showBottom(int imageId);
};

#endif /* defined(__syanago__RaceBackground__) */
