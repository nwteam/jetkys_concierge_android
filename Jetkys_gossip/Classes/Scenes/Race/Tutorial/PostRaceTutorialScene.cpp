#include "PostRaceTutorialScene.h"
#include "PlayerController.h"
#include "MainScene.h"
#include "UserDefaultManager.h"

Scene* PostRaceTutorialScene::createScene()
{
    Scene* result = Scene::create();
    result->addChild(PostRaceTutorialScene::create());
    return result;
}

PostRaceTutorialScene::PostRaceTutorialScene():
    _tapStage(0)
{
    progress = new DefaultProgress();
    api = new RequestAPI(progress);
}

PostRaceTutorialScene::~PostRaceTutorialScene()
{
    delete progress;
    delete api;
}

bool PostRaceTutorialScene::init()
{
    if (!Layer::init()) {
        return false;
    }
    return true;
}

void PostRaceTutorialScene::onEnterTransitionDidFinish()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tuto_tap1.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tuto_tap2.plist");

    showFirstImage();

    _touchEventListener = EventListenerTouchOneByOne::create();
    _touchEventListener->onTouchBegan = [&](Touch*, Event*) -> bool {return true; };
    _touchEventListener->onTouchMoved = [&](Touch*, Event*) {};
    _touchEventListener->onTouchCancelled = [&](Touch*, Event*) {};
    _touchEventListener->onTouchEnded = CC_CALLBACK_2(PostRaceTutorialScene::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchEventListener, this);
}

void PostRaceTutorialScene::showFirstImage()
{
    Size frame = Director::getInstance()->getWinSize();
    auto blackImage = makeSprite("black_image.png");
    blackImage->setPosition(Point(frame.width / 2, frame.height / 2));
    addChild(blackImage, 1110);
    tapImage = makeSprite("tuto_tap1.png");
    tapImage->setPosition(Point(frame.width / 2, frame.height / 2));
    // ipad,iphone4
    if (frame.height == 960) {
        tapImage->setPosition(Point(frame.width / 2, 960 / 2 - 87));
    }
    tapImage->setOpacity(0);
    tapImage->runAction(FadeIn::create(0.1f));
    addChild(tapImage, 1111);
    ft = makeSprite("ftTutorial.png");
    ft->setPosition(Point(frame.width / 2, tapImage->getContentSize().height - frame.height + ft->getContentSize().height / 2));
    tapImage->addChild(ft);
    _tapStage = 1;
}

void PostRaceTutorialScene::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* unused_event)
{
    Size frame = Director::getInstance()->getWinSize();
    if (_tapStage == 29) {
        if (PLAYERCONTROLLER->_player->getGameStep() > 1) {
            // ガチャを引いている場合飛ばす
            //「次はお待ちかねのプレゼントです！！」
            _tapStage++;
        }
    }
    if (_tapStage < 30) {
        createTutoImage();
    } else {
        Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        if (PLAYERCONTROLLER->_player->getGameStep() < 1) {
            progress->onStart();
            api->setGameStep(PLAYERCONTROLLER->_player->getRank(), 10, CC_CALLBACK_1(PostRaceTutorialScene::onResponseSetGameStep, this));
        } else {
            SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap1.plist");
            SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap5.plist");
            UserDefault::getInstance()->setBoolForKey("NewPlayer", true);
            UserDefault::getInstance()->setBoolForKey("Tutorial", false);
            UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::OPEN_GACHA, true);
            Scene* home = MainScene::createScene();
            transitScene(home);
        }
    }
}

void PostRaceTutorialScene::createTutoImage()
{
    switch (_tapStage) {
    case 1:
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tuto_tap1.plist");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tuto_tap2.plist");
        break;
    case 8:
        SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap2.plist");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tuto_tap3.plist");
        break;
    case 17:
        SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap3.plist");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tuto_tap4.plist");
        break;
    case 26:
        SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap4.plist");
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tuto_tap5.plist");
        break;
    default:
        break;
    }
    Size frame = Director::getInstance()->getWinSize();
    Director::getInstance()->getTextureCache()->removeTextureForKey(StringUtils::format("tuto_tap%d.png", _tapStage));
    tapImage->stopAllActions();
    tapImage->removeFromParent();
    tapImage = makeSprite(StringUtils::format("tuto_tap%d.png", _tapStage + 1).c_str());
    tapImage->setPosition(Point(frame.width / 2, frame.height / 2));
    // ipad,iphone4
    if (frame.height == 960) {
        tapImage->setPosition(Point(frame.width / 2, 960 / 2 - 87));
    }
    if (_tapStage == 1 || _tapStage == 2 || _tapStage == 3 || _tapStage == 8 || _tapStage == 9 || _tapStage == 27 || _tapStage == 28 || _tapStage == 29) {
        ft = makeSprite("ftTutorial.png");
        ft->setPosition(Point(frame.width / 2, tapImage->getContentSize().height - frame.height + ft->getContentSize().height / 2));
        tapImage->addChild(ft);
    }
    addChild(tapImage, 1112);
    _tapStage++;
}

void PostRaceTutorialScene::onResponseSetGameStep(Json* json)
{
    std::string result = Json_getString(Json_getItem(json, "log_action"), "result", "false");
    if (result == "true") {
        SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap1.plist");
        SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap2.plist");
        SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap3.plist");
        SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap4.plist");
        SpriteFrameCache::getInstance()->removeSpriteFrameByName("tuto_tap5.plist");
        UserDefault::getInstance()->setBoolForKey("NewPlayer", true);
        UserDefault::getInstance()->setBoolForKey("Tutorial", false);
        UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::OPEN_GACHA, false);
        UserDefault::getInstance()->setBoolForKey("NoHomeVoice", true);
        Scene* home = MainScene::createScene();
        transitScene(home);
    }
}

