#include "RaceTutorial.h"
#include "jCommon.h"
#include "FontDefines.h"

bool RaceTutorail::init()
{
    if (!Layer::init()) {
        return false;
    }
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tuto_tap1.plist");

    _winSize = Director::getInstance()->getWinSize();

    blackBgr = makeSprite("black_image.png");
    blackBgr->setScale(4);
    blackBgr->setOpacity(160);
    blackBgr->setPosition(Point(_winSize.width / 2, _winSize.height / 2));

    return true;
}

void RaceTutorail::ShowRace(int indexG)
{
    _winSize = Director::getInstance()->getWinSize();
    if (indexG == 1) {
        bgrTuto = makeSprite("TutorialBgr1.png");
        bgrTuto->setPosition(Point(_winSize.width / 2, _winSize.height / 2));
        // ipad,iphone4
        if (_winSize.height == 960) {
            bgrTuto->setPosition(Point(_winSize.width / 2, 960 / 2 - 10));
        }
        addChild(bgrTuto, 3);
        dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
        dialogNextPoint->setPosition(Point(bgrTuto->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                           dialogNextPoint->getContentSize().height / 2 + 10));
        bgrTuto->addChild(dialogNextPoint);

        titleShow1 = Label::createWithTTF("まずは画面の見方に", FONT_NAME_2, 25);
        titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        bgrTuto->addChild(titleShow1);
        titleShow2 = Label::createWithTTF("ついて説明します。", FONT_NAME_2, 25);
        titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow2);
        titleShow3 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow3);
    } else if (indexG == 2) {
        bgrTuto = makeSprite("TutorialBgr1.png");
        bgrTuto->setPosition(Point(_winSize.width / 2, 1136 / 2));
        addChild(bgrTuto, 3);
        dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
        dialogNextPoint->setPosition(Point(bgrTuto->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                           dialogNextPoint->getContentSize().height / 2 + 10));
        bgrTuto->addChild(dialogNextPoint);

        titleShow1 = Label::createWithTTF("ではレースに挑戦してみましょう。", FONT_NAME_2, 25);
        titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        bgrTuto->addChild(titleShow1);
        titleShow2 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow2);
        titleShow3 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow3);
    }
}

void RaceTutorail::ShowMasu(int indexRace)
{
    if (indexRace == 1) {
        bgrTuto = makeSprite("TutorialBgr1.png");
        bgrTuto->setPosition(Point(_winSize.width / 2, _winSize.height / 2));
        if (_winSize.height == 960) { // ipad,iphone4
            bgrTuto->setPosition(Point(_winSize.width / 2, 960 / 2));
        }
        addChild(bgrTuto, 3);
        dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
        dialogNextPoint->setPosition(Point(bgrTuto->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                           dialogNextPoint->getContentSize().height / 2 + 10));
        bgrTuto->addChild(dialogNextPoint);

        titleShow1 = Label::createWithTTF("ここがレースコースです。", FONT_NAME_2, 25);
        titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height  - 30));
        bgrTuto->addChild(titleShow1);
        titleShow2 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow2);
        titleShow3 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow3);

        moveSprite = makeSprite("TutorialDown.png");
        moveSprite->setPosition(Point(bgrTuto->getContentSize().width / 2 + 50.0, -bgrTuto->getContentSize().height * 0.5 + 10.0));
        //    moveSprite1 = makeSprite("TutorialDown.png");
        //    moveSprite1->setPosition(Point(bgrTuto->getContentSize().width/2 + 50.0, - bgrTuto->getContentSize().height * 0.5 + 10.0));
        bgrTuto->addChild(moveSprite, 5);
        auto seq = Sequence::create(MoveTo::create(0.5, Point(moveSprite->getPositionX(),  -bgrTuto->getContentSize().height * 0.5 + 0.0)), MoveTo::create(0.5, Point(moveSprite->getPositionX(), -bgrTuto->getContentSize().height * 0.5 + 10.0)), NULL);
        auto rep = RepeatForever::create(seq);
        moveSprite->runAction(rep);
    } else {
        titleShow1->setString("自分の車なごは一番手前を走ります。");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    }
}

void RaceTutorail::ShowSaikoro(int indexSaikoro)
{
    if (indexSaikoro == 1) {
        bgrTuto = makeSprite("TutorialBgr1.png");
        bgrTuto->setPosition(Point(_winSize.width / 2, 1136 * 0.29));
        addChild(bgrTuto, 3);
        dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
        dialogNextPoint->setPosition(Point(bgrTuto->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                           dialogNextPoint->getContentSize().height / 2 + 10));
        bgrTuto->addChild(dialogNextPoint);

        titleShow1 = Label::createWithTTF("レースではこのサイコロを", FONT_NAME_2, 25);
        titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        bgrTuto->addChild(titleShow1);
        titleShow2 = Label::createWithTTF("使用して進みます。", FONT_NAME_2, 25);
        titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow2);
        titleShow3 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow3);

        moveSprite = makeSprite("TutorialDown.png");
        moveSprite->setPosition(Point(bgrTuto->getContentSize().width - moveSprite->getContentSize().width / 2 - 10.0, -bgrTuto->getContentSize().height * 0.5 + 10.0));
        bgrTuto->addChild(moveSprite, 5);
        auto seq = Sequence::create(MoveTo::create(0.5, Point(moveSprite->getPositionX(),  -bgrTuto->getContentSize().height * 0.5 + 0.0)), MoveTo::create(0.5, Point(moveSprite->getPositionX(), -bgrTuto->getContentSize().height * 0.5 + 10.0)), NULL);
        auto rep = RepeatForever::create(seq);
        moveSprite->runAction(rep);
    } else if (indexSaikoro == 2) {
        titleShow1->setString("サイコロは自動でも、");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("自分で止める事も可能です。");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("※今回は自動で止まります。");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    } else if (indexSaikoro == 3) {
        bgrTuto = makeSprite("TutorialBgr1.png");
        bgrTuto->setPosition(Point(_winSize.width / 2, 1136 * 0.29));
        addChild(bgrTuto, 3);
        dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
        dialogNextPoint->setPosition(Point(bgrTuto->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                           dialogNextPoint->getContentSize().height / 2 + 10));
        bgrTuto->addChild(dialogNextPoint);

        titleShow1 = Label::createWithTTF("これでレースのチュートリアルを", FONT_NAME_2, 25);
        titleShow1->setAnchorPoint(Vec2(0, 0.5));
        titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        bgrTuto->addChild(titleShow1);
        titleShow2 = Label::createWithTTF("終了します。", FONT_NAME_2, 25);
        titleShow2->setAnchorPoint(Vec2(0, 0.5));
        titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow2);
        titleShow3 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow3->setAnchorPoint(Vec2(0, 0.5));
        titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow3);

        moveSprite = makeSprite("TutorialDown.png");
        moveSprite->setPosition(Point(bgrTuto->getContentSize().width - moveSprite->getContentSize().width / 2 - 10.0, -bgrTuto->getContentSize().height * 0.5 + 10.0));
        bgrTuto->addChild(moveSprite, 5);
        auto seq = Sequence::create(MoveTo::create(0.5, Point(moveSprite->getPositionX(),  -bgrTuto->getContentSize().height * 0.5 + 0.0)), MoveTo::create(0.5, Point(moveSprite->getPositionX(), -bgrTuto->getContentSize().height * 0.5 + 10.0)), NULL);
        auto rep = RepeatForever::create(seq);
        moveSprite->runAction(rep);
    } else if (indexSaikoro == 4) {
        titleShow1->setString("ではサイコロを振ってみましょう！");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("※サイコロをタップしてください※");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
        dialogNextPoint->setVisible(false);
    }
}

void RaceTutorail::Showitem(int indexItem)
{
    if (indexItem == 1) {
        bgrTuto = makeSprite("TutorialBgr1.png");
        bgrTuto->setPosition(Point(_winSize.width / 2, 1136 * 0.29));
        addChild(bgrTuto, 3);
        dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
        dialogNextPoint->setPosition(Point(bgrTuto->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                           dialogNextPoint->getContentSize().height / 2 + 10));
        bgrTuto->addChild(dialogNextPoint);


        titleShow1 = Label::createWithTTF("もし、レース中にアイテムを", FONT_NAME_2, 25);
        titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        bgrTuto->addChild(titleShow1);
        titleShow2 = Label::createWithTTF("獲得した時は、アイテムを", FONT_NAME_2, 25);
        titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow2);
        titleShow3 = Label::createWithTTF("タップすると発動出来ます。", FONT_NAME_2, 25);
        titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow3);

        moveSprite = makeSprite("TutorialDown.png");
        moveSprite->setPosition(Point(bgrTuto->getContentSize().width - moveSprite->getContentSize().width / 2 - 10.0, -bgrTuto->getContentSize().height * 0.5 + 10.0));
        bgrTuto->addChild(moveSprite, 5);

        auto nitroSprite = makeSprite("itemBgr.png");
        nitroSprite->setPosition(Point(moveSprite->getPositionX(), moveSprite->getPositionY() - moveSprite->getContentSize().height - 40.0));
        bgrTuto->addChild(nitroSprite, 10);

        auto nitroSprite1 = makeSprite("Item1.png");
        nitroSprite1->setPosition(Point(nitroSprite->getContentSize().width / 2, nitroSprite->getContentSize().height / 2));
        nitroSprite->addChild(nitroSprite1);
        auto seq = Sequence::create(MoveTo::create(0.5, Point(moveSprite->getPositionX(),  -bgrTuto->getContentSize().height * 0.5 + 0.0)), MoveTo::create(0.5, Point(moveSprite->getPositionX(), -bgrTuto->getContentSize().height * 0.5 + 10.0)), NULL);
        auto rep = RepeatForever::create(seq);
        moveSprite->runAction(rep);
    }
}

void RaceTutorail::ShowTouchSaikoro()
{
    bgrTuto = makeSprite("TutorialBgr1.png");
    bgrTuto->setPosition(Point(_winSize.width / 2, 1136 * 0.29));
    addChild(bgrTuto, 3);

    titleShow1 = Label::createWithTTF("このサイコロでレースを進めます。", FONT_NAME_2, 25);
    titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
    titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
    titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
    bgrTuto->addChild(titleShow1);
    titleShow2 = Label::createWithTTF("それではレースを開始します！", FONT_NAME_2, 25);
    titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
    titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
    titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
    bgrTuto->addChild(titleShow2);
    titleShow3 = Label::createWithTTF("※タップをしてください。", FONT_NAME_2, 25);
    titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
    titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
    titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    bgrTuto->addChild(titleShow3);

    moveSprite = makeSprite("TutorialDown.png");
    moveSprite->setPosition(Point(bgrTuto->getContentSize().width - moveSprite->getContentSize().width / 2 - 10.0, -bgrTuto->getContentSize().height * 0.5 + 10.0));
    bgrTuto->addChild(moveSprite, 5);
    auto seq = Sequence::create(MoveTo::create(0.5, Point(moveSprite->getPositionX(),  -bgrTuto->getContentSize().height * 0.5 + 0.0)), MoveTo::create(0.5, Point(moveSprite->getPositionX(), -bgrTuto->getContentSize().height * 0.5 + 10.0)), NULL);
    auto rep = RepeatForever::create(seq);
    moveSprite->runAction(rep);
}

void RaceTutorail::ShowSkill(int indexSkill)
{
    if (indexSkill == 1) {
        bgrTuto = makeSprite("TutorialBgr1.png");
        bgrTuto->setPosition(Point(_winSize.width / 2, 1136 * 0.29));
        addChild(bgrTuto, 3);
        dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
        dialogNextPoint->setPosition(Point(bgrTuto->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                           dialogNextPoint->getContentSize().height / 2 + 10));
        bgrTuto->addChild(dialogNextPoint);


        titleShow1 = Label::createWithTTF("ここには編成を行ったチームの", FONT_NAME_2, 25);
        titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        bgrTuto->addChild(titleShow1);
        titleShow2 = Label::createWithTTF("アイコンが表示されます。", FONT_NAME_2, 25);
        titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow2);
        titleShow3 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow3->setPosition(titleShow3->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow3);

        moveSprite = makeSprite("TutorialDown.png");
        moveSprite->setPosition(Point(moveSprite->getContentSize().width / 2 + 10.0, -bgrTuto->getContentSize().height * 0.5 + 10.0));
        bgrTuto->addChild(moveSprite, 5);
        auto seq = Sequence::create(MoveTo::create(0.5, Point(moveSprite->getPositionX(),  -bgrTuto->getContentSize().height * 0.5 + 0.0)), MoveTo::create(0.5, Point(moveSprite->getPositionX(), -bgrTuto->getContentSize().height * 0.5 + 10.0)), NULL);
        auto rep = RepeatForever::create(seq);
        moveSprite->runAction(rep);
    } else if (indexSkill == 2) {
        titleShow1->setString("アイコンがOKになると");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("スキルを発動出来ます。");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    } else if (indexSkill == 3) {
        titleShow1->setString("自分だけの組み合わせを");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("探してみましょう！");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    }
}

void RaceTutorail::ShowMasuEffect(int indexShow)
{
    if (indexShow == 1) {
        bgrTuto = makeSprite("TutorialBgr1.png");
        bgrTuto->setPosition(Point(_winSize.width / 2, _winSize.height * 0.29));
        if (_winSize.height == 960) { // ipad,iphone4
            bgrTuto->setPosition(Point(_winSize.width / 2, 960 * 0.41));
        }
        addChild(bgrTuto, 3);
        dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
        dialogNextPoint->setPosition(Point(bgrTuto->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                           dialogNextPoint->getContentSize().height / 2 + 10));
        bgrTuto->addChild(dialogNextPoint);


        titleShow1 = Label::createWithTTF("これは効果のあるマスです。", FONT_NAME_2, 25);
        titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        bgrTuto->addChild(titleShow1);
        titleShow2 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow2);
        titleShow3 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow3);

        moveSprite = makeSprite("TutorialDown.png");
        moveSprite->setPosition(Point(bgrTuto->getContentSize().width - moveSprite->getContentSize().width / 2 - 70.0, -bgrTuto->getContentSize().height * 0.5 + 5.0));
        bgrTuto->addChild(moveSprite, 5);
        auto seq = Sequence::create(MoveTo::create(0.5, Point(moveSprite->getPositionX(),  -bgrTuto->getContentSize().height * 0.5 - 5.0)), MoveTo::create(0.5, Point(moveSprite->getPositionX(), -bgrTuto->getContentSize().height * 0.5 + 5.0)), NULL);
        auto rep = RepeatForever::create(seq);
        moveSprite->runAction(rep);


        moveSprite1 = makeSprite("TutorialDown.png");
        moveSprite1->setPosition(Point(moveSprite1->getContentSize().width / 2 + 100.0, -bgrTuto->getContentSize().height * 0.5 - 15.0));
        auto seq1 = Sequence::create(MoveTo::create(0.5, Point(moveSprite1->getPositionX(),  -bgrTuto->getContentSize().height * 0.5 - 30.0)), MoveTo::create(0.5, Point(moveSprite1->getPositionX(), -bgrTuto->getContentSize().height * 0.5 - 15.0)), NULL);
        auto rep1 = RepeatForever::create(seq1);
        moveSprite1->runAction(rep1);
        bgrTuto->addChild(moveSprite1, 5);
    } else if (indexShow == 2) {
        moveSprite->setVisible(false);
        titleShow1->setString("例えばこのマスは『加速マス』です。");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    } else if (indexShow == 3) {
        titleShow1->setString("このマスに止まると、");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("より先に進む事が出来ます。");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    } else if (indexShow == 4) {
        moveSprite1->setVisible(false);
        moveSprite->setVisible(true);
        titleShow1->setString("次にこれは『アイテムマス』です。");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    } else if (indexShow == 5) {
        titleShow1->setString("このマスに止まると、");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("アイテムを獲得出来ます。");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    } else if (indexShow == 6) {
        titleShow1->setString("どんな効果があるかは");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("お楽しみです！");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    } else if (indexShow == 7) {
        titleShow1->setString("アイテムはレースが終わると");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("消えるので注意しましょう。");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    } else if (indexShow == 8) {
        moveSprite1->setVisible(true);
        titleShow1->setString("車なごの世界には様々な効果の");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("マスがあります。");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    } else if (indexShow == 9) {
        titleShow1->setString("見た事のないマスを");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("発見したら踏んでみましょう。");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    }
}

void RaceTutorail::ShowMap(int indexMap)
{
    if (indexMap == 1) {
        bgrTuto = makeSprite("TutorialBgr1.png");
        bgrTuto->setPosition(Point(_winSize.width / 2, 1136 * 0.29));
        addChild(bgrTuto, 3);
        dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
        dialogNextPoint->setPosition(Point(bgrTuto->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                           dialogNextPoint->getContentSize().height / 2 + 10));
        bgrTuto->addChild(dialogNextPoint);


        titleShow1 = Label::createWithTTF("ここはミニMAPです。", FONT_NAME_2, 25);
        titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        bgrTuto->addChild(titleShow1);
        titleShow2 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow2);
        titleShow3 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow3);

        moveSprite = makeSprite("TutorialDown.png");
        moveSprite->setRotation(180.0);
        moveSprite->setPosition(Point(moveSprite->getContentSize().width / 2 + 10.0, bgrTuto->getContentSize().height * 1.5 - 10.0));
        auto seq = Sequence::create(MoveTo::create(0.5, Point(moveSprite->getPositionX(), bgrTuto->getContentSize().height * 1.5 + 5.0)), MoveTo::create(0.5, Point(moveSprite->getPositionX(), bgrTuto->getContentSize().height * 1.5 - 10.0)), NULL);
        auto rep = RepeatForever::create(seq);
        moveSprite->runAction(rep);
        bgrTuto->addChild(moveSprite, 5);
    } else if (indexMap == 2) {
        titleShow1->setString("自分や相手がどこにいるのかを");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("すぐに確認できます。");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    }
}

void RaceTutorail::ShowMenu(int indexMenu)
{
    if (indexMenu == 1) {
        bgrTuto = makeSprite("TutorialBgr1.png");
        bgrTuto->setPosition(Point(_winSize.width / 2, _winSize.height * 0.29));
        if (_winSize.height == 960) {
            bgrTuto->setPosition(Point(_winSize.width / 2, 960 * 0.29));
        }
        addChild(bgrTuto, 3);
        dialogNextPoint = makeSprite("tuto_dialog_next_point.png");
        dialogNextPoint->setPosition(Point(bgrTuto->getContentSize().width - dialogNextPoint->getContentSize().width / 2 - 10,
                                           dialogNextPoint->getContentSize().height / 2 + 10));
        bgrTuto->addChild(dialogNextPoint);


        titleShow1 = Label::createWithTTF("ここはメニューボタンです。", FONT_NAME_2, 25);
        titleShow1->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow1->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        bgrTuto->addChild(titleShow1);
        titleShow2 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow2->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow2->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow2);
        titleShow3 = Label::createWithTTF("", FONT_NAME_2, 25);
        titleShow3->setAnchorPoint(Vec2(0.0, 0.5));
        titleShow3->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
        bgrTuto->addChild(titleShow3);

        moveSprite = makeSprite("TutorialDown.png");
        moveSprite->setRotation(180.0);
        moveSprite->setPosition(Point(bgrTuto->getContentSize().width - moveSprite->getContentSize().width / 2 - 10.0, bgrTuto->getContentSize().height * 1.5 - 10.0));
        auto seq = Sequence::create(MoveTo::create(0.5, Point(moveSprite->getPositionX(), bgrTuto->getContentSize().height * 1.5 + 5.0)), MoveTo::create(0.5, Point(moveSprite->getPositionX(), bgrTuto->getContentSize().height * 1.5 - 10.0)), NULL);
        auto rep = RepeatForever::create(seq);
        moveSprite->runAction(rep);
        bgrTuto->addChild(moveSprite, 5);
    } else if (indexMenu == 2) {
        titleShow1->setString("ここからリタイアする事が可能です。");
        titleShow1->setPosition(Point(10.0, bgrTuto->getContentSize().height - 30));
        titleShow2->setString("");
        titleShow2->setPosition(titleShow1->getPosition() + Vec2(0, -30));
        titleShow3->setString("");
        titleShow3->setPosition(titleShow2->getPosition() + Vec2(0, -30));
    }
}
