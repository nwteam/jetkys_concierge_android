#ifndef __syanago__RaceTutorial__
#define __syanago__RaceTutorial__

#include "cocos2d.h"

USING_NS_CC;

class RaceTutorail: public Layer
{
private:
    Size _winSize;
    Sprite* blackBgr;
    Sprite* bgrTuto;
    Sprite* moveSprite;
    Sprite* moveSprite1;
    Sprite* dialogNextPoint;
    Label* titleShow1;
    Label* titleShow2;
    Label* titleShow3;
    
public:
    bool init();
    CREATE_FUNC(RaceTutorail);
    
    void ShowRace(int indexG);
    void ShowMasu(int indexRace);
    void ShowSaikoro(int indexSaikoro);
    void ShowSkill(int indexSkill);
    void ShowMap(int indexMap);
    void ShowMenu(int indexMenu);
    void ShowTouchSaikoro();
    void ShowMasuEffect(int indexShow);
    void Showitem(int indexItem);
    
};

#endif /* defined(__syanago__RaceTutorial__) */
