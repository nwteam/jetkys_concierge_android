#ifndef __syanago__PostRaceTutorialScene__
#define __syanago__PostRaceTutorialScene__

#include "cocos2d.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"

USING_NS_CC;
using namespace SyanagoAPI;

class PostRaceTutorialScene: public Layer
{
public:
    PostRaceTutorialScene();
    ~PostRaceTutorialScene();
    virtual bool init();
    virtual void onEnterTransitionDidFinish();
    CREATE_FUNC(PostRaceTutorialScene);
    static Scene* createScene();

private:
    virtual void onTouchEnded(Touch* touch, Event* unused_event);

    void showFirstImage();

    void createTutoImage();
    void onResponseSetGameStep(Json* json);
    RequestAPI* api;
    DefaultProgress* progress;

    EventListenerTouchOneByOne* _touchEventListener;

    int _tapStage;
    Sprite* tapImage;
    Sprite* ft;
};

#endif /* defined(__syanago__PostRaceTutorialScene__) */
