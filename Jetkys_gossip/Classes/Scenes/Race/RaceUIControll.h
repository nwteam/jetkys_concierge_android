#ifndef __syanago__RaceUIControll__
#define __syanago__RaceUIControll__

#include "create_func.h"
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "SaikoroLayer1.h"
#include "RaceBackground.h"
#include "CellViewController.h"
#include "CharacterView.h"
#include "SoundHelper.h"
#include "RaceBackgroundManager.h"
#include "TurnPurchaseDialog.h"
#include "SystemSettingModel.h"
#include "CourseModel.h"

USING_NS_CC;

class RaceScene;

class RaceUIControll : public cocos2d::Layer, create_func<RaceUIControll>
{
public:
    RaceUIControll();
    ~RaceUIControll();
    using create_func::create;
    bool init(RaceScene* raceScene,
              const std::shared_ptr<CourseModel>& courseModel,
              Json* courseGrid);
    
    void scaleScreen(float scale);
    void checkCamera(std::function<void()> callback);
    void moveEachCharacters();
    void onEndCheckCameraForMovePlayerSkill();
    void moveOnlyPlayerCharacter(int distance);
    void onEndMoveCharacter();
    
    void finishRace();
    void setRanking();
    
    void activatePostPlayerCellEffect(CELL_TYPE type);
    
    CellViewController* getCellView();
    
    void myPaceRun(int distance);
    bool myPaceRunActivated();
    
    void setVisibleToCharacterNameLabels(bool npcEnableFlag);
    
    bool isPlayerGoal();

    int lederSkillCellEfectUpForward(int forwardCellCount);
    void setViewDiceDoubleValueResultEfect(int doubleValue);
    void setViewDeleteDemeritCellEfect(int type);
    
    RaceBackgroundManager* _raceBackgroundManager;
    
    
    std::vector<std::string> _enemyAnimationKeys;
    
private:
    enum SPRITE_Z_ODER{
        ENEMY_CHARACTER_BASEVALUE = 1007,
        /*1007,1008,1009が敵*/
        PLAYER_CHARACTER = 1011,
    };
    enum SPRITE_Z_ODER_OF_PLAYER_CHARACTER{
        CELL_EFFECT = 200,
    };
    
    void showCourseBackground();
    void showCourseCell(Json* courseGrid);
    void showPlayerCharacter();
    void showEnemyCharacters();
    void showCutinBackGround();
    
    void createRunAnimation();
    
    // zoom function
    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
    virtual void onTouchCancelled(Touch *touch, Event *unused_event);
    
    //check cameras
    bool isDefaultCameraPosition();
    void prepareBackgroundImage(float dt);
    
    
    void playRunSound(const int playerDiceValue, std::vector<int> enemyDiceValues);
    const int getPlayMoveSoundEffectMove(const int playerDiceValue, std::vector<int> enemyDiceValues);
    void playMoveSoundEffect(const int moveDistance);
    void stopRunSoundEffect();

    void moveCharacter(int moveTo);
    void forwardCharacter(int moveTo);
    void backCharacter(int moveTo);

    void moveEnemyCharacters(std::vector<int> moveTo, int playerMove);
    
    void onEndAnimateCharacter();

    
    bool shootDiceActivated();
    bool moveCharactersIfPlayerShouldMove();
    bool moveCharactersIfEnemiesShouldMove();
    
    void setEnemyRaceRank(int characterId);
    int getCalculateScore();
    
    Animation* createEnemyAnimation(int enemyCharacterId);

    void setTapEvent();
    
    void fixConsecutiveStopOnMeritCell();
    
    std::shared_ptr<const CourseModel> _courseModel;
    std::shared_ptr<const SystemSettingModel> _systemModel;
    RaceScene * _raceScene;
    
    CellViewController* _cellView;
    cocos2d::Size _winSize;
    
    int GOAL_INDEX;
    
    //Touch
    std::vector<Touch*> _touches;
    Vec2 _touchPoint;
    Node* _container;
    float _touchLen;
    float _touchLenDelta;
    float _movePointOld;
    EventListenerTouchOneByOne* _touchListener;
    //for drag screen
    cocos2d::Point _positionTemp;
    float _minDrag;
    float _maxDrag;
    
    // back ground
    Sprite* _cutinBgr;
    
    int playerCharacterId;
    
    int _previousPlayerColumnIndex;
    
    //player character ranking
    CC_SYNTHESIZE_READONLY(int, _characterRank, CharacterRank);
    CC_SYNTHESIZE_READONLY(std::vector<int>, _masterCharacterIdsOfRank, MasterCharacterIdsOfRank);
    
    //cameras
    cocos2d::Point _targetPlayerCharacterPosition;
    
    // sound
    int _moveSoundId;
    CC_SYNTHESIZE(bool, _isNomalRunSound, IsNomalRunSound);// サイコロを振って前に進む時,trueで通常の進むSE
    
    
    
};


#endif /* defined(__syanago__RaceUIControll__) */
