#ifndef RACE_SEQUENCE_H_
#define RACE_SEQUENCE_H_

#include <queue>
#include "CharacterStatus.h"

namespace RaceStatus {

class RaceSequence {
public:
	RaceSequence():
    finish(false),
    elaplsedTurn(0),
    tagerTurn(1000),
    shootDiceAgain(false),
    movingByChain(false),
    playerChainResultFoward(0),
    playerChainResultBack(0){};

	bool finish;
    std::queue<int> finishedEnemyIndex;
    CharacterStatus* characterStatusInRanking[4];
    int elaplsedTurn;
    int tagerTurn;
    
    bool shootDiceAgain;
    bool movingByChain;
    
    //TODO:一時的にここに置いておくが、後で統一
    int playerChainResultFoward;
    int playerChainResultBack;
    
    


};

};
#endif /* RACE_SEQUENCE_H_ */
