#ifndef __syanago__CharacterStatus__
#define __syanago__CharacterStatus__

#include "CharacterModel.h"
#include "CharacterView.h"
#include "CellType.h"

namespace RaceStatus {

class CharacterStatus {
public:
    CharacterStatus(const std::shared_ptr<const CharacterModel>& _characterModel, bool isPlayer = false):
    characterModel(_characterModel),
    isPlayer(isPlayer),
    isMoving(false),
    finished(false),
    demeritCellType(0),
    chainForwardCount(0),
    chainBackCount(0),
    chainBonus(0),
    moveDistance(0),
    isBlocked(false),
    remainningBlockTurn(0),
    mustStop(false),
    remainningStopTurn(0),
    mustSlowDown(false),
    slowDownValue(0),
    mustLimitSpeed(false),
    mustHighSpeed(false),
    downwardSlopeType(NONE),
    upwardSlopeType(NONE),
    usedSkillCount(0){
        stoppedGridIds.clear();
    };
    
    void resetPreCellEffect();
    
    std::shared_ptr<const CharacterModel> characterModel;
    CharacterView* characterView;
    
    bool isPlayer;
    bool isMoving;
    int moveDistance;
    bool finished;
    
    
    //cell chain
    int chainForwardCount;
    int chainBackCount;
    int chainBonus;
    
    //skill
    bool isBlocked;
    int remainningBlockTurn;
    
    //TODO remove this flag
    //for enemies;
    int demeritCellType;
    
    //pre cell effects
    //stop
    bool mustStop;
    int remainningStopTurn;
    
    //slowDown
    bool mustSlowDown;
    int slowDownValue;
    
    //徐行
    bool mustLimitSpeed;
    
    //高速道路
    bool mustHighSpeed;
    
    //下り坂
    CELL_TYPE downwardSlopeType;
    
    //上り坂
    CELL_TYPE upwardSlopeType;
    
    //レース中に止まったマスの情報
    std::vector<int> stoppedGridIds;
    
    //スキルの使用回数
    int usedSkillCount;
private:
    CharacterView* createPlayerCharacter();
    CharacterView* createEnemyCharacter();
};
    
}


#endif /* defined(__syanago__CharacterStatus__) */
