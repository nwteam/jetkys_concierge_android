#include "LeaderSkill.h"
#include "LeaderSkillModel.h"
#include "SkillAndLederSkillKind.h"
#include "CharacterModel.h"
#include "CharacterEfectView.h"

using namespace cocos2d;

LeaderSkill::LeaderSkill(const int characterId, const int characterIdOfHelpPlayer, const bool isFriend):
    deleteDemeritCellProbability(1.0), deleteDemeritCellAtProbability(false), diceResultUpAtProbavilityFlag(false), scoreMagnification(0), forwardMagnification(0), chargeAtDiceValueTarget(0), chargeAtEffectPercent(0), chargeAtDiceValueTargetFriend(0), chargeAtEffectPercentFriend(0)
{
    statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::POWOER_RATE] = 1.0;
    statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::TECHNIQUE_RATE] = 1.0;
    statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::BRAKE_RATE] = 1.0;

    statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::POWOER_RATE] = 1.0;
    statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::TECHNIQUE_RATE] = 1.0;
    statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::BRAKE_RATE] = 1.0;

    statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::BODY_TYPE] = -1.0;
    statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::BODY_TYPE] = -1.0;

    UserDefault::getInstance()->setBoolForKey("Friends", isFriend);

    const std::shared_ptr<const LeaderSkillModel>& leaderSkillModel = CharacterModel::getLeaderSkillModel(characterId);
    if (isFriend == false) {
        initializeLeaderSkill(leaderSkillModel, false);
    } else {
        const std::shared_ptr<const LeaderSkillModel>& leaderSkillModelOfHelpPlayer = CharacterModel::getLeaderSkillModel(characterIdOfHelpPlayer);
        initializeLeaderSkill(leaderSkillModel, false);
        initializeLeaderSkill(leaderSkillModelOfHelpPlayer, true);
    }
}

void LeaderSkill::initializeLeaderSkill(const std::shared_ptr<const LeaderSkillModel>& leaderSkillModel, bool isFriend)
{
    switch (leaderSkillModel->getSkillKind()) {
    case LEADER_SKILL_TYPE::PLUS_STATUS: {
        setCharacterStatusUpOfLeaderSkill(leaderSkillModel->getValue2(), leaderSkillModel->getValue3(), leaderSkillModel->getValue4(), leaderSkillModel->getValue1(), isFriend);
        break;
    }
    case LEADER_SKILL_TYPE::DELETE_DEMERIT_CELL: {
        deleteDemeritCellAtProbability = true;
        deleteDemeritCellProbability = deleteDemeritCellProbability + leaderSkillModel->getValue1();
        break;
    }
    case LEADER_SKILL_TYPE::CELL_EFECT_UP: {
        if (leaderSkillModel->getValue1() == 5) {
            scoreMagnification += leaderSkillModel->getValue2();
        } else if (leaderSkillModel->getValue1() == 7) {
            forwardMagnification += leaderSkillModel->getValue2();
        }
        break;
    }
    case LEADER_SKILL_TYPE::DICE_RESULT_UP: {
        diceResultUpAtProbavilityFlag = true;
        diceResultUpProbability = leaderSkillModel->getValue1();
        diceResultUpMagnification = leaderSkillModel->getValue2();
        break;
    }
    case LEADER_SKILL_TYPE::CHARGE_AT_DICE_VALUE: {
        if (chargeAtDiceValueTarget == 0 || chargeAtDiceValueTarget == leaderSkillModel->getValue1()) {
            chargeAtDiceValueTarget = leaderSkillModel->getValue1();
            chargeAtEffectPercent = std::max(chargeAtEffectPercent, leaderSkillModel->getValue2());
        } else {
            chargeAtDiceValueTargetFriend = leaderSkillModel->getValue1();
            chargeAtEffectPercentFriend = leaderSkillModel->getValue2();
        }
    }
    }
}


bool LeaderSkill::isDeleteTarget(CELL_TYPE type)
{
    if (arc4random() % 100 < deleteDemeritCellProbability &&
        CellType::isDemerit(type)) {
        return true;
    } else {
        return false;
    }
}

int LeaderSkill::scoreUp(int score, RaceStatus::CharacterStatus* playerStatus)
{
    int plusScoreCellCount = score * (scoreMagnification * 0.01);
    int result = score + plusScoreCellCount;
    // show effect
    auto characterEffect = CharacterEfectView::create()->createViewCellEfectUpEfect(plusScoreCellCount, 2);
    characterEffect->setPosition(Point(playerStatus->characterView->getContentSize().width / 2,
                                       playerStatus->characterView->getContentSize().height / 2));
    playerStatus->characterView->addChild(characterEffect, 200);
    return result;
}

// 車なごのステータス変更のリーダースキル
void LeaderSkill::setCharacterStatusUpOfLeaderSkill(int powoerPercent, int techniquePercent, int brakePercent, int bodyType, bool isFriend)
{
    if (!isFriend) {
        statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::POWOER_RATE] = (powoerPercent * 0.01) + 1;
        statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::TECHNIQUE_RATE] = (techniquePercent * 0.01) + 1;
        statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::BRAKE_RATE] = (brakePercent * 0.01) + 1;
        statusUpOfLeaderSkill[LEADER_SKILL::PLAYER::BODY_TYPE] = bodyType;
    } else {
        statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::POWOER_RATE] = (powoerPercent * 0.01) + 1;
        statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::TECHNIQUE_RATE] = (techniquePercent * 0.01) + 1;
        statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::BRAKE_RATE] = (brakePercent * 0.01) + 1;
        statusUpOfLeaderSkill[LEADER_SKILL::HELP_PLAYER::BODY_TYPE] = bodyType;
    }
}
