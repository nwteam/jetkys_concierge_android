#ifndef SKILL_H_
#define SKILL_H_

#include "CellType.h"

namespace RaceStatus {

class Skill {
public:
	Skill():
    remainningTurn(0),
    characterId(0),
    movePlayerCharacterDistance(0),
    plusDiceValue(0),
    criticalDiceFlag(false),
    criticalDiceTargetValue(0),
    criticalDiceResult(0),
    forceStop(false),
	myPaceFlag(false),
	myPaceRunDistance(0),
	myPaceSkipCellFlag(false),
    deleteDemeritCell(false),
    deleteDemeritCellType(0){}

    bool isDeleteTarget(CELL_TYPE type);
    
    int characterId;
    //スキル発動後の残りターン数
    int remainningTurn;
    
    //move player character (accel and back);
    int movePlayerCharacterDistance;
    
    //plus dice
    int plusDiceValue;
    
    //critical dice
    bool criticalDiceFlag;
    int criticalDiceTargetValue;
    int criticalDiceResult;
    
    //force stop
    bool forceStop;
    
	//my pace run
	bool myPaceFlag;
	int myPaceRunDistance;
	bool myPaceSkipCellFlag;
    
    //DeleteDemeritCell
    bool deleteDemeritCell;
    int deleteDemeritCellType;
    bool deleteDemeritCellActivated;

	
};

}
#endif /* SKILL_H_ */
