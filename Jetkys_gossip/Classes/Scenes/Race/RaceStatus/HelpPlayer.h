#ifndef __syanago__HelpPlayer__
#define __syanago__HelpPlayer__

#include "cocos2d.h"

class HelpPlayer
{
public:
    HelpPlayer(int id_, int characterId, bool isFriend, std::string name, int rank, int level);
    
    CC_SYNTHESIZE_READONLY(int, _id, Id);
    CC_SYNTHESIZE_READONLY(int, _characterId, CharacterId);
    CC_SYNTHESIZE_READONLY(bool, _isFriend, IsFriend);
    CC_SYNTHESIZE_READONLY(std::string, _name, Name);
    CC_SYNTHESIZE_READONLY(int, _rank, Rank);
    CC_SYNTHESIZE_READONLY(int, _level, Level);
    CC_SYNTHESIZE_READONLY(std::string, _lastLogInTime, LastLoginTime);
    
    void setLastLogInTime(std::string logInTime);
};


#endif /* defined(__syanago__HelpPlayer__) */
