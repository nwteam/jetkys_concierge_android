#ifndef __syanago__Item__
#define __syanago__Item__
#include "ItemType.h"

namespace RaceStatus {
    class Item {
    public:
        Item():
        itemAvailable(false),
        currentItem(ITEM_NO_NON_ITEM),
        patolampActivated(false),
        patolampUsed(false),
        trafficControlActivated(false),
        trafficControlUsed(false),
        nitoroUsed(false){}
        
        ITEM_TYPE getRandomItem();
        
        bool itemAvailable;
        ITEM_TYPE currentItem;
        
        bool patolampActivated;
        bool patolampUsed;
        
        bool trafficControlActivated;
        bool trafficControlUsed;
        
        bool nitoroUsed;
        
    };
}


#endif /* defined(__syanago__Item__) */
