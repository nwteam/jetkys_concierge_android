
#ifndef __syanago__View__
#define __syanago__View__

namespace RaceStatus {
 
class View {
public:
    View():
    cameraPositionFixed(true),
    pause(false),
    diceAvailable(false),
    diceRolling(false),
    iconItemTapped(false),
    itemDescriptionActive(false){};

    bool cameraPositionFixed;
    bool pause;
    bool diceAvailable;
    bool diceRolling;
    bool iconItemTapped;
    bool itemDescriptionActive;
};

}





#endif /* defined(__syanago__View__) */
