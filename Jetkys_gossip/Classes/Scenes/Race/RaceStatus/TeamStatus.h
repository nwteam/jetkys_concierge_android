#ifndef __syanago__TeamStatus__
#define __syanago__TeamStatus__

#include "cocos2d.h"

class TeamStatus
{
public:
    TeamStatus(int power, int technique, int brake);
    //ex
    CC_SYNTHESIZE_READONLY(int, _power, Power);
    //rea
    CC_SYNTHESIZE_READONLY(int, _technique, Technique);
    //des
    CC_SYNTHESIZE_READONLY(int, _brake, Brake);
};


#endif /* defined(__syanago__TeamStatus__) */
