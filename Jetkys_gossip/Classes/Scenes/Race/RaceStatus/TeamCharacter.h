#ifndef __syanago__TeamCharacter__
#define __syanago__TeamCharacter__

#include "CharacterModel.h"

namespace RaceStatus {
  
    class TeamCharacter {
    public:
        TeamCharacter(std::shared_ptr<const CharacterModel> _characterModel):
        characterModel(_characterModel){};
    
        std::shared_ptr<const CharacterModel> characterModel;
        
    };
    
};


#endif /* defined(__syanago__TeamCharacter__) */
