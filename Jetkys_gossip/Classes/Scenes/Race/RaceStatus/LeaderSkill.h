#ifndef __syanago__LeaderSkill__
#define __syanago__LeaderSkill__

#include "cocos2d.h"
#include "LeaderSkillModel.h"
#include "CellType.h"
#include "CharacterStatus.h"

namespace LEADER_SKILL{
    namespace PLAYER{
        static const std::string BODY_TYPE = "LeaderSkillBodyTypeOfPlayer";
        static const std::string POWOER_RATE = "LeaderSkillPowoerRateOfPlayer";
        static const std::string TECHNIQUE_RATE = "LeaderSkillTechniqueRateOfPlayer";
        static const std::string BRAKE_RATE = "LeaderSkillBrakeRateOfPlayer";
    };
    namespace HELP_PLAYER{
        static const std::string BODY_TYPE = "LeaderSkillBodyTypeOfHelpPlayer";
        static const std::string POWOER_RATE = "LeaderSkillPowoerRateOfHelpPlayer";
        static const std::string TECHNIQUE_RATE = "LeaderSkillTechniqueRateOfHelpPlayer";
        static const std::string BRAKE_RATE = "LeaderSkillBrakeRateOfHelpPlayer";
    };
};

class LeaderSkill
{
public:
    LeaderSkill(const int masterCharacterIdOfPlayer, const int masterCharacterIdOfHelpPlayer = 0, const bool isFriend = false);
    
    bool isDeleteTarget(CELL_TYPE type);
    int scoreUp(int score, RaceStatus::CharacterStatus* playerStatus);
    
    //DELETE_DEMERIT_CELL
    bool deleteDemeritCellAtProbability;
    int deleteDemeritCellProbability;
    
    //DICE_RESULT_UP
    bool diceResultUpAtProbavilityFlag;
    int diceResultUpProbability;
    int diceResultUpMagnification;
    
    //CELL_EFECT_UP
    float forwardMagnification;
    float scoreMagnification;
    
    //CHARGE_AT_DICE_VALUE
    int chargeAtDiceValueTarget;
    int chargeAtEffectPercent;
    int chargeAtDiceValueTargetFriend;
    int chargeAtEffectPercentFriend;
    
    std::map<std::string, float> statusUpOfLeaderSkill;
    
private:
    
    void initializeLeaderSkill(const std::shared_ptr<const LeaderSkillModel>& leaderSkillModel, bool isFriend);
    
    void setCharacterStatusUpOfLeaderSkill(int powoerPercent, int techniquePercent, int brakePercent, int bodyType, bool isFriend);
    
};

#endif /* defined(__syanago__LeaderSkill__) */
