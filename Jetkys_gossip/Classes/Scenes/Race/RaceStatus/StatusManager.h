#ifndef __syanago__StatusManager__
#define __syanago__StatusManager__

#include "CharacterModel.h"
#include "HelpPlayer.h"

#include "View.h"
#include "RaceSequence.h"
#include "Skill.h"
#include "Dice.h"
#include "CharacterStatus.h"
#include "TeamCharacter.h"
#include "LeaderSkill.h"
#include "TeamStatus.h"
#include "Define.h"
#include "CourseModel.h"
#include "Item.h"
#include "ScoreStatus.h"
#include "CellViewController.h"
#include "SystemSettingModel.h"

namespace RaceStatus {

class StatusManager
{
public:
    StatusManager(const std::vector<std::shared_ptr<const CharacterModel>>& playerCharacterModels,
                  HelpPlayer* helpPlayer,
                  const std::vector<std::shared_ptr<const CharacterModel>>& enemyCharacters,
                  TeamStatus* _teamStatus, const std::shared_ptr<const CourseModel>& _courseModel);
    
    ~StatusManager();
    
    std::vector<int> createEnemyDiceValue(CellViewController* cellViewController);
    
    void checkDeleteDemeritCellSkills(CELL_TYPE type);
    void disableUsedItems();
    void countStopOnMeritIfPlayerIsOnMeritCell(CELL_TYPE type);
    void resetChainCounts();
    
    bool isEnemiesReachedTargetPosition();
    
    void showCellEffect(const CELL_TYPE type);
    
    void activatePostEnemyCellEffect(int index, CellViewController* cellView);
    
    View* view;
    RaceSequence* sequence;
    Skill* skill;
    Dice* dice;
    Item* item;
    ScoreStatus* score;
    
    TeamStatus* teamStatus;
    std::shared_ptr<const CourseModel> courseModel;
    
    CharacterStatus* playerStatus;
    CharacterStatus* enemyStatus[3];
    
    //for skill
    TeamCharacter* teamCharacters[5];
    
    LeaderSkill* leaderSkill;
    
    std::shared_ptr<SystemSettingModel> systemModel;

private:
    void initTeamCharacters(const std::vector<std::shared_ptr<const CharacterModel>>& characterModels, HelpPlayer* helpPlayer);
    CharacterView* createEnemyCharacter(int index);
    
    int activatePreEnemyCellEffect(CELL_TYPE cellType, CharacterStatus* characterStatus, int moveDistance);
    void setBonusLabel(std::string bonusText, Color3B color);
    void removeBonusLabel(Label* label);
    
};
    
}






#endif /* defined(__syanago__StatusManager__) */
