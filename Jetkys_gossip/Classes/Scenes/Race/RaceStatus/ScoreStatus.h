#ifndef __syanago__ScoreStatus__
#define __syanago__ScoreStatus__

namespace RaceStatus{
    class ScoreStatus {
    public:
        ScoreStatus():
        currentConsecutiveStopOnMeritCell(0),
        consecutiveStopOnMeritCell(0),
        bonusScore(0),
        targetScore(0)
        {};
        
        int currentConsecutiveStopOnMeritCell;
        int consecutiveStopOnMeritCell;
        int bonusScore;
        int targetScore;
    };
}

#endif /* defined(__syanago__ScoreStatus__) */
