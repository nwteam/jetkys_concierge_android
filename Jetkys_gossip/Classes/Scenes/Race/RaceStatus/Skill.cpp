#include "Skill.h"

using namespace RaceStatus;

bool Skill::isDeleteTarget(CELL_TYPE type)
{
    if ((deleteDemeritCellType == 1 && CellType::isDemerit(type)) ||
        (deleteDemeritCellType == 2 && (CellType::isSpeedDown(type) || CellType::isUpwardSlope(type))) ||
        (deleteDemeritCellType == 4 && CellType::isBack(type))) {
        return true;
    } else {
        return false;
    }
}
