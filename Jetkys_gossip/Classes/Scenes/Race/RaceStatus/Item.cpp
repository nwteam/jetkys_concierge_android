#include "Item.h"
#include <random>

using namespace RaceStatus;

ITEM_TYPE Item::getRandomItem()
{
    if (currentItem == 5) {
        ITEM_TYPE itemBox[3] = {ITEM_NO_NITORO, ITEM_NO_TRAFFIC_CONTROL, ITEM_NO_PATORANP};
        currentItem =  itemBox[arc4random() % 3];
    }
    return currentItem;
}