#include "StatusManager.h"
#include "EnemyDiceValueLogicModel.h"
#include "RaceCellRogicModel.h"
#include "SystemSettingModel.h"
#include "SoundHelper.h"
#include "FontDefines.h"
#include "CharacterEfectView.h"
#include "LevelModel.h"

using namespace RaceStatus;


StatusManager::~StatusManager()
{
    delete view;
    delete sequence;
    delete skill;
    delete dice;
    delete playerStatus;
    delete leaderSkill;
    delete item;
    delete score;
    for (int i = 0; i < 3; i++) {
        delete enemyStatus[i];
    }
    for (int i = 0; i < 5; i++) {
        if (teamCharacters[i] != nullptr) {
            delete teamCharacters[i];
        }
    }
}

StatusManager::StatusManager(const std::vector<std::shared_ptr<const CharacterModel> >& playerCharacterModels, HelpPlayer* helpPlayer, const std::vector<std::shared_ptr<const CharacterModel> >& enemyCharacters, TeamStatus* _teamStatus, const std::shared_ptr<const CourseModel>& courseModel):
    teamStatus(_teamStatus), courseModel(courseModel)
{
    view = new View();
    sequence = new RaceSequence();
    skill = new Skill();
    dice = new Dice();
    item = new Item();
    score = new ScoreStatus();
    systemModel = SystemSettingModel::getModel();

    initTeamCharacters(playerCharacterModels, helpPlayer);

    playerStatus = new CharacterStatus(teamCharacters[0]->characterModel, true);
    playerStatus->characterView = CharacterView::create(teamStatus, StringUtils::format("%d_sd.png", playerStatus->characterModel->getID()), "E_Target.png", "1P", 3, START_CELL);
    for (int i = 0; i < 3; i++) {
        enemyStatus[i] = new CharacterStatus(enemyCharacters.at(i));
        enemyStatus[i]->characterView = createEnemyCharacter(i);
    }
    if (teamCharacters[4]->characterModel != nullptr) {
        leaderSkill = new LeaderSkill(teamCharacters[0]->characterModel->getID(), teamCharacters[4]->characterModel->getID(), helpPlayer->getIsFriend());
    } else {
        leaderSkill = new LeaderSkill(teamCharacters[0]->characterModel->getID());
    }
}

void StatusManager::initTeamCharacters(const std::vector<std::shared_ptr<const CharacterModel> >& characterModels, HelpPlayer* helpPlayer)
{
    for (int i = 0; i < 5; i++) {
        teamCharacters[i] = nullptr;
    }
    for (int i = 0; i < characterModels.size(); i++) {
        teamCharacters[i] = new TeamCharacter(characterModels.at(i));
    }
    teamCharacters[4] = new TeamCharacter(CharacterModel::find(helpPlayer->getCharacterId()));
}

CharacterView* StatusManager::createEnemyCharacter(int index)
{
    std::string spriteName = "tu_sd.png";
    if (!UserDefault::getInstance()->getBoolForKey("Tutorial")) {
        spriteName = StringUtils::format("%d_sd.png", enemyStatus[index]->characterModel->getID());
    }

    std::shared_ptr<const LevelModel>levelModel = LevelModel::findByCourseModel(courseModel, index);
    int plusExtension = enemyStatus[index]->characterModel->getBaseExercise() * levelModel->getExercise() * 0.01;
    int plusReaction = enemyStatus[index]->characterModel->getBaseReaction() * levelModel->getReaction() * 0.01;
    int plusDesition = enemyStatus[index]->characterModel->getBaseDecision() * levelModel->getDecision() * 0.01;
    TeamStatus* status = new TeamStatus(
        (enemyStatus[index]->characterModel->getBaseExercise() + plusExtension) * courseModel->getEnemyStatusRate(),
        (enemyStatus[index]->characterModel->getBaseDecision() + plusDesition) * courseModel->getEnemyStatusRate(),
        (enemyStatus[index]->characterModel->getBaseReaction() + plusReaction) * courseModel->getEnemyStatusRate()
        );
    std::string label = "";
    if (index == 0 && courseModel->getCourseType() == CourseModel::COURSE_TYPE::BOSS) {
        label = "BOSS";
    }
    return CharacterView::create(status, spriteName, "Target.png", label, index, START_CELL);
}

void StatusManager::checkDeleteDemeritCellSkills(CELL_TYPE type)
{
    // check skills
    if (skill->deleteDemeritCell == true) {
        if (skill->isDeleteTarget(type)) {
            skill->deleteDemeritCellActivated = true;
        }
    }
    // check leader skill
    if (leaderSkill->deleteDemeritCellAtProbability && leaderSkill->isDeleteTarget(type)) {
        skill->deleteDemeritCellActivated = true;
    }
}

void StatusManager::disableUsedItems()
{
    if (item->nitoroUsed) {
        item->nitoroUsed = false;
        playerStatus->characterView->removeEffectOnTop();
    }
    if (item->patolampUsed) {
        item->patolampUsed = false;
        playerStatus->characterView->removeEffectOnTop();
    }
    if (item->trafficControlUsed) {
        item->trafficControlUsed = false;
        for (int i = 0; i < 3; i++) {
            enemyStatus[i]->characterView->removeEffectOnTop();
        }
    }
}

void StatusManager::countStopOnMeritIfPlayerIsOnMeritCell(CELL_TYPE type)
{
    if (CellType::isMerit(type) ||
        (CellType::isForward(type) && playerStatus->chainForwardCount < SystemSettingModel::getModel()->getMaxNumberOfGridChain())) {
        score->currentConsecutiveStopOnMeritCell++;
        if (score->currentConsecutiveStopOnMeritCell >= score->consecutiveStopOnMeritCell) {
            score->consecutiveStopOnMeritCell = score->currentConsecutiveStopOnMeritCell;
        }
    } else {
        score->currentConsecutiveStopOnMeritCell = 0;
    }
}

void StatusManager::resetChainCounts()
{
    if (playerStatus->remainningStopTurn > -1) {
        playerStatus->chainForwardCount = 0;
        playerStatus->chainBackCount = 0;
    }
    for (int i = 0; i < 3; i++) {
        enemyStatus[i]->chainForwardCount = 0;
        enemyStatus[i]->chainBackCount = 0;
    }
}

bool StatusManager::isEnemiesReachedTargetPosition()
{
    int targetPosition = 0;
    switch (courseModel->getRaceType()) {
    case 1:
    case 2:
    case 3:
        targetPosition = 3;
        break;

    case 4:
    case 6:
    case 8:
        targetPosition = 2;
        break;

    case 5:
    case 7:
    case 9:
        targetPosition = 1;
        break;

    default:
        break;
    }
    int finishedEnemies = 0;
    for (int i = 0; i < 3; i++) {
        if (enemyStatus[i]->finished) {
            finishedEnemies++;
        }
    }
    return (targetPosition > 0 && targetPosition <= finishedEnemies);
}

void StatusManager::showCellEffect(const CELL_TYPE type)
{
    if (CellType::isForward(type)) {
        if (playerStatus->chainForwardCount < systemModel->getMaxNumberOfGridChain()) {
            SOUND_HELPER->playCharacterVoiceEfect(playerStatus->characterModel->getID(), 21);
            if (playerStatus->chainForwardCount == systemModel->getMaxNumberOfGridChain() - 1) {
                setBonusLabel("MAX Chain", Color3B::ORANGE);
            } else if (playerStatus->chainForwardCount > 0) {
                setBonusLabel(StringUtils::format("%d", playerStatus->chainForwardCount + 1) + "Chain", Color3B::ORANGE);
            }
            auto characterEffect = CharacterEfectView::create()->createCellActionEfect(type, 0, sequence->playerChainResultFoward, teamStatus->getPower(), 0, 0);
            characterEffect->setPosition(Point(playerStatus->characterView->getContentSize().width / 2,  playerStatus->characterView->getContentSize().height / 2));
            playerStatus->characterView->addChild(characterEffect);
        }
    } else if (CellType::isBack(type)) {
        if (playerStatus->chainBackCount < systemModel->getMaxNumberOfUturnGridChai()) {
            SOUND_HELPER->playCharacterVoiceEfect(playerStatus->characterModel->getID(), 22);
            if (playerStatus->chainBackCount == systemModel->getMaxNumberOfUturnGridChai() - 1) {
                setBonusLabel("MAX Chain", Color3B::BLUE);
            } else if (playerStatus->chainBackCount > 0) {
                setBonusLabel(StringUtils::format("%d", playerStatus->chainBackCount + 1) + "Chain", Color3B::BLUE);
            }
            auto characterEffect = CharacterEfectView::create()->createCellActionEfect(type, 0, -sequence->playerChainResultBack, 0, teamStatus->getTechnique(), 0);
            characterEffect->setPosition(Point(playerStatus->characterView->getContentSize().width / 2, playerStatus->characterView->getContentSize().height / 2));
            playerStatus->characterView->addChild(characterEffect);
        }
    } else if (CellType::isSpeedDown(type)) {
        SOUND_HELPER->playCharacterVoiceEfect(playerStatus->characterModel->getID(), 23);
        SOUND_HELPER->playEffect(STATUS_DOWN_EFFECT, false);
        playerStatus->characterView->gensokuEffect(true);
        auto characterEffect = CharacterEfectView::create()->createCellActionEfect(type, 0, -playerStatus->slowDownValue, 0, teamStatus->getTechnique(), 0);
        characterEffect->setPosition(Point(playerStatus->characterView->getContentSize().width / 2, playerStatus->characterView->getContentSize().height / 2));
        playerStatus->characterView->addChild(characterEffect);
    } else if (CellType::isStop(type)) {
        SOUND_HELPER->playCharacterVoiceEfect(playerStatus->characterModel->getID(), 24);
        if (!playerStatus->mustStop) {
            auto characterEffect = CharacterEfectView::create()->createCellActionEfect(type, 0, playerStatus->remainningStopTurn, 0, 0, teamStatus->getBrake());
            characterEffect->setPosition(Point(playerStatus->characterView->getContentSize().width / 2, playerStatus->characterView->getContentSize().height / 2));
            playerStatus->characterView->addChild(characterEffect);
        }
    } else if (PREFERENCE_CELL_1 == type) {
        SOUND_HELPER->playCharacterVoiceEfect(playerStatus->characterModel->getID(), 25);
        SOUND_HELPER->playEffect(DICE_REPLAY_EFFECT, false);
    } else if (CellType::isDownwardSlope(type)) {
        if (playerStatus->downwardSlopeType == NONE) {
            SOUND_HELPER->playEffect(STATUS_UP_EFFECT, false);
        } else {
            if (playerStatus->moveDistance > 0) {
                auto characterEffect = CharacterEfectView::create()->createCellActionEfect(type, playerStatus->moveDistance, RaceCellRogicModel::cellRogicDownSlope(playerStatus->moveDistance, teamStatus->getBrake(), type, courseModel->getOperandId()), 0, 0, teamStatus->getBrake());
                characterEffect->setPosition(Point(playerStatus->characterView->getContentSize().width / 2, playerStatus->characterView->getContentSize().height / 2));
                playerStatus->characterView->addChild(characterEffect);
                SOUND_HELPER->playCharacterVoiceEfect(playerStatus->characterModel->getID(), 21);
            }
        }
    } else if (CellType::isUpwardSlope(type)) {
        if (playerStatus->upwardSlopeType == NONE) {
            SOUND_HELPER->playEffect(STATUS_DOWN_EFFECT, false);
        } else {
            if (playerStatus->moveDistance > 0) {
                auto characterEffect = CharacterEfectView::create()->createCellActionEfect(type, playerStatus->moveDistance, RaceCellRogicModel::cellRogicUpSlope(playerStatus->moveDistance, teamStatus->getPower(), playerStatus->upwardSlopeType, courseModel->getOperandId()), teamStatus->getPower(), 0, 0);
                characterEffect->setPosition(Point(playerStatus->characterView->getContentSize().width / 2, playerStatus->characterView->getContentSize().height / 2));
                playerStatus->characterView->addChild(characterEffect);
                SOUND_HELPER->playCharacterVoiceEfect(playerStatus->characterModel->getID(), 23);
            }
        }
    } else if (GET_ITEM_CELL_1 == type) {
        if (item->currentItem == 5) {
            SOUND_HELPER->playCharacterVoiceEfect(playerStatus->characterModel->getID(), 28);
            SOUND_HELPER->playEffect(GET_ITEM_EFFECT, false);
            playerStatus->characterView->showEffectOnTop("bikkuri.png");
        }
    } else if (CellType::isScoreCell(type)) {
        SOUND_HELPER->playCharacterVoiceEfect(playerStatus->characterModel->getID(), 26);
        SOUND_HELPER->playEffect(GET_SCORE_EFFECT, false);
        playerStatus->characterView->showEffectOnTop("hikarukyuu.png");
    } else if (CHARGE_CELL_1 == type) {
        SOUND_HELPER->playCharacterVoiceEfect(playerStatus->characterModel->getID(), 27);
        SOUND_HELPER->playEffect(SKILL_CHARGE_EFFECT, false);
    }
}

void StatusManager::activatePostEnemyCellEffect(int i, CellViewController* cellView)
{
    CELL_TYPE cellType = cellView->_cell[2][enemyStatus[i]->characterView->getCharacterColumn()]->getCellType();
    if (CellType::isForward(cellType)) {
        if (enemyStatus[i]->chainForwardCount < SystemSettingModel::getModel()->getMaxNumberOfGridChain()) {
            enemyStatus[i]->chainForwardCount++;
            enemyStatus[i]->moveDistance = RaceCellRogicModel::cellRogicFoword(enemyStatus[i]->characterView->getPower(), cellType, courseModel->getOperandId());
            enemyStatus[i]->chainBonus++;
        } else {
            enemyStatus[i]->chainBonus = 0;
        }
    } else if (CellType::isBack(cellType)) {
        if (enemyStatus[i]->chainBackCount < SystemSettingModel::getModel()->getMaxNumberOfUturnGridChai()) {
            enemyStatus[i]->chainBackCount++;
            enemyStatus[i]->moveDistance = -RaceCellRogicModel::cellRogicBack(enemyStatus[i]->characterView->getTechnique(), cellType, courseModel->getOperandId());
            enemyStatus[i]->chainBonus++;
        } else {
            enemyStatus[i]->chainBonus = 0;
        }
    } else {
        enemyStatus[i]->chainBonus = 0;
        if (CellType::isStop(cellType)) {
            if (!enemyStatus[i]->mustStop && enemyStatus[i]->remainningStopTurn == 0) {
                enemyStatus[i]->mustStop = true;
                enemyStatus[i]->remainningStopTurn = RaceCellRogicModel::cellRogicLose(enemyStatus[0]->characterView->getBrake(), cellType,  courseModel->getOperandId());
            } else if (enemyStatus[i]->mustStop && enemyStatus[i]->remainningStopTurn == 0) {
                enemyStatus[i]->mustStop = false;
            }
        } else if (PREFERENCE_CELL_1 == cellType) {
            auto _enemyDiceLogic = EnemyDiceValueLogicModel::create(playerStatus->characterView->getCharacterColumn(), courseModel, cellView);
            enemyStatus[i]->moveDistance = _enemyDiceLogic->getEnemyDiceValue(enemyStatus[i]->characterModel, enemyStatus[i]->characterView);
        } else if (SLOWLY_CELL_1 == cellType) {
            enemyStatus[i]->characterView->removeEffectOnTop();
            enemyStatus[i]->demeritCellType = SLOWLY_CELL_1;
        } else if (MINIMUM_SPEED_CELL_1 == cellType) {
            enemyStatus[i]->demeritCellType = MINIMUM_SPEED_CELL_1;
        }
    }
    if (enemyStatus[i]->mustStop && enemyStatus[i]->remainningStopTurn > 0) {
        enemyStatus[i]->moveDistance = 0;
    }
}

void StatusManager::setBonusLabel(std::string bonusText, Color3B color)
{
    auto bonusLabel = Label::createWithTTF(bonusText, FONT_NAME_2, 50, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::TOP);
    bonusLabel->enableOutline(Color4B(color), 2);
    playerStatus->characterView->addChild(bonusLabel);
    bonusLabel->setPosition(playerStatus->characterView->getNameLabelPosition() + Point(60, 0));
    bonusLabel->setOpacity(0);

    bonusLabel->runAction(Sequence::create(FadeIn::create(0.1), DelayTime::create(0.8), FadeOut::create(0.1), NULL));
    auto moveAction1 = MoveBy::create(0.1, Vec2(-30, 0));
    auto moveAction2 = MoveBy::create(0.1, Vec2(-20, 0));
    auto moveAction3 = MoveBy::create(0.1, Vec2(-10, 0));
    bonusLabel->runAction(Sequence::create(moveAction1,
                                           moveAction2,
                                           moveAction3,
                                           DelayTime::create(0.4),
                                           moveAction3,
                                           moveAction2,
                                           moveAction1,
                                           NULL));

    playerStatus->characterView->runAction(Sequence::create(DelayTime::create(1.0), CallFunc::create(CC_CALLBACK_0(StatusManager::removeBonusLabel, this, bonusLabel)), NULL));
}

void StatusManager::removeBonusLabel(Label* label)
{
    playerStatus->characterView->removeChild(label, true);
}

std::vector<int>StatusManager::createEnemyDiceValue(CellViewController* cellViewController)
{
    // reset effect on top of the enemies
    for (CharacterStatus* _enemyStatus : enemyStatus) {
        _enemyStatus->characterView->removeEffectOnTop();
    }

    std::vector<int>results = { 0, 0, 0 };
    auto _enemyDiceLogic = EnemyDiceValueLogicModel::create(playerStatus->characterView->getCharacterColumn(), courseModel, cellViewController);

    for (int i = 0; i < 3; i++) {
        RaceStatus::CharacterStatus* _enemyStatus = enemyStatus[i];

        results.at(i) = _enemyDiceLogic->getEnemyDiceValue(_enemyStatus->characterModel, enemyStatus[i]->characterView);

        results.at(i) = activatePreEnemyCellEffect(cellViewController->_cell[1][_enemyStatus->characterView->getCharacterColumn()]->getCellType(), _enemyStatus, results.at(i));

        // force stop
        if (_enemyStatus->demeritCellType == 0 && _enemyStatus->remainningBlockTurn == 0) {
            _enemyStatus->characterView->removeEffectOnTop();
        }
        if (skill->forceStop  && _enemyStatus->remainningBlockTurn > 0) {
            if (!sequence->movingByChain) {
                _enemyStatus->remainningBlockTurn--;
            }
            results.at(i) = 0;
        }

        // traffic controll
        if (item->trafficControlActivated) {
            if (_enemyStatus->characterView->getCharacterColumn() - playerStatus->characterView->getCharacterColumn() > 0 &&
                _enemyStatus->characterView->getCharacterColumn() - playerStatus->characterView->getCharacterColumn() <= 5) {
                results.at(i) = 0;
                _enemyStatus->characterView->removeEffectOnTop();
                _enemyStatus->characterView->showEffectOnTop("Item4.png");
            }
        }
        // patolamp
        if (item->patolampActivated) {
            if (_enemyStatus->characterView->getCharacterColumn() >= playerStatus->characterView->getCharacterColumn() &&
                _enemyStatus->characterView->getCharacterColumn() + results.at(i) < playerStatus->characterView->getCharacterColumn() + playerStatus->moveDistance) {
                results.at(i) = 0;
                _enemyStatus->characterView->removeEffectOnTop();
                _enemyStatus->characterView->showEffectOnTop("Item4.png");
            }
        }
        // finished
        if (_enemyStatus->characterView->getCharacterColumn() > cellViewController->GOAL_SELL) {
            results.at(i) = 0;
        }
        if (_enemyStatus->moveDistance > 0) {
            SOUND_HELPER->playEffect(FAST_RUN, false);
        } else if (_enemyStatus->moveDistance < 0) {
            SOUND_HELPER->playEffect(BACK, false);
        }
    }

    item->trafficControlActivated = false;
    return results;
}

int StatusManager::activatePreEnemyCellEffect(CELL_TYPE cellType, CharacterStatus* characterStatus, int moveDistance)
{
    if (CellType::isUpwardSlope(cellType)) {
        moveDistance = RaceCellRogicModel::cellRogicUpSlope(moveDistance, characterStatus->characterView->getPower(), cellType, courseModel->getOperandId());
    } else if (CellType::isDownwardSlope(cellType)) {
        moveDistance = RaceCellRogicModel::cellRogicDownSlope(moveDistance, characterStatus->characterView->getBrake(), cellType, courseModel->getOperandId());
    } else if (cellType == MINIMUM_SPEED_CELL_1) {
        // TODO: remove demeritCellType
        characterStatus->demeritCellType = 0;
        if (moveDistance == 1 || moveDistance == 2) {
            moveDistance = 0;
            characterStatus->characterView->setEffectOnHeadByCellType(cellType);
        }
    } else if (cellType == SLOWLY_CELL_1) {
        characterStatus->demeritCellType = 0;
        if (moveDistance == 5 || moveDistance == 6) {
            moveDistance = 0;
            characterStatus->characterView->setEffectOnHeadByCellType(cellType);
        }
    }

    // stop cell
    if (characterStatus->mustStop && characterStatus->remainningStopTurn > 0) {
        characterStatus->remainningStopTurn--;
        moveDistance = 0;
    }

    return moveDistance;
}
