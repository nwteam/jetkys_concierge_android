#include "CharacterStatus.h"

using namespace RaceStatus;

void CharacterStatus::resetPreCellEffect()
{
    mustStop = false;
    remainningStopTurn = 0;
    mustSlowDown = false;
    slowDownValue = 0;
    mustLimitSpeed = false;
    mustHighSpeed = false;
    downwardSlopeType = NONE;
    upwardSlopeType = NONE;
}