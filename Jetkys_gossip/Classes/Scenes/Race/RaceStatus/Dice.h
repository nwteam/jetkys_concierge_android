#ifndef __syanago__Dice__
#define __syanago__Dice__

namespace RaceStatus {

class Dice {
public:
    Dice():
        buff(0)
        , diceValue(0){};
    
    //by nitoro and patoranp
    int buff;
    //this value is not affected by criticalDice
    int diceValue;
};
    
};

#endif /* defined(__syanago__Dice__) */
