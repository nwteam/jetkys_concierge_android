#include "HelpPlayer.h"

HelpPlayer::HelpPlayer(int id, int characterId, bool isFriend, std::string name, int rank, int level):
    _id(id)
    , _characterId(characterId)
    , _isFriend(isFriend)
    , _name(name)
    , _rank(rank)
    , _level(level) {}

void HelpPlayer::setLastLogInTime(std::string logInTime)
{
    _lastLogInTime = logInTime;
}