//
//  Define.h
//  syanago
//
//  Created by Hao Nguyen on 8/2/14.
//
//

#ifndef syanago_Define_h
#define syanago_Define_h

// cell
#define START_CELL 12
#define END_CELL 180
#define MAX_CELL 200

// animation
#define ANIMATION_AITE_2 "Animation_RunAite2"
#define ANIMATION_AITE_3 "Animation_RunAite_3"
#define ANIMATION_AITE_4 "Animation_RunAite_4"
#define ANIMATION_GENSOKU "Animation_Gensoku"

// background
#define HEIGHT_VIEW_CHANGE 368.937
#define WIDTH_VIEW_CHANGE 639

// Distance one cell
#define VELOCITY_CHARACTER Vec2(58, 33.53)
#define MAX_DIS_MINIMAP 145.0

#endif
