#include "SyanagoDebugger.h"
#include "DesignsInDevelop.h"
#include "SceneDebugger.h"
#include "SplashScene.h"
#include "UserDefaultManager.h"
#include "RaceDebugger.h"
#include "ResourceDebugger.h"

Scene* SyanagoDebugger::createScene()
{
    Scene* scene = Scene::create();
    scene->addChild(SyanagoDebugger::create());
    return scene;
}

bool SyanagoDebugger::init()
{
    if (!LayerColor::initWithColor(DesignsInDevelop::BACKGROUND)) {
        return false;
    }
    showHeadline();
    showDebuggerArea();
    showStartButton();
    return true;
}

void SyanagoDebugger::showHeadline()
{
    showConnectionStatusLabel();
    showConnectionStatus();
    showTitle();
    showChangeConnectionButton();
}

void SyanagoDebugger::showConnectionStatusLabel()
{
    Sprite* sprite = Sprite::create();
    sprite->setTextureRect(Rect(0, 0, DesignsInDevelop::GRID * 5, DesignsInDevelop::GRID * 3));
    sprite->setColor(Color3B::BLACK);
    sprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    sprite->setPosition(0, Director::getInstance()->getWinSize().height);

    Label* label = Label::createWithSystemFont("current\nconnection", "Meiryo", 16);
    label->setColor(Color3B::WHITE);
    label->setAlignment(TextHAlignment::LEFT);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    label->setWidth(DesignsInDevelop::GRID * 5);
    label->setPosition(DesignsInDevelop::GRID * 0.5, DesignsInDevelop::GRID * 1.5);
    sprite->addChild(label);
    addChild(sprite);
}

void SyanagoDebugger::showConnectionStatus()
{
    Sprite* sprite = Sprite::create();
    sprite->setTextureRect(Rect(0, 0, DesignsInDevelop::GRID * 20, DesignsInDevelop::GRID * 3));
    sprite->setColor(DesignsInDevelop::SUB);
    sprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    sprite->setPosition(DesignsInDevelop::GRID * 5, Director::getInstance()->getWinSize().height);

    _connectionStatus = Label::createWithSystemFont(UserDefaultManager::getCurrentDomainName(), "Meiryo", 30);
    _connectionStatus->setColor(Color3B::WHITE);
    _connectionStatus->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    _connectionStatus->setAlignment(TextHAlignment::LEFT);
    _connectionStatus->setPosition(DesignsInDevelop::GRID, DesignsInDevelop::GRID * 1.5);
    sprite->addChild(_connectionStatus);
    addChild(sprite);
}

void SyanagoDebugger::showTitle()
{
    Label* label = Label::createWithSystemFont("SyanagoDebugger", "Meiryo", 30);
    label->setColor(DesignsInDevelop::STRING);
    label->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    label->setPosition(DesignsInDevelop::GRID * 2, Director::getInstance()->getWinSize().height - DesignsInDevelop::GRID * 5);
    addChild(label);
}

void SyanagoDebugger::showChangeConnectionButton()
{
    ui::Button* button = ui::Button::create("tomato.png");
    button->setScale9Enabled(true);
    button->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    button->setPosition(Vec2(Director::getInstance()->getWinSize()));
    button->setContentSize(Size(DesignsInDevelop::GRID * 7, DesignsInDevelop::GRID * 3));
    button->setTitleText("Change\nConnection");
    button->setTitleFontSize(20);
    button->setTitleFontName("Meiryo");
    button->setTitleColor(Color3B::WHITE);
    button->addTouchEventListener([this](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            UserDefaultManager::cycleConnection();
            _connectionStatus->setString(UserDefaultManager::getCurrentDomainName());
        }
    });

    addChild(button);
}

void SyanagoDebugger::showDebuggerArea()
{
    Sprite* sprite = Sprite::create();
    sprite->setTextureRect(Rect(0, 0, Director::getInstance()->getWinSize().width - DesignsInDevelop::GRID * 2, DesignsInDevelop::GRID * 26));
    sprite->setColor(DesignsInDevelop::SUB);
    sprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    sprite->setPosition(DesignsInDevelop::GRID, Director::getInstance()->getWinSize().height - DesignsInDevelop::GRID * 9);

    Label* label = Label::createWithSystemFont("Debuggers", "Meiryo", 30);
    label->setColor(Color3B::WHITE);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    label->setPosition(DesignsInDevelop::GRID * 4, DesignsInDevelop::GRID * 19);
    sprite->addChild(label);


    sprite->addChild(createDebuggerButton("Scene\nDebugger",
                                          Vec2(DesignsInDevelop::GRID * 16, DesignsInDevelop::GRID * 24), [](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            Director::getInstance()->replaceScene(SceneDebugger::createScene());
        }
    }));
    sprite->addChild(createDebuggerButton("Resource\nDebugger",
                                          Vec2(DesignsInDevelop::GRID * 16, DesignsInDevelop::GRID * 12), [](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            Director::getInstance()->replaceScene(ResourceDebugger::createScene());
        }
    }));
    sprite->addChild(createDebuggerButton("Race\nDebugger",
                                          Vec2(DesignsInDevelop::GRID * 2, DesignsInDevelop::GRID * 12), [](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            Director::getInstance()->replaceScene(RaceDebugger::createScene());
        }
    }));
    addChild(sprite);
}

ui::Button* SyanagoDebugger::createDebuggerButton(const std::string title, const Vec2 position, const Callback& callback)
{
    ui::Button* button = ui::Button::create("light_gray.png");
    button->setScale9Enabled(true);
    button->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    button->setPosition(position);
    button->setContentSize(Size(DesignsInDevelop::GRID * 12, DesignsInDevelop::GRID * 10));
    button->setTitleText(title);
    button->setTitleFontSize(20);
    button->setTitleFontName("Meiryo");
    button->setTitleColor(DesignsInDevelop::STRING);
    button->addTouchEventListener(callback);
    return button;
}


void SyanagoDebugger::showStartButton()
{
    ui::Button* button = ui::Button::create("tomato.png");
    button->setScale9Enabled(true);
    button->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    button->setPosition(Vec2(0, 0));
    button->setContentSize(Size(Director::getInstance()->getWinSize().width, DesignsInDevelop::GRID * 12));
    button->setTitleText("Start Game");
    button->setTitleFontSize(50);
    button->setTitleFontName("Meiryo");
    button->setTitleColor(Color3B::WHITE);
    button->addTouchEventListener([](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            Director::getInstance()->replaceScene(SplashScene::createScene());
        }
    });
    addChild(button);
}


















