#ifndef __syanago__SyanagoDebugger__
#define __syanago__SyanagoDebugger__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class SyanagoDebugger : public LayerColor
{
public:
    static Scene* createScene();
    CREATE_FUNC(SyanagoDebugger);
    virtual bool init();
private:
    void showHeadline();
    void showConnectionStatusLabel();
    void showConnectionStatus();
    void showChangeConnectionButton();
    void showTitle();
    
    void showDebuggerArea();
    typedef std::function<void(Ref*, ui::Widget::TouchEventType)> Callback;
    ui::Button* createDebuggerButton(const std::string title, const Vec2 position, const Callback&);
    
    void showStartButton();
    
    Label* _connectionStatus;
};

#endif /* defined(__syanago__SyanagoDebugger__) */
