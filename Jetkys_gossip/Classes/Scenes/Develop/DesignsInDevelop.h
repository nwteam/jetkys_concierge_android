#ifndef __syanago__DesignsInDevelop__
#define __syanago__DesignsInDevelop__

#include "cocos2d.h"

USING_NS_CC;

class DesignsInDevelop {
public:
    static const Vec2 getCenterPosition();
    static const Vec2 getLeftTopPosition();
    
    static const int GRID;
    static const Color4B BACKGROUND;
    static const Color3B SIDEBAR;
    static const Color3B HEADER;
    static const Color3B SUB;
    static const Color3B HIGHLIGHT;
    static const Color3B TOMATO;
    
    static const Color3B CELL_BACKGROUND;
    static const Color3B BORDER;
    static const Color3B STRING;
    static const Color3B BACK_BUTTON;
    static const std::string NORMAL_BUTTON;

};


#endif /* defined(__syanago__DesignsInDevelop__) */
