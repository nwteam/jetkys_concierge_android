#include "DebuggerHeader.h"
#include "DesignsInDevelop.h"
#include "SyanagoDebugger.h"
#include "UserDefaultManager.h"

const int DebuggerHeader::HEIGHT (DesignsInDevelop::GRID * 6);

DebuggerHeader::DebuggerHeader() {}

bool DebuggerHeader::init(std::string title)
{
    return init(title, [] {
        Director::getInstance()->replaceScene(SyanagoDebugger::createScene());
    });
}

bool DebuggerHeader::init(std::string title, const BackButtonCallback& callback)
{
    if (!Sprite::init()) {
        return false;
    }
    _backButtonCallback = callback;

    setTextureRect(Rect(0, 0, Director::getInstance()->getWinSize().width, DebuggerHeader::HEIGHT));
    setColor(DesignsInDevelop::HEADER);
    setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    setPosition(0, Director::getInstance()->getWinSize().height);

    showBackButton();
    showConnectionStatusLabel();
    showConnectionStatus();
    showTitle(title);
    return true;
}

void DebuggerHeader::showBackButton()
{
    ui::Button* button = ui::Button::create("tomato.png");
    button->setScale9Enabled(true);
    button->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    button->setPosition(Vec2(DesignsInDevelop::GRID, DesignsInDevelop::GRID));
    button->setContentSize(Size(DesignsInDevelop::GRID * 4, DesignsInDevelop::GRID * 4));
    button->setTitleText("Back");
    button->setTitleFontSize(20);
    button->setTitleFontName("Meiryo");
    button->setTitleColor(Color3B::WHITE);
    button->addTouchEventListener([this](Ref* ref, ui::Widget::TouchEventType type) {
        _backButtonCallback();
    });
    addChild(button);
}

void DebuggerHeader::showConnectionStatusLabel()
{
    Sprite* sprite = Sprite::create();
    sprite->setTextureRect(Rect(0, 0, DesignsInDevelop::GRID * 5, DesignsInDevelop::GRID * 3));
    sprite->setColor(Color3B::BLACK);
    sprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    sprite->setPosition(DesignsInDevelop::GRID * 7, DebuggerHeader::HEIGHT);

    Label* label = Label::createWithSystemFont("current\nconnection", "Meiryo", 16);
    label->setColor(Color3B::WHITE);
    label->setAlignment(TextHAlignment::LEFT);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    label->setWidth(DesignsInDevelop::GRID * 5);
    label->setPosition(DesignsInDevelop::GRID * 0.5, DesignsInDevelop::GRID * 1.5);
    sprite->addChild(label);
    addChild(sprite);
}
void DebuggerHeader::showConnectionStatus()
{
    Sprite* sprite = Sprite::create();
    sprite->setTextureRect(Rect(0, 0, DesignsInDevelop::GRID * 20, DesignsInDevelop::GRID * 3));
    sprite->setColor(DesignsInDevelop::SUB);
    sprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    sprite->setPosition(DesignsInDevelop::GRID * 12, DebuggerHeader::HEIGHT);

    Label* label = Label::createWithSystemFont(UserDefaultManager::getCurrentDomainName(), "Meiryo", 30);
    label->setColor(Color3B::WHITE);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    label->setAlignment(TextHAlignment::LEFT);
    label->setPosition(DesignsInDevelop::GRID, DesignsInDevelop::GRID * 1.5);
    sprite->addChild(label);
    addChild(sprite);
}
void DebuggerHeader::showTitle(std::string title)
{
    Label* label = Label::createWithSystemFont(title, "Meiryo", 30);
    label->setColor(DesignsInDevelop::STRING);
    label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    label->setAlignment(TextHAlignment::LEFT);
    label->setPosition(DesignsInDevelop::GRID * 7, DesignsInDevelop::GRID * 1);
    addChild(label);
}


















