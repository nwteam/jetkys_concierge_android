#include "DebuggerSidebar.h"
#include "DesignsInDevelop.h"
#include "DebuggerHeader.h"
#include "SplashScene.h"
#include "SyanagoDebugger.h"
#include "SceneDebugger.h"
#include "ResourceDebugger.h"
#include "RaceDebugger.h"

const int DebuggerSidebar::WIDTH (DesignsInDevelop::GRID * 5);

DebuggerSidebar::DebuggerSidebar():
    HEIGHT(Director::getInstance()->getWinSize().height - DebuggerHeader::HEIGHT)
{}

bool DebuggerSidebar::init(CURRENT_SCENE currentScene)
{
    if (!Sprite::init()) {
        return false;
    }

    _currentScene = currentScene;
    setPosition(0, 0);
    setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    setTextureRect(Rect(0, 0, DebuggerSidebar::WIDTH, HEIGHT));
    setColor(DesignsInDevelop::SIDEBAR);


    showButton(HOME, "Syanago\nDebugger", []() {
        Director::getInstance()->replaceScene(SyanagoDebugger::createScene());
    });
    showButton(SCENE, "Scene\nDebug", []() {
        Director::getInstance()->replaceScene(SceneDebugger::createScene());
    });
    showButton(RACE, "Race\nDebug", []() {
        Director::getInstance()->replaceScene(RaceDebugger::createScene());
    });
    showButton(RESOURCES, "Resource\nDebug", []() {
        Director::getInstance()->replaceScene(ResourceDebugger::createScene());
    });

    showSeparateLines();

    showStartButton();
    return true;
}

void DebuggerSidebar::showStartButton()
{
    ui::Button* button = ui::Button::create("tomato.png");
    button->setScale9Enabled(true);
    button->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    button->setPosition(Vec2::ZERO);
    button->setContentSize(Size(DebuggerSidebar::WIDTH, DesignsInDevelop::GRID * 8));
    button->setTitleText("Start\nGame");
    button->setTitleFontSize(30);
    button->setTitleFontName("Meiryo");
    button->setTitleColor(Color3B::WHITE);
    button->addTouchEventListener([this](Ref* ref, ui::Widget::TouchEventType type) {
        Director::getInstance()->replaceScene(SplashScene::createScene());
    });
    addChild(button);
}

void DebuggerSidebar::showButton(CURRENT_SCENE scene,  std::string title, const std::function<void()>& callback)
{
    ui::Button* button = ui::Button::create("black.png");
    button->setScale9Enabled(true);
    button->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    button->setPosition(Vec2(0, HEIGHT - DebuggerSidebar::WIDTH * scene));
    button->setContentSize(Size(DebuggerSidebar::WIDTH, DebuggerSidebar::WIDTH));
    button->setTitleText(title);
    button->setTitleFontSize(20);
    button->setTitleFontName("Meiryo");
    button->setTitleColor(Color3B::WHITE);
    if (_currentScene == scene) {
        button->setEnabled(false);
        button->setColor(Color3B::GRAY);
    } else {
        button->addTouchEventListener([callback](Ref* ref, ui::Widget::TouchEventType type) {
            if (type == ui::Widget::TouchEventType::ENDED) {
                callback();
            }
        });
    }
    addChild(button);
}

void DebuggerSidebar::showSeparateLines()
{
    for (int i = 1; i < 5; i++) {
        Sprite* sprite = Sprite::create();
        sprite->setTextureRect(Rect(0, 0, DesignsInDevelop::GRID * 4, 1));
        sprite->setColor(DesignsInDevelop::SUB);
        sprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
        sprite->setPosition(DesignsInDevelop::GRID / 2, HEIGHT - DebuggerSidebar::WIDTH * i);
        addChild(sprite);
    }
}





















