#ifndef __syanago__DevelopScene__
#define __syanago__DevelopScene__

#include <stdio.h>
#include "cocos2d.h"
#include "DialogView.h"
#include "cocos-ext.h"
#include "RequestAPI.h"

USING_NS_CC;
USING_NS_CC_EXT;

class DevelopScene: public LayerColor, public DialogDelegate
{
public:
    virtual bool init();
    CREATE_FUNC(DevelopScene);
    static Scene* createScene();
    ~DevelopScene();
    
    void btDialogCallback(ButtonIndex aIndex, int tag);
    void btDialogCallback(ButtonIndex aIndex){};
    
    void onTweetAndGetFuelEnded();
    
private:
    enum SPRITE_TAG{
        SWICHING_SERVER = 0,
        RESOURCES_DELETE,
        KEY_CHAIN_DELETE,
        DEVELOP_SPRITE,
        GET_FUEL_TWEET_BUTTON,
        RANKING_LAYER,
        DISABLE_EVENT_LAYER,
        IN_APP_PARCHASE,
        TURN_PURCHASE_DIALOG,
        REWARD_MESSAGE_DIALOG
    };
    
    enum Z_ORDER{
        BACKGROUND = 0,
        DEVELOP_IMAGE,
        SELECT_SCENE_LIST,
        DEVELOP_BUTTON,
        RANKING_LAYER_ORDER
    };
    
    enum DIALOG_RESPONSE{
        DIALOG_KEY_CHAIN_DELETE_TAG = 1,
        DIALOG_RESOURCES_DELETE_TAG,
    };
    
    void showStartGameButton();
    void showDevelopImage();
    void showGotoRaceDebuggerButton();
    void showGotoTurnPurchaseDialogButton();
    void showMissionRewardDialog();
    void showItemUseButton();
    void showMasterTabelUpdateButton();
    
    
    void showDevelopButton();
    void showServerImage(const char* filePath, std::string serverName);
    MenuItemImage* createSwitchingOurDatabaseButton();
    
    MenuItemSprite* createKeyChainDeleteButton();
    MenuItemSprite* createTweetButton();
    MenuItemSprite* createInAppPurcheseButton();
    void onTapSwichOurDatabaseButton(Ref*sender);
    void onTapResourcesDeleteButton(Ref*sender);
    void onTapTweetButton(Ref* sender);
    void onTapInAppParchase(Ref* sender);
};

#endif /* defined(__syanago__DevelopScene__) */
