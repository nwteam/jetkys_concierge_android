#include "DevelopScene.h"
#include "UserDefaultManager.h"
#include "FontDefines.h"
#include "VisibleRect.h"

#include "SplashScene.h"
#include "PasswordScene.h"
#include "FriendLayer.h"
#include "MainScene.h"
#include "TitleScene.h"
#include "RegisterScene.h"
#include "DownloadLayer.h"
#include "LoadingScene.h"
#include "GetFuelTweetButton.h"
#include "RaceDebugger.h"
#include "TurnPurchaseDialog.h"
#include "Token.h"
#include "InAppPurchaseModel.h"
#include "DataPlayer.h"
#include "ResouseDeleteModel.h"
#include "MessageDialog.h"
#include "HeaderLayer.h"
#include "GachaLayer.h"
#include "GachaEffectModel.h"
#include "GachaEffectLayer.h"
#include "DropEffectLayer.h"
#include "ItemButton.h"
#include "AwakeResponseModel.h"
#include "API.h"
#include "editor-support/spine/Json.h"
#include "SelectFirstCarScene.h"
#include "Native.h"
#include "ColectionDetail.h"
#include "DesignsInDevelop.h"

DevelopScene::~DevelopScene()
{
    Director::getInstance()->getTextureCache()->removeAllTextures();
    Director::getInstance()->purgeCachedData();
}

Scene* DevelopScene::createScene()
{
    auto scene = Scene::create();
    scene->addChild(DevelopScene::create());
    return scene;
}

bool DevelopScene::init()
{
    if (!LayerColor::initWithColor(DesignsInDevelop::BACKGROUND)) {
        return false;
    }

    showStartGameButton();
    showDevelopImage();
    showDevelopButton();
    showGotoRaceDebuggerButton();

    return true;
}

void DevelopScene::showStartGameButton()
{
    auto button =  ui::Button::create("gacha_dialog_button.png");
    Size winSize = Director::getInstance()->getWinSize();
    button->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    button->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    button->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            Director::getInstance()->replaceScene(SplashScene::createScene());
        }
    });

    auto label = Label::createWithTTF("StartGame", FONT_NAME_2, 18);
    label->setAnchorPoint(Vec2(0.5, 0.5));
    label->setPosition(Vec2(button->getContentSize() / 2) - Vec2(0, 9));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    button->addChild(label);
    addChild(button);
}

void DevelopScene::showDevelopImage()
{
    if (UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DOMAIN_URL) == SyanagoAPI::API::DOMAINS::DEVELOPMENT) {
        showServerImage("develop_image.png", "開発サーバー");
    } else if (UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DOMAIN_URL) == SyanagoAPI::API::DOMAINS::TESTING) {
        showServerImage("Fit.png", "検証サーバー");
    } else {
        showServerImage("db.png", "本番サーバー");
    }
}

void DevelopScene::showGotoRaceDebuggerButton()
{
    auto button =  ui::Button::create("gacha_dialog_button.png");
    button->setPosition(Vec2(Director::getInstance()->getWinSize()) - Vec2(10, 40));
    button->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    button->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            Director::getInstance()->replaceScene(RaceDebugger::createScene());
        }
    });

    auto label = Label::createWithTTF("RaceScene", FONT_NAME_2, 18);
    label->setAnchorPoint(Vec2(0.5, 0.5));
    label->setPosition(Vec2(button->getContentSize() / 2) - Vec2(0, 9));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    button->addChild(label);
    addChild(button);
}



void DevelopScene::showGotoTurnPurchaseDialogButton()
{
    auto button = ui::Button::create("gacha_dialog_button.png");
    button->setPosition(Vec2(500, 80));
    button->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    button->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            TurnPurchaseDialog* dialog = TurnPurchaseDialog::create([this](int additionalTurn) {
                CCLOG("[DevelopScene::showGotoTurnPurchaseDialogButton] additionalTurn: %d", additionalTurn);
                removeChildByTag(SPRITE_TAG::TURN_PURCHASE_DIALOG);
            }, [this]() {
                transitScene(DevelopScene::createScene());
            });
            dialog->setTag(SPRITE_TAG::TURN_PURCHASE_DIALOG);
            addChild(dialog, 1001);
        }
    });

    auto label = Label::createWithTTF("turn", FONT_NAME_2, 18);
    label->setAnchorPoint(Vec2(0.5, 0.5));
    label->setPosition(Vec2(button->getContentSize() / 2) - Vec2(0, 9));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    button->addChild(label);
    addChild(button);
}

void DevelopScene::showItemUseButton()
{
    auto button =  ItemButton::create([]() {
        CCLOG("Item使用");
    }, []() {
        CCLOG("ITem使用停止");
    }, []() {
        CCLOG("説明文表示");
    }, []() {
        CCLOG("説明文非表示");
    });
    button->setPosition(Vec2(500, 80));
    button->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    addChild(button);

    button->setItem(ITEM_NO_NITORO);
}

void DevelopScene::showMissionRewardDialog()
{
    auto button =  ui::Button::create("gacha_dialog_button.png");
    button->setPosition(Vec2(500, 80));
    button->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
    button->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            auto dialog = MessageDialog::create("ミッション", "®あああああああ\nああああああああああああああああああああああああああああああああああああああ", []() {});
            dialog->setTag(SPRITE_TAG::REWARD_MESSAGE_DIALOG);
            addChild(dialog, 1001);
        }
    });

    auto label = Label::createWithTTF("MissionReward", FONT_NAME_2, 18);
    label->setAnchorPoint(Vec2(0.5, 0.5));
    label->setPosition(Vec2(button->getContentSize() / 2) - Vec2(0, 9));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    button->addChild(label);
    addChild(button);
}

void DevelopScene::showServerImage(const char* filePath, std::string serverName)
{
    auto sprite = makeSprite(filePath);
    sprite->setTag(SPRITE_TAG::DEVELOP_SPRITE);
    sprite->setPosition(Point(sprite->getContentSize() / 2) + Point(10, 10));
    addChild(sprite, Z_ORDER::DEVELOP_IMAGE);

    auto label = Label::createWithTTF(serverName, FONT_NAME_2, 30);
    label->enableOutline(Color4B(Color3B::BLACK), 2);
    label->setPosition(Point(sprite->getContentSize().width / 2, 10));
    sprite->addChild(label, 100);
}


void DevelopScene::showDevelopButton()
{
    auto devMenu = MenuTouch::create(createSwitchingOurDatabaseButton(),
                                     NULL);
    devMenu->setPosition(Point::ZERO);
    addChild(devMenu, Z_ORDER::DEVELOP_BUTTON);
}

void DevelopScene::showMasterTabelUpdateButton()
{
    auto button =  ui::Button::create("gacha_dialog_button.png");
    button->setPosition(Vec2(0, Director::getInstance()->getWinSize().height) + Vec2(15, -115));
    button->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    button->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            UserDefault::getInstance()->setBoolForKey(UserDefaultManager::KEY::IS_RESET_MASTER_TABLE, true);
        }
    });
    auto label = Label::createWithTTF("マスタ更新", FONT_NAME_2, 18);
    label->setAnchorPoint(Vec2(0.5, 0.5));
    label->setPosition(Vec2(button->getContentSize() / 2) - Vec2(0, 9));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    button->addChild(label);
    addChild(button);
}

MenuItemImage* DevelopScene::createSwitchingOurDatabaseButton()
{
    auto result = MenuItemImage::create("gacha_dialog_button.png", "gacha_dialog_button.png", CC_CALLBACK_1(DevelopScene::onTapSwichOurDatabaseButton, this));
    result->setPosition(VisibleRect::leftBottom() + Point(result->getContentSize().width / 2 + 14, Director::getInstance()->getWinSize().height - result->getContentSize().height - 10));
    result->setTag(SPRITE_TAG::SWICHING_SERVER);

    auto label = Label::createWithTTF("開発用", FONT_NAME_2, 28);
    label->setPosition(Point(result->getContentSize().width / 2, result->getContentSize().height / 2 - 14));
    label->enableShadow();
    result->addChild(label);
    return result;
}

void DevelopScene::onTapSwichOurDatabaseButton(Ref* sender)
{
    if (UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DOMAIN_URL) == SyanagoAPI::API::DOMAINS::TESTING) {
        UserDefault::getInstance()->setStringForKey(UserDefaultManager::KEY::DOMAIN_URL, SyanagoAPI::API::DOMAINS::DEVELOPMENT);
    } else if (UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DOMAIN_URL) == SyanagoAPI::API::DOMAINS::DEVELOPMENT) {
        UserDefault::getInstance()->setStringForKey(UserDefaultManager::KEY::DOMAIN_URL, SyanagoAPI::API::DOMAINS::PRODUCTION);
    } else if (UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DOMAIN_URL) == SyanagoAPI::API::DOMAINS::PRODUCTION) {
        UserDefault::getInstance()->setStringForKey(UserDefaultManager::KEY::DOMAIN_URL, SyanagoAPI::API::DOMAINS::TESTING);
    }
    getChildByTag(SPRITE_TAG::DEVELOP_SPRITE)->removeFromParent();
    showDevelopImage();
}

void DevelopScene::onTapResourcesDeleteButton(Ref* sender)
{
    auto dialog = DialogView::createDialog("リソースデータを全て消去しますか？", "", 2, DIALOG_RESPONSE::DIALOG_RESOURCES_DELETE_TAG);
    addChild(dialog, DIALOG_ZORDER);
    dialog->_delegate = this;
}



MenuItemSprite* DevelopScene::createKeyChainDeleteButton()
{
    auto result = makeMenuItem("gacha_dialog_button.png", [&](Ref* sender) {
        auto dialog = DialogView::createDialog("キーチェーンのDIDを消去しますか？", "", 2, DIALOG_RESPONSE::DIALOG_KEY_CHAIN_DELETE_TAG);
        addChild(dialog, DIALOG_ZORDER);
        dialog->_delegate = this;
    });
    result->setPosition(VisibleRect::leftBottom() + Point(result->getContentSize().width / 2 + 14,
                                                          Director::getInstance()->getWinSize().height - result->getContentSize().height - 10) + Vec2(result->getContentSize().width + 20, 0));
    result->setTag(SPRITE_TAG::KEY_CHAIN_DELETE);

    auto label = Label::createWithTTF("キーチェーンの\nDID消去", FONT_NAME_2, 18);
    label->setAnchorPoint(Vec2(0.5, 0.5));
    label->setPosition(Point(result->getContentSize().width / 2, result->getContentSize().height / 2 - 9));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->addChild(label);
    return result;
}

void DevelopScene::btDialogCallback(ButtonIndex aIndex, int tag)
{
    if (tag == DIALOG_RESPONSE::DIALOG_KEY_CHAIN_DELETE_TAG) {
        if (aIndex == BT_YES) {
            Native::delOsDIDInKeychain();
        }
        return;
    } else if (tag == DIALOG_RESPONSE::DIALOG_RESOURCES_DELETE_TAG) {
        if (aIndex == BT_YES) {
            auto deleteMethod = new ResouseDeleteModel();
            deleteMethod->StartDeleteResouses();
        }
        return;
    }
}

MenuItemSprite* DevelopScene::createInAppPurcheseButton()
{
    auto result = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DevelopScene::onTapInAppParchase, this));
    result->setPosition(Point(VisibleRect::leftBottom() + Point(Director::getInstance()->getWinSize().width / 2 - result->getContentSize().width / 2 - 14, result->getContentSize().height - 10) + Vec2((result->getContentSize().width + 20) * 2, 0)));
    result->setTag(SPRITE_TAG::IN_APP_PARCHASE);
    auto label = Label::createWithTTF("課金処理", FONT_NAME_2, 18);
    label->setAnchorPoint(Vec2(0.5, 0.5));
    label->setPosition(Point(result->getContentSize().width / 2, result->getContentSize().height / 2 - 9));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->addChild(label);
    return result;
}

void DevelopScene::onTapInAppParchase(Ref* sender)
{
    if (DATAPLAYER->getPlayerID() == "") {
        CCLOG("[DevelopScene::onTapInAppParchase]PlayerIdがアプリ内にありません");
        return;
    }
    CCLOG("[DevelopScene::onTapInAppParchase] 課金開始");

    auto request = new RequestAPI;
    request->getPlayer([](Json* response) {
        Json* data = Json_getItem(Json_getItem(response, "get_player"), "data");
        PLAYERCONTROLLER->updatePlayerController(data);
        auto request = new RequestAPI;
        request->getPayTokenItem([](Json* response) {
            std::vector<std::shared_ptr<const Token> >token;
            std::string result = Json_getString(Json_getItem(response, "get_pay_item"), "result", "false");
            if (result == "true") {
                Json* mDatas = Json_getItem(Json_getItem(Json_getItem(response, "get_pay_item"), "data"), "pay_items");
                if (mDatas && mDatas->type == Json_Array) {
                    Json* child;
                    for (child = mDatas->child; child; child = child->next) {
                        std::shared_ptr<Token>model(new Token(child));
                        token.push_back(model);
                    }
                }
            }
            // std::string productName = "android.test.purchased";//Android マーケットは正常にアイテムが購入
            // std::string productName = "android.test.canceled";//Android マーケットは購入がキャンセル
            std::string productName = "android.test.refunded";             // Android マーケットは購入が払い戻し
            // std::string productName = "android.test.item_unavailable";//Android マーケットは購入しようとしたアイテムがアプリのプロダクトリストにリストされていなかったか
            // std::string productName = "token1";
            InAppPurchaseModel* model = new InAppPurchaseModel;
            model->buyProduct(productName, 20, token, [&]() {
                CCLOG("[DevelopScene::onTapInAppParchase] 課金完了");
            }, [&]() {
                CCLOG("[DevelopScene::onTapInAppParchase] 課金エラー");
            });
        });
    });
}
