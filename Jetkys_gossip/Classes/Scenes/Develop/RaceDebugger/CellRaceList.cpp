#include "CellRaceList.h"

CellRaceList::CellRaceList():
    RaceListAbstract()
{
    title = "Cell";
    tableCellObjects = {
        TableCellObject  {
            "[cell]:加速",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 5; i++){
                    gridCell += R"(
                        {"id":"1","kind":"1","score":"500"},
                        {"id":"2","kind":"1","score":"500"},
                        {"id":"3","kind":"1","score":"500"},
                        {"id":"4","kind":"2","score":"500"},
                        {"id":"5","kind":"2","score":"500"},
                        {"id":"6","kind":"2","score":"500"},
                        {"id":"7","kind":"3","score":"500"},
                        )";

                    for (int j = 0; j < 6; j++){
                        gridCell += R"({"id":"37","kind":"1","score":"0"},)";
                    }
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[cell]:Uターン",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 10; i++){
                    gridCell += R"(
                    {"id":"8","kind":"1","score":"-100"},
                    {"id":"9","kind":"1","score":"-100"},
                    {"id":"10","kind":"2","score":"-100"},
                    {"id":"11","kind":"2","score":"-100"},)";

                    for (int j = 0; j < 3; j++){
                        gridCell += R"({"id":"37","kind":"1","score":"0"},)";
                    }
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[cell]:減速 カーブ",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 10; i++){
                    gridCell += R"(
                    {"id":"12","kind":"1","score":"-100"},
                    {"id":"13","kind":"1","score":"-100"},
                    {"id":"14","kind":"2","score":"-100"},
                    {"id":"15","kind":"2","score":"-100"},)";

                    for (int j = 0; j < 3; j++){
                        gridCell += R"({"id":"37","kind":"1","score":"0"},)";
                    }
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[cell]:一時停止",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 10; i++){
                    gridCell += R"(
                    {"id":"16","kind":"1","score":"-100"},
                    {"id":"17","kind":"1","score":"-100"},)";

                    for (int j = 0; j < 1; j++){
                        gridCell += R"({"id":"37","kind":"1","score":"0"},)";
                    }
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[cell]:優先(もう一回)",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 50; i++){
                    gridCell += R"({"id":"18","kind":"1","score":"500"},)";
                    gridCell += R"({"id":"37","kind":"1","score":"0"},)";
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[cell]:徐行",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 20; i++){
                    for (int j = 0; j < 4; j++){
                        gridCell += R"({"id":"19","kind":"1","score":"-100"},)";
                    }

                    for (int j = 0; j < 1; j++){
                        gridCell += R"({"id":"37","kind":"1","score":"0"},)";
                    }
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[cell]:高速道路",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 20; i++){
                    for (int j = 0; j < 4; j++){
                        gridCell += R"({"id":"20","kind":"1","score":"-100"},)";
                    }

                    for (int j = 0; j < 2; j++){
                        gridCell += R"({"id":"37","kind":"1","score":"0"},)";
                    }
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[cell]:下り坂",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 10; i++){
                    gridCell += R"(
                    {"id":"21","kind":"1","score":"500"},
                    {"id":"22","kind":"1","score":"500"},
                    {"id":"23","kind":"2","score":"500"},
                    {"id":"24","kind":"2","score":"500"},
                    {"id":"25","kind":"3","score":"500"},)";

                    for (int j = 0; j < 3; j++){
                        gridCell += R"({"id":"37","kind":"1","score":"0"},)";
                    }
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[cell]:登り坂",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 10; i++){
                    gridCell += R"(
                    {"id":"26","kind":"1","score":"-100"},
                    {"id":"27","kind":"1","score":"-100"},
                    {"id":"28","kind":"2","score":"-100"},
                    {"id":"29","kind":"2","score":"-100"},
                    {"id":"30","kind":"3","score":"-100"},)";

                    for (int j = 0; j < 3; j++){
                        gridCell += R"({"id":"37","kind":"1","score":"0"},)";
                    }
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[cell]:item",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 50; i++){
                    gridCell += R"({"id":"31","kind":"1","score":"500"},)";
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[cell]:報酬",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 10; i++){
                    gridCell += R"(
                    {"id":"32","kind":"1","score":"1000"},
                    {"id":"33","kind":"1","score":"1000"},
                    {"id":"34","kind":"2","score":"1000"},
                    {"id":"35","kind":"2","score":"1000"},)";

                    for (int j = 0; j < 3; j++){
                        gridCell += R"({"id":"37","kind":"1","score":"0"},)";
                    }
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[cell]:チャージ",
            [this]()     {
                std::string gridCell = R"({"grid":[)";

                for (int i = 0; i < 30; i++){
                    gridCell += R"(
                    {"id":"36","kind":"1","score":"500"},)";

                    for (int j = 0; j < 1; j++){
                        gridCell += R"({"id":"37","kind":"1","score":"0"},)";
                    }
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(CourseModel::find(1),
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        }
    };
}























