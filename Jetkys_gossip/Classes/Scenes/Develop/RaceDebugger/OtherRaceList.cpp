#include "OtherRaceList.h"

OtherRaceList::OtherRaceList():
    RaceListAbstract()
{
    title = "Others";
    tableCellObjects = {
        TableCellObject  {
            "３ターン以内でクリア",
            [this]()     {
                std::string gridCell = R"({"grid":[)";
                for (int i = 0; i < 30; i++){
                    gridCell += R"(
                    {"id":"37","kind":"1","score":"0"},
                    {"id":"37","kind":"1","score":"0"},
                    )";
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");
                gridCell = "";

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(124);
                courseModel->setMaxNumberOfTurns(3);
                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  grid,
                                                  DEFAULT_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "ショートコース",
            [this]()     {
                std::string gridCell = R"({"grid":[)";
                for (int i = 0; i < 10; i++){
                    gridCell += R"(
                    {"id":"37","kind":"1","score":"0"},
                    )";
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");
                gridCell = "";

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(124);
                courseModel->setMaxNumberOfTurns(3);
                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  grid,
                                                  DEFAULT_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "ランダムセル",
            [this]()     {
                std::array<std::string, 11>cells ={
                    // 加速
                    R"({"id":"3","kind":"1","score":"500"},)",
                    // Uターン
                    R"({"id":"9","kind":"1","score":"-100"},)",
                    // 減速
                    R"({"id":"12","kind":"1","score":"-100"},)",
                    // 一時停止
                    R"({"id":"16","kind":"1","score":"-100"},)",
                    // もう一回
                    R"({"id":"18","kind":"1","score":"500"},)",
                    // 徐行
                    R"({"id":"19","kind":"1","score":"-100"},)",
                    //下り坂
                    R"({"id":"22","kind":"1","score":"500"},)",
                    //上り坂
                    R"({"id":"27","kind":"1","score":"-100"},)",
                    // アイテム
                    R"({"id":"31","kind":"1","score":"500"},)",
                    // スコア
                    R"({"id":"32","kind":"1","score":"1000"},)",
                    // チャージ
                    R"({"id":"36","kind":"1","score":"500"},)"
                };
                std::random_device rd;
                std::mt19937 mt(rd());
                std::uniform_int_distribution<int>grid_index(0, 10);

                std::string gridCell = R"({"grid":[)";
                for (int i = 0; i < 200; i++){
                    gridCell += cells[grid_index(mt)];
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");
                gridCell = "";

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(124);

                DEMERIT_CANCEL_CHARACTERS.push_back(CharacterModel::find(30));

                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  new TeamStatus(100000, 100000, 100000),
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "チュートリアルレース",
            [this]()     {
                std::string gridCell = "[{\"id\":\"37\",\"kind\":\"1\",\"score\":\"0\"},{\"id\":\"37\",\"kind\":\"1\",\"score\":\"0\"},{\"id\":\"2\",\"kind\":\"1\",\"score\":\"250\"},{\"id\":\"37\",\"kind\":\"1\",\"score\":\"0\"},{\"id\":\"31\",\"kind\":\"1\",\"score\":\"250\"},{\"id\":\"37\",\"kind\":\"1\",\"score\":\"0\"}]";
                Json* grid = Json_create(gridCell.c_str());

                std::vector<std::shared_ptr<const CharacterModel> >enemyCharacterModels;
                enemyCharacterModels.push_back(CharacterModel::find(1));
                enemyCharacterModels.push_back(CharacterModel::find(17));
                enemyCharacterModels.push_back(CharacterModel::find(20));

                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(14));

                UserDefault::getInstance()->setBoolForKey("Tutorial", true);
                Scene* scene = Scene::create();
                auto raceScene = RaceScene::create(CourseModel::find(1),
                                                   grid,
                                                   playerCharacters,
                                                   new TeamStatus(7500, 7500, 7500),
                                                   new HelpPlayer(0, 0, false, "", 1, 1),
                                                   enemyCharacterModels);
                scene->addChild(raceScene);
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "パトランプ",
            [this]()     {
                std::string gridCell = R"({"grid":[)";
                for (int i = 0; i < 100; i++){
                    gridCell += R"(
                    {"id":"31","kind":"1","score":"500"},
                    {"id":"9","kind":"1","score":"-100"},
                    )";
                }

                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");
                gridCell = "";

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(124);
                courseModel->setMaxNumberOfTurns(3);
                Scene* scene = Scene::create();
                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(30));
                playerCharacters.push_back(CharacterModel::find(30));
                playerCharacters.push_back(CharacterModel::find(22));
                playerCharacters.push_back(CharacterModel::find(22));
                scene->addChild(RaceScene::create(courseModel,
                                                  grid,
                                                  playerCharacters,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "加速とバック",
            [this]()     {
                std::string gridCell = R"({"grid":[)";
                for (int i = 0; i < 100; i++){
                    gridCell += R"(
                    {"id":"2","kind":"1","score":"500"},
                    {"id":"9","kind":"1","score":"-100"},
                    )";
                }

                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");
                gridCell = "";

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(124);
                courseModel->setMaxNumberOfTurns(3);
                Scene* scene = Scene::create();
                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(30));
                playerCharacters.push_back(CharacterModel::find(30));
                playerCharacters.push_back(CharacterModel::find(22));
                playerCharacters.push_back(CharacterModel::find(22));
                scene->addChild(RaceScene::create(courseModel,
                                                  grid,
                                                  DEMERIT_CANCEL_CHARACTERS,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        }
    };
}