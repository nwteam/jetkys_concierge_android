#include "LeaderSkillRaceList.h"


LeaderSkillRaceList::LeaderSkillRaceList():
    RaceListAbstract()
{
    title = "LeaderSkill";
    tableCellObjects = {
        TableCellObject  {
            "[L-SKILL]:低確率で出目が４倍",
            [this]()     {
                std::string gridCell = R"({"grid":[)";
                for (int i = 0; i < 50; i++){
                    gridCell += R"(
                    {"id":"37","kind":"1","score":"0"},
                    {"id":"37","kind":"1","score":"0"},
                    )";
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");
                gridCell = "";

                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(1));
                playerCharacters.push_back(CharacterModel::find(1));
                playerCharacters.push_back(CharacterModel::find(1));
                playerCharacters.push_back(CharacterModel::find(1));

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(84);
                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  grid,
                                                  playerCharacters,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[L-SKILL]:デメリットマスの影響を一定確率で無効化する",
            [this]()     {
                std::array<std::string, 11>cells ={
                    // Uターン
                    R"({"id":"9","kind":"1","score":"-100"},)",
                    // 減速
                    R"({"id":"12","kind":"1","score":"-100"},)",
                    // 一時停止
                    R"({"id":"16","kind":"1","score":"-100"},)",
                    // 徐行
                    R"({"id":"19","kind":"1","score":"-100"},)",
                    //上り坂
                    R"({"id":"27","kind":"1","score":"-100"},)",
                    // 無し
                    R"({"id":"37","kind":"1","score":"0"},)",
                };

                std::random_device rd;
                std::mt19937 mt(rd());
                std::uniform_int_distribution<int>grid_index(0, 10);

                std::string gridCell = R"({"grid":[)";
                for (int i = 0; i < 100; i++){
                    gridCell += cells[grid_index(mt)];
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");
                gridCell = "";

                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(8));
                playerCharacters.push_back(CharacterModel::find(8));
                playerCharacters.push_back(CharacterModel::find(8));
                playerCharacters.push_back(CharacterModel::find(8));

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(84);
                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  grid,
                                                  playerCharacters,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        }
    };
}
