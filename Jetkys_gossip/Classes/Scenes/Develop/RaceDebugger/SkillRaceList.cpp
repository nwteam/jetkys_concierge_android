#include "SkillRaceList.h"


SkillRaceList::SkillRaceList():
    RaceListAbstract()
{
    title = "Skill";

    tableCellObjects = {
        TableCellObject  {
            "[skill]:マイペース走行",
            [this]()     {
                std::array<std::string, 4>cells ={
                    // 加速
                    R"({"id":"3","kind":"1","score":"500"},)",
                    // 空
                    R"({"id":"37","kind":"1","score":"0"},)",
                    // もう一回
                    R"({"id":"18","kind":"1","score":"500"},)",
                };
                std::random_device rd;
                std::mt19937 mt(rd());
                std::uniform_int_distribution<int>grid_index(0, 3);

                std::string gridCell = R"({"grid":[)";
                for (int i = 0; i < 200; i++){
                    gridCell += cells[grid_index(mt)];
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(19));
                playerCharacters.push_back(CharacterModel::find(37));
                playerCharacters.push_back(CharacterModel::find(38));
                playerCharacters.push_back(CharacterModel::find(61));

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(84);
                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  grid,
                                                  playerCharacters,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[skill]:強制停車",
            [this]()     {
                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(14));
                playerCharacters.push_back(CharacterModel::find(14));
                playerCharacters.push_back(CharacterModel::find(14));
                playerCharacters.push_back(CharacterModel::find(14));

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(84);
                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  DEFAULT_GRID,
                                                  playerCharacters,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[skill]:アクセル　バック",
            [this]()     {
                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(30));
                playerCharacters.push_back(CharacterModel::find(30));
                playerCharacters.push_back(CharacterModel::find(22));
                playerCharacters.push_back(CharacterModel::find(22));

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(84);
                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  DEFAULT_GRID,
                                                  playerCharacters,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[skill]:スキルチャージ",
            [this]()     {
                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(4));
                playerCharacters.push_back(CharacterModel::find(8));

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(35);
                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  DEFAULT_GRID,
                                                  playerCharacters,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[skill]:プラスダイス",
            [this]()     {
                std::string gridCell = R"({"grid":[)";
                for (int i = 0; i < 200; i++){
                    // 徐行
                    gridCell += R"({"id":"19","kind":"1","score":"-100"},)";
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(77));
                playerCharacters.push_back(CharacterModel::find(49));
                playerCharacters.push_back(CharacterModel::find(23));
                playerCharacters.push_back(CharacterModel::find(10));

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(35);
                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  grid,
                                                  playerCharacters,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[skill]:クリティカルダイス",
            [this]()     {
                std::string gridCell = R"({"grid":[)";
                for (int i = 0; i < 200; i++){
                    // 徐行
                    gridCell += R"({"id":"19","kind":"1","score":"-100"},)";
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");

                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(28));
                playerCharacters.push_back(CharacterModel::find(62));
                playerCharacters.push_back(CharacterModel::find(28));
                playerCharacters.push_back(CharacterModel::find(62));

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(35);
                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  grid,
                                                  playerCharacters,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        },
        TableCellObject  {
            "[skill]:悪路耐性",
            [this]()     {
                std::array<std::string, 11>cells ={
                    // Uターン
                    R"({"id":"9","kind":"1","score":"-100"},)",
                    // 減速
                    R"({"id":"12","kind":"1","score":"-100"},)",
                    // 一時停止
                    R"({"id":"16","kind":"1","score":"-100"},)",
                    // 徐行
                    R"({"id":"19","kind":"1","score":"-100"},)",
                    //上り坂
                    R"({"id":"27","kind":"1","score":"-100"},)",
                    // 無し
                    R"({"id":"37","kind":"1","score":"0"},)",
                };
                std::random_device rd;
                std::mt19937 mt(rd());
                std::uniform_int_distribution<int>grid_index(0, 10);

                std::string gridCell = R"({"grid":[)";
                for (int i = 0; i < 100; i++){
                    gridCell += cells[grid_index(mt)];
                }
                gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
                Json* grid = Json_getItem(Json_create(gridCell.c_str()), "grid");
                gridCell = "";

                std::vector<std::shared_ptr<const CharacterModel> >playerCharacters;
                playerCharacters.push_back(CharacterModel::find(7));
                playerCharacters.push_back(CharacterModel::find(6));
                playerCharacters.push_back(CharacterModel::find(67));
                playerCharacters.push_back(CharacterModel::find(7));

                std::shared_ptr<CourseModel>courseModel = CourseModel::find(35);
                Scene* scene = Scene::create();
                scene->addChild(RaceScene::create(courseModel,
                                                  grid,
                                                  playerCharacters,
                                                  DEFAULT_TEAM_STATUS,
                                                  DEFAULT_HELP_PLAYER,
                                                  DEFAULT_CHARACTERS));
                Director::getInstance()->replaceScene(scene);
            }
        }
    };
}



