#include "RaceDebugger.h"

#include "DesignsInDevelop.h"
#include "DebuggerHeader.h"
#include "DebuggerSidebar.h"

#include "PlayerController.h"



using namespace SyanagoAPI;

RaceDebugger::RaceDebugger():
    _current(TEST_RACE_INDEX::SKILL)
{
    progress = new DefaultProgress();
    request = new RequestAPI(progress);
};

RaceDebugger::~RaceDebugger()
{
    delete progress;
    delete request;
    Director::getInstance()->getTextureCache()->removeAllTextures();
}

Scene* RaceDebugger::createScene()
{
    auto scene = Scene::create();
    scene->addChild(RaceDebugger::create());
    return scene;
}

bool RaceDebugger::init()
{
    if (!LayerColor::initWithColor(DesignsInDevelop::BACKGROUND)) {
        return false;
    }
    UserDefault::getInstance()->setBoolForKey("Tutorial ", false);

    addChild(DebuggerHeader::create("Race Debugger "));
    addChild(DebuggerSidebar::create(DebuggerSidebar::CURRENT_SCENE::RACE));
    return true;
}

void RaceDebugger::onEnterTransitionDidFinish()
{
    progress->onStart();
    request->getPlayer(CC_CALLBACK_1(RaceDebugger::onResponseGetPlayer, this));
}

void RaceDebugger::onResponseGetPlayer(Json* response)
{
    PLAYERCONTROLLER->updatePlayerController(Json_getItem(Json_getItem(response, "get_player"), "data"));
    showList();

    showButton(TEST_RACE_INDEX::SKILL);
    showButton(TEST_RACE_INDEX::LEADER_SKILL);
    showButton(TEST_RACE_INDEX::CELL);
    showButton(TEST_RACE_INDEX::UNIQUE);

    _buttons[TEST_RACE_INDEX::SKILL]->setEnabled(false);
    _buttons[TEST_RACE_INDEX::SKILL]->setBright(false);
}

void RaceDebugger::showButton(TEST_RACE_INDEX index)
{
    const int BUTTON_WIDTH = DesignsInDevelop::GRID * 7;
    _buttons[index] = ui::Button::create("tomato.png", "deep_gray.png", "deep_gray.png");
    _buttons[index]->setScale9Enabled(true);
    _buttons[index]->setContentSize(Size(BUTTON_WIDTH, DesignsInDevelop::GRID * 4));
    _buttons[index]->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    _buttons[index]->setPosition(Vec2(DebuggerSidebar::WIDTH + BUTTON_WIDTH * index,
                                      Director::getInstance()->getWinSize().height - DebuggerHeader::HEIGHT));
    _buttons[index]->setTitleFontSize(20);
    _buttons[index]->setTitleText(_items[index].title);
    _buttons[index]->addTouchEventListener([this, index](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            for (ui::Button* button : _buttons) {
                button->setEnabled(true);
                button->setBright(true);
            }
            _buttons[index]->setEnabled(false);
            _buttons[index]->setBright(false);
            _current = index;
            _tableView->reloadData();
        }
    });
    addChild(_buttons[index]);
}


void RaceDebugger::showList()
{
    if (_items[_current].tableCellObjects.size() > 0) {
        auto winSize = Director::getInstance()->getWinSize();
        _tableView = TableView::create(this, Size(CELL_SIZE.width, winSize.height - DebuggerHeader::HEIGHT + 1 - DesignsInDevelop::GRID * 4));
        _tableView->setPosition(Point(DebuggerSidebar::WIDTH, 0));
        _tableView->setDirection(TableView::Direction::VERTICAL);
        _tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
        _tableView->setDelegate(this);
        _tableView->reloadData();
        addChild(_tableView);
    }
}

cocos2d::Size RaceDebugger::cellSizeForTable(TableView* table)
{
    return CELL_SIZE;
}

TableViewCell* RaceDebugger::tableCellAtIndex(TableView* table, ssize_t idx)
{
    TableViewCell* cell = table->dequeueCell();
    cell = new TableViewCell;
    cell->autorelease();
    cell->setOpacity(100);
    cell->setName(_items[_current].tableCellObjects.at(idx).title);

    auto cellBackGround = Sprite::create();
    cellBackGround->setAnchorPoint(Vec2(0, 0));
    cellBackGround->setTextureRect(Rect(0, 0, CELL_SIZE.width, CELL_SIZE.height));
    cellBackGround->setColor(Color3B::WHITE);
    cell->addChild(cellBackGround);

    auto cellOutLine = Sprite::create();
    cellOutLine->setAnchorPoint(Vec2(0, 0));
    cellOutLine->setTextureRect(Rect(0, 0, CELL_SIZE.width, 1));
    cellOutLine->setColor(DesignsInDevelop::SUB);
    cell->addChild(cellOutLine);

    const int FONT_SIZE = 30;
    auto label = Label::createWithSystemFont(_items[_current].tableCellObjects.at(idx).title, "Arial", FONT_SIZE);
    label->setAnchorPoint(Vec2(0, 0));
    label->setPosition(Vec2(5, (CELL_SIZE.height - FONT_SIZE) / 2));
    label->setColor(DesignsInDevelop::STRING);
    cell->addChild(label);
    return cell;
}

ssize_t RaceDebugger::numberOfCellsInTableView(TableView* table)
{
    return _items[_current].tableCellObjects.size();
}

void RaceDebugger::tableCellTouched(TableView* table, TableViewCell* cell)
{
    LayerPriority* disableEventLayer = LayerPriority::create();
    addChild(disableEventLayer);
    RaceListAbstract::TableCellObject cellObject = _items[_current].tableCellObjects.at(cell->getIdx());
    cellObject.callback();
}



















