#include "RaceListAbstract.h"
#include "DesignsInDevelop.h"
#include "PlayerController.h"

RaceListAbstract::RaceListAbstract()
{
    DEFAULT_CHARACTERS = {CharacterModel::find(4),
                          CharacterModel::find(5),
                          CharacterModel::find(7)};
    DEMERIT_CANCEL_CHARACTERS = {CharacterModel::find(7),
                                 CharacterModel::find(6),
                                 CharacterModel::find(67), CharacterModel::find(30)};
    DEFAULT_TEAM_STATUS = new TeamStatus(7500, 7500, 7500);

    DEFAULT_HELP_PLAYER = new HelpPlayer(1, 19, false, "test", 1, 2);

    std::string gridCell = R"({"grid":[)";
    for (int i = 0; i < 100; i++) {
        gridCell += R"(
        {"id":"37","kind":"1","score":"0"},
        )";
    }
    gridCell += R"({"id":"37","kind":"1","score":"0"}]})";
    DEFAULT_GRID = Json_getItem(Json_create(gridCell.c_str()), "grid");
}

























