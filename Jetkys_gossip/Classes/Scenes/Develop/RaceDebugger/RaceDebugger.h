#ifndef __syanago__RaceDebugger__
#define __syanago__RaceDebugger__

#include <array>

#include "cocos2d.h"
#include "RequestAPI.h"
#include "DefaultProgress.h"
#include "ui/CocosGUI.h"
#include "CharacterModel.h"
#include "TeamStatus.h"
#include "RaceListAbstract.h"
#include "cocos-ext.h"

#include "SkillRaceList.h"
#include "LeaderSkillRaceList.h"
#include "CellRaceList.h"
#include "OtherRaceList.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace SyanagoAPI;

class RaceDebugger : public LayerColor, public TableViewDelegate ,public TableViewDataSource
{
public:
    RaceDebugger();
    ~RaceDebugger();
    static Scene* createScene();
    CREATE_FUNC(RaceDebugger);
    virtual bool init();
    
private:
    enum Z_ORDER {
        LIST = 0,
        BUTTON
    };
    enum TEST_RACE_INDEX {
        SKILL = 0,
        LEADER_SKILL,
        CELL,
        UNIQUE,
        SIZE
    };
    void onEnterTransitionDidFinish();
    void onResponseGetPlayer(Json* response);
    
    void showButton(TEST_RACE_INDEX index);
    
    void showList();
    
    virtual cocos2d::Size cellSizeForTable(TableView* table);
    virtual TableViewCell* tableCellAtIndex(TableView* table,ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(TableView* table);
    virtual void tableCellTouched(TableView* table,TableViewCell* cell);
    
    const cocos2d::Size CELL_SIZE = cocos2d::Size(Director::getInstance()->getWinSize().width - DebuggerSidebar::WIDTH, 100);
    
    RequestAPI* request;
    DefaultProgress* progress;
    
    std::array<ui::Button*,TEST_RACE_INDEX::SIZE>_buttons;
    
    TEST_RACE_INDEX _current;
    
    SkillRaceList skill;
    LeaderSkillRaceList leaderSkill;
    CellRaceList cellRaceList;
    OtherRaceList otherRaceList;
    const RaceListAbstract _items[TEST_RACE_INDEX::SIZE] = {
        skill,
        leaderSkill,
        cellRaceList,
        otherRaceList
    };
    TableView* _tableView;
};


#endif /* defined(__syanago__RaceDebugger__) */
