
#ifndef __syanago__RaceListAbstract__
#define __syanago__RaceListAbstract__

#include "cocos2d.h"

#include "DebuggerHeader.h"
#include "DebuggerSidebar.h"
#include "RaceScene.h"
#include "CharacterModel.h"
#include "TeamStatus.h"
#include "CourseModel.h"
#include <functional>
#include <array>

USING_NS_CC;



class RaceListAbstract
{
public:
    RaceListAbstract();
    virtual ~RaceListAbstract () {}
    std::string title;
    
    struct TableCellObject {
        std::string title;
        std::function<void()> callback;
    };
    std::vector<TableCellObject> tableCellObjects;
    
protected:
    std::vector<std::shared_ptr<const CharacterModel>> DEFAULT_CHARACTERS;
    std::vector<std::shared_ptr<const CharacterModel>> DEMERIT_CANCEL_CHARACTERS;
    HelpPlayer* DEFAULT_HELP_PLAYER;
    TeamStatus* DEFAULT_TEAM_STATUS;
    Json* DEFAULT_GRID;
};
    




#endif /* defined(__syanago__RaceListAbstract__) */
