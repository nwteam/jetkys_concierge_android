#ifndef __syanago__CharacterResourceViewer__
#define __syanago__CharacterResourceViewer__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "create_func.h"
#include "DesignsInDevelop.h"

USING_NS_CC;

class CharacterResourceViewer : public LayerColor, public create_func<CharacterResourceViewer>
{
public:
    static Scene* createScene(int characterId);
    using create_func::create;
    bool init(int characterId);
    
private:
    
    const int SCROLLVIEW_HEIGHT = 3000;
    const int IMAGE_AREA_HEIGHT = DesignsInDevelop::GRID * 52;
    const int VOICE_AREA_HEIGHT = DesignsInDevelop::GRID * 15;
    const int ANIMATION_AREA_HEIGHT = DesignsInDevelop::GRID * 21;
    const int MAINIMAGE_AREA_HEIGHT = DesignsInDevelop::GRID * 60;
    
    struct Image {
        std::string postFix;
        Vec2 position;
    };
    
    const Image images[6] = {
        Image{"r",   Vec2(DesignsInDevelop::GRID, IMAGE_AREA_HEIGHT - DesignsInDevelop::GRID* 5)},
        Image{"s",   Vec2(DesignsInDevelop::GRID * 13, IMAGE_AREA_HEIGHT - DesignsInDevelop::GRID* 5)},
        Image{"hk",  Vec2(DesignsInDevelop::GRID * 21, IMAGE_AREA_HEIGHT - DesignsInDevelop::GRID* 5)},
        Image{"cus", Vec2(DesignsInDevelop::GRID, IMAGE_AREA_HEIGHT - DesignsInDevelop::GRID* 21)},
        //Image{"i",   Vec2(DesignsInDevelop::GRID * 15, IMAGE_AREA_HEIGHT - DesignsInDevelop::GRID* 19)},
        Image{"co",  Vec2(DesignsInDevelop::GRID, IMAGE_AREA_HEIGHT - DesignsInDevelop::GRID * 37)},
        Image{"sdb", Vec2(DesignsInDevelop::GRID * 13, IMAGE_AREA_HEIGHT - DesignsInDevelop::GRID * 27)},
    };
    
    void showCharacterName();
    void setScrollView();
    void showImageArea();
    void showImage(const Image& image);
    void showVoiceArea();
    void showAnimationArea();
    void showMainImageArea();
    
    int SCROLLVIEW_WIDTH;
    
    ui::ScrollView* _scrollView;
    Sprite* _areaSprite;
    int _characterId;
};



#endif /* defined(__syanago__CharacterResourceViewer__) */
