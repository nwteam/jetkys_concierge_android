#include "ResourceDebugger.h"
#include "DevelopScene.h"
#include "DesignsInDevelop.h"
#include "API.h"
#include "LoadingScene.h"
#include "VisibleRect.h"
#include "ResouseDeleteModel.h"
#include "DebuggerHeader.h"
#include "DebuggerSidebar.h"
#include "CharacterResourceDebugger.h"

Scene* ResourceDebugger::createScene()
{
    Scene* scene = Scene::create();
    scene->addChild(ResourceDebugger::create());
    return scene;
}

bool ResourceDebugger::init()
{
    if (!LayerColor::initWithColor(DesignsInDevelop::BACKGROUND)) {
        return false;
    }

    addChild(DebuggerHeader::create("ResourceDebugger"));
    addChild(DebuggerSidebar::create(DebuggerSidebar::CURRENT_SCENE::RESOURCES));

    showArea(AREA {
        0,
        true,
        "Delete Resources",
        "端末内の古い画像、音声ファイルを削除",
        "Delete",
        [this]() {
            auto dialog = DialogView::createDialog("リソースデータを全て消去しますか？", "", 2, 0);
            dialog->_delegate = this;
            addChild(dialog, DIALOG_ZORDER);
        }
    });

    showArea(AREA {
        1,
        false,
        "Download Resources",
        "画像、音声ファイルをダウンロードしてから\nデバッガーを開始",
        "Download",
        [this]() {
            Director::getInstance()->replaceScene(LoadingScene::createScene(LoadingScene::SEQUENCE::FROM_DEBUG_RESOURCES));
        }
    });

    showArea(AREA {
        2,
        false,
        "Start Debugger",
        "デバッガーを開始",
        "Start",
        [this]() {
            Director::getInstance()->replaceScene(CharacterResourceDebugger::createScene());
        }
    });

    return true;
}


void ResourceDebugger::showArea(AREA area)
{
    const int AREA_WIDTH = DesignsInDevelop::GRID * 25;
    const int AREA_HEIGHT = DesignsInDevelop::GRID * 12;
    Sprite* areaSprite = Sprite::create();
    areaSprite->setTextureRect(Rect(0, 0, AREA_WIDTH, AREA_HEIGHT));
    areaSprite->setColor(DesignsInDevelop::SUB);
    areaSprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    areaSprite->setPosition(DebuggerSidebar::WIDTH + DesignsInDevelop::GRID,
                            Director::getInstance()->getWinSize().height - DebuggerHeader::HEIGHT - DesignsInDevelop::GRID * 2 - (DesignsInDevelop::GRID * 1 + AREA_HEIGHT) * area.index);

    Sprite* header = Sprite::create();
    header->setTextureRect(Rect(0, 0, AREA_WIDTH, DesignsInDevelop::GRID * 2));
    header->setColor(DesignsInDevelop::HIGHLIGHT);
    header->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    header->setPosition(0, DesignsInDevelop::GRID * 11);

    Label* label = Label::createWithSystemFont(area.title, "Meiryo", 15);
    label->setColor(Color3B::WHITE);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    label->setPosition(AREA_WIDTH - DesignsInDevelop::GRID, DesignsInDevelop::GRID);
    header->addChild(label);

    areaSprite->addChild(header);

    Label* description = Label::createWithSystemFont(area.description, "Meiryo", 20);
    description->setColor(Color3B::WHITE);
    description->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    description->setPosition(AREA_WIDTH / 2, DesignsInDevelop::GRID * 6);
    areaSprite->addChild(description);

    ui::Button* button = ui::Button::create(area.highlight ? "tomato.png" : "light_gray.png");
    button->setScale9Enabled(true);
    button->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    button->setPosition(Vec2(AREA_WIDTH / 2, DesignsInDevelop::GRID));
    button->setContentSize(Size(DesignsInDevelop::GRID * 11, DesignsInDevelop::GRID * 3));
    button->setTitleText(area.buttonTitle);
    button->setTitleFontSize(20);
    button->setTitleFontName("Meiryo");
    button->setTitleColor(area.highlight ? Color3B::WHITE : DesignsInDevelop::STRING);
    button->addTouchEventListener([area](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            area.callback();
        }
    });
    areaSprite->addChild(button);
    addChild(areaSprite);
}

void ResourceDebugger::btDialogCallback(ButtonIndex aIndex, int tag)
{
    if (aIndex == BT_YES) {
        auto deleteMethod = new ResouseDeleteModel();
        deleteMethod->StartDeleteResouses();
    }
}





















