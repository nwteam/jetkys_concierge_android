#include "CharacterResourceViewer.h"
#include "DebuggerHeader.h"
#include "CharacterResourceDebugger.h"


Scene* CharacterResourceViewer::createScene(int characterId)
{
    Scene* scene = Scene::create();
    scene->addChild(CharacterResourceViewer::create(characterId));
    return scene;
}

bool CharacterResourceViewer::init(int characterId)
{
    if (!LayerColor::initWithColor(DesignsInDevelop::BACKGROUND)) {
        return false;
    }

    _characterId = characterId;
    SCROLLVIEW_WIDTH = Director::getInstance()->getWinSize().width - DesignsInDevelop::GRID * 2;

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(StringUtils::format("%d_ca.plist", characterId));
    for (Image image : images) {
        std::string fileName = StringUtils::format("%d_%s.plist", characterId, image.postFix.c_str());
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(fileName);
    }


    addChild(DebuggerHeader::create("CharacterResourceViewer", []() {
        Director::getInstance()->replaceScene(CharacterResourceDebugger::createScene());
    }));

    showCharacterName();
    setScrollView();
    showImageArea();
    showVoiceArea();
    showAnimationArea();
    showMainImageArea();
    return true;
}

void CharacterResourceViewer::showCharacterName()
{
    std::shared_ptr<const CharacterModel>characterModel = CharacterModel::find(_characterId);
    Label* label = Label::createWithSystemFont(StringUtils::format("%d : %s", characterModel->getID(), characterModel->getCarName().c_str()), "Meiryo", 30);
    label->setColor(DesignsInDevelop::STRING);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    label->setPosition(DesignsInDevelop::GRID, Director::getInstance()->getWinSize().height - DebuggerHeader::HEIGHT  - DesignsInDevelop::GRID * 2);
    addChild(label);
}

void CharacterResourceViewer::setScrollView()
{
    _scrollView = ui::ScrollView::create();
    _scrollView->setContentSize(Size(SCROLLVIEW_WIDTH, Director::getInstance()->getWinSize().height - DebuggerHeader::HEIGHT - DesignsInDevelop::GRID * 4));
    _scrollView->setDirection(ui::ScrollView::Direction::VERTICAL);
    _scrollView->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    _scrollView->setPosition(Vec2(DesignsInDevelop::GRID, 0));
    _scrollView->setInnerContainerSize(Size(SCROLLVIEW_WIDTH, SCROLLVIEW_HEIGHT));
    addChild(_scrollView);
}

void CharacterResourceViewer::showImageArea()
{
    _areaSprite = Sprite::create();
    _areaSprite->setTextureRect(Rect(0, 0, SCROLLVIEW_WIDTH, IMAGE_AREA_HEIGHT));
    _areaSprite->setColor(DesignsInDevelop::SUB);
    _areaSprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    _areaSprite->setPosition(Vec2(0, SCROLLVIEW_HEIGHT));

    Label* label = Label::createWithSystemFont("Images", "Meiryo", 30);
    label->setColor(Color3B::WHITE);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    label->setPosition(DesignsInDevelop::GRID, IMAGE_AREA_HEIGHT - DesignsInDevelop::GRID * 2);
    _areaSprite->addChild(label);

    _scrollView->addChild(_areaSprite);
    for (Image image : images) {
        showImage(image);
    }
}

void CharacterResourceViewer::showImage(const Image& image)
{
    const std::string IMAGE_FILE_NAME = StringUtils::format("%d_%s.png", _characterId, image.postFix.c_str());

    Label* label = Label::createWithSystemFont(IMAGE_FILE_NAME, "Meiryo", 15);
    label->setColor(Color3B::WHITE);
    label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    label->setPosition(image.position);
    _areaSprite->addChild(label);

    if (SpriteFrameCache::getInstance()->getSpriteFrameByName(IMAGE_FILE_NAME) == NULL) {
        Label* nullLabel = Label::createWithSystemFont("NOT FOUND", "Meiryo", 30);
        nullLabel->setColor(Color3B::WHITE);
        nullLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        nullLabel->setPosition(image.position - Vec2(0, DesignsInDevelop::GRID * 6));
        _areaSprite->addChild(nullLabel);
        return;
    }
    Sprite* imageSprite = Sprite::createWithSpriteFrameName(IMAGE_FILE_NAME);
    imageSprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    imageSprite->setPosition(image.position);
    _areaSprite->addChild(imageSprite);
}

void CharacterResourceViewer::showVoiceArea()
{
    Sprite* areaSprite = Sprite::create();
    areaSprite->setTextureRect(Rect(0, 0, SCROLLVIEW_WIDTH, VOICE_AREA_HEIGHT));
    areaSprite->setColor(DesignsInDevelop::SUB);
    areaSprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    areaSprite->setPosition(Vec2(0, SCROLLVIEW_HEIGHT - IMAGE_AREA_HEIGHT - DesignsInDevelop::GRID * 2));

    Label* label = Label::createWithSystemFont("Voices", "Meiryo", 30);
    label->setColor(Color3B::WHITE);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    label->setPosition(DesignsInDevelop::GRID, VOICE_AREA_HEIGHT - DesignsInDevelop::GRID * 2);
    areaSprite->addChild(label);

    _scrollView->addChild(areaSprite);
}

void CharacterResourceViewer::showAnimationArea()
{
    Sprite* areaSprite = Sprite::create();
    areaSprite->setTextureRect(Rect(0, 0, SCROLLVIEW_WIDTH, ANIMATION_AREA_HEIGHT));
    areaSprite->setColor(DesignsInDevelop::SUB);
    areaSprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    areaSprite->setPosition(Vec2(0, SCROLLVIEW_HEIGHT - IMAGE_AREA_HEIGHT - VOICE_AREA_HEIGHT - DesignsInDevelop::GRID * 4));

    Label* label = Label::createWithSystemFont("Animation", "Meiryo", 30);
    label->setColor(Color3B::WHITE);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    label->setPosition(DesignsInDevelop::GRID, ANIMATION_AREA_HEIGHT - DesignsInDevelop::GRID * 2);
    areaSprite->addChild(label);
    _scrollView->addChild(areaSprite);

    auto animation = Animation::create();
    for (int i = 1; i <= 4; i++) {
        const std::string fileName = StringUtils::format("%d_sd%d.png", _characterId, i);
        if (SpriteFrameCache::getInstance()->getSpriteFrameByName(fileName) == NULL) {
            Label* nullLabel = Label::createWithSystemFont("NOT FOUND", "Meiryo", 30);
            nullLabel->setColor(Color3B::WHITE);
            nullLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
            nullLabel->setPosition(Vec2::ZERO);
            _areaSprite->addChild(nullLabel);
            return;
        }
        animation->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(fileName));
    }
    animation->setDelayPerUnit(0.1f);
    animation->setLoops(3);
    animation->setRestoreOriginalFrame(true);

    const std::string ANIMATION_KEY = "runningAnimation";
    AnimationCache::getInstance()->addAnimation(animation, ANIMATION_KEY);

    Sprite* character = Sprite::create();
    character->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    character->setPosition(SCROLLVIEW_WIDTH / 2 - DesignsInDevelop::GRID * 2.5, ANIMATION_AREA_HEIGHT - DesignsInDevelop::GRID * 6);
    character->runAction(RepeatForever::create(Animate::create(AnimationCache::getInstance()->getAnimation(ANIMATION_KEY))));
    areaSprite->addChild(character);
}

void CharacterResourceViewer::showMainImageArea()
{
    Sprite* areaSprite = Sprite::create();
    areaSprite->setTextureRect(Rect(0, 0, SCROLLVIEW_WIDTH, MAINIMAGE_AREA_HEIGHT));
    areaSprite->setColor(DesignsInDevelop::SUB);
    areaSprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    areaSprite->setPosition(Vec2(0, SCROLLVIEW_HEIGHT - IMAGE_AREA_HEIGHT - VOICE_AREA_HEIGHT - ANIMATION_AREA_HEIGHT - DesignsInDevelop::GRID * 6));

    Label* label = Label::createWithSystemFont("Main Image", "Meiryo", 30);
    label->setColor(Color3B::WHITE);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    label->setPosition(DesignsInDevelop::GRID, MAINIMAGE_AREA_HEIGHT - DesignsInDevelop::GRID * 2);
    areaSprite->addChild(label);

    const std::string fileName = StringUtils::format("%d_t.png", _characterId);

    if (SpriteFrameCache::getInstance()->getSpriteFrameByName(fileName.c_str()) == NULL) {
        Label* nullLabel = Label::createWithSystemFont("NOT FOUND", "Meiryo", 30);
        nullLabel->setColor(Color3B::WHITE);
        nullLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        nullLabel->setPosition(Vec2::ZERO);
        _areaSprite->addChild(nullLabel);
        return;
    }
    Sprite* imageSprite = Sprite::createWithSpriteFrameName(fileName);
    imageSprite->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    imageSprite->setPosition(Vec2(-50, MAINIMAGE_AREA_HEIGHT - DesignsInDevelop::GRID * 2));
    areaSprite->addChild(imageSprite);

    _scrollView->addChild(areaSprite);
}



















