#ifndef __syanago__ResourceDebugger__
#define __syanago__ResourceDebugger__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "DialogView.h"

USING_NS_CC;

class ResourceDebugger : public LayerColor, public DialogDelegate
{
public:
    static Scene* createScene();
    CREATE_FUNC(ResourceDebugger);
    virtual bool init();
private:
    struct AREA {
        int index;
        bool highlight;
        std::string title;
        std::string description;
        std::string buttonTitle;
        std::function<void()> callback;
    };
    
    void showArea(AREA area);
    
    void btDialogCallback(ButtonIndex aIndex, int tag);
    
};


#endif /* defined(__syanago__ResourceDebugger__) */
