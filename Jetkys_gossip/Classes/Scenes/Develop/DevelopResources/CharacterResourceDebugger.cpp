#include "CharacterResourceDebugger.h"
#include "ResourceDebugger.h"
#include "DesignsInDevelop.h"

#include "LayerPriority.h"
#include "SplashScene.h"
#include "MainScene.h"
#include "LoadingScene.h"
#include "TitleScene.h"
#include "RegisterScene.h"
#include "PasswordScene.h"
#include "GachaEffectLayer.h"
#include "GachaEffectModel.h"
#include "SelectFirstCarScene.h"
#include "AwakeResponseModel.h"
#include "ColectionDetail.h"
#include "CharacterResourceViewer.h"

Scene* CharacterResourceDebugger::createScene()
{
    Scene* scene = Scene::create();
    scene->addChild(CharacterResourceDebugger::create());
    return scene;
}

bool CharacterResourceDebugger::init()
{
    if (!LayerColor::initWithColor(DesignsInDevelop::BACKGROUND)) {
        return false;
    }
    addChild(DebuggerHeader::create("CharacterResourceDebugger", []() {
        Director::getInstance()->replaceScene(ResourceDebugger::createScene());
    }));
    addChild(DebuggerSidebar::create(DebuggerSidebar::CURRENT_SCENE::RESOURCES));

    _cellItems = CharacterModel::findAll();
    showList();

    return true;
}

void CharacterResourceDebugger::showList()
{
    // initSceneList();
    if (_cellItems.size() > 0) {
        auto winSize = Director::getInstance()->getWinSize();
        TableView* tableView = TableView::create(this, Size(CELL_SIZE.width, winSize.height - DebuggerHeader::HEIGHT + 1));
        tableView->setPosition(Point(DebuggerSidebar::WIDTH, 0));
        tableView->setDirection(TableView::Direction::VERTICAL);
        tableView->setVerticalFillOrder(TableView::VerticalFillOrder::BOTTOM_UP);
        tableView->setDelegate(this);
        tableView->reloadData();
        addChild(tableView);
    }
}

cocos2d::Size CharacterResourceDebugger::cellSizeForTable(TableView* table)
{
    return CELL_SIZE;
}

TableViewCell* CharacterResourceDebugger::tableCellAtIndex(TableView* table, ssize_t idx)
{
    TableViewCell* cell = table->dequeueCell();
    cell = new TableViewCell;
    cell->autorelease();
    cell->setOpacity(100);
    cell->setTag(_cellItems.at(idx)->getID());

    auto cellBackGround = Sprite::create();
    cellBackGround->setAnchorPoint(Vec2(0, 0));
    cellBackGround->setTextureRect(Rect(0, 0, CELL_SIZE.width, CELL_SIZE.height));
    cellBackGround->setColor(Color3B::WHITE);
    cell->addChild(cellBackGround);

    auto cellOutLine = Sprite::create();
    cellOutLine->setAnchorPoint(Vec2(0, 0));
    cellOutLine->setTextureRect(Rect(0, 0, CELL_SIZE.width, 1));
    cellOutLine->setColor(DesignsInDevelop::SUB);
    cell->addChild(cellOutLine);

    const int FONT_SIZE = 30;
    auto label = Label::createWithSystemFont(StringUtils::format("%d: %s", _cellItems.at(idx)->getID(), _cellItems.at(idx)->getCarName().c_str()), "Arial", FONT_SIZE);
    label->setAnchorPoint(Vec2(0, 0));
    label->setPosition(Vec2(5, (CELL_SIZE.height - FONT_SIZE) / 2));
    label->setColor(DesignsInDevelop::STRING);
    cell->addChild(label);
    return cell;
}

ssize_t CharacterResourceDebugger::numberOfCellsInTableView(TableView* table)
{
    return _cellItems.size();
}

void CharacterResourceDebugger::tableCellTouched(TableView* table, TableViewCell* cell)
{
    Director::getInstance()->replaceScene(CharacterResourceViewer::createScene(cell->getTag()));
}

























