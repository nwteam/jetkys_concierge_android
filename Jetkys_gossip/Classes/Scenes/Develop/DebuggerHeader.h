#ifndef __syanago__DebuggerHeader__
#define __syanago__DebuggerHeader__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class DebuggerHeader : public Sprite, public create_func<DebuggerHeader>
{
public:
    static const int HEIGHT;
    
    DebuggerHeader();
    
    using create_func::create;
    bool init(std::string title);
    typedef std::function<void()> BackButtonCallback;
    bool init(std::string title, const BackButtonCallback& callback);
private:
    void showBackButton();
    void showConnectionStatusLabel();
    void showConnectionStatus();
    void showTitle(std::string title);
    BackButtonCallback _backButtonCallback;
    
    
    
};



#endif /* defined(__syanago__DebuggerHeader__) */
