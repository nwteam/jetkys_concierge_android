#ifndef __syanago__DebuggerSidebar__
#define __syanago__DebuggerSidebar__


#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class DebuggerSidebar : public Sprite, create_func<DebuggerSidebar>
{
public:
    const static int WIDTH;
    
    enum CURRENT_SCENE {
        HOME = 0,
        SCENE,
        RACE,
        RESOURCES
    };
    DebuggerSidebar();
    using create_func::create;
    bool init(CURRENT_SCENE currentScene);
    
private:
    void showButton(CURRENT_SCENE scene, std::string title, const std::function<void()>& callback);
    void showSeparateLines();
    void showStartButton();
    
    CURRENT_SCENE _currentScene;
    const int HEIGHT;
};

#endif /* defined(__syanago__DebuggerSidebar__) */
