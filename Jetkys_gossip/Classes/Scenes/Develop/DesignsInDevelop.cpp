#include "DesignsInDevelop.h"


const int DesignsInDevelop::GRID (20);

/*
   $color1: #114b5f; //rgba(17, 75, 95, 100)
   $color2: #028090; //rgba(2, 128, 144, 100)
   $color3: #e4fde1; //rgba(228, 253, 225, 100)
   $color4: #456990; //rgba(69, 105, 144, 100)
   $color5: #f45b69; //rgba(244, 91, 105, 100)
 */

const Color4B DesignsInDevelop::BACKGROUND (255, 255, 255, 255);
const Color3B DesignsInDevelop::HEADER (221, 221, 221);
const Color3B DesignsInDevelop::SIDEBAR (17, 17, 17);
const Color3B DesignsInDevelop::SUB (68, 68, 68);
const Color3B DesignsInDevelop::HIGHLIGHT(170, 170, 170);
// #ff6347
const Color3B DesignsInDevelop::TOMATO(255, 99, 71);
const Color3B DesignsInDevelop::STRING (100, 100, 100);



// TODO: delete
const Color3B DesignsInDevelop::CELL_BACKGROUND (228, 253, 225);
const Color3B DesignsInDevelop::BORDER (69, 105, 144);
const Color3B DesignsInDevelop::BACK_BUTTON (244, 91, 105);



const std::string DesignsInDevelop::NORMAL_BUTTON("gacha_dialog_button.png");

const Vec2 DesignsInDevelop::getLeftTopPosition()
{
    Size size = Director::getInstance()->getWinSize();
    return Vec2(0, size.height);
}

const Vec2 DesignsInDevelop::getCenterPosition()
{
    Size size = Director::getInstance()->getWinSize();
    return Vec2(size.width / 2, size.height / 2);
}
