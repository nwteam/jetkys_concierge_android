#ifndef __syanago__SceneDebugger__
#define __syanago__SceneDebugger__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "DebuggerHeader.h"
#include "DebuggerSidebar.h"

USING_NS_CC;
USING_NS_CC_EXT;

class SceneDebugger : public LayerColor, public TableViewDelegate ,public TableViewDataSource
{
public:
    static Scene* createScene();
    CREATE_FUNC(SceneDebugger);
    virtual bool init();
    
    virtual cocos2d::Size cellSizeForTable(TableView* table);
    virtual TableViewCell* tableCellAtIndex(TableView* table,ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(TableView* table);
    
private:
    void initSceneList();
    void showSceneList();
    
    virtual void tableCellTouched(TableView* table,TableViewCell* cell);
    
    const cocos2d::Size CELL_SIZE = cocos2d::Size(Director::getInstance()->getWinSize().width - DebuggerSidebar::WIDTH, 100);
    std::vector<std::string> _testSceneNames;
};


#endif /* defined(__syanago__SceneDebugger__) */
