#include "SceneDebugger.h"
#include "DesignsInDevelop.h"

#include "LayerPriority.h"
#include "SplashScene.h"
#include "MainScene.h"
#include "LoadingScene.h"
#include "TitleScene.h"
#include "RegisterScene.h"
#include "PasswordScene.h"
#include "GachaEffectLayer.h"
#include "GachaEffectModel.h"
#include "SelectFirstCarScene.h"
#include "AwakeResponseModel.h"
#include "ColectionDetail.h"


Scene* SceneDebugger::createScene()
{
    Scene* scene = Scene::create();
    scene->addChild(SceneDebugger::create());
    return scene;
}

bool SceneDebugger::init()
{
    if (!LayerColor::initWithColor(DesignsInDevelop::BACKGROUND)) {
        return false;
    }
    addChild(DebuggerHeader::create("Scene Debugger"));
    addChild(DebuggerSidebar::create(DebuggerSidebar::SCENE));

    initSceneList();
    showSceneList();

    return true;
}

void SceneDebugger::initSceneList()
{
    _testSceneNames.push_back("「車なご」ゲームスタート");
    _testSceneNames.push_back("LoadingScene");
    _testSceneNames.push_back("MainScene");
    _testSceneNames.push_back("TitleScene");
    _testSceneNames.push_back("RegisterScene");
    _testSceneNames.push_back("PasswordScene");
    _testSceneNames.push_back("MainScene with Tutorial");
    _testSceneNames.push_back("HeaderLayer");
    _testSceneNames.push_back("GachaLayer");
    _testSceneNames.push_back("GachaEffect");
    _testSceneNames.push_back("DropEffect");
    _testSceneNames.push_back("AwakeResponseModel");
    _testSceneNames.push_back("SelectFirstCarScene");
    _testSceneNames.push_back("ColectionDetail");
}

void SceneDebugger::showSceneList()
{
    // initSceneList();
    if (_testSceneNames.size() > 0) {
        auto winSize = Director::getInstance()->getWinSize();
        TableView* tableView = TableView::create(this, Size(CELL_SIZE.width, winSize.height - DebuggerHeader::HEIGHT + 1));
        tableView->setPosition(Point(DebuggerSidebar::WIDTH, 0));
        tableView->setDirection(TableView::Direction::VERTICAL);
        tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
        tableView->setDelegate(this);
        tableView->reloadData();
        addChild(tableView);
    }
}

cocos2d::Size SceneDebugger::cellSizeForTable(TableView* table)
{
    return CELL_SIZE;
}

TableViewCell* SceneDebugger::tableCellAtIndex(TableView* table, ssize_t idx)
{
    TableViewCell* cell = table->dequeueCell();
    cell = new TableViewCell;
    cell->autorelease();
    cell->setOpacity(100);
    cell->setName(_testSceneNames.at(idx));

    auto cellBackGround = Sprite::create();
    cellBackGround->setAnchorPoint(Vec2(0, 0));
    cellBackGround->setTextureRect(Rect(0, 0, CELL_SIZE.width, CELL_SIZE.height));
    cellBackGround->setColor(Color3B::WHITE);
    cell->addChild(cellBackGround);

    auto cellOutLine = Sprite::create();
    cellOutLine->setAnchorPoint(Vec2(0, 0));
    cellOutLine->setTextureRect(Rect(0, 0, CELL_SIZE.width, 1));
    cellOutLine->setColor(DesignsInDevelop::SUB);
    cell->addChild(cellOutLine);

    const int FONT_SIZE = 30;
    auto label = Label::createWithSystemFont(_testSceneNames.at(idx).c_str(), "Arial", FONT_SIZE);
    label->setAnchorPoint(Vec2(0, 0));
    label->setPosition(Vec2(5, (CELL_SIZE.height - FONT_SIZE) / 2));
    label->setColor(DesignsInDevelop::STRING);
    cell->addChild(label);
    return cell;
}

ssize_t SceneDebugger::numberOfCellsInTableView(TableView* table)
{
    return _testSceneNames.size();
}

void SceneDebugger::tableCellTouched(TableView* table, TableViewCell* cell)
{
    LayerPriority* disableEventLayer = LayerPriority::create();
    addChild(disableEventLayer);

    Scene* scene;
    if (cell->getName() == "「車なご」ゲームスタート") {
        scene = SplashScene::createScene();
    } else if (cell->getName() == "MainScene") {
        UserDefault::getInstance()->setBoolForKey("NewPlayer", true);
        scene = MainScene::createScene();
    } else if (cell->getName() == "LoadingScene") {
        scene = LoadingScene::createScene(LoadingScene::SEQUENCE::NOTING_TO_DO);
    } else if (cell->getName() == "TitleScene") {
        scene = TitleScene::createScene();
    } else if (cell->getName() == "RegisterScene") {
        scene = RegisterScene::createScene();
    } else if (cell->getName() == "PasswordScene") {
        scene = PasswordScene::createScene();
    } else if (cell->getName() == "MainScene with Tutorial") {
        UserDefault::getInstance()->setBoolForKey("NewPlayer", false);
        scene = MainScene::createScene();
    } else if (cell->getName() == "HeaderLayer") {
        auto request = new RequestAPI;
        request->getPlayer([](Json* response) {
            Json* data = Json_getItem(Json_getItem(response, "get_player"), "data");
            PLAYERCONTROLLER->updatePlayerController(data);
            Scene* scene = Scene::create();
            auto whileLayer = LayerColor::create(Color4B::WHITE);
            scene->addChild(whileLayer, 1);
            auto headerLayer = HeaderLayer::create();
            scene->addChild(headerLayer, 2);
            Director::getInstance()->replaceScene(scene);
        });
        return;
    } else if (cell->getName() == "GachaLayer") {
        auto request = new RequestAPI;
        request->getPlayer([](Json* response) {
            Json* data = Json_getItem(Json_getItem(response, "get_player"), "data");
            PLAYERCONTROLLER->updatePlayerController(data);
            Scene* scene = Scene::create();
            auto gachaLayer = GachaLayer::create();
            scene->addChild(gachaLayer);
            Director::getInstance()->replaceScene(scene);
        });
        return;
    } else if (cell->getName() == "GachaEffect") {
        scene = Scene::create();
        std::shared_ptr<GachaEffectModel>model(new GachaEffectModel(5, PREMIUM));
        auto gachaEffectLayer = GachaEffectLayer::create(model, [this]() {
            auto scene = SceneDebugger::createScene();
            Director::getInstance()->replaceScene(scene);
        });
        scene->addChild(gachaEffectLayer);
    } else if (cell->getName() == "DropEffect") {
        scene = Scene::create();
        auto dropEffectLayer = DropEffectLayer::create(DROP_CHARACTER, 2, true, [this]() {
            auto scene = SceneDebugger::createScene();
            Director::getInstance()->replaceScene(scene);
        });
        scene->addChild(dropEffectLayer);
    } else if (cell->getName() == "AwakeResponseModel") {
        Json* response = Json_create(R"({
                                     "awake_character": {
                                         "data": {
                                             "awoken_player_character": {
                                                 "characters_id": "24",
                                                 "dear_value": "1",
                                                 "decision": "1000",
                                                 "dice_value1": "2",
                                                 "dice_value2": "2",
                                                 "dice_value3": "3",
                                                 "dice_value4": "3",
                                                 "dice_value5": "4",
                                                 "dice_value6": "1",
                                                 "dice_value7": "0",
                                                 "dice_value8": "0",
                                                 "dice_value9": "0",
                                                 "exercise": "1000",
                                                 "exp": "0",
                                                 "id": "1410",
                                                 "level": "1",
                                                 "reaction": "1900"
                                             },
                                             "deleted_player_character_ids": [
                                                                              "1542"
                                                                              ],
                                             "deleted_player_parts_ids": [],
                                             "free_coin": "0"
                                         },
                                         "result": "true"
                                     }
                                     })");
        std::shared_ptr<AwakeResponseModel>model(new AwakeResponseModel(response));
        CCLOG("playerCharacterId=%d", model->getPlayerCharacterId());
        CCLOG("masterCharacterId=%d", model->getMasterCharacterId());
        return;
    } else if (cell->getName() == "SelectFirstCarScene") {
        scene = SelectFirstCarScene::createScene();
    } else if (cell->getName() == "ColectionDetail") {
        scene = Scene::create();
        std::shared_ptr<CharacterModel>model(CharacterModel::find(8));
        auto colectionDetaileLayer = ColectionDetail::create(model, []() {});
        scene->addChild(colectionDetaileLayer);
    }
    Director::getInstance()->replaceScene(scene);
}
























