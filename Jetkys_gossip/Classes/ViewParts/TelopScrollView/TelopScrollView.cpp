#include "TelopScrollView.h"
#include "FontDefines.h"

TelopScrollView* TelopScrollView::initScrollTelop(std::string telopMessage, int fontSize, float baseWidthSize, float baseHeightSize, Color3B fontColor)
{
    auto telop = new TelopScrollView();
    telop->createTelop(telopMessage, fontSize, baseWidthSize, baseHeightSize, fontColor);
    telop->autorelease();
    return telop;
}

void TelopScrollView::createTelop(std::string message, int fontSize, float baseWidthSize, float baseHeightSize, Color3B fontColor)
{
    _subjectLabel = Label::createWithTTF(message, FONT_NAME_2, fontSize);
    _subjectLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    _subjectLabel->setAnchorPoint(Vec2(0, 0));
    _subjectLabel->setColor(fontColor);
    if (_subjectLabel->getContentSize().width <= baseWidthSize) {
        _subjectLabel->setPosition(Point(0, -fontSize));
        addChild(_subjectLabel);
    } else {
        _scrollTerop = cocos2d::extension::ScrollView::create();
        _scrollTerop->setDirection(cocos2d::extension::ScrollView::Direction::HORIZONTAL);
        _scrollTerop->setViewSize(Size(baseWidthSize, baseHeightSize));
        _scrollTerop->setContainer(_subjectLabel);
        addChild(_scrollTerop);
        _scrollTerop->setPosition(Point::ZERO);
        _scrollTerop->setContentOffset(Point(0, -fontSize));
        _startPosition = Point(baseWidthSize + 10, -fontSize);
        _scrollTerop->setAnchorPoint(Vec2(0, 0));


        runAction(Sequence::create(DelayTime::create(2.0),
                                         CallFuncN::create([&](Ref* sender) {
            runAction(RepeatForever::create(Sequence::create(DelayTime::create(0.01f),
                                                                   CallFuncN::create([&](Ref* sender) {
                _scrollTerop->setContentOffset(_scrollTerop->getContentOffset() - Vec2(1, 0));
                if (_scrollTerop->getContentOffset().x < -50 - _scrollTerop->getContentSize().width) {
                    _scrollTerop->setContentOffset(_startPosition);
                }
            }), NULL)));
        }), NULL));
    }
}

void TelopScrollView::setFontShadow(Color3B shadowColor, int shadowSize)
{
    if (!_subjectLabel) {
        return;
    }
    _subjectLabel->enableShadow(Color4B(shadowColor), Size(shadowSize, -shadowSize));
}
