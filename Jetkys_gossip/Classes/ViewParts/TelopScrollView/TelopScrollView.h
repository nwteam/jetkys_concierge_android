#ifndef __syanago__TelopScrollView__
#define __syanago__TelopScrollView__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;

class TelopScrollView : public cocos2d::Layer
{
public:
    /*
     *
     スクロールするテロップを作成する事ができる
     telopMessage   :テロップに表示する文字
     fontSize       :フォントサイズ
     baseWidthSize  :テロップの表示する横幅
     baseHeightSize :テロプの表示する立て幅
     fontColor      :フォントカラー(Color3B)
     *
     **/
    static TelopScrollView* initScrollTelop(std::string telopMessage, int fontSize, float baseWidthSize, float baseHeightSize, Color3B fontColor);
    
    /*
     *
     スクロールする文字にシャドウを付ける事が出来ます
     shadowColor    :影の色
     hadowSize      :影の大きさ
     *
     **/
    void setFontShadow(Color3B shadowColor, int shadowSize);
    
private:
    cocos2d::extension::ScrollView* _scrollTerop;
    
    void createTelop(std::string message, int fontSize, float baseWidthSize, float baseHeightSize, Color3B fontColor);
    Point _startPosition;
    Label* _subjectLabel;
};


#endif /* defined(__syanago__TelopScrollView__) */
