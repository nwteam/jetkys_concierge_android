#include "ScrollBar.h"

USING_NS_CC;

ScrollBar* ScrollBar::initScrollBar(float baseScrollBarSize, float baseScrollSize, float innarScrollSize, float maxOffset)
{
    auto scrollBar = new ScrollBar();
    scrollBar->setScrollContentsHeightSize(baseScrollBarSize, baseScrollSize, innarScrollSize, maxOffset);
    scrollBar->autorelease();

    return scrollBar;
}

void ScrollBar::setScrollContentsHeightSize(float baseScrollBarSize, float baseScrollSize, float innarScrollSize, float maxOffset)
{
    ////CCLOG("スクロールバーの長さ: %f,　スクロールビューの高さ: %f, コンテンツの高さ: %f, オフセットの最大数: %f" , baseScrollBarSize, baseScrollSize, innarScrollSize, maxOffset);
    if (innarScrollSize <= baseScrollSize) {
        innarScrollSize = baseScrollSize;
    }

    _scrollbarBase = makeSprite("scroll_bar_base.png");
    _scrollPoint = makeSprite("scroll_bar.png");
    addChild(_scrollbarBase);
    addChild(_scrollPoint);

    // バーの基本X座標
    float PositionX = 13 + 6.5;
    _scrollbarBase->setPositionX(PositionX);
    _scrollPoint->setPositionX(PositionX);

    // init
    _maxOffsetPoint = maxOffset;
    _lockFlag = false;

    // バーの高さ
    float inBarHeight = (baseScrollSize * baseScrollBarSize) / innarScrollSize;
    _pointScrollBarSize = inBarHeight;
    _baseScrollBarSize = baseScrollBarSize;

    // バーの拡大サイズ
    float baseScale =  baseScrollBarSize / _scrollbarBase->getContentSize().height;
    float pointScale = inBarHeight / _scrollPoint->getContentSize().height;

    if (pointScale >= baseScale) {
        pointScale = baseScale;
        _lockFlag = true;
    }
    _scrollbarBase->setScaleY(baseScale);
    _scrollPoint->setScaleY(pointScale);
    ////CCLOG("基礎のスケール:%f, ポイントのスケール:%f",baseScale, pointScale);

    // スクロールバーポイントの初期の場所指定
    _scrollPoint->setPositionY(_scrollbarBase->getPosition().y + baseScrollBarSize / 2 - inBarHeight / 2);
    _basePosi = _scrollPoint->getPosition().y;

    if (innarScrollSize == baseScrollSize) {
        _scrollbarBase->setVisible(false);
        _scrollPoint->setVisible(false);
    }
}


void ScrollBar::updateScrollPoint(float contentOffsetY)
{
    if (_lockFlag == false) {
        if (contentOffsetY >= _maxOffsetPoint) {
            _scrollPoint->setPositionY(_basePosi - ((_maxOffsetPoint - contentOffsetY) * (_baseScrollBarSize - _pointScrollBarSize)) / _maxOffsetPoint);
        }
    }
}