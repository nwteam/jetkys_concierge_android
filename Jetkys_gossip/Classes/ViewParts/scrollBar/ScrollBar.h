#ifndef __syanago__ScrollBar__
#define __syanago__ScrollBar__

#include "cocos2d.h"
#include "jCommon.h"

class ScrollBar : public cocos2d::Layer
{
public:
    static ScrollBar* initScrollBar(float baseScrollBarSize, float baseScrollSize, float innarScrollSize, float maxOffset);
    
    void setScrollContentsHeightSize(float baseScrollBarSize, float baseScrollSize, float innarScrollSize, float maxOffset);
    void updateScrollPoint(float contentOfsetY);
    
private:
    Sprite* _scrollbarBase;
    Sprite* _scrollPoint;
    
    float _maxOffsetPoint;
    float _baseScrollBarSize;
    float _pointScrollBarSize;
    
    float _basePosi;
    
    bool _lockFlag;
    
};

#endif /* defined(__syanago__ScrollBar__) */
