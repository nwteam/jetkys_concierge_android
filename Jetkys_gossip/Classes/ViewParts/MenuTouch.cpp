#include "MenuTouch.h"

Vector<MenuItem*>MenuTouch::itemsSelected = Vector<MenuItem*>();

MenuTouch* MenuTouch::create(MenuItem* item, ...)
{
    va_list args;
    va_start(args, item);

    Vector<MenuItem*>items;
    if (item) {
        items.pushBack(item);
        MenuItem* i = va_arg(args, MenuItem*);
        while (i) {
            items.pushBack(i);
            i = va_arg(args, MenuItem*);
        }
    }

    return MenuTouch::createWithArray(items);
}

MenuTouch* MenuTouch::createWithArray(const Vector<MenuItem*>& arrayOfItems)
{
    auto ret = new MenuTouch();
    if (ret && ret->initWithArray(arrayOfItems)) {
        ret->autorelease();
    } else   {
        CC_SAFE_DELETE(ret);
    }

    return ret;
}

bool MenuTouch::onTouchBegan(Touch* touch, Event* unused_event)
{
    ////CCLOG("MenuPriority::onTouchBegan");
    if (Menu::onTouchBegan(touch, unused_event)) {
        itemsSelected.pushBack(_selectedItem);
        ////CCLOG("Began: Items selected number: %d", (int)itemsSelected.size());
        return true;
    }
    return false;
}

void MenuTouch::onTouchMoved(Touch* touch, Event* unused_event)
{
    ////CCLOG("MenuPriority::onTouchMoved");
    if (itemsSelected.contains(_selectedItem)) {
        MenuItem* pre = _selectedItem;
        Menu::onTouchMoved(touch, unused_event);
        if (!_selectedItem) {
            itemsSelected.eraseObject(pre);
        }
    }
}

void MenuTouch::onTouchEnded(Touch* touch, Event* unused_event)
{
    ////CCLOG("MenuPriority::onTouchEnded");
    ////CCLOG("Ended : Items selected number: %d", (int)itemsSelected.size());

    if (_selectedItem && itemsSelected.contains(_selectedItem)) {
        Menu::onTouchEnded(touch, unused_event);
        // itemsSelected.eraseObject(_selectedItem);

        itemsSelected.clear();
    } else {
        Menu::onTouchCancelled(touch, unused_event);
    }
}

void MenuTouch::onTouchCancelled(Touch* touch, Event* unused_event)
{
    ////CCLOG("MenuPriority::onTouchCancelled");

    Menu::onTouchCancelled(touch, unused_event);
    if (_selectedItem) {
        itemsSelected.eraseObject(_selectedItem);
    }

    ////CCLOG("Canceled: Items selected number: %d", (int)itemsSelected.size());
}
