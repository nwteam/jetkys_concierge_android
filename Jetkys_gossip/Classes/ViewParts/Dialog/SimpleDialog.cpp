#include "SimpleDialog.h"
#include "FontDefines.h"

bool SimpleDialog::init(std::string title, std::string description)
{
    if (!DialogAbstract::init()) {
        return false;
    }
    showTitle(title);
    showDescription(description);
    showCloseButton();

    return true;
}

void SimpleDialog::showCloseButton()
{
    Size winSize = Director::getInstance()->getWinSize();
    ui::Button* button = ui::Button::create("gacha_dialog_button.png");
    button->setPosition(Vec2(winSize.width / 2, winSize.height / 2) + Vec2(0, -background->getContentSize().height / 2 + 30 + button->getContentSize().height / 2));
    button->addTouchEventListener(CC_CALLBACK_2(SimpleDialog::onTapCloseButton, this));

    auto label = Label::createWithTTF("OK", FONT_NAME_2, 36);
    button->addChild(label);
    label->setPosition(Point(button->getContentSize().width / 2, button->getContentSize().height / 2 - 18));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(button);
}




