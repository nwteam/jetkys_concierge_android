#ifndef __syanago__DialogAbstract__
#define __syanago__DialogAbstract__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "DialogDefine.h"

USING_NS_CC;

class DialogAbstract: public Layer
{
protected:
    typedef std::function<void()> DialogCallback;
    bool init(std::string fileName = "dialog_base.png");
    void showTitle(std::string title);
    void showDescription(std::string description);
    void onTapCloseButton(Ref* sender, ui::Widget::TouchEventType type);
    
    Sprite* background;
private:
    void disableEvents();
    void showDialogBase();
    
    EventListenerTouchOneByOne* _listener;
    std::string backgroundFileName;
};




#endif /* defined(__syanago__DialogAbstract__) */
