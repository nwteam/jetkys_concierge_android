#ifndef __syanago__DialogWithCallbackAndCloseButton__
#define __syanago__DialogWithCallbackAndCloseButton__

#include "DialogAbstract.h"
#include "create_func.h"


class DialogWithCallbackAndCloseButton: public DialogAbstract, public create_func<DialogWithCallbackAndCloseButton>
{
public:
    using create_func::create;
    bool init(std::string title, std::string description, const DialogCallback& callback);
private:
    void showCloseButton();
    void showCallbackButton(const DialogCallback& callback);
};

#endif /* defined(__syanago__DialogWithCallbackAndCloseButton__) */
