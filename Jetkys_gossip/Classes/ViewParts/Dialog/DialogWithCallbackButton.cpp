#include "DialogWithCallbackButton.h"
#include "FontDefines.h"

bool DialogWithCallbackButton::init(std::string title, std::string description, const DialogCallback& callback)
{
    if (!DialogAbstract::init()) {
        return false;
    }
    showTitle(title);
    showDescription(description);
    showCallbackButton(callback);
    return true;
}

void DialogWithCallbackButton::showCallbackButton(const DialogCallback &callback)
{
    Size winSize = Director::getInstance()->getWinSize();
    ui::Button* button = ui::Button::create("gacha_dialog_button.png");
    button->setPosition(Vec2(winSize.width / 2, winSize.height / 2) + Vec2(0, -background->getContentSize().height / 2 + 30 + button->getContentSize().height / 2));
    button->addTouchEventListener([callback](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            callback();
        }
    });

    auto label = Label::createWithTTF("OK", FONT_NAME_2, 36);
    button->addChild(label);
    label->setPosition(Point(button->getContentSize().width / 2, button->getContentSize().height / 2 - 18));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(button);
}



