#ifndef __syanago__DialogWithCallbackButton__
#define __syanago__DialogWithCallbackButton__

#include "DialogAbstract.h"
#include "create_func.h"

class DialogWithCallbackButton : public DialogAbstract, public create_func<DialogWithCallbackButton>
{
public:
    using create_func::create;
    bool init(std::string title, std::string description, const DialogCallback& callback);
private:
    void showCallbackButton(const DialogCallback& callback);
};

#endif /* defined(__syanago__DialogWithCallbackButton__) */
