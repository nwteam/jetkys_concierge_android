#ifndef __syanago__DialogWithDoubleCallbackButton__
#define __syanago__DialogWithDoubleCallbackButton__

#include "DialogAbstract.h"
#include "create_func.h"

class DialogWithDoubleCallbackButton : public DialogAbstract, public create_func<DialogWithDoubleCallbackButton>
{
public:
    using create_func::create;
    bool init(std::string title, std::string description, const DialogCallback& callbackLeft, const DialogCallback& callbackRight);
private:
    void showLeftButton(const DialogCallback& callback);
    void showRightButton(const DialogCallback& callback);
};


#endif /* defined(__syanago__DialogWithDoubleCallbackButton__) */
