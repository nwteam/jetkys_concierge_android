#include "DialogAbstract.h"
#include "SoundHelper.h"
#include "FontDefines.h"

bool DialogAbstract::init(std::string fileName)
{
    if (!Layer::init()) {
        return false;
    }
    backgroundFileName = fileName;
    disableEvents();
    showDialogBase();
    return true;
}

void DialogAbstract::disableEvents()
{
    _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);
}

void DialogAbstract::showDialogBase()
{
    Size winSize = Director::getInstance()->getWinSize();

    auto black = LayerColor::create(Color4B(Color3B::BLACK));
    black->setOpacity(180);
    addChild(black);

    background = Sprite::create(backgroundFileName);
    background->setPosition(Point(winSize.width / 2, winSize.height / 2));
    addChild(background);
}

void DialogAbstract::showTitle(std::string title)
{
    Size winSize = Director::getInstance()->getWinSize();
    auto label = Label::createWithTTF(title, FONT_NAME_2, 22);
    label->setDimensions(500, 0);
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    label->setPosition(Point(winSize.width / 2,
                             winSize.height / 2 + 11));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    addChild(label, 1);
}

void DialogAbstract::showDescription(std::string description)
{
    Size winSize = Director::getInstance()->getWinSize();
    auto label = Label::createWithTTF(description, FONT_NAME_2, 28);
    label->setDimensions(500, 0);
    label->setPosition(Point(winSize.width / 2,
                             winSize.height / 2 + background->getContentSize().height / 2 - 30));
    label->setVerticalAlignment(TextVAlignment::TOP);
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setAnchorPoint(Vec2(0.5, 1.0f));
    addChild(label, 1);
}

void DialogAbstract::onTapCloseButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        getEventDispatcher()->removeEventListener(_listener);
        removeFromParent();
    }
}














