#ifndef __syanago__SimpleDialog__
#define __syanago__SimpleDialog__

#include "create_func.h"
#include "DialogAbstract.h"

class SimpleDialog : public DialogAbstract, public create_func<SimpleDialog>
{
public:
    using create_func::create;
    bool init(std::string title, std::string description);
private:
    void showCloseButton();
    
};  

#endif /* defined(__syanago__SimpleDialog__) */
