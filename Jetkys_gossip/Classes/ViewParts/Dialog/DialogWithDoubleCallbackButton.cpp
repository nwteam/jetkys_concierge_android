#include "DialogWithDoubleCallbackButton.h"
#include "FontDefines.h"

bool DialogWithDoubleCallbackButton::init(std::string title, std::string description, const DialogCallback& callbackLeft, const DialogCallback& callbackRight)
{
    if (!DialogAbstract::init()) {
        return false;
    }
    showTitle(title);
    showDescription(description);
    showLeftButton(callbackLeft);
    showRightButton(callbackRight);
    return true;
}


void DialogWithDoubleCallbackButton::showLeftButton(const DialogCallback &callback)
{
    Size winSize = Director::getInstance()->getWinSize();
    ui::Button* button = ui::Button::create("gacha_dialog_button.png");
    button->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    button->setPosition(Vec2(winSize.width / 2, winSize.height / 2) + Vec2(-15, -background->getContentSize().height / 2 + 30 + button->getContentSize().height / 2));
    button->addTouchEventListener([callback](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            callback();
        }
    });

    auto label = Label::createWithTTF("はい", FONT_NAME_2, 36);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setPosition(Point(button->getContentSize().width / 2, button->getContentSize().height / 2 - 18));
    button->addChild(label);

    addChild(button);
}

void DialogWithDoubleCallbackButton::showRightButton(const DialogCallback &callback)
{
    Size winSize = Director::getInstance()->getWinSize();
    ui::Button* button = ui::Button::create("gacha_dialog_button.png");
    button->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    button->setPosition(Vec2(winSize.width / 2, winSize.height / 2) + Vec2(15, -background->getContentSize().height / 2 + 30 + button->getContentSize().height / 2));
    button->addTouchEventListener([callback](Ref* ref, ui::Widget::TouchEventType type) {
        if (type == ui::Widget::TouchEventType::ENDED) {
            callback();
        }
    });

    auto label = Label::createWithTTF("いいえ", FONT_NAME_2, 36);
    button->addChild(label);
    label->setPosition(Point(button->getContentSize().width / 2, button->getContentSize().height / 2 - 18));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(button);
}