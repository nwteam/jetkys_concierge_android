#ifndef __syanago__MenuTouch__
#define __syanago__MenuTouch__

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

class MenuTouch : public Menu {
public:
    CREATE_FUNC(MenuTouch);
    /** creates a Menu with MenuItem objects */
    static MenuTouch* create(MenuItem* item, ...) CC_REQUIRES_NULL_TERMINATION;
    /** creates a Menu with a Array of MenuItem objects */
    static MenuTouch* createWithArray(const Vector<MenuItem*>& arrayOfItems);
    
    static Vector<MenuItem *> itemsSelected;
    
    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
    virtual void onTouchCancelled(Touch *touch, Event *unused_event);
};

#endif /* defined(__syanago__MenuTouch__) */
