#ifndef syanago_PopUpDelegate_h
#define syanago_PopUpDelegate_h


#define ZORDER_POPUP 100

enum PopUpButton {
    PU_BT_CANCEL,
    PU_BT_1,
    PU_BT_2,
    PU_BT_3,
    PU_BT_4,
    PU_BT_5,
    PU_BT_6,
    PU_BT_7
};


class PopUpDelegate {
public:
    virtual void btCallback(int btIndex, void *object) { };
    virtual void btCallback(int btIndex, void *object, int tag) { };
};

#endif
