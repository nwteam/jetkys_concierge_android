#ifndef __syanago__WebViewAlertDialog__
#define __syanago__WebViewAlertDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class WebViewAlertDialog : public Layer, public create_func<WebViewAlertDialog>
{
public:
    typedef std::function<void()> OnResponceCallback;
    using create_func::create;
    bool init(const OnResponceCallback& onDecisionCallBack, const OnResponceCallback& onCancelCallBack);
    
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        TITLE,
        DICISION_BUTTON,
        CANCEL_BUTTON,
    };
    enum Z_ORDER{
        Z_TITLE = 0,
        Z_DICISION_BUTTON,
        Z_CANCEL_BUTTON,
    };
    OnResponceCallback _onDecisionCallBack;
    OnResponceCallback _onCancelCallBack;
    EventListenerTouchOneByOne* _listener;
    
    void showBackground();
    void showTitle();
    void showButton();
    
    void onTouchDicisionButton(Ref* sender, ui::Widget::TouchEventType type);
    void onTouchCancelButton(Ref* sender, ui::Widget::TouchEventType type);

};

#endif
