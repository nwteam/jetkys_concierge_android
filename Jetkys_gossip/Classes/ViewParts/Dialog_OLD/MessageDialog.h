#ifndef __syanago__MessageDialog__
#define __syanago__MessageDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class MessageDialog : public Layer, public create_func<MessageDialog>
{
public:
    typedef std::function<void()> OnCloseCallback;
    using create_func::create;
    bool init(const std::string dialogTitle, const std::string dialogMessage, const OnCloseCallback& callBack);
    
    MessageDialog():
    onCloseCallBack(nullptr)
    {}
    ~MessageDialog();
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        BUTTON,
        TITLE,
        MESSAGE ,
    };
    enum Z_ORDER{
        Z_BUTTON = 0,
        Z_TITLE,
        Z_MESSAGE,
    };
    EventListenerTouchOneByOne* _listener;
    OnCloseCallback onCloseCallBack;
    
    void showBackground();
    void showCloseButton();
    void onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type);
    void showTitle(const std::string title);
    void showMessage(const std::string message);
};

#endif