//
//  DialogView.cpp
//  syanago
//
//  Created by Hao Nguyen on 7/23/14.
//
//

#include "DialogView.h"
#include "FontDefines.h"
#include "PlayerController.h"
#include "SplashScene.h"
#include "TelopScrollView.h"
#include "TreasureItemEffectModel.h"
#include "CourseModel.h"
#include "SkillNameModel.h"
#include "RankModel.h"
#include "MapModel.h"

USING_NS_CC;

enum {
    COUSE_SEA_SIDE = 1,
    COUSE_MOUNTAIN = 2,
    COUSE_CIRCUIT = 3,
    COUSE_COUNTRY = 4,
    COUSE_HEIGHT_WAY = 5,
    COUSE_CITY = 6
};

DialogView::DialogView() {}

DialogView::~DialogView()
{
    SpriteFrameCache::getInstance()->removeSpriteFrameByName("dialog_map.plist");
}

DialogView* DialogView::createWithMessage(std::string errorCode, std::string errorMes)
{
    auto dialog = new DialogView();
    dialog->initWithMes(errorCode, errorMes);
    dialog->autorelease();

    return dialog;
}

DialogView* DialogView::createDialog(std::string errorCode, std::string errorMes, int numberOfbt, int tag /* = -1*/)
{
    auto dialog = new DialogView();
    dialog->initDialog(errorCode, errorMes, numberOfbt, tag != -1 ? tag : -1);
    dialog->autorelease();

    return dialog;
}


DialogView* DialogView::createFriendApplyDialog(std::string errorCode, std::string errorMes, int tag /* = -1*/)
{
    auto dialog = new DialogView();
    dialog->initFriendApplyDialog(errorCode, errorMes, tag != -1 ? tag : -1);
    dialog->autorelease();

    return dialog;
}

DialogView* DialogView::createWithShortMessage(std::string Mes, int number)
{
    auto dialog = new DialogView();
    dialog->initWithShortMes(Mes, number, -1);
    dialog->autorelease();

    return dialog;
}

DialogView* DialogView::createWithShortMessage(std::string Mes, int number, int tag)
{
    auto dialog = new DialogView();
    dialog->initWithShortMes(Mes, number, tag);
    dialog->autorelease();

    return dialog;
}

DialogView* DialogView::createWithCoseDialog(std::string mes, std::string title, int coseId)
{
    auto dialog = new DialogView();
    dialog->initWithCoseDialog(mes, title, coseId);
    dialog->autorelease();

    return dialog;
}


DialogView* DialogView::createWithNetworkError()
{
    auto dialog = new DialogView();
    dialog->initWithNetworkError();
    dialog->autorelease();
    return dialog;
}

DialogView* DialogView::createSkillSelectDialog(int mstSkillNamesId)
{
    auto dialog = new DialogView();
    dialog->initWithSkillSelectDialog(mstSkillNamesId);
    dialog->autorelease();
    dialog->setPosition(Point(0, -100));

    return dialog;
}

DialogView* DialogView::createBrowserConfirm(int tag)
{
    auto dialog = new DialogView();
    dialog->initBrowserConfirm(tag);
    dialog->autorelease();

    return dialog;
}

DialogView* DialogView::createRankUpDialog()
{
    auto dialog = new DialogView();
    dialog->initRankUpDialog();
    dialog->autorelease();

    return dialog;
}

DialogView* DialogView::createNextMapOpenDialog()
{
    auto dialog = new DialogView();
    dialog->initNextMapOpenDialog();
    dialog->autorelease();

    return dialog;
}

DialogView* DialogView::createTitleBackDialog()
{
    auto dialog = new DialogView();
    dialog->initTitleBackDialog();
    dialog->autorelease();

    return dialog;
}

bool DialogView::initWithShortMes(std::string Mes, int number, int tag)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;
    _tag = tag;

    Size winSize = Director::getInstance()->getWinSize();

    auto blackbg = LayerColor::create(Color4B(Color3B::BLACK));
    addChild(blackbg);
    blackbg->setOpacity(180);
    blackbg->setPosition(Point(-winSize.width / 2, -winSize.height / 2));

    // add bgr dialog
    auto bgrDialog = Sprite::create("dialog_small_base.png");
    bgrDialog->setPosition(Point::ZERO);
    addChild(bgrDialog);

    auto menu = MenuPriority::create();
    menu->setPosition(Point::ZERO);
    addChild(menu, 1);
    if (number == 2) {
        // add button ok
        auto yesButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
        menu->addChild(yesButton);
        yesButton->setPosition(Point(-yesButton->getContentSize().width / 2 - 15,
                                     -bgrDialog->getContentSize().height / 2 + 30 + yesButton->getContentSize().height / 2));
        yesButton->setTag(BT_YES);
        auto yesLabel = Label::createWithTTF("はい", FONT_NAME_2, 36);
        yesLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        yesButton->addChild(yesLabel);
        yesLabel->setPosition(Point(yesButton->getContentSize().width / 2, yesButton->getContentSize().height / 2 - 18));

        // add button ok
        auto noButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
        menu->addChild(noButton);
        noButton->setPosition(Point(yesButton->getContentSize().width / 2 + 15,
                                    -bgrDialog->getContentSize().height / 2 + 30 + noButton->getContentSize().height / 2));
        noButton->setTag(BT_NO);
        auto noLabel = Label::createWithTTF("いいえ", FONT_NAME_2, 36);
        noLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        noButton->addChild(noLabel);
        noLabel->setPosition(Point(noButton->getContentSize().width / 2, noButton->getContentSize().height / 2 - 18));
    } else if (number == 1) {
        // add button ok
        auto okButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
        menu->addChild(okButton);
        okButton->setPosition(Point(0,
                                    -bgrDialog->getContentSize().height / 2 + 30 + okButton->getContentSize().height / 2));
        okButton->setTag(BT_YES);
        auto okLabel = Label::createWithTTF("閉じる", FONT_NAME_2, 36);
        okButton->addChild(okLabel);
        okLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        okLabel->setPosition(Point(okButton->getContentSize().width / 2, okButton->getContentSize().height / 2 - 18));
    }

    // add label description of error
    auto textLabel = Label::createWithTTF(Mes, FONT_NAME_2, 32);
    textLabel->setDimensions(500, 0);
    textLabel->setPosition(Point(0, bgrDialog->getContentSize().height / 2 - 60));
    textLabel->setVerticalAlignment(TextVAlignment::TOP);
    textLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    textLabel->setAnchorPoint(Vec2(0.5, 1.0f));
    addChild(textLabel, 1);

    setPosition(Vec2(winSize.width / 2, winSize.height / 2));

    return true;
}


bool DialogView::initWithMes(std::string errorCode, std::string errosMes)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;

    Size winSize = Director::getInstance()->getWinSize();

    // add bgr dialog
    auto bgrDialog = Sprite::create("dialog_base.png");
    bgrDialog->setPosition(Point::ZERO);
    addChild(bgrDialog, 0);

    // add button ok
    auto okButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
    okButton->setPosition(Point::ZERO);
    auto okLabel = Label::createWithTTF("OK", FONT_NAME_2, 36);
    okButton->addChild(okLabel);
    okLabel->setPosition(Point(okButton->getContentSize().width / 2, okButton->getContentSize().height / 2 - 18));

    auto menu = MenuPriority::create();
    menu->addChild(okButton);
    menu->setPosition(Point(0, -bgrDialog->getContentSize().height / 2 + okButton->getContentSize().height / 2 + 10));
    addChild(menu, 1);

    // add label title of error
    auto titleLabel = Label::createWithTTF(errorCode, FONT_NAME_2, 22);
    titleLabel->setDimensions(500, 40);
    titleLabel->setPosition(Point(0, -bgrDialog->getContentSize().height / 2 + titleLabel->getContentSize().height / 2 + 80 - 11));
    titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    titleLabel->setAnchorPoint(Vec2(0.5, 1.0f));
    addChild(titleLabel, 1);


    // add label description of error
    auto textLabel = Label::createWithTTF(errosMes, FONT_NAME_2, 28);
    textLabel->setDimensions(500, 0);
    textLabel->setPosition(Point(0, titleLabel->getContentSize().height / 2 - 14));
    textLabel->setVerticalAlignment(TextVAlignment::TOP);
    textLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    textLabel->setAnchorPoint(Vec2(0.5, 1.0f));
    addChild(textLabel, 1);

    setPosition(Vec2(winSize.width / 2, winSize.height / 2));

    return true;
}

bool DialogView::initDialog(std::string errorCode, std::string errorMes, int numberOfbt, int tag /* = -1*/)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;
    _tag = tag;

    Size winSize = Director::getInstance()->getWinSize();

    auto blackbg = LayerColor::create(Color4B(Color3B::BLACK));
    addChild(blackbg);
    blackbg->setOpacity(180);

    // add bgr dialog
    auto bgrDialog = Sprite::create("dialog_base.png");
    bgrDialog->setPosition(Point(winSize.width / 2, winSize.height / 2));
    addChild(bgrDialog, 0);

    // add label title of error
    ////CCLOG("ダイアログの内容: %s: %s, tag: %d",errorCode.c_str(),errorMes.c_str(), _tag);
    auto titleLabel = Label::createWithTTF(errorCode, FONT_NAME_2, 22);
    addChild(titleLabel, 1);
    titleLabel->setDimensions(500, 0);
    titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    titleLabel->setPosition(Point(winSize.width / 2,
                                  winSize.height / 2 /* - bgrDialog->getContentSize().height/2 +height/2 */ + 11));
    titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    // add label description of error
    auto textLabel = Label::createWithTTF(errorMes, FONT_NAME_2, 28);
    textLabel->setDimensions(500, 0);
    textLabel->setPosition(Point(winSize.width / 2,
                                 winSize.height / 2 + bgrDialog->getContentSize().height / 2 - 30));
    textLabel->setVerticalAlignment(TextVAlignment::TOP);
    textLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    textLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    textLabel->setAnchorPoint(Vec2(0.5, 1.0f));
    addChild(textLabel, 1);



    auto menu = MenuPriority::create();
    menu->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(menu, 1);

    if (numberOfbt == 1) {
        // add button ok
        auto okButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
        menu->addChild(okButton);
        okButton->setPosition(Point(0,
                                    -bgrDialog->getContentSize().height / 2 + 30 + okButton->getContentSize().height / 2));
        okButton->setTag(BT_YES);
        auto okLabel = Label::createWithTTF("OK", FONT_NAME_2, 36);
        okButton->addChild(okLabel);
        okLabel->setPosition(Point(okButton->getContentSize().width / 2, okButton->getContentSize().height / 2 - 18));
        okLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    } else if (numberOfbt == 2) {
        // add button ok
        auto yesButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
        menu->addChild(yesButton);
        yesButton->setPosition(Point(-yesButton->getContentSize().width / 2 - 15,
                                     -bgrDialog->getContentSize().height / 2 + 30 + yesButton->getContentSize().height / 2));
        yesButton->setTag(BT_YES);
        auto yesLabel = Label::createWithTTF("はい", FONT_NAME_2, 36);
        yesLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        yesButton->addChild(yesLabel);
        yesLabel->setPosition(Point(yesButton->getContentSize().width / 2, yesButton->getContentSize().height / 2 - 18));

        // add button ok
        auto noButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
        menu->addChild(noButton);
        noButton->setPosition(Point(yesButton->getContentSize().width / 2 + 15,
                                    -bgrDialog->getContentSize().height / 2 + 30 + noButton->getContentSize().height / 2));
        noButton->setTag(BT_NO);
        auto noLabel = Label::createWithTTF("いいえ", FONT_NAME_2, 36);
        noLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        noButton->addChild(noLabel);
        noLabel->setPosition(Point(noButton->getContentSize().width / 2, noButton->getContentSize().height / 2 - 18));
    } else if (numberOfbt == 3) {
        // add button ok
        auto yesButton = makeMenuItem("yes.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
        menu->addChild(yesButton);
        yesButton->setPosition(Point(-100, -200));
        yesButton->setTag(BT_YES);

        auto yes2Button = makeMenuItem("yes.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
        menu->addChild(yes2Button);
        yes2Button->setPosition(Point(0, -200));
        yes2Button->setTag(BT_YES_2);

        // add button ok
        auto noButton = makeMenuItem("no.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
        menu->addChild(noButton);
        noButton->setPosition(Point(100, -200));
        noButton->setTag(BT_NO);
    }

    return true;
}

bool DialogView::initWithCoseDialog(std::string mes, std::string title, int coseId)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;
    _tag = -1;

    Size winSize = Director::getInstance()->getWinSize();
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("dialog_map.plist");
    auto blackbg = LayerColor::create(Color4B(Color3B::BLACK));
    addChild(blackbg);
    blackbg->setOpacity(180);
    std::shared_ptr<CourseModel>courseModel(CourseModel::find(coseId));
    // add bgr dialog
    std::string dialogBase;
    if (courseModel->getCourseType() == 2) {
        dialogBase = "couse_dialog_boss_base.png";
    } else {
        dialogBase = "couse_dialog_nomal_base.png";
    }
    auto bgrDialog = makeSprite(dialogBase.c_str());
    bgrDialog->setPosition(Point(winSize.width / 2, winSize.height / 2));
    addChild(bgrDialog, 0);


    auto coseLabelSprite = makeSprite(StringUtils::format("%d_couse_label.png", courseModel->getImageId()).c_str());
    bgrDialog->addChild(coseLabelSprite);
    coseLabelSprite->setPosition(Vec2(40, bgrDialog->getContentSize().height));
    ;
    auto selectLabel = Label::createWithTTF("このコースに挑戦しますか？", FONT_NAME_2, 34);
    bgrDialog->addChild(selectLabel);
    selectLabel->setColor(COLOR_YELLOW);
    selectLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    selectLabel->setPosition(Point(bgrDialog->getContentSize().width / 2, bgrDialog->getContentSize().height - 60 - 12));

    auto coseNoLabel = Label::createWithTTF(StringUtils::format("【コース No.%d】", courseModel->getNo()), FONT_NAME_2, 22);
    bgrDialog->addChild(coseNoLabel);
    coseNoLabel->setPosition(selectLabel->getPosition() + Vec2(0, -30 - 11 - 12));
    coseNoLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    if (title.size() <= 54) {
        auto coseMapAndAreaNameLabel = Label::createWithTTF(title, FONT_NAME_2, 26);
        bgrDialog->addChild(coseMapAndAreaNameLabel);
        coseMapAndAreaNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        coseMapAndAreaNameLabel->setPosition(coseNoLabel->getPosition() + Vec2(0, -11 - 10 - 13));
    } else {
        auto coseMapAndAreaNameLabel = Label::createWithTTF(title, FONT_NAME_2, 26);
        coseMapAndAreaNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        coseMapAndAreaNameLabel->setPosition(Point::ZERO);

        _scrollView = ScrollView::create();
        _scrollView->setViewSize(Size(500, 30));
        _scrollView->setContainer(coseMapAndAreaNameLabel);
        bgrDialog->addChild(_scrollView);
        _scrollView->setPosition(coseNoLabel->getPosition() + Vec2(-250, -11 - 10 - 13));
        _scrollView->setContentOffset(Point(0, -26));
        _posi = Point(500, -26);
        runAction(Sequence::create(DelayTime::create(1.0),
                                   CallFuncN::create([&](Ref* sender) {
            runAction(RepeatForever::create(Sequence::create(DelayTime::create(0.01f),
                                                             CallFuncN::create([&](Ref* sender) {
                _scrollView->setContentOffset(_scrollView->getContentOffset() - Vec2(1, 0));
                if (_scrollView->getContentOffset().x < -50 - _scrollView->getContentSize().width) {
                    _scrollView->setContentOffset(_posi);
                }
            }), NULL)));
        }), NULL));
    }


    //



    auto coseUseFuelLabel = Label::createWithTTF(StringUtils::format("(消費燃料 %d)", courseModel->getRequiredFuel()).c_str(), FONT_NAME_2, 22);
    bgrDialog->addChild(coseUseFuelLabel);
    coseUseFuelLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    coseUseFuelLabel->setPosition(coseNoLabel->getPosition() + Vec2(0, -11 - 10 - 13) + Vec2(0, -13 - 10 - 11));

    auto coseCleaLabel = Label::createWithTTF("【クリア条件】", FONT_NAME_2, 22);
    bgrDialog->addChild(coseCleaLabel);
    coseCleaLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    coseCleaLabel->setPosition(coseUseFuelLabel->getPosition() + Vec2(0, -11 - 30 - 11));

    Label* raceTypeLabel;
    if (courseModel->getRaceType() == 1) {
        raceTypeLabel = Label::createWithTTF("３位以内でゴールせよ！", FONT_NAME_2, 26);
    } else if (courseModel->getRaceType() == 2) {
        raceTypeLabel = Label::createWithTTF(StringUtils::format("スコア%d以上獲得せよ！", courseModel->getTargetScore()).c_str(), FONT_NAME_2, 26);
    } else if (courseModel->getRaceType() == 3) {
        raceTypeLabel = Label::createWithTTF(StringUtils::format("%dターン以内にゴールせよ！", courseModel->getMaxNumberOfTurns()).c_str(), FONT_NAME_2, 26);
    } else if (courseModel->getRaceType() == 4) {
        raceTypeLabel = Label::createWithTTF("２位以内でゴールせよ！", FONT_NAME_2, 26);
    } else if (courseModel->getRaceType() == 5) {
        raceTypeLabel = Label::createWithTTF("１位でゴールせよ！", FONT_NAME_2, 26);
    } else if (courseModel->getRaceType() == 6) {
        raceTypeLabel = Label::createWithTTF(StringUtils::format("２位以内でスコア%d以上獲得せよ！", courseModel->getTargetScore()).c_str(), FONT_NAME_2, 26);
    } else if (courseModel->getRaceType() == 7) {
        raceTypeLabel = Label::createWithTTF(StringUtils::format("１位でスコア%d以上獲得せよ！", courseModel->getTargetScore()).c_str(), FONT_NAME_2, 26);
    } else if (courseModel->getRaceType() == 8) {
        raceTypeLabel = Label::createWithTTF(StringUtils::format("２位以内で%dターン以内にゴールせよ！", courseModel->getMaxNumberOfTurns()).c_str(), FONT_NAME_2, 26);
    } else if (courseModel->getRaceType() == 9) {
        raceTypeLabel = Label::createWithTTF(StringUtils::format("１位で%dターン以内にゴールせよ！", courseModel->getMaxNumberOfTurns()).c_str(), FONT_NAME_2, 26);
    }
    bgrDialog->addChild(raceTypeLabel);
    raceTypeLabel->setPosition(coseCleaLabel->getPosition() + Vec2(0, -11 - 10 - 13));
    raceTypeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));


    auto menu = MenuPriority::create();
    menu->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(menu, 1);

    // add button ok
    auto yesButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
    menu->addChild(yesButton);
    yesButton->setPosition(Point(-yesButton->getContentSize().width / 2 - 15,
                                 -bgrDialog->getContentSize().height / 2 + 30 + yesButton->getContentSize().height / 2));
    yesButton->setTag(BT_YES);
    auto yesLabel = Label::createWithTTF("はい", FONT_NAME_2, 36);
    yesLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    yesButton->addChild(yesLabel);
    yesLabel->setPosition(Point(yesButton->getContentSize().width / 2, yesButton->getContentSize().height / 2 - 18));

    // add button ok
    auto noButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
    menu->addChild(noButton);
    noButton->setPosition(Point(yesButton->getContentSize().width / 2 + 15,
                                -bgrDialog->getContentSize().height / 2 + 30 + noButton->getContentSize().height / 2));
    noButton->setTag(BT_NO);
    auto noLabel = Label::createWithTTF("いいえ", FONT_NAME_2, 36);
    noLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    noButton->addChild(noLabel);
    noLabel->setPosition(Point(noButton->getContentSize().width / 2, noButton->getContentSize().height / 2 - 18));

    return true;
}

bool DialogView::initWithNetworkError()
{
    if (!LayerPriority::init()) {
        return false;
    }
    _tag = TAG_DIALOG_VIEW_NETWORK_ERROR;
    _delegate = nullptr;

    Size winSize = Director::getInstance()->getWinSize();

    // add bgr dialog
    auto bgrDialog = Sprite::create("dialog_base.png");
    bgrDialog->setPosition(Point::ZERO);
    addChild(bgrDialog, 0);

    // add button ok
    auto okButton = makeMenuItem("te_popup_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
    okButton->setPosition(Point::ZERO + Vec2(0, 30));
    auto okLabel = Label::createWithTTF("再接続", FONT_NAME_2, 36);
    okButton->addChild(okLabel);
    okLabel->setPosition(Point(okButton->getContentSize().width / 2, okButton->getContentSize().height / 2 - 18));
    okButton->setTag(BT_YES);
    okLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));


    auto menu = MenuPriority::create();
    menu->addChild(okButton);
    menu->setPosition(Point(0, -bgrDialog->getContentSize().height / 2 + okButton->getContentSize().height / 2 + 10));
    addChild(menu, 1);

    // add label title of error
    auto titleLabel = Label::createWithTTF("ネットワークエラー", FONT_NAME_2, 36);
    titleLabel->setDimensions(500, 40);
    titleLabel->setPosition(Point(0, bgrDialog->getContentSize().height / 2 -  titleLabel->getContentSize().height / 2 - 30));
    titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    titleLabel->setAnchorPoint(Vec2(0.5, 1.0f));
    addChild(titleLabel, 1);
    titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    titleLabel->setColor(COLOR_YELLOW);


    // add label description of error
    auto textLabel = Label::createWithTTF("ネットワーク環境の良いところで", FONT_NAME_2, 28);
    textLabel->setDimensions(500, 0);
    textLabel->setPosition(Point(0, titleLabel->getPosition().y - 70));
    textLabel->setVerticalAlignment(TextVAlignment::TOP);
    textLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    textLabel->setAnchorPoint(Vec2(0.5, 1.0f));
    addChild(textLabel, 1);
    textLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    auto textLabel2 = Label::createWithTTF("再接続を行ってください", FONT_NAME_2, 28);
    textLabel2->setDimensions(500, 0);
    textLabel2->setPosition(Point(0, textLabel->getPosition().y - 28 - 10));
    textLabel2->setVerticalAlignment(TextVAlignment::TOP);
    textLabel2->setHorizontalAlignment(TextHAlignment::CENTER);
    textLabel2->setAnchorPoint(Vec2(0.5, 1.0f));
    addChild(textLabel2, 1);
    textLabel2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

    setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    return true;
}

bool DialogView::initWithSkillSelectDialog(int mstSkillNamesId)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;
    _tag = -1;

    Size winSize = Director::getInstance()->getWinSize();

    auto blackbg = Layer::create();
    addChild(blackbg);
    blackbg->setOpacity(180);

    // add bgr dialog
    auto bgrDialog = Sprite::create("skill_select_base.png");
    bgrDialog->setPosition(Point(winSize.width / 2, winSize.height / 2));
    addChild(bgrDialog, 0);

    // add label title of error
    ////CCLOG("ダイアログの内容: %s: %s, tag: %d",errorCode.c_str(),errorMes.c_str(), _tag);
    std::shared_ptr<SkillNameModel>skillNameModel(SkillNameModel::find(mstSkillNamesId));
    if (skillNameModel->getDescription().size() <= 51) {
        auto titleLabel = Label::createWithTTF(skillNameModel->getDescription(), FONT_NAME_2, 28);
        addChild(titleLabel, 1);
        titleLabel->setDimensions(500, 0);
        titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
        titleLabel->setPosition(Point(winSize.width / 2,
                                      winSize.height / 2 - 7 /* - bgrDialog->getContentSize().height/2 +height/2 */ + 11));
        titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    } else {
        auto titleLabel = Label::createWithTTF(skillNameModel->getDescription(), FONT_NAME_2, 28);
        titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
        titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        titleLabel->setPosition(Point::ZERO);
        _scrollView = ScrollView::create();
        _scrollView->setViewSize(Size(500, 50));
        _scrollView->setContainer(titleLabel);
        addChild(_scrollView);
        _scrollView->setPosition(Point(winSize.width / 2 - 250,
                                       winSize.height / 2 - 10));
        _scrollView->setContentOffset(Point(0, -14));
        _posi = Point(500, -14);
        runAction(Sequence::create(DelayTime::create(1.0),
                                   CallFuncN::create([&](Ref* sender) {
            runAction(RepeatForever::create(Sequence::create(DelayTime::create(0.01f),
                                                             CallFuncN::create([&](Ref* sender) {
                _scrollView->setContentOffset(_scrollView->getContentOffset() - Vec2(1, 0));
                if (_scrollView->getContentOffset().x < -50 - _scrollView->getContentSize().width) {
                    _scrollView->setContentOffset(_posi);
                }
            }), NULL)));
        }), NULL));
    }
    // add label description of error
    auto textLabel = Label::createWithTTF(skillNameModel->getName(), FONT_NAME_2, 32);
    textLabel->setDimensions(500, 0);
    textLabel->setPosition(Point(winSize.width / 2,
                                 winSize.height / 2 + bgrDialog->getContentSize().height / 2 - 30));
    textLabel->setVerticalAlignment(TextVAlignment::TOP);
    textLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    textLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    textLabel->setAnchorPoint(Vec2(0.5, 1.0f));
    textLabel->setColor(COLOR_YELLOW);
    addChild(textLabel, 1);



    auto menu = MenuPriority::create();
    menu->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(menu, 1);

    // add button ok
    auto yesButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
    menu->addChild(yesButton);
    yesButton->setPosition(Point(-yesButton->getContentSize().width / 2 - 15,
                                 -bgrDialog->getContentSize().height / 2 + 30 + yesButton->getContentSize().height / 2));
    yesButton->setTag(BT_YES);
    auto yesLabel = Label::createWithTTF("発動", FONT_NAME_2, 32);
    yesLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    yesButton->addChild(yesLabel);
    yesLabel->setPosition(Point(yesButton->getContentSize().width / 2, yesButton->getContentSize().height / 2 - 16));

    // add button ok
    auto noButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
    menu->addChild(noButton);
    noButton->setPosition(Point(yesButton->getContentSize().width / 2 + 15,
                                -bgrDialog->getContentSize().height / 2 + 30 + noButton->getContentSize().height / 2));
    noButton->setTag(BT_NO);
    auto noLabel = Label::createWithTTF("閉じる", FONT_NAME_2, 32);
    noLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    noButton->addChild(noLabel);
    noLabel->setPosition(Point(noButton->getContentSize().width / 2, noButton->getContentSize().height / 2 - 16));

    return true;
}

bool DialogView::initFriendApplyDialog(std::string errorCode, std::string errorMes, int tag)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;
    _tag = tag;

    Size winSize = Director::getInstance()->getWinSize();

    auto blackbg = LayerColor::create(Color4B(Color3B::BLACK));
    addChild(blackbg);
    blackbg->setOpacity(180);

    // add bgr dialog
    auto bgrDialog = Sprite::create("dialog_base.png");
    bgrDialog->setPosition(Point(winSize.width / 2, winSize.height / 2));
    addChild(bgrDialog, 0);

    // add label title of error
    ////CCLOG("ダイアログの内容: %s: %s, tag: %d",errorCode.c_str(),errorMes.c_str(), _tag);
    auto titleLabel = Label::createWithTTF(errorCode, FONT_NAME_2, 28);
    addChild(titleLabel, 1);
    titleLabel->setDimensions(500, 0);
    titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    titleLabel->setPosition(Point(winSize.width / 2,
                                  winSize.height / 2 /* - bgrDialog->getContentSize().height/2 +height/2 */ + 11));
    titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    titleLabel->setColor(COLOR_YELLOW);

    // add label description of error
    auto textLabel = Label::createWithTTF(errorMes, FONT_NAME_2, 32);
    textLabel->setDimensions(500, 0);
    textLabel->setPosition(Point(winSize.width / 2,
                                 winSize.height / 2 + bgrDialog->getContentSize().height / 2 - 30));
    textLabel->setVerticalAlignment(TextVAlignment::TOP);
    textLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    textLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    textLabel->setAnchorPoint(Vec2(0.5, 1.0f));
    addChild(textLabel, 1);

    auto menu = MenuPriority::create();
    menu->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(menu, 1);

    // add button ok
    auto yesButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
    menu->addChild(yesButton);
    yesButton->setPosition(Point(-yesButton->getContentSize().width / 2 - 15,
                                 -bgrDialog->getContentSize().height / 2 + 30 + yesButton->getContentSize().height / 2));
    yesButton->setTag(BT_YES);
    auto yesLabel = Label::createWithTTF("はい", FONT_NAME_2, 36);
    yesLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    yesButton->addChild(yesLabel);
    yesLabel->setPosition(Point(yesButton->getContentSize().width / 2, yesButton->getContentSize().height / 2 - 18));

    // add button ok
    auto noButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
    menu->addChild(noButton);
    noButton->setPosition(Point(yesButton->getContentSize().width / 2 + 15,
                                -bgrDialog->getContentSize().height / 2 + 30 + noButton->getContentSize().height / 2));
    noButton->setTag(BT_NO);
    auto noLabel = Label::createWithTTF("いいえ", FONT_NAME_2, 36);
    noLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    noButton->addChild(noLabel);
    noLabel->setPosition(Point(noButton->getContentSize().width / 2, noButton->getContentSize().height / 2 - 18));

    return true;
}


bool DialogView::initBrowserConfirm(int tag)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;
    _tag = tag;

    Size winSize = Director::getInstance()->getWinSize();

    auto blackbg = LayerColor::create(Color4B(Color3B::BLACK));
    addChild(blackbg);
    blackbg->setOpacity(180);

    // add bgr dialog
    auto bgrDialog = Sprite::create("dialog_base.png");
    bgrDialog->setPosition(Point(winSize.width / 2, winSize.height / 2));
    addChild(bgrDialog, 0);

    // add label title of error
    ////CCLOG("ダイアログの内容: %s: %s, tag: %d",errorCode.c_str(),errorMes.c_str(), _tag);
    auto titleLabel = Label::createWithTTF("外部ブラウザを起動しますか？	", FONT_NAME_2, 36);
    addChild(titleLabel, 1);
    titleLabel->setDimensions(500, 0);
    titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    titleLabel->setPosition(Point(winSize.width / 2,
                                  winSize.height / 2 + 11));
    titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));



    auto menu = MenuPriority::create();
    menu->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    addChild(menu, 1);

    {
        // add button ok
        auto button = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
        menu->addChild(button);
        button->setPosition(Point(-button->getContentSize().width / 2 - 15,
                                  -bgrDialog->getContentSize().height / 2 + 30 + button->getContentSize().height / 2));
        button->setTag(BT_YES);
        auto label = Label::createWithTTF("起動する", FONT_NAME_2, 32);
        label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        button->addChild(label);
        label->setPosition(Point(button->getContentSize().width / 2, button->getContentSize().height / 2 - 18));
    }

    {
        // add button ok
        auto button = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
        menu->addChild(button);
        button->setPosition(Point(button->getContentSize().width / 2 + 15,
                                  -bgrDialog->getContentSize().height / 2 + 30 + button->getContentSize().height / 2));
        button->setTag(BT_NO);
        auto label = Label::createWithTTF("閉じる", FONT_NAME_2, 32);
        label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        button->addChild(label);
        label->setPosition(Point(button->getContentSize().width / 2, button->getContentSize().height / 2 - 18));
    }

    return true;
}

bool DialogView::initRankUpDialog()
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;

    Size winSize = Director::getInstance()->getWinSize();

    auto blackbg = LayerColor::create(Color4B(Color3B::BLACK));
    addChild(blackbg);
    blackbg->setOpacity(180);
    blackbg->setPosition(Point(-winSize.width / 2, -winSize.height / 2));

    // add bgr dialog
    auto bgrDialog = Sprite::create("rank_up_dialog_base.png");
    bgrDialog->setPosition(Point::ZERO);
    addChild(bgrDialog);

    auto menu = MenuPriority::create();
    menu->setPosition(Point::ZERO);
    addChild(menu, 1);

    // add button ok
    auto okButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
    menu->addChild(okButton);
    okButton->setPosition(Point(0,
                                -bgrDialog->getContentSize().height / 2 + 30 + okButton->getContentSize().height / 2));
    okButton->setTag(BT_YES);
    auto okLabel = Label::createWithTTF("OK", FONT_NAME_2, 32);
    okButton->addChild(okLabel);
    okLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    okLabel->setPosition(Point(okButton->getContentSize().width / 2, okButton->getContentSize().height / 2 - 16));
    // ******以下ダイアログ内容
    // add label description of error
    int nowRank = PLAYERCONTROLLER->_player->getRank();
    int oldRank = nowRank - 1;
    std::shared_ptr<RankModel>rankModel(RankModel::find(nowRank));
    int maxCost = rankModel->getMaxCost();
    int maxFuel = rankModel->getMaxFuel() + TreasureItemEffectModel::getTreasureItemIncrementalDifferenceEffect(ITEM::EFFECT_TYPE::MAX_FUEL_UP);
    int maxFriend = rankModel->getMaxFriend() + TreasureItemEffectModel::getTreasureItemIncrementalDifferenceEffect(ITEM::EFFECT_TYPE::MAX_FRIENDS_UP);
    int getToken = rankModel->getToken();

    std::shared_ptr<RankModel>oldRankModel(RankModel::find(oldRank));
    int oldMaxCost = oldRankModel->getMaxCost();
    int oldMaxFuel = oldRankModel->getMaxFuel() + TreasureItemEffectModel::getTreasureItemIncrementalDifferenceEffect(ITEM::EFFECT_TYPE::MAX_FUEL_UP);
    int oldMaxFriend = oldRankModel->getMaxFriend() + TreasureItemEffectModel::getTreasureItemIncrementalDifferenceEffect(ITEM::EFFECT_TYPE::MAX_FRIENDS_UP);

    int titleFontSize = 34;
    int newRankFontSize = 32;
    int oldRankFontSize = 30;
    int nomalFontSize = 26;

    auto titleLabel = Label::createWithTTF("ランクが上昇しました！", FONT_NAME_2, titleFontSize);
    titleLabel->setColor(COLOR_YELLOW);
    titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    bgrDialog->addChild(titleLabel);
    titleLabel->setPosition(Point(bgrDialog->getContentSize().width / 2, bgrDialog->getContentSize().height - 20 - titleLabel->getContentSize().height / 2 - titleFontSize / 2));

    auto oldRankLabel = Label::createWithTTF(StringUtils::format("ランク%d", oldRank), FONT_NAME_2, oldRankFontSize);
    bgrDialog->addChild(oldRankLabel);
    oldRankLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto newRankLabel = Label::createWithTTF(StringUtils::format("ランク%d", nowRank), FONT_NAME_2, newRankFontSize);
    bgrDialog->addChild(newRankLabel);
    newRankLabel->setColor(COLOR_YELLOW);
    newRankLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    auto arrowSprite = makeSprite("rank_up_arrow_image.png");
    bgrDialog->addChild(arrowSprite);

    arrowSprite->setPosition(titleLabel->getPosition() - Point(0, titleFontSize / 2 + 20 + arrowSprite->getContentSize().height / 2));
    oldRankLabel->setPosition(arrowSprite->getPosition() - Point(arrowSprite->getContentSize().width / 2 + 14 + oldRankLabel->getContentSize().width / 2,
                                                                 oldRankFontSize / 2 - 3));
    newRankLabel->setPosition(arrowSprite->getPosition() + Point(arrowSprite->getContentSize().width / 2 + 10 - (newRankFontSize - oldRankFontSize) + newRankLabel->getContentSize().width / 2 + 2,
                                                                 -newRankFontSize / 2 + 4));


    auto reparFuelabel = Label::createWithTTF("燃料が全回復しました", FONT_NAME_2, nomalFontSize);
    bgrDialog->addChild(reparFuelabel);
    reparFuelabel->setAnchorPoint(Vec2(0.0, 0.5));
    reparFuelabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    reparFuelabel->setPosition(arrowSprite->getPosition() - Point(reparFuelabel->getContentSize().width, arrowSprite->getContentSize().height / 2 + reparFuelabel->getContentSize().height / 2));

    int maxWidthlabel = reparFuelabel->getContentSize().width;
    int viewLabelCount = 1;
    int upFuel = maxFuel - oldMaxFuel;
    Label* upFuelLabel1 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    Label* upFuelLabel2 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    Label* upFuelLabel3 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    if (upFuel > 0) {
        viewLabelCount++;
        upFuelLabel1->setString("燃料の最大数が");
        bgrDialog->addChild(upFuelLabel1);
        upFuelLabel1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upFuelLabel1->setAnchorPoint(Vec2(0.0, 0.5));
        upFuelLabel2->setString(StringUtils::format("%d", upFuel));
        bgrDialog->addChild(upFuelLabel2);
        upFuelLabel2->setColor(COLOR_YELLOW);
        upFuelLabel2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upFuelLabel2->setAnchorPoint(Vec2(0.0, 0.5));
        upFuelLabel3->setString("上昇しました");
        bgrDialog->addChild(upFuelLabel3);
        upFuelLabel3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upFuelLabel3->setAnchorPoint(Vec2(0.0, 0.5));

        float labelWidth = upFuelLabel1->getContentSize().width + upFuelLabel2->getContentSize().width + upFuelLabel3->getContentSize().width;
        if (maxWidthlabel < labelWidth) {
            maxWidthlabel = labelWidth;
        }
    }
    int upCost = maxCost - oldMaxCost;
    Label* upCostLabel1 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    Label* upCostLabel2 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    Label* upCostLabel3 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    if (upCost > 0) {
        viewLabelCount++;
        upCostLabel1->setString("編成コストの最大数が");
        bgrDialog->addChild(upCostLabel1);
        upCostLabel1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upCostLabel1->setAnchorPoint(Vec2(0.0, 0.5));
        upCostLabel2->setString(StringUtils::format("%d", upCost));
        bgrDialog->addChild(upCostLabel2);
        upCostLabel2->setColor(COLOR_YELLOW);
        upCostLabel2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upCostLabel2->setAnchorPoint(Vec2(0.0, 0.5));
        upCostLabel3->setString("上昇しました");
        bgrDialog->addChild(upCostLabel3);
        upCostLabel3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upCostLabel3->setAnchorPoint(Vec2(0.0, 0.5));

        float labelWidth = upCostLabel1->getContentSize().width + upCostLabel2->getContentSize().width + upCostLabel3->getContentSize().width;
        if (maxWidthlabel < labelWidth) {
            maxWidthlabel = labelWidth;
        }
    }
    int upFriend = maxFriend - oldMaxFriend;
    Label* upFriendLabel1 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    Label* upFriendLabel2 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    Label* upFriendLabel3 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    if (upFriend > 0) {
        viewLabelCount++;
        upFriendLabel1->setString("フレンドの最大数が");
        bgrDialog->addChild(upFriendLabel1);
        upFriendLabel1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upFriendLabel1->setAnchorPoint(Vec2(0.0, 0.5));
        upFriendLabel2->setString(StringUtils::format("%d", upFriend));
        bgrDialog->addChild(upFriendLabel2);
        upFriendLabel2->setColor(COLOR_YELLOW);
        upFriendLabel2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upFriendLabel2->setAnchorPoint(Vec2(0.0, 0.5));
        upFriendLabel3->setString("上昇しました");
        bgrDialog->addChild(upFriendLabel3);
        upFriendLabel3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upFriendLabel3->setAnchorPoint(Vec2(0.0, 0.5));

        float labelWidth = upFriendLabel1->getContentSize().width + upFriendLabel2->getContentSize().width + upFriendLabel3->getContentSize().width;
        if (maxWidthlabel < labelWidth) {
            maxWidthlabel = labelWidth;
        }
    }
    Label* upTokenLabel1 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    Label* upTokenLabel2 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    Label* upTokenLabel3 = Label::createWithTTF("", FONT_NAME_2, nomalFontSize);
    if (getToken > 0) {
        viewLabelCount++;
        upTokenLabel1->setString("トークンを");
        bgrDialog->addChild(upTokenLabel1);
        upTokenLabel1->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upTokenLabel1->setAnchorPoint(Vec2(0.0, 0.5));
        upTokenLabel2->setString(StringUtils::format("%d", getToken));
        bgrDialog->addChild(upTokenLabel2);
        upTokenLabel2->setColor(COLOR_YELLOW);
        upTokenLabel2->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upTokenLabel2->setAnchorPoint(Vec2(0.0, 0.5));
        upTokenLabel3->setString("獲得しました");
        bgrDialog->addChild(upTokenLabel3);
        upTokenLabel3->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        upTokenLabel3->setAnchorPoint(Vec2(0.0, 0.5));

        float labelWidth = upTokenLabel1->getContentSize().width + upTokenLabel2->getContentSize().width + upTokenLabel3->getContentSize().width;
        if (maxWidthlabel < labelWidth) {
            maxWidthlabel = labelWidth;
        }
    }

    reparFuelabel->setPosition(Point((bgrDialog->getContentSize().width - maxWidthlabel) / 2,
                                     reparFuelabel->getPositionY() - (5 - viewLabelCount) * nomalFontSize));
    float textPosition = reparFuelabel->getPositionY();
    float reparFuelLabelHeight = reparFuelabel->getContentSize().height;
    if (upFuel > 0) {
        upFuelLabel1->setPosition(Point((bgrDialog->getContentSize().width - maxWidthlabel) / 2,
                                        textPosition - reparFuelLabelHeight / 2 - upFuelLabel1->getContentSize().height / 2 + nomalFontSize / 2));

        upFuelLabel2->setPosition(upFuelLabel1->getPosition() + Point(upFuelLabel1->getContentSize().width + 5, 0));
        upFuelLabel3->setPosition(upFuelLabel2->getPosition() + Point(upFuelLabel2->getContentSize().width + 5, 0));

        textPosition = upFuelLabel1->getPositionY();
        reparFuelLabelHeight = upFuelLabel1->getContentSize().height;
    }
    if (upCost > 0) {
        upCostLabel1->setPosition(Point((bgrDialog->getContentSize().width - maxWidthlabel) / 2,
                                        textPosition - reparFuelLabelHeight / 2 - upCostLabel1->getContentSize().height / 2 + nomalFontSize / 2));

        upCostLabel2->setPosition(upCostLabel1->getPosition() + Point(upCostLabel1->getContentSize().width + 5, 0));
        upCostLabel3->setPosition(upCostLabel2->getPosition() + Point(upCostLabel2->getContentSize().width + 5, 0));

        textPosition = upCostLabel1->getPositionY();
        reparFuelLabelHeight = upCostLabel1->getContentSize().height;
    }
    if (upFriend > 0) {
        upFriendLabel1->setPosition(Point((bgrDialog->getContentSize().width - maxWidthlabel) / 2,
                                          textPosition - reparFuelLabelHeight / 2 - upFriendLabel1->getContentSize().height / 2 + nomalFontSize / 2));

        upFriendLabel2->setPosition(upFriendLabel1->getPosition() + Point(upFriendLabel1->getContentSize().width + 5, 0));
        upFriendLabel3->setPosition(upFriendLabel2->getPosition() + Point(upFriendLabel2->getContentSize().width + 5, 0));

        textPosition = upFriendLabel1->getPositionY();
        reparFuelLabelHeight = upFriendLabel1->getContentSize().height;
    }
    if (getToken > 0) {
        upTokenLabel1->setPosition(Point((bgrDialog->getContentSize().width - maxWidthlabel) / 2,
                                         textPosition - reparFuelLabelHeight / 2 - upTokenLabel1->getContentSize().height / 2 + nomalFontSize / 2));

        upTokenLabel2->setPosition(upTokenLabel1->getPosition() + Point(upTokenLabel1->getContentSize().width + 5, 0));
        upTokenLabel3->setPosition(upTokenLabel2->getPosition() + Point(upTokenLabel2->getContentSize().width + 5, 0));

        textPosition = upTokenLabel1->getPositionY();
        reparFuelLabelHeight = upTokenLabel1->getContentSize().height;
    }

    // *********************
    setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    return true;
}


bool DialogView::initNextMapOpenDialog()
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;

    Size winSize = Director::getInstance()->getWinSize();

    auto blackbg = LayerColor::create(Color4B(Color3B::BLACK));
    addChild(blackbg);
    blackbg->setOpacity(180);
    blackbg->setPosition(Point(-winSize.width / 2, -winSize.height / 2));

    // add bgr dialog
    auto bgrDialog = Sprite::create("dialog_small_base.png");
    bgrDialog->setPosition(Point::ZERO);
    addChild(bgrDialog);

    auto menu = MenuPriority::create();
    menu->setPosition(Point::ZERO);
    addChild(menu, 1);

    // add button ok
    auto okButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callBackbtn, this));
    menu->addChild(okButton);
    okButton->setPosition(Point(0,
                                -bgrDialog->getContentSize().height / 2 + 30 + okButton->getContentSize().height / 2));
    okButton->setTag(BT_YES);
    auto okLabel = Label::createWithTTF("OK", FONT_NAME_2, 32);
    okButton->addChild(okLabel);
    okLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    okLabel->setPosition(Point(okButton->getContentSize().width / 2, okButton->getContentSize().height / 2 - 16));
    // ******以下ダイアログ内容

    int titleFontSize = 28;
    int mapNameFontSize = 38;
    auto titleLabel = Label::createWithTTF("新しい地方が解放されました", FONT_NAME_2, titleFontSize);
    titleLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    bgrDialog->addChild(titleLabel);
    titleLabel->setPosition(Point(bgrDialog->getContentSize().width / 2,
                                  bgrDialog->getContentSize().height - 20 - titleLabel->getContentSize().height / 2 - titleFontSize / 2));

    int mapId = UserDefault::getInstance()->getIntegerForKey("OPEN_NEXT_MAP_ID");
    std::shared_ptr<MapModel>mapModel(MapModel::find(mapId));
    std::string name = mapModel->getName();
    name = "「" + name + "」";
    // CCLOG("nameSize:%lu",name.size());
    // *****
    if (name.size() <= 42) {
        auto mapNameLabel = Label::createWithTTF(name, FONT_NAME_2,  mapNameFontSize);
        mapNameLabel->setColor(COLOR_YELLOW);
        mapNameLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
        bgrDialog->addChild(mapNameLabel);
        mapNameLabel->setPosition(Point(bgrDialog->getContentSize().width / 2,
                                        bgrDialog->getContentSize().height / 2));
    } else {
        TelopScrollView* telopView = TelopScrollView::initScrollTelop(name,
                                                                      mapNameFontSize,
                                                                      bgrDialog->getContentSize().width - 20,
                                                                      mapNameFontSize + 2,
                                                                      COLOR_YELLOW);
        telopView->setFontShadow(Color3B::BLACK, 2);
        bgrDialog->addChild(telopView);
        telopView->setAnchorPoint(Vec2(0, 0.5));
        telopView->setPosition(Point(bgrDialog->getContentSize().width / 2,
                                     bgrDialog->getContentSize().height / 2));
    }
    // *********************
    UserDefault::getInstance()->setBoolForKey("OPEN_NEXT_MAP_FLAG", false);
    UserDefault::getInstance()->setIntegerForKey("OPEN_NEXT_MAP_ID", 0);
    setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    return true;
}



bool DialogView::initTitleBackDialog()
{
    if (!LayerPriority::init()) {
        return false;
    }
    _delegate = nullptr;
    UserDefault* userDefalt = CCUserDefault::getInstance();
    userDefalt->setIntegerForKey("backgroundTime", 0);
    Size winSize = Director::getInstance()->getWinSize();

    auto blackbg = LayerColor::create(Color4B(Color3B::BLACK));
    addChild(blackbg);
    blackbg->setOpacity(180);
    blackbg->setPosition(Point(-winSize.width / 2, -winSize.height / 2));

    // add bgr dialog
    auto bgrDialog = Sprite::create("dialog_small_base.png");
    bgrDialog->setPosition(Point::ZERO);
    addChild(bgrDialog);

    auto menu = MenuPriority::create();
    menu->setPosition(Point::ZERO);
    addChild(menu, 1);

    // add button ok
    auto okButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(DialogView::callbackTitleBtn, this));
    menu->addChild(okButton);
    okButton->setPosition(Point(0,
                                -bgrDialog->getContentSize().height / 2 + 30 + okButton->getContentSize().height / 2));
    okButton->setTag(BT_YES);
    auto okLabel = Label::createWithTTF("閉じる", FONT_NAME_2, 36);
    okButton->addChild(okLabel);
    okLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    okLabel->setPosition(Point(okButton->getContentSize().width / 2, okButton->getContentSize().height / 2 - 18));

    // add label description of error
    auto textLabel = Label::createWithTTF("最新のサーバー情報と同期するために、\nタイトルに移動します", FONT_NAME_2, 28);
    textLabel->enableShadow();
    textLabel->setDimensions(500, 0);
    textLabel->setPosition(Point(0, bgrDialog->getContentSize().height / 2 - 60));
    textLabel->setVerticalAlignment(TextVAlignment::TOP);
    textLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    textLabel->setAnchorPoint(Vec2(0.5, 1.0f));
    addChild(textLabel, 1);

    setPosition(Vec2(winSize.width / 2, winSize.height / 2));

    return true;
}


void DialogView::callBackbtn(Ref* psender)
{
    ButtonIndex aIndex = (ButtonIndex)((Node*)psender)->getTag();
    switch (aIndex) {
    case BT_NO:
        _soundManeger->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        break;
    case BT_YES:
    case BT_YES_2:
    case BT_YES_3:
        _soundManeger->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        break;
    default:
        _soundManeger->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        break;
    }

    int tag = _tag;
    DialogDelegate* delegate = _delegate;
    removeFromParent();
    // call back
    if (delegate != nullptr) {
        ////CCLOG("Delegate call back: index = %d, tag = %d", (int)aIndex, tag);
        if (tag == -1) {
            delegate->btDialogCallback(aIndex);
        } else {
            delegate->btDialogCallback(aIndex, tag);
        }
    }
}

void DialogView::callbackTitleBtn(cocos2d::Ref* pSender)
{
    _soundManeger->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    auto scene = SplashScene::createScene();
    transitScene(scene);
}

