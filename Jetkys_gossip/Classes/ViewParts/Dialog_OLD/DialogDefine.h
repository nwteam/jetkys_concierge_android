#ifndef syanago_DialogDefine_h
#define syanago_DialogDefine_h

#include "cocos2d.h"

USING_NS_CC;

#define ERROR_MES_FULL "あああああああああああああああああああああ"
#define ERROR_MES_FULL1 "ユーザー登録失敗失敗ユーザー登録失敗失敗ユー\nエラーコード 201"
#define ERROR_CODE_201 "エラーコード 201"
#define ERROR_CODE_NETWORK  250//"250"
#define ERROR_SERVER_MAINTENANCE 237
//#define ERROR_CODE_NETWORK_STR "250"
#define ERROR_MES_201  "POST以外の不正アクセス"
#define ERROR_NETWORK "Network error!"

#define DIALOG_ZORDER 100

enum ButtonIndex {
    BT_YES, BT_NO, BT_YES_2, BT_YES_3
};

#endif
