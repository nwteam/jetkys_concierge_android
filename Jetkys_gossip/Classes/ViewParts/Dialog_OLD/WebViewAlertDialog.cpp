#include "WebViewAlertDialog.h"
#include "FontDefines.h"
#include "SoundHelper.h"


bool WebViewAlertDialog::init(const OnResponceCallback& onDecisionCallBack, const OnResponceCallback& onCancelCallBack)
{
    if (!Layer::init()) {
        return false;
    }
    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    _onDecisionCallBack = onDecisionCallBack;
    _onCancelCallBack = onCancelCallBack;

    showBackground();
    showTitle();
    showButton();

    return true;
}

void WebViewAlertDialog::showBackground()
{
    auto black = LayerColor::create(Color4B(Color3B::BLACK));
    black->setOpacity(180);
    addChild(black);

    auto backGround = Sprite::create("dialog_small_base.png");
    backGround->setPosition(Vec2(Director::getInstance()->getWinSize() / 2));
    backGround->setTag(TAG_SPRITE::BACKGROUND);
    addChild(backGround);
}

void WebViewAlertDialog::showTitle()
{
    auto label = Label::createWithTTF("発行します。よろしいですか？", FONT_NAME_2, 25);
    label->setTag(TAG_SPRITE::TITLE);
    label->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2, getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().height - label->getContentSize().height / 2 - 25));
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(label, Z_ORDER::Z_TITLE);
}

void WebViewAlertDialog::showButton()
{
    auto cancelButton = ui::Button::create("gacha_dialog_button.png");
    cancelButton->addTouchEventListener(CC_CALLBACK_2(WebViewAlertDialog::onTouchCancelButton, this));
    cancelButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    cancelButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 + 15 + cancelButton->getContentSize().width / 2, 40));
    cancelButton->setTag(TAG_SPRITE::CANCEL_BUTTON);
    auto cancelLabel = Label::createWithTTF("いいえ", FONT_NAME_2, 28);
    cancelLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    cancelLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    cancelLabel->setPosition(Vec2(cancelButton->getContentSize() / 2) - Vec2(0, 14));
    cancelButton->addChild(cancelLabel);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(cancelButton, Z_ORDER::Z_CANCEL_BUTTON);

    auto dicisionButton = ui::Button::create("gacha_dialog_button.png");
    dicisionButton->addTouchEventListener(CC_CALLBACK_2(WebViewAlertDialog::onTouchDicisionButton, this));
    dicisionButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    dicisionButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2 - 15 - dicisionButton->getContentSize().width / 2, 40));
    dicisionButton->setTag(TAG_SPRITE::DICISION_BUTTON);
    auto dicisionLabel = Label::createWithTTF("はい", FONT_NAME_2, 28);
    dicisionLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    dicisionLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    dicisionLabel->setPosition(Vec2(dicisionButton->getContentSize() / 2) - Vec2(0, 14));
    dicisionButton->addChild(dicisionLabel);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(dicisionButton, Z_ORDER::Z_DICISION_BUTTON);
}

void WebViewAlertDialog::onTouchDicisionButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
        getEventDispatcher()->removeEventListener(_listener);
        _onDecisionCallBack();
        removeFromParent();
    }
}

void WebViewAlertDialog::onTouchCancelButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        getEventDispatcher()->removeEventListener(_listener);
        _onCancelCallBack();
        removeFromParent();
    }
}

