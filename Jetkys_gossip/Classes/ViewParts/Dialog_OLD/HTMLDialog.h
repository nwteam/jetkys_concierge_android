#ifndef __syanago__HTMLDialog__
#define __syanago__HTMLDialog__

#include "cocos2d.h"
#include "Loading.h"
#include "FontDefines.h"
#include "LayerPriority.h"
#include "ui/CocosGUI.h"
#include "DefaultProgress.h"
#include "RequestAPI.h"

using namespace SyanagoAPI;

#define DIALOG_ZORDER 100

class HtMLDialogDelegate {
public:
    virtual ~HtMLDialogDelegate(){};
    virtual void endHtmlDialog() = 0;
};

class HTMLDialog : public LayerPriority
{
public:
    HTMLDialog();
    ~HTMLDialog();
    virtual bool init();
    CREATE_FUNC(HTMLDialog);
    
    static HTMLDialog* createWithUrl(std::string viewUrl);
    static HTMLDialog *createWithHtml(std::string html);
    
    bool initWithMes(std::string viewUrl, bool isURL);
    
    void callBackbtn(Ref *pSender);
    
    void reloadHtmlCode();
    void closeDialog();
    void setDialogViewFlag(bool flag);
    
    std::map<std::string, std::string> _listURLs;
    HtMLDialogDelegate *_delegate;
    bool _doNotShowFlag;
private:
    DefaultProgress* _progress;
    RequestAPI* _request;
    
    cocos2d::experimental::ui::WebView* _webview;
    void setRectNode(Node *node, float x, float y, float width, float height);
    
    void responseGetDialog(Json* response);
};

#endif /* defined(__syanago__HTMLDialog__) */
