#include "Loading.h"
#include "FontDefines.h"
#include "jcommon.h"
#include "LayerPriority.h"

Loading::Loading():
    scene(nullptr) {}
bool Loading::init()
{
    if (!Layer::init()) {
        return false;
    }
    setTag(200);
    Size winSize = Director::getInstance()->getWinSize();

    auto background = LayerColor::create(Color4B(0, 0, 0, 150), winSize.width, winSize.height);
    addChild(background);

    _count = 0;
    _labelStatusCode = Label::createWithTTF("Loading...", FONT_NAME_2, 50);
    _labelStatusCode->setAnchorPoint(Vec2(0.0, 0.5));
    _labelStatusCode->setPosition(Vec2(winSize.width / 2 - _labelStatusCode->getContentSize().width / 2, winSize.height / 2 - 25 + 50));
    addChild(_labelStatusCode);

    makeSdAnimation();

    auto charaLayer = Layer::create();
    charaLayer->setPosition(Point(Vec2(winSize.width / 2, winSize.height / 2 - 50)));
    addChild(charaLayer);

    auto sdSprite = makeSprite("home_sd_1.png");
    sdSprite->setPosition(Point::ZERO);
    auto action = Animate::create(_MenuSdAnimation);
    sdSprite->runAction(CCRepeatForever::create(action));
    sdSprite->setScale(1.0f);
    sdSprite->setRotation(7.5);
    charaLayer->addChild(sdSprite);

    schedule(schedule_selector(Loading::stringUpdate), 0.2f);

    addChild(LayerPriority::create());

    return true;
}

void Loading::show()
{
    scene = Director::getInstance()->getRunningScene();
    scene->addChild(this, 1002);
}

void Loading::dissmiss()
{
    if (scene == Director::getInstance()->getRunningScene()) {
        scene->removeChildByTag(200);
    }
}


void Loading::makeSdAnimation()
{
    // CCAnimationの初期化
    _MenuSdAnimation = Animation::create();
    // 画像を配列に代入
    for (int i = 1; i < 4; i++) {
        _MenuSdAnimation->addSpriteFrameWithFile(StringUtils::format("home_sd_%d.png", i));
    }
    _MenuSdAnimation->setDelayPerUnit(0.1f); // アニメの動く時間を設定
}

void Loading::stringUpdate(float delta)
{
    switch (_count) {
    case 0: {
        _labelStatusCode->setString("Loading");
        _count++;
        break;
    }
    case 1: {
        _labelStatusCode->setString("Loading.");
        _count++;
        break;
    }
    case 2: {
        _labelStatusCode->setString("Loading..");
        _count++;
        break;
    }
    case 3: {
        _labelStatusCode->setString("Loading...");
        _count = 0;
        break;
    }
    default:
        _count = 0;
        break;
    }
}

