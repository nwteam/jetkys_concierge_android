#ifndef __syanago__DialogView__
#define __syanago__DialogView__

#include "jCommon.h"
#include "DialogDefine.h"
#include "LayerPriority.h"
#include "SoundHelper.h"
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "MenuTouch.h"

USING_NS_CC_EXT;

#define TAG_DIALOG_VIEW_NETWORK_ERROR 1000000
#define TAG_DIALOG_VIEW_GARAGE_FULL TAG_DIALOG_VIEW_NETWORK_ERROR + 1

class DialogDelegate {
public:
    ~DialogDelegate() {};
    virtual void btDialogCallback(ButtonIndex aIndex) { };
    virtual void btDialogCallback(ButtonIndex aIndex, int tag) { };
};

class DialogView : public LayerPriority
{
private:
    
public:
    DialogView();
    ~DialogView();
    
    /**
 *	@brief	Create dialogview with errorCode + errorMessage
 *
 *	@param 	errorCode 	code
 *	@param 	errorMes 	description
 */
    DialogDelegate *_delegate;
    
    static DialogView* createWithMessage(std::string errorCode, std::string errorMes);
    static DialogView *createDialog(std::string errorCode, std::string errorMes, int numberOfbt, int tag = -1);
    
    static DialogView* createWithShortMessage(std::string Mes, int number);
    static DialogView* createWithShortMessage(std::string Mes, int number, int tag);
    static DialogView* createWithCoseDialog(std::string mes, std::string title, int coseId);
    static DialogView* createWithNetworkError();
    static DialogView* createSkillSelectDialog(int mstSkillNamesId);
    static DialogView* createFriendApplyDialog(std::string errorCode, std::string errorMes, int tag = -1);
    static DialogView *createBrowserConfirm(int tag = -1);
    static DialogView* createRankUpDialog();
    static DialogView* createTitleBackDialog();
    static DialogView* createNextMapOpenDialog();
    
    bool initWithMes(std::string errorCode, std::string errorMes);
    bool initDialog(std::string errorCode, std::string errorMes, int numberOfbt, int tag = -1);
    
    bool initFriendApplyDialog(std::string errorCode, std::string errorMes, int tag = -1);
    bool initWithShortMes(std::string errorMes, int number, int tag);
    bool initWithCoseDialog(std::string mes, std::string title, int coseId);
    bool initWithNetworkError();
    bool initWithSkillSelectDialog(int mstSkillNamesId);
    bool initBrowserConfirm(int tag);
    bool initRankUpDialog();
    bool initTitleBackDialog();
    bool initNextMapOpenDialog();
    
    void callBackbtn(Ref *pSender);
    void callbackTitleBtn(Ref* pSender);
    
    SoundHelper* _soundManeger;
    cocos2d::extension::ScrollView* _scrollView;
    cocos2d::Point _posi;
    int _tag;
};

#endif /* defined(__syanago__DialogView__) */
