#include "HTMLDialog.h"
#include "PlayerController.h"
#include "SoundHelper.h"
#include "jCommon.h"

HTMLDialog::HTMLDialog():
    _doNotShowFlag(false), _webview(nullptr), _progress(nullptr), _request(nullptr)
{
    _listURLs = std::map<std::string, std::string>();
    _progress = new DefaultProgress();
    _request = new RequestAPI(_progress);
}
HTMLDialog::~HTMLDialog()
{
    delete _progress;
    delete _request;
    _progress = nullptr;
    _request = nullptr;
}

HTMLDialog* HTMLDialog::createWithUrl(std::string viewUrl)
{
    auto dialog = new HTMLDialog();
    dialog->initWithMes(viewUrl, true);
    dialog->autorelease();
    return dialog;
}

HTMLDialog* HTMLDialog::createWithHtml(std::string html)
{
    auto dialog = new HTMLDialog();
    dialog->initWithMes(html, false);
    dialog->autorelease();
    return dialog;
}

bool HTMLDialog::init()
{
    if (!LayerPriority::initWithPriority(-998)) {
        return false;
    }
    // todo Getdialog api
    _progress->onStart();
    _request->getDialog(CC_CALLBACK_1(HTMLDialog::responseGetDialog, this));
    return true;
}

bool HTMLDialog::initWithMes(std::string viewUrl, bool isURL)
{
    if (!Layer::init()) {
        return false;
    }
    Size winSize = Director::getInstance()->getWinSize();

    // add bgr dialog
    auto BgSprite = Sprite::create("webview_bg_big.png");
    BgSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
    BgSprite->setPosition(Point(winSize.width / 2, winSize.height / 2));
    BgSprite->setScale(2);
    BgSprite->setColor(Color3B::GREEN);
    addChild(BgSprite);

    // add button ok
    auto okButton = makeMenuItem("dialog_ok.png", CC_CALLBACK_1(HTMLDialog::callBackbtn, this));
    okButton->setPosition(Point::ZERO);

    Menu* menu = MenuTouch::create(okButton, NULL);
    menu->setPosition(Point(winSize.width / 2, (winSize.height / 2) - 400));
    addChild(menu, 1);

    // todo Getdialog api
    _progress->onStart();
    _request->getDialog(CC_CALLBACK_1(HTMLDialog::responseGetDialog, this));
    return true;
}

void HTMLDialog::responseGetDialog(Json* response)
{
    Json* jsonData1 = Json_getItem(response, "get_dialog");
    std::string result = Json_getString(jsonData1, "result", "false");

    if (result == "true") {
        // parse and save to listURLs
        Json* jsonDataData = Json_getItem(jsonData1, "data");
        _listURLs["event"] = Json_getString(jsonDataData, "event", "");
        _listURLs["login_bonus"] = Json_getString(jsonDataData, "login_bonus", "");
        _listURLs["help"] = Json_getString(jsonDataData, "help", "");
        _listURLs["invitation_bonus"] = Json_getString(jsonDataData, "invitation_bonus", "");
        _listURLs["notice"] = Json_getString(jsonDataData, "notice", "");

        // add bgr dialog
        Size winSize = Director::getInstance()->getWinSize();

        auto BgSprite = Sprite::create("webview_bg_big.png");
        BgSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
        BgSprite->setPosition(Point(winSize.width / 2, winSize.height / 2));
        addChild(BgSprite);

        // add button ok
        auto okButton = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(HTMLDialog::callBackbtn, this));
        okButton->setPosition(Point::ZERO);
        auto label = Label::createWithTTF("閉じる", FONT_NAME_2, 30);
        okButton->addChild(label);
        label->setPosition(Point(okButton->getContentSize().width / 2, okButton->getContentSize().height / 2 - 15));
        label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));

        Menu* menu = MenuPriority::createWithPriority(-999);
        menu->addChild(okButton);
        menu->setPosition(Point(BgSprite->getContentSize().width / 2, 30 + okButton->getContentSize().height / 2));
        BgSprite->addChild(menu, 1);

        // ①フレンド申請を受けている数
        // ②運営からのお知らせの数
        // ③プレゼントBOXに届いているプレゼントの数
        // "number_of_pending_friend":"0","number_of_notice":"6","number_of_compensation":"3"
        if (Json_getItem(jsonDataData, "number_of_pending_friend")) {
            PLAYERCONTROLLER->_numberOfPendingFriend = atoi(Json_getString(jsonDataData, "number_of_pending_friend", "0"));
        }

        if (Json_getItem(jsonDataData, "number_of_notice")) {
            PLAYERCONTROLLER->_numberOfNotice = atoi(Json_getString(jsonDataData, "number_of_notice", "0"));
        }
        if (Json_getItem(jsonDataData, "number_of_compensation")) {
            PLAYERCONTROLLER->_numberOfCompensation = atoi(Json_getString(jsonDataData, "number_of_compensation", "0"));
        }

        // Save scroll message
        for (int i = 0; i < PLAYERCONTROLLER->_messages.size(); i++) {
            std::string key = StringUtils::format("scroll_message_%d", i + 1);
            PLAYERCONTROLLER->_messages.at(i) = Json_getString(jsonDataData, key.c_str(), "");
        }
    }
    reloadHtmlCode();
}

void HTMLDialog::callBackbtn(Ref* psender)
{
    SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    reloadHtmlCode();
}

void HTMLDialog::reloadHtmlCode()
{
    if (_listURLs.empty()) {
        closeDialog();
        return;
    }
    auto iter = _listURLs.begin();
    auto htmlCode = iter->second;
    _listURLs.erase(iter->first);

    if (htmlCode != "null") {
        if (_webview != nullptr) {
            _webview->removeFromParent();
        }
        Size winSize = Director::getInstance()->getWinSize();

        _webview = cocos2d::experimental::ui::WebView::create();
        setRectNode(_webview, winSize.width / 2 - 240, winSize.height / 2 - 310, 480, 670);
        _webview->loadHTMLString(htmlCode, "");
        addChild(_webview, 50);
    } else if (!_listURLs.empty()) {
        reloadHtmlCode();
    } else {
        if (_doNotShowFlag == false) {
            Size winSize = Director::getInstance()->getWinSize();
            auto resultLabel = Label::createWithTTF("現在お知らせはありません", FONT_NAME_2, 34);
            resultLabel->setPosition(Point(winSize.width / 2, (winSize.height / 2)));
            addChild(resultLabel, 2);
        } else {
            closeDialog();
        }
    }
}

void HTMLDialog::closeDialog()
{
    removeAllChildren();
    _delegate->endHtmlDialog();
    removeFromParent();
}

void HTMLDialog::setDialogViewFlag(bool flag)
{
    _doNotShowFlag = flag;
}

void HTMLDialog::setRectNode(Node* node, float x, float y, float width, float height)
{
    node->setPosition(Vec2(x + width * 0.5, y + height * 0.5f));
    node->setContentSize(Size(width, height));
}