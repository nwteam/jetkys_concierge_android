#include "ShortMessageDialog.h"
#include "SoundHelper.h"
#include "FontDefines.h"

ShortMessageDialog::~ShortMessageDialog()
{
    onCloseCallBack = nullptr;
}

bool ShortMessageDialog::init(const std::string dialogMessage, const OnCloseCallback& callBack)
{
    if (!Layer::init()) {
        return false;
    }
    EventListenerTouchOneByOne* _listener = EventListenerTouchOneByOne::create();
    _listener->setSwallowTouches(true);
    _listener->onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                                  return true;
                              };
    _listener->onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    _listener->onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);

    showBackground();
    showCloseButton();
    showMessage(dialogMessage);
    onCloseCallBack = callBack;
    return true;
}

void ShortMessageDialog::showBackground()
{
    auto black = LayerColor::create(Color4B(Color3B::BLACK));
    black->setOpacity(180);
    addChild(black);
    auto backGround = Sprite::create("dialog_small_base.png");
    Size winSize = Director::getInstance()->getWinSize();
    backGround->setPosition(Point(winSize.width / 2, winSize.height / 2));
    backGround->setTag(TAG_SPRITE::BACKGROUND);
    addChild(backGround);
}

void ShortMessageDialog::showMessage(const std::string message)
{
    auto label = Label::createWithTTF(message, FONT_NAME_2, 32);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    float messageWidth = getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width - 70;
    if (label->getContentSize().width > messageWidth) {
        label->setDimensions(messageWidth, 0);
    }
    label->setTag(TAG_SPRITE::MESSAGE);
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    label->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize() / 2) + Vec2(0, label->getContentSize().height / 2));
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(label, Z_ORDER::Z_MESSAGE);
}

void ShortMessageDialog::showCloseButton()
{
    auto closeButton = ui::Button::create("gacha_dialog_button.png");
    closeButton->addTouchEventListener(CC_CALLBACK_2(ShortMessageDialog::onTouchCloseButton, this));
    closeButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    closeButton->setPosition(Vec2(getChildByTag(TAG_SPRITE::BACKGROUND)->getContentSize().width / 2, 25));
    closeButton->setTag(TAG_SPRITE::BUTTON);
    auto closeLabel = Label::createWithTTF("閉じる", FONT_NAME_2, 28);
    closeLabel->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    closeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    closeLabel->setPosition(Vec2(closeButton->getContentSize() / 2) - Vec2(0, 14));
    closeButton->addChild(closeLabel);
    getChildByTag(TAG_SPRITE::BACKGROUND)->addChild(closeButton, Z_ORDER::Z_BUTTON);
}

void ShortMessageDialog::onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED) {
        SOUND_HELPER->playeMainSceneEffect(SOUND_TAP_FALSE, false);
        getEventDispatcher()->removeEventListener(_listener);
        onCloseCallBack();
        removeFromParent();
    }
}