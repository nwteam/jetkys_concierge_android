#ifndef __syanago__ShortMessageDialog__
#define __syanago__ShortMessageDialog__

#include "cocos2d.h"
#include "create_func.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class ShortMessageDialog : public Layer, public create_func<ShortMessageDialog>
{
public:
    typedef std::function<void()> OnCloseCallback;
    using create_func::create;
    bool init(const std::string dialogMessage, const OnCloseCallback& callBack);
    
    ShortMessageDialog():
    onCloseCallBack(nullptr)
    {}
    ~ShortMessageDialog();
private:
    enum TAG_SPRITE{
        BACKGROUND = 0,
        BUTTON,
        MESSAGE ,
    };
    enum Z_ORDER{
        Z_BUTTON = 0,
        Z_MESSAGE,
    };
    EventListenerTouchOneByOne* _listener;
    OnCloseCallback onCloseCallBack;
    
    void showBackground();
    void showCloseButton();
    void onTouchCloseButton(Ref* sender, ui::Widget::TouchEventType type);
    void showMessage(const std::string message);
};

#endif