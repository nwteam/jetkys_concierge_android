#ifndef __Syanago__Loading__
#define __Syanago__Loading__

#include "cocos2d.h"
USING_NS_CC;

#define LOADING_ZORDER 1000
#define TAG_SPRITE_LOADING 200

class Loading : public Layer {
public:
    Loading();
    bool init();
    CREATE_FUNC(Loading);
    
    void show();
    void dissmiss();
    void makeSdAnimation();
    void stringUpdate(float delta);
    
private:
    Scene* scene;
    Label *_labelStatusCode;
    Animation* _MenuSdAnimation;
    int _count;
};

#endif /* defined(__Syanago__Loading__) */
