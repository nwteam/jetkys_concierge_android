#ifndef __Syanago__LayerPriority__
#define __Syanago__LayerPriority__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "MenuTouch.h"

USING_NS_CC;
USING_NS_CC_EXT;
using cocos2d::Size;
using cocos2d::extension::ScrollView;

#define ZORDER_LAYERPRIORITY 1000

#define PRIORITY_LAYER -1000
#define PRIORITY_SCROLL PRIORITY_LAYER - 1
#define PRIORITY_MENU PRIORITY_LAYER - 2


class MenuPriority : public MenuTouch {
public:
    MenuPriority();
    virtual ~MenuPriority();
    virtual bool init();
    CREATE_FUNC(MenuPriority);
    
    static MenuPriority *createWithPriority(int priority);
    
    EventListenerTouchOneByOne * _touchListener;
    
    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
    virtual void onTouchCancelled(Touch *touch, Event *unused_event);

    CC_SYNTHESIZE(int, _priority, Priority);
    
    cocos2d::extension::ScrollView *_scrollView;
};

class LayerPriority : public Layer{
public:
    LayerPriority();
    virtual ~LayerPriority();
    static LayerPriority *createWithPriority(int priority);
    
    virtual bool init();
    virtual bool initWithPriority(int priority);
    
    CREATE_FUNC(LayerPriority);
    
    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
    virtual void onTouchCancelled(Touch *touch, Event *unused_event);
    
    EventListenerTouchOneByOne * _touchListener;
    

    CC_SYNTHESIZE(int, _priority, Priority);
};

class SyanagoBaseLayer;

class ScrollViewPriority : public cocos2d::extension::ScrollView {
public:
    ScrollViewPriority();
    virtual ~ScrollViewPriority();
    
    /**
     * Returns an autoreleased scroll view object.
     *
     * @param size view size
     * @param container parent object
     * @return autoreleased scroll view object
     */
    static ScrollViewPriority* create(Size size, int priority, Node* container);
    
    
    /**
     * Returns a scroll view object
     *
     * @param size view size
     * @param container parent object
     * @return scroll view object
     */
    bool initWithViewSize(Size size, Node* container);
    
    EventListenerTouchOneByOne * _touchListener;
    
    CC_SYNTHESIZE(int, _priority, Priority);
    
    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
    virtual void onTouchCancelled(Touch *touch, Event *unused_event);
    
    SyanagoBaseLayer *_parent;
};


#endif /* defined(__Syanago__LayerPriority__) */
