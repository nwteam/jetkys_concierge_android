#include "LayerPriority.h"
#include "SyanagoBaseLayer.h"

/**
   Menu Priority
 */
MenuPriority::MenuPriority()
{
    _priority = PRIORITY_MENU;
    _scrollView = nullptr;
}

MenuPriority::~MenuPriority()
{
//    ////CCLOG("MenuPriority::~MenuPriority");
    _eventDispatcher->removeEventListener(_touchListener);
}

MenuPriority* MenuPriority::createWithPriority(int priority)
{
    auto menu = new MenuPriority();
    menu->setPriority(priority);
    menu->init();
    menu->autorelease();

    return menu;
}

bool MenuPriority::init()
{
    if (Layer::init()) {
        _enabled = true;
        // menu in the center of the screen
        Size s = Director::getInstance()->getWinSize();

        ignoreAnchorPointForPosition(true);
        setAnchorPoint(Vec2(0.5f, 0.5f));
        setContentSize(s);

        setPosition(Vec2(s.width / 2, s.height / 2));

        _selectedItem = nullptr;
        _state = Menu::State::WAITING;

        // enable cascade color and opacity on menus
        setCascadeColorEnabled(true);
        setCascadeOpacityEnabled(true);


        _touchListener = EventListenerTouchOneByOne::create();
        _touchListener->setSwallowTouches(true);

        _touchListener->onTouchBegan = CC_CALLBACK_2(MenuPriority::onTouchBegan, this);
        _touchListener->onTouchMoved = CC_CALLBACK_2(MenuPriority::onTouchMoved, this);
        _touchListener->onTouchEnded = CC_CALLBACK_2(MenuPriority::onTouchEnded, this);
        _touchListener->onTouchCancelled = CC_CALLBACK_2(MenuPriority::onTouchCancelled, this);

        _eventDispatcher->addEventListenerWithFixedPriority(_touchListener, _priority);

        return true;
    }
    return false;
}

bool MenuPriority::onTouchBegan(Touch* touch, Event* unused_event)
{
    if (_scrollView) {
        if (!_scrollView->onTouchBegan(touch, unused_event))
            return false;
    }
    return MenuTouch::onTouchBegan(touch, unused_event);
}

void MenuPriority::onTouchMoved(Touch* touch, Event* unused_event)
{
    if (_scrollView) {
        _scrollView->onTouchMoved(touch, unused_event);
    }
    MenuTouch::onTouchMoved(touch, unused_event);
}

void MenuPriority::onTouchEnded(Touch* touch, Event* unused_event)
{
    if (_scrollView) {
        if (_scrollView->isTouchMoved()) {
            _scrollView->onTouchEnded(touch, unused_event);
            MenuTouch::onTouchCancelled(touch, unused_event);
            return;
        }
        _scrollView->onTouchEnded(touch, unused_event);
    }
    MenuTouch::onTouchEnded(touch, unused_event);
}

void MenuPriority::onTouchCancelled(Touch* touch, Event* unused_event)
{
    if (_scrollView) {
        _scrollView->onTouchCancelled(touch, unused_event);
    }
    MenuTouch::onTouchCancelled(touch, unused_event);
}

/**
 *
   Layer Priority
 */

LayerPriority::LayerPriority()
{
    _priority = PRIORITY_LAYER;
}

LayerPriority::~LayerPriority()
{
    //////CCLOG("LayerPriority::~LayerPriority");
    _eventDispatcher->removeEventListener(_touchListener);
}

LayerPriority* LayerPriority::createWithPriority(int priority)
{
    auto layer = new LayerPriority();
    layer->initWithPriority(priority);
    layer->autorelease();

    return layer;
}

bool LayerPriority::init()
{
    if (!Layer::init()) {
        return false;
    }

    // setTouch
    _touchListener = EventListenerTouchOneByOne::create();
    _touchListener->setSwallowTouches(true);
    _touchListener->onTouchBegan = CC_CALLBACK_2(LayerPriority::onTouchBegan, this);
    _touchListener->onTouchMoved = CC_CALLBACK_2(LayerPriority::onTouchMoved, this);
    _touchListener->onTouchEnded = CC_CALLBACK_2(LayerPriority::onTouchEnded, this);
    _touchListener->onTouchCancelled = CC_CALLBACK_2(LayerPriority::onTouchCancelled, this);
    _eventDispatcher->addEventListenerWithFixedPriority(_touchListener, _priority);

    return true;
}

bool LayerPriority::initWithPriority(int priority)
{
    if (!Layer::init()) {
        return false;
    }

    _priority = priority;

    // setTouch
    _touchListener = EventListenerTouchOneByOne::create();
    _touchListener->setSwallowTouches(true);
    _touchListener->onTouchBegan = CC_CALLBACK_2(LayerPriority::onTouchBegan, this);
    _touchListener->onTouchMoved = CC_CALLBACK_2(LayerPriority::onTouchMoved, this);
    _touchListener->onTouchEnded = CC_CALLBACK_2(LayerPriority::onTouchEnded, this);
    _touchListener->onTouchCancelled = CC_CALLBACK_2(LayerPriority::onTouchCancelled, this);
    _eventDispatcher->addEventListenerWithFixedPriority(_touchListener, _priority);

    return true;
}

bool LayerPriority::onTouchBegan(Touch* touch, Event* unused_event)
{
    //////CCLOG("LayerPriority::onTouchBegan");
    if (!isVisible()) {
        return false;
    }
    return true;
}

void LayerPriority::onTouchMoved(Touch* touch, Event* unused_event)
{
    //////CCLOG("LayerPriority::onTouchMoved");
}
void LayerPriority::onTouchEnded(Touch* touch, Event* unused_event)
{
    //////CCLOG("LayerPriority::onTouchEnded");
}
void LayerPriority::onTouchCancelled(Touch* touch, Event* unused_event)
{
    //////CCLOG("LayerPriority::onTouchCancelled");
}


/**
   ScrollView Priority
 */
ScrollViewPriority::ScrollViewPriority()
{
    _priority = PRIORITY_LAYER;
    _parent = nullptr;
}

ScrollViewPriority::~ScrollViewPriority()
{
    ////CCLOG("ScrollViewPriority::~ScrollViewPriority");
    _eventDispatcher->removeEventListener(_touchListener);
}

ScrollViewPriority* ScrollViewPriority::create(Size size, int priority, Node* container)
{
    ScrollViewPriority* pRet = new ScrollViewPriority();
    pRet->setPriority(priority);
    if (pRet && pRet->initWithViewSize(size, container)) {
        pRet->autorelease();
    } else {
        CC_SAFE_DELETE(pRet);
    }
    return pRet;
}


bool ScrollViewPriority::initWithViewSize(cocos2d::Size size, Node* container)
{
    if (!Layer::init()) {
        return false;
    }

    _container = container;


    setViewSize(size);

    _touches.reserve(EventTouch::MAX_TOUCHES);

    _delegate = NULL;
    _bounceable = true;
    _clippingToBounds = true;

    _direction  = Direction::BOTH;
    _container->setPosition(Vec2(0.0f, 0.0f));
    _touchLength = 0.0f;

    addChild(_container);
    _minScale = _maxScale = 1.0f;

    // setTouch
    _touchListener = EventListenerTouchOneByOne::create();
    _touchListener->setSwallowTouches(true);
    _touchListener->onTouchBegan = CC_CALLBACK_2(ScrollViewPriority::onTouchBegan, this);
    _touchListener->onTouchMoved = CC_CALLBACK_2(ScrollViewPriority::onTouchMoved, this);
    _touchListener->onTouchEnded = CC_CALLBACK_2(ScrollViewPriority::onTouchEnded, this);
    _touchListener->onTouchCancelled = CC_CALLBACK_2(ScrollViewPriority::onTouchCancelled, this);
    _eventDispatcher->addEventListenerWithFixedPriority(_touchListener, _priority);

    return true;
}

bool ScrollViewPriority::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* unused_event)
{
    if (ScrollView::onTouchBegan(touch, unused_event)) {
        if (_parent) {
            _parent->onTouchBegan(touch, unused_event);
        }

        return true;
    }

    return false;
}

void ScrollViewPriority::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* unused_event)
{
    if (!_parent || (_parent && !_parent->_isMove)) {
        ScrollView::onTouchMoved(touch, unused_event);
    }
    ////CCLOG("_scrooldistance: %f, %f", _scrollDistance.x, powf(_scrollDistance.y, 2));
    if (_parent && (_parent->_isMove || (powf(_scrollDistance.y, 2) < 4.0f && _scrollDistance.x > 2.0f))) {
        _parent->onTouchMoved(touch, unused_event);
    }
}

void ScrollViewPriority::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* unused_event)
{
    if (_parent) {
        _parent->onTouchEnded(touch, unused_event);
    }

    ScrollView::onTouchEnded(touch, unused_event);
}

void ScrollViewPriority::onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* unused_event)
{
    ////CCLOG("touches: %d", (int)_touches.size());
    if (_parent) {
        _parent->onTouchCancelled(touch, unused_event);
    }

    ScrollView::onTouchCancelled(touch, unused_event);
}

