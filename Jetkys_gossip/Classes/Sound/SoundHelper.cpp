#include "SoundHelper.h"
#include "UserDefaultManager.h"
#include "Native.h"

static SoundHelper* instance = NULL;


SoundHelper* SoundHelper::shareHelper()
{
    if (!instance) {
        instance = new SoundHelper();
    }
    return instance;
}

int SoundHelper::playEffect(RaceSceneSoundType type, bool sloop)
{
    unsigned int idSound;
    const char* soundName;

    switch (type) {
    case RUN:
        soundName = "No05.mp3";
        break;
    case BACK:
        soundName = "No07.mp3";
        break;
    case START_SIGNAL:
        soundName = "No35.mp3";
        break;
    case GOAL_FANFARE:
        soundName = "No15.mp3";
        break;
    case DICE_TAP:
        soundName = "No29_1.mp3";
        break;
    case DICE_ROTATION:
        soundName = "No29_2.mp3";
        break;
    case DICE_STOP:
        soundName = "No29_3.mp3";
        break;
    case RACE_START:
        soundName = "No04.mp3";
        break;
    case FAST_RUN:
        soundName = "No06.mp3";
        break;
    case ITEM_SAFETY_CONE:
        soundName = "No36.mp3";
        break;
    case ITEM_NITORO:
        soundName = "No33.mp3";
        break;
    case ITEM_PATOLAMP:
        soundName = "No36.mp3";
        break;
    case ITEM_TRAFFIC_CONTROL:
        soundName = "No08.mp3";
        break;
    case ITEM_THUNDER:
        soundName = "No13.mp3";
        break;
    case DICE_REPLAY_EFFECT:
        soundName = "No09.mp3";
        break;
    case STATUS_UP_EFFECT:
        soundName = "No33.mp3";
        break;
    case STATUS_DOWN_EFFECT:
        soundName = "No34.mp3";
        break;
    case GET_ITEM_EFFECT:
        soundName = "No10.mp3";
        break;
    case GET_SCORE_EFFECT:
        soundName = "No11.mp3";
        break;
    case SKILL_CHARGE_EFFECT:
        soundName = "No12.mp3";
        break;
    case CHARACTER_CUT_IN_EFFECT:
        soundName = "No28.mp3";
        break;
    case SKILL_CHARGE_END_EFFECT:
        soundName = "No27.mp3";
        break;
    case LOSE_EFFECT:
        soundName = "No14.mp3";
        break;
    case ENEMY_GOAL_EFFECT:
        soundName = "No32.mp3";
        break;
    case DROP_NOMAL_EFFECT:
        soundName = "No24.mp3";
        break;
    case DROP_GOOD_EFFECT:
        soundName = "No25.mp3";
        break;
    case DROP_EXCELLENT_EFFECT:
        soundName = "No26.mp3";
        break;
    default:
        break;
    }
    if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::SE_OPTION_STATUS, false)) {
        idSound = SimpleAudioEngine::getInstance()->playEffect(soundName, sloop);
    }
    return idSound;
}

int SoundHelper::playeMainSceneEffect(MainSceneSoundType type, bool sloop)
{
    unsigned int idSound;
    const char* soundName;
    switch (type) {
    case SOUND_BUY:
        soundName = "No22.mp3";
        break;
    case SOUND_SELL:
        soundName = "No23.mp3";
        break;
    case SOUND_GAGE_UP:
        soundName = "No16.mp3";    //
        break;
    case SOUND_GACHA_1:
        soundName = "No17_1.mp3";
        break;
    case SOUND_GACHA_2:
        soundName = "No17_2.mp3";
        break;
    case SOUND_GACHA_3:
        soundName = "No17_3.mp3";
        break;
    case SOUND_LEVEL_UP:
        soundName = "No19.mp3";    //
        break;
    case SOUND_TAP_FALSE:
        soundName = "No21.mp3";
        break;
    case SOUND_TAP_TRUE:
        soundName = "No20.mp3";
        break;
    default:
        break;
    }
    if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::SE_OPTION_STATUS, false)) {
        idSound = SimpleAudioEngine::getInstance()->playEffect(soundName, sloop);
    }
    return idSound;
}

void SoundHelper::playSplashSceneVoice()
{
    if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::VOICE_OPTION_STATUS, false)) {
        if (UserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::LEADER_CHARACTOR_MASTER_ID) > 0) {
            playCharacterVoiceEfect(UserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::LEADER_CHARACTOR_MASTER_ID), 11);
        } else {
            SimpleAudioEngine::getInstance()->stopEffect(UserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::VOICE_SOUND_ID));
            unsigned int idSound = SimpleAudioEngine::getInstance()->playEffect("company_voice.mp3", false);
            UserDefault::getInstance()->setIntegerForKey(UserDefaultManager::KEY::VOICE_SOUND_ID, idSound);
        }
    }
}

void SoundHelper::playTitleSceneVoice()
{
    if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::VOICE_OPTION_STATUS, false)) {
        if (UserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::LEADER_CHARACTOR_MASTER_ID) > 0) {
            SoundHelper::shareHelper()->playCharacterVoiceEfect(UserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::LEADER_CHARACTOR_MASTER_ID), 12);
        } else {
            SimpleAudioEngine::getInstance()->stopEffect(UserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::VOICE_SOUND_ID));
            unsigned int idSound = SimpleAudioEngine::getInstance()->playEffect("title_voice.mp3", false);
            UserDefault::getInstance()->setIntegerForKey(UserDefaultManager::KEY::VOICE_SOUND_ID, idSound);
        }
    }
}

void SoundHelper::stopEffect(unsigned int idSound)
{
    SimpleAudioEngine::getInstance()->stopEffect(idSound);
}

void SoundHelper::playBackgroundMusic(int type)
{
    SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    const char* soundName;
    if (type == 1) {
        soundName = "No02.mp3";
    } else {
        soundName = "No03.mp3";
    }
    if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::BGM_OPTION_STATUS, false)) {
        SimpleAudioEngine::getInstance()->preloadBackgroundMusic(soundName);
        SimpleAudioEngine::getInstance()->playBackgroundMusic(soundName, true);
    }
}

void pauseBackgroundMusic()
{
    SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}
void stopBackgroundMusic()
{
    SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}

void SoundHelper::playingMainSceneBackgroundMusic()
{
    SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::BGM_OPTION_STATUS, false)) {
        SimpleAudioEngine::getInstance()->preloadBackgroundMusic("No01.mp3");
        SimpleAudioEngine::getInstance()->playBackgroundMusic("No01.mp3", true);
    }
}

void SoundHelper::pauseAllMusic()
{
    SimpleAudioEngine::getInstance()->pauseAllEffects();
    SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

void SoundHelper::resumeAllMuic()
{
    SimpleAudioEngine::getInstance()->resumeAllEffects();
    SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}

void SoundHelper::stopAllMusic()
{
    SimpleAudioEngine::getInstance()->stopAllEffects();
    SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}

void SoundHelper::playCharacterVoiceEfect(const int mstCharacterId, const int wordId)
{
    if (mstCharacterId < 1 || wordId < 1) {
        return;
    }
    std::string voice = FileUtils::getInstance()->getWritablePath() + "Resources/" + StringUtils::format("voice_%d_%d.mp3", mstCharacterId, wordId);
    unsigned int idSound;
    if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::VOICE_OPTION_STATUS, false)) {
        SimpleAudioEngine::getInstance()->stopEffect(UserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::VOICE_SOUND_ID));
        #if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
            idSound = SimpleAudioEngine::getInstance()->playEffect(voice.c_str(), false);
            UserDefault::getInstance()->setIntegerForKey(UserDefaultManager::KEY::VOICE_SOUND_ID, idSound);
        #else
            Native::playCharacterVoice(voice.c_str());
        #endif
    }
}

void SoundHelper::stopVoiceEfect()
{
    #if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
        SimpleAudioEngine::getInstance()->stopEffect(UserDefault::getInstance()->getIntegerForKey(UserDefaultManager::KEY::VOICE_SOUND_ID));
    #else
        Native::playCharacterVoice("");         // this mean Stop Character Voice if it is playing
    #endif
}

void SoundHelper::playRaceSceneBackgroundMusic(int bgm_id)
{
    if (bgm_id < 1) {
        return;
    }

    SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::BGM_OPTION_STATUS, false)) {
        SimpleAudioEngine::getInstance()->preloadBackgroundMusic(StringUtils::format("bgm_%d.mp3", bgm_id).c_str());
        SimpleAudioEngine::getInstance()->playBackgroundMusic(StringUtils::format("bgm_%d.mp3", bgm_id).c_str(), true);
    }
}
