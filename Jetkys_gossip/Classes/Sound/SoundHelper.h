#ifndef __Dapgian__SoundHelper__
#define __Dapgian__SoundHelper__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"


USING_NS_CC;
using namespace CocosDenshion;

enum RaceSceneSoundType{
    RUN, BACK,
    START_SIGNAL,//使ってない
    GOAL_FANFARE, DICE_TAP, DICE_ROTATION, DICE_STOP, RACE_START, FAST_RUN,
    ITEM_SAFETY_CONE,//使ってない
    ITEM_NITORO, ITEM_PATOLAMP, ITEM_TRAFFIC_CONTROL,
    ITEM_THUNDER,//使ってない
    DICE_REPLAY_EFFECT, STATUS_UP_EFFECT, STATUS_DOWN_EFFECT, GET_ITEM_EFFECT, GET_SCORE_EFFECT, SKILL_CHARGE_EFFECT, CHARACTER_CUT_IN_EFFECT, SKILL_CHARGE_END_EFFECT, LOSE_EFFECT, ENEMY_GOAL_EFFECT, DROP_NOMAL_EFFECT, DROP_GOOD_EFFECT, DROP_EXCELLENT_EFFECT
};

enum MainSceneSoundType{
    SOUND_TAP_TRUE, SOUND_TAP_FALSE, SOUND_GACHA_1, SOUND_GACHA_2, SOUND_GACHA_3, SOUND_GAGE_UP, SOUND_LEVEL_UP,SOUND_BUY,SOUND_SELL
};

enum VoiceType{
    TITLE_CALL,
    COMPANY_CALL
};



#define SOUND_HELPER SoundHelper::shareHelper()

class SoundHelper {
public:
    static SoundHelper *shareHelper();
    
    void stopEffect(unsigned int idSound);
    
    int playEffect(RaceSceneSoundType type, bool sloop);
    void playBackgroundMusic(int type);
    void pauseBackgroundMusic();
    void stopBackgroundMusic();
    int playeMainSceneEffect(MainSceneSoundType type,bool sloop);
    void playCharacterVoiceEfect(const int mstCharacterId, const int wordId);
    void playSplashSceneVoice();
    void playTitleSceneVoice();
    
    // refactoring(main)
    void playingMainSceneBackgroundMusic();
    
    // refactoring(race)
    void playRaceSceneBackgroundMusic(int bgm_id);
    void pauseAllMusic();
    void resumeAllMuic();
    void stopAllMusic();
    
    // refactoring(gachadrop,newcharacter, otherscolection)
    void stopVoiceEfect();
};
#endif /* defined(__Dapgian__SoundHelper__) */
