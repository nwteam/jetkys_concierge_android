#ifndef __syanago__DBManager__
#define __syanago__DBManager__

#include "sqlite3.h"

#define DB_MANAGER CDBManager::shareDBManager()

#define DB_FILENAME "syanago_db.sqlite"

class CDBManager{
    
private:
    sqlite3 *_db;
    int _result;
public:
    CDBManager();
    ~CDBManager();
    static CDBManager* shareDBManager(void);
    bool openDB(void);
    void closeDB(void);
    sqlite3 *getDB(){
        return _db;
    }
};

#endif /* defined(__syanago__DBManager__) */
