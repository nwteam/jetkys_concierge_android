#ifndef __Syanago__DataPlayer__
#define __Syanago__DataPlayer__

#include <string>

#define PUBLIC_KEY "Af2d3qXgaWrWyF4C"
#define IV_HEX "00000000000000000000000000000000"
#define KEY_DID_SAVE "key_did_save"
#define KEY_IS_FIRST_INSTALL "key_is_first_install"

#define DATAPLAYER DataPlayer::getInstance()

class DataPlayer{
public:
    DataPlayer();
    ~DataPlayer();
    static DataPlayer *getInstance();
    
    std::string getIV();
    
    /**
     *
     UUID + YYMMDDHHiiss + ランダム4桁」
     ※）ハイフンを含めると、36桁 + 13桁 + 5桁 = 54桁
     例）6bffaf56-7af9-4d1a-a268-6195ca3c1de9-130609205011-A3c5
     */
    std::string getDID();
    
    /**
     *
      アプリ固有キー = Af2d3qXgaWrWyF4C
     "「アプリ固有キー:did:datetime」をSHA256でハッシュ化したもの。
     サーバー側でも同じ方法でハッシュ化＆比較して認証を行う。
     ※アプリ側のdatetimeと、サーバー側のdatetimeについて、600秒以内（管理画面で変更可）であれば有効。"
     */
    std::string getDIDEncrypted(std::string did, std::string iv);
    
    /**
     *
     "「アプリ固有キー:did:datetime」をSHA256でハッシュ化したもの。
     サーバー側でも同じ方法でハッシュ化＆比較して認証を行う。
     ※アプリ側のdatetimeと、サーバー側のdatetimeについて、600秒以内（管理画面で変更可）であれば有効。"
     */
    std::string getPlayerID();
    
    /**
     *
     「アプリ固有キー:did:datetime」をSHA256でハッシュ化したもの。
     *
     */
    std::string getDIDToken(std::string did, std::string datetime);
    
    std::string getPlayerIDToken(std::string datetime);
    
    std::string getDateTime();
    
    std::string getIPv4Address();
    
    bool isFirstInstall();
    void setFirstInstall(bool isInstall);
    
private:
    std::string iv;
    std::string _did;
};

#endif /* defined(__Syanago__DataPlayer__) */
