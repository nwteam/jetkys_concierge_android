#include "DataPlayer.h"
#include "cocos2d.h"
#include "Native.h"
#include "JDate.h"
#include "MacroDefines.h"
#include "SecurityUtil.h"
#include "GameDefines.h"
#include "PlayerController.h"
#include "jCommon.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

USING_NS_CC;

static DataPlayer* instance = nullptr;

DataPlayer::DataPlayer()
{
    _did = getDID();
}

DataPlayer::~DataPlayer()
{}

DataPlayer* DataPlayer::getInstance()
{
    if (!instance) {
        instance = new DataPlayer();
    }
    return instance;
}

bool DataPlayer::isFirstInstall()
{
    return UserDefault::getInstance()->getBoolForKey(KEY_IS_FIRST_INSTALL, false);
}

void DataPlayer::setFirstInstall(bool isInstall)
{
    UserDefault::getInstance()->setBoolForKey(KEY_IS_FIRST_INSTALL, isInstall);
}

std::string DataPlayer::getDID()
{
    //    アプリ初回登録時にDIDを発行したら、キーチェーンアクセスとSQLiteにその値を保持する。
    //    ログインの度に、SQLite内のDIDをキーチェーンアクセスに書き込む。
    //    これはプロビジョニングファイルを更新した際、キーチェーンが空になったときの救済処理とする。
    //    ------ 以下、田矢氏よりアドバイス -------
    //    キーチェーンの情報はプロビジョニングファイルに基づきます。
    //    ですので、キーチェーン情報に頼ったアプリの場合、一度サービスインしたら絶対にリリース用のプロビジョニングファイルを変えてはいけません。データ消えます。
    if (_did != "") {
        return _did;
    }

    std::string result = UserDefault::getInstance()->getStringForKey(KEY_DID_SAVE, "");
    if (result != "") {
        Native::saveOsDIDInKeychain(result);
        return result;
    } else {
        result = Native::getOsDIDInKeychain();
        if (result == "") {
            setFirstInstall(true);
            std::string did = Native::getOSUUID();
            did += "-" + JDate::getTimeWithFormat("%y%m%d%H%M%S");
            char randomNo[4] = {0};
            sprintf(randomNo, "%04d", 1 + (arc4random() % 999));
            did += "-";
            did += randomNo;

            result = did;
            UserDefault::getInstance()->setStringForKey(KEY_DID_SAVE, result);
            Native::saveOsDIDInKeychain(result);
            return result;
        } else {
            UserDefault::getInstance()->setStringForKey(KEY_DID_SAVE, result);
            return result;
        }
    }
    return result;
}



std::string DataPlayer::getDIDEncrypted(std::string did, std::string iv)
{
    return SecurityUtil::encrypt(did.c_str(), iv.c_str());
}

std::string DataPlayer::getPlayerID()
{
    return PLAYERCONTROLLER->getPlayerID();
}

std::string DataPlayer::getDIDToken(std::string did, std::string datetime)
{
    std::string didToken = PUBLIC_KEY;
    didToken += ":" + did + ":" + datetime;
    return SecurityUtil::sha256(didToken.c_str());
}

std::string DataPlayer::getPlayerIDToken(std::string datetime)
{
    std::string didToken = PUBLIC_KEY;
    didToken += ":" + PLAYERCONTROLLER->_player->getInstallKey() + ":" + PLAYERCONTROLLER->getPlayerID() + ":" + datetime;
    return SecurityUtil::sha256(didToken.c_str());
}

std::string DataPlayer::getDateTime()
{
    return JDate::getTimeWithFormat("%Y%m%d%H%M%S");
}

std::string DataPlayer::getIV()
{
    return SecurityUtil::getIV();
}

std::string DataPlayer::getIPv4Address()
{
    std::string address = "";
    struct ifaddrs* interfaces = NULL;
    struct ifaddrs* temp_addr = NULL;
    int success = 0;

    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if (temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone

                if (strcmp(temp_addr->ifa_name, "en0") == 0) {
                    address = inet_ntoa(((struct sockaddr_in*)temp_addr->ifa_addr)->sin_addr);
                }
            }

            temp_addr = temp_addr->ifa_next;
        }
    }

    // Free memory
    freeifaddrs(interfaces);
    return address;
}
