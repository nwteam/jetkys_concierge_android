#include "LastModifiedConverter.h"
#include "UserDefaultManager.h"
#include "JDate.h"
#include "Version.h"
#include "Native.h"
#include "Model.h"

const std::string LastModifiedConverter::getLastModified(std::string table_name)
{
    if (UserDefault::getInstance()->getBoolForKey(UserDefaultManager::KEY::IS_RESET_MASTER_TABLE) == false) {
        if (Version::isNotNeedUpdate(Native::getApplicationVersion(), "1.4.6")) {
            return JDate::getTimeIgnoreNoNumber(Model::getLastModified(table_name));
        } else {
            // force full update
            return "20140101000000";
        }
    } else {
        return "20140101000000";
    }
}