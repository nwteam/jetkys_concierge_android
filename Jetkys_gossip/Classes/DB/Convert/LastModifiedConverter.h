#ifndef __syanago__LastModifiedConverter__
#define __syanago__LastModifiedConverter__

#include "cocos2d.h"

USING_NS_CC;

class LastModifiedConverter
{
public:
    static const std::string getLastModified(std::string table_name);
    
};

#endif
