#ifndef __Syanago__PlayerController__
#define __Syanago__PlayerController__

#include "PlayerModel.h"
#include "PlayerCharactersModel.h"
#include "PlayerTeamModel.h"
#include "PlayerPartModel.h"
#include "PlayerItemUnitModel.h"
#include "PlayerPictureBookModel.h"
#include "PlayerCoursesModel.h"
#include "PlayerCharacterComparator.h"
#include "PlayerPartsComparator.h"
#include "SortPlayerCharacter.h"
#include "SortPlayerParts.h"
#include "UncompletedMissionCourse.h"

#define PLAYERCONTROLLER PlayerController::getInstance()

class PlayerController {
public:
    PlayerController();
    ~PlayerController();
    
    static PlayerController *getInstance();
    
    void saveNewPlayer(Json* json);
    void updatePlayerController(Json* data);
    
    //characters
    void addCharacter(Json* json, int lock, int is_new = -1);
    void updateCharacter(Json* json, int lock, int is_new = -1);
    void deleteCharacter(int ID);
    
    //Player part
    void savePart(int ID, int player_id, int parts_id, std::string created, std::string modified);
    void addPart(int ID, int player_id, int parts_id, std::string created, std::string modified, int is_new = -1);
    void deletePart(int ID);
    
    //Player item unit
    void saveItemUnit(int player_id, int item_unit_id, int item_unit_kind, std::string created, std::string modified );
    void addItemUnit(int player_id, int item_unit_id, int item_unit_kind, std::string created, std::string modified );
    
    //PictureBook & Treasure
    void addPictureBook(int player_id, int mst_character_id, int number_of_holdings, int number_of_contacts);
    
    int findIndexOfPlayerPartId(int player_part_id);
    int findIndexOfPlayerPartSortedId(int player_part_id);
    
    std::vector<int> getPlayerCharacterIdsUsed();
    std::vector<int> getPlayerPartIdsUsed(int exceptPlayerCharacterId = -1);
    
    void resetPlayerCharacterSorted();
    void resetPlayerPartsSorted();
    
    void sortPlayerCharacter(const PlayerCharacterComparator::Comparator& comparator);
    void sortPlayerParts(const PlayerPartsComparator::Comparator& comparator);
    
    std::string getNewPlayerModifide();
    std::string getNewPlayerCharacterModifide();
    std::string getNewItemUnitModifide();
    std::string getNewPartsModifide();
    std::string getNewCourseModifide();
    
    bool isCleared(int courseId);
    int getClearCourseBage(int courseId);
    std::vector<int> getPlayerCharacterDiceValues(const int playerCharacterId);
    std::vector<int> getPlayerCharacterPartsIds(const int playerCharacterId);
    
    void setUncompleteMissionCourses(Json* data);
    void updatePlayerTeam(Json *json);
    
    bool hasFairy();
    
    bool isExistPlayer();
    std::string getPlayerID();
    
    
    CC_SYNTHESIZE_READONLY(int, activeMissionCount, ActiveMissionCount);
    CC_SYNTHESIZE_READONLY(std::shared_ptr<UncompletedMissionCourse>, uncompleteMissionCourses, UncompleteMissionCourses);
    
    PlayerModel *_player;
    std::map<int, PlayerCharactersModel*> _playerCharacterModels;
    PlayerTeamModel* _playerTeams;
    std::vector<PlayerPartModel*> _playerParts;
    std::vector<PlayerItemUnitModel*> _playerItems;
    std::vector<PlayerPictureBookModel*>_playerPictureBook;
    std::vector<PlayerCoursesModel*> _playerCourses;
    std::vector<PlayerCharactersModel*> _playerCharactersSorted;
    std::vector<PlayerPartModel*> _playerPartsSorted;
    
    //save messages from server
    std::vector<std::string> _messages;
    int _numberOfPendingFriend;
    int _numberOfNotice;
    int _numberOfCompensation;

    SortPlayerCharacter::KEY currentSelectedSortCharacterKey;
    SortPlayerParts::KEY currentSelectedSortPartsKey;

private:
    void updatePlayer(Json *json);
    void updatePlayerCharacteres(Json *json);
    void updatePlayerCourses(Json *json);
    void updatePlayerItemUnit(Json *json);
    void updatePlayerPart(Json *json);
    void setActiveMissionCount(Json* data);
    
    
};


#endif /* defined(__Syanago__PlayerController__) */
