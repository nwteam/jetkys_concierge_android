#include "PlayerController.h"
#include "GameDefines.h"
#include "DataPlayer.h"
#include "editor-support/spine/Json.h"
#include "PartModel.h"

static PlayerController* instance = nullptr;

PlayerController* PlayerController::getInstance()
{
    if (!instance) {
        instance = new PlayerController();
    }
    return instance;
}

PlayerController::PlayerController():
    _messages(std::vector<std::string>())
    , _numberOfPendingFriend(0)
    , _numberOfNotice(0)
    , _numberOfCompensation(0)
    , _player(nullptr)
    , _playerTeams(new PlayerTeamModel())
    , _playerParts(std::vector<PlayerPartModel*>())
    , _playerItems(std::vector<PlayerItemUnitModel*>())
    , _playerCourses(std::vector<PlayerCoursesModel*>())
    , _playerCharactersSorted(std::vector<PlayerCharactersModel*>())
    , _playerPartsSorted(std::vector<PlayerPartModel*>())
    , currentSelectedSortCharacterKey(SortPlayerCharacter::KEY::CREATED)
    , currentSelectedSortPartsKey(SortPlayerParts::KEY::DISPLAY_NO)
{
    for (int i = 0; i < 25; i++) {
        _messages.push_back("");
    }
    _player = PlayerModel::getUser();
}

PlayerController::~PlayerController()
{
    if (_player)
        delete _player;
    _playerCharactersSorted.clear();
    _playerPartsSorted.clear();

    CC_SAFE_DELETE(_playerTeams);
    delete _playerTeams;

    for (std::pair<int, PlayerCharactersModel*>pair : _playerCharacterModels) {
        delete pair.second;
    }
    _playerCharacterModels.clear();

    for (int i = 0; i < _playerParts.size(); i++) {
        delete _playerParts.at(i);
    }
    _playerParts.clear();

    for (int i = 0; i < _playerItems.size(); i++) {
        delete _playerItems.at(i);
    }
    _playerItems.clear();

    for (int i = 0; i < _playerCourses.size(); i++) {
        delete _playerCourses.at(i);
    }
    _playerCourses.clear();
}

void PlayerController::updatePlayerController(Json* data)
{
    updatePlayer(Json_getItem(data, "players"));
    updatePlayerCharacteres(Json_getItem(data, "player_characters"));
    updatePlayerTeam(Json_getItem(data, "player_teams"));
    updatePlayerPart(Json_getItem(data, "player_parts"));
    updatePlayerItemUnit(Json_getItem(data, "player_item_units"));
    updatePlayerCourses(Json_getItem(data, "player_courses"));
    setUncompleteMissionCourses(data);
    setActiveMissionCount(data);
    UserDefault::getInstance()->setBoolForKey(KEY_FIRST_CREATE_PLAYER, false);
}

void PlayerController::saveNewPlayer(Json* json)
{
    if (_player) {
        _player->deleteRow(StringUtils::format("%lld", _player->getID()));
        delete _player;
    }
    Json* data = Json_getItem(json, "data");
    _player = new PlayerModel();
    _player->setID(atoll(Json_getString(data, "player_id", "")));
    _player->setInstallKey(Json_getString(data, "install_key", ""));
    _player->setContactCode(Json_getString(data, "contact_code", ""));
    _player->insert();

    DataPlayer::getInstance()->setFirstInstall(false);
}

void PlayerController::updatePlayer(Json* jsonPlayer)
{
    _player->setID(atoll(Json_getString(jsonPlayer, "id", "")));
    _player->setPlayerName(Json_getString(jsonPlayer, "player_name", ""));
    _player->setRank(atoi(Json_getString(jsonPlayer, "rank", "")));
    _player->setTotalExp(atoll(Json_getString(jsonPlayer, "total_exp", "")));
    _player->setFuel(atoi(Json_getString(jsonPlayer, "fuel", "")));
    _player->setGarageSize(atoi(Json_getString(jsonPlayer, "garage_size", "")));
    _player->setFreeCoin(atoi(Json_getString(jsonPlayer, "free_coin", "")));
    _player->setPayCoin(atoi(Json_getString(jsonPlayer, "pay_coin", "")));
    _player->setFreeToken(atoi(Json_getString(jsonPlayer, "free_token", "")));
    _player->setPayToken(atoi(Json_getString(jsonPlayer, "pay_token", "")));
    _player->setFriendPoint(atoi(Json_getString(jsonPlayer, "friend_point", "")));
    _player->setHelpCount(atoi(Json_getString(jsonPlayer, "help_count", "")));
    _player->setHelpCountAsFriend(atoi(Json_getString(jsonPlayer, "help_count_as_friend", "")));
    _player->setPushID(Json_getString(jsonPlayer, "push_id", ""));
    _player->setInstallKey(Json_getString(jsonPlayer, "install_key", ""));
    _player->setDeviceType(atoi(Json_getString(jsonPlayer, "device_type", "")));
    _player->setOsVersion(Json_getString(jsonPlayer, "os_version", ""));
    _player->setInvitedPlayerID(atoll(nullIgrore(Json_getString(jsonPlayer, "invited_player_id", "")).c_str()));
    _player->setInvitedPlayerRewardFlag(atoi(Json_getString(jsonPlayer, "invited_player_reward_flag", "")));
    _player->setNumberOfInvitation(atoi(Json_getString(jsonPlayer, "number_of_invitation", "")));
    _player->setNumberOfInvitationSuccess(atoi(Json_getString(jsonPlayer, "number_of_invitation_success", "")));
    _player->setLastLogged(Json_getString(jsonPlayer, "last_logged", ""));
    _player->setBeforeLastLogged(nullIgrore(Json_getString(jsonPlayer, "before_last_logged", "")).c_str());
    _player->setNumberOfContinuityLogin(atoi(Json_getString(jsonPlayer, "number_of_continuity_login", "")));
    _player->setNumberOfTotalLogin(atoi(Json_getString(jsonPlayer, "number_of_total_login", "")));
    _player->setNumberOfPurchases(atoi(Json_getString(jsonPlayer, "number_of_purchases", "")));
    _player->setJustBeforeOwnerHelp(atoi(Json_getString(jsonPlayer, "just_before_owner_help", "")));
    _player->setJustBeforeFriendHelp(atoi(Json_getString(jsonPlayer, "just_before_friend_help", "")));
    _player->setCurrentCourseId(atoi(nullIgrore(Json_getString(jsonPlayer, "current_course_id", "")).c_str()));
    _player->setGameStep(atoi(Json_getString(jsonPlayer, "game_step", "")));
    _player->setCreated(Json_getString(jsonPlayer, "created", ""));
    _player->setModified(Json_getString(jsonPlayer, "modified", ""));
    _player->setFuelTweetCount(atoi(Json_getString(jsonPlayer, "fuel_tweet_count", "0")));
    _player->setContactCode(Json_getString(jsonPlayer, "contact_code", ""));
    _player->update();
}


void PlayerController::addCharacter(Json* child, int lock, int is_new)
{
    auto character = new PlayerCharactersModel();
    character->setID(atoi(Json_getString(child, "id", "")));
    character->setValues(child, lock, is_new);
    _playerCharacterModels[character->getID()] = character;
}


void PlayerController::updateCharacter(Json* child, int lock, int is_new)
{
    auto character = PLAYERCONTROLLER->_playerCharacterModels[atoi(Json_getString(child, "id", ""))];
    character->setValues(child, lock, is_new);
}

void PlayerController::deleteCharacter(int ID)
{
    _playerCharacterModels.erase(ID);
}

void PlayerController::deletePart(int ID)
{
    int pcIndex = findIndexOfPlayerPartId(ID);
    _playerParts.erase(_playerParts.begin() + pcIndex);
    resetPlayerPartsSorted();
}

// Player part
void PlayerController::savePart(int ID, int player_id, int parts_id, std::string created, std::string modified)
{
    auto part = new PlayerPartModel();
    part->setID(ID);
    part->setPlayerId(player_id);
    part->setPartsId(parts_id);
    part->setCreated(created);
    part->setModified(modified);

    if (part->isExist(StringUtils::format("%d", ID))) {
        part->update();
    } else {
        part->insert();
    }
    delete part;
}

void PlayerController::addPart(int ID, int player_id, int parts_id, std::string created, std::string modified, int is_new)
{
    auto part = new PlayerPartModel();
    part->setID(ID);
    part->setPlayerId(player_id);
    part->setPartsId(parts_id);
    part->setCreated(created);
    part->setModified(modified);
    if (is_new != -1) {
        part->saveIsNew(is_new);
    } else {
        if (UserDefault::getInstance()->getBoolForKey(KEY_FIRST_CREATE_PLAYER)) {
            part->saveIsNew(0);
        } else {
            part->getIsNewFromFile();
        }
    }

    _playerParts.push_back(part);
}

// Player item unit
void PlayerController::saveItemUnit(int player_id, int item_unit_id, int item_unit_kind, std::string created, std::string modified)
{
    auto item = new PlayerItemUnitModel();
    item->setPlayerId(player_id);
    item->setItemUnitId(item_unit_id);
    item->setItemUnitKind(item_unit_kind);
    item->setCreated(created);
    item->setModified(modified);

    if (item->isExist()) {
        item->update();
    } else
        item->insert();
    delete item;
}

void PlayerController::addItemUnit(int player_id, int item_unit_id, int item_unit_kind, std::string created, std::string modified)
{
    auto item = new PlayerItemUnitModel();
    item->setPlayerId(player_id);
    item->setItemUnitId(item_unit_id);
    item->setItemUnitKind(item_unit_kind);
    item->setCreated(created);
    item->setModified(modified);

    _playerItems.push_back(item);
}


void PlayerController::addPictureBook(int player_id, int mst_character_id, int number_of_holdings, int number_of_contacts)
{
    auto picBook = new PlayerPictureBookModel();
    picBook->setPlayerId(player_id);
    picBook->setMstCharactersId(mst_character_id);
    picBook->setNumberOfHoldings(number_of_holdings);
    picBook->setNumberOfContacts(number_of_contacts);

    _playerPictureBook.push_back(picBook);
}

void PlayerController::updatePlayerTeam(Json* jsonDatas)
{
    if (!jsonDatas) {
        return;
    }
    CC_SAFE_DELETE(_playerTeams);
    for (Json* child = jsonDatas->child; child; child = child->next) {
        _playerTeams = new PlayerTeamModel();
        _playerTeams->setPlayerId(atoi(Json_getString(child, "player_id", "")));
        _playerTeams->setTeamNo(atoi(Json_getString(child, "team_no", "")));
        _playerTeams->setLearderCharacterId(atoiNull(Json_getString(child, "learder_character_id", "")));
        _playerTeams->setSupport1CharacterId(atoiNull(Json_getString(child, "support1_character_id", "")));
        _playerTeams->setSupport2CharacterId(atoiNull(Json_getString(child, "support2_character_id", "")));
        _playerTeams->setSupport3CharacterId(atoiNull(Json_getString(child, "support3_character_id", "")));
        _playerTeams->setCreated(Json_getString(child, "created", ""));
        _playerTeams->setModified(Json_getString(child, "modified", ""));
    }
}

std::vector<int>PlayerController::getPlayerCharacterIdsUsed()
{
    std::vector<int>pcUsed;
    if (_playerTeams->getLearderCharacterId() > 0) {
        pcUsed.push_back(_playerTeams->getLearderCharacterId());
    }
    if (_playerTeams->getSupport1CharacterId() > 0) {
        pcUsed.push_back(_playerTeams->getSupport1CharacterId());
    }
    if (_playerTeams->getSupport2CharacterId() > 0) {
        pcUsed.push_back(_playerTeams->getSupport2CharacterId());
    }
    if (_playerTeams->getSupport3CharacterId() > 0) {
        pcUsed.push_back(_playerTeams->getSupport3CharacterId());
    }
    return pcUsed;
}

std::vector<int>PlayerController::getPlayerPartIdsUsed(int exceptPlayerCharacterId)
{
    std::vector<int>results;
    for (std::pair<int, PlayerCharactersModel*>pair : _playerCharacterModels) {
        // 現在選択されているキャラクターが持っている装備は別の箇所で判定するので除いている
        if (pair.first == exceptPlayerCharacterId) {
            continue;
        }
        if (pair.second->getPartsSlot1()) {
            results.push_back(pair.second->getPartsSlot1());
        }
        if (pair.second->getPartsSlot2()) {
            results.push_back(pair.second->getPartsSlot2());
        }
    }
    return results;
}

int PlayerController::findIndexOfPlayerPartId(int player_part_id)
{
    for (int i = 0; i < _playerParts.size(); i++) {
        if (_playerParts.at(i)->getID() == player_part_id) {
            return i;
        }
    }
    return -1;
}

int PlayerController::findIndexOfPlayerPartSortedId(int player_part_id)
{
    for (int i = 0; i < _playerPartsSorted.size(); i++) {
        if (_playerPartsSorted.at(i)->getID() == player_part_id) {
            return i;
        }
    }
    return -1;
}


void PlayerController::resetPlayerCharacterSorted()
{
    _playerCharactersSorted.clear();
    for (std::pair<int, PlayerCharactersModel*>pair : _playerCharacterModels) {
        _playerCharactersSorted.push_back(pair.second);
    }
}

void PlayerController::resetPlayerPartsSorted()
{
    _playerPartsSorted.clear();
    _playerPartsSorted = _playerParts;
}

void PlayerController::sortPlayerCharacter(const PlayerCharacterComparator::Comparator& comparator)
{
    _playerCharactersSorted.clear();
    for (std::pair<int, PlayerCharactersModel*>pair : _playerCharacterModels) {
        _playerCharactersSorted.push_back(pair.second);
    }
    std::sort(_playerCharactersSorted.begin(), _playerCharactersSorted.end(), comparator);
}

void PlayerController::sortPlayerParts(const PlayerPartsComparator::Comparator &comparator)
{
    _playerPartsSorted.clear();
    _playerPartsSorted = _playerParts;
    std::sort(_playerPartsSorted.begin(), _playerPartsSorted.end(), comparator);
}

std::string PlayerController::getNewPlayerModifide()
{
    std::string modifide = "1000-01-01 00:00:00";
    if (_player) {
        modifide = _player->getModified();
    }
    return modifide;
}

std::string PlayerController::getNewPlayerCharacterModifide()
{
    std::string modifide = "1000-01-01 00:00:00";
    int modifideInt = 0;
    for (std::pair<int, PlayerCharactersModel*>pair : _playerCharacterModels) {
        std::string tempModifide = pair.second->getModified();
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), '-'), tempModifide.end());
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), ':'), tempModifide.end());
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), ' '), tempModifide.end());
        int tempModifideint = std::atoi(tempModifide.c_str());
        if (tempModifideint > modifideInt) {
            modifideInt = tempModifideint;
            modifide = pair.second->getModified();
        }
    }
    return modifide;
}

std::string PlayerController::getNewItemUnitModifide()
{
    std::string modifide = "1000-01-01 00:00:00";
    int modifideInt = 0;
    for (int i = 0; i < _playerItems.size(); i++) {
        std::string tempModifide = _playerItems.at(i)->getModified();
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), '-'), tempModifide.end());
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), ':'), tempModifide.end());
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), ' '), tempModifide.end());
        int tempModifideint = std::atoi(tempModifide.c_str());
        if (tempModifideint > modifideInt) {
            modifideInt = tempModifideint;
            modifide = _playerItems.at(i)->getModified();
        }
    }
    return modifide;
}

std::string PlayerController::getNewPartsModifide()
{
    std::string modifide = "1000-01-01 00:00:00";
    int modifideInt = 0;
    for (int i = 0; i < _playerParts.size(); i++) {
        std::string tempModifide = _playerParts.at(i)->getModified();
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), '-'), tempModifide.end());
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), ':'), tempModifide.end());
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), ' '), tempModifide.end());
        int tempModifideint = std::atoi(tempModifide.c_str());
        if (tempModifideint > modifideInt) {
            modifideInt = tempModifideint;
            modifide = _playerParts.at(i)->getModified();
        }
    }
    return modifide;
}

std::string PlayerController::getNewCourseModifide()
{
    std::string modifide = "1000-01-01 00:00:00";
    int modifideInt = 0;
    for (int i = 0; i < _playerCourses.size(); i++) {
        std::string tempModifide = _playerCourses.at(i)->getModified();
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), '-'), tempModifide.end());
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), ':'), tempModifide.end());
        tempModifide.erase(std::remove(tempModifide.begin(), tempModifide.end(), ' '), tempModifide.end());
        int tempModifideint = std::atoi(tempModifide.c_str());
        if (tempModifideint > modifideInt) {
            modifideInt = tempModifideint;
            modifide = _playerCourses.at(i)->getModified();
        }
    }
    return modifide;
}



void PlayerController::updatePlayerCharacteres(Json* jsonDatas)
{
    if (!jsonDatas) {
        return;
    }

    for (std::pair<int, PlayerCharactersModel*>pair : _playerCharacterModels) {
        delete pair.second;
    }
    _playerCharacterModels.clear();

    for (Json* child = jsonDatas->child; child; child = child->next) {
        addCharacter(child, atoiNull(Json_getString(child, "lock", "")));
    }
}




void PlayerController::updatePlayerCourses(Json* jsonDatas)
{
    if (!jsonDatas) {
        return;
    }
    for (auto tmp : _playerCourses) {
        delete tmp;
    }
    _playerCourses.clear();

    for (Json* child = jsonDatas->child; child; child = child->next) {
        auto course = new PlayerCoursesModel();
        course->setPlayerId(atoi(Json_getString(child, "player_id", "")));
        course->setCourseId(atoi(Json_getString(child, "course_id", "")));
        course->setBadge(atoiNull(Json_getString(child, "badge", "")));
        course->setCreated(Json_getString(child, "created", ""));
        course->setModified(Json_getString(child, "modified", ""));
        _playerCourses.push_back(course);
    }
}

void PlayerController::updatePlayerItemUnit(Json* jsonDatas)
{
    if (!jsonDatas) {
        return;
    }
    for (auto tmp : _playerItems) {
        delete tmp;
    }
    _playerItems.clear();
    for (Json* child = jsonDatas->child; child;
         child = child->next) {
        addItemUnit(
            atoiNull(Json_getString(child, "player_id", "")),
            atoiNull(Json_getString(child, "item_unit_id", "")),
            atoiNull(Json_getString(child, "item_unit_kind", "")),
            Json_getString(child, "created", ""),
            Json_getString(child, "modified", ""));
    }
}

void PlayerController::updatePlayerPart(Json* jsonDatas)
{
    if (!jsonDatas) {
        return;
    }
    for (auto tmp : _playerParts) {
        delete tmp;
    }
    _playerParts.clear();
    for (Json* child = jsonDatas->child; child;
         child = child->next) {
        addPart(
            atoi(Json_getString(child, "id", "")),
            atoi(Json_getString(child, "player_id", "")),
            atoi(Json_getString(child, "parts_id", "")),
            Json_getString(child, "created", ""),
            Json_getString(child, "modified", ""));
    }
}

bool PlayerController::isCleared(int courseId)
{
    bool clearFlag = false;
    for (int i = 0; i < _playerCourses.size(); i++) {
        if (courseId == _playerCourses.at(i)->getCourseId()) {
            clearFlag = true;
        }
    }
    return clearFlag;
}

int PlayerController::getClearCourseBage(int courseId)
{
    int bage = -1;
    for (int i = 0; i < _playerCourses.size(); i++) {
        if (courseId == _playerCourses.at(i)->getCourseId()) {
            bage = _playerCourses.at(i)->getBadge();
        }
    }
    return bage;
}

std::vector<int>PlayerController::getPlayerCharacterDiceValues(const int playerCharacterId)
{
    std::vector<int>result;
    const auto playerCharacterModel = PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId];
    result.push_back(playerCharacterModel->getDiceValue1());
    result.push_back(playerCharacterModel->getDiceValue2());
    result.push_back(playerCharacterModel->getDiceValue3());
    result.push_back(playerCharacterModel->getDiceValue4());
    result.push_back(playerCharacterModel->getDiceValue5());
    result.push_back(playerCharacterModel->getDiceValue6());
    result.push_back(playerCharacterModel->getDiceValue7());
    result.push_back(playerCharacterModel->getDiceValue8());
    result.push_back(playerCharacterModel->getDiceValue9());
    return result;
}

std::vector<int>PlayerController::getPlayerCharacterPartsIds(const int playerCharacterId)
{
    std::vector<int>result;
    const auto playerCharacterModel = PLAYERCONTROLLER->_playerCharacterModels[playerCharacterId];
    result.push_back(playerCharacterModel->getPartsSlot1());
    result.push_back(playerCharacterModel->getPartsSlot2());
    result.push_back(playerCharacterModel->getPartsSlot3());
    result.push_back(playerCharacterModel->getPartsSlot4());
    return result;
}

void PlayerController::setActiveMissionCount(Json* data)
{
    activeMissionCount =  atoi(Json_getString(data, "active_mission_count", ""));
}

void PlayerController::setUncompleteMissionCourses(Json* data)
{
    std::shared_ptr<UncompletedMissionCourse>model(new UncompletedMissionCourse(data));
    uncompleteMissionCourses = model;
}

bool PlayerController::hasFairy()
{
    PlayerCharactersModel* leader = _playerCharacterModels[_playerTeams->getLearderCharacterId()];
    // find parts index
    int cpIndex1 =  leader->getPartsSlot(0) > 0 ? findIndexOfPlayerPartId(leader->getPartsSlot(0)) : -1;
    int cpIndex2 =  leader->getPartsSlot(1) > 0 ? findIndexOfPlayerPartId(leader->getPartsSlot(1)) : -1;
    // find parts id
    int aPartID1 = cpIndex1 >= 0 ? _playerParts.at(cpIndex1)->getPartsId() : -1;
    int aPartID2 = cpIndex2 >= 0 ? _playerParts.at(cpIndex2)->getPartsId() : -1;
    // find parts type
    std::shared_ptr<PartModel>partsModel = nullptr;
    if (aPartID1 > 0) {
        partsModel = PartModel::find(aPartID1);
    }
    if (aPartID2 > 0) {
        partsModel = PartModel::find(aPartID2);
    }
    return partsModel != nullptr && partsModel->getType() == 2;
}


std::string PlayerController::getPlayerID()
{
    return StringUtils::format("%lld", _player->getID());
}


bool PlayerController::isExistPlayer()
{
    return (_player != nullptr);
}











