#include "DBManager.h"
#include "cocos2d.h"

using namespace cocos2d;

static CDBManager* _cdbManager = NULL;

CDBManager::CDBManager() {}

CDBManager::~CDBManager() {}

CDBManager* CDBManager::shareDBManager()
{
    if (_cdbManager == NULL) {
        _cdbManager = new CDBManager();
    }
    return _cdbManager;
}

bool CDBManager::openDB()
{
    FileUtils* fileUtil = FileUtils::getInstance();
    std::string path = fileUtil->getWritablePath() + DB_FILENAME;

    FILE* file = fopen(path.c_str(), "r");
    // ユーザーDB初期化
    if (file == nullptr) {
        // 初期DBコピー
        Data data = fileUtil->getDataFromFile(DB_FILENAME);
        file = fopen(path.c_str(), "wb");
        fwrite(data.getBytes(), data.getSize(), 1, file);
    }
    fclose(file);

    _result = sqlite3_open(path.c_str(), &_db);
    if (_result != SQLITE_OK) {
        sqlite3_close(_db);
        return false;
    } else {
        return true;
    }
}

void CDBManager::closeDB()
{
    // close file sqlite
    sqlite3_close(_db);
}