#ifndef __syanago__PlayerCharactersModel__
#define __syanago__PlayerCharactersModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class PlayerCharactersModel : public Model {
public:
    PlayerCharactersModel();
    
    void setValues(Json* child, int lock, int is_new = -1);
    
    int getDiceValue(int index);
    
    std::vector<int> getDiceValues();
    
    int getPartNumber();
    int getSaikoroNumber();
    
    int getPartsSlot(int index);
    bool isAnyPartsEquiped();
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(int, player_id, PlayerId);
    CC_SYNTHESIZE(int, characters_id, CharactersId);
    CC_SYNTHESIZE(int, exp, Exp);
    CC_SYNTHESIZE(int, level, Level);
    CC_SYNTHESIZE(int, exercise, Exercise);
    CC_SYNTHESIZE(int, reaction, Reaction);
    CC_SYNTHESIZE(int, decision, Decision);
    CC_SYNTHESIZE(int, dice_value1, DiceValue1);
    CC_SYNTHESIZE(int, dice_value2, DiceValue2);
    CC_SYNTHESIZE(int, dice_value3, DiceValue3);
    CC_SYNTHESIZE(int, dice_value4, DiceValue4);
    CC_SYNTHESIZE(int, dice_value5, DiceValue5);
    CC_SYNTHESIZE(int, dice_value6, DiceValue6);
    CC_SYNTHESIZE(int, dice_value7, DiceValue7);
    CC_SYNTHESIZE(int, dice_value8, DiceValue8);
    CC_SYNTHESIZE(int, dice_value9, DiceValue9);
    CC_SYNTHESIZE(int, free_dice7_count, FreeDice7Count);
    CC_SYNTHESIZE(int, free_dice8_count, FreeDice8Count);
    CC_SYNTHESIZE(int, free_dice9_count, FreeDice9Count);
    CC_SYNTHESIZE(int, dice_custom_number, DiceCustomNumber)
    CC_SYNTHESIZE(int, parts_slot1, PartsSlot1);
    CC_SYNTHESIZE(int, parts_slot2, PartsSlot2);
    //未使用
    CC_SYNTHESIZE(int, parts_slot3, PartsSlot3);
    CC_SYNTHESIZE(int, parts_slot4, PartsSlot4);
    
    CC_SYNTHESIZE(int, skill_gauge, SkillGauge);
    CC_SYNTHESIZE(float, dear_value, DearValue);
    CC_SYNTHESIZE(int, lock, Lock);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
    //0: not new(default), 1: new
    CC_SYNTHESIZE(int, is_new, IsNew);
    
    void saveIsNew(int isNew);
    int getIsNewFromFile();
};

#endif /* defined(__syanago__PlayersCharacterModel__) */
