#ifndef __Syanago__PlayerItemUnitModel__
#define __Syanago__PlayerItemUnitModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class PlayerItemUnitModel : public Model {
public:
    PlayerItemUnitModel();
    
    CC_SYNTHESIZE(int, player_id, PlayerId);
    CC_SYNTHESIZE(int, item_unit_id, ItemUnitId);
    CC_SYNTHESIZE(int, item_unit_kind, ItemUnitKind);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);

};

#endif /* defined(__Syanago__PlayerItemUnitModel__) */
