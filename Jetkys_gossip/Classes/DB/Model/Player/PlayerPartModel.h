#ifndef __Syanago__PlayerPartModel__
#define __Syanago__PlayerPartModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class PlayerPartModel : public Model {
public:
    PlayerPartModel();
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(int, player_id, PlayerId);
    CC_SYNTHESIZE(int, parts_id, PartsId);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
    CC_SYNTHESIZE(int, is_new, IsNew);
    
    void saveIsNew(int isNew);
    int getIsNewFromFile();
};

#endif /* defined(__Syanago__PlayerPartModel__) */
