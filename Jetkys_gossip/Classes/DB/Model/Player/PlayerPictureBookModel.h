#ifndef __syanago__PlayerPictureBookModel__
#define __syanago__PlayerPictureBookModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class PlayerPictureBookModel : public Model {
public:
    PlayerPictureBookModel();
    
    CC_SYNTHESIZE(int, player_id, PlayerId);
    CC_SYNTHESIZE(int, mst_characters_id, MstCharactersId);
    CC_SYNTHESIZE(int, number_of_holdings, NumberOfHoldings);
    CC_SYNTHESIZE(int, number_of_contacts, NumberOfContacts);
};

#endif /* defined(__syanago__PlayerPictureBookModel__) */
