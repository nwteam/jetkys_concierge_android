#include "PlayerCharactersModel.h"


PlayerCharactersModel::PlayerCharactersModel():
    parts_slot1(0)
    , parts_slot2(0)
    , is_new(0)
    , lock(0)
    , characters_id(0)
{
    tableName = TABLE_PLAYERCHARACTERS;
}

void PlayerCharactersModel::setValues(Json* child, int lock, int is_new)
{
    setPlayerId(atoi(Json_getString(child, "player_id", "")));
    setCharactersId(atoi(Json_getString(child, "characters_id", "")));
    setLevel(atoi(Json_getString(child, "level", "")));
    setExp(atoi(Json_getString(child, "exp", "")));
    setExercise(atoi(Json_getString(child, "exercise", "")));
    setReaction(atoi(Json_getString(child, "reaction", "")));
    setDecision(atoi(Json_getString(child, "decision", "")));
    setDiceValue1(atoiNull(Json_getString(child, "dice_value1", "")));
    setDiceValue2(atoiNull(Json_getString(child, "dice_value2", "")));
    setDiceValue3(atoiNull(Json_getString(child, "dice_value3", "")));
    setDiceValue4(atoiNull(Json_getString(child, "dice_value4", "")));
    setDiceValue5(atoiNull(Json_getString(child, "dice_value5", "")));
    setDiceValue6(atoiNull(Json_getString(child, "dice_value6", "")));
    setDiceValue7(atoiNull(Json_getString(child, "dice_value7", "")));
    setDiceValue8(atoiNull(Json_getString(child, "dice_value8", "")));
    setDiceValue9(atoiNull(Json_getString(child, "dice_value9", "")));
    setFreeDice7Count(atoi(Json_getString(child, "free_dice7_count", "")));
    setFreeDice8Count(atoi(Json_getString(child, "free_dice8_count", "")));
    setFreeDice9Count(atoi(Json_getString(child, "free_dice9_count", "")));
    setDiceCustomNumber(atoiNull(Json_getString(child, "dice_custom_number", "")));
    setPartsSlot1(atoiNull(Json_getString(child, "parts_slot1", "")));
    setPartsSlot2(atoiNull(Json_getString(child, "parts_slot2", "")));
    setPartsSlot3(atoiNull(Json_getString(child, "parts_slot3", "")));
    setPartsSlot4(atoiNull(Json_getString(child, "parts_slot4", "")));
    setSkillGauge(atoiNull(Json_getString(child, "skill_gauge", "")));
    setDearValue(atof(Json_getString(child, "dear_value", "")));
    setCreated(Json_getString(child, "created", ""));
    setModified(Json_getString(child, "modified", ""));
    setLock(lock);
    if (is_new != -1) {
        saveIsNew(is_new);
    } else {
        if (UserDefault::getInstance()->getBoolForKey(KEY_FIRST_CREATE_PLAYER)) {
            saveIsNew(0);
        } else {
            getIsNewFromFile();
        }
    }
}


int PlayerCharactersModel::getPartsSlot(int index)
{
    switch (index) {
    case 0: return getPartsSlot1();
    case 1: return getPartsSlot2();
    case 2: return getPartsSlot3();
    case 3: return getPartsSlot4();
    }
    return 0;
}

bool PlayerCharactersModel::isAnyPartsEquiped()
{
    for (int i = 0; i < 2; i++) {
        if (getPartsSlot(i) > 0) return true;
    }
    return false;
}

int PlayerCharactersModel::getDiceValue(int index)
{
    switch (index) {
    case 0:
        return getDiceValue1();
    case 1:
        return getDiceValue2();
    case 2:
        return getDiceValue3();
    case 3:
        return getDiceValue4();
    case 4:
        return getDiceValue5();
    case 5:
        return getDiceValue6();
    case 6:
        return getDiceValue7();
    case 7:
        return getDiceValue8();
    case 8:
        return getDiceValue9();
    default:
        return 0;
    }
}

std::vector<int>PlayerCharactersModel::getDiceValues()
{
    std::vector<int>results;
    results.push_back(getDiceValue1());
    results.push_back(getDiceValue2());
    results.push_back(getDiceValue3());
    results.push_back(getDiceValue4());
    results.push_back(getDiceValue5());
    results.push_back(getDiceValue6());
    results.push_back(getDiceValue7());
    results.push_back(getDiceValue8());
    results.push_back(getDiceValue9());
    return results;
}

int PlayerCharactersModel::getPartNumber()
{
    return (getPartsSlot1() > 0 ? 1 : 0) + (getPartsSlot2() > 0 ? 1 : 0) + (getPartsSlot3() > 0 ? 1 : 0) + (getPartsSlot4() > 0 ? 1 : 0);
}

int PlayerCharactersModel::getSaikoroNumber()
{
    return (getDiceValue1() > 0 ? 1 : 0) +
           (getDiceValue2() > 0 ? 1 : 0) +
           (getDiceValue3() > 0 ? 1 : 0) +
           (getDiceValue4() > 0 ? 1 : 0) +
           (getDiceValue5() > 0 ? 1 : 0) +
           (getDiceValue6() > 0 ? 1 : 0) +
           (getDiceValue7() > 0 ? 1 : 0) +
           (getDiceValue8() > 0 ? 1 : 0) +
           (getDiceValue9() > 0 ? 1 : 0);
}

void PlayerCharactersModel::saveIsNew(int isNew)
{
    is_new = isNew;
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(KEY_CHARACTER_IS_NEW, ID).c_str(), is_new);
}

int PlayerCharactersModel::getIsNewFromFile()
{
    is_new = UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_CHARACTER_IS_NEW, ID).c_str(), 1);
    return is_new;
}