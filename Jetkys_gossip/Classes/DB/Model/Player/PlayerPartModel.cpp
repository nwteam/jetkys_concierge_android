#include "PlayerPartModel.h"

PlayerPartModel::PlayerPartModel()
{
    tableName = TABLE_PLAYERPARTS;
    is_new = 0;
}

void PlayerPartModel::saveIsNew(int isNew)
{
    is_new = isNew;
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(KEY_PART_IS_NEW, ID).c_str(), is_new);
}

int PlayerPartModel::getIsNewFromFile()
{
    is_new = UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_PART_IS_NEW, ID).c_str(), 1);
    return is_new;
}