#ifndef __syanago__PlayerCourses__
#define __syanago__PlayerCourses__

#include "Model.h"
#include "editor-support/spine/Json.h"

class PlayerCoursesModel : public Model {
public:
    PlayerCoursesModel();
    
    CC_SYNTHESIZE(int, player_id, PlayerId);
    CC_SYNTHESIZE(int, course_id, CourseId);
    CC_SYNTHESIZE(int, badge , Badge);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
};

#endif /* defined(__syanago__PlayerCourses__) */
