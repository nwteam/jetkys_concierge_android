#ifndef __Syanago__PlayerTeamModel__
#define __Syanago__PlayerTeamModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class PlayerTeamModel : public Model {
public:
    PlayerTeamModel();
    
    CC_SYNTHESIZE(int, player_id, PlayerId);
    CC_SYNTHESIZE(int, team_no, TeamNo);
    CC_SYNTHESIZE(int, learder_character_id, LearderCharacterId);
    CC_SYNTHESIZE(int, support1_character_id, Support1CharacterId);
    CC_SYNTHESIZE(int, support2_character_id, Support2CharacterId);
    CC_SYNTHESIZE(int, support3_character_id, Support3CharacterId);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
};

#endif /* defined(__Syanago__PlayerTeamModel__) */
