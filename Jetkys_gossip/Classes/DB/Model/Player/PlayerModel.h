#ifndef __Syanago__PlayerModel__
#define __Syanago__PlayerModel__

#include <sys/types.h>
#include "cocos2d.h"
#include "Model.h"
#include "editor-support/spine/Json.h"

class PlayerModel : public Model{
public:
    PlayerModel();
    virtual ~PlayerModel();
    
    static PlayerModel *getUser();
    void update();
    void insert();
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int64_t, id, ID);
    CC_SYNTHESIZE(std::string, player_name, PlayerName);
    CC_SYNTHESIZE(int, rank, Rank);
    CC_SYNTHESIZE(int64_t, total_exp, TotalExp);
    CC_SYNTHESIZE(int, fuel, Fuel);
    CC_SYNTHESIZE(int, garage_size, GarageSize);
    CC_SYNTHESIZE(int, free_coin, FreeCoin);
    CC_SYNTHESIZE(int, pay_coin, PayCoin);
    CC_SYNTHESIZE(int, free_token, FreeToken);
    CC_SYNTHESIZE(int, pay_token, PayToken);
    CC_SYNTHESIZE(int, friend_point, FriendPoint);
    CC_SYNTHESIZE(int, help_count, HelpCount);
    CC_SYNTHESIZE(int, help_count_as_friend, HelpCountAsFriend);
    CC_SYNTHESIZE(std::string, push_id, PushID);
    CC_SYNTHESIZE(std::string, install_key, InstallKey);
    CC_SYNTHESIZE(int, device_type, DeviceType);
    CC_SYNTHESIZE(std::string, os_version, OsVersion);
    CC_SYNTHESIZE(int64_t, invited_player_id, InvitedPlayerID);
    CC_SYNTHESIZE(int, invited_player_reward_flag, InvitedPlayerRewardFlag);
    CC_SYNTHESIZE(int, number_of_invitation, NumberOfInvitation);
    CC_SYNTHESIZE(int, number_of_invitation_success, NumberOfInvitationSuccess);
    CC_SYNTHESIZE(std::string, last_logged, LastLogged);
    CC_SYNTHESIZE(std::string, before_last_logged, BeforeLastLogged);
    CC_SYNTHESIZE(int, number_of_continuity_login, NumberOfContinuityLogin);
    CC_SYNTHESIZE(int, number_of_total_login, NumberOfTotalLogin);
    CC_SYNTHESIZE(int, number_of_purchases, NumberOfPurchases);
    CC_SYNTHESIZE(int, just_before_owner_help, JustBeforeOwnerHelp);
    CC_SYNTHESIZE(int, just_before_friend_help, JustBeforeFriendHelp);
    CC_SYNTHESIZE(int, current_course_id, CurrentCourseId);
    //CC_SYNTHESIZE(std::string, input_campaign_code, InputCampaignCode);
    CC_SYNTHESIZE(int, game_step, GameStep);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
    CC_SYNTHESIZE(std::string, contact_code, ContactCode);
    CC_SYNTHESIZE(int, fuel_tweet_count, FuelTweetCount);
};

#endif /* defined(__Syanago__PlayerModel__) */
