#include "PlayerModel.h"
#include "DBManager.h"
#include "MacroDefines.h"

USING_NS_CC;

static PlayerModel* player = nullptr;

PlayerModel::PlayerModel():
    rank(1)
    , total_exp(0)
    , fuel(0)
    , garage_size(1)
    , free_coin(0)
    , pay_coin(0)
    , free_token(0)
    , pay_token(0)
    , friend_point(0)
    , help_count(0)
    , help_count_as_friend(0)
    , device_type(0)
    , invited_player_id(0)
    , invited_player_reward_flag(0)
    , number_of_invitation(0)
    , number_of_invitation_success(0)
    , number_of_continuity_login(1)
    , number_of_total_login(0)
    , number_of_purchases(0)
    , just_before_friend_help(0)
    , just_before_owner_help(0)
    , current_course_id(0)
    , contact_code("")
    , fuel_tweet_count(0)
{
    tableName = TABLE_PLAYERS;
};

PlayerModel::~PlayerModel() {}

PlayerModel* PlayerModel::getUser()
{
    player = new PlayerModel();
    std::string sqlstr = "SELECT * FROM trn_players";
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            player->setID((int64_t)sqlite3_column_int64(ppStmt1, 0));
            player->setPlayerName((const char*)sqlite3_column_text(ppStmt1, 1));
            player->setRank(sqlite3_column_int(ppStmt1, 2));
            player->setTotalExp((int64_t)sqlite3_column_int64(ppStmt1, 3));
            player->setFuel(sqlite3_column_int(ppStmt1, 4));
            player->setGarageSize(sqlite3_column_int(ppStmt1, 5));
            player->setFreeCoin(sqlite3_column_int(ppStmt1, 6));
            player->setPayCoin(sqlite3_column_int(ppStmt1, 7));
            player->setFreeToken(sqlite3_column_int(ppStmt1, 8));
            player->setPayToken(sqlite3_column_int(ppStmt1, 9));
            player->setFriendPoint(sqlite3_column_int(ppStmt1, 10));
            player->setHelpCount(sqlite3_column_int(ppStmt1, 11));
            player->setHelpCountAsFriend(sqlite3_column_int(ppStmt1, 12));
            player->setPushID((const char*)sqlite3_column_text(ppStmt1, 13));
            player->setInstallKey((const char*)sqlite3_column_text(ppStmt1, 14));
            player->setDeviceType(sqlite3_column_int(ppStmt1, 15));
            player->setOsVersion((const char*)sqlite3_column_text(ppStmt1, 16));
            player->setInvitedPlayerID((int64_t)sqlite3_column_int64(ppStmt1, 17));
            player->setInvitedPlayerRewardFlag(sqlite3_column_int(ppStmt1, 18));
            player->setNumberOfInvitation(sqlite3_column_int(ppStmt1, 19));
            player->setNumberOfInvitationSuccess(sqlite3_column_int(ppStmt1, 20));
            player->setLastLogged((const char*)sqlite3_column_text(ppStmt1, 21));
            player->setBeforeLastLogged((const char*)sqlite3_column_text(ppStmt1, 22));
            player->setNumberOfContinuityLogin(sqlite3_column_int(ppStmt1, 23));
            player->setNumberOfTotalLogin(sqlite3_column_int(ppStmt1, 24));
            player->setNumberOfPurchases(sqlite3_column_int(ppStmt1, 25));
            player->setJustBeforeOwnerHelp(sqlite3_column_int(ppStmt1, 26));
            player->setJustBeforeFriendHelp(sqlite3_column_int(ppStmt1, 27));
            player->setCurrentCourseId(sqlite3_column_int(ppStmt1, 28));
            // player->setInputCampaignCode((const char*)sqlite3_column_text(ppStmt1, 29));
            player->setGameStep(sqlite3_column_int(ppStmt1, 29));
            player->setCreated((const char*)sqlite3_column_text(ppStmt1, 30));
            player->setModified((const char*)sqlite3_column_text(ppStmt1, 31));
            player->setContactCode((const char*)sqlite3_column_text(ppStmt1, 32));

            sqlite3_finalize(ppStmt1);
            return player;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void PlayerModel::insert()
{
    std::string buffer = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21, ?22, ?23, ?24, ?25, ?26, ?27, ?28, ?29, ?30, ?31, ?32, ?33)";

    char* errmsg;
    sqlite3* pDB = DB_MANAGER->getDB();
    sqlite3_stmt* stmt;
    sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, &errmsg);

    sqlite3_prepare_v2(pDB, buffer.c_str(), (int)buffer.length(), &stmt, NULL);

    sqlite3_bind_int64(stmt, 1, id);
    sqlite3_bind_text(stmt, 2, player_name.c_str(), (int)player_name.size(), SQLITE_STATIC);
    sqlite3_bind_int(stmt, 3, rank);
    sqlite3_bind_int64(stmt, 4, total_exp);
    sqlite3_bind_int(stmt, 5, fuel);
    sqlite3_bind_int(stmt, 6, garage_size);
    sqlite3_bind_int(stmt, 7, free_coin);
    sqlite3_bind_int(stmt, 8, pay_coin);
    sqlite3_bind_int(stmt, 9, free_token);
    sqlite3_bind_int(stmt, 10, pay_token);
    sqlite3_bind_int(stmt, 11, friend_point);
    sqlite3_bind_int(stmt, 12, help_count);
    sqlite3_bind_int(stmt, 13, help_count_as_friend);
    sqlite3_bind_text(stmt, 14, push_id.c_str(), (int)push_id.size(), SQLITE_STATIC);
    sqlite3_bind_text(stmt, 15, install_key.c_str(), (int)install_key.size(), SQLITE_STATIC);
    sqlite3_bind_int(stmt, 16, device_type);
    sqlite3_bind_text(stmt, 17, os_version.c_str(), (int)os_version.size(), SQLITE_STATIC);
    sqlite3_bind_int64(stmt, 18, invited_player_id);
    sqlite3_bind_int(stmt, 19, invited_player_reward_flag);
    sqlite3_bind_int(stmt, 20, number_of_invitation);
    sqlite3_bind_int(stmt, 21, number_of_invitation_success);
    sqlite3_bind_text(stmt, 22, last_logged.c_str(), (int)last_logged.size(), SQLITE_STATIC);
    sqlite3_bind_text(stmt, 23, before_last_logged.c_str(), (int)before_last_logged.size(), SQLITE_STATIC);
    sqlite3_bind_int(stmt, 24, number_of_continuity_login);
    sqlite3_bind_int(stmt, 25, number_of_total_login);
    sqlite3_bind_int(stmt, 26, number_of_purchases);
    sqlite3_bind_int(stmt, 27, just_before_owner_help);
    sqlite3_bind_int(stmt, 28, just_before_friend_help);
    sqlite3_bind_int(stmt, 29, current_course_id);
    sqlite3_bind_int(stmt, 30, game_step);
    sqlite3_bind_text(stmt, 31, created.c_str(), (int)created.size(), SQLITE_STATIC);
    sqlite3_bind_text(stmt, 32, modified.c_str(), (int)modified.size(), SQLITE_STATIC);
    sqlite3_bind_text(stmt, 33, contact_code.c_str(), (int)contact_code.size(), SQLITE_STATIC);

    if (sqlite3_step(stmt) != SQLITE_DONE) {}

    sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, &errmsg);
    free(errmsg);
    sqlite3_finalize(stmt);
}

void PlayerModel::update()
{
    std::string sqlstr = "UPDATE trn_players SET player_name = '" + player_name +
                         "', rank = '" + stringInt(rank)->_string +
                         "', total_exp = '" + stringInt64(total_exp)->_string +
                         "', fuel = '" + stringInt(fuel)->_string +
                         "', garage_size = '" + stringInt(garage_size)->_string +
                         "', free_coin = '" + stringInt(free_coin)->_string +
                         "', pay_coin = '" + stringInt(pay_coin)->_string +
                         "', free_token = '" + stringInt(free_token)->_string +
                         "', pay_token = '" + stringInt(pay_token)->_string +
                         "', friend_point = '" + stringInt(friend_point)->_string +
                         "', help_count = '" + stringInt(help_count)->_string +
                         "', help_count_as_friend = '" + stringInt(help_count_as_friend)->_string +
                         "', push_id = '" + push_id +
                         "', install_key = '" + install_key +
                         "', device_type = '" + stringInt(device_type)->_string +
                         "', os_version = '" + os_version +
                         "', invited_player_id = '" + stringInt64(invited_player_id)->_string +
                         "', invited_player_reward_flag = '" + stringInt(invited_player_reward_flag)->_string +
                         "', number_of_invitation = '" + stringInt(number_of_invitation)->_string +
                         "', number_of_invitation_success = '" + stringInt(number_of_invitation_success)->_string +
                         "', last_logged = '" + last_logged +
                         "', before_last_logged = '" + before_last_logged +
                         "', number_of_continuity_login = '" + stringInt(number_of_continuity_login)->_string +
                         "', number_of_total_login = '" + stringInt(number_of_total_login)->_string +
                         "', number_of_purchases = '" + stringInt(number_of_purchases)->_string +
                         "', just_before_owner_help = '" + stringInt(just_before_owner_help)->_string +
                         "', just_before_friend_help = '" + stringInt(just_before_friend_help)->_string +
                         "', current_course_id = '" + stringInt(current_course_id)->_string +
                         //    "', input_campaign_code = '" + input_campaign_code +
                         "', game_step = '" + stringInt(game_step)->_string +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "', contact_code = '" + contact_code +
                         "' WHERE id = '" + stringInt64(id)->_string + "'";

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {}
}
