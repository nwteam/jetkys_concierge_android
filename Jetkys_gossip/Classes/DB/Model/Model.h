#ifndef __Syanago__Model__
#define __Syanago__Model__

#include "cocos2d.h"
#include "DBManager.h"
#include "MacroDefines.h"
#include "StringDefines.h"

#define TABLE_PREFIX "mst_"
#define TABLE_AREAS "mst_areas"
#define TABLE_BIRTHPLACES "mst_birthplaces"
#define TABLE_BODY_TYPES "mst_body_types"
#define TABLE_CHARACTERS "mst_characters"
#define TABLE_COURSES "mst_courses"
#define TABLE_GRID_TYPES "mst_grid_types"
#define TABLE_ITEM_UNITS "mst_item_units"
#define TABLE_ITEMS "mst_items"
#define TABLE_LEADER_SKILLS "mst_leader_skills"
#define TABLE_SKILLS "mst_skills"
#define TABLE_SKILLNAMES "mst_skill_names"
#define TABLE_LEADER_SKILLNAMES "mst_leader_skill_names"
#define TABLE_LEVELS "mst_levels"
#define TABLE_MAKERS "mst_makers"
#define TABLE_MAPS "mst_maps"
#define TABLE_PARTS "mst_parts"
#define TABLE_RANKS "mst_ranks"
#define TABLE_RARITIES "mst_rarities"
#define TABLE_SYSTEM_SETTINGS "mst_system_settings"
#define TABLE_WORDS "mst_words"
#define TABLE_GRID_OPERANDS "mst_grid_operands"

#define TABLE_PLAYERCHARACTERS "trn_player_characters"
#define TABLE_PLAYERS "trn_players"
#define TABLE_PLAYERPARTS "trn_player_parts"
#define TABLE_PLAYERTEAMS "trn_player_teams"
#define TABLE_PLAYERITEMUNITS "trn_player_item_units"
#define TABLE_PLAYERITEMUNITS "trn_player_item_units"

#define TABLE_PLAYERPICTERBOOKS "trn_player_picture_books"


#define TABLE_PLAYERCOURSES "history_player_courses"


USING_NS_CC;

class Model {
public:
    Model();
    virtual ~Model();
    
    virtual bool isExist(std::string ID = "");
    virtual void insert();
    virtual void update();
    virtual void deleteRow(std::string ID = "");
    
    virtual void insertOrUpdate(std::string ID);
    
    virtual int count();
    
    static std::string getLastModified(std::string table_name);
    
    //using to delete all data in table before run bulk insert
    bool deleteTableData(std::string tableName);
    static bool dropTable(std::string tableName);
    
    std::string tableName;
};

#endif /* defined(__Syanago__Model__) */
