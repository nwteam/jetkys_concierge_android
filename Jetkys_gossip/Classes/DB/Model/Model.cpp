#include "Model.h"

Model::Model()
{
    tableName = "";
}

Model::~Model() {}

int Model::count()
{
    std::string sqlstr = "SELECT count(id) FROM " + tableName;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    int result = 0;
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return result;
}

bool Model::isExist(std::string ID /* = "" */)
{
    std::string idStr = tableName == TABLE_GRID_TYPES ? "no" : "id";
    std::string sqlstr = "SELECT * FROM " + tableName + " WHERE " + idStr + " = " + ID;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            sqlite3_finalize(ppStmt1);
            return true;
        }
        sqlite3_finalize(ppStmt1);
    }
    return false;
}

void Model::insert() {}

void Model::update() {}

void Model::insertOrUpdate(std::string ID)
{
    if (isExist(ID)) {
        insert();
    } else {
        update();
    }
}

std::string Model::getLastModified(std::string table_name)
{
    std::string table_name2 = TABLE_PREFIX + table_name;
    std::string sqlstr = "SELECT modified FROM " + table_name2 + " WHERE modified = (SELECT MAX(modified) FROM " + table_name2 + ")";

    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::string a = (const char*)sqlite3_column_text(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return a;
        }
        sqlite3_finalize(ppStmt1);
    }
    return "20140101000000";
}

void Model::deleteRow(std::string ID /* = "" */)
{
    std::string idStr = tableName == TABLE_GRID_TYPES ? "no" : "id";
    std::string sqlstr = "DELETE FROM " + tableName + " WHERE " + idStr + " = " + ID;

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {}
}


bool Model::deleteTableData(std::string tableName)
{
    std::string sqlstr = "DELETE FROM " + tableName;
    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);

    if (result != SQLITE_OK) {
        return false;
    }

    return true;
}


bool Model::dropTable(std::string tableName)
{
    std::string sqlstr = "DROP TABLE " + tableName;
    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);

    if (result != SQLITE_OK) {
        return false;
    }

    return true;
}

