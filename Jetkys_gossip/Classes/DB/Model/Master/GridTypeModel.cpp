#include "GridTypeModel.h"
#include <cstring>

GridTypeModel::GridTypeModel()
{
    tableName = TABLE_GRID_TYPES;
}

GridTypeModel* GridTypeModel::get(int ID)
{
    //////CCLOG("%s%s", TABLE_AREAS, "::get(std::string ID)");
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_GRID_TYPES;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new GridTypeModel();
            model->setID(sqlite3_column_int(ppStmt1, 0));
            model->setKind(sqlite3_column_int(ppStmt1, 1));
            model->setName((const char*)sqlite3_column_text(ppStmt1, 2));
            model->setScore(sqlite3_column_int(ppStmt1, 3));
            model->setCreated((const char*)sqlite3_column_text(ppStmt1, 4));
            model->setModified((const char*)sqlite3_column_text(ppStmt1, 5));

            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void GridTypeModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         stringInt(kind)->_string + "','" +
                         name + "','" +
                         stringInt(score)->_string + "','" +
                         created + "','" +
                         modified + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void GridTypeModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET kind = '" + stringInt(kind)->_string +
                         "', name = '" + name +
                         "', score = '" + stringInt(score)->_string +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "' WHERE no = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void GridTypeModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        char buffer[] = "INSERT INTO mst_grid_types VALUES (?1, ?2, ?3, ?4, ?5 ,?6)";
//        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5 ,?6)";
//        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "no", "-1"));
            int kind =  atoi(Json_getString(child, "kind", "-1"));
            std::string name = Json_getString(child, "name", "");
            int score = atoi(Json_getString(child, "score", "-1"));
            std::string created = Json_getString(child, "created", "");
            std::string modified = Json_getString(child, "modified", "");

            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_int(stmt, 2, kind);
            sqlite3_bind_text(stmt, 3, name.c_str(), (int)name.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 4, score);
            sqlite3_bind_text(stmt, 5, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 6, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                ////CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}
