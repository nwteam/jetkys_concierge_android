#ifndef __Syanago__PartModel__
#define __Syanago__PartModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class PartModel : public Model {
public:
    PartModel();
    
    static std::shared_ptr<PartModel> find(int ID);
    static PartModel *get(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(int, exercise, Exercise);
    CC_SYNTHESIZE(int, reaction, Reaction);
    CC_SYNTHESIZE(int, decision, Decision);
    CC_SYNTHESIZE(int, type, Type);
    CC_SYNTHESIZE(int, rarity, Rarity);
    CC_SYNTHESIZE(int, buy_price, BuyPrice);
    CC_SYNTHESIZE(int, sale_price, SalePrice);
    CC_SYNTHESIZE(std::string, remarks, Remarks);
    CC_SYNTHESIZE(int, display_order, DisplayOrder);
    CC_SYNTHESIZE(std::string, start_datetime, StartDatetime);
    CC_SYNTHESIZE(std::string, end_datetime, EndDatetime);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
};

#endif /* defined(__Syanago__PartModel__) */
