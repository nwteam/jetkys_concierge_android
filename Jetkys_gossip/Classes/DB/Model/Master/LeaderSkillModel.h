#ifndef __Syanago__LeaderSkillModel__
#define __Syanago__LeaderSkillModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class LeaderSkillModel : public Model {
public:
    LeaderSkillModel();
    
    void setValues(sqlite3_stmt* ppStmt);
    static std::shared_ptr<LeaderSkillModel> findByCharacterId(int characterId);
    
    static LeaderSkillModel *get(int ID);
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(int, skill_kind, SkillKind);
    CC_SYNTHESIZE(int, value1, Value1);
    CC_SYNTHESIZE(int, value2, Value2);
    CC_SYNTHESIZE(float, value3, Value3);
    CC_SYNTHESIZE(int, value4, Value4);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
};

#endif /* defined(__Syanago__LeaderSkillModel__) */
