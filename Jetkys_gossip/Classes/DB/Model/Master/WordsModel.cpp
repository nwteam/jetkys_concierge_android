#include "WordsModel.h"
#include <cstring>

WordsModel::WordsModel()
{
    tableName = TABLE_WORDS;
}

std::shared_ptr<WordsModel>WordsModel::find(int chracterId, int placeId, int actionId, int wordId)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_WORDS;
    sqlstr += " WHERE character_id = " + stringInt(chracterId)->_string +
              " AND place_id = " + stringInt(placeId)->_string +
              " AND action_id = " + stringInt(actionId)->_string +
              " AND word_id = " + stringInt(wordId)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<WordsModel>model(new WordsModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void WordsModel::setValues(sqlite3_stmt* ppStmt1)
{
    setCharacterId(sqlite3_column_int(ppStmt1, 0));
    setPlaceId(sqlite3_column_int(ppStmt1, 1));
    setActionId(sqlite3_column_int(ppStmt1, 2));
    setWordId(sqlite3_column_int(ppStmt1, 3));
    setWords((const char*)sqlite3_column_text(ppStmt1, 4));
    setDearValue(sqlite3_column_int(ppStmt1, 5));
    setCreated((const char*)sqlite3_column_text(ppStmt1, 6));
    setModified((const char*)sqlite3_column_text(ppStmt1, 7));
}

bool WordsModel::isExist()
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_WORDS;
    sqlstr += " WHERE character_id = " + stringInt(character_id)->_string +
              " AND place_id = " + stringInt(place_id)->_string +
              " AND action_id = " + stringInt(action_id)->_string +
              " AND word_id = " + stringInt(word_id)->_string;
    ////CCLOG("WordsModel SQL: %s", sqlstr.c_str());

    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            sqlite3_finalize(ppStmt1);
            return true;
        }
        sqlite3_finalize(ppStmt1);
    }
    return false;
}

WordsModel* WordsModel::get(int chracterId, int placeId, int actionId, int wordId)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_WORDS;
    sqlstr += " WHERE character_id = " + stringInt(chracterId)->_string +
              " AND place_id = " + stringInt(placeId)->_string +
              " AND action_id = " + stringInt(actionId)->_string +
              " AND word_id = " + stringInt(wordId)->_string;
    ////CCLOG("WordsModel SQL: %s", sqlstr.c_str());

    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new WordsModel();
            model->setCharacterId(sqlite3_column_int(ppStmt1, 0));
            model->setPlaceId(sqlite3_column_int(ppStmt1, 1));
            model->setActionId(sqlite3_column_int(ppStmt1, 2));
            model->setWordId(sqlite3_column_int(ppStmt1, 3));
            model->setWords((const char*)sqlite3_column_text(ppStmt1, 4));
            model->setDearValue(sqlite3_column_int(ppStmt1, 5));
            model->setCreated((const char*)sqlite3_column_text(ppStmt1, 6));
            model->setModified((const char*)sqlite3_column_text(ppStmt1, 7));

            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void WordsModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" +
                         stringInt(character_id)->_string + "','" +
                         stringInt(place_id)->_string + "','" +
                         stringInt(action_id)->_string + "','" +
                         stringInt(word_id)->_string + "','" +
                         words + "','" +
                         stringInt(dear_value)->_string + "','" +
                         created + "','" +
                         modified + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void WordsModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET words = '" + words +
                         "', dear_value = " + stringInt(dear_value)->_string +
                         ", created = '" + created +
                         "', modified = '" + modified +
                         "' WHERE character_id = " + stringInt(character_id)->_string +
                         " AND place_id = " + stringInt(place_id)->_string +
                         " AND action_id = " + stringInt(action_id)->_string +
                         " AND word_id = " + stringInt(word_id)->_string;

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}


void WordsModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        char buffer[] = "INSERT INTO mst_words VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8)";
//        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8)";
//        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int character_id = atoi(Json_getString(child, "character_id", "-1"));
            int place_id = atoi(Json_getString(child, "place_id", "-1"));
            int action_id = atoi(Json_getString(child, "action_id", "-1"));
            int word_id = atoi(Json_getString(child, "word_id", "-1"));
            std::string words = Json_getString(child, "words", "");
            int dear_value = atoi(Json_getString(child,  "dear_value", "-1"));
            std::string created = Json_getString(child, "created", "");
            std::string modified = Json_getString(child, "modified", "");

            sqlite3_bind_int(stmt, 1, character_id);
            sqlite3_bind_int(stmt, 2, place_id);
            sqlite3_bind_int(stmt, 3, action_id);
            sqlite3_bind_int(stmt, 4, word_id);
            sqlite3_bind_text(stmt, 5, words.c_str(), (int)words.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 6, dear_value);
            sqlite3_bind_text(stmt, 7, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 8, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                ////CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}
