#include "GridOperandsModel.h"
#include <cstring>

GridOperandsModel::GridOperandsModel()
{
    tableName = TABLE_GRID_OPERANDS;
}

bool GridOperandsModel::isExist()
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_GRID_OPERANDS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string +
              " AND effect_kind = " + stringInt(effect_kind)->_string +
              " AND kind_sub_no = " + stringInt(kind_sub_no)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            sqlite3_finalize(ppStmt1);
            return true;
        }
        sqlite3_finalize(ppStmt1);
    }
    return false;
}

GridOperandsModel* GridOperandsModel::get(int ID, int effectKind, int kindSubNo)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_GRID_OPERANDS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string +
              " AND effect_kind = " + stringInt(effectKind)->_string +
              " AND kind_sub_no = " + stringInt(kindSubNo)->_string;

    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new GridOperandsModel();
            model->setID(sqlite3_column_int(ppStmt1, 0));
            model->setEffectKind(sqlite3_column_int(ppStmt1, 1));
            model->setKindSubNo(sqlite3_column_int(ppStmt1, 2));
            model->setOperand1(sqlite3_column_double(ppStmt1, 3));
            model->setOperand2(sqlite3_column_double(ppStmt1, 4));

            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void GridOperandsModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" +
                         stringInt(ID)->_string + "','" +
                         stringInt(effect_kind)->_string + "','" +
                         stringInt(kind_sub_no)->_string + "','" +
                         stringFloat(operand1)->_string + "','" +
                         stringFloat(operand2)->_string + "')";

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {}
}

void GridOperandsModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET operand1 = '" + stringFloat(operand1)->_string +
                         "', operand2 = " + stringFloat(operand2)->_string +
                         "' WHERE ID = " + stringInt(ID)->_string +
                         " AND effect_kind = " + stringInt(effect_kind)->_string +
                         " AND kind_sub_no = " + stringInt(kind_sub_no)->_string + "'";

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {}
}


void GridOperandsModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        dropTable(tableName);
        createGridOperandsTable();

        char buffer[] = "INSERT INTO mst_grid_operands VALUES (?1, ?2, ?3, ?4, ?5)";
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {
            int id = atoi(Json_getString(child, "id", "-1"));
            int effectKind = atoi(Json_getString(child, "effect_kind", "-1"));
            int kindSubNo = atoi(Json_getString(child, "kind_sub_no", "-1"));
            float operand1 = atof(Json_getString(child, "operand1", "-1"));
            float operand2 = atof(Json_getString(child, "operand2", "-1"));

            sqlite3_bind_int(stmt, 1, id);
            sqlite3_bind_int(stmt, 2, effectKind);
            sqlite3_bind_int(stmt, 3, kindSubNo);
            sqlite3_bind_double(stmt, 4, operand1);
            sqlite3_bind_double(stmt, 5, operand2);

            if (sqlite3_step(stmt) != SQLITE_DONE) {}
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}

bool GridOperandsModel::createGridOperandsTable()
{
    std::string sqlCreateGridOperands = "CREATE TABLE mst_grid_operands (id int(11) NOT NULL,\
    effect_kind	int(11) NOT NULL,\
    kind_sub_no	int(11) NOT NULL,\
    operand1	float DEFAULT '1',\
    operand2	float DEFAULT '1'\
    )";

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlCreateGridOperands.c_str(), NULL, NULL, &errmsg);

    if (result != SQLITE_OK) {
        return false;
    }
    return true;
}

std::shared_ptr<GridOperandsModel>GridOperandsModel::find(const int ID, const int effectKind, const int kindSubNo)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_GRID_OPERANDS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string +
              " AND effect_kind = " + stringInt(effectKind)->_string +
              " AND kind_sub_no = " + stringInt(kindSubNo)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<GridOperandsModel>result(new GridOperandsModel());
            result->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void GridOperandsModel::setValues(sqlite3_stmt* ppStmt1)
{
    setID(sqlite3_column_int(ppStmt1, 0));
    setEffectKind(sqlite3_column_int(ppStmt1, 1));
    setKindSubNo(sqlite3_column_int(ppStmt1, 2));
    setOperand1(sqlite3_column_double(ppStmt1, 3));
    setOperand2(sqlite3_column_double(ppStmt1, 4));
}
