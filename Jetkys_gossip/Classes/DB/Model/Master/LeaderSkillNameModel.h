#ifndef __Syanago__LeaderSkillNameModel__
#define __Syanago__LeaderSkillNameModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class LeaderSkillNameModel : public Model {
public:
    LeaderSkillNameModel();
    
    static std::shared_ptr<LeaderSkillNameModel> find(int ID);
    static std::shared_ptr<LeaderSkillNameModel> getLeaderSkillNameByCharacterId(int characterId);
    
    static LeaderSkillNameModel *get(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(std::string, description, Description);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
private:
    void setValues(sqlite3_stmt* ppStmt);

};

#endif /* defined(__Syanago__LeaderSkillNameModel__) */
