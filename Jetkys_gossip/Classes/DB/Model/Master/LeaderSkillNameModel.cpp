#include "LeaderSkillNameModel.h"
#include <cstring>

LeaderSkillNameModel::LeaderSkillNameModel()
{
    tableName = TABLE_LEADER_SKILLNAMES;
}

std::shared_ptr<LeaderSkillNameModel>LeaderSkillNameModel::find(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_LEADER_SKILLNAMES;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<LeaderSkillNameModel>model(new LeaderSkillNameModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

std::shared_ptr<LeaderSkillNameModel>LeaderSkillNameModel::getLeaderSkillNameByCharacterId(int characterId)
{
    std::stringstream sql;
    sql << "select sn.* from mst_characters as c\
    inner join mst_leader_skill_names as sn\
    on c.leader_skill_name = sn.id\
    where c.id = " << characterId;

    sqlite3_stmt* statment;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &statment, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(statment) == SQLITE_ROW) {
            std::shared_ptr<LeaderSkillNameModel>model(new LeaderSkillNameModel());
            model->setValues(statment);
            sqlite3_finalize(statment);
            return model;
        }
        sqlite3_finalize(statment);
    }
    return nullptr;
}

void LeaderSkillNameModel::setValues(sqlite3_stmt* ppStmt)
{
    setID(sqlite3_column_int(ppStmt, 0));
    setName((const char*)sqlite3_column_text(ppStmt, 1));
    setDescription((const char*)sqlite3_column_text(ppStmt, 2));
    setCreated((const char*)sqlite3_column_text(ppStmt, 3));
    setModified((const char*)sqlite3_column_text(ppStmt, 4));
}

LeaderSkillNameModel* LeaderSkillNameModel::get(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_LEADER_SKILLNAMES;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;

    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new LeaderSkillNameModel();
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void LeaderSkillNameModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         name + "','" +
                         description + "','" +
                         created + "','" +
                         modified + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void LeaderSkillNameModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET name = '" + name +
                         "', description = '" + description +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}



void LeaderSkillNameModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        char buffer[] = "INSERT INTO mst_leader_skill_names VALUES (?1, ?2, ?3, ?4, ?5)";
//        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5)";
//        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            std::string name = Json_getString(child, "name", "");
            std::string description = Json_getString(child, "description", "");
            std::string created = Json_getString(child, "created", "");
            std::string modified =   Json_getString(child, "modified", "");

            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_text(stmt, 2, name.c_str(), (int)name.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 3, description.c_str(), (int)description.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 4, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 5, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                ////CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}

