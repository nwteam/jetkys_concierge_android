#ifndef __Syanago__MapModel__
#define __Syanago__MapModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class MapModel : public Model {
public:
    enum MAP_TYPE {
        COMMING_SOON = 0,
        NORMAL,
        EVENT,
    };
    
    MapModel();
    
    static std::shared_ptr<MapModel> find(int ID);
    static std::vector<int> getIds();
    
    static MapModel *get(int ID);
    static std::vector<std::shared_ptr<MapModel>> findMapGroupId(const int groupId);
    
    static int getFirstMapId(const int groupId);
    static int getLastMapId(const int groupId);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    bool createMapsTable();
    
    static std::vector<std::string> getResources();
    static const int getMapGroupCount();
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(int, map_type, MapType);
    CC_SYNTHESIZE(int, position_x, PositionX);
    CC_SYNTHESIZE(int, position_y, PositionY);
    CC_SYNTHESIZE(int, prev_id, PrevId);
    CC_SYNTHESIZE(int, next_id, NextId);
    CC_SYNTHESIZE(int, group_id, GroupId);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
    
private:
    void setValues(sqlite3_stmt* ppStmt1);
};

#endif /* defined(__Syanago__MapModel__) */
