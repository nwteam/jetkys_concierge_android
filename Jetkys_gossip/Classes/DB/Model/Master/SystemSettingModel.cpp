#include "SystemSettingModel.h"
#include <cstring>

SystemSettingModel::SystemSettingModel()
{
    tableName = TABLE_SYSTEM_SETTINGS;
}

std::shared_ptr<SystemSettingModel> SystemSettingModel::getModel()
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_SYSTEM_SETTINGS;
    sqlstr += " WHERE id = 1";
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<SystemSettingModel>model(new SystemSettingModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void SystemSettingModel::setValues(sqlite3_stmt* ppStmt1)
{
    setID(sqlite3_column_int(ppStmt1, 0));
    setDlServerUrl((const char*)sqlite3_column_text(ppStmt1, 1));
    setPublicSiteUrl((const char*)sqlite3_column_text(ppStmt1, 2));
    setRuleServerUrl((const char*)sqlite3_column_text(ppStmt1, 3));
    setEventServerUrl((const char*)sqlite3_column_text(ppStmt1, 4));
    setHelpServerUrl((const char*)sqlite3_column_text(ppStmt1, 5));
    setSettlementServerUrl((const char*)sqlite3_column_text(ppStmt1, 6));
    setFaqServerUrl((const char*)sqlite3_column_text(ppStmt1, 7));
    setIosStoreMode(sqlite3_column_int(ppStmt1, 8));
    setReviewIosAppVersion((const char*)sqlite3_column_text(ppStmt1, 9));
    setIosAppVersion((const char*)sqlite3_column_text(ppStmt1, 10));
    setIosForceUpdate(sqlite3_column_int(ppStmt1, 11));
    setAppStoreUrl((const char*)sqlite3_column_text(ppStmt1, 12));
    setAndroidAppVersion((const char*)sqlite3_column_text(ppStmt1, 13));
    setAndroidForceUpdate(sqlite3_column_int(ppStmt1, 14));
    setGooglePlayUrl((const char*)sqlite3_column_text(ppStmt1, 15));
    setCoinOfLogin(sqlite3_column_int(ppStmt1, 16));
    setFpOfLogin(sqlite3_column_int(ppStmt1, 17));
    setFpOfHelp(sqlite3_column_int(ppStmt1, 18));
    setInviteMessages((const char*)sqlite3_column_text(ppStmt1, 19));
    setTokenOfInvitationSuccess(sqlite3_column_int(ppStmt1, 20));
    setMaxNumberOfInvitationSuccess(sqlite3_column_int(ppStmt1, 21));
    setTokenOfInvited(sqlite3_column_int(ppStmt1, 22));
    setTokenOfDiceCustom(sqlite3_column_int(ppStmt1, 23));
    setTokenOfSynthesis(sqlite3_column_int(ppStmt1, 24));
    setFpForFreeGacha(sqlite3_column_int(ppStmt1, 25));
    setTokenForPayGacha(sqlite3_column_int(ppStmt1, 26));
    setBannerUrlFreeGacha((const char*)sqlite3_column_text(ppStmt1, 27));
    setBannerUrlPayGacha((const char*)sqlite3_column_text(ppStmt1, 28));
    setSupportCompressionRate((float)sqlite3_column_double(ppStmt1, 29));
    setMaxNumberOfGridChain(sqlite3_column_int(ppStmt1, 30));
    setRankBonusRate1((float)sqlite3_column_double(ppStmt1, 31));
    setRankBonusRate2((float)sqlite3_column_double(ppStmt1, 32));
    setRankBonusRate3((float)sqlite3_column_double(ppStmt1, 33));
    setRankBonusRate4((float)sqlite3_column_double(ppStmt1, 34));
    setMixedCoefficient1(sqlite3_column_int(ppStmt1, 35));
    setMixedCoefficient2(sqlite3_column_int(ppStmt1, 36));
    setInitFuel(sqlite3_column_int(ppStmt1, 37));
    setInitNumOfGarage(sqlite3_column_int(ppStmt1, 38));
    setNumOfTokenForExtension(sqlite3_column_int(ppStmt1, 39));
    setExpandedNumOfGarage(sqlite3_column_int(ppStmt1, 40));
    setMaxNumOfGarage(sqlite3_column_int(ppStmt1, 41));
    setMaxNumOfTeam(sqlite3_column_int(ppStmt1, 42));
    setIntervalLogin(sqlite3_column_int(ppStmt1, 43));
    setFuelRecoveryInterval(sqlite3_column_int(ppStmt1, 44));
    setCoinConversionRate((float)sqlite3_column_double(ppStmt1, 45));
    setFuelBill(sqlite3_column_int(ppStmt1, 46));
    setCreated((const char*)sqlite3_column_text(ppStmt1, 47));
    setModified((const char*)sqlite3_column_text(ppStmt1, 48));
    setCoinOfSynthesis(sqlite3_column_int(ppStmt1, 49));
    setMaxNumberOfUturnGridChai(sqlite3_column_int(ppStmt1, 50));
    setTokenOfDiceReset(sqlite3_column_int(ppStmt1, 51));
    setTurnPerToken(sqlite3_column_int(ppStmt1, 52));
}

SystemSettingModel* SystemSettingModel::get(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_SYSTEM_SETTINGS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new SystemSettingModel();
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

SystemSettingModel* SystemSettingModel::getWithLastModified()
{
    ////CCLOG("TABLE_SYSTEM_SETTINGS::getWithLastModified()");
    std::string table_name2 = TABLE_SYSTEM_SETTINGS;
    std::string sqlstr = "SELECT * FROM " + table_name2 + " WHERE modified = (SELECT MAX(modified) FROM " + table_name2 + ")";
    ////CCLOG("%s", sqlstr.c_str());

    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    for (;; ) {
        result = sqlite3_step(ppStmt1);
        if (result == SQLITE_DONE) {
            ////CCLOG("rows = 0");
            return nullptr;
        }
        if (result != SQLITE_ROW) {
            ////CCLOG("error: %s!\n", sqlite3_errmsg(pDB));
            return nullptr;
        }
        auto model = new SystemSettingModel();
        model->setID(sqlite3_column_int(ppStmt1, 0));
        model->setDlServerUrl((const char*)sqlite3_column_text(ppStmt1, 1));
        model->setPublicSiteUrl((const char*)sqlite3_column_text(ppStmt1, 2));
        model->setRuleServerUrl((const char*)sqlite3_column_text(ppStmt1, 3));
        model->setEventServerUrl((const char*)sqlite3_column_text(ppStmt1, 4));
        model->setHelpServerUrl((const char*)sqlite3_column_text(ppStmt1, 5));
        model->setSettlementServerUrl((const char*)sqlite3_column_text(ppStmt1, 6));
        model->setFaqServerUrl((const char*)sqlite3_column_text(ppStmt1, 7));
        model->setIosStoreMode(sqlite3_column_int(ppStmt1, 8));
        model->setReviewIosAppVersion((const char*)sqlite3_column_text(ppStmt1, 9));
        model->setIosAppVersion((const char*)sqlite3_column_text(ppStmt1, 10));
        model->setIosForceUpdate(sqlite3_column_int(ppStmt1, 11));
        model->setAppStoreUrl((const char*)sqlite3_column_text(ppStmt1, 12));
        model->setAndroidAppVersion((const char*)sqlite3_column_text(ppStmt1, 13));
        model->setAndroidForceUpdate(sqlite3_column_int(ppStmt1, 14));
        model->setGooglePlayUrl((const char*)sqlite3_column_text(ppStmt1, 15));
        model->setCoinOfLogin(sqlite3_column_int(ppStmt1, 16));
        model->setFpOfLogin(sqlite3_column_int(ppStmt1, 17));
        model->setFpOfHelp(sqlite3_column_int(ppStmt1, 18));
        model->setInviteMessages((const char*)sqlite3_column_text(ppStmt1, 19));
        model->setTokenOfInvitationSuccess(sqlite3_column_int(ppStmt1, 20));
        model->setMaxNumberOfInvitationSuccess(sqlite3_column_int(ppStmt1, 21));
        model->setTokenOfInvited(sqlite3_column_int(ppStmt1, 22));
        model->setTokenOfDiceCustom(sqlite3_column_int(ppStmt1, 23));
        model->setTokenOfSynthesis(sqlite3_column_int(ppStmt1, 24));
        model->setFpForFreeGacha(sqlite3_column_int(ppStmt1, 25));
        model->setTokenForPayGacha(sqlite3_column_int(ppStmt1, 26));
        model->setBannerUrlFreeGacha((const char*)sqlite3_column_text(ppStmt1, 27));
        model->setBannerUrlPayGacha((const char*)sqlite3_column_text(ppStmt1, 28));
        model->setSupportCompressionRate((float)sqlite3_column_double(ppStmt1, 29));
        model->setMaxNumberOfGridChain(sqlite3_column_int(ppStmt1, 30));
        model->setRankBonusRate1((float)sqlite3_column_double(ppStmt1, 31));
        model->setRankBonusRate2((float)sqlite3_column_double(ppStmt1, 32));
        model->setRankBonusRate3((float)sqlite3_column_double(ppStmt1, 33));
        model->setRankBonusRate4((float)sqlite3_column_double(ppStmt1, 34));
        model->setMixedCoefficient1(sqlite3_column_int(ppStmt1, 35));
        model->setMixedCoefficient2(sqlite3_column_int(ppStmt1, 36));
        model->setInitFuel(sqlite3_column_int(ppStmt1, 37));
        model->setInitNumOfGarage(sqlite3_column_int(ppStmt1, 38));
        model->setNumOfTokenForExtension(sqlite3_column_int(ppStmt1, 39));
        model->setExpandedNumOfGarage(sqlite3_column_int(ppStmt1, 40));
        model->setMaxNumOfGarage(sqlite3_column_int(ppStmt1, 41));
        model->setMaxNumOfTeam(sqlite3_column_int(ppStmt1, 42));
        model->setIntervalLogin(sqlite3_column_int(ppStmt1, 43));
        model->setFuelRecoveryInterval(sqlite3_column_int(ppStmt1, 44));
        model->setCoinConversionRate((float)sqlite3_column_double(ppStmt1, 45));
        model->setFuelBill(sqlite3_column_int(ppStmt1, 46));
        model->setCreated((const char*)sqlite3_column_text(ppStmt1, 47));
        model->setModified((const char*)sqlite3_column_text(ppStmt1, 48));
        model->setCoinOfSynthesis(sqlite3_column_int(ppStmt1, 49));
        model->setMaxNumberOfUturnGridChai(sqlite3_column_int(ppStmt1, 50));
        model->setTokenOfDiceReset(sqlite3_column_int(ppStmt1, 51));
        model->setTurnPerToken(sqlite3_column_int(ppStmt1, 52));
        //////CCLOG("geted: ID = %d, mapid = %d", model->getID(), model->getMapID());
        return model;
    }
}

void SystemSettingModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         dl_server_url + "','" +
                         public_site_url + "','" +
                         rule_server_url + "','" +
                         event_server_url + "','" +
                         help_server_url + "','" +
                         settlement_server_url + "','" +
                         faq_server_url + "','" +
                         stringInt(ios_store_mode)->_string + "','" +
                         review_ios_app_version + "','" +
                         ios_app_version + "','" +
                         stringInt(ios_force_update)->_string + "','" +
                         app_store_url + "','" +
                         android_app_version + "','" +
                         stringInt(android_force_update)->_string + "','" +
                         google_play_url + "','" +
                         stringInt(coin_of_login)->_string + "','" +
                         stringInt(fp_of_login)->_string + "','" +
                         stringInt(fp_of_help)->_string + "','" +
                         invite_messages + "','" +
                         stringInt(token_of_invitation_success)->_string + "','" +
                         stringInt(max_number_of_invitation_success)->_string + "','" +
                         stringInt(token_of_invited)->_string + "','" +
                         stringInt(token_of_dice_custom)->_string + "','" +
                         stringInt(token_of_synthesis)->_string + "','" +
                         stringInt(fp_for_free_gacha)->_string + "','" +
                         stringInt(token_for_pay_gacha)->_string + "','" +
                         banner_url_free_gacha + "','" +
                         banner_url_pay_gacha + "','" +
                         stringFloat(support_compression_rate)->_string + "','" +
                         stringInt(max_number_of_grid_chain)->_string + "','" +
                         stringFloat(rank_bonus_rate1)->_string + "','" +
                         stringFloat(rank_bonus_rate2)->_string + "','" +
                         stringFloat(rank_bonus_rate3)->_string + "','" +
                         stringFloat(rank_bonus_rate4)->_string + "','" +
                         stringInt(mixed_coefficient1)->_string + "','" +
                         stringInt(mixed_coefficient2)->_string + "','" +
                         stringInt(init_fuel)->_string + "','" +
                         stringInt(init_num_of_garage)->_string + "','" +
                         stringInt(num_of_token_for_extension)->_string + "','" +
                         stringInt(expanded_num_of_garage)->_string + "','" +
                         stringInt(max_num_of_garage)->_string + "','" +
                         stringInt(max_num_of_team)->_string + "','" +
                         stringInt(interval_login)->_string + "','" +
                         stringInt(fuel_recovery_interval)->_string + "','" +
                         stringFloat(coin_conversion_rate)->_string + "','" +
                         stringInt(fuel_bill)->_string + "','" +
                         created + "','" +
                         modified + "','" +
                         stringInt(coin_of_synthesis)->_string + "','" +
                         stringInt(max_number_of_uturn_grid_chai)->_string + "','" +
                         stringInt(token_of_dice_reset)->_string + "','" +
                         stringInt(turn_per_token)->_string + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void SystemSettingModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET dl_server_url = '" + dl_server_url +
                         "', public_site_url = '" + public_site_url +
                         "', rule_server_url = '" + rule_server_url +
                         "', event_server_url = '" + event_server_url +
                         "', help_server_url = '" + help_server_url +
                         "', settlement_server_url = '" + settlement_server_url +
                         "', faq_server_url = '" + faq_server_url +
                         "', ios_store_mode = '" + stringInt(ios_store_mode)->_string +
                         "', review_ios_app_version = '" + review_ios_app_version +
                         "', ios_app_version = '" + ios_app_version +
                         "', ios_force_update = '" + stringInt(ios_force_update)->_string +
                         "', app_store_url = '" + app_store_url +
                         "', android_app_version = '" + android_app_version +
                         "', android_force_update = '" + stringInt(android_force_update)->_string +
                         "', google_play_url = '" + google_play_url +
                         "', coin_of_login = '" + stringInt(coin_of_login)->_string +
                         "', fp_of_login = '" + stringInt(fp_of_login)->_string +
                         "', fp_of_help = '" + stringInt(fp_of_help)->_string +
                         "', invite_messages = '" + invite_messages +
                         "', token_of_invitation_success = '" + stringInt(token_of_invitation_success)->_string +
                         "', max_number_of_invitation_success = '" + stringInt(max_number_of_invitation_success)->_string +
                         "', token_of_invited = '" + stringInt(token_of_invited)->_string +
                         "', token_of_dice_custom = '" + stringInt(token_of_dice_custom)->_string +
                         "', token_of_synthesis = '" + stringInt(token_of_synthesis)->_string +
                         "', fp_for_free_gacha = '" + stringInt(fp_for_free_gacha)->_string +
                         "', token_for_pay_gacha = '" + stringInt(token_for_pay_gacha)->_string +
                         "', banner_url_free_gacha = '" + banner_url_free_gacha +
                         "', banner_url_pay_gacha = '" + banner_url_pay_gacha +
                         "', support_compression_rate = '" + stringFloat(support_compression_rate)->_string +
                         "', max_number_of_grid_chain = '" + stringInt(max_number_of_grid_chain)->_string +
                         "', rank_bonus_rate1 = '" + stringFloat(rank_bonus_rate1)->_string +
                         "', rank_bonus_rate2 = '" + stringFloat(rank_bonus_rate2)->_string +
                         "', rank_bonus_rate3 = '" + stringFloat(rank_bonus_rate3)->_string +
                         "', rank_bonus_rate4 = '" + stringFloat(rank_bonus_rate4)->_string +
                         "', mixed_coefficient1 = '" + stringInt(mixed_coefficient1)->_string +
                         "', mixed_coefficient2 = '" + stringInt(mixed_coefficient2)->_string +
                         "', init_fuel = '" + stringInt(init_fuel)->_string +
                         "', init_num_of_garage = '" + stringInt(init_num_of_garage)->_string +
                         "', num_of_token_for_extension = '" + stringInt(num_of_token_for_extension)->_string +
                         "', expanded_num_of_garage = '" + stringInt(expanded_num_of_garage)->_string +
                         "', max_num_of_garage = '" + stringInt(max_num_of_garage)->_string +
                         "', max_num_of_team = '" + stringInt(max_num_of_team)->_string +
                         "', interval_login = '" + stringInt(interval_login)->_string +
                         "', fuel_recovery_interval = '" + stringInt(fuel_recovery_interval)->_string +
                         "', coin_conversion_rate = '" + stringFloat(coin_conversion_rate)->_string +
                         "', fuel_bill = '" + stringInt(fuel_bill)->_string +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "', coin_of_synthesis = '" + stringInt(coin_of_synthesis)->_string +
                         "', max_number_of_uturn_grid_chai = '" + stringInt(max_number_of_uturn_grid_chai)->_string +
                         "', token_of_dice_reset = '" + stringInt(token_of_dice_reset)->_string +
                         "', turn_per_token = '" + stringInt(turn_per_token)->_string +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}



void SystemSettingModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        dropTable(tableName);
        createSystemSettingTable();

        char buffer[] = "INSERT INTO mst_system_settings VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8 ,?9 ,?10 ,?11 ,?12 ,?13 ,?14 ,?15 ,?16 ,?17 ,?18 ,?19 ,?20 ,?21 ,?22 ,?23 ,?24 ,?25 ,?26 ,?27 ,?28 ,?29 ,?30 ,?31 ,?32 ,?33 ,?34 ,?35 ,?36 ,?37 ,?38 ,?39 ,?40 ,?41 ,?42 ,?43 ,?44 ,?45 ,?46 ,?47 ,?48 ,?49 ,?50, ?51, ?52, ?53)";

//        strcpy(buffer, command.c_str());
//        //CCLOG("---------");
//        //CCLOG("%s" ,buffer);
//        //CCLOG("---------");
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            std::string dl_server_url = Json_getString(child, "dl_server_url", "");
            std::string public_site_url = Json_getString(child, "public_site_url", "");
            std::string rule_server_url = Json_getString(child, "rule_server_url", "");
            std::string event_server_url = Json_getString(child, "event_server_url", "");
            std::string help_server_url = Json_getString(child, "help_server_url", "");
            std::string settlement_server_url = Json_getString(child, "settlement_server_url", "");
            std::string faq_server_url = Json_getString(child, "faq_server_url", "");
            int ios_store_mode = atoi(Json_getString(child, "ios_store_mode", "-1"));
            std::string review_ios_app_version = Json_getString(child, "review_ios_app_version", "");
            std::string ios_app_version = Json_getString(child, "ios_app_version", "");
            int ios_force_update = atoi(Json_getString(child, "ios_force_update", "-1"));
            std::string app_store_url = Json_getString(child, "app_store_url", "");
            std::string android_app_version = Json_getString(child, "android_app_version", "");
            int android_force_update = atoi(Json_getString(child, "android_force_update", "-1"));
            std::string google_play_url = Json_getString(child, "google_play_url", "");

            int coin_of_login = atoi(Json_getString(child, "coin_of_login", "-1"));
            int fp_of_login = atoi(Json_getString(child, "fp_of_login", "-1"));
            int fp_of_help = atoi(Json_getString(child, "fp_of_help", "-1"));
            std::string invite_messages = Json_getString(child, "invite_messages", "");
            int token_of_invitation_success = atoi(Json_getString(child, "token_of_invitation_success", "-1"));

            int max_number_of_invitation_success = atoi(Json_getString(child, "max_number_of_invitation_success", "-1"));
            int token_of_invited = atoi(Json_getString(child, "token_of_invited", "-1"));
            int token_of_dice_custom = atoi(Json_getString(child, "token_of_dice_custom", "-1"));
            int token_of_synthesis = atoi(Json_getString(child, "token_of_synthesis", "-1"));
            int fp_for_free_gacha = atoi(Json_getString(child, "fp_for_free_gacha", "-1"));

            int token_for_pay_gacha = atoi(Json_getString(child, "token_for_pay_gacha", "-1"));
            std::string banner_url_free_gacha = Json_getString(child, "banner_url_free_gacha", "");
            std::string banner_url_pay_gacha = Json_getString(child, "banner_url_pay_gacha", "");
            double support_compression_rate = atof(Json_getString(child, "support_compression_rate", "-1"));
            int max_number_of_grid_chain = atoi(Json_getString(child, "max_number_of_grid_chain", "-1"));

            double rank_bonus_rate1 = atof(Json_getString(child, "rank_bonus_rate1", "-1"));
            double rank_bonus_rate2 = atof(Json_getString(child, "rank_bonus_rate2", "-1"));
            double rank_bonus_rate3 = atof(Json_getString(child, "rank_bonus_rate3", "-1"));
            double rank_bonus_rate4 = atof(Json_getString(child, "rank_bonus_rate4", "-1"));
            int mixed_coefficient1 = atoi(Json_getString(child, "mixed_coefficient1", "-1"));
            int mixed_coefficient2 = atoi(Json_getString(child, "mixed_coefficient2", "-1"));

            int init_fuel = atoi(Json_getString(child, "init_fuel", "-1"));
            int init_num_of_garage = atoi(Json_getString(child, "init_num_of_garage", "-1"));
            int num_of_token_for_extension = atoi(Json_getString(child, "num_of_token_for_extension", "-1"));
            int expanded_num_of_garage = atoi(Json_getString(child, "expanded_num_of_garage", "-1"));
            int max_num_of_garage = atof(Json_getString(child, "max_num_of_garage", "-1"));
            int max_num_of_team = atoi(Json_getString(child, "max_num_of_team", "-1"));
            int interval_login = atoi(Json_getString(child, "interval_login", "-1"));
            int fuel_recovery_interval = atoi(Json_getString(child, "fuel_recovery_interval", "-1"));
            float coin_conversion_rate = atof(Json_getString(child, "coin_conversion_rate", "-1"));
            int fuel_bill = atof(Json_getString(child, "fuel_bill", "-1"));

            std::string created = Json_getString(child, "created", "");
            std::string modified = Json_getString(child, "modified", "");
            int coin_of_synthesisTemp = atoi(Json_getString(child, "coin_of_synthesis", "-1"));
            int max_number_of_uturn_grid_chai = atoi(Json_getString(child, "max_number_of_uturn_grid_chain", "1"));
            int token_of_dice_reset = atoi(Json_getString(child, "token_of_dice_reset", "1"));
            int turn_per_token = atoi(Json_getString(child, "turn_per_token", "1"));

            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_text(stmt, 2, dl_server_url.c_str(), (int)dl_server_url.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 3, public_site_url.c_str(), (int)public_site_url.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 4, rule_server_url.c_str(), (int)rule_server_url.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 5, event_server_url.c_str(), (int)event_server_url.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 6, help_server_url.c_str(), (int)help_server_url.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 7, settlement_server_url.c_str(), (int)settlement_server_url.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 8, faq_server_url.c_str(), (int)faq_server_url.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 9, ios_store_mode);
            sqlite3_bind_text(stmt, 10, review_ios_app_version.c_str(), (int)review_ios_app_version.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 11, ios_app_version.c_str(), (int)ios_app_version.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 12, ios_force_update);
            sqlite3_bind_text(stmt, 13, app_store_url.c_str(), (int)app_store_url.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 14, android_app_version.c_str(), (int)android_app_version.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 15, android_force_update);
            sqlite3_bind_text(stmt, 16, google_play_url.c_str(), (int)google_play_url.size(), SQLITE_STATIC);

            sqlite3_bind_int(stmt, 17, coin_of_login);
            sqlite3_bind_int(stmt, 18, fp_of_login);
            sqlite3_bind_int(stmt, 19, fp_of_help);
            sqlite3_bind_text(stmt, 20, invite_messages.c_str(), (int)invite_messages.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 21, token_of_invitation_success);

            sqlite3_bind_int(stmt, 22, max_number_of_invitation_success);
            sqlite3_bind_int(stmt, 23, token_of_invited);
            sqlite3_bind_int(stmt, 24, token_of_dice_custom);
            sqlite3_bind_int(stmt, 25, token_of_synthesis);
            sqlite3_bind_int(stmt, 26, fp_for_free_gacha);

            sqlite3_bind_int(stmt, 27, token_for_pay_gacha);
            sqlite3_bind_text(stmt, 28, banner_url_free_gacha.c_str(), (int)banner_url_free_gacha.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 29, banner_url_pay_gacha.c_str(), (int)banner_url_pay_gacha.size(), SQLITE_STATIC);
            sqlite3_bind_double(stmt, 30, support_compression_rate);
            sqlite3_bind_int(stmt, 31, max_number_of_grid_chain);

            sqlite3_bind_double(stmt, 32, rank_bonus_rate1);
            sqlite3_bind_double(stmt, 33, rank_bonus_rate2);
            sqlite3_bind_double(stmt, 34, rank_bonus_rate3);
            sqlite3_bind_double(stmt, 35, rank_bonus_rate4);
            sqlite3_bind_int(stmt, 36, mixed_coefficient1);
            sqlite3_bind_int(stmt, 37, mixed_coefficient2);

            sqlite3_bind_int(stmt, 38, init_fuel);
            sqlite3_bind_int(stmt, 39, init_num_of_garage);
            sqlite3_bind_int(stmt, 40, num_of_token_for_extension);
            sqlite3_bind_int(stmt, 41, expanded_num_of_garage);
            sqlite3_bind_int(stmt, 42, max_num_of_garage);
            sqlite3_bind_int(stmt, 43, max_num_of_team);
            sqlite3_bind_int(stmt, 44, interval_login);
            sqlite3_bind_int(stmt, 45, fuel_recovery_interval);
            sqlite3_bind_double(stmt, 46, coin_conversion_rate);
            sqlite3_bind_int(stmt, 47, fuel_bill);
            sqlite3_bind_text(stmt, 48, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 49, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            sqlite3_bind_int(stmt, 50, coin_of_synthesisTemp);
            sqlite3_bind_int(stmt, 51, max_number_of_uturn_grid_chai);
            sqlite3_bind_int(stmt, 52, token_of_dice_reset);
            sqlite3_bind_int(stmt, 53, turn_per_token);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                ////CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}



bool SystemSettingModel::createSystemSettingTable()
{
    std::string sqlCreateSystemSetting;
    sqlCreateSystemSetting = "CREATE TABLE mst_system_settings( id	int(11) NOT NULL , \
    dl_server_url	varchar(255) DEFAULT 'NULL',\
    public_site_url	varchar(255) DEFAULT 'NULL',\
    rule_server_url	varchar(255),\
    event_server_url	varchar(255),\
    help_server_url	varchar(255),\
    settlement_server_url	varchar(255),\
    faq_server_url	varchar(255),\
    ios_store_mode	tinyint(3) NOT NULL DEFAULT '1',\
    review_ios_app_version	varchar(45) DEFAULT 'NULL',\
    ios_app_version	varchar(45) DEFAULT 'NULL',\
    ios_force_update	tinyint(3) NOT NULL DEFAULT '2',\
    app_store_url	varchar(255) DEFAULT 'NULL',\
    android_app_version	varchar(45) DEFAULT 'NULL',\
    android_force_update	tinyint(3) NOT NULL DEFAULT '2',\
    google_play_url	varchar(255) DEFAULT 'NULL',\
    coin_of_login	int(11) NOT NULL DEFAULT '0',\
    fp_of_login	int(11) NOT NULL DEFAULT '0',\
    fp_of_help	int(11) NOT NULL DEFAULT '20',\
    invite_messages	varchar(255) DEFAULT 'NULL',\
    token_of_invitation_success	int(11) DEFAULT 'NULL',\
    max_number_of_invitation_success	int(11) NOT NULL DEFAULT '0',\
    token_of_invited	int(11) DEFAULT 'NULL',\
    token_of_dice_custom	int(11) NOT NULL DEFAULT '0',\
    token_of_synthesis	int(11) NOT NULL DEFAULT '0',\
    fp_for_free_gacha	int(11) DEFAULT 'NULL',\
    token_for_pay_gacha	int(11) DEFAULT 'NULL',\
    banner_url_free_gacha	varchar(255) DEFAULT 'NULL',\
    banner_url_pay_gacha	varchar(255) DEFAULT 'NULL',\
    support_compression_rate	float DEFAULT '0.25',\
    max_number_of_grid_chain	int(11) NOT NULL DEFAULT '1',\
    rank_bonus_rate1	float DEFAULT '1',\
    rank_bonus_rate2	float DEFAULT '1',\
    rank_bonus_rate3	float DEFAULT '1',\
    rank_bonus_rate4	float DEFAULT '1',\
    mixed_coefficient1	int(11) DEFAULT '15',\
    mixed_coefficient2	int(11) DEFAULT '500',\
    init_fuel	int(11) DEFAULT '100',\
    init_num_of_garage	int(11) DEFAULT '10',\
    num_of_token_for_extension	int(11) DEFAULT '5',\
    expanded_num_of_garage	int(11) DEFAULT 'NULL',\
    max_num_of_garage	int(11) DEFAULT '100',\
    max_num_of_team	int(11) DEFAULT '1',\
    interval_login	int(11) NOT NULL DEFAULT '1',\
    fuel_recovery_interval	int(11) DEFAULT 'NULL',\
    coin_conversion_rate	float NOT NULL DEFAULT '1',\
    fuel_bill	int(11) DEFAULT '0',\
    created	timestamp DEFAULT 'NULL',\
    modified	timestamp NOT NULL,\
    coin_of_synthesis int(11) DEFAULT '-1',\
    max_number_of_uturn_grid_chain int(11) DEFAULT '1',\
    token_of_dice_reset int(11) DEFAULT '0',\
    turn_per_token int(11) DEFAULT '1',\
    PRIMARY KEY(id)\
    )";

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlCreateSystemSetting.c_str(), NULL, NULL, &errmsg);

    if (result != SQLITE_OK) {
        return false;
    }
    return true;
}
