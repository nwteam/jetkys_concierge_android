#include "ItemModel.h"
#include <cstring>
#include "editor-support/spine/Json.h"

ItemModel::ItemModel()
{
    tableName = TABLE_ITEMS;
}

std::shared_ptr<ItemModel>ItemModel::find(int id)
{
    std::stringstream sql;
    sql << "select * from mst_items where id = " << id;
    sqlite3_stmt* ppStmt1;
    if (SQLITE_OK == sqlite3_prepare_v2(DB_MANAGER->getDB(), sql.str().c_str(), -1, &ppStmt1, NULL)) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<ItemModel>model(new ItemModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void ItemModel::setValues(sqlite3_stmt* statement)
{
    setID(sqlite3_column_int(statement, 0));
    setName((const char*)sqlite3_column_text(statement, 1));
    setType(sqlite3_column_int(statement, 2));
    setEffectId(sqlite3_column_int(statement, 3));
    setBuyPrice(sqlite3_column_int(statement, 4));
    setSalePrice(sqlite3_column_int(statement, 5));
    setStar3(sqlite3_column_double(statement, 6));
    setStar4(sqlite3_column_double(statement, 7));
    setStar5(sqlite3_column_double(statement, 8));
    setStar6(sqlite3_column_double(statement, 9));
    setStar7(sqlite3_column_double(statement, 10));
    setStar8(sqlite3_column_double(statement, 11));
    setStar9(sqlite3_column_double(statement, 12));
    setCreated((const char*)sqlite3_column_text(statement, 13));
    setModified((const char*)sqlite3_column_text(statement, 14));
};

const int ItemModel::getMaxItemCount()
{
    std::string sqlstr = "SELECT COUNT (id) FROM ";
    sqlstr += TABLE_ITEMS;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            const int resultCount = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return resultCount;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}

ItemModel* ItemModel::get(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_ITEMS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new ItemModel();
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void ItemModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         name + "','" +
                         stringInt(type)->_string + "','" +
                         stringInt(effect_id)->_string + "','" +
                         stringInt(buy_price)->_string + "','" +
                         stringInt(sale_price)->_string + "','" +
                         stringFloat(star3)->_string + "','" +
                         stringFloat(star4)->_string + "','" +
                         stringFloat(star5)->_string + "','" +
                         stringFloat(star6)->_string + "','" +
                         stringFloat(star7)->_string + "','" +
                         stringFloat(star8)->_string + "','" +
                         stringFloat(star9)->_string + "','" +
                         created + "','" +
                         modified + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void ItemModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET name = '" + name +
                         "', type = '" + stringInt(type)->_string +
                         "', effect_id = '" + stringInt(effect_id)->_string +
                         "', buy_price = '" + stringInt(buy_price)->_string +
                         "', sale_price = '" + stringInt(sale_price)->_string +
                         "', star3 = '" + stringFloat(star3)->_string +
                         "', star4 = '" + stringFloat(star4)->_string +
                         "', star5 = '" + stringFloat(star5)->_string +
                         "', star6 = '" + stringFloat(star6)->_string +
                         "', star7 = '" + stringFloat(star7)->_string +
                         "', star8 = '" + stringFloat(star8)->_string +
                         "', star9 = '" + stringFloat(star9)->_string +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}


void ItemModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        char buffer[] = "INSERT INTO mst_items VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12 ,?13 ,?14 ,?15 )";
//        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12 ,?13 ,?14 ,?15 )";
//        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            std::string name =  Json_getString(child, "name", "");
            int type = atoi(Json_getString(child, "type", "-1"));
            int effect_id = atoi(Json_getString(child, "effect_id", "-1"));
            int buy_price = atoi(Json_getString(child, "buy_price", "-1"));
            int sale_price = atoi(Json_getString(child, "sale_price", "-1"));
            double star3 = atofNull(Json_getString(child, "star3", "-1.0f"));
            double star4 = atofNull(Json_getString(child, "star4", "-1.0f"));
            double star5 = atofNull(Json_getString(child, "star5", "-1.0f"));
            double star6 = atofNull(Json_getString(child, "star6", "-1.0f"));
            double star7 = atofNull(Json_getString(child, "star7", "-1.0f"));
            double star8 = atofNull(Json_getString(child, "star8", "-1.0f"));
            double star9 = atofNull(Json_getString(child, "star9", "-1.0f"));
            std::string created = Json_getString(child, "created", "");
            std::string modified = Json_getString(child, "modified", "");


            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_text(stmt, 2, name.c_str(), (int)name.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 3, type);
            sqlite3_bind_int(stmt, 4, effect_id);
            sqlite3_bind_int(stmt, 5, buy_price);
            sqlite3_bind_int(stmt, 6, sale_price);
            sqlite3_bind_double(stmt, 7, star3);
            sqlite3_bind_double(stmt, 8, star4);
            sqlite3_bind_double(stmt, 9, star5);
            sqlite3_bind_double(stmt, 10, star6);
            sqlite3_bind_double(stmt, 11, star7);
            sqlite3_bind_double(stmt, 12, star8);
            sqlite3_bind_double(stmt, 13, star9);
            sqlite3_bind_text(stmt, 14, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 15, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                ////CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}
