#ifndef __syanago__GridOperandsModel__
#define __syanago__GridOperandsModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class GridOperandsModel : public Model {
public:
    GridOperandsModel();
    static GridOperandsModel *get(int ID, int effect_kind, int kind_sub_no);
    static std::shared_ptr<GridOperandsModel> find(const int ID, const int effectKind, const int kindSubNo);
    
    bool isExist();
    void insert();
    void update();
    
    void hulkInsert(Json* mDatas);
    static bool createGridOperandsTable();
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(int, effect_kind, EffectKind);
    CC_SYNTHESIZE(int, kind_sub_no, KindSubNo);
    CC_SYNTHESIZE(float, operand1, Operand1);
    CC_SYNTHESIZE(float, operand2, Operand2);
private:
    void setValues(sqlite3_stmt* ppStmt1);
};

#endif
