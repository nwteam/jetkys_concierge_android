#include "ItemUnitModel.h"
#include <cstring>

ItemUnitModel::ItemUnitModel()
{
    tableName = TABLE_ITEM_UNITS;
}

std::shared_ptr<ItemUnitModel>ItemUnitModel::find(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_ITEM_UNITS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<ItemUnitModel>model(new ItemUnitModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

const int ItemUnitModel::getMaxItemUnitCount()
{
    std::string sqlstr = "SELECT COUNT (id) FROM ";
    sqlstr += TABLE_ITEM_UNITS;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            const int resultCount = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return resultCount;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}

const int ItemUnitModel::getItemUnitCount(const int masterItemId)
{
    std::string sqlstr = "SELECT COUNT (id) FROM ";
    sqlstr += TABLE_ITEM_UNITS;
    sqlstr += " WHERE item_id = " + stringInt(masterItemId)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            const int resultCount = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return resultCount;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}

void ItemUnitModel::setValues(sqlite3_stmt* ppStmt1)
{
    setID(sqlite3_column_int(ppStmt1, 0));
    setItemId(sqlite3_column_int(ppStmt1, 1));
    setUnitNo(sqlite3_column_int(ppStmt1, 2));
    setName((const char*)sqlite3_column_text(ppStmt1, 3));
    setFromCourse(sqlite3_column_int(ppStmt1, 4));
    setToCourse(sqlite3_column_int(ppStmt1, 5));
    setGoldRate((float)sqlite3_column_double(ppStmt1, 6));
    setSilverRate((float)sqlite3_column_double(ppStmt1, 7));
    setBronzeRate((float)sqlite3_column_double(ppStmt1, 8));
    setNonRate((float)sqlite3_column_double(ppStmt1, 9));
    setCreated((const char*)sqlite3_column_text(ppStmt1, 10));
    setModified((const char*)sqlite3_column_text(ppStmt1, 11));
}


ItemUnitModel* ItemUnitModel::get(int ID)
{
    //////CCLOG("%s%s", TABLE_AREAS, "::get(std::string ID)");
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_ITEM_UNITS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new ItemUnitModel();
            model->setID(sqlite3_column_int(ppStmt1, 0));
            model->setItemId(sqlite3_column_int(ppStmt1, 1));
            model->setUnitNo(sqlite3_column_int(ppStmt1, 2));
            model->setName((const char*)sqlite3_column_text(ppStmt1, 3));
            model->setFromCourse(sqlite3_column_int(ppStmt1, 4));
            model->setToCourse(sqlite3_column_int(ppStmt1, 5));
            model->setGoldRate((float)sqlite3_column_double(ppStmt1, 6));
            model->setSilverRate((float)sqlite3_column_double(ppStmt1, 7));
            model->setBronzeRate((float)sqlite3_column_double(ppStmt1, 8));
            model->setNonRate((float)sqlite3_column_double(ppStmt1, 9));
            model->setCreated((const char*)sqlite3_column_text(ppStmt1, 10));
            model->setModified((const char*)sqlite3_column_text(ppStmt1, 11));

            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void ItemUnitModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         stringInt(item_id)->_string + "','" +
                         stringInt(unit_no)->_string + "','" +
                         name + "','" +
                         stringInt(from_course)->_string + "','" +
                         stringInt(to_course)->_string + "','" +
                         created + "','" +
                         stringFloat(gold_rate)->_string + "','" +
                         stringFloat(silver_rate)->_string + "','" +
                         stringFloat(bronze_rate)->_string + "','" +
                         stringFloat(non_rate)->_string + "','" +
                         modified + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void ItemUnitModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET item_id = '" + stringInt(item_id)->_string +
                         "', unit_no = '" + stringInt(unit_no)->_string +
                         "', name = '" + name +
                         "', from_course = '" + stringInt(from_course)->_string +
                         "', to_course = '" + stringInt(to_course)->_string +
                         "', gold_rate = '" + stringFloat(gold_rate)->_string +
                         "', silver_rate = '" + stringFloat(silver_rate)->_string +
                         "', bronze_rate = '" + stringFloat(bronze_rate)->_string +
                         "', non_rate = '" + stringFloat(non_rate)->_string +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}


void ItemUnitModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        char buffer[] = "INSERT INTO mst_item_units VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8 ,?9 ,?10 ,?11 ,?12 )";
//        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8 ,?9 ,?10 ,?11 ,?12 )";
//        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            int item_id = atoi(Json_getString(child, "item_id", "-1"));
            int unit_no = atoi(Json_getString(child, "unit_no", "-1"));

            std::string name = Json_getString(child, "name", "");
            int from_course = atoi(Json_getString(child, "from_course", "-1"));
            int to_course = atoi(Json_getString(child, "to_course", "-1"));
            double gold_rate = atof(Json_getString(child, "gold_rate", "-1"));
            double silver_rate = atof(Json_getString(child, "silver_rate", "-1"));
            double bronze_rate = atof(Json_getString(child, "bronze_rate", "-1"));
            double non_rate = atof(Json_getString(child, "non_rate", "-1"));
            std::string created = Json_getString(child, "created", "");
            std::string modified =  Json_getString(child, "modified", "");

            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_int(stmt, 2, item_id);
            sqlite3_bind_int(stmt, 3, unit_no);
            sqlite3_bind_text(stmt, 4, name.c_str(), (int)name.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 5, from_course);
            sqlite3_bind_int(stmt, 6, to_course);
            sqlite3_bind_double(stmt, 7, gold_rate);
            sqlite3_bind_double(stmt, 8, silver_rate);
            sqlite3_bind_double(stmt, 9, bronze_rate);
            sqlite3_bind_double(stmt, 10, non_rate);
            sqlite3_bind_text(stmt, 11, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 12, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                ////CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}
