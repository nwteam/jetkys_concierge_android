#ifndef __Syanago__LevelModel__
#define __Syanago__LevelModel__

#include "Model.h"
#include "CourseModel.h"

class LevelModel : public Model {
public:
    LevelModel();
    
    static std::shared_ptr<LevelModel> find(int ID);
    static std::shared_ptr<const LevelModel> findByCourseModel(std::shared_ptr<const CourseModel> courseModel, int index);
    
    static const int maxLevel();
    
    void setValues(sqlite3_stmt *ppStmt1);
    
    static LevelModel *get(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID)
    CC_SYNTHESIZE(int64_t, required_exp, RequiredExp)
    CC_SYNTHESIZE(int, exercise, Exercise)
    CC_SYNTHESIZE(int, reaction, Reaction)
    CC_SYNTHESIZE(int, decision, Decision)
    CC_SYNTHESIZE(int, add_dice, AddDice)
    CC_SYNTHESIZE(std::string, created, Created)
    CC_SYNTHESIZE(std::string, modified, Modified)
};

#endif /* defined(__Syanago__LevelModel__) */
