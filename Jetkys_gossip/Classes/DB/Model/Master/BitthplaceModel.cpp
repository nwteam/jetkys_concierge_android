#include "BirthplaceModel.h"
#include <cstring>

BirthplaceModel::BirthplaceModel()
{
    tableName = TABLE_BIRTHPLACES;
}

std::shared_ptr<BirthplaceModel>BirthplaceModel::find(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_BIRTHPLACES;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<BirthplaceModel>model(new BirthplaceModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void BirthplaceModel::setValues(sqlite3_stmt* ppStmt1)
{
    setID(sqlite3_column_int(ppStmt1, 0));
    setName((const char*)sqlite3_column_text(ppStmt1, 1));
    setCreated((const char*)sqlite3_column_text(ppStmt1, 2));
    setModified((const char*)sqlite3_column_text(ppStmt1, 3));
}

BirthplaceModel* BirthplaceModel::get(int ID)
{
    //////CCLOG("%s%s", TABLE_BIRTHPLACES, "::get(std::string ID)");
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_BIRTHPLACES;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;

    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new BirthplaceModel();
            model->setID(sqlite3_column_int(ppStmt1, 0));
            model->setName((const char*)sqlite3_column_text(ppStmt1, 1));
            model->setCreated((const char*)sqlite3_column_text(ppStmt1, 2));
            model->setModified((const char*)sqlite3_column_text(ppStmt1, 3));

            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void BirthplaceModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         name + "','" +
                         created + "','" +
                         modified + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void BirthplaceModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET name = '" + name +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}



void BirthplaceModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        char buffer[] = "INSERT INTO mst_birthplaces VALUES (?1, ?2, ?3, ?4)";
//        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4)";
//        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;
        //        char *errmsg;
        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            std::string name = Json_getString(child, "name", "");
            std::string created = Json_getString(child, "created", "");
            std::string modified =  Json_getString(child, "modified", "");

            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_text(stmt, 2, name.c_str(), (int)name.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 3, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 4, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                // CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}
