#include "LeaderSkillModel.h"
#include <cstring>

LeaderSkillModel::LeaderSkillModel()
{
    tableName = TABLE_LEADER_SKILLS;
}

std::shared_ptr<LeaderSkillModel>LeaderSkillModel::findByCharacterId(int characterId)
{
    std::stringstream sql;
    sql <<  "select s.* from mst_characters as c\
    inner join mst_leader_skills as s\
    on c.leader_skill = s.id\
    where c.id = " << characterId;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<LeaderSkillModel>model(new LeaderSkillModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void LeaderSkillModel::setValues(sqlite3_stmt* ppStmt)
{
    CCLOG("[LeaderSkillModel::setValues]%d", sqlite3_column_int(ppStmt, 0));
    setID(sqlite3_column_int(ppStmt, 0));
    setSkillKind(sqlite3_column_int(ppStmt, 1));
    setValue1(sqlite3_column_int(ppStmt, 2));
    setValue2(sqlite3_column_int(ppStmt, 3));
    setValue3(sqlite3_column_double(ppStmt, 4));
    setValue4(sqlite3_column_int(ppStmt, 5));
    setCreated((const char*)sqlite3_column_text(ppStmt, 6));
    setModified((const char*)sqlite3_column_text(ppStmt, 7));
}

LeaderSkillModel* LeaderSkillModel::get(int ID)
{
    //////CCLOG("%s%s", TABLE_BODY_TYPES, "::get(std::string ID)");
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_LEADER_SKILLS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new LeaderSkillModel();
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void LeaderSkillModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         stringInt(skill_kind)->_string + "','" +
                         stringInt(value1)->_string + "','" +
                         stringInt(value2)->_string + "','" +
                         stringFloat(value3)->_string + "','" +
                         stringInt(value4)->_string + "','" +
                         created + "','" +
                         modified + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void LeaderSkillModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET  skill_kind = '" + stringInt(skill_kind)->_string  +
                         "', value1 = '" + stringInt(value1)->_string +
                         "', value2 = '" + stringInt(value2)->_string +
                         "', value3 = '" + stringFloat(value3)->_string +
                         "', value4 = '" + stringInt(value4)->_string +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}


void LeaderSkillModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        char buffer[] = "INSERT INTO mst_leader_skills VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8 )";
//        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8 )";
//        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            int skill_kind = atoiNull(Json_getString(child, "skill_kind", "-1"));
            int value1 = atoiNull(Json_getString(child, "value1", "-1"));
            int value2 = atoiNull(Json_getString(child, "value2", "-1"));
            double value3 = atofNull(Json_getString(child, "value3", "-1.0f"));
            int value4 = atoiNull(Json_getString(child, "value4", "-1"));
            std::string created = Json_getString(child, "created", "");
            std::string modified = Json_getString(child, "modified", "");

            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_int(stmt, 2, skill_kind);
            sqlite3_bind_int(stmt, 3, value1);
            sqlite3_bind_int(stmt, 4, value2);
            sqlite3_bind_double(stmt, 5, value3);
            sqlite3_bind_int(stmt, 6, value4);
            sqlite3_bind_text(stmt, 7, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 8, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                ////CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}
