#ifndef __Syanago__AreaModel__
#define __Syanago__AreaModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class AreaModel : public Model {
public:
    enum AREA_TYPE{
        COMMING_SOON = 0,
        NORMAL,
        EVENT,
        //3はまだ設定されてない
        TEST = 4
    };
    
    AreaModel();
    
    static std::shared_ptr<AreaModel> find(int ID);
    static std::vector<int> getNormalIds(int mapId);
    static std::vector<int> getEventIds();
    static std::vector<int> getTestIds();
    
    static AreaModel *get(int ID);
    
    static const int getFirstAreaId(const int mapId);
    static const int getLastAreaId(const int mapId);
    
    void insert();
    void update();
    
    void hulkInsert(Json* mDatas);
    
    bool createAreaTable();
    
    static std::vector<std::string> getResources();
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(int, mapID, MapID);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
    CC_SYNTHESIZE(int, area_type, AreaType);
    CC_SYNTHESIZE(int, prev_id, PrevId);
    CC_SYNTHESIZE(int, next_id, NextId);
private:
    void setValues(sqlite3_stmt* ppStmt1);
};

#endif /* defined(__Syanago__AreaModel__) */
