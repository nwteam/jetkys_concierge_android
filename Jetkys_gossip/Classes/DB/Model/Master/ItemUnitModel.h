#ifndef __Syanago__ItemUnitModel__
#define __Syanago__ItemUnitModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class ItemUnitModel : public Model {
public:
    ItemUnitModel();
    
    static std::shared_ptr<ItemUnitModel> find(int ID);
    static ItemUnitModel *get(int ID);
    
    static const int getMaxItemUnitCount();
    static const int getItemUnitCount(const int masterItemId);
    
    void setValues(sqlite3_stmt *ppStmt1);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(int, item_id, ItemId);
    CC_SYNTHESIZE(int, unit_no, UnitNo);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(int, from_course, FromCourse);
    CC_SYNTHESIZE(int, to_course, ToCourse);
    CC_SYNTHESIZE(float, gold_rate, GoldRate);
    CC_SYNTHESIZE(float, silver_rate, SilverRate);
    CC_SYNTHESIZE(float, bronze_rate, BronzeRate);
    CC_SYNTHESIZE(float, non_rate, NonRate);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
};

#endif /* defined(__Syanago__ItemUnitModel__) */
