#include "AreaModel.h"
#include "sqlite3.h"
#include <cstring>

AreaModel::AreaModel()
{
    tableName = TABLE_AREAS;
}

void AreaModel::setValues(sqlite3_stmt* ppStmt1)
{
    setID(sqlite3_column_int(ppStmt1, 0));
    setMapID(sqlite3_column_int(ppStmt1, 1));
    setName((const char*)sqlite3_column_text(ppStmt1, 2));
    setCreated((const char*)sqlite3_column_text(ppStmt1, 3));
    setModified((const char*)sqlite3_column_text(ppStmt1, 4));
    setAreaType(sqlite3_column_int(ppStmt1, 5));
    setPrevId(sqlite3_column_int(ppStmt1, 6));
    setNextId(sqlite3_column_int(ppStmt1, 7));
}

std::shared_ptr<AreaModel>AreaModel::find(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_AREAS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<AreaModel>model(new AreaModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}



std::vector<int>AreaModel::getNormalIds(int mapId)
{
    std::stringstream sql;
    sql << "SELECT id FROM mst_areas where map_id = " << mapId <<
        " and area_type = " << AREA_TYPE::NORMAL;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    std::vector<int>results;
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            results.push_back(sqlite3_column_int(ppStmt1, 0));
        }
        sqlite3_finalize(ppStmt1);
    }
    return results;
}

std::vector<int>AreaModel::getEventIds()
{
    std::stringstream sql;
    sql << "SELECT id FROM mst_areas where area_type = " << AREA_TYPE::EVENT;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    std::vector<int>results;
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            results.push_back(sqlite3_column_int(ppStmt1, 0));
        }
        sqlite3_finalize(ppStmt1);
    }
    return results;
}

std::vector<int>AreaModel::getTestIds()
{
    std::stringstream sql;
    sql << "SELECT id FROM mst_areas where area_type = " << AREA_TYPE::TEST;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    std::vector<int>results;
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            results.push_back(sqlite3_column_int(ppStmt1, 0));
        }
        sqlite3_finalize(ppStmt1);
    }
    return results;
}

const int AreaModel::getFirstAreaId(const int mapId)
{
    std::stringstream sql;
    sql << "select id from mst_areas \
    where map_id = " << mapId <<
        " AND area_type != " << AREA_TYPE::EVENT <<
        " AND area_type != " << AREA_TYPE::TEST <<
        " order by prev_id asc limit 1";
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}

const int AreaModel::getLastAreaId(const int mapId)
{
    std::stringstream sql;
    sql << "select id from mst_areas \
    where map_id = " << mapId <<
        " order by prev_id desc limit 1";
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}


AreaModel* AreaModel::get(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_AREAS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new AreaModel();
            model->setID(sqlite3_column_int(ppStmt1, 0));
            model->setMapID(sqlite3_column_int(ppStmt1, 1));
            model->setName((const char*)sqlite3_column_text(ppStmt1, 2));
            model->setCreated((const char*)sqlite3_column_text(ppStmt1, 3));
            model->setModified((const char*)sqlite3_column_text(ppStmt1, 4));
            model->setAreaType(sqlite3_column_int(ppStmt1, 5));
            model->setPrevId(sqlite3_column_int(ppStmt1, 6));
            model->setNextId(sqlite3_column_int(ppStmt1, 7));
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void AreaModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         stringInt(mapID)->_string + "','" +
                         name + "','" +
                         created + "','" +
                         modified + "','" +
                         stringInt(area_type)->_string + "','" +
                         stringInt(prev_id)->_string + "','" +
                         stringInt(next_id)->_string + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void AreaModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET map_id = '" + stringInt(mapID)->_string +
                         "', name = '" + name +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "', area_type = '" + stringInt(area_type)->_string +
                         "', prev_id = '" + stringInt(prev_id)->_string +
                         "', next_id = '" + stringInt(next_id)->_string +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}


void AreaModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        dropTable(tableName);
        createAreaTable();

//        Json* root = mDatas;
        char buffer[] = "INSERT INTO mst_areas VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)";
//        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5)";
//        char buffer[] ="";
//        std::string command = "INSERT INTO mst_areas  VALUESSS";
//        strcpy(buffer, command.c_str());
//        //CCLOG("---------------");
//        //CCLOG("%s",buffer);
//        //CCLOG("---------------");
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            int mapID = atoi(Json_getString(child, "map_id", "-1"));
            std::string name = Json_getString(child, "name", "");
            std::string created = Json_getString(child, "created", "");
            std::string modified = Json_getString(child, "modified", "");
            int area_type = atoi(Json_getString(child, "area_type", "-1"));
            int prev_id = atoi(Json_getString(child, "prev_id", "-1"));
            int next_id = atoi(Json_getString(child, "next_id", "-1"));

            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_int(stmt, 2, mapID);
            sqlite3_bind_text(stmt, 3, name.c_str(), (int)name.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 4, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 5, modified.c_str(), (int)modified.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 6, area_type);
            sqlite3_bind_int(stmt, 7, prev_id);
            sqlite3_bind_int(stmt, 8, next_id);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                // CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
//        //CCLOG("*******************************");
//        //CCLOG("%s",buffer);
//         //CCLOG("--------SUSSCESSFULY----------");
//         //CCLOG("*******************************");
    }
}

std::vector<std::string>AreaModel::getResources()
{
    ////CCLOG("%s%s", TABLE_AREAS, "::getResources");
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_AREAS;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    std::vector<std::string>resources;

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int cID = sqlite3_column_int(ppStmt1, 0);
            ////CCLOG("row %04d", cID);
            std::string path = "/worldmaps/map/area/" + StringUtils::format("%d_area", cID);
            resources.push_back(path);
            std::string path2 = "/worldmaps/map/selectAreaMenu/" + StringUtils::format("%d_select_area_menu", cID);
            resources.push_back(path2);
        }
        sqlite3_finalize(ppStmt1);
    }
    return resources;
}

bool AreaModel::createAreaTable()
{
    std::string sqlCreateArea;
    sqlCreateArea = "CREATE TABLE mst_areas (id int(11)  NOT NULL , \
    map_id int(11) NOT NULL,\
    name varchar(45) DEFAULT NULL,\
    created timestamp NULL DEFAULT NULL,\
    modified timestamp NOT NULL ,\
    area_type int(11)  NOT NULL DEFAULT '1',\
    prev_id int(11) NOT NULL DEFAULT '0',\
    next_id int(11) NOT NULL DEFAULT '0',\
    PRIMARY KEY (id)\
    )";

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlCreateArea.c_str(), NULL, NULL, &errmsg);

    if (result != SQLITE_OK) {
        return false;
    }

    return true;
}

