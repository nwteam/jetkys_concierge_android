#ifndef __Syanago__RankModel__
#define __Syanago__RankModel__

#include "Model.h"

#include "editor-support/spine/Json.h"

class RankModel : public Model {
public:
    RankModel();

    static std::shared_ptr<RankModel> find(int Id);
    static RankModel *get(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(int64_t, required_exp, RequiredExp);
    CC_SYNTHESIZE(int, max_cost, MaxCost);
    CC_SYNTHESIZE(int, max_fuel, MaxFuel);
    CC_SYNTHESIZE(int, max_friend, MaxFriend);
    CC_SYNTHESIZE(int, token, Token);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
};

#endif /* defined(__Syanago__RankModel__) */
