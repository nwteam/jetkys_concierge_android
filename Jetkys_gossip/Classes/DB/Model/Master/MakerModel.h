#ifndef __Syanago__MakerModel__
#define __Syanago__MakerModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class MakerModel : public Model {
public:
    MakerModel();
    
    static MakerModel *get(int ID);
    static std::shared_ptr<MakerModel> find(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
private:
    void setValues(sqlite3_stmt* ppStmt1);
};

#endif /* defined(__Syanago__MakerModel__) */
