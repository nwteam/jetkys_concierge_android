#ifndef __Syanago__SystemSettingModel__
#define __Syanago__SystemSettingModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class SystemSettingModel : public Model {
public:
    SystemSettingModel();
    
    static std::shared_ptr<SystemSettingModel> getModel();
    
    static SystemSettingModel *get(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    static bool createSystemSettingTable();
    
    static SystemSettingModel *getWithLastModified();
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(std::string, dl_server_url, DlServerUrl);
    CC_SYNTHESIZE(std::string, public_site_url, PublicSiteUrl);
    CC_SYNTHESIZE(std::string, rule_server_url, RuleServerUrl);
    CC_SYNTHESIZE(std::string, event_server_url, EventServerUrl);
    CC_SYNTHESIZE(std::string, help_server_url, HelpServerUrl);
    CC_SYNTHESIZE(std::string, settlement_server_url, SettlementServerUrl);
    CC_SYNTHESIZE(std::string, faq_server_url, FaqServerUrl);
    CC_SYNTHESIZE(int, ios_store_mode, IosStoreMode);
    CC_SYNTHESIZE(std::string, review_ios_app_version, ReviewIosAppVersion);
    CC_SYNTHESIZE(std::string, ios_app_version, IosAppVersion);
    CC_SYNTHESIZE(int, ios_force_update, IosForceUpdate);
    CC_SYNTHESIZE(std::string, app_store_url, AppStoreUrl);
    CC_SYNTHESIZE(std::string, android_app_version, AndroidAppVersion);
    CC_SYNTHESIZE(int, android_force_update, AndroidForceUpdate);
    CC_SYNTHESIZE(std::string, google_play_url, GooglePlayUrl);
    CC_SYNTHESIZE(int, coin_of_login, CoinOfLogin);
    CC_SYNTHESIZE(int, fp_of_login, FpOfLogin);
    CC_SYNTHESIZE(int, fp_of_help, FpOfHelp);
    CC_SYNTHESIZE(std::string, invite_messages, InviteMessages);
    CC_SYNTHESIZE(int, token_of_invitation_success, TokenOfInvitationSuccess);
    CC_SYNTHESIZE(int, max_number_of_invitation_success, MaxNumberOfInvitationSuccess);
    CC_SYNTHESIZE(int, token_of_invited, TokenOfInvited);
    CC_SYNTHESIZE(int, token_of_dice_custom, TokenOfDiceCustom);
    CC_SYNTHESIZE(int, token_of_synthesis, TokenOfSynthesis)
    CC_SYNTHESIZE(int, fp_for_free_gacha, FpForFreeGacha)
    CC_SYNTHESIZE(int, token_for_pay_gacha, TokenForPayGacha)
    CC_SYNTHESIZE(std::string, banner_url_free_gacha, BannerUrlFreeGacha)
    CC_SYNTHESIZE(std::string, banner_url_pay_gacha, BannerUrlPayGacha)
    CC_SYNTHESIZE(float, support_compression_rate, SupportCompressionRate)
    CC_SYNTHESIZE(int, max_number_of_grid_chain, MaxNumberOfGridChain)
    CC_SYNTHESIZE(float, rank_bonus_rate1, RankBonusRate1)
    CC_SYNTHESIZE(float, rank_bonus_rate2, RankBonusRate2)
    CC_SYNTHESIZE(float, rank_bonus_rate3, RankBonusRate3)
    CC_SYNTHESIZE(float, rank_bonus_rate4, RankBonusRate4)
    CC_SYNTHESIZE(int, mixed_coefficient1, MixedCoefficient1)
    CC_SYNTHESIZE(int, mixed_coefficient2, MixedCoefficient2)
    CC_SYNTHESIZE(int, init_fuel, InitFuel)
    CC_SYNTHESIZE(int, init_num_of_garage, InitNumOfGarage)
    CC_SYNTHESIZE(int, num_of_token_for_extension, NumOfTokenForExtension)
    CC_SYNTHESIZE(int, expanded_num_of_garage, ExpandedNumOfGarage)
    CC_SYNTHESIZE(int, max_num_of_garage, MaxNumOfGarage)
    CC_SYNTHESIZE(int, max_num_of_team, MaxNumOfTeam)
    CC_SYNTHESIZE(int, interval_login, IntervalLogin)
    CC_SYNTHESIZE(int, fuel_recovery_interval, FuelRecoveryInterval)
    CC_SYNTHESIZE(float, coin_conversion_rate, CoinConversionRate)
    CC_SYNTHESIZE(int, fuel_bill, FuelBill)
    CC_SYNTHESIZE(std::string, created, Created)
    CC_SYNTHESIZE(std::string, modified, Modified)
    CC_SYNTHESIZE(int, coin_of_synthesis, CoinOfSynthesis)
    CC_SYNTHESIZE(int, max_number_of_uturn_grid_chai, MaxNumberOfUturnGridChai)
    CC_SYNTHESIZE(int, token_of_dice_reset, TokenOfDiceReset);
    CC_SYNTHESIZE(int, turn_per_token, TurnPerToken);
private:
    void setValues(sqlite3_stmt* ppStmt1);
};

#endif /* defined(__Syanago__SystemSettingModel__) */
