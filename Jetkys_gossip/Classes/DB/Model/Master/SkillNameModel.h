#ifndef __Syanago__SkillNameModel__
#define __Syanago__SkillNameModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class SkillNameModel : public Model {
public:
    SkillNameModel();
    
    static std::shared_ptr<SkillNameModel> find(int ID);
    
    static SkillNameModel *get(int ID);
    
    void insert();
    void update();
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(std::string, description, Description);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
private:
    void setValues(sqlite3_stmt *ppStmt);
};

#endif /* defined(__Syanago__SkillNameModel__) */
