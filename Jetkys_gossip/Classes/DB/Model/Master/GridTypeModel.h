#ifndef __Syanago__GridTypeModel__
#define __Syanago__GridTypeModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class GridTypeModel : public Model {
public:
    GridTypeModel();
    
    static GridTypeModel *get(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(int, kind, Kind);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(int, score, Score);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
};

#endif /* defined(__Syanago__GridTypeModel__) */
