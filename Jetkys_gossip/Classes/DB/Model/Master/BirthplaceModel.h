#ifndef __Syanago__BirthplaceModel__
#define __Syanago__BirthplaceModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class BirthplaceModel : public Model {
public:
    BirthplaceModel();
    
    static BirthplaceModel *get(int ID);
    static std::shared_ptr<BirthplaceModel> find(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
    
private:
    void setValues(sqlite3_stmt* ppStmt1);
};

#endif /* defined(__Syanago__BirthplaceModel__) */
