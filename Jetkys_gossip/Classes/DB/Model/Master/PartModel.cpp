#include "PartModel.h"
#include <cstring>

PartModel::PartModel()
{
    tableName = TABLE_PARTS;
}

std::shared_ptr<PartModel>PartModel::find(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_PARTS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<PartModel>model(new PartModel());
            model->setID(sqlite3_column_int(ppStmt1, 0));
            model->setName((const char*)sqlite3_column_text(ppStmt1, 1));
            model->setExercise(sqlite3_column_int(ppStmt1, 2));
            model->setReaction(sqlite3_column_int(ppStmt1, 3));
            model->setDecision(sqlite3_column_int(ppStmt1, 4));
            model->setType(sqlite3_column_int(ppStmt1, 5));
            model->setRarity(sqlite3_column_int(ppStmt1, 6));
            model->setBuyPrice(sqlite3_column_int(ppStmt1, 7));
            model->setSalePrice(sqlite3_column_int(ppStmt1, 8));
            model->setRemarks((const char*)sqlite3_column_text(ppStmt1, 9));
            model->setDisplayOrder(sqlite3_column_int(ppStmt1, 10));
            model->setStartDatetime((const char*)sqlite3_column_text(ppStmt1, 11));
            model->setEndDatetime((const char*)sqlite3_column_text(ppStmt1, 12));
            model->setCreated((const char*)sqlite3_column_text(ppStmt1, 13));
            model->setModified((const char*)sqlite3_column_text(ppStmt1, 14));

            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

PartModel* PartModel::get(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_PARTS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;

    //////CCLOG("PartModel SQL: %s", sqlstr.c_str());

    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new PartModel();
            model->setID(sqlite3_column_int(ppStmt1, 0));
            model->setName((const char*)sqlite3_column_text(ppStmt1, 1));
            model->setExercise(sqlite3_column_int(ppStmt1, 2));
            model->setReaction(sqlite3_column_int(ppStmt1, 3));
            model->setDecision(sqlite3_column_int(ppStmt1, 4));
            model->setType(sqlite3_column_int(ppStmt1, 5));
            model->setRarity(sqlite3_column_int(ppStmt1, 6));
            model->setBuyPrice(sqlite3_column_int(ppStmt1, 7));
            model->setSalePrice(sqlite3_column_int(ppStmt1, 8));
            model->setRemarks((const char*)sqlite3_column_text(ppStmt1, 9));
            model->setDisplayOrder(sqlite3_column_int(ppStmt1, 10));
            model->setStartDatetime((const char*)sqlite3_column_text(ppStmt1, 11));
            model->setEndDatetime((const char*)sqlite3_column_text(ppStmt1, 12));
            model->setCreated((const char*)sqlite3_column_text(ppStmt1, 13));
            model->setModified((const char*)sqlite3_column_text(ppStmt1, 14));

            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void PartModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         name + "','" +
                         stringInt(exercise)->_string + "','" +
                         stringInt(reaction)->_string + "','" +
                         stringInt(decision)->_string + "','" +
                         stringInt(type)->_string + "','" +
                         stringInt(rarity)->_string + "','" +
                         stringInt(buy_price)->_string + "','" +
                         stringInt(sale_price)->_string + "','" +
                         remarks + "','" +
                         stringInt(display_order)->_string + "','" +
                         start_datetime + "','" +
                         end_datetime + "','" +
                         created + "','" +
                         modified + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void PartModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET name = '" + name +
                         "', exercise = '" + stringInt(exercise)->_string +
                         "', reaction = '" + stringInt(reaction)->_string +
                         "', decision = '" + stringInt(decision)->_string +
                         "', type = '" + stringInt(type)->_string +
                         "', rarity = '" + stringInt(rarity)->_string +
                         "', buy_price = '" + stringInt(buy_price)->_string +
                         "', remarks = '" + remarks +
                         "', display_order = '" + stringInt(display_order)->_string +
                         "', start_datetime = '" + start_datetime +
                         "', end_datetime = '" + end_datetime +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}


void PartModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        char buffer[] = "INSERT INTO mst_parts VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8 ,?9 ,?10 ,?11 ,?12 ,?13 ,?14 ,?15 )";
//        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8 ,?9 ,?10 ,?11 ,?12 ,?13 ,?14 ,?15 )";
//        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            std::string name = Json_getString(child, "name", "");
            int exercise = atoi(Json_getString(child, "exercise", "-1"));
            int reaction = atoi(Json_getString(child, "reaction", "-1"));
            int decision = atoi(Json_getString(child, "decision", "-1"));
            int type = atoi(Json_getString(child, "type", "-1"));
            int rarity = atoi(Json_getString(child, "rarity", "-1"));
            int buy_price = atoi(Json_getString(child, "buy_price", "-1"));
            int sale_price = atoi(Json_getString(child, "sale_price", "-1"));
            std::string remarks = Json_getString(child, "remarks", "");
            int display_order = atoi(Json_getString(child, "display_order", "-1"));
            std::string start_datetime = Json_getString(child, "start_datetime", "");
            std::string end_datetime = Json_getString(child, "end_datetime", "");
            std::string created = Json_getString(child, "created", "");
            std::string modified = Json_getString(child, "modified", "");

            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_text(stmt, 2, name.c_str(), (int)name.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 3, exercise);
            sqlite3_bind_int(stmt, 4, reaction);
            sqlite3_bind_int(stmt, 5, decision);
            sqlite3_bind_int(stmt, 6, type);
            sqlite3_bind_int(stmt, 7, rarity);
            sqlite3_bind_int(stmt, 8, buy_price);
            sqlite3_bind_int(stmt, 9, sale_price);
            sqlite3_bind_text(stmt, 10, remarks.c_str(), (int)remarks.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 11, display_order);
            sqlite3_bind_text(stmt, 12, start_datetime.c_str(), (int)start_datetime.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 13, modified.c_str(), (int)modified.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 14, end_datetime.c_str(), (int)end_datetime.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 15, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                ////CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}
