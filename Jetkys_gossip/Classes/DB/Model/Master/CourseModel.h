#ifndef __Syanago__CourseModel__
#define __Syanago__CourseModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class CourseModel : public Model {
public:
    enum COURSE_TYPE {
        COMMING_SOON = 0,
        NORMAL,
        BOSS,
        EVENT,
        TEST,
    };
    
    CourseModel();
    
    static std::shared_ptr<CourseModel> find(int ID);
    static std::vector<std::shared_ptr<CourseModel>> findAreaId(const int areaId);
    static std::vector<std::shared_ptr<CourseModel>> findMapId(const int mapId);
    static bool isFirstCourseCommingSoon(int areaId);
    
    static int getFirstCourseId(int areaId);
    static int getFirstCourseIdRemoveEventAndTest(int areaId);
    static int getLastCourseId(int areaId);
    
    static std::vector<int> getIds(int areaId);
    
    bool isScoreTarget() const;
    bool isTurnTarget() const;
    std::string getRaceTypeName() const;
    
    bool isGoalTarget() const;
    
    static CourseModel *get(int ID);
    
    static const int getCourseBgmIdCount();
    static const int getCourseImageIdCount();
    
    void insert();
    void update();
    
    static bool createCourseTable();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(int, map_id, MapID);
    CC_SYNTHESIZE(int, area_id, AreaID);
    CC_SYNTHESIZE(int, position_x, PositionX);
    CC_SYNTHESIZE(int, position_y, PositionY);
    CC_SYNTHESIZE(int, no, No);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(std::string, description, Description);
    CC_SYNTHESIZE(int, course_type, CourseType);
    CC_SYNTHESIZE(int, participation_rank, ParticipationRank);
    CC_SYNTHESIZE(std::string, event_start_datetime, EventStartDatetime);
    CC_SYNTHESIZE(std::string, event_end_datetime, EventEndDatetime);
    CC_SYNTHESIZE(int, base_score, BaseScore);
    CC_SYNTHESIZE(int, race_type, RaceType);
    CC_SYNTHESIZE(int, target_score, TargetScore);
    CC_SYNTHESIZE(int, max_number_of_turns, MaxNumberOfTurns);
    CC_SYNTHESIZE(int, image_id, ImageId);
    CC_SYNTHESIZE(int, bgm_id, BgmId);
    CC_SYNTHESIZE(int, required_fuel, RequiredFuel);
    CC_SYNTHESIZE(int, boss_character_id, BossCharacterId);
    CC_SYNTHESIZE(int, boss_character_level, BossCharacterLevel);
    CC_SYNTHESIZE(int, sub1_character_id, Sub1CharacterId);
    CC_SYNTHESIZE(int, sub1_character_level, Sub1CharacterLevel);
    CC_SYNTHESIZE(int, sub2_character_id, Sub2CharacterId);
    CC_SYNTHESIZE(int, sub2_character_level, Sub2CharacterLevel);
    CC_SYNTHESIZE(int, enemy_character_level, EnemyCharacterLevel);
    CC_SYNTHESIZE(float, enemy_status_rate, EnemyStatusRate);
    CC_SYNTHESIZE(int, dear_value_rank1, DearValueRank1);
    CC_SYNTHESIZE(int, dear_value_rank2, DearValueRank2);
    CC_SYNTHESIZE(int, dear_value_rank3, DearValueRank3);
    CC_SYNTHESIZE(float, leader_dear_value_rate, LeaderDearValueRate);
    CC_SYNTHESIZE(int, coin, Coin);
    CC_SYNTHESIZE(std::string, start_boss_message, StartBossMessage);
    CC_SYNTHESIZE(std::string, clear_boss_message, ClearBossMessage);
    CC_SYNTHESIZE(int, status, Status)
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
    CC_SYNTHESIZE(int, prev_id, PrevId);
    CC_SYNTHESIZE(int, next_id, NextId);
    CC_SYNTHESIZE(int, operand_id, OperandId);
    CC_SYNTHESIZE(int, enemy_plus_dice1, EnemyPlusDice1);
    CC_SYNTHESIZE(int, enemy_plus_dice2, EnemyPlusDice2);
    CC_SYNTHESIZE(int, enemy_plus_dice3, EnemyPlusDice3);
    CC_SYNTHESIZE(int, enemy_dice_correction, EnemyDiceCorrection);
    CC_SYNTHESIZE(int, enemy_dice_probability, EnemyDiceProbability);
    CC_SYNTHESIZE(int, ranking_rank1_constant, RankingRank1Constant);
    CC_SYNTHESIZE(int, ranking_rank2_constant, RankingRank2Constant);
    CC_SYNTHESIZE(int, ranking_rank3_constant, RankingRank3Constant);
    CC_SYNTHESIZE(int, ranking_base_score, RankingBaseScore);
    CC_SYNTHESIZE(int, ranking_score_constant, RankingScoreConstant);
    CC_SYNTHESIZE(int, ranking_base_turn, RankingBaseTurn);
    CC_SYNTHESIZE(int, ranking_turn_constant, RankingTurnConstant);
    CC_SYNTHESIZE(int, ranking_item_point, RankingItemPoint);
    CC_SYNTHESIZE(int, ranking_merit_constant, RankingMeritConstant);
    CC_SYNTHESIZE(int, ranking_skill_charge_constant, RankingSkillChargeConstant);
private:
    void setValues(sqlite3_stmt* ppStmt1);
};

#endif /* defined(__Syanago__CourseModel__) */
