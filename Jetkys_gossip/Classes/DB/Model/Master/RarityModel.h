#ifndef __Syanago__RarityModel__
#define __Syanago__RarityModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class RarityModel : public Model {
public:
    RarityModel();
    
    static RarityModel *get(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(int, num_of_star, NumOfStar);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
};

#endif /* defined(__Syanago__RarityModel__) */
