#ifndef __Syanago__CharacterModel__
#define __Syanago__CharacterModel__

#include "Model.h"
#include "LeaderSkillModel.h"
#include "editor-support/spine/Json.h"

class CharacterModel : public Model {
public:
    CharacterModel();
    
    static std::shared_ptr<CharacterModel> find(int ID);
    static std::vector<std::shared_ptr<const CharacterModel>> findAll();
    static std::shared_ptr<CharacterModel> awakeCharacterFind(int ID);
    static std::shared_ptr<LeaderSkillModel> getLeaderSkillModel(int ID);
    
    static int getLeaderSkillNameById(int ID);
    
    static CharacterModel *get(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    static std::vector<int> getIDs();
    static std::vector<std::string> getResources();
    static std::vector<std::shared_ptr<CharacterModel>> getFirstChoiceCharacters();
    
    static bool createCharacterTable();
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(int, base_character_id, BaseCharacterID);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(std::string, car_name, CarName);
    CC_SYNTHESIZE(int, level, Level);
    CC_SYNTHESIZE(int, cost, Cost);
    CC_SYNTHESIZE(int, base_exercise, BaseExercise);
    CC_SYNTHESIZE(int, base_reaction, BaseReaction);
    CC_SYNTHESIZE(int, base_decision, BaseDecision);
    CC_SYNTHESIZE(int, rarity, Rarity);
    CC_SYNTHESIZE(int, dice_value1, DiceValue1);
    CC_SYNTHESIZE(int, dice_value2, DiceValue2);
    CC_SYNTHESIZE(int, dice_value3, DiceValue3);
    CC_SYNTHESIZE(int, dice_value4, DiceValue4);
    CC_SYNTHESIZE(int, dice_value5, DiceValue5);
    CC_SYNTHESIZE(int, dice_value6, DiceValue6);
    CC_SYNTHESIZE(int, parts_slot1, PartsSlot1);
    CC_SYNTHESIZE(int, parts_slot2, PartsSlot2);
    CC_SYNTHESIZE(int, leader_skill, LeaderSkill);
    CC_SYNTHESIZE(int, leader_skill_name, LeaderSkillName);
    CC_SYNTHESIZE(int, skill, Skill);
    CC_SYNTHESIZE(int, body_type, BodyType);
    CC_SYNTHESIZE(int, maker_id, MakerId);
    CC_SYNTHESIZE(int, birthplace_id, BirthplaceId);
    CC_SYNTHESIZE(std::string, gacha_text, GachaText);
    CC_SYNTHESIZE(std::string, flavor_text, FlavorText);
    CC_SYNTHESIZE(int, sort_no, SortNo);
    CC_SYNTHESIZE(int, Initial_select_sortno, InitialSelectSortno);
    CC_SYNTHESIZE(int, awake_level, AwakeLevel);
    CC_SYNTHESIZE(int, awake_character_count, AwakeCharacterCount);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
private:
    void setValues(sqlite3_stmt* ppStmt1);
};

#endif /* defined(__Syanago__CharacterModel__) */
