#include "MapModel.h"
#include <cstring>

MapModel::MapModel()
{
    tableName = TABLE_MAPS;
}

void MapModel::setValues(sqlite3_stmt* ppStmt1)
{
    setID(sqlite3_column_int(ppStmt1, 0));
    setName((const char*)sqlite3_column_text(ppStmt1, 1));
    setMapType(sqlite3_column_int(ppStmt1, 2));
    setPrevId(sqlite3_column_int(ppStmt1, 3));
    setNextId(sqlite3_column_int(ppStmt1, 4));
    setCreated((const char*)sqlite3_column_text(ppStmt1, 5));
    setModified((const char*)sqlite3_column_text(ppStmt1, 6));
    setPositionX(sqlite3_column_int(ppStmt1, 7));
    setPositionY(sqlite3_column_int(ppStmt1, 8));
    setGroupId(sqlite3_column_int(ppStmt1, 9));
}

std::shared_ptr<MapModel>MapModel::find(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_MAPS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<MapModel>model(new MapModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

std::vector<std::shared_ptr<MapModel> >MapModel::findMapGroupId(const int groupId)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_MAPS;
    sqlstr += " WHERE group_id = " + stringInt(groupId)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();

    std::vector<std::shared_ptr<MapModel> >resultModel;
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<MapModel>model(new MapModel());
            model->setValues(ppStmt1);
            resultModel.push_back(model);
        }
        sqlite3_finalize(ppStmt1);
    }
    return resultModel;
}

int MapModel::getFirstMapId(const int groupId)
{
    std::stringstream sql;
    sql << "select id from mst_maps \
    where group_id = " << groupId <<
        " AND course_type != " << MAP_TYPE::COMMING_SOON <<
        " AND course_type != " << MAP_TYPE::EVENT <<
        " order by prev_id asc limit 1";
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}

int MapModel::getLastMapId(const int groupId)
{
    std::stringstream sql;
    sql << "select id from mst_maps \
    where group_id = " << groupId <<
        " AND course_type != " << MAP_TYPE::COMMING_SOON <<
        " AND course_type != " << MAP_TYPE::EVENT <<
        " order by prev_id desc limit 1";
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}


std::vector<int>MapModel::getIds()
{
    std::string sqlstr = "SELECT id FROM ";
    sqlstr += TABLE_MAPS;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    std::vector<int>results;
    int status = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            results.push_back(sqlite3_column_int(ppStmt1, 0));
        }
        sqlite3_finalize(ppStmt1);
    }
    return results;
}


MapModel* MapModel::get(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_MAPS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new MapModel();
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void MapModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         name + "','" +
                         stringInt(map_type)->_string + "','" +
                         stringInt(prev_id)->_string + "','" +
                         stringInt(next_id)->_string + "','" +
                         created + "','" +
                         modified + "','" +
                         stringInt(position_x)->_string + "','" +
                         stringInt(position_y)->_string + "','" +
                         stringInt(group_id)->_string + "')";

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {}
}

void MapModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET name = '" + name +
                         "', map_type = '" + stringInt(map_type)->_string +
                         "', prev_id = '" + stringInt(prev_id)->_string +
                         "', next_id = '" + stringInt(next_id)->_string +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "', position_x = '" + stringInt(position_x)->_string +
                         "', position_y = '" + stringInt(position_y)->_string +
                         "', group_id = '" + stringInt(group_id)->_string +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {}
}



void MapModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        dropTable(tableName);
        createMapsTable();

        char buffer[] = "INSERT INTO mst_maps VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7, ?8, ?9, ?10)";
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            std::string name = Json_getString(child, "name", "");
            int map_type = atoi(Json_getString(child, "map_type", "-1"));
            int prev_id = atoi(Json_getString(child, "prev_id", "-1"));
            int next_id = atoi(Json_getString(child, "next_id", "-1"));
            std::string created = Json_getString(child, "created", "");
            std::string modified = Json_getString(child, "modified", "");
            int positionX = atoi(Json_getString(child, "position_x", "0"));
            int positionY = atoi(Json_getString(child, "position_y", "0"));
            int groupId = atoi(Json_getString(child, "group_id", "0"));

            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_text(stmt, 2, name.c_str(), (int)name.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 3, map_type);
            sqlite3_bind_int(stmt, 4, prev_id);
            sqlite3_bind_int(stmt, 5, next_id);
            sqlite3_bind_text(stmt, 6, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 7, modified.c_str(), (int)modified.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 8, positionX);
            sqlite3_bind_int(stmt, 9, positionY);
            sqlite3_bind_int(stmt, 10, groupId);

            if (sqlite3_step(stmt) != SQLITE_DONE) {}
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}

bool MapModel::createMapsTable()
{
    std::string sqlCreateMaps;
    sqlCreateMaps = "CREATE TABLE mst_maps( id	int(11) NOT NULL , \
    name varchar(45) DEFAULT 'NULL',\
    map_type int(11)  NOT NULL DEFAULT '1',\
    prev_id int(11) NOT NULL DEFAULT '0',\
    next_id int(11) NOT NULL DEFAULT '0',\
    created	timestamp DEFAULT 'NULL',\
    modified	timestamp NOT NULL,\
    position_x int(11) NOT NULL DEFAULT '0',\
    position_y int(11) NOT NULL DEFAULT '0',\
    group_id int(11) NOT NULL DEFAULT '0',\
    PRIMARY KEY(id)\
    )";

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlCreateMaps.c_str(), NULL, NULL, &errmsg);

    if (result != SQLITE_OK) {
        return false;
    }


    return true;
}

std::vector<std::string>MapModel::getResources()
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_MAPS;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    std::vector<std::string>resources;

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int cID = sqlite3_column_int(ppStmt1, 0);
            ////CCLOG("row %04d", cID);
            std::string path1 = "/worldmaps/map/map/" + StringUtils::format("%d_map", cID);
            resources.push_back(path1);
            std::string path2 = "/worldmaps/map/mapClear/" + StringUtils::format("%d_map_clear", cID);
            resources.push_back(path2);
            std::string path3 = "/worldmaps/map/mapNonClear/" + StringUtils::format("%d_map_non_clear", cID);
            resources.push_back(path3);
            std::string path4 = "/worldmaps/map/mapSign/" + StringUtils::format("%d_map_sign", cID);
            resources.push_back(path4);
        }
        sqlite3_finalize(ppStmt1);
    }
    return resources;
}

const int MapModel::getMapGroupCount()
{
    std::string sqlstr = "SELECT DISTINCT group_id FROM ";
    sqlstr += TABLE_MAPS;
    sqlstr += " order by group_id desc limit 1";

    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}
