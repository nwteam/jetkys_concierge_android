#ifndef __Syanago__ItemModel__
#define __Syanago__ItemModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class ItemModel : public Model {
public:
    ItemModel();
    
    static std::shared_ptr<ItemModel> find(int id);
    static const int getMaxItemCount();
    
    static ItemModel *get(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(int, type, Type);
    CC_SYNTHESIZE(int, effect_id, EffectId);
    CC_SYNTHESIZE(int, buy_price, BuyPrice);
    CC_SYNTHESIZE(int, sale_price, SalePrice);
    CC_SYNTHESIZE(float, star3, Star3);
    CC_SYNTHESIZE(float, star4, Star4);
    CC_SYNTHESIZE(float, star5, Star5);
    CC_SYNTHESIZE(float, star6, Star6);
    CC_SYNTHESIZE(float, star7, Star7);
    CC_SYNTHESIZE(float, star8, Star8);
    CC_SYNTHESIZE(float, star9, Star9);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
    
private:
    void setValues(sqlite3_stmt* statement);
};

#endif /* defined(__Syanago__ItemModel__) */
