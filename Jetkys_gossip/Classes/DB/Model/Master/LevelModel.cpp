#include "LevelModel.h"
#include <cstring>

LevelModel::LevelModel()
{
    tableName = TABLE_LEVELS;
}

std::shared_ptr<LevelModel>LevelModel::find(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_LEVELS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<LevelModel>model(new LevelModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

std::shared_ptr<const LevelModel>LevelModel::findByCourseModel(std::shared_ptr<const CourseModel>courseModel, int index)
{
    int enemyLevel[3];
    if (CourseModel::COURSE_TYPE::BOSS == courseModel->getCourseType()) {
        if (index == 2) {
            enemyLevel[index] = courseModel->getBossCharacterLevel();
        } else if (index == 1)   {
            enemyLevel[index] = courseModel->getSub1CharacterLevel();
        } else  {
            enemyLevel[index] = courseModel->getSub2CharacterLevel();
        }
    } else {
        enemyLevel[index] = courseModel->getEnemyCharacterLevel();
    }
    return LevelModel::find(enemyLevel[index]);
}

void LevelModel::setValues(sqlite3_stmt* ppStmt1)
{
    setID(sqlite3_column_int(ppStmt1, 0));
    setRequiredExp(sqlite3_column_int64(ppStmt1, 1));
    setExercise(sqlite3_column_int(ppStmt1, 2));
    setReaction(sqlite3_column_int(ppStmt1, 3));
    setDecision(sqlite3_column_int(ppStmt1, 4));
    setAddDice(sqlite3_column_int(ppStmt1, 5));
    setCreated((const char*)sqlite3_column_text(ppStmt1, 6));
    setModified((const char*)sqlite3_column_text(ppStmt1, 7));
}

const int LevelModel::maxLevel()
{
    std::string sqlstr = "SELECT COUNT (id) FROM ";
    sqlstr += TABLE_LEVELS;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            const int resultCount = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return resultCount;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}


LevelModel* LevelModel::get(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_LEVELS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new LevelModel();
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void LevelModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         stringInt64(required_exp)->_string + "','" +
                         stringInt(exercise)->_string + "','" +
                         stringInt(reaction)->_string + "','" +
                         stringInt(decision)->_string + "','" +
                         stringInt(add_dice)->_string + "','" +
                         created + "','" +
                         modified + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void LevelModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET required_exp = '" + stringInt64(required_exp)->_string +
                         "', exercise = '" + stringInt(exercise)->_string +
                         "', reaction = '" + stringInt(reaction)->_string +
                         "', decision = '" + stringInt(decision)->_string +
                         "', add_dice = '" + stringInt(add_dice)->_string +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}



void LevelModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        char buffer[] = "INSERT INTO mst_levels VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8)";
//        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8)";
//        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            int64_t required_exp = atoll(Json_getString(child, "required_exp", "-1"));
            int exercise = atoi(Json_getString(child, "exercise", "-1"));
            int reaction = atoi(Json_getString(child, "reaction", "-1"));
            int decision = atoi(Json_getString(child, "decision", "-1"));
            int add_dice = atoi(Json_getString(child, "add_dice", "-1"));
            std::string created = Json_getString(child, "created", "");
            std::string modified =  Json_getString(child, "modified", "");

            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_int64(stmt, 2, required_exp);
            sqlite3_bind_int(stmt, 3, exercise);
            sqlite3_bind_int(stmt, 4, reaction);
            sqlite3_bind_int(stmt, 5, decision);
            sqlite3_bind_int(stmt, 6, add_dice);
            sqlite3_bind_text(stmt, 7, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 8, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                ////CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}
