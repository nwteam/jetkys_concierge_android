#include "CharacterModel.h"
#include <cstring>

CharacterModel::CharacterModel()
{
    tableName = TABLE_CHARACTERS;
}

std::shared_ptr<CharacterModel>CharacterModel::find(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_CHARACTERS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<CharacterModel>model(new CharacterModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

std::vector<std::shared_ptr<const CharacterModel> >CharacterModel::findAll()
{
    std::vector<std::shared_ptr<const CharacterModel> >results;
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_CHARACTERS;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<CharacterModel>model(new CharacterModel());
            model->setValues(ppStmt1);
            results.push_back(model);
        }
        sqlite3_finalize(ppStmt1);
    }
    return results;
}

std::shared_ptr<CharacterModel>CharacterModel::awakeCharacterFind(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_CHARACTERS;
    sqlstr += " WHERE base_character_id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<CharacterModel>model(new CharacterModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

std::shared_ptr<LeaderSkillModel>CharacterModel::getLeaderSkillModel(int ID)
{
    std::string sqlstr = "Select ls.* from ";
    sqlstr += TABLE_CHARACTERS;
    sqlstr += " AS c INNER JOIN ";
    sqlstr += TABLE_LEADER_SKILLS;
    sqlstr += " AS ls ON c.leader_skill = ls.id";
    sqlstr += " WHERE c.id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<LeaderSkillModel>model(new LeaderSkillModel());
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

std::vector<std::shared_ptr<CharacterModel> >CharacterModel::getFirstChoiceCharacters()
{
    std::vector<std::shared_ptr<CharacterModel> >resultCharacters;
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_CHARACTERS;
    sqlstr += " WHERE Initial_select_sortno > 0";
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<CharacterModel>model(new CharacterModel());
            model->setValues(ppStmt1);
            resultCharacters.push_back(model);
        }
        sqlite3_finalize(ppStmt1);
    }
    return resultCharacters;
}


void CharacterModel::setValues(sqlite3_stmt* ppStmt1)
{
    setID(sqlite3_column_int(ppStmt1, 0));
    setBaseCharacterID(sqlite3_column_int(ppStmt1, 1));
    setName((const char*)sqlite3_column_text(ppStmt1, 2));
    setCarName((const char*)sqlite3_column_text(ppStmt1, 3));
    setLevel(sqlite3_column_int(ppStmt1, 4));
    setCost(sqlite3_column_int(ppStmt1, 5));
    setBaseExercise(sqlite3_column_int(ppStmt1, 6));
    setBaseReaction(sqlite3_column_int(ppStmt1, 7));
    setBaseDecision(sqlite3_column_int(ppStmt1, 8));
    setRarity(sqlite3_column_int(ppStmt1, 9));
    setDiceValue1(sqlite3_column_int(ppStmt1, 10));
    setDiceValue2(sqlite3_column_int(ppStmt1, 11));
    setDiceValue3(sqlite3_column_int(ppStmt1, 12));
    setDiceValue4(sqlite3_column_int(ppStmt1, 13));
    setDiceValue5(sqlite3_column_int(ppStmt1, 14));
    setDiceValue6(sqlite3_column_int(ppStmt1, 15));
    setPartsSlot1(sqlite3_column_int(ppStmt1, 16));
    setPartsSlot2(sqlite3_column_int(ppStmt1, 17));
    setLeaderSkill(sqlite3_column_int(ppStmt1, 18));
    setLeaderSkillName(sqlite3_column_int(ppStmt1, 19));
    setSkill(sqlite3_column_int(ppStmt1, 20));
    setBodyType(sqlite3_column_int(ppStmt1, 21));
    setMakerId(sqlite3_column_int(ppStmt1, 22));
    setBirthplaceId(sqlite3_column_int(ppStmt1, 23));
    setGachaText((const char*)sqlite3_column_text(ppStmt1, 24));
    setFlavorText((const char*)sqlite3_column_text(ppStmt1, 25));
    setSortNo(sqlite3_column_int(ppStmt1, 26));
    setInitialSelectSortno(sqlite3_column_int(ppStmt1, 27));
    setAwakeLevel(sqlite3_column_int(ppStmt1, 28));
    setAwakeCharacterCount(sqlite3_column_int(ppStmt1, 29));
    setCreated((const char*)sqlite3_column_text(ppStmt1, 30));
    setModified((const char*)sqlite3_column_text(ppStmt1, 31));
}


CharacterModel* CharacterModel::get(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_CHARACTERS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new CharacterModel();
            model->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void CharacterModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         stringInt(base_character_id)->_string + "','" +
                         name + "','" +
                         car_name + "','" +
                         stringInt(level)->_string + "','" +
                         stringInt(cost)->_string + "','" +
                         stringInt(base_exercise)->_string + "','" +
                         stringInt(base_reaction)->_string + "','" +
                         stringInt(base_decision)->_string + "','" +
                         stringInt(rarity)->_string + "','" +
                         stringInt(dice_value1)->_string + "','" +
                         stringInt(dice_value2)->_string + "','" +
                         stringInt(dice_value3)->_string + "','" +
                         stringInt(dice_value4)->_string + "','" +
                         stringInt(dice_value5)->_string + "','" +
                         stringInt(dice_value6)->_string + "','" +
                         stringInt(parts_slot1)->_string + "','" +
                         stringInt(parts_slot2)->_string + "','" +
                         stringInt(leader_skill)->_string + "','" +
                         stringInt(leader_skill_name)->_string + "','" +
                         stringInt(skill)->_string + "','" +
                         stringInt(body_type)->_string + "','" +
                         stringInt(maker_id)->_string + "','" +
                         stringInt(birthplace_id)->_string + "','" +
                         gacha_text + "','" +
                         flavor_text + "','" +
                         stringInt(sort_no)->_string + "','" +
                         stringInt(Initial_select_sortno)->_string + "','" +
                         stringInt(awake_level)->_string + "','" +
                         stringInt(awake_character_count)->_string + "','" +
                         created + "','" +
                         modified + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void CharacterModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET base_character_id = '" + stringInt(base_character_id)->_string +
                         "', name = '" + name +
                         "', car_name = '" + car_name +
                         "', level = '" + stringInt(level)->_string +
                         "', cost = '" + stringInt(cost)->_string +
                         "', base_exercise = '" + stringInt(base_exercise)->_string +
                         "', base_reaction = '" + stringInt(base_reaction)->_string +
                         "', base_decision = '" + stringInt(base_decision)->_string +
                         "', rarity = '" + stringInt(rarity)->_string +
                         "', dice_value1 = '" + stringInt(dice_value1)->_string +
                         "', dice_value2 = '" + stringInt(dice_value2)->_string +
                         "', dice_value3 = '" + stringInt(dice_value3)->_string +
                         "', dice_value4 = '" + stringInt(dice_value4)->_string +
                         "', dice_value5 = '" + stringInt(dice_value5)->_string +
                         "', dice_value6 = '" + stringInt(dice_value6)->_string +
                         "', parts_slot1 = '" + stringInt(parts_slot1)->_string +
                         "', parts_slot2 = '" + stringInt(parts_slot2)->_string +
                         "', leader_skill = '" + stringInt(leader_skill)->_string +
                         "', leader_skill_name = '" + stringInt(leader_skill_name)->_string +
                         "', skill = '" + stringInt(skill)->_string +
                         "', body_type = '" + stringInt(body_type)->_string +
                         "', maker_id = '" + stringInt(maker_id)->_string +
                         "', birthplace_id = '" + stringInt(birthplace_id)->_string +
                         "', gacha_text = '" + gacha_text +
                         "', flavor_text = '" + flavor_text +
                         "', sort_no = '" + stringInt(sort_no)->_string +
                         "', Initial_select_sortno = '" + stringInt(Initial_select_sortno)->_string +
                         "', awake_level = '" + stringInt(Initial_select_sortno)->_string +
                         "', awake_character_count = '" + stringInt(Initial_select_sortno)->_string +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

std::vector<int>CharacterModel::getIDs()
{
    ////CCLOG("%s%s", TABLE_AREAS, "::getIDs");
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_CHARACTERS;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    std::vector<int>IDs;
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            IDs.push_back(sqlite3_column_int(ppStmt1, 0));
        }
        sqlite3_finalize(ppStmt1);
    }

    return IDs;
}

std::vector<std::string>CharacterModel::getResources()
{
    ////CCLOG("%s%s", TABLE_CHARACTERS, "::getResources");
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_CHARACTERS;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    std::vector<std::string>resources;

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int cID = sqlite3_column_int(ppStmt1, 0);
            //////CCLOG("row %04d", cID);
            std::string path = "/characters/" + StringUtils::format("%d_ca", cID);
            resources.push_back(path);
            std::string voicePath = "/sound/voice/" + StringUtils::format("voice_%d", cID);
            resources.push_back(voicePath);
            std::string voiceRacePath = "/sound/voice/" + StringUtils::format("voice_race_%d", cID);
            resources.push_back(voiceRacePath);
        }
        sqlite3_finalize(ppStmt1);
    }
    return resources;
}


void CharacterModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        char buffer[] = "INSERT INTO mst_characters VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8 ,?9 ,?10 ,?11 ,?12 ,?13 ,?14 ,?15 ,?16 ,?17 ,?18 ,?19 ,?20 ,?21 ,?22 ,?23 ,?24 ,?25 ,?26 ,?27 ,?28 ,?29 ,?30 , ?31, ?32)";
//        std::string command = "INSERT INTO example VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8 ,?9 ,?10 ,?11 ,?12 ,?13 ,?14 ,?15 ,?16 ,?17 ,?18 ,?19 ,?20 ,?21 ,?22 ,?23 ,?24 ,?25 ,?26 ,?27 ,?28 ,?29 ,?30)";
//        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            int base_character_id = atoi(Json_getString(child, "base_character_id", "-1"));
            std::string name = Json_getString(child, "name", "");
            std::string car_name = Json_getString(child, "car_name", "");
            int level = atoi(Json_getString(child, "level", "-1"));
            int cost = atoi(Json_getString(child, "cost", "-1"));
            int base_exercise = atoi(Json_getString(child, "base_exercise", "-1"));
            int base_reaction = atoi(Json_getString(child, "base_reaction", "-1"));
            int base_decision = atoi(Json_getString(child, "base_decision", "-1"));
            int rarity = atoi(Json_getString(child, "rarity", "-1"));
            int dice_value1 = atoi(Json_getString(child, "dice_value1", "-1"));
            int dice_value2 = atoi(Json_getString(child, "dice_value2", "-1"));
            int dice_value3 = atoi(Json_getString(child, "dice_value3", "-1"));
            int dice_value4 = atoi(Json_getString(child, "dice_value4", "-1"));
            int dice_value5 = atoi(Json_getString(child, "dice_value5", "-1"));
            int dice_value6 = atoi(Json_getString(child, "dice_value6", "-1"));
            int parts_slot1 = atoi(Json_getString(child, "parts_slot1", "-1"));
            int parts_slot2 = atoi(Json_getString(child, "parts_slot2", "-1"));
            int leader_skill = atoi(Json_getString(child, "leader_skill", "-1"));
            int leader_skill_name = atoi(Json_getString(child, "leader_skill_name", "-1"));
            int skill = atoi(Json_getString(child, "skill", "-1"));
            int body_type = atoi(Json_getString(child, "body_type", "-1"));
            int maker_id = atoi(Json_getString(child, "maker_id", "-1"));
            int birthplace_id = atoi(Json_getString(child, "birthplace_id", "-1"));
            std::string gacha_text = Json_getString(child, "gacha_text", "");
            std::string flavor_text = Json_getString(child, "flavor_text", "");
            int sort_no = atoi(Json_getString(child, "sort_no", "-1"));
            int Initial_select_sortno = atoi(Json_getString(child, "Initial_select_sortno", "-1"));
            int awake_level = atoi(Json_getString(child, "awake_level", "-1"));
            int awake_character_count = atoi(Json_getString(child, "awake_character_count", "-1"));
            std::string created = Json_getString(child, "created", "");
            std::string modified = Json_getString(child, "modified", "");
            //            sqlite3_bind_text(stmt, 1, ID.c_str(), ID.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_int(stmt, 2, base_character_id);
            sqlite3_bind_text(stmt, 3, name.c_str(), (int)name.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 4, car_name.c_str(), (int)car_name.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 5, level);
            sqlite3_bind_int(stmt, 6, cost);
            sqlite3_bind_int(stmt, 7, base_exercise);
            sqlite3_bind_int(stmt, 8, base_reaction);
            sqlite3_bind_int(stmt, 9, base_decision);
            sqlite3_bind_int(stmt, 10, rarity);
            sqlite3_bind_int(stmt, 11, dice_value1);
            sqlite3_bind_int(stmt, 12, dice_value2);
            sqlite3_bind_int(stmt, 13, dice_value3);
            sqlite3_bind_int(stmt, 14, dice_value4);
            sqlite3_bind_int(stmt, 15, dice_value5);
            sqlite3_bind_int(stmt, 16, dice_value6);
            sqlite3_bind_int(stmt, 17, parts_slot1);
            sqlite3_bind_int(stmt, 18, parts_slot2);
            sqlite3_bind_int(stmt, 19, leader_skill);
            sqlite3_bind_int(stmt, 20, leader_skill_name);
            sqlite3_bind_int(stmt, 21, skill);
            sqlite3_bind_int(stmt, 22, body_type);
            sqlite3_bind_int(stmt, 23, maker_id);
            sqlite3_bind_int(stmt, 24, birthplace_id);
            sqlite3_bind_text(stmt, 25, gacha_text.c_str(), (int)gacha_text.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 26, flavor_text.c_str(), (int)flavor_text.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 27, sort_no);
            sqlite3_bind_int(stmt, 28, Initial_select_sortno);
            sqlite3_bind_int(stmt, 29, awake_level);
            sqlite3_bind_int(stmt, 30, awake_character_count);
            sqlite3_bind_text(stmt, 31, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 32, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                // CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}

bool CharacterModel::createCharacterTable()
{
    std::string sqlCreateCourse;
    sqlCreateCourse = "CREATE TABLE mst_characters (id int(11) NOT NULL,\
    base_character_id int(11) NOT NULL,\
    name varchar(45) DEFAULT NULL,\
    car_name varchar(45) DEFAULT NULL,\
    level int(11) DEFAULT '0',\
    cost int(11) DEFAULT '0',\
    base_exercise int(11) DEFAULT '0',\
    base_reaction int(11) DEFAULT '0',\
    base_decision int(11) DEFAULT '0',\
    rarity int(11) DEFAULT '0',\
    dice_value1 int(11) DEFAULT '0',\
    dice_value2 int(11) DEFAULT '0',\
    dice_value3 int(11) DEFAULT '0',\
    dice_value4 int(11) DEFAULT '0',\
    dice_value5 int(11) DEFAULT '0',\
    dice_value6 int(11) DEFAULT '0',\
    parts_slot1 int(11) DEFAULT '0',\
    parts_slot2 int(11) DEFAULT '0',\
    leader_skill int(11) DEFAULT '0',\
    leader_skill_name int(11) DEFAULT '0',\
    skill int(11) DEFAULT '0',\
    body_type int(11) DEFAULT '0',\
    maker_id int(11) DEFAULT '0',\
    birthplace_id int(11) DEFAULT '0',\
    gacha_text varchar(45) DEFAULT NULL,\
    flavor_text varchar(45) DEFAULT NULL,\
    sort_no int(11) DEFAULT '0',\
    Initial_select_sortno int(11) DEFAULT '0',\
    awake_level int(11) DEFAULT '0',\
    awake_character_count int(11) DEFAULT '0',\
    created	timestamp DEFAULT NULL,\
    modified timestamp NOT NULL,\
    PRIMARY KEY(id) \
    )";

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlCreateCourse.c_str(), NULL, NULL, &errmsg);

    if (result != SQLITE_OK) {
        CCLOG("Drop table failed!");

        ////CCLOG("err: %d, mess %s", result, errmsg);
        return false;
    }


    CCLOG("Drop table susscessfully!");

    return true;
}

