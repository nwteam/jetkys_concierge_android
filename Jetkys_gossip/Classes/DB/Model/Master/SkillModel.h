#ifndef __Syanago__SkillModel__
#define __Syanago__SkillModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class SkillModel : public Model {
public:
    SkillModel();
    
    static std::shared_ptr<SkillModel> find(int ID);
    const std::string getSkillName() const;
    const std::string getSkillDescription() const;
    
    
    static SkillModel *get(int ID);
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(int, skill_kind, SkillKind);
    CC_SYNTHESIZE(int, value1, Value1);
    CC_SYNTHESIZE(int, value2, Value2);
    CC_SYNTHESIZE(int, value3, Value3);
    CC_SYNTHESIZE(int, trigger_conditions, TriggerConditions);
    //CC_SYNTHESIZE(int, execution_timing, ExecutionTiming);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
private:
    void setValues(sqlite3_stmt *ppStmt);
};

#endif /* defined(__Syanago__SkillModel__) */
