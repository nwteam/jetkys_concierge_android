#include "RankModel.h"
#include <cstring>

RankModel::RankModel()
{
    tableName = TABLE_RANKS;
}

std::shared_ptr<RankModel>RankModel::find(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_RANKS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<RankModel>model(new RankModel());
            model->setID(sqlite3_column_int(ppStmt1, 0));
            model->setRequiredExp(sqlite3_column_int64(ppStmt1, 1));
            model->setMaxCost(sqlite3_column_int(ppStmt1, 2));
            model->setMaxFuel(sqlite3_column_int(ppStmt1, 3));
            model->setMaxFriend(sqlite3_column_int(ppStmt1, 4));
            model->setToken(sqlite3_column_int(ppStmt1, 5));
            model->setCreated((const char*)sqlite3_column_text(ppStmt1, 6));
            model->setModified((const char*)sqlite3_column_text(ppStmt1, 7));

            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

RankModel* RankModel::get(int ID)
{
    //////CCLOG("%s%s", TABLE_AREAS, "::get(std::string ID)");
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_RANKS;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new RankModel();
            model->setID(sqlite3_column_int(ppStmt1, 0));
            model->setRequiredExp(sqlite3_column_int64(ppStmt1, 1));
            model->setMaxCost(sqlite3_column_int(ppStmt1, 2));
            model->setMaxFuel(sqlite3_column_int(ppStmt1, 3));
            model->setMaxFriend(sqlite3_column_int(ppStmt1, 4));
            model->setToken(sqlite3_column_int(ppStmt1, 5));
            model->setCreated((const char*)sqlite3_column_text(ppStmt1, 6));
            model->setModified((const char*)sqlite3_column_text(ppStmt1, 7));

            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void RankModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         stringInt64(required_exp)->_string + "','" +
                         stringInt(max_cost)->_string + "','" +
                         stringInt(max_fuel)->_string + "','" +
                         stringInt(max_friend)->_string + "','" +
                         stringInt(token)->_string + "','" +
                         created + "','" +
                         modified + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void RankModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET required_exp = '" + stringInt64(required_exp)->_string +
                         "', max_cost = '" + stringInt(max_cost)->_string +
                         "', max_fuel = '" + stringInt(max_fuel)->_string +
                         "', max_friend = '" + stringInt(max_friend)->_string +
                         "', token = '" + stringInt(token)->_string +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}




void RankModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        char buffer[] = "INSERT INTO mst_ranks VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8)";
//        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8)";
//        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            int64_t required_exp = atoll(Json_getString(child, "required_exp", "-1"));
            int max_cost = atoi(Json_getString(child, "max_cost", "-1"));
            int max_fuel = atoi(Json_getString(child, "max_fuel", "-1"));
            int max_friend = atoi(Json_getString(child, "max_friend", "-1"));
            int token = atoi(Json_getString(child, "token", "-1"));
            std::string created = Json_getString(child, "created", "");
            std::string modified = Json_getString(child, "modified", "");

            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_int64(stmt, 2, required_exp);
            sqlite3_bind_int(stmt, 3, max_cost);
            sqlite3_bind_int(stmt, 4, max_fuel);
            sqlite3_bind_int(stmt, 5, max_friend);
            sqlite3_bind_int(stmt, 6, token);
            sqlite3_bind_text(stmt, 7, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 8, modified.c_str(), (int)modified.size(), SQLITE_STATIC);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                ////CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}
