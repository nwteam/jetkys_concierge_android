#ifndef __Syanago__BodyTypeModel__
#define __Syanago__BodyTypeModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class BodyTypeModel : public Model {
public:
    BodyTypeModel();
    
    static BodyTypeModel *get(int ID);
    static std::shared_ptr<BodyTypeModel> find(int ID);
    
    static const int getMaxBodyTypeCount();
    
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, ID, ID);
    CC_SYNTHESIZE(std::string, name, Name);
    CC_SYNTHESIZE(int, price_coefficient, PriceCoefficient);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
    
private:
    void setValues(sqlite3_stmt* ppStmt1);

};

#endif /* defined(__Syanago__BodyTypeModel__) */
