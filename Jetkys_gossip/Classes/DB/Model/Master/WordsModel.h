#ifndef __syanago__WordsModel__
#define __syanago__WordsModel__

#include "Model.h"
#include "editor-support/spine/Json.h"

class WordsModel : public Model {
public:
    WordsModel();
    static WordsModel *get(int chracterId, int placeId, int actionId, int wordId);
    static std::shared_ptr<WordsModel> find(int chracterId, int placeId, int actionId, int wordId);
    
    bool isExist();
    void insert();
    void update();
    //haianhnc20141121
    void hulkInsert(Json* mDatas);
    
    CC_SYNTHESIZE(int, character_id, CharacterId);
    CC_SYNTHESIZE(int, place_id, PlaceId);
    CC_SYNTHESIZE(int, action_id, ActionId);
    CC_SYNTHESIZE(int, word_id, WordId);
    CC_SYNTHESIZE(std::string, words, Words);
    CC_SYNTHESIZE(int, dear_value, DearValue);
    CC_SYNTHESIZE(std::string, created, Created);
    CC_SYNTHESIZE(std::string, modified, Modified);
private:
    void setValues(sqlite3_stmt* ppStmt1);
};

#endif /* defined(__syanago__WordsModel__) */
