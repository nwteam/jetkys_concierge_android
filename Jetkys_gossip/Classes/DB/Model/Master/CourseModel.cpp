#include "CourseModel.h"
#include <cstring>

CourseModel::CourseModel()
{
    tableName = TABLE_COURSES;
}

std::vector<int>CourseModel::getIds(int areaId)
{
    std::stringstream sql;
    sql << "SELECT id FROM mst_courses where area_id = " << areaId <<
        " and course_type != " << COURSE_TYPE::COMMING_SOON <<
        " and course_type != " << COURSE_TYPE::EVENT;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    std::vector<int>results;
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            results.push_back(sqlite3_column_int(ppStmt1, 0));
        }
        sqlite3_finalize(ppStmt1);
    }
    return results;
}

bool CourseModel::isFirstCourseCommingSoon(int areaId)
{
    std::stringstream sql;
    sql <<  "select course_type from mst_courses \
    where area_id = " << areaId <<
        " order by prev_id limit 1";
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            bool result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return (0 == result);
        }
        sqlite3_finalize(ppStmt1);
    }
    return true;
}

int CourseModel::getFirstCourseId(int areaId)
{
    std::stringstream sql;
    sql << "select id from mst_courses \
    where area_id = " << areaId <<
        " order by prev_id asc limit 1";
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}

int CourseModel::getFirstCourseIdRemoveEventAndTest(int areaId)
{
    std::stringstream sql;
    sql << "select id from mst_courses \
    where area_id = " << areaId <<
        " AND course_type != " << COURSE_TYPE::EVENT <<
        " AND course_type != " << COURSE_TYPE::TEST <<
        " order by prev_id asc limit 1";
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}

int CourseModel::getLastCourseId(int areaId)
{
    std::stringstream sql;
    sql << "select id from mst_courses \
    where area_id = " << areaId <<
        " AND course_type != " << COURSE_TYPE::EVENT <<
        " AND course_type != " << COURSE_TYPE::TEST <<
        " order by prev_id desc limit 1";
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sql.str().c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}


std::shared_ptr<CourseModel>CourseModel::find(int ID)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_COURSES;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<CourseModel>result(new CourseModel());
            result->setValues(ppStmt1);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

std::vector<std::shared_ptr<CourseModel> >CourseModel::findAreaId(const int areaId)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_COURSES;
    sqlstr += " WHERE area_id = " + stringInt(areaId)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    std::vector<std::shared_ptr<CourseModel> >resultModels;
    int status = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<CourseModel>result(new CourseModel());
            result->setValues(ppStmt1);
            resultModels.push_back(result);
        }
        sqlite3_finalize(ppStmt1);
    }
    return resultModels;
}

std::vector<std::shared_ptr<CourseModel> >CourseModel::findMapId(const int mapId)
{
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_COURSES;
    sqlstr += " WHERE map_id = " + stringInt(mapId)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    std::vector<std::shared_ptr<CourseModel> >resultModels;
    int status = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            std::shared_ptr<CourseModel>result(new CourseModel());
            result->setValues(ppStmt1);
            resultModels.push_back(result);
        }
        sqlite3_finalize(ppStmt1);
    }
    return resultModels;
}

void CourseModel::setValues(sqlite3_stmt* ppStmt1)
{
    setID(sqlite3_column_int(ppStmt1, 0));
    setMapID(sqlite3_column_int(ppStmt1, 1));
    setAreaID(sqlite3_column_int(ppStmt1, 2));
    setPositionX(sqlite3_column_int(ppStmt1, 3));
    setPositionY(sqlite3_column_int(ppStmt1, 4));
    setNo(sqlite3_column_int(ppStmt1, 5));
    setName((const char*)sqlite3_column_text(ppStmt1, 6));
    setDescription((const char*)sqlite3_column_text(ppStmt1, 7));
    setCourseType(sqlite3_column_int(ppStmt1, 8));
    setParticipationRank(sqlite3_column_int(ppStmt1, 9));
    setEventStartDatetime((const char*)sqlite3_column_text(ppStmt1, 10));
    setEventEndDatetime((const char*)sqlite3_column_text(ppStmt1, 11));
    setBaseScore(sqlite3_column_int(ppStmt1, 12));
    setRaceType(sqlite3_column_int(ppStmt1, 13));
    setTargetScore(sqlite3_column_int(ppStmt1, 14));
    setMaxNumberOfTurns(sqlite3_column_int(ppStmt1, 15));
    setImageId(sqlite3_column_int(ppStmt1, 16));
    setRequiredFuel(sqlite3_column_int(ppStmt1, 17));
    setBossCharacterId(sqlite3_column_int(ppStmt1, 18));
    setBossCharacterLevel(sqlite3_column_int(ppStmt1, 19));
    setSub1CharacterId(sqlite3_column_int(ppStmt1, 20));
    setSub1CharacterLevel(sqlite3_column_int(ppStmt1, 21));
    setSub2CharacterId(sqlite3_column_int(ppStmt1, 22));
    setSub2CharacterLevel(sqlite3_column_int(ppStmt1, 23));
    setEnemyCharacterLevel(sqlite3_column_int(ppStmt1, 24));
    setEnemyStatusRate((float)sqlite3_column_double(ppStmt1, 25));
    setDearValueRank1(sqlite3_column_int(ppStmt1, 26));
    setDearValueRank2(sqlite3_column_int(ppStmt1, 27));
    setDearValueRank3(sqlite3_column_int(ppStmt1, 28));
    setLeaderDearValueRate((float)sqlite3_column_double(ppStmt1, 29));
    setCoin(sqlite3_column_int(ppStmt1, 30));
    setStartBossMessage((const char*)sqlite3_column_text(ppStmt1, 31));
    setClearBossMessage((const char*)sqlite3_column_text(ppStmt1, 32));
    setStatus(sqlite3_column_int(ppStmt1, 33));
    setCreated((const char*)sqlite3_column_text(ppStmt1, 34));
    setModified((const char*)sqlite3_column_text(ppStmt1, 35));
    setBgmId(sqlite3_column_int(ppStmt1, 36));
    setPrevId(sqlite3_column_int(ppStmt1, 37));
    setNextId(sqlite3_column_int(ppStmt1, 38));
    setOperandId(sqlite3_column_int(ppStmt1, 39));
    setEnemyPlusDice1(sqlite3_column_int(ppStmt1, 40));
    setEnemyPlusDice2(sqlite3_column_int(ppStmt1, 41));
    setEnemyPlusDice3(sqlite3_column_int(ppStmt1, 42));
    setEnemyDiceCorrection(sqlite3_column_int(ppStmt1, 43));
    setEnemyDiceProbability(sqlite3_column_int(ppStmt1, 44));
    setRankingRank1Constant(sqlite3_column_int(ppStmt1, 45));
    setRankingRank2Constant(sqlite3_column_int(ppStmt1, 46));
    setRankingRank3Constant(sqlite3_column_int(ppStmt1, 47));
    setRankingBaseScore(sqlite3_column_int(ppStmt1, 48));
    setRankingScoreConstant(sqlite3_column_int(ppStmt1, 49));
    setRankingBaseTurn(sqlite3_column_int(ppStmt1, 50));
    setRankingTurnConstant(sqlite3_column_int(ppStmt1, 51));
    setRankingItemPoint(sqlite3_column_int(ppStmt1, 52));
    setRankingMeritConstant(sqlite3_column_int(ppStmt1, 53));
    setRankingSkillChargeConstant(sqlite3_column_int(ppStmt1, 54));
}

bool CourseModel::isScoreTarget() const
{
    return race_type == 2 || race_type == 6 || race_type == 7;
}

bool CourseModel::isTurnTarget() const
{
    return race_type == 3 || race_type == 8 || race_type == 9;
}

bool CourseModel::isGoalTarget() const
{
    return race_type == 1 || race_type == 4 || race_type == 5;
}

std::string CourseModel::getRaceTypeName() const
{
    std::string result = "";
    if (race_type == 1) {
        result = "３位以内でゴールせよ！";
    } else if (race_type == 2)  {
        result = StringUtils::format("スコア%d以上獲得せよ！", target_score);
    } else if (race_type == 3)  {
        result = StringUtils::format("%dターン以内にゴールせよ！", max_number_of_turns);
    } else if (race_type == 4)  {
        result = "２位以内でゴールせよ！";
    } else if (race_type == 5)  {
        result = "１位でゴールせよ！";
    } else if (race_type == 6)  {
        result = StringUtils::format("２位以内でスコア%d以上獲得せよ！", target_score);
    } else if (race_type == 7)  {
        result = StringUtils::format("１位でスコア%d以上獲得せよ！", target_score);
    } else if (race_type == 8)  {
        result = StringUtils::format("２位以内で%dターン以内にゴールせよ！", max_number_of_turns);
    } else if (race_type == 9)  {
        result = StringUtils::format("１位で%dターン以内にゴールせよ！", max_number_of_turns);
    }
    return result;
}

CourseModel* CourseModel::get(int ID)
{
    //////CCLOG("%s%s", TABLE_COURSES, "::get(std::string ID)");
    std::string sqlstr = "SELECT * FROM ";
    sqlstr += TABLE_COURSES;
    sqlstr += " WHERE id = " + stringInt(ID)->_string;
    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int result = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);

    if (result == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            auto model = new CourseModel();
            model->setID(sqlite3_column_int(ppStmt1, 0));
            model->setMapID(sqlite3_column_int(ppStmt1, 1));
            model->setAreaID(sqlite3_column_int(ppStmt1, 2));
            model->setPositionX(sqlite3_column_int(ppStmt1, 3));
            model->setPositionY(sqlite3_column_int(ppStmt1, 4));
            model->setNo(sqlite3_column_int(ppStmt1, 5));
            model->setName((const char*)sqlite3_column_text(ppStmt1, 6));
            model->setDescription((const char*)sqlite3_column_text(ppStmt1, 7));
            model->setCourseType(sqlite3_column_int(ppStmt1, 8));
            model->setParticipationRank(sqlite3_column_int(ppStmt1, 9));
            model->setEventStartDatetime((const char*)sqlite3_column_text(ppStmt1, 10));
            model->setEventEndDatetime((const char*)sqlite3_column_text(ppStmt1, 11));
            model->setBaseScore(sqlite3_column_int(ppStmt1, 12));
            model->setRaceType(sqlite3_column_int(ppStmt1, 13));
            model->setTargetScore(sqlite3_column_int(ppStmt1, 14));
            model->setMaxNumberOfTurns(sqlite3_column_int(ppStmt1, 15));
            model->setImageId(sqlite3_column_int(ppStmt1, 16));
            model->setRequiredFuel(sqlite3_column_int(ppStmt1, 17));
            model->setBossCharacterId(sqlite3_column_int(ppStmt1, 18));
            model->setBossCharacterLevel(sqlite3_column_int(ppStmt1, 19));
            model->setSub1CharacterId(sqlite3_column_int(ppStmt1, 20));
            model->setSub1CharacterLevel(sqlite3_column_int(ppStmt1, 21));
            model->setSub2CharacterId(sqlite3_column_int(ppStmt1, 22));
            model->setSub2CharacterLevel(sqlite3_column_int(ppStmt1, 23));
            model->setEnemyCharacterLevel(sqlite3_column_int(ppStmt1, 24));
            model->setEnemyStatusRate((float)sqlite3_column_double(ppStmt1, 25));
            model->setDearValueRank1(sqlite3_column_int(ppStmt1, 26));
            model->setDearValueRank2(sqlite3_column_int(ppStmt1, 27));
            model->setDearValueRank3(sqlite3_column_int(ppStmt1, 28));
            model->setLeaderDearValueRate((float)sqlite3_column_double(ppStmt1, 29));
            model->setCoin(sqlite3_column_int(ppStmt1, 30));
            model->setStartBossMessage((const char*)sqlite3_column_text(ppStmt1, 31));
            model->setClearBossMessage((const char*)sqlite3_column_text(ppStmt1, 32));
            model->setStatus(sqlite3_column_int(ppStmt1, 33));
            model->setCreated((const char*)sqlite3_column_text(ppStmt1, 34));
            model->setModified((const char*)sqlite3_column_text(ppStmt1, 35));
            model->setBgmId(sqlite3_column_int(ppStmt1, 36));
            model->setPrevId(sqlite3_column_int(ppStmt1, 37));
            model->setNextId(sqlite3_column_int(ppStmt1, 38));
            model->setOperandId(sqlite3_column_int(ppStmt1, 39));
            model->setEnemyPlusDice1(sqlite3_column_int(ppStmt1, 40));
            model->setEnemyPlusDice2(sqlite3_column_int(ppStmt1, 41));
            model->setEnemyPlusDice3(sqlite3_column_int(ppStmt1, 42));
            model->setEnemyDiceCorrection(sqlite3_column_int(ppStmt1, 43));
            model->setEnemyDiceProbability(sqlite3_column_int(ppStmt1, 44));
            model->setRankingRank1Constant(sqlite3_column_int(ppStmt1, 45));
            model->setRankingRank2Constant(sqlite3_column_int(ppStmt1, 46));
            model->setRankingRank3Constant(sqlite3_column_int(ppStmt1, 47));
            model->setRankingBaseScore(sqlite3_column_int(ppStmt1, 48));
            model->setRankingScoreConstant(sqlite3_column_int(ppStmt1, 49));
            model->setRankingBaseTurn(sqlite3_column_int(ppStmt1, 50));
            model->setRankingTurnConstant(sqlite3_column_int(ppStmt1, 51));
            model->setRankingItemPoint(sqlite3_column_int(ppStmt1, 52));
            model->setRankingMeritConstant(sqlite3_column_int(ppStmt1, 53));
            model->setRankingSkillChargeConstant(sqlite3_column_int(ppStmt1, 54));
            sqlite3_finalize(ppStmt1);
            return model;
        }
        sqlite3_finalize(ppStmt1);
    }
    return nullptr;
}

void CourseModel::insert()
{
    Model::insert();
    std::string sqlstr = "INSERT INTO " + tableName + " VALUES('" + stringInt(ID)->_string + "','" +
                         stringInt(map_id)->_string + "','" +
                         stringInt(area_id)->_string + "','" +
                         stringInt(position_x)->_string + "','" +
                         stringInt(position_y)->_string + "','" +
                         stringInt(no)->_string + "','" +
                         name + "','" +
                         description + "','" +
                         stringInt(course_type)->_string + "','" +
                         stringInt(participation_rank)->_string + "','" +
                         event_start_datetime + "','" +
                         event_end_datetime + "','" +
                         stringInt(base_score)->_string + "','" +
                         stringInt(race_type)->_string + "','" +
                         stringInt(target_score)->_string + "','" +
                         stringInt(max_number_of_turns)->_string + "','" +
                         stringInt(image_id)->_string + "','" +
                         stringInt(required_fuel)->_string + "','" +
                         stringInt(boss_character_id)->_string + "','" +
                         stringInt(boss_character_level)->_string + "','" +
                         stringInt(sub1_character_id)->_string + "','" +
                         stringInt(sub1_character_level)->_string + "','" +
                         stringInt(sub2_character_id)->_string + "','" +
                         stringInt(sub2_character_level)->_string + "','" +
                         stringInt(enemy_character_level)->_string + "','" +
                         stringFloat(enemy_status_rate)->_string + "','" +
                         stringInt(dear_value_rank1)->_string + "','" +
                         stringInt(dear_value_rank2)->_string + "','" +
                         stringInt(dear_value_rank3)->_string + "','" +
                         stringFloat(leader_dear_value_rate)->_string + "','" +
                         stringInt(coin)->_string + "','" +
                         start_boss_message + "','" +
                         clear_boss_message + "','" +
                         stringInt(status)->_string + "','" +
                         created + "','" +
                         modified + "','" +
                         stringInt(bgm_id)->_string + "','" +
                         stringInt(prev_id)->_string + "','" +
                         stringInt(next_id)->_string + "','" +
                         stringInt(operand_id)->_string +  "','" +
                         stringInt(enemy_plus_dice1)->_string + "','" +
                         stringInt(enemy_plus_dice2)->_string + "','" +
                         stringInt(enemy_plus_dice3)->_string + "','" +
                         stringInt(enemy_dice_correction)->_string + "','" +
                         stringInt(enemy_dice_probability)->_string + "','" +
                         stringInt(ranking_rank1_constant)->_string + "','" +
                         stringInt(ranking_rank2_constant)->_string + "','" +
                         stringInt(ranking_rank3_constant)->_string + "','" +
                         stringInt(ranking_base_score)->_string + "','" +
                         stringInt(ranking_score_constant)->_string + "','" +
                         stringInt(ranking_base_turn)->_string + "','" +
                         stringInt(ranking_turn_constant)->_string + "','" +
                         stringInt(ranking_item_point)->_string + "','" +
                         stringInt(ranking_merit_constant)->_string + "','" +
                         stringInt(ranking_skill_charge_constant)->_string + "')";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Insert data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}

void CourseModel::update()
{
    Model::update();
    std::string sqlstr = "UPDATE " + tableName + " SET map_id = '" + stringInt(map_id)->_string +
                         "', area_id = '" + stringInt(area_id)->_string +
                         "', position_x = '" + stringInt(position_x)->_string +
                         "', position_y = '" + stringInt(position_y)->_string +
                         "', no = '" + stringInt(no)->_string +
                         "', name = '" + name +
                         "', description = '" + description +
                         "', course_type = '" + stringInt(course_type)->_string +
                         "', participation_rank = '" + stringInt(participation_rank)->_string +
                         "', event_start_datetime = '" + event_start_datetime +
                         "', event_end_datetime = '" + event_end_datetime +
                         "', base_score = '" + stringInt(base_score)->_string +
                         "', race_type = '" + stringInt(race_type)->_string +
                         "', target_score = '" + stringInt(target_score)->_string +
                         "', max_number_of_turns = '" + stringInt(max_number_of_turns)->_string +
                         "', image_id = '" + stringInt(image_id)->_string +
                         "', required_fuel = '" + stringInt(required_fuel)->_string +
                         "', boss_character_id = '" + stringInt(boss_character_id)->_string +
                         "', boss_character_level = '" + stringInt(boss_character_level)->_string +
                         "', sub1_character_id = '" + stringInt(sub1_character_id)->_string +
                         "', sub1_character_level = '" + stringInt(sub1_character_level)->_string +
                         "', sub2_character_id = '" + stringInt(sub2_character_id)->_string +
                         "', sub2_character_level = '" + stringInt(sub2_character_level)->_string +
                         "', enemy_character_level = '" + stringInt(enemy_character_level)->_string +
                         "', enemy_status_rate = '" + stringFloat(enemy_status_rate)->_string +
                         "', dear_value_rank1 = '" + stringInt(dear_value_rank1)->_string +
                         "', dear_value_rank2 = '" + stringInt(dear_value_rank2)->_string +
                         "', dear_value_rank3 = '" + stringInt(dear_value_rank3)->_string +
                         "', leader_dear_value_rate = '" + stringFloat(leader_dear_value_rate)->_string +
                         "', coin = '" + stringInt(coin)->_string +
                         "', start_boss_message = '" + start_boss_message +
                         "', clear_boss_message = '" + clear_boss_message +
                         "', status = '" + stringInt(status)->_string +
                         "', created = '" + created +
                         "', modified = '" + modified +
                         "', bgm_id = '" + stringInt(bgm_id)->_string +
                         "', prev_id = '" + stringInt(prev_id)->_string +
                         "', next_id = '" + stringInt(next_id)->_string +
                         "', operand_id = '" + stringInt(operand_id)->_string +
                         "', enemy_plus_dice1 = '" + stringInt(enemy_plus_dice1)->_string +
                         "', enemy_plus_dice2 = '" + stringInt(enemy_plus_dice2)->_string +
                         "', enemy_plus_dice3 = '" + stringInt(enemy_plus_dice3)->_string +
                         "', enemy_dice_correction = '" + stringInt(enemy_dice_correction)->_string +
                         "', enemy_dice_probability = '" + stringInt(enemy_dice_probability)->_string +
                         "', ranking_rank1_constant = '" + stringInt(ranking_rank1_constant)->_string +
                         "', ranking_rank2_constant = '" + stringInt(ranking_rank2_constant)->_string +
                         "', ranking_rank3_constant = '" + stringInt(ranking_rank3_constant)->_string +
                         "', ranking_base_score = '" + stringInt(ranking_base_score)->_string +
                         "', ranking_score_constant = '" + stringInt(ranking_score_constant)->_string +
                         "', ranking_base_turn = '" + stringInt(ranking_base_turn)->_string +
                         "', ranking_turn_constant = '" + stringInt(ranking_turn_constant)->_string +
                         "', ranking_item_point = '" + stringInt(ranking_item_point)->_string +
                         "', ranking_merit_constant = '" + stringInt(ranking_merit_constant)->_string +
                         "', ranking_skill_charge_constant = '" + stringInt(ranking_skill_charge_constant)->_string +
                         "' WHERE id = '" + stringInt(ID)->_string + "'";

    ////CCLOG("sql: %s", sqlstr.c_str());

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlstr.c_str(), NULL, NULL, &errmsg);
    if (result != SQLITE_OK) {
        ////CCLOG("Update data failed!");
        ////CCLOG("err: %d, mess %s", result, errmsg);
    }
}



void CourseModel::hulkInsert(Json* mDatas)
{
    bool isdelete = Model::deleteTableData(tableName);
    if (isdelete) {
        dropTable(tableName);
        createCourseTable();

        char buffer[] = "INSERT INTO mst_courses VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8 ,?9 ,?10 ,?11 ,?12 ,?13 ,?14 ,?15 ,?16 ,?17 ,?18 ,?19 ,?20 ,?21 ,?22 ,?23 ,?24 ,?25 ,?26 ,?27 ,?28 ,?29 ,?30 ,?31 ,?32 ,?33 ,?34 ,?35 ,?36, ?37, ?38, ?39, ?40, ?41, ?42, ?43, ?44, ?45, ?46, ?47, ?48, ?49, ?50, ?51, ?52, ?53, ?54, ?55)";
        //        std::string command = "INSERT INTO " + tableName + " VALUES (?1, ?2, ?3, ?4, ?5 ,?6 ,?7 ,?8 ,?9 ,?10 ,?11 ,?12 ,?13 ,?14 ,?15 ,?16 ,?17 ,?18 ,?19 ,?20 ,?21 ,?22 ,?23 ,?24 ,?25 ,?26 ,?27 ,?28 ,?29 ,?30 ,?31 ,?32 ,?33 ,?34 ,?35 ,?36)";
        //        strcpy(buffer, command.c_str());
        sqlite3* pDB = DB_MANAGER->getDB();
        sqlite3_stmt* stmt;

        sqlite3_exec(pDB, "BEGIN TRANSACTION;", NULL, NULL, NULL);
        sqlite3_prepare_v2(pDB, buffer, (int)std::strlen(buffer), &stmt, NULL);

        Json* child;
        for (child = mDatas->child; child; child = child->next) {       // data binlding
            int ID = atoi(Json_getString(child, "id", "-1"));
            int map_id = atoi(Json_getString(child, "map_id", "-1"));
            int area_id = atoi(Json_getString(child, "area_id", "-1"));
            int position_x = atoi(Json_getString(child, "position_x", "-1"));
            int position_y = atoi(Json_getString(child, "position_y", "-1"));
            int no = atoi(Json_getString(child, "no", "-1"));
            std::string name = Json_getString(child, "name", "");
            std::string description = Json_getString(child, "description", "");
            int course_type = atoi(Json_getString(child, "course_type", "-1"));
            int participation_rank = atoi(Json_getString(child, "participation_rank", "-1"));
            std::string event_start_datetime =  Json_getString(child, "event_start_datetime", "");
            std::string event_end_datetime = Json_getString(child, "event_end_datetime", "");
            int base_score = atoi(Json_getString(child, "base_score", "-1"));
            int race_type = atoi(Json_getString(child, "race_type", "-1"));
            int target_score = atoi(Json_getString(child, "target_score", "-1"));
            int max_number_of_turns = atoi(Json_getString(child, "max_number_of_turns", "-1"));
            int image_id = atoi(Json_getString(child, "image_id", "-1"));
            int bgm_id = atoi(Json_getString(child, "bgm_id", "-1"));
            int required_fuel = atoi(Json_getString(child, "required_fuel", "-1"));
            int boss_character_id = atoi(Json_getString(child, "boss_character_id", "-1"));
            int boss_character_level = atoi(Json_getString(child, "boss_character_level", "-1"));
            int sub1_character_id = atoi(Json_getString(child, "sub1_character_id", "-1"));
            int sub1_character_level = atoi(Json_getString(child, "sub1_character_level", "-1"));
            int sub2_character_id = atoi(Json_getString(child, "sub2_character_id", "-1"));
            int sub2_character_level = atoi(Json_getString(child, "sub2_character_level", "-1"));
            int enemy_character_level = atoi(Json_getString(child, "enemy_character_level", "-1"));
            double enemy_status_rate = atof(Json_getString(child, "enemy_status_rate", "-1"));
            int dear_value_rank1 = atoi(Json_getString(child, "dear_value_rank1", "-1"));
            int dear_value_rank2 = atoi(Json_getString(child, "dear_value_rank2", "-1"));
            int dear_value_rank3 = atoi(Json_getString(child, "dear_value_rank3", "-1"));
            double leader_dear_value_rate = atof(Json_getString(child, "leader_dear_value_rate", "-1"));
            int coin = atoi(Json_getString(child, "coin", "-1"));
            std::string start_boss_message = Json_getString(child, "start_boss_message", "");
            std::string clear_boss_message = Json_getString(child, "clear_boss_message", "");
            int status = atoi(Json_getString(child, "status", "-1"));
            std::string created = Json_getString(child, "created", "");
            std::string modified =  Json_getString(child, "modified", "");
            int prevId = atoi(Json_getString(child, "prev_id", "-1"));
            int nextId = atoi(Json_getString(child, "next_id", "-1"));
            int operandId = atoi(Json_getString(child, "operand_id", "-1"));
            int enemyDicePulus1 = atoi(Json_getString(child,  "enemy_plus_dice1", "-1"));
            int enemyDicePulus2 = atoi(Json_getString(child,  "enemy_plus_dice2", "-1"));
            int enemyDicePulus3 = atoi(Json_getString(child,  "enemy_plus_dice3", "-1"));
            int enemyDiceCorrection = atoi(Json_getString(child,  "enemy_dice_correction", "-1"));
            int enemyDiceProbability = atoi(Json_getString(child,  "enemy_dice_probability", "-1"));
            int ranking_rank1_constant = atoi(Json_getString(child, "ranking_rank1_constant", "0"));
            int ranking_rank2_constant = atoi(Json_getString(child, "ranking_rank2_constant", "0"));
            int ranking_rank3_constant = atoi(Json_getString(child, "ranking_rank3_constant", "0"));
            int ranking_base_score = atoi(Json_getString(child, "ranking_base_score", "0"));
            int ranking_score_constant = atoi(Json_getString(child, "ranking_score_constant", "0"));
            int ranking_base_turn = atoi(Json_getString(child, "ranking_base_turn", "0"));
            int ranking_turn_constant = atoi(Json_getString(child, "ranking_turn_constant", "0"));
            int ranking_item_point = atoi(Json_getString(child, "ranking_item_point", "0"));
            int ranking_merit_constant = atoi(Json_getString(child, "ranking_merit_constant", "0"));
            int ranking_skill_charge_constant = atoi(Json_getString(child, "ranking_skill_charge_constant", "0"));


            sqlite3_bind_int(stmt, 1, ID);
            sqlite3_bind_int(stmt, 2, map_id);
            sqlite3_bind_int(stmt, 3, area_id);
            sqlite3_bind_int(stmt, 4, position_x);
            sqlite3_bind_int(stmt, 5, position_y);
            sqlite3_bind_int(stmt, 6, no);
            sqlite3_bind_text(stmt, 7, name.c_str(), (int)name.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 8, description.c_str(), (int)description.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 9, course_type);
            sqlite3_bind_int(stmt, 10, participation_rank);
            sqlite3_bind_text(stmt, 11, event_start_datetime.c_str(), (int)event_start_datetime.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 12, event_end_datetime.c_str(), (int)event_end_datetime.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 13, base_score);
            sqlite3_bind_int(stmt, 14, race_type);
            sqlite3_bind_int(stmt, 15, target_score);
            sqlite3_bind_int(stmt, 16, max_number_of_turns);
            sqlite3_bind_int(stmt, 17, image_id);
            sqlite3_bind_int(stmt, 18, required_fuel);
            sqlite3_bind_int(stmt, 19, boss_character_id);
            sqlite3_bind_int(stmt, 20, boss_character_level);
            sqlite3_bind_int(stmt, 21, sub1_character_id);
            sqlite3_bind_int(stmt, 22, sub1_character_level);
            sqlite3_bind_int(stmt, 23, sub2_character_id);
            sqlite3_bind_int(stmt, 24, sub2_character_level);
            sqlite3_bind_int(stmt, 25, enemy_character_level);
            sqlite3_bind_double(stmt, 26, enemy_status_rate);
            sqlite3_bind_int(stmt, 27, dear_value_rank1);
            sqlite3_bind_int(stmt, 28, dear_value_rank2);
            sqlite3_bind_int(stmt, 29, dear_value_rank3);
            sqlite3_bind_double(stmt, 30, leader_dear_value_rate);
            sqlite3_bind_int(stmt, 31, coin);
            sqlite3_bind_text(stmt, 32, start_boss_message.c_str(), (int)start_boss_message.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 33, clear_boss_message.c_str(), (int)clear_boss_message.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 34, status);
            sqlite3_bind_text(stmt, 35, created.c_str(), (int)created.size(), SQLITE_STATIC);
            sqlite3_bind_text(stmt, 36, modified.c_str(), (int)modified.size(), SQLITE_STATIC);
            sqlite3_bind_int(stmt, 37, bgm_id);
            sqlite3_bind_int(stmt, 38, prevId);
            sqlite3_bind_int(stmt, 39, nextId);
            sqlite3_bind_int(stmt, 40, operandId);
            sqlite3_bind_int(stmt, 41, enemyDicePulus1);
            sqlite3_bind_int(stmt, 42, enemyDicePulus2);
            sqlite3_bind_int(stmt, 43, enemyDicePulus3);
            sqlite3_bind_int(stmt, 44, enemyDiceCorrection);
            sqlite3_bind_int(stmt, 45, enemyDiceProbability);
            sqlite3_bind_int(stmt, 46, ranking_rank1_constant);
            sqlite3_bind_int(stmt, 47, ranking_rank2_constant);
            sqlite3_bind_int(stmt, 48, ranking_rank3_constant);
            sqlite3_bind_int(stmt, 49, ranking_base_score);
            sqlite3_bind_int(stmt, 50, ranking_score_constant);
            sqlite3_bind_int(stmt, 51, ranking_base_turn);
            sqlite3_bind_int(stmt, 52, ranking_turn_constant);
            sqlite3_bind_int(stmt, 53, ranking_item_point);
            sqlite3_bind_int(stmt, 54, ranking_merit_constant);
            sqlite3_bind_int(stmt, 55, ranking_skill_charge_constant);

            if (sqlite3_step(stmt) != SQLITE_DONE) {
                ////CCLOG("%s commit failed!", tableName.c_str());
            }
            sqlite3_reset(stmt);
        }

        sqlite3_finalize(stmt);
        sqlite3_exec(pDB, "END TRANSACTION;", NULL, NULL, NULL);
    }
}

bool CourseModel::createCourseTable()
{
    std::string sqlCreateCourse = "CREATE TABLE mst_courses (id int(11) NOT NULL,\
    map_id	int(11) NOT NULL,\
    area_id	int(11) NOT NULL,\
    position_x	int(11) DEFAULT '0',\
    position_y	int(11) DEFAULT '0',\
    no	int(11) NOT NULL DEFAULT '0',\
    name	varchar(45) DEFAULT NULL,\
    description	varchar(1024) DEFAULT NULL,\
    course_type	tinyint(3) NOT NULL DEFAULT '1',\
    participation_rank int(11) DEFAULT '0',\
    event_start_datetime	datetime DEFAULT NULL,\
    event_end_datetime datetime DEFAULT NULL,\
    base_score	int(11) NOT NULL DEFAULT '0',\
    race_type	tinyint(3) DEFAULT NULL,\
    target_score	int(11) DEFAULT NULL,\
    max_number_of_turns int(11) DEFAULT NULL,\
    image_id	int(11) DEFAULT NULL,\
    required_fuel	int(11) DEFAULT NULL,\
    boss_character_id int(11) DEFAULT NULL,\
    boss_character_level int(11) DEFAULT NULL,\
    sub1_character_id	int(11) DEFAULT NULL,\
    sub1_character_level int(11) DEFAULT NULL,\
    sub2_character_id	int(11) DEFAULT NULL,\
    sub2_character_level int(11) DEFAULT NULL,\
    enemy_character_level	int(11) DEFAULT NULL,\
    enemy_status_rate	float DEFAULT '1',\
    dear_value_rank1 int(11) DEFAULT '0',\
    dear_value_rank2 int(11) DEFAULT '0',\
    dear_value_rank3 int(11) DEFAULT '0',\
    leader_dear_value_rate float DEFAULT '1',\
    coin	int(11) NOT NULL DEFAULT '0',\
    start_boss_message	text,\
    clear_boss_message	text,\
    status	tinyint(3) DEFAULT NULL,\
    created	timestamp DEFAULT NULL,\
    modified	timestamp NOT NULL,\
    bgm_id	int(11) DEFAULT NULL,\
    prev_id	int(11) DEFAULT NULL,\
    next_id	int(11) DEFAULT NULL,\
    operand_id int(11) DEFAULT NULL,\
    enemy_plus_dice1 int(11) DEFAULT '0',\
    enemy_plus_dice2 int(11) DEFAULT '0',\
    enemy_plus_dice3 int(11) DEFAULT '0',\
    enemy_dice_correction int(11) DEFAULT '0',\
    enemy_dice_probability int(11) DEFAULT '0',\
    ranking_rank1_constant int(11) DEFAULT '0',\
    ranking_rank2_constant int(11) DEFAULT '0',\
    ranking_rank3_constant int(11) DEFAULT '0',\
    ranking_base_score int(11) DEFAULT '0',\
    ranking_score_constant int(11) DEFAULT '0',\
    ranking_base_turn int(11) DEFAULT '0',\
    ranking_turn_constant int(11) DEFAULT '0',\
    ranking_item_point int(11) DEFAULT '0',\
    ranking_merit_constant int(11) DEFAULT '0',\
    ranking_skill_charge_constant int(11) DEFAULT '0',\
    PRIMARY KEY(id) \
    )";

    sqlite3* pDB = DB_MANAGER->getDB();
    char* errmsg;
    int result = sqlite3_exec(pDB, sqlCreateCourse.c_str(), NULL, NULL, &errmsg);

    if (result != SQLITE_OK) {
        CCLOG("Drop table failed!");
        return false;
    }
    CCLOG("Drop table susscessfully!");

    return true;
}

const int CourseModel::getCourseBgmIdCount()
{
    std::string sqlstr = "SELECT DISTINCT bgm_id FROM ";
    sqlstr += TABLE_COURSES;
    sqlstr += " order by bgm_id desc limit 1";

    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}

const int CourseModel::getCourseImageIdCount()
{
    std::string sqlstr = "SELECT DISTINCT image_id FROM ";
    sqlstr += TABLE_COURSES;
    sqlstr += " order by image_id desc limit 1";

    sqlite3_stmt* ppStmt1;
    sqlite3* pDB = DB_MANAGER->getDB();
    int status = sqlite3_prepare_v2(pDB, sqlstr.c_str(), -1, &ppStmt1, NULL);
    if (status == SQLITE_OK) {
        while (sqlite3_step(ppStmt1) == SQLITE_ROW) {
            int result = sqlite3_column_int(ppStmt1, 0);
            sqlite3_finalize(ppStmt1);
            return result;
        }
        sqlite3_finalize(ppStmt1);
    }
    return 0;
}
