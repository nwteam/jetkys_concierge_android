#ifndef  _APP_DELEGATE_H_
#define  _APP_DELEGATE_H_

#include "cocos2d.h"
#include "StoreHandler.h"

USING_NS_CC;

class AppDelegate: private Application
{
public:
	AppDelegate();
	virtual ~AppDelegate();

	virtual void initGLContextAttrs();

	virtual bool applicationDidFinishLaunching();
	virtual void applicationDidEnterBackground();
	virtual void applicationWillEnterForeground();

private:
	void addSearchPathForAndroid();

	void initDataBase();

	StoreHandler *m_StoreHandler;
};

#endif // _APP_DELEGATE_H_
