#include "AppDelegate.h"
#include "DBManager.h"
#include "DataPlayer.h"
#include "LocalNotification.h"
#include "PlayerController.h"
#include "UserDefaultManager.h"
#include "SplashScene.h"
#include "DevelopScene.h"
#include "SyanagoDebugger.h"

#include "UserDefaultManager.h"
#include "jCommon.h"
#include "Native.h"
#include "API.h"
#include "CourseModel.h"
#include "CharacterModel.h"
#include "SystemSettingModel.h"
#include "GridOperandsModel.h"


#include "CharacterResourceViewer.h"

USING_NS_CC;

AppDelegate::AppDelegate():
    m_StoreHandler(nullptr) {}

AppDelegate::~AppDelegate() {}

void AppDelegate::initGLContextAttrs()
{
    GLContextAttrs glContextAttrs = { 8, 8, 8, 8, 24, 8 };
    GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching()
{
    if (m_StoreHandler == nullptr) {
        m_StoreHandler = new StoreHandler();
    }

    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if (!glview) {
        director->setOpenGLView(GLViewImpl::create("My Game"));
    }
    // fixed a bug on ipad that the Sprite which has the same position as others but has the different z-order to others cannot be visible.
    director->setDepthTest(false);

    Size designResolution = Size(640, 960);

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        if (isWideScreen()) {
            designResolution.height = 1136;
        }
    #endif
    glview->setDesignResolutionSize(designResolution.width, designResolution.height, ResolutionPolicy::SHOW_ALL);
    Director::getInstance()->setContentScaleFactor(1.0f);

    // Search path of sqlite file
    FileUtils* fileUtils = FileUtils::getInstance();
    fileUtils->addSearchPath("db");
    fileUtils->addSearchPath((fileUtils->getWritablePath() + "Resources").c_str());

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        addSearchPathForAndroid();
    #endif

    director->setDisplayStats(false);
    director->setAnimationInterval(1.0 / 60);

    DB_MANAGER->openDB();

    initDataBase();

    // run
    if (getenv("TESTING") != NULL) {
        UserDefault::getInstance()->setStringForKey(UserDefaultManager::KEY::DOMAIN_URL, SyanagoAPI::API::DOMAINS::DEVELOPMENT);
        return true;
    }
    Scene* scene;
    #if !defined(COCOS2D_DEBUG) || COCOS2D_DEBUG == 0
        UserDefault::getInstance()->setStringForKey(UserDefaultManager::KEY::DOMAIN_URL, SyanagoAPI::API::DOMAINS::PRODUCTION);
        scene = SplashScene::createScene();
    #elif COCOS2D_DEBUG > 0
        // UserDefault::getInstance()->setStringForKey(UserDefaultManager::KEY::DOMAIN_URL, SyanagoAPI::API::DOMAINS::DEVELOPMENT);
        UserDefault::getInstance()->setStringForKey(UserDefaultManager::KEY::DOMAIN_URL, SyanagoAPI::API::DOMAINS::TESTING);
        DevelopScene::createScene();
        scene = SyanagoDebugger::createScene();
    #endif
    director->runWithScene(scene);
    return true;
}

void AppDelegate::applicationDidEnterBackground()
{
    if (m_StoreHandler != nullptr) {
        m_StoreHandler->applicationDidEnterBackground();
    }
    Director::getInstance()->stopAnimation();
    UserDefaultManager::setTimeOnEnteringBackground();
}

void AppDelegate::applicationWillEnterForeground()
{
    if (m_StoreHandler != nullptr) {
        m_StoreHandler->applicationWillEnterForeground();
    }
    Director::getInstance()->startAnimation();
    UserDefaultManager::setElapsedTime();
}

void AppDelegate::addSearchPathForAndroid()
{
    FileUtils* fileUtils = FileUtils::getInstance();
    fileUtils->addSearchPath("tips/Fire.plist");
    fileUtils->addSearchPath("tips/HelloParticle.plist");
    fileUtils->addSearchPath("tips/loadbar_bg.png");
    fileUtils->addSearchPath("tips/loadbar_on.png");
    fileUtils->addSearchPath("tips/tips_bg.png");
    fileUtils->addSearchPath("tips/tips_frame.png");
    fileUtils->addSearchPath("tips/tips.json");
    fileUtils->addSearchPath("UIdesign/common/button");
    fileUtils->addSearchPath("UIdesign/common/button/tweet_button.png");
    fileUtils->addSearchPath("UIdesign/common/button/get_fuel_tweet_button.png");
    fileUtils->addSearchPath("UIdesign/common/button/get_fuel_tweet_button_off.png");
    fileUtils->addSearchPath("UIdesign/common/dialog");
    fileUtils->addSearchPath("UIdesign/common/Items");
    fileUtils->addSearchPath("UIdesign/common/view");
    fileUtils->addSearchPath("UIdesign/common/Fairy");
    fileUtils->addSearchPath("UIdesign/mainScene");
    fileUtils->addSearchPath("UIdesign/mainScene/treicon");// ダミー
    fileUtils->addSearchPath("UIdesign/mainScene/lock_icon.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/itembox_left_arrow.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/itembox_list_base.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/itembox_right_arrow.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/itembox_ticket_coin.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/itembox_ticket_dearvalue.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/itembox_ticket_exp.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/itembox_ticket_fuel.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/itembox_ticket_gacha.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/itembox_ticket_token.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/itembox_title_base.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/sdAnimation");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/sdAnimation/itembox_button.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/sdAnimation/itembox_comment_1.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/sdAnimation/itembox_comment_2.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/sdAnimation/itembox_comment_3.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/sdAnimation/itembox_comment_4.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/sdAnimation/itembox_sd_1.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/sdAnimation/itembox_sd_2.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/sdAnimation/itembox_sd_3.png");
    fileUtils->addSearchPath("UIdesign/mainScene/itemBox/sdAnimation/itembox_sd_4.png");
    fileUtils->addSearchPath("UIdesign/mainScene/detale");
    fileUtils->addSearchPath("UIdesign/mainScene/footer");
    fileUtils->addSearchPath("UIdesign/mainScene/friend");
    fileUtils->addSearchPath("UIdesign/mainScene/gacha");
    fileUtils->addSearchPath("UIdesign/mainScene/gacha/gacha_animation");
    fileUtils->addSearchPath("UIdesign/mainScene/gacha/gacha_friend_button.png");
    fileUtils->addSearchPath("UIdesign/mainScene/garage");
    fileUtils->addSearchPath("UIdesign/mainScene/garage/composition");
    fileUtils->addSearchPath("UIdesign/mainScene/garage/garage_list");
    fileUtils->addSearchPath("UIdesign/mainScene/garage/garage_part_icon");
    fileUtils->addSearchPath("UIdesign/mainScene/garage/garage_popup");
    fileUtils->addSearchPath("UIdesign/mainScene/garage/garage_popup_sort");
    fileUtils->addSearchPath("UIdesign/mainScene/garage/garage_sell");
    fileUtils->addSearchPath("UIdesign/mainScene/garage/customize");
    fileUtils->addSearchPath("UIdesign/mainScene/garage/customize/popup");
    fileUtils->addSearchPath("UIdesign/mainScene/garage/awake_character");
    fileUtils->addSearchPath("UIdesign/mainScene/header");
    fileUtils->addSearchPath("UIdesign/mainScene/other/colection");
    fileUtils->addSearchPath("UIdesign/mainScene/other/invite");
    fileUtils->addSearchPath("UIdesign/mainScene/other/option");
    fileUtils->addSearchPath("UIdesign/mainScene/other/treasures");
    fileUtils->addSearchPath("UIdesign/mainScene/shop");
    fileUtils->addSearchPath("UIdesign/mainScene/sidemenu");
    fileUtils->addSearchPath("UIdesign/mainScene/sort");
    fileUtils->addSearchPath("UIdesign/mainScene/start");
    fileUtils->addSearchPath("UIdesign/mainScene/start/decisionRace");
    fileUtils->addSearchPath("UIdesign/mainScene/team_edit");
    fileUtils->addSearchPath("UIdesign/mainScene/team_edit/te_popup");
    fileUtils->addSearchPath("UIdesign/mainScene/ranking");
    fileUtils->addSearchPath("UIdesign/mainScene/mission");
    fileUtils->addSearchPath("UIdesign/titleScene");
    fileUtils->addSearchPath("UIdesign/titleScene/mainCarSelect");
    fileUtils->addSearchPath("UIdesign/raceScene/scene/SelectFriend");
    fileUtils->addSearchPath("UIdesign/raceScene/scene/Race");
    fileUtils->addSearchPath("UIdesign/raceScene/scene/Race/MiniMap");
    fileUtils->addSearchPath("UIdesign/raceScene/scene/Race/confetti");
    fileUtils->addSearchPath("UIdesign/raceScene/scene/Race/Roulette");
    fileUtils->addSearchPath("UIdesign/raceScene/scene/Race/background");
    fileUtils->addSearchPath("UIdesign/raceScene/scene/Race/rank_syanago");
    fileUtils->addSearchPath("UIdesign/raceScene/scene/Race/cell");
    fileUtils->addSearchPath("UIdesign/raceScene/scene/Item");
    fileUtils->addSearchPath("tips");
    fileUtils->addSearchPath("fonts");
    fileUtils->addSearchPath("sound");
    fileUtils->addSearchPath("Tutorial");
    fileUtils->addSearchPath("develop");
    fileUtils->addSearchPath("dummy");
}

void AppDelegate::initDataBase()
{
    bool databaseUpdated = false;
    if (Version::isNotNeedUpdate(UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DATA_BASE_VERSION), "1.5.9") == false) {
        GridOperandsModel::createGridOperandsTable();
        databaseUpdated = true;
    }
    if (Version::isNotNeedUpdate(UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DATA_BASE_VERSION), "1.8.6") == false) {
        CourseModel::dropTable(TABLE_COURSES);
        CourseModel::createCourseTable();
        SystemSettingModel::dropTable(TABLE_SYSTEM_SETTINGS);
        SystemSettingModel::createSystemSettingTable();
        databaseUpdated = true;
    }
    if (Version::isNotNeedUpdate(UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DATA_BASE_VERSION), "1.9.2") == false) {
        CharacterModel::dropTable(TABLE_CHARACTERS);
        CharacterModel::createCharacterTable();
        databaseUpdated = true;
    }

    if (databaseUpdated == true) {
        UserDefault::getInstance()->setStringForKey(UserDefaultManager::KEY::DATA_BASE_VERSION, Native::getApplicationVersion());
    }
}
