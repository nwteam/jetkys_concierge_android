#ifndef __syanago__TreasureItemEffectModel__
#define __syanago__TreasureItemEffectModel__

namespace ITEM{
    namespace EFFECT_TYPE{
        static const int MAX_FUEL_UP = 60;
        static const int MAX_FRIENDS_UP = 50;
    }
}

class TreasureItemEffectModel
{
public:
    static int getTreasureItemIncrementalDifferenceEffect(const int itemEffectId);
    
private:
    static int convertStarCount(const int itemKind);
};

#endif /* defined(__syanago__TreasureItemEffectModel__) */
