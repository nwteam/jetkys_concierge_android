#include "Native.h"
#include "UserDefaultManager.h"
#include "API.h"
#include "PlayerController.h"

namespace Native
{
std::string getOsDIDInKeychain()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        return getDIDInKeychain();
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        return "";
    #endif
}

void saveOsDIDInKeychain(std::string did)
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        saveDIDInKeychain(did);
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    #endif
}

void delOsDIDInKeychain()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        delDIDInKeychain();
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    #endif
}

std::string getOSUUID()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        return getUUID();
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        return android_getUUID();
    #endif
}

std::string getOSVersion()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        return getIOsVersion();
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        return android_getOSVersion();
    #endif
}

std::string getApplicationVersion()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        return getAppVersion();
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        return android_getAppVersion();
    #endif
}


std::string getIV()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        return "";
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        return android_getIV();
    #endif
}

std::string encrypt(const char* targetString, const char* hexIV)
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        return "";
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        return android_encrypt(targetString, hexIV);
    #endif
}

std::string decrypt(std::string targetString, std::string hexIV)
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        return "";
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        return android_decrypt(targetString.c_str(), hexIV.c_str());
    #endif
}

std::string sha256(std::string targetString)
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        return "";
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        return android_sha256(targetString.c_str());
    #endif
}

std::string getPushToken()
{
    std::string result;
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        result = ios_getPushToken();
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        result = android_getPushToken();
    #endif
    if (result == "") {
        result = UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DEVICE_PUSH_TOKEN);
    } else {
        UserDefault::getInstance()->setStringForKey(UserDefaultManager::KEY::DEVICE_PUSH_TOKEN, result);
    }
    CCLOG("[Native::getPushToken] %s", result.c_str());
    return result;
}

void playCharacterVoice(const char* voice)
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        return;
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        iaonPlayCharacterVoice(voice);
        return;
    #endif
}
}
