#ifndef Syanago_MacroDefines_h
#define Syanago_MacroDefines_h

#include "cocos2d.h"
#include <iomanip>
#include <locale>
#include <typeinfo>


#define stringInt64(number) String::createWithFormat("%lld", number)
#define stringInt(number) String::createWithFormat("%d", number)
#define stringFloat(number) String::createWithFormat("%f", number)

inline std::string nullIgrore(std::string str){
    return str == "null" ? "" : str;
};

inline std::string nullNumIgnore(std::string str) {
    return str == "null" ? "-1" : str;
}

inline int atoiNull(const char * str) {
    if (strcmp(str, "null") == 0 || strcmp(str, "") == 0) {
        return -1;
    } else {
        return atoi(str);
    }
}

inline float atofNull(const char * str) {
    if (strcmp(str, "null") == 0) {
        return -1.0f;
    } else {
        return atof(str);
    }
}

inline std::string itoaNull(int number) {
    if (number < 0) {
        return "";
    } else {
        return cocos2d::StringUtils::format("%d", number);
    }
}


inline std::string FormatWithCommas(int value)
{
    std::string result = cocos2d::stringInt(value)->_string;
    int insertPosition = result.length() - 3;
    while (insertPosition > 0) {
        result.insert(insertPosition, ",");
        insertPosition-=3;
    }
    return result;
}

#endif
