#include "UserDefaultManager.h"
#include "cocos2d.h"
#include "API.h"

USING_NS_CC;

const char* UserDefaultManager::KEY::GO_TO_TITLE_SCENE = "GoToTitleSceneFlag";
const char* UserDefaultManager::KEY::DEMAND_MODE = "demandMode";
const char* UserDefaultManager::KEY::FIRST_VOICE_SYSTEM = "fistVoiceSystem";
const char* UserDefaultManager::KEY::VOICE_OPTION_STATUS = "voiceOptionStatus";
const char* UserDefaultManager::KEY::DOMAIN_URL = "domein";
const char* UserDefaultManager::KEY::NEW_DEVICE = "NewDevice";
const char* UserDefaultManager::KEY::SE_OPTION_STATUS  = "seOptionStatus";
const char* UserDefaultManager::KEY::BGM_OPTION_STATUS = "bgmOptionStatus";
const char* UserDefaultManager::KEY::LEADER_CHARACTOR_MASTER_ID = "LeaderCharaMst";
const char* UserDefaultManager::KEY::VOICE_SOUND_ID = "VoiceSoundId";
const char* UserDefaultManager::KEY::OLD_RESOURCES_DELETE = "oldResourseDeleteFkag";
const char* UserDefaultManager::KEY::NON_RACE_SCENE_FLAG = "NonRaceSceneFlag";
const char* UserDefaultManager::KEY::NEW_PLAYER = "NewPlayer";
const char* UserDefaultManager::KEY::OPEN_GACHA = "OpenGacha";
const char* UserDefaultManager::KEY::END_FIRST_LOGIN = "EndFirstLogin";
const char* UserDefaultManager::KEY::NO_HOME_VOICE = "NoHomeVoice";
const char* UserDefaultManager::KEY::DEVICE_PUSH_TOKEN = "pushToken";
const char* UserDefaultManager::KEY::DATA_BASE_VERSION = "dataBaseVersion";
const char* UserDefaultManager::KEY::IS_EVENT_RACE = "isEventRace";
const char* UserDefaultManager::KEY::IS_RESET_MASTER_TABLE = "isResetMasterTable";
const char* UserDefaultManager::KEY::IS_RANK_UP = "isRankUp";
const char* UserDefaultManager::KEY::TIME_OF_RACE_START = "saveRaceTime";
const char* UserDefaultManager::KEY::TIME_OF_BACKGROUND_STAY = "backgroundTime";
const char* UserDefaultManager::KEY::TIME_OF_HEADER = "saveFeulTime";
const char* UserDefaultManager::KEY::TIME_OF_ENTERING_BACKGROUND = "saveTime";

void UserDefaultManager::cycleConnection()
{
    const std::string currentDomain = UserDefault::getInstance()->getStringForKey(KEY::DOMAIN_URL);
    std::string nextDomain = SyanagoAPI::API::DOMAINS::DEVELOPMENT;
    if (currentDomain == SyanagoAPI::API::DOMAINS::DEVELOPMENT) {
        nextDomain =  SyanagoAPI::API::DOMAINS::TESTING;
    } else if (currentDomain == SyanagoAPI::API::DOMAINS::TESTING) {
        nextDomain =  SyanagoAPI::API::DOMAINS::PRODUCTION;
    } else if (currentDomain == SyanagoAPI::API::DOMAINS::PRODUCTION) {
        nextDomain = SyanagoAPI::API::DOMAINS::DEVELOPMENT;
    }
    UserDefault::getInstance()->setStringForKey(KEY::DOMAIN_URL, nextDomain);
}

std::string UserDefaultManager::getCurrentDomainName()
{
    const std::string currentDomain = UserDefault::getInstance()->getStringForKey(KEY::DOMAIN_URL);
    std::string result = "";
    if (currentDomain == SyanagoAPI::API::DOMAINS::DEVELOPMENT) {
        result =  "開発";
    } else if (currentDomain == SyanagoAPI::API::DOMAINS::TESTING) {
        result = "検収";
    } else if (currentDomain == SyanagoAPI::API::DOMAINS::PRODUCTION) {
        result = "本番";
    }
    return result + " :" + currentDomain;
}


void UserDefaultManager::initSplashScene()
{
    UserDefault::getInstance()->setBoolForKey(KEY::GO_TO_TITLE_SCENE, false);
    UserDefault::getInstance()->setBoolForKey(KEY::DEMAND_MODE, false);

    if (!(UserDefault::getInstance()->getBoolForKey(KEY::FIRST_VOICE_SYSTEM, false))) {
        UserDefault::getInstance()->setBoolForKey(KEY::VOICE_OPTION_STATUS, true);
        UserDefault::getInstance()->setBoolForKey(KEY::FIRST_VOICE_SYSTEM, true);
    }

    if ("" == UserDefault::getInstance()->getStringForKey(KEY::DOMAIN_URL)) {
        UserDefault::getInstance()->setStringForKey(KEY::DOMAIN_URL, SyanagoAPI::API::DOMAINS::PRODUCTION);
    }

    if (!(UserDefault::getInstance())->getBoolForKey(KEY::NEW_DEVICE, false)) {
        UserDefault::getInstance()->setBoolForKey(KEY::SE_OPTION_STATUS, true);
        UserDefault::getInstance()->setBoolForKey(KEY::BGM_OPTION_STATUS, true);
        UserDefault::getInstance()->setBoolForKey(KEY::VOICE_OPTION_STATUS, true);
        UserDefault::getInstance()->setBoolForKey(KEY::NEW_DEVICE, true);
        UserDefault::getInstance()->setIntegerForKey(KEY::LEADER_CHARACTOR_MASTER_ID, -1);
    }
}

void UserDefaultManager::initTitleScene()
{
    UserDefault::getInstance()->setBoolForKey(KEY::GO_TO_TITLE_SCENE, false);
}

void UserDefaultManager::initMainScene()
{
    UserDefault::getInstance()->setBoolForKey(KEY::NON_RACE_SCENE_FLAG, false);
}

void UserDefaultManager::setTimeOnEnteringBackground()
{
    UserDefault::getInstance()->setIntegerForKey(KEY::TIME_OF_ENTERING_BACKGROUND, (int)time(NULL));
    UserDefault::getInstance()->flush();
}

void UserDefaultManager::setElapsedTime()
{
    int elapsedTime = (int)time(NULL) - UserDefault::getInstance()->getIntegerForKey(KEY::TIME_OF_ENTERING_BACKGROUND);
    if (elapsedTime > 7200) {
        UserDefault::getInstance()->setBoolForKey(KEY::GO_TO_TITLE_SCENE, true);
    }
    if (UserDefault::getInstance()->getBoolForKey(KEY::NON_RACE_SCENE_FLAG) == true) {
        elapsedTime = 0;
    }
    UserDefault::getInstance()->setIntegerForKey(KEY::TIME_OF_BACKGROUND_STAY, elapsedTime);
}







