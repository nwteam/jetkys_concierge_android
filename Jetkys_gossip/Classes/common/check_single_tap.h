#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

struct check_single_tap {
    
protected:
    check_single_tap():_firstTappedButton(nullptr){};
    void checkSingleTap(Ref* sender, cocos2d::ui::Widget::TouchEventType type, const std::function<void()>&callback)
    {
        if (type == cocos2d::ui::Widget::TouchEventType::BEGAN && _firstTappedButton == nullptr) {
            _firstTappedButton = sender;
        }
        if (type == cocos2d::ui::Widget::TouchEventType::CANCELED) {
            _firstTappedButton = nullptr;
        }
        if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
            if (_firstTappedButton == sender) {
                callback();
            }
            _firstTappedButton = nullptr;
        }
    }
private:
    Ref* _firstTappedButton;
};