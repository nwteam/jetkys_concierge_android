#include "JDate.h"
#include <time.h>

namespace JDate
{
std::string getTimeWithFormat(const char* format)
{
    ////CCLOG("Format: %s", format);
    time_t rawtime;
    struct tm* timeinfo;
    char buffer [80];

    time (&rawtime);
    timeinfo = localtime (&rawtime);

    strftime(buffer, 80, format, timeinfo);

    ////CCLOG("JDate::getTimeWithFormat %s, %zu", buffer, strlen(buffer));
    return buffer;
}

float getDateTime()
{
    timeval time;
    gettimeofday(&time, NULL);
    unsigned long millisecs = (time.tv_sec * 1000) + (time.tv_usec / 1000);
    return (float)millisecs;
}

std::string getTimeIgnoreNoNumber(std::string datetime)
{
    std::string ignoreStr = "";
    //////CCLOG("getTimeIgnoreNoNumber: %s", datetime.c_str());
    for (int i = 0; i < datetime.length(); i++) {
        //////CCLOG("%hhd", datetime.at(i));
        if (datetime.at(i) >= '0' && datetime.at(i) <= '9') {
            ignoreStr += datetime.at(i);
        }
    }
    return ignoreStr;
}

int calculateTimeStringDiffOnDay(const std::string &timeString1, const std::string &timeString2, const std::string &format)
{
    struct tm tm1;
    struct tm tm2;

    strptime(timeString1.c_str(), format.c_str(), &tm1);
    strptime(timeString2.c_str(), format.c_str(), &tm2);

    double diff = difftime(mktime(&tm1), mktime(&tm2));

    return (int)diff / (60 * 60 * 24);
}
}
