#ifndef Syanago_FontDefines_h
#define Syanago_FontDefines_h

#include "cocos2d.h"

//フォント設定
#define FONT_CHARMAP "fps_number.png"
#define FONT_NAME "Kokumincho_regular.ttf"
#define FONT_NAME_2 "FOT-SKIPSTD-B.ttf"
#define FONT_NAME_3 "MMCedar-P.ttf"
#define FONT_NAME_4 "MMCedar.ttf"
#define FONT_NAME_5 "komatuna-p.ttf"
#define FONT_NAME_6 "komatuna.ttf"

#define FONT_SYSTEM "HiraKakuProN-W6"

//色設定
#define COLOR_YELLOW Color3B(255, 231, 117)
#define COLOR_PINK Color3B(255, 173, 194)
#define COLOR_D_ORANGEE Color3B(122, 85, 26)
#define COLOR_ORANGEE Color3B(255, 180, 32)

//テロップスピード
#define TEROP_SPEED 20.0f

enum VoiceActionTextCode{
    WELCOME_VOICE = 1,
    CHARACTER_TAP = 2,
};

inline void fixFontPosition(cocos2d::Label *label) {
    cocos2d::Vec2 anchor = label->getAnchorPoint();
    cocos2d::Size size = label->getContentSize();
    cocos2d::Vec2 pos = label->getPosition();
    anchor.y = (anchor.y + 1) * 0.5f;
    //pos.y = pos.y -
    size.height = size.height * 0.5f;
    
    label->setAnchorPoint(anchor);
    
    //label->setDimensions(size.width, size.height * 0.5f);
    
    //label->setPosition(pos + Vec2(0, -label->getContentSize().height * (1.0f - anchor.y)));
}

inline void fixFontContentSize(cocos2d::Label *label) {
    cocos2d::Size size = label->getContentSize();
    label->setContentSize(cocos2d::Size(size.width, size.height * 0.5f));
}

inline std::string removeSpaces(std::string input)
{
    int length = (int)input.length();
    for (int i = 0; i < length; i++) {
        if(input[i] == ' '  )
            input.erase(i, 1);
    }
    return input;
}


#endif
