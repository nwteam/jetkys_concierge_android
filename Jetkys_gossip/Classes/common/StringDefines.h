#ifndef syanago_StringDefines_h
#define syanago_StringDefines_h

//Format Texture Packer Image
#define IMAGE_CHARACTER_ICON_LEADER "%d_r.png"
#define IMAGE_CHARACTER_ICON_SUPPORT "%d_s.png"
#define IMAGE_CHARACTER_ICON "%d_i.png"
#define IMAGE_CHARACTER_ICON_HK "%d_hk.png"


//Key is_new(character, part, item)
#define KEY_CHARACTER_IS_NEW "key_character_is_new_%d"
#define KEY_PART_IS_NEW "key_part_is_new_%d"
//value = true -> init all characters of player with is_new = false, set value = false
//value = false -> init all characters of player with is_new = getIsNewFromFile
#define KEY_FIRST_CREATE_PLAYER "key_first_create_player"


#endif
