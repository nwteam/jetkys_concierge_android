#include "TimeConverter.h"

std::string TimeConverter::convertEndTimeString(std::string serverEndTimeResponse)
{
    time_t timer;
    time(&timer);
    struct tm tm1;
    std::string dateFormat = "%Y-%m-%d %H:%M:%S";
    strptime(serverEndTimeResponse.c_str(), dateFormat.c_str(), &tm1);
    tm1.tm_isdst = localtime(&timer)->tm_isdst;
    tm1.tm_gmtoff = localtime(&timer)->tm_gmtoff;
    time_t timer2 = mktime(&tm1);
    double seconds = difftime(timer2, timer);

    int max = 60 * 60 * 24 * 365 * 10;
    int diff = seconds > (double)max ? max : (int)seconds;

    int year = diff / (60 * 60 * 24 * 7 * 52);
    if (year > 0) {
        return StringUtils::format("%d年", year);
    } else {
        int month = diff / (60 * 60 * 24 * 30);
        if (month > 0) {
            return StringUtils::format("%dヶ月", month);
        } else {
            int week = diff / (60 * 60 * 24 * 7);
            if (week > 0) {
                return StringUtils::format("%d週間", week);
            } else {
                int day = diff / (60 * 60 * 24);
                if (day > 0) {
                    return StringUtils::format("%d日", day);
                } else {
                    int hour = diff / (60 * 60);
                    if (hour > 0) {
                        return StringUtils::format("%d時間", hour);
                    } else {
                        int minute = diff / 60;
                        if (minute > 0) {
                            return StringUtils::format("%d分", hour);
                        }
                    }
                }
            }
        }
    }
    return "既に終了しています";
}

std::string TimeConverter::convertLastLogInTimeString(std::string lastLogInTime)
{
    // 2014-08-26 13:07:43
    //上記の様な値を年月日に分ける
    // todo QuynhNM fix
    time_t timer;
    time(&timer); /* get current time; same as: timer = time(NULL)  */
    //    char tmp[100];

    struct tm tm1;
    std::string dateFormat = "%Y-%m-%d %H:%M:%S";

    //    strftime(tmp, 100, "%Y-%m-%d %H:%M:%S %Z %z", localtime(&timer));
    //    //CCLOG("now1: %s", tmp);

    strptime(lastLogInTime.c_str(), dateFormat.c_str(), &tm1);
    //    tm1.tm_zone = (char*)malloc(100 * sizeof(char*));
    tm1.tm_isdst = localtime(&timer)->tm_isdst;
    tm1.tm_gmtoff = localtime(&timer)->tm_gmtoff;
    //    //CCLOG("gmtoff: %ld, daylight: %d", tm1.tm_gmtoff, tm1.tm_isdst);

    time_t timer2 = mktime(&tm1);
    //    strftime(tmp, 100, dateFormat.c_str(), localtime(&timer2));
    //    //CCLOG("now2: %s", tmp);

    double seconds = difftime(timer, timer2);
    int max = 60 * 60 * 24 * 365 * 10;
    int diff = seconds > (double)max ? max : (int)seconds;

    int year = diff / (60 * 60 * 24 * 7 * 52);
    std::string strRes;
    if (year > 0) {
        strRes = StringUtils::format("%d年前", year);
    } else {
        int month = diff / (60 * 60 * 24 * 30);
        if (month > 0) {
            strRes = StringUtils::format("%dヶ月前", month);
        } else {
            int week = diff / (60 * 60 * 24 * 7);
            if (week > 0) {
                strRes = StringUtils::format("%d週間前", week);
            } else {
                int day = diff / (60 * 60 * 24);
                if (day > 0) {
                    strRes = StringUtils::format("%d日前", day);
                } else {
                    int hour = diff / (60 * 60);
                    if (hour > 0) {
                        strRes = StringUtils::format("%d時間前", hour);
                    } else {
                        strRes = "1時間以内";
                    }
                }
            }
        }
    }
    //  //CCLOG("lastLoged: %s -> %s, %d", timeStr.c_str(), strRes.c_str(), diff);
    return strRes;
}

