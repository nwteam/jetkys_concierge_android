#ifndef __syanago__show_head_line__
#define __syanago__show_head_line__

#include "cocos2d.h"
#include "FontDefines.h"
#include "HeaderStatus.h"
#include <utility>

USING_NS_CC;

struct show_head_line {
    enum TAG_HEADLINE {
        HEADLINE_BACKGROUND = 700
    };
    typedef std::function<void(Ref *pSender)> backButtonCallback;
    void showHeadLine(Layer* layer, const std::string& headTitle, const backButtonCallback& callback)
    {
        const cocos2d::Size winSize = Director::getInstance()->getWinSize();
        
        Sprite *background = Sprite::create("head_line_no1.png");
        background->setPosition(background->getContentSize().width/2,
                              winSize.height- background->getContentSize().height / 2 - HEADER_STATUS::SIZE::HEIGHT - 12);
        background->setTag(TAG_HEADLINE::HEADLINE_BACKGROUND);
        
        Sprite *backButtonSlected = Sprite::create("back_button.png");
        backButtonSlected->setColor(Color3B::GRAY);
        Menu *backButtonMenu = Menu::create(MenuItemSprite::create(Sprite::create("back_button.png"),backButtonSlected,callback), NULL);
        backButtonMenu->setPosition(60, background->getContentSize().height/2);
        background->addChild(backButtonMenu);
        
        Label *title = Label::createWithTTF(headTitle, FONT_NAME_2, 34);
        title->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        title->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
        title->setPosition(120, 51);
        title->enableShadow(Color4B(Color3B::BLACK), cocos2d::Size(2, -2));
        background->addChild(title);
        
        layer->addChild(background,11);
    }
};
#endif /* defined(__syanago__show_head_line__) */
