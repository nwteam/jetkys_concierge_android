#ifndef __Syanago__JDate__
#define __Syanago__JDate__

#include "cocos2d.h"

namespace JDate {
    std::string getTimeWithFormat(const char * format);
    float getDateTime();
    std::string getTimeIgnoreNoNumber(std::string datetime);
    int calculateTimeStringDiffOnDay(const std::string &timeString1, const std::string &timeString2, const std::string &format);
}

#endif /* defined(__Syanago__JDate__) */
