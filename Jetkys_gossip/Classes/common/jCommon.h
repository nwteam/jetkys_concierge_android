#ifndef __common__
#define __common__

#include "cocos2d.h"
USING_NS_CC;

extern void transitScene(Scene *scene);
extern Sprite *makeSprite(const char *name);
extern MenuItemSprite * makeMenuItem(const char *frameName, const ccMenuCallback& callback);
extern MenuItemSprite * makeMenuItem(std::string frameName, std::string title, const ccMenuCallback& callback);
extern MenuItemSprite * makeMenuItem(std::string frameName, std::string title, int fonSize, const ccMenuCallback& callback);

#endif /* defined(__common__) */
