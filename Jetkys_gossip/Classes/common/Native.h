#ifndef __SpaceShip__Native__
#define __SpaceShip__Native__

#include "cocos2d.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "UtilsIOS.h"
#include "ArtLvtSupport.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "UtilsAndroid.h"
#endif

namespace Native {
    void webview(std::string url);
    void dialog(std::string message);
    std::string getOSUUID();
    std::string getOSVersion();
    std::string getApplicationVersion();
    std::string getIV();
    std::string getPushToken();
    
    std::string getOsDIDInKeychain();
    void saveOsDIDInKeychain(std::string did);
    void delOsDIDInKeychain();
    
    std::string encrypt(const char * targetString, const char * hexIV);
    std::string decrypt(std::string targetString, std::string hexIV);
    std::string sha256(std::string targetString);
    
    void playCharacterVoice(const char *voice);				//Character voice
};



#endif /* defined(__SpaceShip__Native__) */
