#ifndef __syanago__UserDefaultManager__
#define __syanago__UserDefaultManager__

#include <string>

class UserDefaultManager {
public:
    static void cycleConnection();
    static std::string getCurrentDomainName();
    
    class KEY {
    public:
        static const char* GO_TO_TITLE_SCENE;
        static const char* DEMAND_MODE;
        static const char* FIRST_VOICE_SYSTEM;
        static const char* VOICE_OPTION_STATUS;
        static const char* DOMAIN_URL;
        static const char* NEW_DEVICE;
        static const char* SE_OPTION_STATUS;
        static const char* BGM_OPTION_STATUS;
        static const char* LEADER_CHARACTOR_MASTER_ID;
        static const char* VOICE_SOUND_ID;
        static const char* OLD_RESOURCES_DELETE;
        static const char* NON_RACE_SCENE_FLAG;
        static const char* OPEN_GACHA;
        static const char* NEW_PLAYER;
        static const char* END_FIRST_LOGIN;
        static const char* NO_HOME_VOICE;
        static const char* DEVICE_PUSH_TOKEN;
        static const char* DATA_BASE_VERSION;
        static const char* IS_EVENT_RACE;
        static const char* IS_RESET_MASTER_TABLE;
        static const char* IS_RANK_UP;
        static const char* TIME_OF_RACE_START;
        static const char* TIME_OF_BACKGROUND_STAY;
        static const char* TIME_OF_HEADER;
        static const char* TIME_OF_ENTERING_BACKGROUND;
    };
    
    static void initSplashScene();
    static void initTitleScene();
    static void initMainScene();
    static void setTimeOnEnteringBackground();
    static void setElapsedTime();
    
};
#endif /* defined(__syanago__UserDefaultManager__) */
