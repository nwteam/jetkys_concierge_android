#ifndef __syanago__DisableEventListener__
#define __syanago__DisableEventListener__

#include "cocos2d.h"

USING_NS_CC;

class DisableEventListener : public EventListenerTouchOneByOne
{
public:
    CREATE_FUNC(DisableEventListener);
    bool init();
};

#endif /* defined(__syanago__DisableEventListener__) */
