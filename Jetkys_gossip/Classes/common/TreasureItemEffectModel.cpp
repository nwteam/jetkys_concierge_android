#include "TreasureItemEffectModel.h"
#include "PlayerController.h"
#include "ItemModel.h"
#include "ItemUnitModel.h"

int TreasureItemEffectModel::getTreasureItemIncrementalDifferenceEffect(const int itemEffectId)
{
    int result = 0;
    int count = ItemModel::getMaxItemCount();
    for (int i = 1; i <= count; i++) {
        std::shared_ptr<ItemModel>itemModel(ItemModel::find(i));
        if (itemModel->getEffectId() != itemEffectId) {
            continue;
        }
        const int masterItemId = itemModel->getID();
        const int maxItemUnitCount = ItemUnitModel::getItemUnitCount(masterItemId);

        int playerGetStarCount = 0;
        int getItemUnitCount = 0;
        for (int j = 0; j < PLAYERCONTROLLER->_playerItems.size(); j++) {
            std::shared_ptr<ItemUnitModel>itemModel(ItemUnitModel::find(PLAYERCONTROLLER->_playerItems.at(j)->getItemUnitId()));
            if (itemModel != nullptr&& itemModel->getItemId() == masterItemId) {
                getItemUnitCount++;
                playerGetStarCount += convertStarCount(PLAYERCONTROLLER->_playerItems.at(j)->getItemUnitKind());
            }
        }

        if (getItemUnitCount != maxItemUnitCount) {
            continue;
        }
        switch (playerGetStarCount) {
        case 3: {
            result += itemModel->getStar3();
            break;
        }
        case 4: {
            result += itemModel->getStar4();
            break;
        }
        case 5: {
            result += itemModel->getStar5();
            break;
        }
        case 6: {
            result += itemModel->getStar6();
            break;
        }
        case 7: {
            result += itemModel->getStar7();
            break;
        }
        case 8: {
            result += itemModel->getStar8();
            break;
        }
        case 9: {
            result += itemModel->getStar9();
            break;
        }
        default:
            break;
        }
    }
    return result;
}

int TreasureItemEffectModel::convertStarCount(const int itemKind)
{
    switch (itemKind) {
    case 1: {
        return 3;
        break;
    }
    case 2: {
        return 2;
        break;
    }
    case 3: {
        return 1;
        break;
    }
    default: {
        return 0;
        break;
    }
    }
}



