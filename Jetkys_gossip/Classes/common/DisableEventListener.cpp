#include "DisableEventListener.h"

bool DisableEventListener::init()
{
    if (!EventListenerTouchOneByOne::init()) {
        return false;
    }
    setSwallowTouches(true);
    onTouchBegan = [&](Touch* touch, Event* unused_event) -> bool {
                       return true;
                   };
    onTouchMoved = [&](Touch* touch, Event* unused_event) {};
    onTouchCancelled = [&](Touch* touch, Event* unused_event) {};
    onTouchEnded = [&](Touch* touch, Event* unused_event) {};
    return true;
}
