#ifndef Syanago_GameDefines_h
#define Syanago_GameDefines_h


#define DEVICE_TYPE (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) ? 1 : 2

//#define KEY_ME_ID "key_me_id"
//#define KEY_ME_INSTALL_KEY "key_me_install_key"

#define KEY_TOKEN_PRODUCT_ID_NONE_ACTIVE "key_token_product_id_none_active"
#define KEY_TOKEN_REQUEST_VERIFICATION "key_token_request_verification"

#define KEY_TIME_SYNC_SAVE "key_time_sync_save"
#define TIME_SYNC 20.0f

#endif
