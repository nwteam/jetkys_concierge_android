#include "jCommon.h"
#include "SoundHelper.h"
#include "FontDefines.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    #include "UtilsIOS.h"
#endif

void transitScene(Scene* scene)
{
    SOUND_HELPER->stopAllMusic();
    Director::getInstance()->replaceScene(TransitionFade::create(0.5f, scene));
}

Sprite* makeSprite(const char* name)
{
    return SpriteFrameCache::getInstance()->getSpriteFrameByName(name) ?
           Sprite::createWithSpriteFrameName(name) :
           Sprite::create(name);
}

MenuItemSprite* makeMenuItem(const char* frameName, const ccMenuCallback& callback)
{
    Sprite* selected = makeSprite(frameName);
    selected->setColor(Color3B::GRAY);

    Sprite* disabled = makeSprite(frameName);
    disabled->setColor(Color3B::GRAY);

    return MenuItemSprite::create(makeSprite(frameName), selected, disabled, callback);
}

MenuItemSprite* makeMenuItem(std::string frameName, std::string title, const ccMenuCallback& callback)
{
    auto button = makeMenuItem(frameName.c_str(), callback);

    auto label = Label::createWithTTF(title, FONT_NAME_2, 30, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->setPosition(Vec2(button->getNormalImage()->getContentSize().width / 2, button->getNormalImage()->getContentSize().height / 2 - 15));
    label->enableShadow();

    button->addChild(label);
    return button;
}

MenuItemSprite* makeMenuItem(std::string frameName, std::string title, int fontSize, const ccMenuCallback& callback)
{
    auto button = makeMenuItem(frameName.c_str(), callback);

    auto label = Label::createWithTTF(title, FONT_NAME_2, fontSize, Size(Vec2::ZERO), TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->setPosition(Vec2(button->getNormalImage()->getContentSize().width / 2, button->getNormalImage()->getContentSize().height / 2 - fontSize / 2));
    label->enableShadow();

    button->addChild(label);
    return button;
}

