#ifndef __syanago__create_func__
#define __syanago__create_func__
#include<utility>

template<class Derived>
struct create_func {
    template<class... Args>
    static Derived* create(Args&&... args) {
        auto p = new Derived();
        if (p->init(std::forward<Args>(args)...)) {
            p->autorelease();
            return p;
        } else {
            delete p;
            return nullptr;
        }
    }
};
#endif /* defined(__syanago__create_func__) */
