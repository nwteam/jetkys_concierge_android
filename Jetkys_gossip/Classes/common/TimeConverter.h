#ifndef __syanago__TimeConverter__
#define __syanago__TimeConverter__

#include "cocos2d.h"

USING_NS_CC;

class TimeConverter
{
public:
    static std::string convertEndTimeString(std::string serverEndTimeResponse);
    static std::string convertLastLogInTimeString(std::string lastLogInTime);
};

#endif
