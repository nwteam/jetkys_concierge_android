#ifndef __syanago__PlayerPartsComparator__
#define __syanago__PlayerPartsComparator__
#include <functional>
#include "PlayerPartModel.h"

class PlayerPartsComparator
{
public:
    typedef std::function<bool (PlayerPartModel*, PlayerPartModel*)> Comparator;
    static bool Created(PlayerPartModel* i, PlayerPartModel* j);
    static bool Rarity(PlayerPartModel* i, PlayerPartModel* j);
    static bool Power(PlayerPartModel* i, PlayerPartModel* j);
    static bool Technique(PlayerPartModel* i, PlayerPartModel* j);
    static bool Brake(PlayerPartModel* i, PlayerPartModel* j);
    static bool No(PlayerPartModel* i, PlayerPartModel* j);
};


#endif /* defined(__syanago__PlayerPartsComparator__) */
