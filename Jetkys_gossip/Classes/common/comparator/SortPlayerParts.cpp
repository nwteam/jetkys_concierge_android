#include "SortPlayerParts.h"
#include "PlayerController.h"

const std::vector<std::string>SortPlayerParts::SORT_LABELS = {
    "No.",
    "取得",
    "レア度",
    "馬力",
    "技量",
    "忍耐"
};

std::string SortPlayerParts::getLabelName(KEY key)
{
    return SortPlayerParts::SORT_LABELS.at(key);
}

void SortPlayerParts::sort(SortPlayerParts::KEY key)
{
    PLAYERCONTROLLER->sortPlayerParts(getComparator(key));
}

const PlayerPartsComparator::Comparator SortPlayerParts::getComparator(SortPlayerParts::KEY key)
{
    switch (key) {
    case KEY::CREATED:
        return PlayerPartsComparator::Created;
        break;
    case KEY::RARITY:
        return PlayerPartsComparator::Rarity;
        break;
    case KEY::POWER:
        return PlayerPartsComparator::Power;
        break;
    case KEY::TECHNIQUE:
        return PlayerPartsComparator::Technique;
        break;
    case KEY::BRAKE:
        return PlayerPartsComparator::Brake;
        break;
    case KEY::DISPLAY_NO:
        return PlayerPartsComparator::No;
        break;
    }
}