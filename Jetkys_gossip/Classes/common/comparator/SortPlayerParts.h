#ifndef __syanago__SortPlayerParts__
#define __syanago__SortPlayerParts__

#include "PlayerPartModel.h"
#include "PlayerPartsComparator.h"

class SortPlayerParts {
public:
    enum KEY {
        DISPLAY_NO = 0,
        CREATED,
        RARITY,
        POWER,
        TECHNIQUE,
        BRAKE
    };
    static const std::vector<std::string> SORT_LABELS;
    
    static std::string getLabelName(KEY key);
    
    static void sort(SortPlayerParts::KEY key);
    
private:
    static const PlayerPartsComparator::Comparator getComparator(SortPlayerParts::KEY key);
};

#endif /* defined(__syanago__SortPlayerParts__) */
