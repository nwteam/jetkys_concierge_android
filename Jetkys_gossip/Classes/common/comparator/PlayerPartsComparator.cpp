#include "PlayerPartsComparator.h"
#include "PartModel.h"


bool PlayerPartsComparator::Created(PlayerPartModel* i, PlayerPartModel* j)
{
    if (i->getCreated().compare(j->getCreated()) == 0) {
        return i->getPartsId() < j->getPartsId();
    }
    return (i->getCreated().compare(j->getCreated()) < 0);
}

bool PlayerPartsComparator::Rarity(PlayerPartModel* i, PlayerPartModel* j)
{
    int rarity1 = PartModel::find(i->getPartsId())->getRarity();
    int rarity2 = PartModel::find(j->getPartsId())->getRarity();

    if (rarity1 == rarity2) {
        return i->getPartsId() < j->getPartsId();
    }

    return rarity1 > rarity2;
}

bool PlayerPartsComparator::Power(PlayerPartModel* i, PlayerPartModel* j)
{
    int exercise1 = PartModel::find(i->getPartsId())->getExercise();
    int exercise2 = PartModel::find(j->getPartsId())->getExercise();

    if (exercise1 == exercise2) {
        return i->getPartsId() < j->getPartsId();
    }
    return exercise1 > exercise2;
}

bool PlayerPartsComparator::Technique(PlayerPartModel* i, PlayerPartModel* j)
{
    int reaction1 = PartModel::find(i->getPartsId())->getReaction();
    int reaction2 = PartModel::find(j->getPartsId())->getReaction();

    if (reaction1 == reaction2) {
        return i->getPartsId() < j->getPartsId();
    }

    return reaction1 > reaction2;
}

bool PlayerPartsComparator::Brake(PlayerPartModel* i, PlayerPartModel* j)
{
    int decision1 = PartModel::find(i->getPartsId())->getDecision();
    int decision2 = PartModel::find(j->getPartsId())->getDecision();

    if (decision1 == decision2) {
        return i->getPartsId() < j->getPartsId();
    }

    return decision1 > decision2;
}

bool PlayerPartsComparator::No(PlayerPartModel* i, PlayerPartModel* j)
{
    int no1 = PartModel::find(i->getPartsId())->getDisplayOrder();
    int no2 = PartModel::find(j->getPartsId())->getDisplayOrder();

    if (no1 == no2) {
        return i->getPartsId() < j->getPartsId();
    }

    return no1 > no2;
}