#include "PlayerCharacterComparator.h"
#include "CharacterModel.h"
#include "BodyTypeModel.h"

bool PlayerCharacterComparator::Created(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    if (i->getCreated().compare(j->getCreated()) == 0) {
        return i->getCharactersId() < j->getCharactersId();
    }
    return (i->getCreated().compare(j->getCreated()) < 0);
}

bool PlayerCharacterComparator::Cost(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    int cost1 = CharacterModel::find(i->getCharactersId())->getCost();
    int cost2 = CharacterModel::find(j->getCharactersId())->getCost();

    if (cost1 == cost2) {
        return i->getCharactersId() < j->getCharactersId();
    }

    return (cost1 > cost2);
}


bool PlayerCharacterComparator::Level(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    if (i->getLevel() == j->getLevel()) {
        return i->getCharactersId() < j->getCharactersId();
    }
    return (i->getLevel() > j->getLevel());
}

bool PlayerCharacterComparator::Rarity(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    int rarity1 = CharacterModel::find(i->getCharactersId())->getRarity();
    int rarity2 = CharacterModel::find(j->getCharactersId())->getRarity();

    if (rarity1 == rarity2) {
        return i->getCharactersId() < j->getCharactersId();
    }

    return (rarity1 > rarity2);
}

bool PlayerCharacterComparator::Power(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    if (i->getExercise() == j->getExercise()) {
        return i->getCharactersId() < j->getCharactersId();
    }
    return (i->getExercise() > j->getExercise());
}

bool PlayerCharacterComparator::Technique(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    if (i->getReaction() == j->getReaction()) {
        return i->getCharactersId() < j->getCharactersId();
    }
    return (i->getReaction() > j->getReaction());
}

bool PlayerCharacterComparator::Brake(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    if (i->getDecision() == j->getDecision()) {
        return i->getCharactersId() < j->getCharactersId();
    }
    return (i->getDecision() > j->getDecision());
}

bool PlayerCharacterComparator::EquipmentNumber(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    if (i->getPartNumber() == j->getPartNumber()) {
        return i->getCharactersId() < j->getCharactersId();
    }
    return (i->getPartNumber() > j->getPartNumber());
}

bool PlayerCharacterComparator::MakedDiceNumber(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    if (i->getSaikoroNumber() == j->getSaikoroNumber()) {
        return i->getCharactersId() < j->getCharactersId();
    }
    return (i->getSaikoroNumber() > j->getSaikoroNumber());
}

bool PlayerCharacterComparator::BodyType(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    int bodyTypeId1 = CharacterModel::find(i->getCharactersId())->getBodyType();
    int bodyTypeId2 = CharacterModel::find(j->getCharactersId())->getBodyType();

    if (bodyTypeId1 == bodyTypeId2) {
        return i->getCharactersId() < j->getCharactersId();
    }

    return (bodyTypeId1 > bodyTypeId2);
}

bool PlayerCharacterComparator::Maker(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    int makerId1 = CharacterModel::find(i->getCharactersId())->getMakerId();
    int makerId2 = CharacterModel::find(j->getCharactersId())->getMakerId();

    if (makerId1 == makerId2) {
        return i->getCharactersId() < j->getCharactersId();
    }

    return (makerId1 < makerId2);
}

bool PlayerCharacterComparator::Lock(PlayerCharactersModel* i, PlayerCharactersModel* j)
{
    if (i->getLock() == j->getLock()) {
        return i->getCharactersId() < j->getCharactersId();
    }
    return (i->getLock() > j->getLock());
}