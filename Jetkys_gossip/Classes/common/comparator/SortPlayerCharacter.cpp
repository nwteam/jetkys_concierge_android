#include "SortPlayerCharacter.h"
#include "PlayerController.h"

const std::vector<std::string>SortPlayerCharacter::SORT_LABELS = {
    "取得",
    "コスト",
    "レベル",
    "レア度",
    "馬力",
    "技量",
    "忍耐",
    "装備数",
    "刻印数",
    "タイプ",
    "メーカー",
    "お気に入り"
};


std::string SortPlayerCharacter::getLabelName(KEY key)
{
    return SortPlayerCharacter::SORT_LABELS.at(key);
}

void SortPlayerCharacter::sort(SortPlayerCharacter::KEY key)
{
    PLAYERCONTROLLER->sortPlayerCharacter(getComparator(key));
}

const PlayerCharacterComparator::Comparator SortPlayerCharacter::getComparator(SortPlayerCharacter::KEY key)
{
    switch (key) {
    case KEY::CREATED:
        return PlayerCharacterComparator::Created;
        break;
    case KEY::COST:
        return PlayerCharacterComparator::Cost;
        break;
    case KEY::LEVEL:
        return PlayerCharacterComparator::Level;
        break;
    case KEY::RARITY:
        return PlayerCharacterComparator::Rarity;
        break;
    case KEY::POWER:
        return PlayerCharacterComparator::Power;
        break;
    case KEY::TECHNIQUE:
        return PlayerCharacterComparator::Technique;
        break;
    case KEY::BRAKE:
        return PlayerCharacterComparator::Brake;
        break;
    case KEY::EQUIPMENT_NUMBER:
        return PlayerCharacterComparator::EquipmentNumber;
        break;
    case KEY::MAKED_DICE_NUMBER:
        return PlayerCharacterComparator::MakedDiceNumber;
        break;
    case KEY::BODY_TYPE:
        return PlayerCharacterComparator::BodyType;
        break;
    case KEY::MAKER:
        return PlayerCharacterComparator::Maker;
        break;
    case KEY::LOCK:
        return PlayerCharacterComparator::Lock;
        break;
    }
}