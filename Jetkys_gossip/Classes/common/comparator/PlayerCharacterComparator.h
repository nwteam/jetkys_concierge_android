#ifndef __syanago__PlayerCharacterComparator__
#define __syanago__PlayerCharacterComparator__
#include <functional>
#include "PlayerCharactersModel.h"

class PlayerCharacterComparator
{
public:
    typedef std::function<bool (PlayerCharactersModel*, PlayerCharactersModel*)> Comparator;
    static bool Created(PlayerCharactersModel* i, PlayerCharactersModel* j);
    static bool Cost(PlayerCharactersModel* i, PlayerCharactersModel* j);
    static bool Level(PlayerCharactersModel* i, PlayerCharactersModel* j);
    static bool Rarity(PlayerCharactersModel* i, PlayerCharactersModel* j);
    static bool Power(PlayerCharactersModel* i, PlayerCharactersModel* j);
    static bool Technique(PlayerCharactersModel* i, PlayerCharactersModel* j);
    static bool Brake(PlayerCharactersModel* i, PlayerCharactersModel* j);
    static bool EquipmentNumber(PlayerCharactersModel* i, PlayerCharactersModel* j);
    static bool MakedDiceNumber(PlayerCharactersModel* i, PlayerCharactersModel* j);
    static bool BodyType(PlayerCharactersModel* i, PlayerCharactersModel* j);
    static bool Maker(PlayerCharactersModel* i, PlayerCharactersModel* j);
    static bool Lock(PlayerCharactersModel* i, PlayerCharactersModel* j);
};

#endif /* defined(__syanago__PlayerCharacterComparator__) */
