#ifndef __syanago__SortPlayerCharacter__
#define __syanago__SortPlayerCharacter__

#include "PlayerCharactersModel.h"
#include "PlayerCharacterComparator.h"

class SortPlayerCharacter {
public:
    enum KEY {
        CREATED,
        COST,
        LEVEL,
        RARITY,
        POWER,
        TECHNIQUE,
        BRAKE,
        EQUIPMENT_NUMBER,
        MAKED_DICE_NUMBER,
        BODY_TYPE,
        MAKER,
        LOCK
    };
    
    static const std::vector<std::string> SORT_LABELS;
    
    static std::string getLabelName(KEY key);
    
    static void sort(SortPlayerCharacter::KEY key);
    
private:
    static const PlayerCharacterComparator::Comparator getComparator(SortPlayerCharacter::KEY key);
};


#endif /* defined(__syanago__SortPlayerCharacter__) */
