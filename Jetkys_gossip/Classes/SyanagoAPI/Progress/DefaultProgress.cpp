#include "DefaultProgress.h"
#include "NetworkErrorDialog.h"
#include "API.h"

using namespace SyanagoAPI;

void DefaultProgress::onStart()
{
    if (_loading != nullptr) {
        return;
    }
    _loading = Loading::create();
    _loading->show();
}
void DefaultProgress::onEnd()
{
    if (_loading != nullptr) {
        _loading->dissmiss();
        _loading = nullptr;
    }
}

void DefaultProgress::onError(int errorCode, std::string errorMessage, const RetryRequest& retryRequest)
{
    using namespace SyanagoAPI::API;
    std::string title = errorCode == ERROR::CODE::NETWORK ? "ネットワークエラー" :  "エラー[" + StringUtils::format("%d", errorCode) + "]";
    std::string message = errorCode == ERROR::CODE::NETWORK ? "ネットワーク環境の良いところで\n再接続を行ってください" : errorMessage;

    int tag = errorCode == ERROR::CODE::NETWORK ? ERROR::CODE::NETWORK : -1;

    std::string _apiTag = "";
    if (_apiTag == TAG::LOG_GOOGLEPLAY_PURCHASE || _apiTag == TAG::CHECK_STORE_RECEIPT) {
        tag = 250;
    }
    if (errorCode == ERROR::CODE::SERVER_MAINTENANCE || ERROR::isRecognizeErrors(errorCode) == false) {
        tag = ERROR::CODE::SERVER_MAINTENANCE;
    }
    NetworkErrorDialog::show(title, message, tag, retryRequest);
}
