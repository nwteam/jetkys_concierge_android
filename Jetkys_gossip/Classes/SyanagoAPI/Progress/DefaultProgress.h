#ifndef __syanago__DefaultProgress__
#define __syanago__DefaultProgress__

#include "cocos2d.h"
#include "ProgressAbstract.h"
#include "Loading.h"
#include "RequestAPI.h"

USING_NS_CC;

namespace SyanagoAPI {
    
class DefaultProgress : public ProgressAbstract {
public:
    void setHttpRequestForRetry(RequestAPI* api);
    //WARNING: Do not use this method at onEnterTransitionDidFinish()
    void onStart() override;
    void onEnd() override;
    void onError(int errorCode, std::string message, const RetryRequest& retryRequest) override;
private:
    Loading *_loading;
};

};
#endif /* defined(__syanago__DefaultProgress__) */
