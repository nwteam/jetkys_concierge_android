#ifndef __syanago__ProgressAbstract__
#define __syanago__ProgressAbstract__

#include "cocos2d.h"
#include <string>
#include <functional>

USING_NS_CC;
namespace SyanagoAPI {
    
class ProgressAbstract {
public:
    virtual ~ProgressAbstract(){};
    virtual void onStart() = 0;
    virtual void onEnd() = 0;
    typedef std::function<void()> RetryRequest;
    virtual void onError(int errorCode, std::string message, const RetryRequest& retryRequest) = 0;
};
};

#endif /* defined(__syanago__ProgressAbstract__) */
