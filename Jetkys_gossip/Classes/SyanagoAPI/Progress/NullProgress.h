#ifndef __syanago__NullProgress__
#define __syanago__NullProgress__

#include "ProgressAbstract.h"
#include <string>
namespace SyanagoAPI {
    
class NullProgress : public ProgressAbstract {
public:
    NullProgress(){}
    
    void onStart() override;
    void onEnd() override;
    void onError(int errorCode, std::string message, const RetryRequest& retryRequest) override;
};
    
};
#endif /* defined(__syanago__NullProgress__) */
