#include "API.h"

using namespace SyanagoAPI::API;

const std::string ERROR::getMessage(ERROR::CODE code)
{
    using SyanagoAPI::API::ERROR;
    switch (code) {
    case CODE::NOT_POST:
        return "サーバーへの送信情報に誤りがあります[201]";
    case CODE::NOT_ENOUGH_OR_WRONG_POST:
        return "サーバーへの送信情報に誤りがあります[202]";
    case CODE::APP_VERSION_INCONSISTENCY:
        return "サーバーへの送信情報に誤りがあります[203]";
    case CODE::VALIDATION_FAIL:
        return "プレイヤー情報がみつかりません[204]";
    case CODE::NOT_FOUND_PLAYER:
        return "プレイヤー情報がみつかりません[205]";
    case CODE::WRONG_PLAYER:
        return "プレイヤー情報がみつかりません[206]";
    case CODE::DELETE_PLAYER:
        return "プレイヤー情報がみつかりません[207]";
    case CODE::NOT_ENOUGH_LOG_DATA:
        return "サーバーへの送信情報に誤りがあります[208]";
    case CODE::NOT_FOUND_DATA_IN_APC:
        return "データが見つかりません[209]";
    case CODE::NOT_FOUND_DATA_IN_CACHED:
        return "データが見つかりません[210]";
    case CODE::CONTROLLER_NAME_MISTAKE:
        return "サーバーへの送信情報に誤りがあります[211]";
    case CODE::USER_DATA_REGISTER_FAIL:
        return "ユーザー登録に失敗しました[212]";
    case CODE::USER_DATA_REGISTER_FAIL_SHARD:
        return "ユーザー登録に失敗しました[213]";
    case CODE::USER_DATA_CHANGE_FAIL:
        return "ユーザー情報の変更に失敗しました[214]";
    case CODE::USER_DATA_CHANGE_FAIL_SHARD:
        return "ユーザー情報の変更に失敗しました[215]";
    case CODE::DUPLICATION_LOGIN:
        return "重複ログインが発生しています[216]";
    case CODE::DEVICE_TIME_MISTAKE:
        return "端末の時間設定に誤りがあります[217]";
    case CODE::JSON_PARSE_FAIL:
        return "サーバーへの送信情報に誤りがあります[218]";
    case CODE::TRANSFER_CORD_LIMIT_FAIL:
        return "引き継ぎコードの期限が終了しています[219]";
    case CODE::NOT_FOUND_TRANSFER_CORD:
        return "引き継ぎコードが見つかりません[220]";
    case CODE::NOT_FOUND_PAGE:
        return "ページが見つかりません[221]";
    case CODE::RESET_MARATHON_ERROR:
        return "ユーザー登録に失敗しました[222]";
    case CODE::INPUT_NG_WORD:
        return "入力した文字にNGワードが含まれています[223]";
    case CODE::INPUT_IMAGE_FONT:
        return "入力した文字に絵文字が含まれています[224]";
    case CODE::FRIEND_MAX_HOLD_ERROR:
        return "フレンドが最大数に達しているため先ほどの操作は行えません[225]";
    case CODE::NOT_ENOUGH_HOLD_TOKEN:
        return "所持トークンが不足しています[226]";
    case CODE::NOT_ENOUGH_HOLD_COIN:
        return "所持コインが不足しています[227]";
    case CODE::NOT_ENOUGH_HOLD_FRIEND_POINT:
        return "所持フレンドポイントが不足しています[228]";
    case CODE::FILL_UP_GARAGE:
        return "ガレージが満杯です[229]";
    case CODE::COMPOSITION_FAIL:
        return "合成に失敗しました[230]";
    case CODE::NOT_FOUND_CHARACTER:
        return "キャラクターが見つかりません[231]";
    case CODE::ALREADY_INPUT_ENDED:
        return "既にその情報は送信済みです[232]";
    case CODE::CAN_NOT_SELL:
        return "売却する事が出来ませんでした[233]";
    case CODE::CAN_NOT_BUY:
        return "購入する事が出来ませんでした[234]";
    case CODE::CAN_NOT_BARTER:
        return "交換する事が出来ませんでした[235]";
    case CODE::MAX_GARAGE_SIZE:
        return "これ以上ガレージ拡張は行えません[236]";
    case CODE::SERVER_MAINTENANCE:
        break;
    case CODE::EVENT_LIMIT_FAIL:
        return "イベント期間が終了しています[238]";
    case CODE::NOT_ENOUGH_COST:
        return "コストが不足しています[239]";
    case CODE::GET_LIMIT_FAIL:
        return "受け取り期限が既に終了しています[240]";
    case CODE::NETWORK:
        return "ネットワーク接続エラー[250]";
    case CODE::GAME_SERVER_APC_REQUEST_ERROR:
        return "サーバーへの送信情報に誤りがあります[600]";
    case CODE::UNAVAILABLE_APC:
        return "現在サーバーを利用する事が出来ません[700]";
    case CODE::UNAVAILABLE_CACHED:
        return "現在サーバーを利用する事が出来ません[701]";
    case CODE::SAVE_ERROR:
        return "データの保存に失敗しました[800]";
    case CODE::DATA_BASE_ERROR:
        return "データの保存に失敗しました[801]";
    case CODE::OTHER_ERROR_DATA_BASE:
        return "不明なエラーが発生しました[900]";
    case CODE::OTHER_ERROR:
        return "不明なエラーが発生しました[901]";
    default:
        return "不明なエラーが発生しました";
    }
    return "";
}

const bool ERROR::isRecognizeErrors(const int code)
{
    using SyanagoAPI::API::ERROR;
    switch (code) {
    case CODE::NOT_POST:
    case CODE::NOT_ENOUGH_OR_WRONG_POST:
    case CODE::APP_VERSION_INCONSISTENCY:
    case CODE::VALIDATION_FAIL:
    case CODE::NOT_FOUND_PLAYER:
    case CODE::WRONG_PLAYER:
    case CODE::DELETE_PLAYER:
    case CODE::NOT_ENOUGH_LOG_DATA:
    case CODE::NOT_FOUND_DATA_IN_APC:
    case CODE::NOT_FOUND_DATA_IN_CACHED:
    case CODE::CONTROLLER_NAME_MISTAKE:
    case CODE::USER_DATA_REGISTER_FAIL:
    case CODE::USER_DATA_REGISTER_FAIL_SHARD:
    case CODE::USER_DATA_CHANGE_FAIL:
    case CODE::USER_DATA_CHANGE_FAIL_SHARD:
    case CODE::DUPLICATION_LOGIN:
    case CODE::DEVICE_TIME_MISTAKE:
    case CODE::JSON_PARSE_FAIL:
    case CODE::TRANSFER_CORD_LIMIT_FAIL:
    case CODE::NOT_FOUND_TRANSFER_CORD:
    case CODE::NOT_FOUND_PAGE:
    case CODE::RESET_MARATHON_ERROR:
    case CODE::INPUT_NG_WORD:
    case CODE::INPUT_IMAGE_FONT:
    case CODE::FRIEND_MAX_HOLD_ERROR:
    case CODE::NOT_ENOUGH_HOLD_TOKEN:
    case CODE::NOT_ENOUGH_HOLD_COIN:
    case CODE::NOT_ENOUGH_HOLD_FRIEND_POINT:
    case CODE::FILL_UP_GARAGE:
    case CODE::COMPOSITION_FAIL:
    case CODE::NOT_FOUND_CHARACTER:
    case CODE::ALREADY_INPUT_ENDED:
    case CODE::CAN_NOT_SELL:
    case CODE::CAN_NOT_BUY:
    case CODE::CAN_NOT_BARTER:
    case CODE::MAX_GARAGE_SIZE:
    case CODE::SERVER_MAINTENANCE:
    case CODE::EVENT_LIMIT_FAIL:
    case CODE::NOT_ENOUGH_COST:
    case CODE::GET_LIMIT_FAIL:
    case CODE::NETWORK:
    case CODE::GAME_SERVER_APC_REQUEST_ERROR:
    case CODE::UNAVAILABLE_APC:
    case CODE::UNAVAILABLE_CACHED:
    case CODE::SAVE_ERROR:
    case CODE::DATA_BASE_ERROR:
    case CODE::OTHER_ERROR_DATA_BASE:
    case CODE::OTHER_ERROR:
        return true;
    default:
        return false;
    }
}