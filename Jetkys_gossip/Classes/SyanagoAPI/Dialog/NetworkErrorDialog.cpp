#include "NetworkErrorDialog.h"
#include "FontDefines.h"
#include "SoundHelper.h"
#include "SplashScene.h"
#include "jCommon.h"
#include "DialogDefine.h"

NetworkErrorDialog* NetworkErrorDialog::show(const std::string title, const std::string message, const int tag, const ProgressAbstract::RetryRequest& retryRequest)
{
    auto dialog = new NetworkErrorDialog();
    dialog->init(title, message, tag, retryRequest);
    dialog->autorelease();
    return dialog;
}

bool NetworkErrorDialog::init(const std::string title, const std::string message, const int tag, const ProgressAbstract::RetryRequest& retryRequest)
{
    if (!LayerPriority::init()) {
        return false;
    }
    _tag = tag;
    _retryRequest = retryRequest;

    Size dialogBaseSize = showDialogBase();
    Label* titleLabel = showTitleLabel(title, dialogBaseSize);
    showMessageLabel(message, titleLabel);
    showReconnectButton(createReconnectButton(), dialogBaseSize);

    Size winSize = Director::getInstance()->getWinSize();
    setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    Director::getInstance()->getRunningScene()->addChild(this, 1001);
    return true;
}

const Size NetworkErrorDialog::showDialogBase()
{
    Sprite* dialogBase = Sprite::create("dialog_base.png");
    dialogBase->setPosition(Point::ZERO);
    addChild(dialogBase, 0);
    return dialogBase->getContentSize();
}

Label* NetworkErrorDialog::showTitleLabel(const std::string title, const Size dialogBaseSize)
{
    Label* result = Label::createWithTTF(title, FONT_NAME_2, 36);
    result->setDimensions(500, 0);
    result->setPosition(Point(0, dialogBaseSize.height / 2 - 30));
    result->setHorizontalAlignment(TextHAlignment::CENTER);
    result->setAnchorPoint(Vec2(0.5, 1.0f));
    result->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->setColor(COLOR_YELLOW);
    addChild(result, 1);
    return result;
}

void NetworkErrorDialog::showMessageLabel(const std::string message, Label* titleLabel)
{
    Label* label = Label::createWithTTF(message, FONT_NAME_2, 28);
    label->setDimensions(500, 0);
    label->setPosition(Point(0, titleLabel->getPosition().y - titleLabel->getContentSize().height));
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    label->setAnchorPoint(Vec2(0.5, 1.0f));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(label, 1);
}

void NetworkErrorDialog::showReconnectButton(cocos2d::MenuItemSprite* reconnectButton, cocos2d::Size dialogBaseSize)
{
    auto menu = MenuPriority::create();
    menu->setPosition(Point(0, -dialogBaseSize.height / 2 + reconnectButton->getContentSize().height / 2 + 10));
    menu->addChild(reconnectButton);
    addChild(menu, 1);
}

MenuItemSprite* NetworkErrorDialog::createReconnectButton()
{
    MenuItemSprite* result = makeMenuItem("te_popup_button.png", CC_CALLBACK_1(NetworkErrorDialog::menuButtonCallback, this));
    result->setPosition(Point::ZERO + Vec2(0, 30));
    result->addChild(createReconnectLabel(result->getContentSize()));
    return result;
}

Label* NetworkErrorDialog::createReconnectLabel(const Size& reconnectButtonSize)
{
    Label* result = Label::createWithTTF(_tag == ERROR_CODE_NETWORK ? "再接続" : "OK", FONT_NAME_2, 36);
    result->setPosition(Point(reconnectButtonSize.width / 2, reconnectButtonSize.height / 2 - 18));
    result->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    return result;
}

void NetworkErrorDialog::menuButtonCallback(cocos2d::Ref* pSender)
{
    SoundHelper* soundManeger;
    soundManeger->playeMainSceneEffect(SOUND_TAP_TRUE, false);
    if (_retryRequest && _tag == ERROR_CODE_NETWORK) {
        _retryRequest();
    } else if (_tag == ERROR_SERVER_MAINTENANCE) {
        transitScene(SplashScene::createScene());
    }
    removeFromParent();
}