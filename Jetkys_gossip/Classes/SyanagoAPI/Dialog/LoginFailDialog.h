#ifndef __syanago__LoginFailDialog__
#define __syanago__LoginFailDialog__

#include "cocos2d.h"
#include "LayerPriority.h"
#include "ProgressAbstract.h"

USING_NS_CC;
namespace SyanagoAPI {
    
class LoginFailDialog : public LayerPriority {
public:
    static LoginFailDialog* show(const std::string title);
    
    void menuButtonCallback(Ref *pSender);
    
private:
    LoginFailDialog()
    {
        
    }
    enum BUTTON_CALL_BACK{
        CALL_BACK_YES = 0,
        CALL_BACK_NO
    };
    bool init(const std::string title);
    
    const Size showDialogBase();
    Label* showTitleLabel(const std::string title, const Size dialogBaseSize);
    void showMessageLabel(Label* titleLabel);
    void showButton(MenuItemSprite* yesButton, MenuItemSprite* noButton, Size dialogBaseSize);
    
    MenuItemSprite* createYesButton();
    MenuItemSprite* createNoButton();
    Label* createLabel(std::string labelstring, const Size& reconnectButtonSize);
    
    
};
    
};
#endif /* defined(__syanago__LoginFailDialog__) */
