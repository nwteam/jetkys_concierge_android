#include "LoginFailDialog.h"
#include "FontDefines.h"
#include "SoundHelper.h"
#include "RegisterScene.h"
#include "jCommon.h"

LoginFailDialog* LoginFailDialog::show(const std::string title)
{
    auto dialog = new LoginFailDialog();
    dialog->init(title);
    dialog->autorelease();
    return dialog;
}

bool LoginFailDialog::init(const std::string title)
{
    if (!LayerPriority::init()) {
        return false;
    }

    Size dialogBaseSize = showDialogBase();
    Label* titleLabel = showTitleLabel(title, dialogBaseSize);
    showMessageLabel(titleLabel);
    showButton(createYesButton(), createNoButton(), dialogBaseSize);

    Size winSize = Director::getInstance()->getWinSize();
    setPosition(Vec2(winSize.width / 2, winSize.height / 2));
    Director::getInstance()->getRunningScene()->addChild(this, 1001);
    return true;
}

const Size LoginFailDialog::showDialogBase()
{
    Sprite* dialogBase = Sprite::create("dialog_base.png");
    dialogBase->setPosition(Point::ZERO);
    addChild(dialogBase, 0);
    return dialogBase->getContentSize();
}

Label* LoginFailDialog::showTitleLabel(const std::string title, const Size dialogBaseSize)
{
    Label* result = Label::createWithTTF(title, FONT_NAME_2, 28);
    result->setDimensions(500, 0);
    result->setPosition(Point(0, dialogBaseSize.height / 2 - 30));
    result->setHorizontalAlignment(TextHAlignment::CENTER);
    result->setAnchorPoint(Vec2(0.5, 1.0f));
    result->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    result->setColor(COLOR_YELLOW);
    addChild(result, 1);
    return result;
}

void LoginFailDialog::showMessageLabel(Label* titleLabel)
{
    Label* label = Label::createWithTTF("新規登録をしてよろしいですか？", FONT_NAME_2, 28);
    label->setDimensions(500, 0);
    label->setPosition(Point(0, titleLabel->getPosition().y - titleLabel->getContentSize().height - 30));
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    label->setAnchorPoint(Vec2(0.5, 1.0f));
    label->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    addChild(label, 1);
}

void LoginFailDialog::showButton(MenuItemSprite* yesButton, MenuItemSprite* noButton, Size dialogBaseSize)
{
    auto menu = MenuPriority::create();
    menu->setPosition(Point(0, -dialogBaseSize.height / 2 + yesButton->getContentSize().height / 2 + 10));
    menu->addChild(yesButton);
    menu->addChild(noButton);
    addChild(menu, 1);
}

MenuItemSprite* LoginFailDialog::createYesButton()
{
    MenuItemSprite* result = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(LoginFailDialog::menuButtonCallback, this));
    result->setPosition(Point::ZERO + Vec2(-result->getContentSize().width / 2 - 15, 30));
    result->addChild(createLabel("はい", result->getContentSize()));
    result->setTag(BUTTON_CALL_BACK::CALL_BACK_YES);
    return result;
}

MenuItemSprite* LoginFailDialog::createNoButton()
{
    MenuItemSprite* result = makeMenuItem("gacha_dialog_button.png", CC_CALLBACK_1(LoginFailDialog::menuButtonCallback, this));
    result->setPosition(Point::ZERO + Vec2(result->getContentSize().width / 2 + 15, 30));
    result->addChild(createLabel("いいえ", result->getContentSize()));
    result->setTag(BUTTON_CALL_BACK::CALL_BACK_NO);
    return result;
}

Label* LoginFailDialog::createLabel(std::string labelstring, const Size& reconnectButtonSize)
{
    Label* result = Label::createWithTTF(labelstring, FONT_NAME_2, 36);
    result->setPosition(Point(reconnectButtonSize.width / 2, reconnectButtonSize.height / 2 - 18));
    result->enableShadow(Color4B(Color3B::BLACK), Size(2, -2));
    return result;
}

void LoginFailDialog::menuButtonCallback(cocos2d::Ref* pSender)
{
    int aIndex = ((Node*)pSender)->getTag();
    SoundHelper* soundManeger;
    soundManeger->playeMainSceneEffect(SOUND_TAP_TRUE, false);

    if (aIndex == BUTTON_CALL_BACK::CALL_BACK_YES) {
        transitScene(RegisterScene::createScene());
    }
    removeFromParent();
}
