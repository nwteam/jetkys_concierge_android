#ifndef __syanago__NetworkErrorDialog__
#define __syanago__NetworkErrorDialog__

#include "cocos2d.h"
#include "LayerPriority.h"
#include "ProgressAbstract.h"

USING_NS_CC;
using namespace SyanagoAPI;

namespace SyanagoAPI {
    
class NetworkErrorDialog : public LayerPriority {
public:
    static NetworkErrorDialog* show(const std::string title, const std::string message, const int tag, const ProgressAbstract::RetryRequest& retryRequest);
    
    void menuButtonCallback(Ref *pSender);
    
private:
    NetworkErrorDialog(){}
    bool init(const std::string title, const std::string message, const int tag, const ProgressAbstract::RetryRequest& retryRequest);
    
    const Size showDialogBase();
    Label* showTitleLabel(const std::string title, const Size dialogBaseSize);
    void showMessageLabel(const std::string message, Label* titleLabel);
    void showReconnectButton(MenuItemSprite* reconnectButton, Size dialogBaseSize);
    
    MenuItemSprite* createReconnectButton();
    Label* createReconnectLabel(const Size& reconnectButtonSize);
    
    ProgressAbstract::RetryRequest _retryRequest;
    
    
};
    
};
#endif /* defined(__syanago__NetworkErrorDialog__) */
