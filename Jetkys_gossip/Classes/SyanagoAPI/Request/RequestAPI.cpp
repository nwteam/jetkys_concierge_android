#include "RequestAPI.h"
#include "API.h"
#include "Native.h"
#include "PlayerController.h"
#include "DataPlayer.h"
#include "LastModifiedConverter.h"

using namespace SyanagoAPI;

void RequestAPI::getVersion(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_VERSION, encryptQuery(API::QUERY::GET_VERSION));
}

void RequestAPI::createPlayer(std::string name, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    // copy const value to use StringUtils::format
    std::string format = API::QUERY::CREATE_PLAYER;
    std::string query = StringUtils::format(
        format.c_str(),
        name.c_str(),
        Native::getOSVersion().c_str(),
        Native::getPushToken().c_str(),
        "");
    post(API::URL::CREATE_PLAYER, encryptQuery(query));
}

void RequestAPI::transferPlayer(std::string transferCode, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::CREATE_PLAYER;
    std::string query = StringUtils::format(
        format.c_str(),
        "",
        Native::getOSVersion().c_str(),
        Native::getPushToken().c_str(),
        transferCode.c_str());
    post(API::URL::CREATE_PLAYER, encryptQuery(query));
}

void RequestAPI::auth(const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::AUTH;
    std::string query = StringUtils::format(
        format.c_str(),
        DataPlayer::getInstance()->getDID().c_str(),
        Native::getPushToken().c_str());
    post(API::URL::AUTH, encryptQueryWithPlayerID(query));
}

void RequestAPI::sync(const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::SYNC;
    std::string query = StringUtils::format(format.c_str(),
                                            LastModifiedConverter::getLastModified("areas").c_str(),
                                            LastModifiedConverter::getLastModified("birthplaces").c_str(),
                                            LastModifiedConverter::getLastModified("body_types").c_str(),
                                            LastModifiedConverter::getLastModified("characters").c_str(),
                                            LastModifiedConverter::getLastModified("courses").c_str(),
                                            LastModifiedConverter::getLastModified("grid_types").c_str(),
                                            LastModifiedConverter::getLastModified("item_units").c_str(),
                                            LastModifiedConverter::getLastModified("items").c_str(),
                                            LastModifiedConverter::getLastModified("leader_skills").c_str(),
                                            LastModifiedConverter::getLastModified("leader_skill_names").c_str(),
                                            LastModifiedConverter::getLastModified("skills").c_str(),
                                            LastModifiedConverter::getLastModified("skill_names").c_str(),
                                            LastModifiedConverter::getLastModified("levels").c_str(),
                                            LastModifiedConverter::getLastModified("makers").c_str(),
                                            LastModifiedConverter::getLastModified("maps").c_str(),
                                            LastModifiedConverter::getLastModified("parts").c_str(),
                                            LastModifiedConverter::getLastModified("ranks").c_str(),
                                            LastModifiedConverter::getLastModified("rarities").c_str(),
                                            LastModifiedConverter::getLastModified("system_settings").c_str(),
                                            LastModifiedConverter::getLastModified("tokens").c_str(),
                                            LastModifiedConverter::getLastModified("grid_operands").c_str()
                                            );
    post(API::URL::SYNC, encryptQueryWithPlayerID(query));
}

void RequestAPI::syncWord(const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::SYNC_WORD;
    std::string query = StringUtils::format(format.c_str(),
                                            LastModifiedConverter::getLastModified("words").c_str());
    post(API::URL::SYNC, encryptQueryWithPlayerID(query));
}

void RequestAPI::getInformations(const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_DIALOG, encryptQueryWithPlayerID(API::QUERY::GET_DIALOG));
}

void RequestAPI::getResourceVersions(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_RESOUCE_VERSIONS, encryptQueryWithPlayerID(API::QUERY::GET_RESOURCE_VERSIONS));
}

void RequestAPI::getPlayer(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::GET_PLAYER;
    std::string query = StringUtils::format(format.c_str(),
                                            DATAPLAYER->getPlayerID().c_str(),
                                            DataPlayer::getInstance()->getDID().c_str());
    post(API::URL::GET_PLAYER, encryptQueryWithPlayerID(query));
}

void RequestAPI::getRankingList(const std::string& eventName, int offset, int limit, const OnResponceCallback& callback, int playerId)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::GET_RANKING_LIST;
    std::string query = StringUtils::format(format.c_str(),
                                            eventName.c_str(),
                                            offset,
                                            limit,
                                            playerId
                                            );
    post(API::URL::GET_RANKING_LIST, encryptQueryWithPlayerID(query));
}

void RequestAPI::getPayGachaResult(const int numberOfExecutions, const int type, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::GET_PAY_GACHA_RESULT;
    std::string query = StringUtils::format(format.c_str(),
                                            numberOfExecutions,
                                            type
                                            );
    post(API::URL::GET_PAY_GACHA_RESULT, encryptQueryWithPlayerID(query));
}

void RequestAPI::getFreeGachaResult(const int numberOfExecutions, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::GET_FREE_GACHA_RESULT;
    std::string query = StringUtils::format(format.c_str(),
                                            numberOfExecutions
                                            );
    post(API::URL::GET_FREE_GACHA_RESULT, encryptQueryWithPlayerID(query));
}

void RequestAPI::getFuel(const int ownerRank, const int result, const OnResponceCallback& callback)
{
    const int targetId = NULL;
    const int kind = 200;
    const int code = 31;
    setReceiveCallback(callback);
    std::string format = API::QUERY::LOG_ACTION;
    std::string query = StringUtils::format(format.c_str(),
                                            ownerRank,
                                            targetId,
                                            kind,
                                            code,
                                            result,
                                            DATAPLAYER->getDateTime().c_str());
    post(API::URL::LOG_ACTION, encryptQueryWithPlayerID(query));
}

void RequestAPI::setFirstGacha(const int ownerRank, const OnResponceCallback& callback)
{
    const int targetId = NULL;
    const int kind = 100;
    const int code = 20;
    const int result = NULL;
    setReceiveCallback(callback);
    std::string format = API::QUERY::LOG_ACTION;
    std::string query = StringUtils::format(format.c_str(),
                                            ownerRank,
                                            targetId,
                                            kind,
                                            code,
                                            result,
                                            DATAPLAYER->getDateTime().c_str());
    post(API::URL::LOG_ACTION, encryptQueryWithPlayerID(query));
}

void RequestAPI::setRaceResult(const int courseId, const int rank, const int numberOfTurns, const int totalScore, const int numOfStops, const int numOfItem, const int numOfCharge, const int numOfGridToGoal, const int useNumOfSkills, const int helpPlayerId, const int rank1MstCharacterId, const int rank2MstCharacterId, const int rank3MstCharacterId, const int rank4MstCharacterId, const std::string stopGridIds, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::SET_RACE_RESULT;
    std::string query = StringUtils::format(format.c_str(),
                                            courseId,
                                            rank,
                                            numberOfTurns,
                                            totalScore,
                                            numOfStops,
                                            numOfItem,
                                            numOfCharge,
                                            numOfGridToGoal,
                                            useNumOfSkills,
                                            helpPlayerId,
                                            rank1MstCharacterId,
                                            rank2MstCharacterId,
                                            rank3MstCharacterId,
                                            rank4MstCharacterId,
                                            stopGridIds.c_str());
    post(API::URL::SET_RACE_RESULT, encryptQueryWithPlayerID(query));
}

void RequestAPI::setTryRace(const int ownerRank, const int result, const OnResponceCallback& callback)
{
    const int targetId = NULL;
    const int kind = 100;
    const int code = 40;
    setReceiveCallback(callback);
    std::string format = API::QUERY::LOG_ACTION;
    std::string query = StringUtils::format(format.c_str(),
                                            ownerRank,
                                            targetId,
                                            kind,
                                            code,
                                            result,
                                            DATAPLAYER->getDateTime().c_str());
    post(API::URL::LOG_ACTION, encryptQueryWithPlayerID(query));
}

void RequestAPI::saleCharacter(std::map<int, std::string>saleCharacterIds, const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    std::string idsStr = "";
    for (std::map<int, std::string>::iterator it = saleCharacterIds.begin(); it != saleCharacterIds.end(); ++it) {
        if (idsStr != "") {
            idsStr += ",";
        }
        idsStr += StringUtils::format("\"%d\"", it->first);
    }

    std::string format = API::QUERY::SALE_CHARACTER;
    std::string query = StringUtils::format(format.c_str(),
                                            idsStr.c_str());

    post(API::URL::SALE_CHARACTER, encryptQueryWithPlayerID(query));
}

void RequestAPI::saleParts(std::map<int, std::string>salePartsIds, const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    std::string idsStr = "";
    for (std::map<int, std::string>::iterator it = salePartsIds.begin(); it != salePartsIds.end(); ++it) {
        if (idsStr != "") {
            idsStr += ",";
        }
        idsStr += StringUtils::format("\"%d\"", it->first);
    }

    std::string format = API::QUERY::SALE_PARTS;
    std::string query = StringUtils::format(format.c_str(),
                                            idsStr.c_str());

    post(API::URL::SALE_PARTS, encryptQueryWithPlayerID(query));
}

void RequestAPI::getRaceCourse(const int courseId, const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::GET_RACE_COURSE;
    std::string query = StringUtils::format(format.c_str(),
                                            courseId);
    post(API::URL::GET_RACE_COURSE, encryptQueryWithPlayerID(query));
}

void RequestAPI::getPayTokenItem(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_PAY_ITEM, encryptQueryWithPlayerID(API::QUERY::GET_PAY_ITEM));
}

void RequestAPI::checkAppStoreReceipt(const OnResponceCallback& callback, const int ownerRank,  const int itemId, const int quantity, const int quantityFree, const int singlePrice, const int amountCharged, const std::string transactionId, const std::string clientIp, const std::string receipt)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::CHECK_APPSTORE_RECEIPT;
    std::string query = StringUtils::format(format.c_str(),
                                            ownerRank,
                                            itemId,
                                            quantity,
                                            quantityFree,
                                            singlePrice,
                                            amountCharged,
                                            transactionId.c_str(),
                                            clientIp.c_str(),
                                            receipt.c_str());
    postInAppPurchase(API::URL::CHECK_APPSTORE_RECEIPT, encryptQueryWithPlayerID(query));
}

void RequestAPI::checkGooglePlayReceipt(const OnResponceCallback& callback, const int ownerRank, const int itemId, const int quantity, const int quantityFree, const int amount, const int place, const std::string inappPurchaseData, const std::string inappDataSignature)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::LOG_GOOGLEPLAY_PURCHASE_WITH_RECEIPT;
    std::string query = StringUtils::format(format.c_str(),
                                            ownerRank,
                                            itemId,
                                            quantity,
                                            quantityFree,
                                            amount,
                                            place,
                                            inappPurchaseData.c_str(),
                                            inappDataSignature.c_str());
    postInAppPurchase(API::URL::LOG_GOOGLEPLAY_PURCHASE, encryptQueryWithPlayerID(query));
}

void RequestAPI::lockCharacter(const int playerCharactersModelId, const bool flgLock, const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::LOCK_CHARACTER;
    std::string query = StringUtils::format(format.c_str(),
                                            playerCharactersModelId,
                                            flgLock);
    post(API::URL::LOCK_CHARACTER, encryptQueryWithPlayerID(query));
}

void RequestAPI::buyTurns(const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    post(API::URL::BUY_TURNS, encryptQueryWithPlayerID(API::QUERY::BUY_TURNS));
}

void RequestAPI::setGameStep(const int ownerRank, const int gameStep, const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::LOG_ACTION;
    std::string query = StringUtils::format(format.c_str(), ownerRank, 0, 100, gameStep, 0, DATAPLAYER->getDateTime().c_str());
    post(API::URL::LOG_ACTION, encryptQueryWithPlayerID(query));
}


void RequestAPI::getMissionList(const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_MISSION_LIST, encryptQueryWithPlayerID(API::QUERY::GET_MISSION_LIST));
}

void RequestAPI::setFriendRequest(const int helpPlayerId, const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::SET_FRIEND_REQUEST;
    std::string query = StringUtils::format(format.c_str(), helpPlayerId);
    post(API::URL::SET_FRIEND_REQUEST, encryptQueryWithPlayerID(query));
}

void RequestAPI::sendFuelRequest(const int ownerRank, const int fuel, const OnResponceCallback &callback)
{
    setReceiveCallback(callback);
    const int targetId = NULL;
    const int kind = 100;
    const int code = 30;
    std::string format = API::QUERY::LOG_ACTION;
    std::string query = StringUtils::format(format.c_str(),
                                            ownerRank,
                                            targetId,
                                            kind,
                                            code,
                                            fuel,
                                            DATAPLAYER->getDateTime().c_str());
    post(API::URL::LOG_ACTION, encryptQueryWithPlayerID(query));
}

void RequestAPI::getPresentList(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_COMPENSATION, encryptQueryWithPlayerID(API::QUERY::GET_COMPENSATION));
}

void RequestAPI::setPresentStatus(const int presentId, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::SET_COMPENSATION_STATUS;
    std::string query = StringUtils::format(format.c_str(),
                                            presentId,
                                            4);
    post(API::URL::SET_COMPENSATION_STATUS, encryptQueryWithPlayerID(query));
}

void RequestAPI::awakeCharacter(const int playerCharacterId, const std::string materialPlayerCharacterIds, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::AWAKE_CHARACTER;
    std::string query = StringUtils::format(format.c_str(),
                                            playerCharacterId,
                                            materialPlayerCharacterIds.c_str());
    post(API::URL::AWAKE_CHARACTER, encryptQueryWithPlayerID(query));
}

void RequestAPI::setSyntheticRequest(const int basePlayerCharacterId, const std::string materialPlayerCharacterIds, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::SET_SYNTHETIC_REQUEST;
    std::string query = StringUtils::format(format.c_str(),
                                            basePlayerCharacterId,
                                            materialPlayerCharacterIds.c_str());
    post(API::URL::SET_SYNTHETIC_REQUEST, encryptQueryWithPlayerID(query));
}

void RequestAPI::setCustomCharacter(const int playerCharacterId, const int partsSlot1OfPlayerPartsId, const int partsSlot2OfPlayerPartsId, const int diceSlot7OfNumber, const int diceSlot8OfNumber, const int diceSlot9OfNumber, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::SET_CUSTOM;
    std::string query = StringUtils::format(format.c_str(),
                                            playerCharacterId,
                                            partsSlot1OfPlayerPartsId > 0 ? StringUtils::format("%d", partsSlot1OfPlayerPartsId).c_str() : "null",
                                            partsSlot2OfPlayerPartsId > 0 ? StringUtils::format("%d", partsSlot2OfPlayerPartsId).c_str() : "null",
                                            diceSlot7OfNumber > 0 ? StringUtils::format("%d", diceSlot7OfNumber).c_str() : "null",
                                            diceSlot8OfNumber > 0 ? StringUtils::format("%d", diceSlot8OfNumber).c_str() : "null",
                                            diceSlot9OfNumber > 0 ? StringUtils::format("%d", diceSlot9OfNumber).c_str() : "null");
    post(API::URL::SET_CUSTOM, encryptQueryWithPlayerID(query));
}

void RequestAPI::addCharacter(const int masterCharacterId, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::ADD_CHARACTER;
    std::string query = StringUtils::format(format.c_str(), StringUtils::format("{\"id\":%d,\"learder_flag\":true}", masterCharacterId).c_str());
    post(API::URL::ADD_CHARACTER, encryptQueryWithPlayerID(query));
}

void RequestAPI::getFriendList(const int status, const std::string contactCode, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::GET_FRIEND_LIST;
    std::string statusCode = "";
    if (status == 0) {
        statusCode = "null";
    } else {
        statusCode = StringUtils::format("%d", status);
    }
    std::string query = StringUtils::format(format.c_str(), statusCode.c_str(), contactCode.c_str());
    post(API::URL::GET_FRIEND_LIST, encryptQueryWithPlayerID(query));
}

void RequestAPI::approveFriends(const int status, const int playerId, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::APPROVE_FRIENDS;
    std::string query = StringUtils::format(format.c_str(), status, playerId);
    post(API::URL::APPROVE_FRIENDS, encryptQueryWithPlayerID(query));
}

void RequestAPI::getInvitationReward(const std::string contactCode, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::GET_INVITATION_REWARD;
    std::string query = StringUtils::format(format.c_str(), contactCode.c_str());
    post(API::URL::GET_INVITATION_REWARD, encryptQueryWithPlayerID(query));
}

void RequestAPI::getDialog(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_DIALOG, encryptQueryWithPlayerID(API::QUERY::GET_DIALOG));
}

void RequestAPI::getPictureBook(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_PICTURE_BOOK, encryptQueryWithPlayerID(API::QUERY::GET_PICTURE_BOOK));
}

void RequestAPI::getNotice(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_NOTICE, encryptQueryWithPlayerID(API::QUERY::GET_NOTICE));
}

void RequestAPI::setNoticeStatus(const int noticeId, const int status, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::SET_NOTICE_STATUS;
    std::string query = StringUtils::format(format.c_str(),
                                            noticeId,
                                            status);
    post(API::URL::SET_NOTICE_STATUS, encryptQueryWithPlayerID(query));
}

void RequestAPI::getTresureList(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_TRESURE_LIST, encryptQueryWithPlayerID(API::QUERY::GET_TRESURE_LIST));
}

void RequestAPI::getCoinExchangeList(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_COIN_EXCHANGE_LIST, encryptQueryWithPlayerID(API::QUERY::GET_COIN_EXCHANGE_LIST));
}

void RequestAPI::exchangeIntoCoin(const int coinId, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::EXCHANGE_INTO_COIN;
    std::string query = StringUtils::format(format.c_str(), coinId);
    post(API::URL::EXCHANGE_INTO_COIN, encryptQueryWithPlayerID(query));
}

void RequestAPI::extendGarage(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::EXTEND_GARAGE, encryptQueryWithPlayerID(API::QUERY::EXTEND_GARAGE));
}

void RequestAPI::buyFuel(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::BUY_FUEL, encryptQueryWithPlayerID(API::QUERY::BUY_FUEL));
}

void RequestAPI::getPartsList(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_PARTS_LIST, encryptQueryWithPlayerID(API::QUERY::GET_PARTS_LIST));
}

void RequestAPI::buyParts(const int masterPartsId, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::BUY_PARTS;
    std::string query = StringUtils::format(format.c_str(),
                                            masterPartsId);
    post(API::URL::BUY_PARTS, encryptQueryWithPlayerID(query));
}

void RequestAPI::setTeamComposition(const std::string teamDataString, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::SET_TEAM_COMPOSITION;
    std::string query = StringUtils::format(format.c_str(), teamDataString.c_str());
    post(API::URL::SET_TEAM_COMPOSITION, encryptQueryWithPlayerID(query));
}

void RequestAPI::getConsumptionItem(const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    post(API::URL::GET_CONSUMPTION_ITEM, encryptQueryWithPlayerID(API::QUERY::GET_CONSUMPTION_ITEM));
}

void RequestAPI::useTicket(const int playerTicketId, const OnResponceCallback& callback)
{
    setReceiveCallback(callback);
    std::string format = API::QUERY::USE_TICKET;
    std::string query = StringUtils::format(format.c_str(),
                                            playerTicketId);
    post(API::URL::USE_TICKET, encryptQueryWithPlayerID(query));
}

void RequestAPI::getFAQ(const OnResponceHtmlCallback& callback)
{
    std::string query = API::QUERY::FAQ;
    post(API::URL::FAQ, encryptQueryWithPlayerID(query), callback);
}

void RequestAPI::getTakeoverPasswordForm(const OnResponceHtmlCallback& callback)
{
    post(API::URL::TAKEOVER_PASSWORD, encryptQueryWithPlayerID(API::QUERY::TAKEOVER_PASSWORD), callback);
}

void RequestAPI::getPurchaseInformation(const OnResponceHtmlCallback &callback)
{
    post(API::URL::PURCHASE_INFORMATION, encryptQueryWithPlayerID(API::QUERY::PURCHASE_INFORMATION), callback);
}





















