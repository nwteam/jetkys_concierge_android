#include "Request.h"
#include "API.h"
#include "DataPlayer.h"
#include "SecurityUtil.h"
#include "SystemSettingModel.h"

using namespace SyanagoAPI;

void Request::setProgress(ProgressAbstract* progress)
{
    if (progress != nullptr) {
        delete progress;
    }
    progress = progress;
}

void Request::setReceiveCallback(const OnResponceCallback& callback)
{
    receiveCallback = callback;
}

void Request::get(const std::string& url, const std::string& param)
{
    request(HttpRequest::Type::GET, url, param);
}

void Request::post(const std::string& url, const std::string& param)
{
    request(HttpRequest::Type::POST, url, param);
}

void Request::post(const std::string& url, const std::string& param, const OnResponceHtmlCallback& callback)
{
    CC_SAFE_RELEASE_NULL(httpRequest);
    httpRequest = createHttpRequest(UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DOMAIN_URL) + url, param);
    httpRequest->setRequestType(HttpRequest::Type::POST);
    httpRequest->setResponseCallback([this, callback](HttpClient* sender, HttpResponse* response) {
        progress->onEnd();
        using namespace SyanagoAPI::API;
        if (!response || !response->isSucceed()) {
            progress->onError(ERROR::CODE::NETWORK, ERROR::getMessage(ERROR::CODE::NETWORK), CC_CALLBACK_0(Request::retry, this));
            return;
        }
        std::vector<char>* responseChar = response->getResponseData();
        callback(std::string(responseChar->begin(), responseChar->end()));
    });
    HttpClient::getInstance()->send(httpRequest);
    CCLOG("[Request::request] url: %s", httpRequest->getUrl());
}

void Request::postInAppPurchase(const std::string& url, const std::string& param)
{
    requestInAppPurchase(HttpRequest::Type::POST, url, param);
}

void Request::request(const HttpRequest::Type type, const std::string& url, const std::string& param)
{
    CC_SAFE_RELEASE_NULL(httpRequest);
    httpRequest = createHttpRequest(UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DOMAIN_URL) + url, param);
    httpRequest->setRequestType(type);
    httpRequest->setResponseCallback(CC_CALLBACK_2(Request::onReceive, this));
    HttpClient* client = HttpClient::getInstance();
    client->setTimeoutForConnect(10);
    client->setTimeoutForRead(15);
    client->send(httpRequest);
    CCLOG("[Request::request] url: %s", httpRequest->getUrl());
}

void Request::requestInAppPurchase(const HttpRequest::Type type, const std::string& url, const std::string& param)
{
    CC_SAFE_RELEASE_NULL(httpRequest);
    std::shared_ptr<SystemSettingModel>systemModel = SystemSettingModel::getModel();
    if (systemModel->getRuleServerUrl() != "") {
        httpRequest = createHttpRequest(systemModel->getRuleServerUrl() + url, param);
    } else {
        httpRequest = createHttpRequest(UserDefault::getInstance()->getStringForKey(UserDefaultManager::KEY::DOMAIN_URL) + url, param);
    }
    httpRequest->setRequestType(type);
    httpRequest->setResponseCallback(CC_CALLBACK_2(Request::onReceive, this));
    HttpClient::getInstance()->send(httpRequest);
    CCLOG("[Request::request] url: %s, query: %s", httpRequest->getUrl(), httpRequest->getRequestData());
}

void Request::retry()
{
    HttpClient::getInstance()->send(httpRequest);
}

void Request::onReceive(HttpClient* sender, HttpResponse* response)
{
    progress->onEnd();
    using namespace SyanagoAPI::API;
    if (!response || !response->isSucceed()) {
        progress->onError(ERROR::CODE::NETWORK, ERROR::getMessage(ERROR::CODE::NETWORK), CC_CALLBACK_0(Request::retry, this));
        return;
    }

    Json* json = Request::convertHttpResponseToJson(response);
    if (json == NULL || json->type != Json_Object) {
        progress->onError(0, "responce形式がJsonではありません", CC_CALLBACK_0(Request::retry, this));
        return;
    }
    Json* error = Json_getItem(json, "error");
    if (error != NULL &&
        error->type == Json_Object) {
        progress->onError(Json_getInt(error, "code", 0), Json_getString(error, "message", ""), CC_CALLBACK_0(Request::retry, this));
        return;
    }

    std::string errorMessage = checkJsonFormat(json);
    if (errorMessage != "") {
        progress->onError(ERROR::CODE::NETWORK, errorMessage, CC_CALLBACK_0(Request::retry, this));
        return;
    }
    Json* decryptedResponse = decryptResponse(json);
    if (strncmp("false", Json_getString(decryptedResponse, "result", "false"), 5)) {
        progress->onError(ERROR::CODE::NETWORK, "サーバーエラー", CC_CALLBACK_0(Request::retry, this));
    }
    receiveCallback(decryptedResponse);
    Json_dispose(json);
}
std::string Request::checkJsonFormat(Json* json)
{
    // TODO::return errorMessage
    std::string errorMessage = "";
    if (json == NULL) {
        return "json is null";
    }
    if (json->type != Json_Object) {
        return "json is not a Json_Object";
    }

    std::string data = Json_getString(json, "data", "");
    if (data == "") {
        return "data is not set";
    }
    std::string iv = Json_getString(json, "iv", "");
    if (iv == "") {
        return "iv is not set";
    }
    std::string decryptedData = SecurityUtil::decrypt(data, iv);
    if (decryptedData == "") {
        return "decrypt failed, check your data and iv";
    }
    return "";
}

Json* Request::decryptResponse(Json* json)
{
    std::string data = Json_getString(json, "data", "");
    std::string iv = Json_getString(json, "iv", "");
    const std::string resonse = SecurityUtil::decrypt(data, iv);
    CCLOG("[Request::decryptResponse] resonse: %s", resonse.c_str());
    return Json_create(resonse.c_str());
}

std::string Request::encryptQuery(std::string query)
{
    std::string FORMAT = "did=%s&iv=%s&token=%s&datetime=%s&request_no=%d&device_type=%d&data=%s";
    const std::string IV = DataPlayer::getInstance()->getIV();
    const std::string DID = DataPlayer::getInstance()->getDID();
    const std::string DATE_TIME = DATAPLAYER->getDateTime().c_str();
    const int REQUEST_NO = 0;
    CCLOG("[Request::encryptQuery] query : %s", query.c_str());
    std::string result = StringUtils::format(FORMAT.c_str(),
                                             SecurityUtil::encrypt(DID, IV).c_str(),
                                             IV.c_str(),
                                             DATAPLAYER->getDIDToken(DID.c_str(), DATE_TIME).c_str(),
                                             DATE_TIME.c_str(),
                                             REQUEST_NO,
                                             API::DEVICE_TYPE,
                                             SecurityUtil::encrypt(query, IV).c_str());
    return result;
}

std::string Request::encryptQueryWithPlayerID(std::string query)
{
    std::string FORMAT = "id=%s&iv=%s&token=%s&datetime=%s&request_no=%d&device_type=%d&data=%s";
    const std::string IV = DataPlayer::getInstance()->getIV();
    const std::string DATE_TIME = DATAPLAYER->getDateTime().c_str();
    const int REQUEST_NO = 0;

    CCLOG("[Request::encryptQueryWithPlayerID] query : %s", query.c_str());
    std::string result = StringUtils::format(FORMAT.c_str(),
                                             SecurityUtil::encrypt(DATAPLAYER->getPlayerID().c_str(), IV).c_str(),
                                             IV.c_str(),
                                             DATAPLAYER->getPlayerIDToken(DATE_TIME).c_str(),
                                             DATE_TIME.c_str(),
                                             REQUEST_NO,
                                             API::DEVICE_TYPE,
                                             SecurityUtil::encrypt(query, IV).c_str());
    return result;
}


Json* Request::convertHttpResponseToJson(HttpResponse* response)
{
    std::vector<char>* buffer = response->getResponseData();
    std::string responseStr(buffer->begin(), buffer->end());
    return Json_create(responseStr.c_str());
}


HttpRequest* Request::createHttpRequest(const std::string url, const std::string postData)
{
    HttpRequest* result = new HttpRequest();
    std::vector<std::string>headers;
    headers.push_back("Authorization: Basic c3lhbmFnbzpGd3VIdWhnZldVS1g=");

    result->setHeaders(headers);
    result->setUrl(url.c_str());
    result->setRequestData(postData.c_str(), postData.length());
    return result;
}
