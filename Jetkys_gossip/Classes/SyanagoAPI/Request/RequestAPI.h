#ifndef __syanago__RequestAPI__
#define __syanago__RequestAPI__

#include "Request.h"

namespace SyanagoAPI {

class RequestAPI : public Request {
public:
    RequestAPI():Request(){}
    RequestAPI(ProgressAbstract* aProgress):Request(aProgress){}
    
    void getVersion(const OnResponceCallback& callback);
    void createPlayer(std::string name, const OnResponceCallback& callback);
    void transferPlayer(std::string transferCode, const OnResponceCallback& callback);
    void auth(const OnResponceCallback &callback);
    void sync(const OnResponceCallback& callback);
    void syncWord(const OnResponceCallback& callback);
    void getInformations(const OnResponceCallback& callback);
    void getResourceVersions(const OnResponceCallback& callback);
    void getPlayer(const OnResponceCallback& callback);
    void getRankingList(const std::string& eventName, int offset, int limit, const OnResponceCallback& callback, int playerId = NULL);
    void getPayGachaResult(const int numberOfExecutions, const int type, const OnResponceCallback& callback);
    void getFreeGachaResult(const int numberOfExecutions, const OnResponceCallback& callback);
    void getFuel(const int ownerRank, const int result, const OnResponceCallback& callback);
    void setFirstGacha(const int ownerRank, const OnResponceCallback& callback);
    void setRaceResult(const int courseId, const int rank, const int numberOfTurns, const int totalScore, const int numOfStops, const int numOfItem, const int numOfCharge, const int numOfGridToGoal, const int useNumOfSkills, const int helpPlayerId, const int rank1MstCharacterId, const int rank2MstCharacterId, const int rank3MstCharacterId, const int rank4MstCharacterId, const std::string stopGridIds, const OnResponceCallback& callback);
    void setTryRace(const int ownerRank, const int result, const OnResponceCallback& callback);
    void saleCharacter(std::map<int, std::string> saleCharacterIds, const OnResponceCallback& callback);
    void saleParts(std::map<int, std::string> salePartsIds, const OnResponceCallback& callback);
    void getRaceCourse(const int courseId, const OnResponceCallback &callback);
    void getPayTokenItem(const OnResponceCallback& callback);
    void checkAppStoreReceipt(const OnResponceCallback& callback, const int ownerRank, const int itemId, const int quantity, const int quantityFree, const int singlePrice, const int amountCharged, const std::string transactionId, const std::string clientIp, const std::string receipt);
    void checkGooglePlayReceipt(const OnResponceCallback& callback, const int ownerRank, const int itemId, const int quantity, const int quantityFree, const int amount, const int place, const std::string inapp_purchase_data, const std::string inapp_data_signature);
    void lockCharacter(const int playerCharactersModelId, const bool flgLock, const OnResponceCallback &callback);
    void buyTurns(const OnResponceCallback &callback);
    void setGameStep(const int ownerRank, const int gameStep,const OnResponceCallback &callback);
    void getMissionList(const OnResponceCallback &callback);
    void setFriendRequest(const int helpPlayerId, const OnResponceCallback &callback);
    void sendFuelRequest(const int ownerRank, const int fuel, const OnResponceCallback &callback);
    void getPresentList(const OnResponceCallback& callback);
    void setPresentStatus(const int presentId, const OnResponceCallback& callback);
    void awakeCharacter(const int playerCharacterId, const std::string materialPlayerCharacterIds, const OnResponceCallback& callback);
    void setSyntheticRequest(const int basePlayerCharacterId, const std::string materialPlayerCharacterIds, const OnResponceCallback& callback);
    void setCustomCharacter(const int playerCharacterId, const int partsSlot1OfPlayerPartsId, const int partsSlot2OfPlayerPartsId, const int diceSlot7OfNumber, const int diceSlot8OfNumber, const int diceSlot9OfNumber, const OnResponceCallback& callback);
    void addCharacter(const int masterCharacterId, const OnResponceCallback& callback);
    void getFriendList(const int status, const std::string contactCode, const OnResponceCallback& callback);
    void approveFriends(const int status, const int playerId, const OnResponceCallback& callback);
    void getInvitationReward(const std::string contactCode, const OnResponceCallback& callback);
    void getDialog(const OnResponceCallback& callback);
    void getPictureBook(const OnResponceCallback& callback);
    void getNotice(const OnResponceCallback& callback);
    void setNoticeStatus(const int noticeId, const int status, const OnResponceCallback& callback);
    void getTresureList(const OnResponceCallback& callback);
    void getCoinExchangeList(const OnResponceCallback& callback);
    void exchangeIntoCoin(const int coinId, const OnResponceCallback& callback);
    void extendGarage(const OnResponceCallback& callback);
    void buyFuel(const OnResponceCallback& callback);
    void getPartsList(const OnResponceCallback& callback);
    void buyParts(const int masterPartsId, const OnResponceCallback& callback);
    void setTeamComposition(const std::string teamDataString, const OnResponceCallback& callback);
    void getConsumptionItem(const OnResponceCallback& callback);
    void useTicket(const int playerTicketId, const OnResponceCallback& callback);
    
    void getFAQ(const OnResponceHtmlCallback& callback);
    void getTakeoverPasswordForm(const OnResponceHtmlCallback& callback);
    void getPurchaseInformation(const OnResponceHtmlCallback& callback);
};
};


#endif /* defined(__syanago__RequestAPI__) */
