#ifndef __syanago__Request__
#define __syanago__Request__

#include "cocos2d.h"
#include <functional>
#include <string>
#include "network/HttpClient.h"
#include "editor-support/spine/Json.h"
#include "Version.h"
#include <vector>
#include "ProgressAbstract.h"
#include "NullProgress.h"

USING_NS_CC;
using namespace cocos2d::network;

namespace SyanagoAPI {
class Request {
public:
    Request():
    httpRequest(nullptr),
    receiveCallback(nullptr)
    {
        progress = new NullProgress();
    }
    
    Request(ProgressAbstract* aProgress):
    httpRequest(nullptr),
    receiveCallback(nullptr)
    {
        progress = aProgress;
    }
    
    ~Request()
    {
        CC_SAFE_RELEASE_NULL(httpRequest);
    }
    
    typedef std::function<void(Json*)> OnResponceCallback;
    typedef std::function<void(std::string)> OnResponceHtmlCallback;
    
    static std::string encryptQuery(std::string query);
    static std::string encryptQueryWithPlayerID(std::string query);
    static Json* convertHttpResponseToJson(HttpResponse* response);
    static std::string checkJsonFormat(Json* json);
    static Json* decryptResponse(Json* json);
    
    void setProgress(ProgressAbstract* progress);
    void setReceiveCallback(const OnResponceCallback& callback);
    void get(const std::string& url, const std::string& param);
    void post(const std::string& url, const std::string& param);
    void post(const std::string& url, const std::string& param, const OnResponceHtmlCallback& callback);
    void postInAppPurchase(const std::string& url, const std::string& param);
    void request(const HttpRequest::Type type, const std::string& url, const std::string& param);
    void requestInAppPurchase(const HttpRequest::Type type, const std::string& url, const std::string& param);
    void retry();
    
    ProgressAbstract* progress;
    
private:
    
    HttpRequest* httpRequest;
    OnResponceCallback receiveCallback;
    
    HttpRequest* createHttpRequest(const std::string url, const std::string postData);
    void onReceive(network::HttpClient* sender, network::HttpResponse* response);
    
};
};


#endif /* defined(__syanago__Request__) */
