#ifndef __syanago__API__
#define __syanago__API__

#include <string>
#include "cocos2d.h"
#include "UserDefaultManager.h"

USING_NS_CC;

namespace SyanagoAPI {
    namespace API {
        static const int DEVICE_TYPE = (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) ? 1 : 2;
        namespace DOMAINS {
            //検収サーバー
            static const std::string TESTING = "http://210.140.174.195";;
            //開発サーバー
            static const std::string DEVELOPMENT = "http://210.140.172.107";
            //本番サーバー
            static const std::string PRODUCTION = "http://api.syanago.com:8000";
        };
        namespace URL {
            static const std::string GET_VERSION =  "/api/get_version";
            static const std::string CREATE_PLAYER = "/api/create_player";
            static const std::string AUTH = "/api/auth";
            static const std::string SYNC = "/api/sync";
            static const std::string GET_RESOUCE_VERSIONS = "/api/get_resource_versions";
            static const std::string ADD_CHARACTER  = "/api/add_character";
            static const std::string GET_DIALOG  = "/api/get_dialog";
            static const std::string GET_PLAYER  = "/api/get_player";
            static const std::string GET_RACE_COURSE  = "/api/get_race_course";
            static const std::string SET_RACE_RESULT  = "/api/set_race_results";

            //URL SHOP
            static const std::string GET_PAY_ITEM  = "/api/get_pay_item";
            static const std::string GET_COIN_EXCHANGE_LIST  = "/api/get_coin_exchange_list";
            static const std::string GET_PARTS_LIST  = "/api/get_parts_list";
            static const std::string EXCHANGE_INTO_COIN  = "/api/exchange_into_coin";
            static const std::string BUY_PARTS  = "/api/buy_parts";
            static const std::string EXTEND_GARAGE  = "/api/extend_garage";
            static const std::string BUY_FUEL  = "/api/buy_fuel";
            static const std::string BUY_TURNS = "/api/buy_turns";
            static const std::string CHECK_APPSTORE_RECEIPT = "/api/check_appstore_receipt";
            static const std::string LOG_ACTION  = "/api/log_action";

            //URL OTHERS
            static const std::string CAMPAIGN_CODE  = "/webview/campaign_code";
            static const std::string GET_PICTURE_BOOK  = "/api/get_picture_book";
            static const std::string GET_TRESURE_LIST  = "/api/get_treasure_list";
            static const std::string TAKEOVER_PASSWORD  = "/webview/takeover_password";
            static const std::string PURCHASE_INFORMATION  = "/webview/purchase_information";
            static const std::string GET_INVITATION_REWARD  = "/api/get_invitation_reward";
            static const std::string GET_NOTICE  = "/api/get_notice";
            static const std::string SET_NOTICE_STATUS  = "/api/set_notice_status";
            static const std::string GET_COMPENSATION  = "/api/get_compensation";
            static const std::string SET_COMPENSATION_STATUS  = "/api/set_compensation_status";
            static const std::string PLAYER_NAME  = "/webview/player_name";
            static const std::string FAQ  = "/webview/faq";

            //URL TEAM EDIT
            static const std::string SET_TEAM_COMPOSITION  = "/api/set_team_composition";
            static const std::string SALE_CHARACTER  = "/api/sale_character";
            static const std::string SALE_PARTS  = "/api/sale_parts";
            static const std::string SET_SYNTHETIC_REQUEST  = "/api/set_synthetic_request";
            static const std::string SET_CUSTOM  = "/api/set_custom";
            static const std::string LOCK_CHARACTER = "/api/lock_character";

            //URL GACHA
            static const std::string GET_PAY_GACHA_RESULT  = "/api/get_pay_gacha_result";
            static const std::string GET_FREE_GACHA_RESULT  = "/api/get_free_gacha_result";
            
            //URL_FRIEND
            static const std::string SET_FRIEND_REQUEST  = "/api/set_friend_request";
            
            //URL_FRIEND
            static const std::string GET_FRIEND_LIST  = "/api/get_friend_list";
            static const std::string APPROVE_FRIENDS  = "/api/approve_friends";

            //URL GOOGLE PLAY PURCHASE
            static const std::string LOG_GOOGLEPLAY_PURCHASE = "/api/log_googleplay_purchase";
            
            static const std::string GET_RANKING_LIST = "/api/get_ranking_list";
            static const std::string GET_MISSION_LIST = "/api/get_mission_list";
            
            static const std::string AWAKE_CHARACTER = "/api/awake_character";
            static const std::string GET_CONSUMPTION_ITEM = "/api/get_consumption_item";
            static const std::string USE_TICKET = "/api/use_ticket";
        };
        namespace QUERY {
            static const std::string GET_VERSION = "{\"get_version\":null}";
            static const std::string CREATE_PLAYER = "{\"create_player\":{\"player_name\":\"%s\",\"os_version\":\"%s\",\"push_id\":\"%s\",\"takeover_pass\":\"%s\"}}";
            static const std::string AUTH = "{\"auth\":{\"did\":\"%s\",\"push_id\":\"%s\"}}";
            static const std::string SYNC = "{\"sync\": {\"last_updated_datetime\": {\"areas\": \"%s\",\"birthplaces\": \"%s\",\"body_types\": \"%s\",\"characters\": \"%s\",\"courses\": \"%s\",\"grid_types\": \"%s\",\"item_units\": \"%s\",\"items\": \"%s\",\"leader_skills\": \"%s\",\"leader_skill_names\": \"%s\",\"skills\": \"%s\",\"skill_names\": \"%s\",\"levels\": \"%s\",\"makers\": \"%s\",\"maps\": \"%s\",\"parts\": \"%s\",\"ranks\": \"%s\",\"rarities\": \"%s\",\"system_settings\": \"%s\",\"tokens\": \"%s\",\"grid_operands\": \"%s\"}}}";
            static const std::string SYNC_WORD = "{\"sync\": {\"last_updated_datetime\": {\"words\": \"%s\"}}}";
            static const std::string GET_RESOURCE_VERSIONS = "{\"get_resource_versions\":null}";
            
            //REQUEST FORMAT
            static const std::string SYNC_2 = "{\"sync\": {\"last_updated_datetime\": {\"words\": \"%s\"}}}";
            static const std::string GET_RACE_COURSE = "{\"get_race_course\": {\"course_id\": %d}}";
            static const std::string SET_RACE_RESULT = R"(
            {
                "set_race_results": {
                    "course_id": %d,
                    "rank": %d,
                    "number_of_turns": %d,
                    "total_score": %d,
                    "num_of_stops": %d,
                    "num_of_Item": %d,
                    "num_of_charge": %d,
                    "num_of_grid_to_goal": %d,
                    "use_num_of_skills": %d,
                    "help_player_id": %d,
                    "rank1_mst_character_id": %d,
                    "rank2_mst_character_id": %d,
                    "rank3_mst_character_id": %d,
                    "rank4_mst_character_id": %d,
                    "stop_grid_ids": %s
                }
            })";
            /*
            static const std::string SET_RACE_RESULT = R"(
            {
                "set_race_results":
                {
                    "course_id": %d,
                    "rank": %d,
                    "number_of_turns": %d,
                    "total_score": %d,
                    "num_of_stops": %d,
                    "num_of_Item": %d,
                    "num_of_charge": %d,
                    "help_player_id": %d,
                    "rank1_mst_character_id": %d,
                    "rank2_mst_character_id": %d,
                    "rank3_mst_character_id": %d,
                    "rank4_mst_character_id": %d,
                }
            })";*/
            static const std::string GET_DIALOG = "{\"get_dialog\":null}";
            static const std::string GET_PLAYER = "{\"get_player\":{\"player_id\": \"%s\",\"did\": \"%s\"}}";
            static const std::string GET_PLAYER_2 = "{\"get_player\": {\"player_id\": \"%s\",\"did\": \"%s\",\"Player\": \"%s\",\"PlayerCharacter\": \"%s\",\"PlayerItemUnit\": \"%s\",\"PlayerParts\": \"%s\",\"PlayerTeam\": \"%s\",\"PlayerCourse\": \"%s\"}}";

            static const std::string ADD_CHARACTER = "{\"add_character\":{\"data\":{\"character\":[\%s]}}}";

            //REQUEST SHOP
            static const std::string GET_PAY_ITEM = "{\"get_pay_item\":null}";
            static const std::string GET_COIN_EXCHANGE_LIST = "{\"get_coin_exchange_list\":null}";
            static const std::string GET_PARTS_LIST = "{\"get_parts_list\":null}";
            static const std::string EXCHANGE_INTO_COIN = "{\"exchange_into_coin\": {\"id\":%d}}";
            static const std::string BUY_PARTS = "{\"buy_parts\": {\"id\":%d}}";
            static const std::string EXTEND_GARAGE = "{\"extend_garage\":null}";
            static const std::string BUY_FUEL = "{\"buy_fuel\":null}";
            static const std::string BUY_TURNS = R"(
            {
                "buy_turns":null
            })";
            static const std::string CHECK_APPSTORE_RECEIPT = R"(
            {
            "check_appstore_receipt": {
                "owner_rank": "%d",
                "item_id": "%d",
                "quantity": "%d",
                "quantity_free": "%d",
                "single_price": "%d",
                "amount_charged": "%d",
                "transaction_id": "%s",
                "client_ip": "%s",
                "receipt": "%s"
            }
            })";
            static const std::string LOG_ACTION = R"(
            {
                "log_action" :
                {
                    "logs" :
                        [{
                            "owner_rank": %d,
                            "target_id": %d,
                            "kind": %d,
                            "code": %d,
                            "result": "%d",
                            "action_datetime": "%s"
                    }]
                }
            })";
            static const std::string LOG_GOOGLEPLAY_PURCHASE = "{\"log_googleplay_purchase\": {\"owner_rank\": %d, \"item_id\": \"%d\", \"quantity\": %d, \"quantity_free\": %d, \"amount\": %d, \"place\": %d}}";
            static const std::string LOG_GOOGLEPLAY_PURCHASE_WITH_RECEIPT = R"(
            {
                "log_googleplay_purchase": {
                    "owner_rank": %d, 
                    "item_id": %d, 
                    "quantity": %d, 
                    "quantity_free": %d, 
                    "amount": %d, 
                    "place": %d, 
                    "inapp_purchase_data": "%s", 
                    "inapp_data_signature": "%s"
                 }
            })";


            //REQUEST OTHERS
            static const std::string CAMPAIGN_CODE = "{\"campaign_code\":null}";
            static const std::string GET_PICTURE_BOOK = "{\"get_picture_book\":null}";
            static const std::string GET_TRESURE_LIST = "{\"get_treasure_list\":null}";
            static const std::string TAKEOVER_PASSWORD = "{\"takeover_password\":null}";
            static const std::string PURCHASE_INFORMATION = "{\"purchase_information\":null}";
            static const std::string GET_INVITATION_REWARD = "{\"get_invitation_reward\":{\"friend_contact_code\":\"%s\"}}";
            static const std::string GET_NOTICE = "{\"get_notice\":null}";
            static const std::string SET_NOTICE_STATUS = "{\"set_notice_status\": {\"notice_id\": %d, \"status\": %d}}";
            static const std::string GET_COMPENSATION = "{\"get_compensation\":null}";
            static const std::string SET_COMPENSATION_STATUS = "{\"set_compensation_status\": {\"compensation_id\": %d, \"status\": %d}}";
            static const std::string PLAYER_NAME = "{\"player_name\":null}";
            static const std::string FAQ = "{\"faq\":null}";

            //REQUEST GACHA
            static const std::string GET_PAY_GACHA_RESULT = "{\"get_pay_gacha_result\": {\"number_of_executions\":%d,\"type\": %d}}";
            static const std::string GET_FREE_GACHA_RESULT = "{\"get_free_gacha_result\": {\"number_of_executions\": %d}}";

            //REQUEST_FRIEND
            static const std::string SET_FRIEND_REQUEST = "{\"set_friend_request\": {\"player_id\": %d}}";
            static const std::string GET_FRIEND_LIST = "{\"get_friend_list\": {\"status\": %s, \"contact_code\": \"%s\"}}";
            static const std::string APPROVE_FRIENDS = "{\"approve_friends\": {\"status\": %d,\"player_ids\": [%d]}}";

            //REQUEST TEAM EDIT
            static const std::string SET_TEAM_COMPOSITION = "{\"set_team_composition\" : {\"data\" : [%s]}}";
            static const std::string SALE_CHARACTER = R"(
            {
                "sale_character":
                {
                    "sale_character_ids": [%s]
                }
            }
            )";
            static const std::string SALE_PARTS = R"(
            {
                "sale_parts":
                {
                    "sale_parts_ids": [%s]
                }
            }
            )";
            
            static const std::string AWAKE_CHARACTER = R"(
            {
                "awake_character":
                {
                    "base_character_id": %d,
                    "material_character_ids": [%s]
                }
            }
            )";
            static const std::string SALE_INFO = "\"%d\"";
            static const std::string SET_SYNTHETIC_REQUEST = "{\"set_synthetic_request\": {\"base_character_id\": %d,\"material_character_ids\": [%s]}}";
            static const std::string SET_CUSTOM = "{\"set_custom\": {\"data\": {\"character_id\": %d,\"parts\": {\"slot1_parts_id\": %s,\"slot2_parts_id\": %s},\"dice\": {\"side7\": %s,\"side8\": %s,\"side9\": %s}}}}";
            static const std::string LOCK_CHARACTER = R"(
            {
                "lock_character": {
                    "data": {
                        "character":
                        [
                         {
                             "id" : %d,
                             "lock" : %d
                         }
                        ]
                    }
                }
            }
            )";
            
            static const std::string GET_RANKING_LIST = R"(
            {
                "get_ranking_list":
                {
                    "event": "%s",
                    "offset": %d,
                    "limit": %d,
                    "player_id": %d
                }
            }
            )";
            static const std::string GET_MISSION_LIST = "{\"get_mission_list\":null}";
            
            static const std::string GET_CONSUMPTION_ITEM = "{\"get_consumption_item\": null}";
            static const std::string USE_TICKET = R"(
            {
                "use_ticket":
                {
                    "player_ticket_id": %d
                }
            }
            )";
        };
        namespace TAG {
            static const std::string LOG_GOOGLEPLAY_PURCHASE = "API_LOG_GOOGLEPLAY_PURCHASE";
            static const std::string CHECK_STORE_RECEIPT = "API_CHECK_STORE_RECEIPT";
        }
        class ERROR {
        public:
            enum CODE {
                NOT_POST = 201,
                NOT_ENOUGH_OR_WRONG_POST = 202,
                APP_VERSION_INCONSISTENCY = 203,
                VALIDATION_FAIL = 204,
                NOT_FOUND_PLAYER = 205,
                WRONG_PLAYER = 206,
                DELETE_PLAYER = 207,
                NOT_ENOUGH_LOG_DATA = 208,
                NOT_FOUND_DATA_IN_APC = 209,
                NOT_FOUND_DATA_IN_CACHED = 210,
                CONTROLLER_NAME_MISTAKE = 211,
                USER_DATA_REGISTER_FAIL = 212,
                USER_DATA_REGISTER_FAIL_SHARD = 213,
                USER_DATA_CHANGE_FAIL = 214,
                USER_DATA_CHANGE_FAIL_SHARD = 215,
                DUPLICATION_LOGIN = 216,
                DEVICE_TIME_MISTAKE = 217,
                JSON_PARSE_FAIL = 218,
                TRANSFER_CORD_LIMIT_FAIL = 219,
                NOT_FOUND_TRANSFER_CORD = 220,
                NOT_FOUND_PAGE = 221,
                RESET_MARATHON_ERROR = 222,
                INPUT_NG_WORD = 223,
                INPUT_IMAGE_FONT = 224,
                FRIEND_MAX_HOLD_ERROR = 225,
                NOT_ENOUGH_HOLD_TOKEN = 226,
                NOT_ENOUGH_HOLD_COIN = 227,
                NOT_ENOUGH_HOLD_FRIEND_POINT = 228,
                FILL_UP_GARAGE = 229,
                COMPOSITION_FAIL = 230,
                NOT_FOUND_CHARACTER = 231,
                ALREADY_INPUT_ENDED = 232,
                CAN_NOT_SELL = 233,
                CAN_NOT_BUY = 234,
                CAN_NOT_BARTER = 235,
                MAX_GARAGE_SIZE = 236,
                SERVER_MAINTENANCE = 237,
                EVENT_LIMIT_FAIL = 238,
                NOT_ENOUGH_COST = 239,
                GET_LIMIT_FAIL = 240,
                NETWORK = 250,
                GAME_SERVER_APC_REQUEST_ERROR = 600,
                UNAVAILABLE_APC = 700,
                UNAVAILABLE_CACHED = 701,
                SAVE_ERROR = 800,
                DATA_BASE_ERROR = 801,
                OTHER_ERROR_DATA_BASE = 900,
                OTHER_ERROR = 901,
            };
            static const std::string getMessage(const CODE code);
            static const bool isRecognizeErrors(const int code);
        };
    };
};
#endif /* defined(__syanago__API__) */
