#include "Version.h"
#include "Native.h"
#include <vector>
#include <string>
#include <sstream>

USING_NS_CC;


bool Version::checkVersion()
{
    // ios申請バージョンモードで実行
    if (Native::getApplicationVersion() == review_ios_app_version) {
        UserDefault::getInstance()->setBoolForKey("demandMode", true);
    }

    // appバージョン情報が未設定 || バージョン一致(最新)
    if (server_app_version == "" || Version::isNotNeedUpdate(Native::getApplicationVersion(), server_app_version)) {
        return true;
    } else {
        return false;
    }
}

bool Version::isNotNeedUpdate(const std::string& appVersion, const std::string& serverAppVersion)
{
    std::map<std::string, int>appVersions = trimVersionString(appVersion);
    std::map<std::string, int>serverVersions = trimVersionString(serverAppVersion);

    if (appVersions["major"] > serverVersions["major"]) {
        return true;
    } else if (appVersions["major"] < serverVersions["major"]) {
        return false;
    }

    if (appVersions["minor"] > serverVersions["minor"]) {
        return true;
    } else if (appVersions["minor"] < serverVersions["minor"]) {
        return false;
    }

    if (appVersions["fix"]  >= serverVersions["fix"]) {
        return true;
    }
    return false;
}

std::map<std::string, int>Version::trimVersionString(const std::string&versionString)
{
    std::map<std::string, int>results;
    results["major"] = 0;
    results["minor"] = 0;
    results["fix"] = 0;

    int founded = 0;
    for (std::string version : Version::split(versionString, '.')) {
        if (founded == 0) {
            results["major"] = atoi(version.c_str());
        } else if (founded == 1) {
            results["minor"] = atoi(version.c_str());
        } else if (founded == 2) {
            results["fix"] = atoi(version.c_str());
        } else {
            break;
        }
        founded++;
    }
    return results;
}

std::vector<std::string>Version::split(const std::string &str, char sep)
{
    std::vector<std::string>v;
    std::stringstream ss(str);
    std::string buffer;
    while (std::getline(ss, buffer, sep)) {
        v.push_back(buffer);
    }
    return v;
}
