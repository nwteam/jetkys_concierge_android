#ifndef __syanago__Version__
#define __syanago__Version__

#include "cocos2d.h"
#include "editor-support/spine/Json.h"
#include <string>
#include <map>
#include <vector>
#include "SecurityUtil.h"

class Version {

public:
    CC_SYNTHESIZE_READONLY(std::string, store_url, StoreUrl);
    CC_SYNTHESIZE_READONLY(std::string, force_update, ForceUpdate);
    CC_SYNTHESIZE_READONLY(std::string, errorMessage, ErrorMessage);
    
    Version():
    success(""),
    errorMessage(""),
    store_url(""),
    server_app_version(""),
    force_update(""),
    review_ios_app_version(""){}
    
    Version(Json* json):
        success("false"),
        errorMessage(""),
        store_url(""),
        server_app_version(""),
        force_update(""),
        review_ios_app_version("")
    {
        Json* getVersion = Json_getItem(json, "get_version");
        //success = Json_getString(getVersion, "result", "false");
        Json* version = Json_getItem(getVersion, "data");
        server_app_version = Json_getString(version,"app_version", "");
        force_update = Json_getString(version,"force_update", "");
        store_url = Json_getString(version,"store_url", "");
        review_ios_app_version = Json_getString(version, "review_ios_app_version", "");
    }
    
    bool checkVersion();
    
    static std::vector<std::string> split(const std::string &str, char sprit);
    static bool isNotNeedUpdate(const std::string& appVersion, const std::string& serverAppVersion);
    static std::map<std::string, int> trimVersionString(const std::string&versionString);
private:
    std::string review_ios_app_version;
    std::string server_app_version;
    std::string success;
};


#endif /* defined(__syanago__Version__) */
