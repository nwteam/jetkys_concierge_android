package com.app.net;

import android.text.TextUtils;

import com.app.models.RegiterInfo;
import gameutils.encrypt.EncryptData;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.app.models.User;
import com.app.utils.ZLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("unused")
public class JsonParser implements NetParams {

    /**
     * @param id
     * @param json
     * @return
     */
    public static NetResult parseJson(InterfaceIds id, String json) {
        NetResult obj = null;
        obj = call(id.PARSE_METHOD, json); // 通过函数名调用函数
        return obj;
    }

    /**
     * call method in this class to parse json
     *
     * @param methodName
     * @param json
     * @return
     */
    private static NetResult call(String methodName, String json) {
        NetResult resultInfo = null;
        try {
            Method m = JsonParser.class.getDeclaredMethod(methodName, String.class); // 找到对应函数名的函数
            resultInfo = (NetResult) m.invoke(null, json); // 由于是静态函数所以不需要实例去执行它
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return resultInfo;
    }

    private static String outterParse(String json, NetResult result) throws JSONException {
        JSONObject jo = new JSONObject(json);
        result.setError(jo.optString(NetConst.ERROR));
        JSONObject resultJO = null;
        if(result.getError() != null &&  !result.getError().equals("null")){
         resultJO = jo.getJSONObject(NetConst.ERROR);
        }
        result.setDatetime(jo.optString(NetConst.DATETIME));
        result.setIv(jo.optString(NetConst.IV));
        result.setMethod(jo.optString(NetConst.METHOD));
        result.setToken(jo.optString(NetConst.TOKEN));
        result.setRequest_no(jo.optInt(NetConst.REQUEST_NO,2));
        if (resultJO != null) { // failed
            result.setMessage(resultJO.optString(NetConst.MESSAGE));
            result.setResultCode(resultJO.optInt(NetConst.CODE));
            return null;
        } else {
            return jo.getString(NetConst.DATA);
        }
    }

    private static NetResult makeErrorResult(Exception e, NetResult result) {
        e.printStackTrace();
        result.setResultCode(NetConst.CODE_NET_FAILED);
        result.setMessage(NetConst.ERR_MSG_SERVER_ERR);

        return result;
    }

    private static NetResult loginParse(String json) {
        NetResult netResult = new NetResult();
        try {
        	ZLog.v("loginParse json="+json);
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            
//            JSONObject dataJo = new JSONObject(datas);

//            User user = new User();

//            user.setId(dataJo.optString(USER_ID));
//            user.setToken(dataJo.optString(USER_TOKEN));
//            user.setCityId(dataJo.optString(CITY_ID));
//            user.setNick(dataJo.optString(USER_NICK));
//            user.setEmail(dataJo.optString(USER_EMAIL));
//            user.setPwd(dataJo.optString(USER_PWD));
//            user.setSex(dataJo.optString(USER_SEX));
//            user.setBirth(dataJo.optString(USER_BIRTH));
//            user.setCityId(dataJo.optString(CITY_ID));
//            user.setDistrict(dataJo.optString(USER_DIST));
//            user.setBalance(dataJo.optString(USER_BALANCE));
//            user.setFollowedPeople(dataJo.optString(USER_FOLLOWED_PEOPLE));
//            user.setScore(dataJo.optString(USER_RATE));
//            user.setSelfIntro(dataJo.optString(USER_SELF_INTRO));
//            user.setJobId(dataJo.optString(JOB_ID));
//            user.setJob(dataJo.optString(JOB_NAME));

            netResult.setResultObject(json);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult registerParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }
            ZLog.v("datas="+datas+" netResult.getIv()="+netResult.getIv());
            String data = EncryptData.decrypt(datas, netResult.getIv());
            ZLog.v("data="+data);
            JSONObject dataJo = new JSONObject(data);
            JSONObject dataCreate = dataJo.optJSONObject("create_user");
            JSONObject dataUser = dataCreate.optJSONObject("data");

            RegiterInfo info = new RegiterInfo();
            info.setUser_id(dataUser.optString("user_id"));
            info.setContact_code(dataUser.optString("contact_code"));
            info.setInstall_key(dataUser.optString("install_key"));
            info.setUser_type(dataUser.optInt("user_type"));

            netResult.setResultObject(data);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

    private static NetResult publishMySeriesUploadParse(String json) {
        NetResult netResult = new NetResult();
        try {
            String datas = outterParse(json, netResult);
            if (datas == null) {
                return netResult;
            }

            JSONObject dataJo = new JSONObject(datas);

            String url = dataJo.optString("url");

            netResult.setResultObject(url);

            return netResult;
        } catch (JSONException e) {
            return makeErrorResult(e, netResult);
        }
    }

}