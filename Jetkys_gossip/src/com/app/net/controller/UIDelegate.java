package com.app.net.controller;

import com.app.net.NetResult;

public interface UIDelegate {

	// public void onHttpRequesting();

	/**
	 * Called when request succeed.
	 * 
	 * @param ifId
	 *            sign which interface
	 * @param obj
	 *            result object
	 * @param json
	 */
	public void onRequestSuccess(int ifId, NetResult obj);

	/**
	 * Called when request failed / logical failed.
	 * 
	 * @param apiId
	 *            IF_ID - in HttpDef.HTTP_API_IDS
	 * @param errMsg
	 *            err msg from server
	 * @param errCode
	 *            defined err code
	 */
	public void onRequestError(int ifId, NetResult errMsg);
}