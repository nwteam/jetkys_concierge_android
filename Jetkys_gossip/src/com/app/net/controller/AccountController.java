package com.app.net.controller;

import com.app.net.InterfaceIds;
import com.app.net.NetResult;
import com.app.utils.ZLog;

public class AccountController extends BaseController {
	
    public AccountController(UIDelegate delegate) {
        super(delegate);
    }

    private String mTmpEmail;

    public void login(String email, String password) {
        mTmpEmail = email;
        String data ="";
        mNetTask.login(email,password);
    }

    public void register(String nickName,String mail,String psw) {
        mNetTask.register(nickName,mail,psw);
    }



    @Override
    public void onRequestSuccess(int ifId, NetResult obj) {
        super.onRequestSuccess(ifId, obj);
        
        ZLog.v("AccountController  ifId="+ifId);
        if (ifId == InterfaceIds.USR_LOGIN.IF_ID || ifId == InterfaceIds.USR_REGISTER.IF_ID) {
            // if login and register, initialize user object
//            User retUser = (User) obj.getResultObject();
        }
    }
}