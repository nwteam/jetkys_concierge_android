package com.app.net.controller;

import com.app.net.HttpIF;
import com.app.net.HttpTask;
import com.app.net.NetCallBack;
import com.app.net.NetResult;
import com.app.utils.ZLog;

public class BaseController implements NetCallBack {

	protected HttpIF mNetTask = new HttpTask(this);

	protected UIDelegate mDelegate;

	public BaseController(UIDelegate delegate) {
		mDelegate = delegate;
	}

	/**
	 * disable callback to UI
	 */
	public void clear() {
		mDelegate = null;
	}

	@Override
	public void onRequestStart(int ifId) {
	}

	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		ZLog.v("BaseController  ifId="+ifId);
		if (mDelegate != null) {
			ZLog.v("mDelegate != null="+ifId);
			mDelegate.onRequestSuccess(ifId, obj);
		}
	}

	@Override
	public void onRequestError(int ifId, NetResult obj) {
		if (mDelegate != null) {
			mDelegate.onRequestError(ifId, obj);
		}
	}
}
