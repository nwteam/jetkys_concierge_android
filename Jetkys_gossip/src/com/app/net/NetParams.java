package com.app.net;

public interface NetParams {
	public String DID="did";
	public String IV="IV";
	public String TOKEN="token";
	public String DATETIME="datetime";
	public String REQUEST_NO="request_no";
	public String DEVICE_TYPE="device_type";
	public String DATA="data";
	
	public  final String REGISTER_URL = "{\"create_user\": {\"nickname\": %s,\"token\":%s}}";
	public  final String TOKENURL="%s:%s:%s";
	public final String LOGIN_URL="{\"auth\": {\"token\": %s}}";
}