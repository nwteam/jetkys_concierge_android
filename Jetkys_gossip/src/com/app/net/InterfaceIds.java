package com.app.net;

public enum InterfaceIds {
    USR_LOGIN(1, "登录", //
            NetConst.SERVER_URL + "api/auth", //
            "loginParse"), //
    USR_REGISTER(2, "用户注册", //
            NetConst.SERVER_URL + "api/create_user", //
            "registerParse"), //

    UPLOAD_PIC(3, "图片上传", //
            NetConst.SERVER_URL + "index.php/upload", //
            "publishMySeriesUploadParse");//

    public int IF_ID; // interface id
    public String REMARK; // interface id
    public String URL = null; // post url
    public String PARSE_METHOD = null; // method in

    private InterfaceIds(int ifId, String remark, String url, String method) {
        IF_ID = ifId;
        REMARK = remark;
        URL = url;
        PARSE_METHOD = method;
    }

    /**
     * Find url by given interface id.
     *
     * @param apiId given id
     * @return
     */
    public static String findUrlById(int apiId) {
        for (InterfaceIds IF : InterfaceIds.values()) {
            if (IF.IF_ID == apiId) {
                return IF.URL;
            }
        }
        return null;
    }

    /**
     * Find json parse method by given interface id.
     *
     * @param apiId given id
     * @return should called method in JsonParser
     */
    public static String findMethodById(int apiId) {
        for (InterfaceIds IF : InterfaceIds.values()) {
            if (IF.IF_ID == apiId) {
                return IF.PARSE_METHOD;
            }
        }
        return null;
    }
}
