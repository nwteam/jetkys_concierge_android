package com.app.net;

import com.app.models.*;

public interface HttpIF {
   public void login(String mail,String psw);

   public void register(String nickName,String mail,String psw);

  /*    public void getMenuList();

    public void getClosureList(String type, int page);

    public void queryClosure(String queryTime, String queryWord, int page);

    public void getClosureCommentList(String id, int page);

    public void getClosureDetail(String id);

    public void commentClosure(String companyId, String content);

    public void likeClosure(String knowId, String commentId);

    //    public void editClosure(String commentId, String content);
    public void editClosureComment(String commentId, String content);

    public void deleteClosureComment(String commentId);

    public void getSystList(String keyword, String type, int page);

    public void getWorkplaceList(String cityId, String companyId, String queryWord);

    public void getMockListByWorkplace(String workpId, int page);

    public void likeMock(String mockId);

    public void commentMock(String content, String img);

    public void addWorkplace(String cityId, String companyId, String workpName);

    public void getMySeriesCatList();

    public void addMyseriesCategory(String category, String content);

    public void getMySeriesArticleList(String categoryId, String type, int page);

    public void getMySeriesCommentList(String articleId, int page);

    public void commentMySeriesArticle(String articleId, String content);

    public void publishMySeriesArticle(String categoryId, String title, String content,
                                       String picUrl);

    public void updateProfile(User newUser);

    public void pushSwitch(String trigger);

    public void modifyPwd(String oldPwd, String newPwd);

    public void feedBack(String type, String content);

    public void getProfile(String userId);

    public void userFollow(String targetUser);

    public void userUnfollow(String targetUser);

    public void userGetFollowedList(String targetUser, int page);

    public void userRate(String targetUser, String score, String title, String content);

    public void userGetRateList(String targetUser, int page);

    public void getMemoList(int page);

    public void getMemo(String targetUid);

    public void modifyMemo(String targetUId, String type, String remark);

    public void getArticleHistoryList(int page);

    public void sendMsgTo(String gid, String uid, String content, String pic);

    public void getConversationList(int page);

    public void deleteConversation(String convIds);*/

    public void uploadPic(String filePath);

   /* public void getNearByToilet(String ll, String time);

    public void getFreeWorkplaceList(int page);

    public void queryFreeWorkplaceList(String queryWord, int page);

    public void getFreeWorkerList(int page);

    public void queryFreeWorkerList(String queryWord, int page);

    public void publishFreeWorker(WorkerInfo info);

    public void publishFreeWorkplace(WorkerInfo info);

    public void takeChat(String uid, String gid, int page);

    public void publishContract(String targetUid, String convId, Contract contract);

    public void submitPushToken(String token);

    public void resetPassword(String email);

    public void getUnreadMsgCount();

    public void getBalance();

    public void getAppIsFree();

    public void deleteMemo(String targetIds);

    public void getShopItemList();

    public void notifyPurchase(String purchasedSKU, String orderResult);

    public void getClosureHotWordList(int page);

    public void searchClosure(String word, int page);

    public void newClosure(String title, String content, String img, boolean urgent);

    public void getSystDetail(String id);

    public void getFriendList(int page);

    public void addFriendList(int page);

    public void addedFriendList(int page);

    public void friendSearchList(String freeWord, int page);

    public void friendSearchListCat(String catId, String groupId, int page);

    public void addSavedGroup(String groupName, String groupId, String groupImg, List<String> idList);

    public void addGroupMember(String groupId);

    public void getGroup(String groupId);

    public void groupCategory(String catType);

    public void modifyGroup(String groupId, String groupName, String groupImg, List<String> memId);

    public void quitGroup(String groupId);

    public void getAddress();

    public void getIndustry();

    public void getUse();

    void likeMockComment(String mockId, String commentId);

    void editMock(String mockId, String content, String pic);

    void deleteMockComment(String mockId);

    void getMockCommentList(String systId, int page);

    void getProfileSettingList(String id, String pid);

    void commentMock2(String birdId, String content);

    void editMockComment(String mockId, String content);

    void editClosure(String id, String title, String content, String img, boolean urgent);

    void deleteClosure(String id);

    void deleteMock(String id);*/
}
