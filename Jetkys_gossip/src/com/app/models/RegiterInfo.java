package com.app.models;

import java.io.Serializable;

/**
 * Created by yangshiqin on 16/2/25.
 */
public class RegiterInfo implements Cloneable, Serializable {

    String User_id;
    String install_key;
    int user_type;
    String contact_code;
    String name;
    String email;
    String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser_id() {
        return User_id;
    }

    public void setUser_id(String user_id) {
        User_id = user_id;
    }

    public String getInstall_key() {
        return install_key;
    }

    public void setInstall_key(String install_key) {
        this.install_key = install_key;
    }

    public int getUser_type() {
        return user_type;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    public String getContact_code() {
        return contact_code;
    }

    public void setContact_code(String contact_code) {
        this.contact_code = contact_code;
    }
}
