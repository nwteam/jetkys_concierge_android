package com.app.utils;

import java.util.HashMap;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

public class FontUtil {
	private static final String FONT_DIR_PREFIX = "fonts/";
//	public static final String FONT_ROUNDED_MPLUS = "rounded-x-mplus-2c-bold.ttf";
//	public static final String FONT_HEADER2 = "KozGoPro-Regular.otf";

	private static HashMap<String, Typeface> sFontMap = new HashMap<String, Typeface>();

	public static Typeface getTypeface(Context context, String fontName) {
		if (sFontMap.containsKey(fontName)) {
			return sFontMap.get(fontName);
		} else {
			Typeface tf = Typeface.createFromAsset(context.getAssets(), FONT_DIR_PREFIX + fontName);
			sFontMap.put(fontName, tf);
			return tf;
		}
	}

	/**
	 * set TEXTVIEW's font, see{@link #FONT_ROUNDED_MPLUS}
	 * 
	 * @param tv
	 * @param fontName
	 */
	public static void makeTextViewFont(TextView tv, String fontName) {
		Typeface tf = getTypeface(tv.getContext(), fontName);
		tv.setTypeface(tf);
	}
}
