package com.app.utils;

import gameutils.encrypt.EncryptData;

import java.security.MessageDigest;
import java.util.Random;
public class CryptUtil {
	private final static String PUBLIC_KEY = "I9singeHk13xqbkc";
	private final static String TOKEN =PUBLIC_KEY+":%s:%s";
	private final static String USERIDTOKEN = PUBLIC_KEY+":%s:%s:%s";
	 private final static String KEY_SHA256 = "SHA-256"; 
	 private final static String DID_FORMAT = "%s-%s-%s";
	 /** 
	     * 全局数组 
	     */  
	    private final static String[] hexDigits = { "0", "1", "2", "3", "4", "5",  
	            "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" }; 
	 
	 /** 
	     * SHA 加密 
	     * @param data 需要加密的字符串 
	     * @return 加密之后的字符串 
	     * @throws Exception 
	     */  
	    public static String encryptSHA(String data) throws Exception {  
	        // 验证传入的字符串  
	        if (StringUtil.isEmpty(data)) {  
	            return "";  
	        }  
	        // 创建具有指定算法名称的信息摘要  
	        MessageDigest sha = MessageDigest.getInstance(KEY_SHA256);  
	        // 使用指定的字节数组对摘要进行最后更新  
	        sha.update(data.getBytes());  
	        // 完成摘要计算  
	        byte[] bytes = sha.digest();  
	        // 将得到的字节数组变成字符串返回  
	        return byteArrayToHexString(bytes);  
	    }  
	    
	    /** 
	     * 将一个字节转化成十六进制形式的字符串 
	     * @param b 字节数组 
	     * @return 字符串 
	     */  
	    private static String byteToHexString(byte b) {  
	        int ret = b;  
	        //System.out.println("ret = " + ret);  
	        if (ret < 0) {  
	            ret += 256;  
	        }  
	        int m = ret / 16;  
	        int n = ret % 16;  
	        return hexDigits[m] + hexDigits[n];  
	    }  
	    
	    /** 
	     * 转换字节数组为十六进制字符串 
	     * @param bytes 字节数组 
	     * @return 十六进制字符串 
	     */  
	    private static String byteArrayToHexString(byte[] bytes) {  
	        StringBuffer sb = new StringBuffer();  
	        for (int i = 0; i < bytes.length; i++) {  
	            sb.append(byteToHexString(bytes[i]));  
	        }  
	        return sb.toString();  
	    }
	    
	    public static String getDIDToken(String DID,String dataTime){
	    	return EncryptData.md5(String.format(TOKEN, DID, dataTime));
	    }

		public static String getUseridToken(String install_key,String UserId,String dataTime){
			return EncryptData.md5(String.format(USERIDTOKEN,install_key,UserId,dataTime));
		}

	    public static String getDateTime(){
	    	return TimeUtil.getCurrentTime(TimeUtil.PATTERN_SEC24);
	    }
	    
	    public static String getDID(String dataTime){
	    	String DID = String.format(DID_FORMAT, EncryptData.getUUID(),dataTime,random4());
	    	return "";
	    }
	    
	    public static String random4(){
	    	//取得一个1000-9999的随机数    
	    	  String   s="";    
	    	   
	    	  int   intCount=0;    
	    	   
	    	  intCount=(new   Random()).nextInt(9999);//    
	    	   
	    	  if(intCount<1000)intCount+=1000;    
	    	   
	    	  s=intCount+""; 
	    	  return s;
	    }
}
