package com.app.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
public class TimeUtil {
    public static String PATTERN_MONTH = "yyyy-MM";
    public static String PATTERN_MONTH2 = "yyyyMM";
    public static String PATTERN_DATE = "yyyy-MM-dd";
    public static String PATTERN_DATE2 = "yyyyMMdd";
    public static String PATTERN_DATE3 = "yyyyMMdd_HHmmss";
    public static String PATTERN_SEC = "yyyy-MM-dd hh:mm:ss";
    public static String PATTERN_MIN = "yyyy-MM-dd hh:mm";  //12小时
    public static String PATTERN_MIN24="yyyy-MM-dd HH:mm";//24小时
    public static String PATTERN_SEC24 ="yyyyMMddHHmmss";

    public static String getCurrentTime(String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String timeString = sdf.format(new Date());

//        return DateFormat.format(pattern, new Date()).toString();
        return timeString;
    }

    /**
     * time[0] - year</br> time[1] - month </br> time[2] - date
     *
     * @param time
     */
    public static void getCurrentTime(int[] time) {
        if (time == null) {
            return;
        }

        if (time.length < 3) {
            return;
        }

        time[0] = Calendar.getInstance().get(Calendar.YEAR);
        time[1] = Calendar.getInstance().get(Calendar.MONTH);
        time[2] = Calendar.getInstance().get(Calendar.DATE);
    }

    public static long getCurrentStamp() {
        return Calendar.getInstance().getTimeInMillis();
    }

    public static String format(int year, int monthOfYear, int dayOfMonth, String pattern) {
        Calendar c = Calendar.getInstance();
        c.set(year, monthOfYear, dayOfMonth);

        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String timeString = sdf.format(c.getTime());
//        return DateFormat.format(parttern, c).toString();
        return timeString;
    }
}
