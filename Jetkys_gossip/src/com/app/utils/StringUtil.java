package com.app.utils;

import java.util.List;
/**
 * 
 * @Description: 检查工具类
 * @author qinwang
 * @createDate 2015-6-5
 * @since iccs V01.00.000
 */
public class StringUtil {
	
	/**
	 * 判断自负窜是否为空（true：为空，false：不为空）
	 * @param source
	 * @return
	 */
	public static boolean isEmpty(String source) {
		if (source != null && source.length() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * 判断两个自负窜是否相匹配（true：匹配，false：不匹配）
	 * @param source
	 * @param target
	 * @return
	 */
	public static boolean isEquals(String source, String target) {
		if (!isEmpty(source) && !isEmpty(target)) {
			if (source.equals(target)) return true;
		}
		return false;
	}
	
	/**
	 * 判断list是否为空（true：为空，false：不为空）
	 * @param list
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isListEmpty(List list) {
		if (list != null && list.size() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * 比如：13554625320abcdef2015-12-22 09:55签名规则是手机号 + 密码 + 日期的MD5
	 * */
	public static String getMd5Signature(String phone ,String password){
		String str = phone+password+TimeUtil.getCurrentTime(TimeUtil.PATTERN_MIN24);
		str = MD5.getMd5Value(str);
		return str;
	}

}
