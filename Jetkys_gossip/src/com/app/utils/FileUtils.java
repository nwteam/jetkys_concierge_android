package com.app.utils;

import java.io.*;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore.Images.ImageColumns;

/**
 * Created by a on 2015/11/26.
 */
public class FileUtils {
    public static final String USER_FILE_NAME = "user.d";
    public static final String REGISITER_FILE_NAME="regisiter.d";
    public static void writeSerializable(Context mContext,String fileName, Serializable data) {
        FileOutputStream stream = null;
        try {
            stream = mContext.openFileOutput(fileName, mContext.MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(stream);
            oos.writeObject(data);//td is an Instance of TableData;
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public static Serializable readSerializable(Context mContext,String fileName){
        Serializable data = null;
        FileInputStream stream = null;
        try {
            stream = mContext.openFileInput(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(stream);
            try {
                data= (Serializable) ois.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return data;
    }

    public static final String DCIM = Environment.getExternalStorageDirectory()
            + "/" + Environment.DIRECTORY_DCIM;

    public static boolean isSDMounted() {
        String status = Environment.getExternalStorageState();
        return status.equals(Environment.MEDIA_MOUNTED);
    }

    public static String getDefaultFileName() {
  		return "IMG_" + TimeUtil.getCurrentTime(TimeUtil.PATTERN_DATE3)+ ".jpg";
  	}
    
    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri
                .getAuthority());
    }
    
    /**
  	 * 转化uri为真实文件路径
  	 * 
  	 * @param context
  	 * @param uri
  	 * @return
  	 */
  	public static String getRealFilePath(final Context context, final Uri uri) {
  		if (null == uri)
  			return null;
  		final String scheme = uri.getScheme();
  		String data = null;
  		if (scheme == null)
  			data = uri.getPath();
  		else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
  			data = uri.getPath();
  		} else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
  			Cursor cursor = context.getContentResolver().query(uri, new String[] { ImageColumns.DATA }, null, null, null);
  			if (null != cursor) {
  				if (cursor.moveToFirst()) {
  					int index = cursor.getColumnIndex(ImageColumns.DATA);
  					if (index > -1) {
  						data = cursor.getString(index);
  					}
  				}
  				cursor.close();
  			}
  		}
  		return data;
  	}

}
