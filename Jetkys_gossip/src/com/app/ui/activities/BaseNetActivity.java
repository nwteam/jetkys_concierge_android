package com.app.ui.activities;

import jp.gossip.jetkys.android.R;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.app.net.NetConst;
import com.app.net.NetResult;
import com.app.net.controller.BaseController;
import com.app.net.controller.UIDelegate;
import com.app.utils.ZLog;

public class BaseNetActivity extends BaseActivity implements UIDelegate {
	protected ProgressDialog mLoadingDialog = null;

	@Override
	public void findViews() {

	}

	@Override
	public void init() {

	}

	@Override
	public void addListeners() {

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	protected void test() {
		ZLog.e("------ TEST DATAS! ------");
	}

	/**
	 * dismiss loading dialog
	 */
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		dismissDialog();
	}

	public void showDialog() {
		if (isFinishing()) {
			return;
		}

		if (mLoadingDialog == null) {
			mLoadingDialog = ProgressDialog.show(this, "", getResources().getString(R.string.hint_requesting_please_wait));
		} else {
			mLoadingDialog.setMessage(getResources().getString(R.string.hint_requesting_please_wait));
			mLoadingDialog.show();
		}
	}

	public void dismissDialog() {
		if (isFinishing()) {
			return;
		}

		if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
			mLoadingDialog.dismiss();
		}
	}

//	public void showOutOfBalanceDlg() {
//		new ZDialog(mContext)
//				.setCancelable(false)
//				.setContent(getString(R.string.hint_out_of_balance))
//				.setLeftButton(getString(R.string.OK),
//						new LeftButtonListener() {
//
//							@Override
//							public void onLeftButtonClick() {
//								outOfBalanceCancelLeft();
//							}
//						})
//				.setRightButton(getString(R.string.goto_shop),
//						new RightButtonListener() {
//
//							@Override
//							public void onRightButtonClick() {
//								// Intent intent = new Intent();
//								// intent.setClass(mContext,
//								// InnerShopActivity.class);
//								// startActivity(intent);
//								// outOfBalanceCancelRight();
//							}
//						}).show();
//	}

	public void clearController(BaseController controller) {
		if (controller != null) {
			controller.clear();
			controller = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		dismissDialog();
	}

	@Override
	protected void onPause() {
		super.onPause();
		dismissDialog();
	}

	@Override
	public void onRequestError(int ifId, NetResult errMsg) {
		// TODO Auto-generated method stub
		dismissDialog();
		if (errMsg.equals(NetConst.ERR_OUT_OF_BALANCE)) {
//			showOutOfBalanceDlg();
		} else {
			showToast(errMsg.getMessage());
			System.out.println("errMsg=" + errMsg.getMessage());
		}
	}
}