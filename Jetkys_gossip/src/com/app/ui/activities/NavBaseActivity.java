package com.app.ui.activities;

import jp.gossip.jetkys.android.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by yangshiqin on 15/11/21.
 */
public abstract class NavBaseActivity extends BaseNetActivity {

    private ViewGroup mContentContainer;
    private ViewGroup headerContainer;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        /** setup header **/
        headerContainer = (ViewGroup)findViewById(R.id.header_container);
        View header = onCreateHeader(getLayoutInflater(), headerContainer);
        headerContainer.removeAllViews();
        headerContainer.addView(header);

        /** setup content **/
        mContentContainer = (ViewGroup)findViewById(R.id.content_container);
        View content = onCreateContent(getLayoutInflater(), mContentContainer,
                savedInstanceState);
        mContentContainer.removeAllViews();
        mContentContainer.addView(content);
    }

    protected void setHeaderBackground(int res){
        headerContainer.setBackgroundResource(res);
    }

    protected void setHeaderBackgroundColor(int color){
        headerContainer.setBackgroundColor(color);
    }

    /** for content **/
    protected View onCreateContent(LayoutInflater inflater,
                                   ViewGroup container, Bundle savedInstanceState) {
        return null;
    }

    /** for header **/
    protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
        return null;
    }

}
