package com.app.ui.activities;

import java.util.List;

import com.app.utils.ConstData;

import jp.gossip.jetkys.android.R;

import android.os.Bundle;
import android.text.TextUtils;

public class BaseListNetActivity extends BaseNetActivity {
	protected enum STATUS {
		NONE, REFRESH, LOAD_MORE
	};

	protected int mCurrPage;

	protected STATUS mCurrStatus;

	protected boolean mIsQuery = false;

	protected String mTmpSearchWord;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mCurrStatus = STATUS.NONE;
		mCurrPage = ConstData.PAGE_FIRST;

		mTmpSearchWord = "";
	}

	protected void judgeRefresh(List<?> list) {
		judgeRefresh(list, getString(R.string.hint_list_no_data));
	}

	protected void judgeRefresh(List<?> list, String hint) {
		if (list == null) {
			return;
		}

		if (TextUtils.isEmpty(hint)) {
			showToast(hint);
		}
	}

	protected void judgeLoadMore(List<?> list) {
		judgeLoadMore(list, getString(R.string.hint_list_load_no_more));
	}

	protected void judgeLoadMore(List<?> list, String hint) {
		if (list == null) {
			return;
		}

		if (list.size() > 0) {
			mCurrPage++;
		} else {
			if (!TextUtils.isEmpty(hint)) {
				showToast(hint);
			}
		}
	}
}
