package com.app.ui.activities;

import jp.gossip.jetkys.android.R;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.app.AppManager;

/**
 * Created by yangshiqin on 15/11/21.
 */
public class NavHeadBaseActivity extends NavBaseActivity {

    public ImageButton mBackIB;
    private ImageButton mNavMessage;
    private TextView rightMsg;
    private TextView mNavTitle;
    public View viewGroup;

    /** for header **/
    protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {

        View view = inflater.inflate(R.layout.activity_nav_layout, container, false);
        mBackIB = (ImageButton)view.findViewById(R.id.ib_nav_black);
        mNavTitle = (TextView)view.findViewById(R.id.tv_nav_title);
        rightMsg = (TextView)view.findViewById(R.id.tv_right);
        
        mNavMessage = (ImageButton)view.findViewById(R.id.im_nav_message);
        
        mBackIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backActivity();
            }
        });

        setHeaderBackgroundColor(Color.BLACK);
        return view;
    }

    public void setLeftImageBar(int resId,OnClickListener listener){
        mBackIB.setVisibility(View.VISIBLE);
        mBackIB.setImageResource(resId);
        mBackIB.setOnClickListener(listener);
    }

   public  void setLeftEnable(boolean show){
    	if(show){
    		mBackIB.setVisibility(View.VISIBLE);
    	}else{
    		mBackIB.setVisibility(View.GONE);
    	}
    }

    public void setRightMsgShow(boolean show){
        if(show){
            rightMsg.setVisibility(View.VISIBLE);
        }else{
            rightMsg.setVisibility(View.INVISIBLE);
        }
    }

    public void setRightTitleBar(String msg,OnClickListener listener){
    	rightMsg.setVisibility(View.VISIBLE);
    	rightMsg.setText(msg);
    	rightMsg.setOnClickListener(listener);
    }

    public void setRightImageBar(int resId,OnClickListener listener){
    	mNavMessage.setVisibility(View.VISIBLE);
    	mNavMessage.setImageResource(resId);
    	mNavMessage.setOnClickListener(listener);
    }
    
    protected void setNavTitle(String title){
        mNavTitle.setText(title);
    }

    protected void setNavTitle(int res){
        mNavTitle.setText(res);
    }

    protected void messageListener()
    {

    }

    protected void backActivity(){
        AppManager.getAppManager().finishActivity(this);
    }

    @Override
    public void findViews() {

    }

    @Override
    public void init() {

    }

    @Override
    public void addListeners() {

    }
}
