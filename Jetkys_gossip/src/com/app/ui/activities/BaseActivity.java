package com.app.ui.activities;

import jp.gossip.jetkys.android.R;
import android.text.InputType;
import android.view.KeyEvent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;
import com.app.AppManager;
import com.app.utils.StringUtil;

public abstract class BaseActivity extends Activity {

    public abstract void findViews();

    public abstract void init();

    public abstract void addListeners();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppManager.getAppManager().addActivity(this);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.in_right_left, R.anim.out_right_left);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.in_right_left, R.anim.out_right_left);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.in_left_right, R.anim.out_left_right);
    }

    public void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    public void showToast(int r) {
        Toast.makeText(this, r, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AppManager.getAppManager().finishActivity(this);
            return true;
        } else {
            return super.onKeyUp(keyCode, event);
        }
    }

    public boolean CheckPassWord(String passWord,String title){
        if(StringUtil.isEmpty(passWord)) {
            showToast(title + getString(R.string.hint));
            return false;
        }else if (passWord.length() < 6){
            showToast(title + getString(R.string.password_len_hint));
            return false;
        }
        return true;
    }

    public void passWordShow(boolean isChecked,EditText view){
        if (isChecked) {
            view.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }
        else {
            view.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }

}
