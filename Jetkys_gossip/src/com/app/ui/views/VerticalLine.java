package com.app.ui.views;

import jp.gossip.jetkys.android.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by yangshiqin on 15/12/25.
 */
public class VerticalLine extends View{

    Paint p;

    float StrokeWidth,dashWidth,dashGap;
    int drawColor;
    int orientation;

    public VerticalLine(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.VerticalLine);
        StrokeWidth = ta.getDimension(R.styleable.VerticalLine_StrokeWidth,1.0f);
        dashWidth = ta.getDimension(R.styleable.VerticalLine_dashWidth,1.0f);
        dashGap = ta.getDimension(R.styleable.VerticalLine_dashGap, 1.0f);
        drawColor= ta.getColor(R.styleable.VerticalLine_drawColor, Color.WHITE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        p.setStyle(Paint.Style.STROKE);
        p.setColor(drawColor);
        p.setStrokeWidth(StrokeWidth);

        float num = 0;
        for(num = 0; num < getHeight();) {
            canvas.drawLine(getWidth() / 2 , num, getWidth() / 2, num + dashWidth, p);
            num += dashWidth + dashGap;
        }
    }
}
