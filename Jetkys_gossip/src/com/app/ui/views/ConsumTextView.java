package com.app.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import com.app.utils.ZLog;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by yangshiqin on 16/2/24.
 */
public class ConsumTextView extends View {

    String[] content;
    float fontSize = 20;
    int dirtection = 0; // 0:h ,1:v

    float textScaleX = 1.15f;


    public ConsumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ConsumTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ConsumTextView(Context context) {
        super(context);
    }

    public void setContentAndSizeD(String ShowStr, float fontSize, int dirtection) {
        ZLog.e("ShowStr="+ShowStr+" fontSize="+fontSize+"  dirtection="+dirtection);
        content = ShowStr.split("\n");
        this.fontSize = fontSize;
        this.dirtection = dirtection;
        invalidate();
    }

    public void setContent(String showStr) {
        content = showStr.split("\n");
        invalidate();
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
        invalidate();
    }

    public void setDirtection(int dir) {
        this.dirtection = dir;
        invalidate();
    }

    Rect bounds1 = new Rect();
    Rect bounds2 = new Rect();

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (content == null) {
            return;
        }
        Rect targetRect = new Rect(0, 0, getWidth(), getHeight());
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(3);
        paint.setTextSize(fontSize);


        paint.getTextBounds("测", 0, 1, bounds1);
//        paint.setColor(Color.CYAN);
//        canvas.drawRect(targetRect, paint);
        paint.setColor(Color.WHITE);
        Paint.FontMetricsInt fontMetrics = paint.getFontMetricsInt();
        // 转载请注明出处：http://blog.csdn.net/hursing
        int baseline = (targetRect.bottom + targetRect.top - fontMetrics.bottom - fontMetrics.top) / 2;
        // 下面这行是实现水平居中，drawText对应改为传入targetRect.centerX()
        paint.setTextAlign(Paint.Align.CENTER);

        if (dirtection == 0) {
            int len = content.length;
            float startY = baseline - (len - 1) * (bounds1.bottom - bounds1.top) * textScaleX / 2;

            for (int i = 0; i < len; i++) {
                String strindex = content[i];
                float startX = (getWidth() - getTextWidth(strindex,paint)) / 2 + getOneFontW(strindex.substring(0,1),paint)/2;
                for (int index = 0; index < strindex.length(); index++) {
                    String textstr = strindex.substring(index, index + 1);
                    canvas.drawText(textstr, startX, startY, paint);
                    if(index != strindex.length() - 1) {
                        startX += (getOneFontW(textstr, paint) * textScaleX + getOneFontW(strindex.substring(index + 1, index + 2), paint) * textScaleX) / 2;
                    }
                }
                startY += (bounds1.bottom - bounds1.top) * textScaleX;
            }

        } else {
            int len = content.length;
            float startX = (getWidth() + (len - 1) * (bounds1.right - bounds1.left) * textScaleX) / 2;
            for (int i = 0; i < len; i++) {
                String strIndex = content[i];
                float startY = baseline - (strIndex.length() - 1) * (bounds1.bottom - bounds1.top) * textScaleX / 2;
                for (int index = 0; index < strIndex.length(); index++) {
                    canvas.drawText(strIndex.substring(index, index + 1), startX, startY, paint);
                    startY += (bounds1.bottom - bounds1.top) * textScaleX;
                }
                startX -= (bounds1.right - bounds1.left) * textScaleX;
            }
        }


    /*    canvas.drawText("测", 100, 100, paint);
        canvas.drawRect(100,100,150,150,paint);

        canvas.drawText("1", 200, 200, paint);
        canvas.drawRect(200,200,250,250,paint);*/
//        canvas.drawLine(0,getHeight()/2,getWidth(),getHeight()/2,paint);
//        String textstr = content[0];
//        ZLog.e("textstr="+textstr);
//        for(int i = 0;i < textstr.length();i++) {
//            canvas.drawText(textstr.substring(i, i + 1), targetRect.centerX(), baseline   , paint);
//            baseline += (bounds1.bottom - bounds1.top)*1.2f;
//        }
    }

    public float getOneFontW(String text,Paint paint){
        float fontW;
        paint.getTextBounds(text,0,text.length(),bounds2);
////        if(isEn(text)){
            fontW = bounds2.right - bounds2.left;
//        }else{
//            fontW = bounds1.right - bounds1.left;
//        }
        return fontW;
    }

    public float getTextWidth(String text,Paint paint) {
        float len = 0;
        for(int i = 0;i < text.length();i++){
            String str = text.substring(i, i + 1);
            float fontW = getOneFontW(str,paint);
            if(i == text.length() -1){
                len += fontW;
            }else{
                len += fontW*textScaleX;
            }
        }
        return len;
    }

    public boolean isEn(String text) {
        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(text);
        if (m.matches())
        {
            return true;
        }

        p = Pattern.compile("[a-zA-Z]");
        m = p.matcher(text);
        if (m.matches()) {
            return true;
        }
        return false;
    }



  /*  public boolean isEN(String str){
        Pattern p= Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);

    }*/
}
