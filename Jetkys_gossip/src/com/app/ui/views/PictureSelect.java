package com.app.ui.views;

import java.io.File;

import jp.gossip.jetkys.android.R;

import com.app.utils.FileUtils;
import com.app.utils.Utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.PopupWindow;

public class PictureSelect extends PopupWindow
{
	View layout;
	Activity activity;
	Uri cameraUri = null;
	boolean isMultipleSeled = false;
	int selectCount = 0;

	/**
	 * 设置已选数量
	 * @param selectCount
	 */
	public void setSelectCount(int selectCount)
	{
		this.selectCount = selectCount;
		isMultipleSeled = true;
	}

	public PictureSelect(Activity activity) {
		this(activity, null);
	}

	public PictureSelect(Activity activity, OnClickListener onclick) {
		super(activity);
		this.activity = activity;
		initWindow();
		if (onclick != null)
		{
			this.click = onclick;
		}
	}

	private void initWindow()
	{
		layout = LayoutInflater.from(activity).inflate(R.layout.picture_select, null);
		View out = layout.findViewById(R.id.out);
		Button btn_camera = (Button) layout.findViewById(R.id.camera);
		Button btn_photo_album = (Button) layout.findViewById(R.id.photo_album);
		Button btn_cancel = (Button) layout.findViewById(R.id.cancel);
		btn_camera.setOnClickListener(click);
		btn_photo_album.setOnClickListener(click);
		btn_cancel.setOnClickListener(click);
		this.setContentView(layout);

		Drawable background = new ColorDrawable(0x99000000);
		this.setBackgroundDrawable(background);
		this.setFocusable(true);
		this.setWidth(LayoutParams.MATCH_PARENT);
		this.setHeight(LayoutParams.WRAP_CONTENT);
		this.setAnimationStyle(R.style.picture_select_animStyle);
		out.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				PictureSelect.this.dismiss();
				return false;
			}
		});
	}

	private OnClickListener click = new OnClickListener() {

		@Override
		public void onClick(View v)
		{

			switch (v.getId())
			{
			case R.id.cancel:

				break;
			case R.id.camera:
				Intent intentFromCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				// 判断存储卡是否可以用，可用进行存储
				String state = Environment.getExternalStorageState();
				if (state.equals(Environment.MEDIA_MOUNTED))
				{
					File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
					File file = new File(path, FileUtils.getDefaultFileName());
					cameraUri = Uri.fromFile(file);
					intentFromCapture.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
				}
//				Log.d("weiquan ", "拍照");
				activity.startActivityForResult(intentFromCapture, Utils.REQUEST_PICK_PHOTO);
				break;
			case R.id.photo_album:
			
					Intent intentFromGallery = new Intent();
					intentFromGallery.setType("image/*"); // 设置文件类型
					intentFromGallery.setAction(Intent.ACTION_GET_CONTENT);
//					Log.d("weiquan ", "相册");
					activity.startActivityForResult(intentFromGallery, Utils.REQUEST_TAKE_PHOTO);
				break;
			}
			PictureSelect.this.dismiss();
		}
	};

	public Uri getCameraUri()
	{
		return cameraUri;
	}
}
