package com.app.ui.fragment;

import jp.gossip.jetkys.android.R;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.app.net.InterfaceIds;
import com.app.net.NetConst;
import com.app.net.NetResult;
import com.app.net.controller.BaseController;
import com.app.net.controller.UIDelegate;

public class BaseNetFragment extends BaseFragment  implements UIDelegate{

	protected ProgressDialog mLoadingDialog = null;
	
	@Override
	public void findViews() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addListeners() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * dismiss loading dialog
	 */
	@Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		dismissDialog();
		if (ifId == InterfaceIds.USR_LOGIN.IF_ID ) {
            // if login and register, initialize user object
			/*User user = ShareInstance.shareInstance.getUser(getActivity());
            User retUser = (User) obj.getResultObject();
            retUser.setPassword(user.getPassword());
		  	ShareInstance.instance(getActivity()).setUser(getActivity(),retUser);*/
        }
	}

	Dialog offlineDialog;
	
	/**
	 * dismiss loading dialog & toast errmsg
	 */
	@Override
	public void onRequestError(int ifId, NetResult obj) {
		
		if(obj.getResultCode()  == 5000) // token 过期
		{
//			if(offlineDialog == null  ||  !offlineDialog.isShowing()){
//				offlineDialog = DialogUtil.ShowOfflineDialog(getActivity());
//			}
		}else{
			dismissDialog();
		if(obj.getMessage().equals(NetConst.ERR_OUT_OF_BALANCE) || obj.getMessage().equals(NetConst.ERR_MSG_SERVER_ERR)) {
//			showOutOfBalanceDlg();
		} else {
			showToast(obj.getMessage());
			System.out.println("errMsg=" + obj.getMessage());
		}
		}
	}

	public void showDialog() {
		if (isHidden()) {
			return;
		}

		if (mLoadingDialog == null) {
			mLoadingDialog = ProgressDialog.show(getActivity(), "", getResources().getString(R.string.hint_requesting_please_wait));
		} else {
			mLoadingDialog.setMessage(getResources().getString(R.string.hint_requesting_please_wait));
			mLoadingDialog.show();
		}
	}

	public void dismissDialog() {
		if (isHidden()) {
			return;
		}

		if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
			mLoadingDialog.dismiss();
		}
	}

	public void clearController(BaseController controller) {
		if (controller != null) {
			controller.clear();
			controller = null;
		}
	}

	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		// TODO Auto-generated method stub
		return super.onCreateHeader(inflater, container);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		dismissDialog();
	}

	@Override
	public void onPause() {
		super.onPause();
		dismissDialog();
	}



}
