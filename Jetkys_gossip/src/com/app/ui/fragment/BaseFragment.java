package com.app.ui.fragment;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.app.utils.ZLog;
import jp.gossip.jetkys.android.R;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import jp.gossip.jetkys.android.activity.PostTextActivity;
import jp.gossip.jetkys.android.fragment.PersonalFragment;
import jp.gossip.jetkys.android.fragment.ShopFragment;

public abstract class BaseFragment extends Fragment implements View.OnClickListener{
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private Handler mHandler;
	private ViewGroup mContentContainer;
	ViewGroup headerContainer;

	LinearLayout buttomLayout;
	ImageView homePresentIV,homeNewsIv,homemyPageIv,homeJoinIv,homePostIv;
	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mHandler = new Handler();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/** common header **/
		View view = inflater.inflate(R.layout.fragment_base, container, false);

		/** setup header **/
		headerContainer = (ViewGroup) view
				.findViewById(R.id.header_container);
		View header = onCreateHeader(inflater, headerContainer);
		headerContainer.removeAllViews();
		headerContainer.addView(header);

		/** setup content **/
		mContentContainer = (ViewGroup) view
				.findViewById(R.id.content_container);
		View content = onCreateContent(inflater, mContentContainer,
				savedInstanceState);
		mContentContainer.removeAllViews();
		mContentContainer.addView(content);

		buttomLayout = (LinearLayout) view.findViewById(R.id.buttom_layout);
		homePresentIV = (ImageView) view.findViewById(R.id.iv_home_present);
		homeNewsIv = (ImageView) view.findViewById(R.id.iv_home_news);
		homemyPageIv = (ImageView) view.findViewById(R.id.iv_home_mypage);
		homeJoinIv = (ImageView) view.findViewById(R.id.iv_home_join);
		homePostIv = (ImageView) view.findViewById(R.id.iv_home_post);

		homePresentIV.setOnClickListener(this);
		homeNewsIv.setOnClickListener(this);
		homemyPageIv.setOnClickListener(this);
		homeJoinIv.setOnClickListener(this);
		homePostIv.setOnClickListener(this);

		return view;
	}

	protected void setHeaderBackground(int res){
        headerContainer.setBackgroundResource(res);
    }

    protected void setHeaderBackgroundColor(int color){
        headerContainer.setBackgroundColor(color);
    }
	
	/** for content **/
	 protected View onCreateContent(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		return null;
	}

	/** for header **/
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		return null;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	  public void showToast(String s) {
	        Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
	    }

	    public void showToast(int r) {
	        Toast.makeText(getActivity(), r, Toast.LENGTH_LONG).show();
	    }

	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	  public abstract void findViews();

	    public abstract void init();

	    public abstract void addListeners();

	@Override
	public void onClick(View arg0) {
		ZLog.v("arg0="+arg0.getId());
		if (arg0 == homeJoinIv) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ShopFragment fragment = new ShopFragment();
			ft.add(R.id.container, fragment);
			ft.addToBackStack(null);
			ft.commit();
		} else if (arg0 == homePostIv) {
			Intent intent = new Intent(getActivity(), PostTextActivity.class);
			startActivity(intent);
		} else if (arg0 == homeNewsIv) {

		} else if (arg0 == homemyPageIv) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			PersonalFragment fragment = new PersonalFragment();
			ft.add(R.id.container, fragment);
			ft.addToBackStack(null);
			ft.commit();
		} else if (arg0 == homePresentIV) {

		}
	}
}
