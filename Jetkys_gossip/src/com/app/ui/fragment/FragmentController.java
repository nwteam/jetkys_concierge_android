package com.app.ui.fragment;
/**
 * 应用
 * mFragmentController = new FragmentController(getSupportFragmentManager(), R.id.realtabcontent);
 *mFragmentController.add(HomeFragment.class, "home", null);
 * */
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class FragmentController {

	private FragmentManager fragmentManager;
	private int resource;

	public static String[] fragmentTags = { "top", "classic", "chat", "callcenter" };

	private Fragment currentFragment;

	public FragmentController(FragmentManager fragmentManager, int resource) {
		this.resource = resource;
		this.fragmentManager = fragmentManager;
	}

	public void add(Class<? extends Fragment> clazz, String tag, Bundle bundle) {
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		for (String tagName : fragmentTags) {
			Fragment fragment = fragmentManager.findFragmentByTag(tagName);
			if (fragment != null) {
				transaction.hide(fragment);
			}
		}
		Fragment fragment = fragmentManager.findFragmentByTag(tag);
		// transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
		currentFragment = null;
		if (fragment != null) {
			currentFragment = fragment;
			transaction.show(fragment);
		} else {
			try {
				fragment = clazz.newInstance();
				currentFragment = fragment;
				transaction.add(resource, fragment, tag);
				if (bundle != null) {
					fragment.setArguments(bundle);
				}
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		// transaction.commit();
		transaction.commitAllowingStateLoss();
	}

	public void addBackFragment(Fragment mFragment){
		FragmentManager fragmentManager1 = getCurrentFragment().getChildFragmentManager();
		FragmentTransaction ft = fragmentManager1
				.beginTransaction();
		ft.add(resource, mFragment);
		ft.addToBackStack(null);
		ft.commit();
	}
	
	public Fragment getCurrentFragment() {
		return currentFragment;
	}

}
