package com.app.ui.fragment;

import jp.gossip.jetkys.android.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import com.app.AppManager;

public class NavBaseFragment extends BaseNetFragment{

	@Override
	public void findViews() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addListeners() {
		// TODO Auto-generated method stub
		
	}
	
	 public void hideTitleBar(){
	    	headerContainer.setVisibility(View.GONE);
	    }
	
	 private ImageButton mBackIB;
	    private ImageButton mNavMessage;
	    private TextView rightMsg;
	    private TextView mNavTitle;
	    public View viewGroup;
	public View headView;
	    
	   @Override
		protected View onCreateHeader(LayoutInflater inflater,
				ViewGroup container) {
			// TODO Auto-generated method stub
		    headView = inflater.inflate(R.layout.activity_nav_layout, container, false);
	        mBackIB = (ImageButton)headView.findViewById(R.id.ib_nav_black);
	        mNavTitle = (TextView)headView.findViewById(R.id.tv_nav_title);
	        rightMsg = (TextView)headView.findViewById(R.id.tv_right);
	        
	        mNavMessage = (ImageButton)headView.findViewById(R.id.im_nav_message);
	        
	        mBackIB.setOnClickListener(new View.OnClickListener() {
	            @Override
	            public void onClick(View view) {
	                backActivity();
	            }
	        });

	        return headView;
		}

	public  void setLeftEnable(boolean show){
	    	if(show){
	    		mBackIB.setVisibility(View.VISIBLE);
	    	}else{
	    		mBackIB.setVisibility(View.GONE);
	    	}
	    }

	public void setLeftImageBar(int resId ,OnClickListener listener){
		mBackIB.setVisibility(View.VISIBLE);
		mBackIB.setImageResource(resId);
		mBackIB.setOnClickListener(listener);
	}

	    public void setRightTitleBar(String msg,OnClickListener listener){
	    	rightMsg.setVisibility(View.VISIBLE);
	    	rightMsg.setText(msg);
	    	rightMsg.setOnClickListener(listener);
	    }

	    public void setRightImageBar(int resId,OnClickListener listener){
	    	mNavMessage.setVisibility(View.VISIBLE);
	    	mNavMessage.setImageResource(resId);
	    	mNavMessage.setOnClickListener(listener);
	    }
	    
	    protected void setNavTitle(String title){
	        mNavTitle.setText(title);
	    }

	    protected void setNavTitle(int res){
	        mNavTitle.setText(res);
	    }

	    protected void messageListener()
	    {

	    }

	    protected void backActivity(){
	        AppManager.getAppManager().finishActivity();
	    }

}
