package jp.gossip.jetkys.android;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import gameutils.encrypt.EncryptData;
import jp.gossip.jetkys.android.manager.SharedManager;

import java.io.*;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by yangshiqin on 15/12/22.
 */
public class MyApplication extends Application implements Thread.UncaughtExceptionHandler {

    private final Thread.UncaughtExceptionHandler originalHandler;

    public MyApplication() {
        super();
        originalHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public File getExternalRoot() {
        File retVal = Environment.getExternalStorageDirectory();
        if (retVal != null) {
            retVal = new File(retVal, this.getPackageName());
            if (!retVal.exists() && !retVal.mkdir()) {
                retVal = null;
            }
        }
        return retVal;
    }

    // 用于格式化日期,作为日志文件名的一部分
    private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        try{
            File fold = this.getExternalRoot();
            SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH.mm.ss.SSS", Locale.CHINA);
            String fileName = "crash " + sdf.format( new Date() );
            File file = new File( fold, fileName );
            FileOutputStream fos = new FileOutputStream( file );
            PrintWriter ps = new PrintWriter( fos );

            logcat(ps);
            throwable.printStackTrace( ps );
            ps.close();
        }catch( Exception e ){
            Log.e(Application.class.getName(), "Can not save exception.", e);
        }finally{
            if( !handleException(throwable) && this.originalHandler != null ){
                this.originalHandler.uncaughtException( thread, throwable );
            }else{
                Log.e( this.getPackageName(), "Thread " + thread.getName() + "uncaught exception.", throwable );
            	try {
    				Thread.sleep(3800);
    			} catch (InterruptedException e) {
    				Log.e( this.getPackageName(), "error : ", e);
    			}
    			// 退出程序
    			exitProgram();
            }
        }
    }


	/*
	 * 这里由UploadLogTask类来回调
	 */
	public void exitProgram() {
		// 退出程序
		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(1);
	}
    
    // 用来存储设备信息和异常信息
    private Map<String, String> infos = new HashMap<String, String>();

    /**
     * 收集设备参数信息
     *
     * @param ctx
     */
    public void collectDeviceInfo(Context ctx) {

        try {
            PackageManager pm = ctx.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(ctx.getPackageName(),
                    PackageManager.GET_ACTIVITIES);
            if (pi != null) {
                String versionName = pi.versionName == null ? "null"
                        : pi.versionName;
                String versionCode = pi.versionCode + "";
                infos.put("versionName", versionName);
                infos.put("versionCode", versionCode);
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("", "an error occured when collect package info", e);
        }
        Field[] fields = Build.class.getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                infos.put(field.getName(), field.get(null).toString());
                // Log.d(TAG, field.getName() + " : " + field.get(null));
            } catch (Exception e) {
                Log.e("", "an error occured when collect crash info", e);
            }
        }

    }

    /**
     * 自定义错误处理,收集错误信息 发送错误报告等操作均在此完成.
     *
     * @param ex
     * @return true:如果处理了该异常信息;否则返回false.
     */
    private boolean handleException(Throwable ex) {
        if (ex == null) {
            return false;
        }
        // 使用Toast来显示异常信息
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                String info = "システムエラーです。";
               /* if (logInTxt) {
                    info += "\n请将 /sdcard/huinongbao/log/crash/ 下的错误日志发送给开发人员.";
                }*/
                Toast.makeText(getApplicationContext(), info, Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
        }.start();
        // 收集设备参数信息
        collectDeviceInfo(this);
        saveCrashInfo2File(ex);
        return true;
    }

    /**
     * 保存错误信息到文件中
     *
     * @param ex
     * @return 返回文件名称,便于将文件传送到服务器
     */
    private String saveCrashInfo2File(Throwable ex) {

        StringBuffer sb = new StringBuffer();
        for (Map.Entry<String, String> entry : infos.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            sb.append(key + "=" + value + "\n");
        }

        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        ex.printStackTrace(printWriter);
        Throwable cause = ex.getCause();
        while (cause != null) {
            cause.printStackTrace(printWriter);
            cause = cause.getCause();
        }
        printWriter.close();
        String result = writer.toString();

        sb.append(result);
        try {
            long timestamp = System.currentTimeMillis();
            String time = formatter.format(new Date());
            String fileName = "crash-" + time + "-" + timestamp + ".log";
            if (Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                String path = appFileDir + "log/crash/";
                File dir = new File(path);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                FileOutputStream fos = new FileOutputStream(path + fileName);
                fos.write(sb.toString().getBytes());
                fos.close();
            }
            return fileName;
        } catch (Exception e) {
            Log.e("", "an error occured while writing file...", e);
        }

        return null;
    }

    private void logcat( PrintWriter ps ){
        try{
            ArrayList< String > commandLine = new ArrayList< String >();
            commandLine.add( "logcat" );
            commandLine.add( "-d" );
            commandLine.add( "-v" );
            commandLine.add( "long" );
            commandLine.add( "*:V" );
            Process process = Runtime.getRuntime().exec( commandLine.toArray( new String[ commandLine.size() ] ) );
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader( process.getInputStream() ) );
            String line;
            String lineSeparator = System.getProperty( "line.separator" );
            while( ( line = bufferedReader.readLine() ) != null ){
                ps.append( line );
                ps.append( lineSeparator );
            }
            bufferedReader.close();
        }catch( IOException e ){
            e.printStackTrace();

        }
    }

    public static String appFileDir;
    @Override
    public void onCreate() {
        super.onCreate();

        appFileDir = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/Jetkys/";
        
        EncryptData.context = this;

        SharedManager.create(this);
    }
    
    public String getUUid(){
    	final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
    	 
        final String tmDevice, tmSerial, tmPhone, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
     
        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String uniqueId = deviceUuid.toString();
        return uniqueId;
    }
}
