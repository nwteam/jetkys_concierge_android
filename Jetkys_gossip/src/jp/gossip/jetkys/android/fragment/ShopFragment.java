package jp.gossip.jetkys.android.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.app.ui.fragment.NavBaseFragment;
import jp.gossip.jetkys.android.R;
import jp.gossip.jetkys.android.activity.HomeActivity;

/**
 * Created by yangshiqin on 16/2/28.
 */
public class ShopFragment extends NavBaseFragment implements View.OnTouchListener,View.OnClickListener{

    ImageView kysShopIv,keyShopIv,kysOrderIv;

    @Override
    public void findViews() {
        super.findViews();
        kysShopIv = (ImageView) viewGroup.findViewById(R.id.kys_shop);
        keyShopIv = (ImageView) viewGroup.findViewById(R.id.key_shop);
        kysOrderIv = (ImageView) viewGroup.findViewById(R.id.record_shop);
    }

    @Override
    public void init() {
        super.init();
        setNavTitle(getString(R.string.shop));
        setLeftImageBar(R.drawable.sub_back, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        headView.setBackgroundColor(getResources().getColor(R.color.gray));

        headView.setOnTouchListener(this);
        viewGroup.setOnTouchListener(this);
    }

    @Override
    public void addListeners() {
        super.addListeners();
        kysOrderIv.setOnClickListener(this);
        keyShopIv.setOnClickListener(this);
        kysShopIv.setOnClickListener(this);

    }

    @Override
    protected View onCreateContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewGroup = inflater.inflate(R.layout.fragment_shop_layout, container, false);
        findViews();
        init();
        addListeners();
        return viewGroup;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return true;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if(v == keyShopIv){
//            ((HomeActivity)getActivity()).gotoKeyShop();

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction ft = fragmentManager
                    .beginTransaction();
            KeyShopFragment shopFragment = new KeyShopFragment();
            ft.add(R.id.container, shopFragment);
            ft.addToBackStack(null);
            ft.commit();

        }else if(v == kysOrderIv){
//            ((HomeActivity)getActivity()).gotoRecordShop();

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction ft = fragmentManager
                    .beginTransaction();
            RecordShopFragment shopFragment = new RecordShopFragment();
            ft.add(R.id.container, shopFragment);
            ft.addToBackStack(null);
            ft.commit();

        }else if (v == kysShopIv){

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction ft = fragmentManager
                    .beginTransaction();
            KysShopFragment shopFragment = new KysShopFragment();
            ft.add(R.id.container, shopFragment);
            ft.addToBackStack(null);
            ft.commit();
//            ((HomeActivity)getActivity()).gotoKysShop();
        }
    }
}
