package jp.gossip.jetkys.android.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.app.ui.fragment.NavBaseFragment;
import jp.gossip.jetkys.android.R;

/**
 * Created by yangshiqin on 16/2/28.
 */
public class KeyShopFragment extends NavBaseFragment implements View.OnTouchListener,View.OnClickListener{

    ImageView kysShopIv;

    @Override
    public void findViews() {
        super.findViews();
        kysShopIv = (ImageView) viewGroup.findViewById(R.id.kys_shop);
    }

    @Override
    public void init() {
        super.init();
        setNavTitle(getString(R.string.key_shop));
        setLeftImageBar(R.drawable.sub_back, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        headView.setBackgroundColor(getResources().getColor(R.color.gray));

        headView.setOnTouchListener(this);
        viewGroup.setOnTouchListener(this);
    }

    @Override
    public void addListeners() {
        super.addListeners();
        kysShopIv.setOnClickListener(this);
    }

    @Override
    protected View onCreateContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewGroup = inflater.inflate(R.layout.fragment_keyshop_layout, container, false);
        findViews();
        init();
        addListeners();
        return viewGroup;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return true;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if(v == kysShopIv){
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction ft = fragmentManager
                    .beginTransaction();
            KysShopFragment shopFragment = new KysShopFragment();
            ft.add(R.id.container, shopFragment);
            ft.addToBackStack(null);
            ft.commit();
        }
    }
}
