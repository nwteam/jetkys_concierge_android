package jp.gossip.jetkys.android.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.app.net.controller.BaseController;
import com.app.ui.fragment.NavBaseFragment;
import jp.gossip.jetkys.android.R;
import jp.gossip.jetkys.android.activity.HomeActivity;

/**
 * Created by yangshiqin on 16/2/28.
 */
public class PersonalFragment extends NavBaseFragment implements View.OnTouchListener,View.OnClickListener{

    TextView nickNameTv,gskysTv,IdTv;
    ImageView photoeditIv,profeditIv,newPresentIv,newNewsIv;
    RelativeLayout shopRl,newsRl,presentRl,passbookRl,configRl,helpRl;


    @Override
    public void findViews() {
        super.findViews();
        nickNameTv = (TextView) viewGroup.findViewById(R.id.nick_name_tv);
        gskysTv = (TextView) viewGroup.findViewById(R.id.gskys_tv);
        IdTv = (TextView) viewGroup.findViewById(R.id.ID_tv);
        photoeditIv = (ImageView) viewGroup.findViewById(R.id.photoedit_iv);
        profeditIv = (ImageView) viewGroup.findViewById(R.id.profedit_iv);
        newPresentIv = (ImageView) viewGroup.findViewById(R.id.new_present_iv);
        newNewsIv = (ImageView) viewGroup.findViewById(R.id.new_news_iv);

        shopRl = (RelativeLayout) viewGroup.findViewById(R.id.shop_ll);
        newsRl = (RelativeLayout) viewGroup.findViewById(R.id.news_ll);
        presentRl = (RelativeLayout) viewGroup.findViewById(R.id.present_ll);
        passbookRl = (RelativeLayout) viewGroup.findViewById(R.id.passbook_ll);
        configRl = (RelativeLayout) viewGroup.findViewById(R.id.config_ll);
        helpRl = (RelativeLayout) viewGroup.findViewById(R.id.help_ll);
    }

    @Override
    public void init() {
        super.init();
        setNavTitle(getString(R.string.personal_title));
        setLeftImageBar(R.drawable.sub_back, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        headView.setBackgroundColor(getResources().getColor(R.color.gray));

        headView.setOnTouchListener(this);
        viewGroup.setOnTouchListener(this);
    }

    @Override
    public void addListeners() {
        super.addListeners();

        photoeditIv.setOnClickListener(this);
        profeditIv.setOnClickListener(this);
        shopRl.setOnClickListener(this);
        newsRl.setOnClickListener(this);
        presentRl.setOnClickListener(this);
        passbookRl.setOnClickListener(this);
        configRl.setOnClickListener(this);
        helpRl.setOnClickListener(this);

    }

    @Override
    protected View onCreateContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewGroup = inflater.inflate(R.layout.fragment_persional_layout, container, false);
        findViews();
        init();
        addListeners();
        return viewGroup;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return true;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if(v == photoeditIv){

        }else if(v == profeditIv){

        }else if(v == shopRl){
//            ((HomeActivity)getActivity()).gotoShop();

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ShopFragment fragment = new ShopFragment();
            ft.add(R.id.container, fragment);
            ft.addToBackStack(null);
            ft.commit();

        }else if(v == newsRl){

        }else if(v == presentRl){

        }else if(v == passbookRl){

        }else if(v == configRl){

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            SettingFragment fragment = new SettingFragment();
            ft.add(R.id.container, fragment);
            ft.addToBackStack(null);
            ft.commit();

        }else if( v == helpRl){

        }
    }
}
