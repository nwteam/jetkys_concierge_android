package jp.gossip.jetkys.android.fragment;

import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ListView;
import jp.gossip.jetkys.android.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.app.ui.fragment.NavBaseFragment;
import jp.gossip.jetkys.android.activity.FriendCertificationActivity;
import jp.gossip.jetkys.android.adapter.FriendListAdapter;
import jp.gossip.jetkys.android.mode.FriendInfo;

import java.util.ArrayList;

public class ChatFragment extends NavBaseFragment implements AdapterView.OnItemClickListener{

	ArrayList<FriendInfo> mFirendLists;

	ListView mListView;
	FriendListAdapter mAdapter;
	@Override
	public void findViews() {
		// TODO Auto-generated method stub
		super.findViews();
		mListView = (ListView) viewGrop.findViewById(R.id.listView_chat);
	}

	void test(){
		FriendInfo info = new FriendInfo();
		info.setName("abc");
		info.setSex(0);
		info.setStatus(0);

		mFirendLists.add(info);

		info = new FriendInfo();
		info.setName("f  abc");
		info.setSex(0);
		info.setStatus(0);

		mFirendLists.add(info);

		info = new FriendInfo();
		info.setName("e hello a");
		info.setSex(1);
		info.setStatus(1);
		info.setUnReadNum(0);
		mFirendLists.add(info);

		info = new FriendInfo();
		info.setName("b hello c");
		info.setSex(1);
		info.setStatus(1);
		info.setUnReadNum(3);
		mFirendLists.add(info);

		info = new FriendInfo();
		info.setName("d hello c");
		info.setSex(0);
		info.setStatus(2);
		mFirendLists.add(info);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		hideTitleBar();

		mFirendLists = new ArrayList<FriendInfo>();
		test();

		mAdapter = new FriendListAdapter(getActivity(),mFirendLists);
		mListView.setAdapter(mAdapter);

		mListView.setOnItemClickListener(this);
	}

	@Override
	public void addListeners() {
		// TODO Auto-generated method stub
		super.addListeners();
	}

	View viewGrop;
	@Override
	protected View onCreateContent(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		viewGrop = inflater.inflate(R.layout.fragment_chat_layout, container, false);
		findViews();
		init();
		addListeners();
        return viewGrop;
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		Object obj = mListView.getAdapter().getItem(i);
		if(obj instanceof FriendInfo){
			FriendInfo info = (FriendInfo) obj;
			if(info.getStatus() == 0){
				Intent intent = new Intent(getActivity(), FriendCertificationActivity.class);
				intent.putExtra("friend",info);
				startActivity(intent);
			}
		}
	}
}
