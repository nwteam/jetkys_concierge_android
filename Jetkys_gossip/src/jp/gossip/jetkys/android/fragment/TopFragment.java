package jp.gossip.jetkys.android.fragment;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import android.widget.*;
import com.app.utils.DisplayUtil;
import jp.gossip.jetkys.android.R;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.app.ui.fragment.NavBaseFragment;
import com.app.ui.views.PictureSelect;
import com.app.utils.FileUtils;
import com.app.utils.Utils;
import com.app.utils.ZLog;
import jp.gossip.jetkys.android.activity.PostDetailActivity;
import jp.gossip.jetkys.android.adapter.PostAdapter;
import jp.gossip.jetkys.android.mode.PostInfo;

public class TopFragment extends NavBaseFragment implements AdapterView.OnItemClickListener{

	GridView mGridView;
	PostAdapter adapter;
	ArrayList<PostInfo> postInfos;

	@Override
	public void findViews() {
		// TODO Auto-generated method stub
		super.findViews();
		mGridView = (GridView) viewGrop.findViewById(R.id.gridView);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		hideTitleBar();
		mGridView.setHorizontalSpacing(DisplayUtil.dp2px(getActivity(), 5));
		mGridView.setVerticalSpacing(DisplayUtil.dp2px(getActivity(), 5));
		testData();
		adapter = new PostAdapter(getActivity(),postInfos);
		mGridView.setAdapter(adapter);
	}

	public void testData(){
		postInfos = new ArrayList<PostInfo>();
		PostInfo info = new PostInfo();
		info.setDirtection(0);
		info.setFontSize(60);
		info.setShowStr("adbcjhioas\noasd");
		info.setImageRes(R.drawable.bg_1);
		info.setWidth(508);
		info.setHeight(360);
		postInfos.add(info);

		info = new PostInfo();
		info.setDirtection(1);
		info.setFontSize(70);
		info.setShowStr("adbcj\nhioas\noasd");
		info.setImageRes(R.drawable.bg_2);
		info.setWidth(724);
		info.setHeight(1280);
		postInfos.add(info);

		info = new PostInfo();
		info.setDirtection(0);
		info.setFontSize(70);
		info.setShowStr("adbcjhioas\noasd");
		info.setImageRes(R.drawable.bg_3);
		info.setWidth(720);
		info.setHeight(1280);

		postInfos.add(info);
		info = new PostInfo();
		info.setDirtection(1);
		info.setFontSize(70);
		info.setShowStr("adbcj\nhioas\noasd");
		info.setImageRes(R.drawable.bg_4);
		info.setWidth(600);
		info.setHeight(450);
		postInfos.add(info);

		info = new PostInfo();
		info.setDirtection(0);
		info.setFontSize(70);
		info.setShowStr("adbcjhioas\noasd");
		info.setImageRes(R.drawable.bg_1);
		info.setWidth(720);
		info.setHeight(1280);

		postInfos.add(info);
		info = new PostInfo();
		info.setDirtection(1);
		info.setFontSize(70);
		info.setShowStr("adbcj\nhioas\noasd");
		info.setImageRes(R.drawable.bg_3);
		info.setWidth(600);
		info.setHeight(450);
		postInfos.add(info);
	}

	@Override
	public void addListeners() {
		// TODO Auto-generated method stub
		super.addListeners();
		mGridView.setOnItemClickListener(this);
	}

	View viewGrop;
	@Override
	protected View onCreateContent(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		viewGrop = inflater.inflate(R.layout.fragment_top_layout, container, false);
		findViews();
		init();
		addListeners();
        return viewGrop;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		super.onClick(arg0);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		Intent intent = new Intent(getActivity(), PostDetailActivity.class);
		intent.putExtra("post", (Serializable) adapterView.getItemAtPosition(i));
		startActivity(intent);
	}

//	/**
//	 * 处理用户头像图片
//	 * 
//	 * @param requestCode
//	 * @param resultCode
//	 * @param data
//	 */
//	private void disposeUserImage(int requestCode, int resultCode, Intent data) {
//		String urifile;
//		if (requestCode == Utils.REQUEST_PICK_PHOTO) {
//			if (mPictureSelect.getCameraUri() == null) {
//				Toast.makeText(getActivity(), "此次未选择图片！", Toast.LENGTH_SHORT).show();
//				return;
//			}
//			urifile = mPictureSelect.getCameraUri().toString().substring(6, mPictureSelect.getCameraUri().toString().length());
//		} else {
//			if (data == null) {
//				Toast.makeText(getActivity(), "此次未选择图片！", Toast.LENGTH_SHORT).show();
//				return;
//			}
//			urifile = FileUtils.getRealFilePath(getActivity(), data.getData());
//		}
//		if (urifile == null) {
//			Toast.makeText(getActivity(), "此次未选择图片！", Toast.LENGTH_SHORT).show();
//			return;
//		}
//		file = new File(urifile);
//		Bitmap bitmap = BitmapFactory.decodeFile(urifile);
//		if (bitmap != null) {
//			Bitmap bitmap2 = Utils.scaleImageTo(bitmap, 105, 105);
//			imageView.setImageBitmap(bitmap2);
//		}
//	}
//
//	File file;
}
