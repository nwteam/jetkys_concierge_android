package jp.gossip.jetkys.android.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.app.ui.fragment.NavBaseFragment;
import jp.gossip.jetkys.android.R;

/**
 * Created by yangshiqin on 16/4/7.
 */
public class SettingFragment  extends NavBaseFragment implements View.OnTouchListener{

    @Override
    protected View onCreateContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewGroup = inflater.inflate(R.layout.fragment_setting_layout, container, false);
        findViews();
        init();
        addListeners();
        return viewGroup;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return true;
    }

    @Override
    public void init() {
        super.init();
        setNavTitle(getString(R.string.setting));
        setLeftImageBar(R.drawable.sub_back, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        headView.setBackgroundColor(getResources().getColor(R.color.gray));

        headView.setOnTouchListener(this);
        viewGroup.setOnTouchListener(this);
    }


    @Override
    public void findViews() {
        super.findViews();
    }

    @Override
    public void onClick(View arg0) {
        super.onClick(arg0);
    }

    @Override
    public void addListeners() {
        super.addListeners();
    }
}
