package jp.gossip.jetkys.android.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.app.ui.fragment.NavBaseFragment;
import jp.gossip.jetkys.android.R;

/**
 * Created by yangshiqin on 16/2/28.
 */
public class KysShopFragment extends NavBaseFragment implements View.OnTouchListener,View.OnClickListener{

    @Override
    public void findViews() {
        super.findViews();
    }

    @Override
    public void init() {
        super.init();
        setNavTitle(getString(R.string.kys_shop));
        setLeftImageBar(R.drawable.sub_back, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        headView.setBackgroundColor(getResources().getColor(R.color.gray));

        headView.setOnTouchListener(this);
        viewGroup.setOnTouchListener(this);
    }

    @Override
    public void addListeners() {
        super.addListeners();
    }

    @Override
    protected View onCreateContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewGroup = inflater.inflate(R.layout.fragment_kysshop_layout, container, false);
        findViews();
        init();
        addListeners();
        return viewGroup;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return true;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
    }
}
