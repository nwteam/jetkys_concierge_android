package jp.gossip.jetkys.android.fragment;

import android.widget.AdapterView;
import android.widget.GridView;
import com.app.utils.DisplayUtil;
import jp.gossip.jetkys.android.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.app.ui.fragment.NavBaseFragment;
import jp.gossip.jetkys.android.adapter.PostAdapter;
import jp.gossip.jetkys.android.mode.PostInfo;

import java.util.ArrayList;

public class RankFragment extends NavBaseFragment implements AdapterView.OnItemClickListener{

	GridView mGridView;
	PostAdapter adapter;
	ArrayList<PostInfo> postInfos;

	@Override
	public void findViews() {
		// TODO Auto-generated method stub
		super.findViews();
		mGridView = (GridView) viewGrop.findViewById(R.id.gridView);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		hideTitleBar();
		mGridView.setHorizontalSpacing(DisplayUtil.dp2px(getActivity(), 5));
		mGridView.setVerticalSpacing(DisplayUtil.dp2px(getActivity(), 5));
		testData();
		adapter = new PostAdapter(getActivity(),postInfos);
		mGridView.setAdapter(adapter);
	}

	public void testData(){
		postInfos = new ArrayList<PostInfo>();
		PostInfo info = new PostInfo();
		info.setDirtection(0);
		info.setFontSize(60);
		info.setShowStr("adbcjhioas\noasd");
		info.setImageRes(R.drawable.bg_1);
		info.setWidth(508);
		info.setHeight(360);
		postInfos.add(info);

		info = new PostInfo();
		info.setDirtection(1);
		info.setFontSize(70);
		info.setShowStr("adbcj\nhioas\noasd");
		info.setImageRes(R.drawable.bg_2);
		info.setWidth(724);
		info.setHeight(1280);
		postInfos.add(info);

		info = new PostInfo();
		info.setDirtection(0);
		info.setFontSize(70);
		info.setShowStr("adbcjhioas\noasd");
		info.setImageRes(R.drawable.bg_3);
		info.setWidth(720);
		info.setHeight(1280);

		postInfos.add(info);
		info = new PostInfo();
		info.setDirtection(1);
		info.setFontSize(70);
		info.setShowStr("adbcj\nhioas\noasd");
		info.setImageRes(R.drawable.bg_4);
		info.setWidth(600);
		info.setHeight(450);
		postInfos.add(info);
	}

	@Override
	public void addListeners() {
		// TODO Auto-generated method stub
		super.addListeners();
		mGridView.setOnItemClickListener(this);
	}

	View viewGrop;
	@Override
	protected View onCreateContent(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		viewGrop = inflater.inflate(R.layout.fragment_rank_layout, container, false);
		findViews();
		init();
		addListeners();
        return viewGrop;
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

	}
}
