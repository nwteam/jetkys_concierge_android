package jp.gossip.jetkys.android.fragment;

import jp.gossip.jetkys.android.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.app.ui.fragment.NavBaseFragment;

public class CallCenterFragment extends NavBaseFragment{

	@Override
	public void findViews() {
		// TODO Auto-generated method stub
		super.findViews();
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		hideTitleBar();
	}

	@Override
	public void addListeners() {
		// TODO Auto-generated method stub
		super.addListeners();
	}

	View viewGrop;
	@Override
	protected View onCreateContent(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		viewGrop = inflater.inflate(R.layout.fragment_callcenter_layout, container, false);
		findViews();
		init();
		addListeners();
        return viewGrop;
	}

}
