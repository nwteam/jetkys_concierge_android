package jp.gossip.jetkys.android.fragment;

import android.content.Intent;
import android.view.MotionEvent;
import android.widget.*;
import com.app.utils.DisplayUtil;
import jp.gossip.jetkys.android.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.app.ui.fragment.NavBaseFragment;
import jp.gossip.jetkys.android.activity.PostDetailActivity;
import jp.gossip.jetkys.android.adapter.PostAdapter;
import jp.gossip.jetkys.android.mode.PostInfo;

import java.io.Serializable;
import java.util.ArrayList;

public class ClassicFragment extends NavBaseFragment  implements AdapterView.OnItemClickListener ,View.OnClickListener{

	GridView mGridView;
	PostAdapter adapter;
	ArrayList<PostInfo> postInfos;

	TextView sendTypeTV;

	@Override
	public void findViews() {
		// TODO Auto-generated method stub
		super.findViews();
		mGridView = (GridView) viewGrop.findViewById(R.id.gridView);
		sendTypeTV = (TextView) viewGrop.findViewById(R.id.tv_send_type);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		hideTitleBar();
		mGridView.setHorizontalSpacing(DisplayUtil.dp2px(getActivity(), 5));
		mGridView.setVerticalSpacing(DisplayUtil.dp2px(getActivity(), 5));
		testData();
		adapter = new PostAdapter(getActivity(),postInfos);
		mGridView.setAdapter(adapter);
	}

	public void testData(){
		postInfos = new ArrayList<PostInfo>();
		PostInfo info = new PostInfo();
		info.setDirtection(0);
		info.setFontSize(60);
		info.setShowStr("adbcjhioas\noasd");
		info.setImageRes(R.drawable.bg_1);
		info.setWidth(508);
		info.setHeight(360);
		postInfos.add(info);

		info = new PostInfo();
		info.setDirtection(1);
		info.setFontSize(70);
		info.setShowStr("adbcj\nhioas\noasd");
		info.setImageRes(R.drawable.bg_2);
		info.setWidth(724);
		info.setHeight(1280);
		postInfos.add(info);

		info = new PostInfo();
		info.setDirtection(0);
		info.setFontSize(70);
		info.setShowStr("adbcjhioas\noasd");
		info.setImageRes(R.drawable.bg_3);
		info.setWidth(720);
		info.setHeight(1280);

		postInfos.add(info);
		info = new PostInfo();
		info.setDirtection(1);
		info.setFontSize(70);
		info.setShowStr("adbcj\nhioas\noasd");
		info.setImageRes(R.drawable.bg_4);
		info.setWidth(600);
		info.setHeight(450);
		postInfos.add(info);
	}

	@Override
	public void addListeners() {
		// TODO Auto-generated method stub
		super.addListeners();
		mGridView.setOnItemClickListener(this);
		sendTypeTV.setOnClickListener(this);
	}

	View viewGrop;
	@Override
	protected View onCreateContent(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		viewGrop = inflater.inflate(R.layout.fragment_classic_layout, container, false);
		findViews();
		init();
		addListeners();
        return viewGrop;
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		Intent intent = new Intent(getActivity(), PostDetailActivity.class);
		intent.putExtra("post", (Serializable) adapterView.getItemAtPosition(i));
		startActivity(intent);
	}

	@Override
	public void onClick(View view) {
		super.onClick(view);
		if(view == sendTypeTV){
			showPopupWindow(sendTypeTV);
		}
	}

	private void showPopupWindow(View view) {

		// 一个自定义的布局，作为显示的内容
		View contentView = LayoutInflater.from(getActivity()).inflate(
				R.layout.pop_window, null);

		final String[] strs = new String[] {
				"aa","aab","aac"
		};
		ListView listView = (ListView) contentView.findViewById(R.id.popup_listview);
		listView.setAdapter(new ArrayAdapter<String>(getActivity(),
				R.layout.simple_list_item1, strs));

		final PopupWindow popupWindow = new PopupWindow(contentView,
				AbsoluteLayout.LayoutParams.MATCH_PARENT, AbsoluteLayout.LayoutParams.WRAP_CONTENT, true);

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				sendTypeTV.setText(strs[i]);
				popupWindow.dismiss();
			}
		});

		popupWindow.setTouchable(true);

		popupWindow.setTouchInterceptor(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				return false;
				// 这里如果返回true的话，touch事件将被拦截
				// 拦截后 PopupWindow的onTouchEvent不被调用，这样点击外部区域无法dismiss
			}
		});

		// 如果不设置PopupWindow的背景，无论是点击外部区域还是Back键都无法dismiss弹框
		// 我觉得这里是API的一个bug
		popupWindow.setBackgroundDrawable(getResources().getDrawable(
				R.color.button_bg));

		// 设置好参数之后再show
		popupWindow.showAsDropDown(view);

	}
}
