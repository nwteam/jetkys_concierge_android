package jp.gossip.jetkys.android.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import jp.gossip.jetkys.android.R;

public class TabFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		System.out.println("TabFragment onCreateView");
		View view = inflater.inflate(R.layout.fragment_tab, container, false);
		if (null == savedInstanceState) {
			Bundle data = getArguments();
			int type = 0;
			if (null != data) {
				type = data.getInt("type");
			}

			switch (type) {
			case 0: {
				if (null == getChildFragmentManager().findFragmentByTag(
						"top")) {
					getChildFragmentManager().addOnBackStackChangedListener(
							new OnBackStackChangedListener() {

								@Override
								public void onBackStackChanged() {

								}
							});

					FragmentTransaction ft = getChildFragmentManager()
							.beginTransaction();
					ft.add(R.id.container, new TopFragment(), "top");
					ft.commit();
				}
				break;
			}
			case 1: {
				if (null == getChildFragmentManager().findFragmentByTag(
						"classic")) {
					FragmentTransaction ft = getChildFragmentManager()
							.beginTransaction();
					ft.add(R.id.container, new ClassicFragment(),
							"classic");
					ft.commit();
				}
				break;
			}
			case 2: {
				if (null == getChildFragmentManager().findFragmentByTag(
						"chat")) {
					FragmentTransaction ft = getChildFragmentManager()
							.beginTransaction();
					ft.add(R.id.container, new ChatFragment(),
							"chat");
					ft.commit();
				}
				break;
			}
			case 3: {
				if (null == getChildFragmentManager().findFragmentByTag(
						"callcenter")) {
					FragmentTransaction ft = getChildFragmentManager()
							.beginTransaction();
					ft.add(R.id.container, new CallCenterFragment(),
							"callcenter");
					ft.commit();
				}

				break;
			}
			default:
				break;
			}
		}
		return view;
	}
}
