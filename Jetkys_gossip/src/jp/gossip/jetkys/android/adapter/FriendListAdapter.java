package jp.gossip.jetkys.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import jp.gossip.jetkys.android.HoldView;
import jp.gossip.jetkys.android.R;
import jp.gossip.jetkys.android.mode.FriendInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by yangshiqin on 16/2/28.
 */
public class FriendListAdapter extends BaseAdapter{

    Context mContext;
    ArrayList<FriendInfo> mFirendLists;

    int statusNum = 0;
    int goodNum = 0;
    int pendingNum = 0;

    public FriendListAdapter(Context mContext, ArrayList<FriendInfo> mFirendLists) {
        this.mContext = mContext;
        this.mFirendLists = mFirendLists;
        handleList();

    }

    public ArrayList<FriendInfo> getmFirendLists() {
        return mFirendLists;
    }

    public void setmFirendLists(ArrayList<FriendInfo> mFirendLists) {
        this.mFirendLists = mFirendLists;
        handleList();
        notifyDataSetInvalidated();
    }


    void handleList(){
        Collections.sort(mFirendLists,new SortComparator());
        goodNum = 0;
        pendingNum = 0;
        statusNum = 0;
        for(int i = 0;i < mFirendLists.size();i++){
            FriendInfo info = mFirendLists.get(i);
            if(info.getStatus() == 1){
                goodNum ++;
            }else if(info.getStatus() == 0){
                pendingNum ++;
            }
        }

        if(goodNum != 0){
            statusNum ++;
        }

        if(pendingNum != 0){
            statusNum ++;
        }

        if(mFirendLists.size() > goodNum + pendingNum){
            statusNum ++;
        }

    }


    @Override
    public int getCount() {
        if(mFirendLists != null){
            return mFirendLists.size() + statusNum;
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if(mFirendLists != null){
           if(statusNum == 1){
               if(pendingNum != 0){
                   if(i == 0){
                       return 0;
                   }else{
                       return mFirendLists.get(i -1);
                   }
               }else if(goodNum != 0){
                   if(i == 0){
                       return 1;
                   }else{
                       return mFirendLists.get(i -1);
                   }
               }else{
                   if(i == 0){
                       return 0;
                   }else{
                       return mFirendLists.get(i -1);
                   }
               }
           }else if(statusNum == 2){
               if(pendingNum == 0){
                   if( i ==0 ){
                       return 1;
                   }else if(i == goodNum + 1){
                       return 0;
                   }else{
                       if(i < goodNum + 1) {
                           return mFirendLists.get(i - 1);
                       }else{
                           return mFirendLists.get( i -2);
                       }
                   }
               }else if(goodNum == 0){
                   if( i ==0 ){
                       return 0;
                   }else if(i == pendingNum + 1){
                       return 0;
                   }else{
                       if(i < pendingNum + 1) {
                           return mFirendLists.get(i - 1);
                       }else{
                           return mFirendLists.get( i -2);
                       }
                   }
               }
           }else{
               if(i == 0){
                   return 0;
               }else if( i == pendingNum + 1){
                   return 1;
               }else if( i == pendingNum + goodNum + 2){
                   return 2;
               }else if( i < pendingNum + 1){
                   return mFirendLists.get(i - 1);
               }else if( i < pendingNum + goodNum + 2){
                   return mFirendLists.get(i - 2);
               }else{
                   return mFirendLists.get(i - 3);
               }
           }
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        HoldView mHoldView;
        Object item = getItem(i);
        if(item instanceof FriendInfo){
            if(view == null){
                view = LayoutInflater.from(mContext).inflate(R.layout.list_friendinfo_item,null);
                mHoldView = new HoldView();
                mHoldView.firstName = (TextView) view.findViewById(R.id.name_first_tv);
                mHoldView.nameTv = (TextView) view.findViewById(R.id.name_tv);
                mHoldView.prev_Iv = (ImageView) view.findViewById(R.id.prev_Iv);
                mHoldView.UnreadNum = (TextView) view.findViewById(R.id.unreadnum_tv);
                view.setTag(R.id.content_list);
            }else{
                mHoldView = (HoldView) view.getTag(R.id.content_list);
                if(mHoldView == null){
                    view = LayoutInflater.from(mContext).inflate(R.layout.list_friendinfo_item,null);
                    mHoldView = new HoldView();
                    mHoldView.firstName = (TextView) view.findViewById(R.id.name_first_tv);
                    mHoldView.nameTv = (TextView) view.findViewById(R.id.name_tv);
                    mHoldView.prev_Iv = (ImageView) view.findViewById(R.id.prev_Iv);
                    mHoldView.UnreadNum = (TextView) view.findViewById(R.id.unreadnum_tv);
                    view.setTag(R.id.content_list);
                }
            }

            FriendInfo info = (FriendInfo) item;
            if(info.getSex() == 0){
                mHoldView.firstName.setBackgroundResource(R.drawable.iconbg_f);
            }else{
                mHoldView.firstName.setBackgroundResource(R.drawable.iconbg_m);
            }

            mHoldView.firstName.setText(info.getName().substring(0,1));
            mHoldView.nameTv.setText(info.getName());

            if(info.getStatus() == 0){
                mHoldView.prev_Iv.setImageResource(R.drawable.friend_prev);
                mHoldView.UnreadNum.setVisibility(View.INVISIBLE);
            }else if(info.getStatus() == 1 ){
                mHoldView.prev_Iv.setImageResource(R.drawable.normal_prev);

                if(info.getUnReadNum() == 0){
                    mHoldView.UnreadNum.setVisibility(View.INVISIBLE);
                }else{
                    mHoldView.UnreadNum.setVisibility(View.VISIBLE);
                    mHoldView.UnreadNum.setText(info.getUnReadNum()+"");
                    mHoldView.prev_Iv.setImageResource(R.drawable.msg_prev);
                }
            }else{
                mHoldView.prev_Iv.setImageResource(R.drawable.normal_prev);
                mHoldView.UnreadNum.setVisibility(View.INVISIBLE);
            }

        }else{

            if(view == null){
                view = LayoutInflater.from(mContext).inflate(R.layout.list_friend_title_item,null);
                mHoldView = new HoldView();
                mHoldView.titleTV = (TextView) view.findViewById(R.id.title);
                mHoldView.new_friend_iv = (ImageView) view.findViewById(R.id.new_friend_iv);
                view.setTag(R.id.title_list);
            }else{
                mHoldView = (HoldView) view.getTag(R.id.title_list);
                if(mHoldView == null){
                    view = LayoutInflater.from(mContext).inflate(R.layout.list_friend_title_item,null);
                    mHoldView = new HoldView();
                    mHoldView.titleTV = (TextView) view.findViewById(R.id.title);
                    mHoldView.new_friend_iv = (ImageView) view.findViewById(R.id.new_friend_iv);
                    view.setTag(R.id.title_list);
                }
            }
            int status = ((Integer)item).intValue();
            if(status == 0){
                mHoldView.new_friend_iv.setVisibility(View.VISIBLE);
                mHoldView.titleTV.setText("フレンド承認待ち");
            }else if(status == 1){
                mHoldView.new_friend_iv.setVisibility(View.GONE);
                mHoldView.titleTV.setText("マイフレンド");
            }else if(status == 2){
                mHoldView.new_friend_iv.setVisibility(View.GONE);
                mHoldView.titleTV.setText("フレンド申請中");
            }
        }
        return view;
    }

    public class SortComparator implements Comparator {
        @Override
        public int compare(Object lhs, Object rhs) {
            FriendInfo a = (FriendInfo) lhs;
            FriendInfo b = (FriendInfo) rhs;

            return ( a.getStatus() - b.getStatus());
        }
    }

    class HoldView{
        TextView firstName,nameTv,UnreadNum,titleTV;
        ImageView prev_Iv,new_friend_iv;
    }
}
