package jp.gossip.jetkys.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.ui.views.ConsumTextView;
import com.app.utils.DisplayUtil;
import jp.gossip.jetkys.android.HoldView;
import jp.gossip.jetkys.android.R;
import jp.gossip.jetkys.android.manager.SharedManager;
import jp.gossip.jetkys.android.mode.PostInfo;

import java.util.ArrayList;

/**
 * Created by yangshiqin on 16/2/27.
 */
public class PostAdapter extends BaseAdapter{
    ArrayList<PostInfo> infoArrayList ;
    Context mContext;

    public PostAdapter(Context mContext,ArrayList<PostInfo> infoArrayList) {
        this.mContext = mContext;
        this.infoArrayList = infoArrayList;
    }

    @Override
    public int getCount() {
        if(infoArrayList != null){
            return infoArrayList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if(infoArrayList != null){
            return infoArrayList.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        if(infoArrayList != null){
            return infoArrayList.size();
        }
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        HoldView mHoldView;
        if(view == null){
            view = LayoutInflater.from(mContext).inflate(R.layout.post_list_item,null);
            view.setLayoutParams(new GridView.LayoutParams(getItemWidth(), getItemHeight()));

            mHoldView = new HoldView();
            mHoldView.initView(view);

            view.setTag(mHoldView);
        }else{
            mHoldView = (HoldView) view.getTag();
        }

        PostInfo info = (PostInfo) getItem(i);

        mHoldView.imageView.setImageResource(info.getImageRes());
        mHoldView.textView.setContentAndSizeD(info.getShowStr(),info.getShowFontSize(getTextViewWidth()),info.getDirtection());

        return view;
    }

    public int getItemWidth(){
        int width = 0;
        width = (SharedManager.WIDTH - 3* DisplayUtil.dp2px(mContext,5))/2;
        return width;
    }

    public int getTextViewWidth(){
        return getItemWidth() - DisplayUtil.dp2px(mContext,5)*2;
    }

    public int getItemHeight(){
        return SharedManager.HEIGHT*2/5;
    }

}
