package jp.gossip.jetkys.android.mode;

import java.io.Serializable;

/**
 * Created by yangshiqin on 16/2/27.
 */
public class PostInfo implements Serializable{

    private float fontSize = 70f;
    private int dirtection = 0; // 0:h ,1:v
    private String showStr = "";
    private int imageRes;
    private int width,height;

    public float getFontSize() {
        return fontSize;
    }

    public void setFontSize(float fontSize) {
        this.fontSize = fontSize;
    }

    public int getDirtection() {
        return dirtection;
    }

    public void setDirtection(int dirtection) {
        this.dirtection = dirtection;
    }

    public String getShowStr() {
        return showStr;
    }

    public void setShowStr(String showStr) {
        this.showStr = showStr;
    }

    public int getImageRes() {
        return imageRes;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getShowFontSize(int showWidth){
        float fontSizeShow = 70;
        fontSizeShow = fontSize*showWidth/width;
        return fontSizeShow;
    }
}
