package jp.gossip.jetkys.android.mode;

import java.io.Serializable;

/**
 * Created by yangshiqin on 16/2/28.
 */
public class FriendInfo implements Serializable{

    String name;
    int sex;  // 0:女  1:男
    int status = 0; // 确认状态  1:好友 2:申请中 0:待确认
    int unReadNum = 1;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUnReadNum() {
        return unReadNum;
    }

    public void setUnReadNum(int unReadNum) {
        this.unReadNum = unReadNum;
    }
}
