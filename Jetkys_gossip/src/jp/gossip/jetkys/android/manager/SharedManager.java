package jp.gossip.jetkys.android.manager;

import android.content.Context;
import android.view.WindowManager;
import com.app.models.RegiterInfo;
import com.app.utils.FileUtils;

/**
 * Created by yangshiqin on 16/2/25.
 */
public class SharedManager {
    public static SharedManager mSharedManager;
    Context mContext;

    public static int WIDTH;
    public static int HEIGHT;

    public static SharedManager create(Context mContext){
        if(mSharedManager == null){
            mSharedManager = new SharedManager(mContext);
        }
        return mSharedManager;
    }

    RegiterInfo info;

    public SharedManager(Context mContext) {
        this.mContext = mContext;
        init(mContext);
        getScreenWH();
    }

    void init(Context mContext){
        getInfo(mContext);
    }

    public RegiterInfo getInfo() {
        return info;
    }

    public void setInfo(RegiterInfo info) {
        this.info = info;
    }

    public RegiterInfo getInfo(Context mContext){
        info = (RegiterInfo) FileUtils.readSerializable(mContext,FileUtils.REGISITER_FILE_NAME);
        return info;
    }

    public void setInfo(Context mContext,RegiterInfo info){
        this.info = info;
        FileUtils.writeSerializable(mContext,FileUtils.REGISITER_FILE_NAME,info);
    }

    public void getScreenWH(){
        WindowManager wm = (WindowManager) mContext
                .getSystemService(Context.WINDOW_SERVICE);

        WIDTH = wm.getDefaultDisplay().getWidth();
        HEIGHT = wm.getDefaultDisplay().getHeight();
    }
}
