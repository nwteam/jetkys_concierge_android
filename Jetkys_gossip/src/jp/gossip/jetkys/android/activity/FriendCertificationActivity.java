package jp.gossip.jetkys.android.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.app.ui.activities.NavHeadBaseActivity;
import jp.gossip.jetkys.android.R;
import jp.gossip.jetkys.android.mode.FriendInfo;

/**
 * Created by yangshiqin on 16/2/28.
 */
public class FriendCertificationActivity extends NavHeadBaseActivity{

    FriendInfo info;

    TextView nameTv;

    @Override
    public void init() {
        super.init();
        setLeftImageBar(R.drawable.login_back, new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                backActivity();
            }
        });

        info = (FriendInfo) getIntent().getSerializableExtra("friend");
        nameTv.setText(info.getName());
    }

    @Override
    public void addListeners() {
        super.addListeners();
    }

    @Override
    public void findViews() {
        super.findViews();
        nameTv = (TextView) viewGroup.findViewById(R.id.name_tv);
    }

    @Override
    protected View onCreateContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewGroup = getLayoutInflater().inflate(R.layout.activity_certification_layout,null);
        findViews();
        init();
        addListeners();
        return viewGroup;
    }
}
