package jp.gossip.jetkys.android.activity;

import android.content.Intent;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.app.utils.DisplayUtil;
import com.app.utils.StringUtil;
import jp.gossip.jetkys.android.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;

import com.app.ui.activities.NavHeadBaseActivity;

public class PostTextActivity extends NavHeadBaseActivity implements OnClickListener{

	ImageView selectIV;
	TextView typeSendTv;
	EditText contentET;
	
	@Override
	public void findViews() {
		// TODO Auto-generated method stub
		super.findViews();
		
		selectIV = (ImageView) viewGroup.findViewById(R.id.iv_select);
		typeSendTv = (TextView) viewGroup.findViewById(R.id.tv_send_type);
		
		contentET = (EditText) viewGroup.findViewById(R.id.et_content);
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		setRightTitleBar(getString(R.string.next), new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				submit();
			}
		});

	}

	public void submit(){
		closeInput();
		if(StringUtil.isEmpty(contentET.getText().toString())){
			showToast("テキストを入力してください");
			return;
		}

		if(StringUtil.isEmpty(typeSendTv.getText().toString())){
			showToast("カテゴリを選択してください");
			return;
		}

		Intent intent = new Intent(this,PostCardActivity.class);
		intent.putExtra("type",typeSendTv.getText().toString());
		intent.putExtra("data", contentET.getText().toString());
		startActivity(intent);
	}

	public void closeInput(){
		InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(contentET.getWindowToken(),0);
	}

	@Override
	public void addListeners() {
		// TODO Auto-generated method stub
		super.addListeners();
		
		typeSendTv.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				AutoCompleteTextView view = (AutoCompleteTextView) v;
				if (hasFocus) {
					view.showDropDown();
					selectIV.setImageResource(R.drawable.select_on);
				} else {
					selectIV.setImageResource(R.drawable.select_off);
				}
			}
		});
		typeSendTv.setOnClickListener(this);
	}

	@Override
	protected View onCreateContent(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		viewGroup = inflater.inflate(R.layout.activity_post1_layout, container, false);
		findViews();
		init();
		addListeners();
        return viewGroup;
	}


	@Override
	public void onClick(View view) {
		if(typeSendTv == view){
			selectIV.setImageResource(R.drawable.select_on);
			showPopupWindow(typeSendTv);
		}
	}

	private void showPopupWindow(View view) {

		// 一个自定义的布局，作为显示的内容
		View contentView = LayoutInflater.from(this).inflate(
				R.layout.pop_window, null);

		String [] arr={"aa","aab","aac"};
		final ListView listView = (ListView) contentView.findViewById(R.id.popup_listview);
		listView.setAdapter(new ArrayAdapter<String>(this,
				R.layout.list_text_item, arr));

		final PopupWindow popupWindow = new PopupWindow(contentView,
				typeSendTv.getWidth(), AbsoluteLayout.LayoutParams.WRAP_CONTENT, true);

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//				typeSendTv.setText((CharSequence) listView.getAdapter().getItem(i));
				popupWindow.dismiss();
			}
		});

		popupWindow.setTouchable(true);

		popupWindow.setTouchInterceptor(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				return false;
				// 这里如果返回true的话，touch事件将被拦截
				// 拦截后 PopupWindow的onTouchEvent不被调用，这样点击外部区域无法dismiss
			}
		});

		popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
			@Override
			public void onDismiss() {
				selectIV.setImageResource(R.drawable.select_off);
			}
		});

		// 如果不设置PopupWindow的背景，无论是点击外部区域还是Back键都无法dismiss弹框
		// 我觉得这里是API的一个bug
		popupWindow.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.menu_list));

		// 设置好参数之后再show
		popupWindow.showAsDropDown(view);

	}
}
