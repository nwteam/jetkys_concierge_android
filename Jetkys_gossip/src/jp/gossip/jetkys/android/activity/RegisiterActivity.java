package jp.gossip.jetkys.android.activity;

import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.app.models.RegiterInfo;
import com.app.net.InterfaceIds;
import com.app.net.NetResult;
import com.app.net.controller.AccountController;
import com.app.ui.activities.BaseNetActivity;

import com.app.utils.StringUtil;
import jp.gossip.jetkys.android.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import jp.gossip.jetkys.android.manager.SharedManager;

public class RegisiterActivity extends BaseNetActivity implements OnClickListener{

	TextView twitterTv,facebookTv,GoogleTv;
	ImageButton backbtn;
	Button regisiterBtn;
	
	EditText idET,pswEt;
	AccountController mAccountController;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_layout);
		
		findView();
        init();
        addListener();
	}

	void closeInput()
	{
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	 void findView(){
	    	twitterTv = (TextView) findViewById(R.id.tv_regisiter_twitter);
	    	facebookTv = (TextView) findViewById(R.id.tv_regisiter_facebook);
	    	GoogleTv = (TextView) findViewById(R.id.tv_regisiter_google);
	    	
	    	backbtn = (ImageButton) findViewById(R.id.ib_login_back);
	    	regisiterBtn = (Button) findViewById(R.id.regisiter_btn);
	    	
	    	idET = (EditText) findViewById(R.id.et_regisiter_user_id);
	    	pswEt = (EditText) findViewById(R.id.et_regisiter_user_psw);
	    }
	    
	    public void init(){
	    	twitterTv.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);//�»���
	    	facebookTv.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);//�»���
	    	GoogleTv.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);//�»���
	    	
	    	mAccountController = new AccountController(this);
	    }
	    
	    void addListener(){
	    	twitterTv.setOnClickListener(this);
	    	facebookTv.setOnClickListener(this);
	    	GoogleTv.setOnClickListener(this);
	    	
	    	backbtn.setOnClickListener(this);
	    	regisiterBtn.setOnClickListener(this);
	    }

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			closeInput();
			if(arg0 == twitterTv){
				
			}else if(arg0 == facebookTv){
				
			}else if(arg0 == GoogleTv){
				
			}else if(arg0 == backbtn){
				finish();
			}else if(arg0 == regisiterBtn){
				regisiter();
			}
		}

	RegiterInfo mRegiterInfo = new RegiterInfo();

		public void regisiter(){
			String idStr = idET.getText().toString();
			if(StringUtil.isEmpty(idStr)){
				showToast(getString(R.string.regisiter_mail)+getString(R.string.hint));
				return;
			}
			String pswStr = pswEt.getText().toString();
			if(StringUtil.isEmpty(pswStr)){
				showToast(getString(R.string.password)+getString(R.string.hint));
				return;
			}
			mRegiterInfo.setEmail(idStr);
			mRegiterInfo.setPassword(pswStr);
			mRegiterInfo.setName("xx");
			showDialog();
			mAccountController.register("xx", idStr, pswStr);
		}

		@Override
		public void onRequestSuccess(int ifId, NetResult obj) {
			// TODO Auto-generated method stub
			super.onRequestSuccess(ifId, obj);
			if(ifId == InterfaceIds.USR_REGISTER.IF_ID){
				RegiterInfo info = (RegiterInfo) obj.getResultObject();
				info.setEmail(mRegiterInfo.getEmail());
				info.setName(mRegiterInfo.getName());
				info.setPassword(mRegiterInfo.getPassword());
				SharedManager.create(this).setInfo(this,info);
			}
		}

		@Override
		public void onRequestError(int ifId, NetResult errMsg) {
			// TODO Auto-generated method stub
			super.onRequestError(ifId, errMsg);
		}

		 void CreateDialog(String message){
		    	AlertDialog.Builder builder = new Builder(this);
		    	  builder.setMessage(message);  
		    	  builder.setTitle(getString(R.string.hint_dialog));  
		    	  builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {   
		    		@Override
		    	   public void onClick(DialogInterface dialog, int which) {
		    	    dialog.dismiss();    
		    	    finish();
		    	   }
		    	  }); 
		    	  builder.create().show();
		    }
		
}
