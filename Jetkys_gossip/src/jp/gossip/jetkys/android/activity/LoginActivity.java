package jp.gossip.jetkys.android.activity;

import com.app.models.RegiterInfo;
import com.app.net.InterfaceIds;
import com.app.net.NetResult;
import com.app.net.controller.AccountController;
import com.app.ui.activities.BaseNetActivity;
import com.app.utils.ZLog;

import jp.gossip.jetkys.android.R;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import jp.gossip.jetkys.android.manager.SharedManager;


public class LoginActivity extends BaseNetActivity implements OnClickListener{

	TextView twitterTv,facebookTv,GoogleTv;
	ImageButton backbtn;
	Button loginBtn,regisiterBtn;
	
	EditText idET,pswEt;
	AccountController mAccountController;

	RegiterInfo info;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_layout);
        
        findView();
        init();
        addListener();
    }
    
    void findView(){
    	twitterTv = (TextView) findViewById(R.id.tv_login_twitter);
    	facebookTv = (TextView) findViewById(R.id.tv_login_facebook);
    	GoogleTv = (TextView) findViewById(R.id.tv_login_google);
    	
    	backbtn = (ImageButton) findViewById(R.id.ib_login_back);
    	loginBtn = (Button) findViewById(R.id.login_btn);
    	regisiterBtn = (Button) findViewById(R.id.regisiter_btn);
    	
    	idET = (EditText) findViewById(R.id.et_login_user_id);
    	pswEt = (EditText) findViewById(R.id.et_login_user_psw);
    }
    
    public void init(){
    	twitterTv.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);//�»���
    	facebookTv.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);//�»���
    	GoogleTv.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);//�»��� 
    	
    	mAccountController = new AccountController(this);

		info = SharedManager.create(this).getInfo(this);
    }
    
    @Override
	public void onRequestSuccess(int ifId, NetResult obj) {
		// TODO Auto-generated method stub
//		super.onRequestSuccess(ifId, obj);
    	ZLog.v("login  ifId="+ifId);
		if(ifId == InterfaceIds.USR_LOGIN.IF_ID){
			CreateDialog((String)obj.getResultObject());
		}
	}

    void CreateDialog(String message){
    	ZLog.v("message="+message);
    	AlertDialog.Builder builder = new Builder(this);
    	  builder.setMessage(message);  
    	  builder.setTitle(getString(R.string.hint_dialog));  
    	  builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {   
    		@Override
    	   public void onClick(DialogInterface dialog, int which) {
    	    dialog.dismiss();    
    	    startHomeActivity();
    	   }
    	  }); 
    	  builder.create().show();
    }
    
	@Override
	public void onRequestError(int ifId, NetResult errMsg) {
		// TODO Auto-generated method stub
		super.onRequestError(ifId, errMsg);
	}

	void addListener(){
    	twitterTv.setOnClickListener(this);
    	facebookTv.setOnClickListener(this);
    	GoogleTv.setOnClickListener(this);
    	
    	backbtn.setOnClickListener(this);
    	loginBtn.setOnClickListener(this);
    	regisiterBtn.setOnClickListener(this);
    }

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if(arg0 == twitterTv){
			
		}else if(arg0 == facebookTv){
			
		}else if(arg0 == GoogleTv){
			
		}else if(arg0 == backbtn){
			
		}else if(arg0 == loginBtn){
			mAccountController.login("", "");
//			startHomeActivity();
		}else if(arg0 == regisiterBtn){
			Intent intent = new Intent(this,RegisiterActivity.class);
			startActivity(intent);
		}
	}
	
	void startHomeActivity(){
		Intent intent = new Intent(this,HomeActivity.class);
		startActivity(intent);
	}

}
