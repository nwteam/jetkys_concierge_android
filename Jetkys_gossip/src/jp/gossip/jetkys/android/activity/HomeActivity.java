package jp.gossip.jetkys.android.activity;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.*;
import com.app.utils.DisplayUtil;
import jp.gossip.jetkys.android.R;
import jp.gossip.jetkys.android.fragment.*;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;

import com.app.AppManager;
import com.app.ui.fragment.FragmentController;
import com.app.utils.ZLog;

public class HomeActivity extends FragmentActivity implements OnClickListener{

	FragmentController mFragmentController;
	RelativeLayout  topLayout,classicLayout,chatLayout,callCenterLayout;

	private RelativeLayout mCurrentLayout = null;// 当前选择项
	private int mPositionIndex = 0;// 当前项的索引
	
	ImageView mMenuList;
	
	int[] seletorRes={R.drawable.top_on,R.drawable.category_on,R.drawable.chat_on,R.drawable.concierge_on};
	int[] Res ={R.drawable.top_off,R.drawable.category_off,R.drawable.chat_off,R.drawable.concierge_off};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_layout);
		AppManager.getAppManager().addActivity(this);
		findViews();
		init();
		addListeners();
	}
	
	public void findViews(){
		topLayout = (RelativeLayout) findViewById(R.id.ll_home_top);
		classicLayout = (RelativeLayout) findViewById(R.id.ll_home_classic);
		chatLayout = (RelativeLayout) findViewById(R.id.ll_home_chat);
		callCenterLayout = (RelativeLayout) findViewById(R.id.ll_home_callcenter);
		


		mMenuList = (ImageView) findViewById(R.id.menu_list);
	}
	
	public void init(){
		mFragmentController = new FragmentController(getSupportFragmentManager(), R.id.realtabcontent);
		Bundle bundle = new Bundle();
		bundle.putInt("type",0);
		mFragmentController.add(TabFragment.class, "top", bundle);
		mCurrentLayout = topLayout;
		mPositionIndex = 0;

		mCurrentLayout.getChildAt(0).setVisibility(View.VISIBLE);
		((ImageView)mCurrentLayout.getChildAt(1)).setImageResource(seletorRes[mPositionIndex]);
	}
	
	public void addListeners(){
		topLayout.setOnClickListener(this);
		classicLayout.setOnClickListener(this);
		chatLayout.setOnClickListener(this);
		callCenterLayout.setOnClickListener(this);
		
		mMenuList.setOnClickListener(this);
		
	}
	/**
	 * 判断是否已经点击过一次回退键	 */
	private boolean isBackPressed = false;
	private void doublePressBackToast() {
		if (!isBackPressed) {
			isBackPressed = true;
			Toast.makeText(this, getString(R.string.again_back), Toast.LENGTH_SHORT).show();
		} else {
			AppManager.getAppManager().appExit();
		}

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				isBackPressed = false;
			}
		}, 2000);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBackPressed();
			ZLog.v("onKeyUp result=" + result);
			if(!result) {
				doublePressBackToast();
			}
			return true;
		} else {
			return super.onKeyUp(keyCode, event);
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if(arg0 == topLayout){
			changeCurrentClickState(0, topLayout, TabFragment.class, "top");
		}else if(arg0 == classicLayout){
			changeCurrentClickState(1,classicLayout,TabFragment.class,"classic");
		}else if(arg0 == chatLayout){
			changeCurrentClickState(2,chatLayout,TabFragment.class,"chat");
		}else if(arg0 == callCenterLayout){
			changeCurrentClickState(3,callCenterLayout,TabFragment.class,"callcenter");
		}else if(arg0 == mMenuList){
			showPopupWindow(mMenuList);
		}
	}

	public void gotoPersonal(){
		PersonalFragment fragment = new PersonalFragment();
		mFragmentController.addBackFragment(fragment);
	}

	public void gotoShop(){
		ShopFragment fragment = new ShopFragment();
		mFragmentController.addBackFragment(fragment);
	}

	// 选中某项，改变状态
		private void changeCurrentClickState(int positionIndex, RelativeLayout layout, Class<? extends Fragment> clazz,
				String tag) {

			mCurrentLayout.getChildAt(0).setVisibility(View.INVISIBLE);
			((ImageView)mCurrentLayout.getChildAt(1)).setImageResource(Res[mPositionIndex]);

			mPositionIndex = positionIndex;
			mCurrentLayout = layout;
			mCurrentLayout.getChildAt(0).setVisibility(View.VISIBLE);
			((ImageView)mCurrentLayout.getChildAt(1)).setImageResource(seletorRes[mPositionIndex]);
			Bundle bundle = new Bundle();
			bundle.putInt("type",positionIndex);
			mFragmentController.add(clazz, tag, bundle);
		}

		@Override
		protected void onActivityResult(int arg0, int arg1, Intent arg2) {
			// TODO Auto-generated method stub
			super.onActivityResult(arg0, arg1, arg2);
			mFragmentController.getCurrentFragment().onActivityResult(arg0, arg1, arg2);
			ZLog.v("requestCode=" + arg0 + " resultCode=" + arg1);
		}

		boolean result = false;



		@Override
		public void onBackPressed() {
			ZLog.e("onBackPressed");
			result = false;
			FragmentManager fm = getSupportFragmentManager();
//			if (mPositionIndex == 0) {
//				/** クーポン */
				Fragment f = fm.findFragmentByTag(FragmentController.fragmentTags[mPositionIndex]);
				ZLog.e("f ="+f+" f.getChildFragmentManager()\n" +
						"getBackStackEntryCount()="+f.getChildFragmentManager()
						.getBackStackEntryCount()+"  fm="+fm.getBackStackEntryCount());
				if (null != f
						&& 0 != f.getChildFragmentManager()
						.getBackStackEntryCount()) {
//					fm.popBackStack();
//					super.onBackPressed();
					f.getChildFragmentManager().popBackStack();
					result = true;
				}
//			} else if (FragmentConstants.TAB_SHOP_SEARCH.equals(mTabHost
//					.getCurrentTabTag())) {
//				/** 店舗検索 */
//				Fragment f = fm
//						.findFragmentByTag(FragmentConstants.TAB_SHOP_SEARCH);
//				if (null != f
//						&& 0 != f.getChildFragmentManager()
//								.getBackStackEntryCount()) {
//					f.getChildFragmentManager().popBackStack();
//					result = true;
//				}
//			} else if (FragmentConstants.TAG_NEWS.equals(mTabHost
//					.getCurrentTabTag())) {
//				/** ニュース */
//				Fragment f = fm.findFragmentByTag(FragmentConstants.TAG_NEWS);
//				if (null != f
//						&& 0 != f.getChildFragmentManager()
//								.getBackStackEntryCount()) {
//					f.getChildFragmentManager().popBackStack();
//					result = true;
//				}
//			}else if (FragmentConstants.TAG_SETTINGS.equals(mTabHost
//					.getCurrentTabTag())) {
//				/** settings */
//				Fragment f = fm.findFragmentByTag(FragmentConstants.TAG_SETTINGS);
//				if (null != f
//						&& 0 != f.getChildFragmentManager()
//								.getBackStackEntryCount()) {
//					f.getChildFragmentManager().popBackStack();
//					result = true;
//				}
//			}

			if (false == result) {
//				super.onBackPressed();
			}
		}

	private void showPopupWindow(View view) {

		// 一个自定义的布局，作为显示的内容
		View contentView = LayoutInflater.from(this).inflate(
				R.layout.pop_window, null);

		String[] strs = new String[] {
				"Q&A", "利用規約", "特商法", "fourth", "fifth"
		};
		ListView listView = (ListView) contentView.findViewById(R.id.popup_listview);
		listView.setAdapter(new ArrayAdapter<String>(this,
				R.layout.simple_list_item, strs));

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

			}
		});

		final PopupWindow popupWindow = new PopupWindow(contentView,
				DisplayUtil.dp2px(this,100), AbsoluteLayout.LayoutParams.WRAP_CONTENT, true);

		popupWindow.setTouchable(true);

		popupWindow.setTouchInterceptor(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				return false;
				// 这里如果返回true的话，touch事件将被拦截
				// 拦截后 PopupWindow的onTouchEvent不被调用，这样点击外部区域无法dismiss
			}
		});

		// 如果不设置PopupWindow的背景，无论是点击外部区域还是Back键都无法dismiss弹框
		// 我觉得这里是API的一个bug
		popupWindow.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.menu_list));

		// 设置好参数之后再show
		popupWindow.showAsDropDown(view);

	}
}
