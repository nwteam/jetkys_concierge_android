package jp.gossip.jetkys.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.app.ui.activities.NavHeadBaseActivity;
import com.app.ui.views.HorizontalListView;
import com.app.utils.DisplayUtil;
import jp.gossip.jetkys.android.HoldView;
import jp.gossip.jetkys.android.R;
import jp.gossip.jetkys.android.adapter.Post1Adapter;
import jp.gossip.jetkys.android.manager.SharedManager;
import jp.gossip.jetkys.android.mode.PostInfo;

import java.util.ArrayList;

/**
 * Created by yangshiqin on 16/2/27.
 */
public class PostDetailActivity extends NavHeadBaseActivity implements View.OnClickListener{

    HoldView mHoldView;
    RelativeLayout postDetailLayout;
    HorizontalListView mHListView;

    Button favoriteBtn,msgBtn,commentBtn;

    PostInfo info;

    ArrayList<PostInfo> postInfos;

    @Override
    public void init() {
        super.init();
        setLeftImageBar(R.drawable.back, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backActivity();
            }
        });

        info = (PostInfo) getIntent().getSerializableExtra("post");
        mHoldView.imageView.setImageResource(info.getImageRes());
        mHoldView.textView.setContentAndSizeD(info.getShowStr(), info.getShowFontSize(getTextViewWidth()), info.getDirtection());

        testData();
        mHListView.setAdapter(new Post1Adapter(this, postInfos));
    }

    @Override
    public void addListeners() {
        super.addListeners();
        mHListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(PostDetailActivity.this, PostDetailActivity.class);
                intent.putExtra("post", postInfos.get(i));
                startActivity(intent);
                backActivity();
            }
        });

        favoriteBtn.setOnClickListener(this);
        msgBtn.setOnClickListener(this);
        commentBtn.setOnClickListener(this);
    }

    @Override
    public void findViews() {
        super.findViews();
        postDetailLayout = (RelativeLayout) viewGroup.findViewById(R.id.postDetail_layout);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(getItemWidth(),getItemHeight());
        lp.setMargins(DisplayUtil.dp2px(this, 5),
                DisplayUtil.dp2px(this, 5),
                DisplayUtil.dp2px(this, 5),
                DisplayUtil.dp2px(this, 5));
        postDetailLayout.setLayoutParams(lp);
        mHoldView = new HoldView();
        mHoldView.initView(viewGroup);

        mHListView = (HorizontalListView) viewGroup.findViewById(R.id.hListView);

        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,SharedManager.HEIGHT*2/5);
        lp1.setMargins(DisplayUtil.dp2px(this, 5),
                0,
                DisplayUtil.dp2px(this, 5),
                DisplayUtil.dp2px(this, 5));
        mHListView.setLayoutParams(lp1);

        favoriteBtn = (Button) viewGroup.findViewById(R.id.favorite_btn);
        msgBtn = (Button) viewGroup.findViewById(R.id.msg_btn);
        commentBtn = (Button) viewGroup.findViewById(R.id.comment_btn);
    }

    @Override
    protected View onCreateContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewGroup = getLayoutInflater().inflate(R.layout.activity_postdetail_layout,null);
        findViews();
        init();
        addListeners();
        return viewGroup;
    }

    public int getItemWidth(){
        int width = 0;
        width = SharedManager.WIDTH - 2* DisplayUtil.dp2px(this, 5);
        return width;
    }

    public int getTextViewWidth(){
        return getItemWidth() - DisplayUtil.dp2px(this,5)*2;
    }

    public int getItemHeight(){
        return SharedManager.HEIGHT*5/7;
    }

    public void testData(){
        postInfos = new ArrayList<PostInfo>();
        PostInfo info = new PostInfo();
        info.setDirtection(0);
        info.setFontSize(60);
        info.setShowStr("adbcjhioas\noasd");
        info.setImageRes(R.drawable.bg_1);
        info.setWidth(508);
        info.setHeight(360);
        postInfos.add(info);

        info = new PostInfo();
        info.setDirtection(1);
        info.setFontSize(70);
        info.setShowStr("adbcj\nhioas\noasd");
        info.setImageRes(R.drawable.bg_2);
        info.setWidth(724);
        info.setHeight(1280);
        postInfos.add(info);

        info = new PostInfo();
        info.setDirtection(0);
        info.setFontSize(70);
        info.setShowStr("adbcjhioas\noasd");
        info.setImageRes(R.drawable.bg_3);
        info.setWidth(720);
        info.setHeight(1280);

        postInfos.add(info);
        info = new PostInfo();
        info.setDirtection(1);
        info.setFontSize(70);
        info.setShowStr("adbcj\nhioas\noasd");
        info.setImageRes(R.drawable.bg_4);
        info.setWidth(600);
        info.setHeight(450);
        postInfos.add(info);
    }

    @Override
    public void onClick(View view) {
        if(view == favoriteBtn){

        }else if(view == msgBtn){

        }else if(view == commentBtn){
            Intent intent = new Intent(this,PostTextActivity.class);
            startActivity(intent);
        }
    }
}
