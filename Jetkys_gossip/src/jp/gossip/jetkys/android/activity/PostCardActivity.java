package jp.gossip.jetkys.android.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.*;
import android.widget.*;
import com.app.ui.activities.NavBaseActivity;
import com.app.ui.activities.NavHeadBaseActivity;
import com.app.ui.views.ConsumTextView;
import com.app.ui.views.PictureSelect;
import com.app.utils.DisplayUtil;
import com.app.utils.FileUtils;
import com.app.utils.Utils;
import com.app.utils.ZLog;
import jp.gossip.jetkys.android.R;
import jp.gossip.jetkys.android.manager.SharedManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yangshiqin on 16/2/24.
 */
public class PostCardActivity extends NavHeadBaseActivity implements View.OnClickListener,ViewPager.OnPageChangeListener{

    ImageView listImage,cameraImage,fontIv,sizeIv,mukiIv;
    ImageView bgIV;

    ConsumTextView textView;

    PictureSelect pictureSelect;
    RelativeLayout contantLayout;

    int muki = 0; // 0:h,1:v
    int fontSize = 50;

    ViewPager mViewPager;
    private List<View> views = new ArrayList<View>();

    int[] imageBg = {R.drawable.bg_2,R.drawable.bg_1,R.drawable.bg_3,R.drawable.bg_4};

    @Override
    protected View onCreateContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewGroup = inflater.inflate(R.layout.activity_postcard_layout, container, false);
        findViews();
        init();
        addListeners();
        return viewGroup;
    }

    @Override
    public void findViews() {
        super.findViews();
        listImage = (ImageView) viewGroup.findViewById(R.id.list_image_iv);
        cameraImage = (ImageView) viewGroup.findViewById(R.id.camera_iv);
        fontIv = (ImageView) viewGroup.findViewById(R.id.font_iv);
        sizeIv = (ImageView) viewGroup.findViewById(R.id.size_iv);
        mukiIv = (ImageView) viewGroup.findViewById(R.id.muki_iv);

        bgIV = (ImageView) viewGroup.findViewById(R.id.bg_iv);

        textView = (ConsumTextView) viewGroup.findViewById(R.id.consumText);

        contantLayout = (RelativeLayout) viewGroup.findViewById(R.id.contant_layout);
        mViewPager = (ViewPager) viewGroup.findViewById(R.id.viewpager);
        mViewPager.setVisibility(View.INVISIBLE);
    }


    public void initViewPager(){
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setPageMargin(DisplayUtil.dp2px(this, 10));
        contantLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return mViewPager.dispatchTouchEvent(motionEvent);
            }
        });
        mViewPager.setOnPageChangeListener(this);

        bgIV.setImageResource(imageBg[0]);

    }

    public void initPageViews(){
        views.clear();
        for(int i = 0;i< imageBg.length;i++){
            View view = getLayoutInflater().inflate(R.layout.post_image_item, null);
            views.add(view);
            bitmaps.add(null);
        }
        ZLog.v("views.size=" + views.size());
        setRightMsgShow(false);
        mViewPager.setVisibility(View.VISIBLE);
        mViewPager.setAdapter(new MyAdapter());
        mViewPager.setCurrentItem(0);
        setBgIV(0);
        textView.setVisibility(View.INVISIBLE);



    }

   Handler handle = new Handler(){
       @Override
       public void handleMessage(Message msg) {
           super.handleMessage(msg);
           if(msg.what == mViewPager.getCurrentItem()){
               setBgIV(msg.what);
           }
       }
   };

    ArrayList<Bitmap> bitmaps = new ArrayList<Bitmap>();

    public void setBgIV(final int position){
        if(bitmaps.get(position) != null){
            bgIV.setImageBitmap(bitmaps.get(position));
        }else{
            final Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imageBg[position]);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Bitmap bitmapTemp = Utils.fastblur(PostCardActivity.this, bitmap, 30);
                    bitmaps.remove(position);
                    bitmaps.add(position,bitmapTemp);
                    handle.sendEmptyMessage(position);
                }
            }).start();
        }
    }

    String contentStr;
    String typeStr;
    @Override
    public void init() {
        super.init();

        setRightTitleBar(getString(R.string.contribute), new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                submit();
            }
        });

        contentStr = getIntent().getStringExtra("data");
        typeStr = getIntent().getStringExtra("type");

        ZLog.e("contentStr="+contentStr+" typeStr="+typeStr);

        pictureSelect = new PictureSelect(this);

        textView.setContentAndSizeD(contentStr,fontSize,muki);

        initViewPager();
    }

    public void submit(){

    }

    @Override
    public void addListeners() {
        super.addListeners();

        listImage.setOnClickListener(this);
        cameraImage.setOnClickListener(this);
        fontIv.setOnClickListener(this);
        sizeIv.setOnClickListener(this);
        mukiIv.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(listImage == view){
            initPageViews();
            return;
        }
        if(mViewPager.getVisibility() == View.VISIBLE){
            HideViewPage();
        }
        else if(cameraImage == view){
            pictureSelect.showAsDropDown(mBackIB);
        }else if(fontIv == view){

        }else if(sizeIv == view){
            showDialogFontSize();
        }else if(mukiIv == view){
            if(muki == 0){
               // VertialText();
                muki = 1;
            }else{
                muki = 0;
            }
            textView.setDirtection(muki);
        }
    }

    public void HideViewPage(){
        mViewPager.setVisibility(View.INVISIBLE);
        textView.setVisibility(View.VISIBLE);
        setRightMsgShow(true);
        bgIV.setImageResource(imageBg[mViewPager.getCurrentItem()]);
    }

    // PagerAdapter是object的子类
    class MyAdapter extends PagerAdapter {

        /**
         * PagerAdapter管理数据大小
         */
        @Override
        public int getCount() {
            return views.size();
        }

        /**
         * 关联key 与 obj是否相等，即是否为同一个对象
         */
        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj; // key
        }

        /**
         * 销毁当前page的相隔2个及2个以上的item时调用
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object); // 将view 类型 的object熊容器中移除,根据key
        }

        /**
         * 当前的page的前一页和后一页也会被调用，如果还没有调用或者已经调用了destroyItem
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = views.get(position);
            ImageView itemBg = (ImageView) view.findViewById(R.id.bg_iv);
            float witdh = SharedManager.WIDTH - DisplayUtil.dp2px(PostCardActivity.this,45)*2;
            itemBg.setImageResource(imageBg[position]);
            ConsumTextView textView = (ConsumTextView) view.findViewById(R.id.consumText);
            textView.setContentAndSizeD(contentStr,fontSize * witdh/SharedManager.WIDTH  ,muki);
            container.addView(view,position);
            return views.get(position); // 返回该view对象，作为key
        }
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (contantLayout != null) {
            contantLayout.invalidate();
        }
    }

    // 一个新页被调用时执行,仍为原来的page时，该方法不被调用
    public void onPageSelected(int position) {
//        tvTitle.setText(getFile(position));
        setBgIV(position);
    }

    /*
     * SCROLL_STATE_IDLE: pager处于空闲状态 SCROLL_STATE_DRAGGING： pager处于正在拖拽中
     * SCROLL_STATE_SETTLING： pager正在自动沉降，相当于松手后，pager恢复到一个完整pager的过程
     */
    public void onPageScrollStateChanged(int state) {
    }

    public void showDialogFontSize(){
        final Dialog mDialog = new AlertDialog.Builder(this).create();
        mDialog.show();

        Window window = mDialog.getWindow();
        window.setContentView(R.layout.dialog_font_size_layout);

        final NumberPicker numberPicker = (NumberPicker) window.findViewById(R.id.fontSize_np);
        Button sumbit = (Button) window.findViewById(R.id.submit);

        numberPicker.setMaxValue(100);
        numberPicker.setMinValue(20);
        numberPicker.setValue(fontSize);

        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fontSize = numberPicker.getValue();
                textView.setFontSize(fontSize);
                mDialog.dismiss();
            }
        });
    }

    /**
     * 处理用户头像图片
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    private void disposeUserImage(int requestCode, int resultCode, Intent data) {
        String urifile;
        if (requestCode == Utils.REQUEST_PICK_PHOTO) {
            if (pictureSelect.getCameraUri() == null) {
//                Toast.makeText(this, "此次未选择图片！", Toast.LENGTH_SHORT).show();
                return;
            }
            urifile = pictureSelect.getCameraUri().toString().substring(6, pictureSelect.getCameraUri().toString().length());
        } else {
            if (data == null) {
//                Toast.makeText(this, "此次未选择图片！", Toast.LENGTH_SHORT).show();
                return;
            }
            urifile = FileUtils.getRealFilePath(this, data.getData());
        }
        if (urifile == null) {
//            Toast.makeText(this, "此次未选择图片！", Toast.LENGTH_SHORT).show();
            return;
        }
        file = new File(urifile);
        Bitmap bitmap = BitmapFactory.decodeFile(urifile);
        if (bitmap != null) {
            bgIV.setImageBitmap(bitmap);
        }
    }

    File file;

    /**
     * 选择上传的图片，返回的图片数据从此方法接受保存，后续的上传图片从此保存的拿数据
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Utils.REQUEST_PICK_PHOTO || requestCode == Utils.REQUEST_TAKE_PHOTO) {
            disposeUserImage(requestCode, resultCode, data);
        }
    }
}
