package jp.gossip.jetkys.android;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.app.ui.views.ConsumTextView;
import jp.gossip.jetkys.android.mode.PostInfo;

/**
 * Created by yangshiqin on 16/2/27.
 */
public class HoldView {
    public ImageView imageView;
    public ConsumTextView textView;
    public TextView timeTv,favoriteTV,commentTv;
    public RelativeLayout postItem;

    public void initView(View view){
        imageView = (ImageView) view.findViewById(R.id.list_image_iv);
        textView = (ConsumTextView) view.findViewById(R.id.textView);
        commentTv = (TextView) view.findViewById(R.id.comment_tv);
        favoriteTV = (TextView) view.findViewById(R.id.favorite_tv);
        timeTv = (TextView) view.findViewById(R.id.time_tv);
        postItem = (RelativeLayout) view.findViewById(R.id.post_layout);
    }

}
